<?php
/**
 * AudDataObject abstraction for DB_DataObject
 */
require_once 'DB/DataObject.php';
require_once '../commons/fecha.php';

class AudDataObject extends DB_DataObject{
    ///////////////////////////////////////////////////////////////////////////
    
    public $selfDAO=null;
    
    ///////////////////////////////////////////////////////////////////////////

    public function insert(){
        $this->aud_ins_date=Fecha::fechaActual();
        $this->aud_upd_date='1000-01-01 00:00:00';
        $this->indice1 = null;
        return parent::insert();
    }

    ///////////////////////////////////////////////////////////////////////////

    public function update($dataObject = false){
        $this->aud_upd_date=Fecha::fechaActual();
        return parent::update();
    }

    ///////////////////////////////////////////////////////////////////////////
    
    public function __construct(){
        $this->selfDAO=$this;
    }

    ///////////////////////////////////////////////////////////////////////////

}
