<?php
/**
 * Table Definition for comentario
 */
require_once '../DAOs/AudDataObject.php';

class DataObjects_Comentario extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'comentario';                      // table name
    public $id_comentario;                   // int(11)  not_null primary_key auto_increment
    public $texto;                           // blob(65535)  not_null blob binary
    public $id_tarea;                        // int(10)  not_null multiple_key unsigned
    public $fecha;                           // datetime(19)  not_null binary
    public $autor;                           // string(45)  not_null
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
################################################################################