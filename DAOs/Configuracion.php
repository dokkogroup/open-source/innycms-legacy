<?php
/**
 * Table Definition for configuracion
 */
require_once '../DAOs/AudDataObject.php';
require_once '../commons/dokko_configurator.php';
require_once 'Validate.php';

define("configuracion_string",0);
define("configuracion_integer",1);
define("configuracion_image",2);
define("configuracion_sound",3);
define("configuracion_multiselect",4);
define("configuracion_time",5);
define("configuracion_date",6);

class DataObjects_Configuracion extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'configuracion';                   // table name
    public $id_configuracion;                // int(11)  not_null primary_key auto_increment
    public $nombre;                          // string(50)  not_null multiple_key
    public $valor;                           // blob(65535)  blob binary
    public $estado;                          // int(11)  not_null
    public $tipo;                            // int(11)  not_null
    public $metadata;                        // blob(65535)  blob binary
    public $descripcion;                     // string(150)  not_null
    public $id_tipoconfiguracion;            // int(10)  multiple_key unsigned
    public $indice1;                         // int(10)  multiple_key unsigned
    public $indice2;                         // int(10)  multiple_key unsigned
    public $filtro;                          // blob(65535)  blob binary
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    function hayQueMostrar(){
        if($this->filtro=='') {return true;}
        $res=evaluateConfigurationExpression($this->filtro,$this->indice1,$this->indice2);
        if($res===null){
            Denko::addErrorMessage('filter_error',null,array('%field'=>$this->descripcion." (".$this->nombre.")"));
            return false;
        }
        return $res;
    }

    function parseOptions(){
        $arr=explode('|',$this->metadata);
        $res=array();
        foreach($arr as $val){
            if($val=="") continue;
            $ax=explode('=',$val);
            if(count($ax)!=2) continue;
            $res[$ax[0]]=$ax[1];
        }
        return $res;
    }

    function validarTipo(){
        if($this->estado==0) return true;

        switch($this->tipo){
            case configuracion_string:
                return Validate::string($this->valor,$this->parseOptions());
            case configuracion_integer:
                return Validate::number($this->valor,$this->parseOptions());
            case configuracion_image: return true;
            case configuracion_sound: return false;
            case configuracion_multiselect:
               return in_array($this->valor,explode('|',$this->metadata));
            case configuracion_time:
                return preg_match("/[0-2][0-9]:[0-5][0-9]:[0-5][0-9]\$/",$this->valor);
            case configuracion_date: return true;
            default: return true;
        }
    }

    function insert(){
        if(!$this->validarTipo()){
            Denko::addErrorMessage('config_error_in_type',null,array('%field'=>$this->descripcion." (".$this->nombre.")"));
            return false;
        }
        $res=parent::insert();
        if($res){
            Denko::addOkMessage('config_saved',null,array('%field'=>$this->descripcion." (".$this->nombre.")"));
        }
        return $res;
    }

    function update($dataObject = false){
        if(!$this->validarTipo()){
            Denko::addErrorMessage('config_error_in_type',null,array('%field'=>$this->descripcion." (".$this->nombre.")"));
            return false;
        }
        $modificado=false;
        foreach($this->clon as $key => $value ){
            if($key=="clon") continue;
            if($key=="selfDAO") continue;
            if($this->$key == $value) continue;
            $modificado=true;
        }
        if(!$modificado){
            return 0;
        }
        $res=parent::update();
        if($res){
            Denko::addOkMessage('config_saved',null,array('%field'=>$this->descripcion." (".$this->nombre.")"));
        }
        return $res;
    }
    
    function fetch(){
        $res=parent::fetch();
        $this->clon=clone($this);
        return $res;
    }

}
