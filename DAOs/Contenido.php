<?php
/**
 *
 *
 */
require_once '../DAOs/Configuracion.php';
require_once '../commons/inny.core.php';

/**
 *
 */
class Contenido extends DataObjects_Configuracion{

    /**
     * @var array arreglo que contiene la metadata parseada
     * @access public
     */
    public $parsedMetadata;

    /**
     * @var array cache del contenido
     * @access public
     */
    public $cache;

    /**
     * @var string prefijo del nombre de la variable global que contiene los valores de la cach�
     * @access public
     * @static
     */
    public static $cache_prefix = 'INNY_CACHE_CONTENIDO_';

    /**
     * Constructora
     *
     * @param string $nombre nombre de la configuraci�n
     * @param integer $indice1 ID del sitio de la configuraci�n
     * @access public
     */
    public function __construct($nombre,$indice1=null){

        DB_DataObject::factory('configuracion');

        # En caso que el nombre sea un entero, asumo que es ID
        if(Denko::isInt($nombre = trim($nombre)) && $indice1 == null){
            $this->id_configuracion = $nombre;
        }

        # En caso que el nombre sea un entero, asumo que es ID
        else{
            $this->nombre = $nombre;
            if($indice1 != null){
                $this->whereAdd('indice1 '.(($indice1 == 'null') ? ' is NULL' : (' = '.$indice1)));
            }
        }

        # Obtengo la configuraci�n de la DB
        $this->find(true);
        $this->parseMetadata();
    }

    /**
     * Redefino para actualizar la metadata en el DAO
     *
     * @access public
     * @return integer
     */
    public function insert(){
        $this->metadata2json();
        return parent::insert();
    }

    /**
     * Redefino para actualizar la metadata en el DAO
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function update($dataObject = false){
        $this->metadata2json();
        return parent::update();
    }

    /**
     * Redefino para eliminar el archivo adjunto, en caso que lo tenga
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function delete($useWhere = false){
        $this->deleteFile();
        return parent::delete();
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                METADATA                                //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Parseo la metadata
     *
     * @access protected
     * @return void
     */
    protected function parseMetadata(){

        # En caso que est� seteada la metadata
        if(!empty($this->metadata) && $this->metadata != 'null'){

            # Decodifico el JSON de la metadata y lo transformo en arreglo

            $this->parsedMetadata = json_decode($this->metadata,true);
            Denko::arrayUtf8Decode($this->parsedMetadata);
        }
    }

    /**
     * Obtiene una configuraci�n de metadata seteada para el contenido
     *
     * @static
     * @access public
     * @return string
     */
    public function getMetadataValue(){

        # Obtengo el nro de par�metros
        $num_args = func_num_args();

        # En caso que no haya par�metros, no retorno valor alguno (NULL)
        if($num_args == 0){
            return null;
        }

        # En caso excepcional que pidan el nombre, retorno la descripci�n
        # Si no est� seteada la descripci�n, retorno el nombre de la configuraci�n
        if($num_args == 1 && strtolower(trim(func_get_arg(0))) == 'nombre'){
            return !empty($this->descripcion) ? $this->descripcion : $this->nombre;
        }

        # Busco cual deber�a ser el �ndice que debo retornar
        $evalIndex = '';
        for($i = 0 ; $i < $num_args; $i++){
            $evalIndex.='["'.strtolower(trim(func_get_arg($i))).'"]';
        }

        # Evalu� el c�digo, teniendo en cuenta los �ndices del arreglo de la metadata
        eval('$metadata = isset($this->parsedMetadata'.$evalIndex.') ? $this->parsedMetadata'.$evalIndex.' : ( isset(InnyCore_Metadata::$defaultMetadata_Contenido'.$evalIndex.') ? InnyCore_Metadata::$defaultMetadata_Contenido'.$evalIndex.' : null );');
        return $metadata;
    }

    /**
     * Convierte la metadata parseada en notaci�n JSON
     *
     * @access protected
     * @return void
     */
    protected function metadata2json(){

        # Copio la metadata parseada a un arreglo, para no alterarlo
        $parsedMetadata = $this->parsedMetadata;
        Denko::arrayUtf8Encode($parsedMetadata);

        # Convierto la metadata parseada a notaci�n JSON
        $this->metadata = json_encode($parsedMetadata);
    }

	/**
	 * Borra los par�metros pasados como argumentos de la metadata
	 *
	 * @access public
	 * @return void
	 */
    public function unsetMetadata(){

        # En caso que no haya par�metros, no se hace nada.
        if(($num_args = func_num_args()) <= 1){
            return false;
        }

        # Obtengo la porci�n de c�digo correspondiente a los �ndices
        $eval = '';
        for($i = 0 ; $i < func_num_args(); $i++){
            $eval.= "['".func_get_arg($i)."']";
        }

        # Eval�a el c�digo y elimino dicha metadata.
        eval('unset($this->parsedMetadata'.$eval.');');
    }

    /**
     * Verifica si el listado tiene cierto permiso
     *
     * @param string $permiso permiso [ A | B | M | V | O ]
     * @access public
     * @return boolean si el listado tiene cierto permiso
     */
    public function tienePermiso($permiso){
        return InnyCore_Metadata::tienePermiso($this,$permiso);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                MISC                                    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Setea un valor de configuraci�n
     *
     * @access public
     * @return boolean si pudo setear el valor
     */
    public function setConfig(){

        # En caso que no haya par�metros, no retorno valor alguno
        if(($num_args = func_num_args()) <= 1){
            return false;
        }

        # Obtengo la porci�n de c�digo correspondiente a los �ndices
        $eval = '';
        for($i = 0 ; $i < (func_num_args()-1); $i++){
            $eval.= "['".func_get_arg($i)."']";
        }

        # Eval�a el c�digo y asigno la variable
        eval('$config = &$this->parsedMetadata'.$eval.';');
        $config = func_get_arg(func_num_args()-1);

        # Si est� todo bien, retorno TRUE
        return true;
    }

    /**
     * Indica si el contenido tiene seteado valor alguno
     *
     * @access public
     * @return boolean si el contenido tiene seteado valor alguno
     */
    public function hasContent(){
        return !InnyCore_Dao::isEmpty($this->valor);
    }

	/**
	 * Retorna el DAO del sitio al que corresponde el contenido
	 *
	 * @access public
	 * @return DataObjects_Sitio
	 */
	public function getSitio(){

	    # En caso que no est� seteado el ID del Sitio
        if(!$this->indice1 || $this->indice1 == 'null'){
            return null;
        }

        # Retorno el DAO del Sitio
        return InnyCore_Dao::getDao('sitio',$this->indice1);
    }

    /**
     * Elimina el archivo relacionado al contenido, y actualiza la metadata
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function deleteFile(){

        # Verifico que el valor pertenezca a un ID de archivo v�lido
        if(!empty($this->valor) && $this->valor != 'null' && Inny::isSupportedFileType($this->getMetadataValue('tipo')) && Denko::isInt($this->valor)){

            # Elimino el archivo relacionado al contenido
            DFM::delete($this->valor);

            $this->valor = 'null';

            # Elimino la metadata del archivo
            $this->unsetMetadata('parametros','filename');
            $this->unsetMetadata('parametros','mime');

            # Actualizo el DAO y retorno el resultado del update
            return $this->update();

        }

        # En caso que no se haya podido actualizar
        return 0;
    }

    /**
     * Elimina el valor del contenido
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function deleteValue(){

        # Si no tengo contenido que eliminar, retorno que todo est� bien
        if(!$this->hasContent()){
            return 0;
        }

        # En caso que el tipo de contenido sea archivo alguno
        if(Inny::isSupportedFileType($this->getMetadataValue('tipo'))){
            return $this->deleteFile();
        }

        # Seteo el contenido en NULL
        $this->valor = 'null';

        # Actualizo el DAO y retorno el resultado del update
        return $this->update();
    }

    /**
     * Crea un InnyType relacionado al Contenido
     *
     * @access public
     * @return InnyType relacionado al Contenido
     */
    public function createInnyType(){

        # Obtengo el tipo del elemento
        $tipo = $this->getMetadataValue('tipo');

        # Creo el elemento
        $innyType = InnyType::factory($this->getMetadataValue('tipo'));

        # Obtengo la metadata del elemento y se la seteo
        # De esta manera se eliminan par�metros no soportados
        $parametros = array();
        $keys = $innyType->getParamKeys();
        foreach($keys as $key){
            $value = isset($this->parsedMetadata['parametros'][$key]) ? $this->parsedMetadata['parametros'][$key] : $this->getMetadataValue('parametros',$key);
            if($value !== null){
                $parametros[$key] = $value;
            }
        }
        $innyType->setMetadata(array( 'nombre' => $this->getMetadataValue('nombre'), 'tipo' => $tipo, 'parametros' => $parametros ));

        # Retorno el elemento
        return $innyType;
    }

    /**
     * Setea un valor en cach�
     *
     * @param array $path ruta del valor
     * @param mixed $valor valor que se desea setear
     * @access public
     * @return boolean si se pudo cachear el valor
     */
    public function setValorEnCache($path,$valor){
        return InnyCore_Dao::isEmpty($this->id_configuracion) ? false : InnyCore_Dao::setValorEnCache(self::$cache_prefix.$this->id_configuracion,$path,$valor);
    }

    /**
     * Obtiene un valor de la cache
     *
     * @param array $path ruta del valor
     * @access public
     * @return mixed valor cacheado, NULL en caso que no exista el valor en cache
     */
    public function getValorEnCache($path){
        return InnyCore_Dao::isEmpty($this->id_configuracion) ? null : InnyCore_Dao::getValorEnCache(self::$cache_prefix.$this->id_configuracion,$path);
    }

    /**
     * Verifica si un contenido es multilenguaje
     *
     * @access public
     * @return boolean si un contenido es multilenguaje
     */
    public function esMultilenguaje(){
        $daoSitio = $this->getSitio();
        return ($daoSitio->getMetadataValue('multilang','enabled') == '1' && $this->getMetadataValue('parametros','multilang') == 'true');
    }
}

################################################################################