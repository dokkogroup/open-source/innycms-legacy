<?php
/**
 * Table Definition for ctacorriente
 */
require_once 'DB/DataObject.php';

class DataObjects_Ctacorriente extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ctacorriente';                    // table name
    public $id_ctacorriente;                 // int(10)  not_null primary_key unsigned auto_increment
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     *
     *
     */
    public function getSitio(){
    	$daoSitio = DB_DataObject::factory('sitio');
    	$daoSitio->whereAdd('id_ctacorriente = '.$this->id_ctacorriente);
    	return $daoSitio->find(true) ? $daoSitio : null;
    }

    /**
     *
     *
     */
    public function getEmpleado(){
    	$daoEmpleado = DB_DataObject::factory('empleado');
    	$daoEmpleado->whereAdd('id_ctacorriente = '.$this->id_ctacorriente);
    	return $daoEmpleado->find(true) ? $daoEmpleado : null;
    }
}
################################################################################