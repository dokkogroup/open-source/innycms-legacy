<?php
/**
 * Table Definition for databaseVersion
 */
require_once '../DAOs/AudDataObject.php';

class DataObjects_DatabaseVersion extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'databaseversion';                 // table name
    public $version;                         // int(11)  not_null

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
