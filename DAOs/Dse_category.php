<?php

/**
 * Table Definition for category
 */
require_once 'DB/DataObject.php';

class DataObjects_Dse_category extends DB_DataObject {
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'dse_category'; // table name
    public $id_category; // int(10)  not_null primary_key unsigned auto_increment
    public $name; // string(45)  not_null
    public $metadata; // string(128)  not_null
    public $id_parent; // int(10)  unsigned
    public $code; // string(20)  not_null blob
    public $id_view; // int(10)  not_null
    public $index1; // int(10)  not_null
    public $index2; // int(10)  not_null
    public $index3; // int(10)  not_null
    public $level; // int(10)  not_null
    public $docscount; // int(10)  not_null

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    ####################################################################
    /**
     * Verifica si la categor�a es hoja en el �rbol.
     * 
     * @return boolean true en caso de ser hoja. false en caso contrario
     */
    function isLeaf() {
        $category = DB_DataObject :: factory('dse_category');
        $category->id_parent = $this->id_category;
        $result = !$category->find();
        $category->free();
        return $result;
    }
    ####################################################################
    /**
     * Construye el url de la categor�a de acuerdo a la vista.
     */
    function buildCategoryUrl() {
        $querystring = preg_replace('/c' . $this->id_view . '=[0-9]*&|&c' . $this->id_view . '=[0-9]*/', '', $_SERVER['QUERY_STRING']);
        $querystring .= '&c' . $this->id_view . '=' . $this->id_category . '&';
        return basename($_SERVER['PHP_SELF']) . '?' . $querystring;
    }
    ####################################################################
    /**
     * Construye el url de la categor�a para los breadcrums.
     */
    function buildBreadcrumbUrl() {
        $querystring = preg_replace('/c' . $this->id_view . '=[0-9]*&|&c' . $this->id_view . '=[0-9]*/', '', $_SERVER['QUERY_STRING']);
        $querystring .= '&c' . $this->id_view . '=' . $this->id_parent . '&';
        return basename($_SERVER['PHP_SELF']) . '?' . $querystring;
    }
    ####################################################################
    /**
     * Obtiene el padre de una categor�a.
     * @return Dao el Dao de la categor�a padre. null si no existe
     */
    function getParent() {

        $category = DB_DataObject :: factory('dse_category');
        
        # No tiene padre. Esto es para evitar el error de PEAR.
        if (!$this->id_parent) {
        	return null;	
        }        	
        
        if ($category->get($this->id_parent)) {
            return $category;
        }
        $category->free();
        return null;
    }
    ####################################################################
    /**
     * Usada para recuperar el path de una categor�a con la funci�n getPath
     */
    private function _getRecursivePath($category, & $path) {
        array_unshift($path, $category);
        if ($category->id_parent == null) {
            return;
        }
        $categoryParent = $category->getParent();
        $this->_getRecursivePath($categoryParent, $path);
    }
    ####################################################################
    /**
     * Obtiene el path de una categor�a
     * @return array un arreglo de Daos representando el path de la categor�a desde
     *               la ra�z.
     */
    function getPath($includeView = false) {

        $path = array ();
        $this->_getRecursivePath($this, $path);
        
        # Si deseo incluir la vista en el path no hago el shift
        if (!$includeView) {
        	array_shift($path);
        }
        
        return $path;
    }
    ####################################################################
    /**
     * Obtiene una categor�a por c�digo. Funciona como el get (por id).
     **/
    function getByCode($code) {
        $this->code = $code;
        return $this->find(true);
    }
    ####################################################################
    /**
     * Funci�n solo por compatibilidad.
     * Es para tener la misma funcionalidad que document_category debido a 
     * que el query puede hacerse indistintamente sobre cualquiera de las
     * dos tablas.
     */
    function getCategory() {
        return $this;
    }
    ####################################################################
    /**
     * Verifica si la categor�a es vista (ra�z).
     * 
     * @return boolean true si la categor�a es vista. false en otro caso.
     */
    function isView() {
        return !$this->id_parent;
    }
    ####################################################################
    /**
     * Obtiene los hijos de esta categor�a.
     * 
     * @return mixed null si no tiene hijos. El DAO category listo para
     *               hacerle fecth() en caso de �xito.
     */
    function getChildren() {
        $children = DB_DataObject :: factory('dse_category');
        $children->id_parent = $this->id_category;
        $children->id_view = $this->id_view;
        if ($children->find()) {
            return $children;
        } else {
            $children->free();
            return null;
        }
    }
    ####################################################################
    /**
     * Verifica si la categor�a tiene hijos o no.
     * 
     * @return boolean true si la categor�a tiene hijos, false en otro caso.
     */
    function hasChildren() {
        $children = $this->getChildren();
        if ($children === null) {
            return false;
        } else {
            $children->free();
            return true;
        }
    }
    ####################################################################
    /**
     * Elimina todas las relaciones entre documentos y categor�as.
     * 
     * @return boolean true en caso de �xito, false en otro caso.
     */
    function dropRelationships($removeFromDocumentTable = true) {
        $relation = DB_DataObject :: factory('dse_document_category');
        $relation->id_category = $this->id_category;
        $relation->id_parent = $this->id_parent;
        
        $result = true;
        //Itero por todas las relaciones y las borro 1 a 1.
        if ($relation->find()) {
            while ($relation->fetch()) {
                $result = $result & $relation->deleteNotRecursive($removeFromDocumentTable);
            }
        }
        $relation->free();
        return $result;
    }
    ####################################################################
    /**
     * Borra esta categor�a.
     * Se encarga de eliminar todas las relaciones con documentos, y de
     * eliminar del campo category_names y category_ids de la tabla de
     * documentos el nombre de esta categor�a y el id.
     * Comportamiento:
     *  -   Si la categor�a es hoja. se borra.
     *  -   Si la categor�a est� en el medio del path (no es hoja ni ra�z)
     *      se borra y todos sus hijos ser�n hijos de su padre.
     *  -   Si la categor�a es ra�z (vista) no la borra, a menos que no
     *      tenga categor�as hijas.
     * 
     * NOTA: El par�metro useWhere ser� ignorado. Solo se puede borrar esta instancia.
     * 
     * @return mixed false si la categor�a no pudo ser borrada. Cantidad de tuplas
     *               afectadas en caso de �xito.
     */
    function delete($useWhere = false) {
        
        if ($this->isView()) {
            //Si es vista, verifico si tiene hijos.
            if (!$this->hasChildren()) {
                return $this->dropRelationships() && parent :: delete();
            } else {
                //No se puede borrar la vista si no tiene hijos
                return false;
            }
        } elseif ($this->isLeaf()) {
            //Si es hoja, primero borro las relaciones, y despues
            //borro la categor�a.
            return $this->dropRelationships() && parent :: delete();
        } else {
            //La categor�a est� en medio del path.
            //1.    Elimino las relaciones de la categor�a.
            $relationshipsResult = $this->dropRelationships();
            // 2.   Hago que todos los hijos sean hijos del padre.
            //      Para eso debo actualizar todos los hijos, y sus relaciones.
            $children = $this->getChildren();
            if ($children) {
                while ($children->fetch()) {
                    // 2.1  Hago que todas las relaciones de las categor�as hijas apunten
                    //      al padre de esta categor�a.
                    $relation = DB_DataObject :: factory('dse_document_category');
                    $relation->id_category = $children->id_category;
                    $relation->id_parent = $children->id_parent;
                    if ($relation->find()) {
                        while ($relation->fetch()) {
                            $relation->id_parent = $this->id_parent;
                            $relation->update();
                        }
                    }
                    // 2.2  Hago que la categor�a hija apunte al padre de esta categor�a.
                    $children->id_parent = $this->id_parent;
                    $children->level = $this->level - 1;
                    $children->update();
                    
                    $relation->free();
                    
                }
                $children->free();
                //Actualizados los hijos.
            }
            //3. Elimino la categor�a.
            return $relationshipsResult && parent :: delete();
        }
    }
    ####################################################################
    /**
     * Borra esta categor�a, y todos sus descendientes, es decir borra
     * todo el sub �rbol que se encuentra debajo de esta categor�a.
     * 
     * Si la categor�a no tiene descendientes, funciona igual que delete.
     * 
     * NOTA: Lo hace de manera recusrsiva.
     */
     function deleteSubTree() {
        
        $children = $this->getChildren();
        
        if ($children) {
            // Llamo recursivamente por cada hijo.
            while ($children->fetch()) {
                $children->deleteSubTree();
            }
            $children->free();
        }
        
        
        // Borro esta categor�a.
        return $this->delete();
     }
     ####################################################################
     /**
      * Obtiene la categor�a vista dado el id_view.
      * Este m�todo puede ser llamado est�ticamente.
      */
      function getView($id_view) {
          $category = DB_DataObject :: factory('dse_category');
          $category->id_view = $id_view;
          $category->whereAdd('id_parent is null');
          if ($category->find(true)) {
              return $category;
          } else {
              $category->free();
              return null;
          }
      }
      ####################################################################
     /**
      * Insert overloaded.
      */
      function insert() {
          $category = DB_DataObject :: factory('dse_category');
          
          if ($category->get($this->id_parent)) {
              $this->level = $category->level + 1;
              $this->id_view = $category->id_view;
          } else {
              $this->level = 0;
          }
          $category->free();
          
          return parent :: insert();
      }
      ####################################################################
     /**
      * Update Overloaded.
      * Me fijo si cambi� el id_parent; si es as�, cambio las referencias
      * en la tabla dse_document_category.
      */
      function update($dataObject = false) {
          
        $_self = DB_DataObject :: factory('dse_category');
        //Si cambi� el padre, actualizo las referencias y el nivel.
        if ($_self->get($this->id_category) && $_self->id_parent !== $this->id_parent) {
            
            //Obtengo todas las referencias de la tabla dse_document_category
            $relation = DB_DataObject :: factory('dse_document_category');
            $relation->id_category = $_self->id_category;
            $relation->id_parent = $_self->id_parent;
            if ($relation->find()) {
                //Por cada relaci�n les asigno el nuevo id_parent
                while ($relation->fetch()) {
                    $relation->id_parent = $this->id_parent;
                    $relation->update();
                }
            }
        } else {
            //MMM algo anda muuuyyy mal.
        }
        $_self->free();
        
        if ($this->id_parent) {
	        $parent = DB_DataObject :: factory('dse_category');
	        if ($parent->get($this->id_parent)) {
	           $this->level = $parent->level + 1;  
	        }
	        $parent->free();
        }
          
        return parent :: update($dataObject);
      }
      
      function updateCount(){
          $c=DB_DataObject::factory('dse_document_category');
		  $c->id_category=$this->id_category;
		  $count=$c->count();
		  if($count != $this->docscount ){
		      $this->docscount=$count;
		      $this->update();
		  }
      }
}