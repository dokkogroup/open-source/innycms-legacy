<?php
/**
 * Table Definition for document
 */

require_once '../DAOs/AudDataObject.php';

class DataObjects_Dse_document extends AudDataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'dse_document';                        // table name
    public $id_document;                     // int(10)  not_null primary_key unsigned auto_increment
    
    /*ESTOS CAMPOS SON PROTECTED PORQUE NO DEBEN PODER SER ACCEDIDOS DESDE AFUERA*/
    protected $field_0;                         // blob(255)  multiple_key blob
    protected $field_1;                         // blob(255)  multiple_key blob
    protected $field_2;
    
    public $d_field_0;                         // blob(255)  multiple_key blob
    public $d_field_1;                         // blob(255)  multiple_key blob
    public $d_field_2;                          // blob(65535)  multiple_key blob
    public $so_field_0;                     // int(10)  not_null
    public $so_field_1;                     // int(10)  not_null
    public $category_names;             // blob(65535)  multiple_key blob
    public $category_ids;                      // blob(65535)  multiple_key blob

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
    /**
     * @access private
     * Solo puede ser accedido por el método doNotInsert().
     * Si este método es llamado antes de efectuar un insert o un update
     * los campos de mappings serán ignorados. Ningún cambio se hará en
     * los search fields.
     */
    private $DSE_INDEX_DATA = true;
    
    /**
     * Obtiene todas las categorías a las que pertenece un documento.
     * @return Dao retorna el dao 'document_category' listo para ser usado con fetch.
     *         En caso de no tener categorías retorna null.
     */
     function getCategories() {
         
         $id_document = DSE_DOCUMENT_TABLEID;
         
         $c = DB_DataObject :: factory('dse_category');
         $c->joinAdd();
         $dc = DB_DataObject :: factory('dse_document_category');
         $dc->id_document = $this->$id_document;
         $c->joinAdd($dc);
         if (!$c->find()) {
           $c->free();
           return null;  
         } 
         return $c;
     }
     /**
      * Obtiene todas las categorías hijas a las que pretenece este documento.
      */
     function getLeafCategories($fromView = false, $fromIndex1 = false) {

        if(!defined(DSE_DOCUMENT_TABLEID)) return null;
        $id_document = DSE_DOCUMENT_TABLEID;

        $c = DB_DataObject :: factory('dse_category');
        $dc = DB_DataObject :: factory('dse_document_category');
        $dc->id_document = $this->$id_document;
        $c->joinAdd($dc);
        
         if ($fromView !== false) {
            $c->id_view = $fromView;
        }
        
        if ($fromIndex1 !== false) {
            $c->index1 = $fromIndex1;
        }
        
        if (dse_useInny()) {
        	$c->index1 = Inny :: getIdSitioActual();
        }
        
        $category = DB_DataObject :: factory('dse_category');
        $category->whereAdd();
        
        $leafs = 0;
        
        if ($c->find()) {
            while($c->fetch()) {
                # Cuento la cantidad de hojas encontradas y lo agrego al query
                # por ID.
                if ($c->isLeaf()) {
                    $leafs++;
                    $category->whereAdd('id_category=' . $c->id_category,'OR');       
                }
            }
            
            # Si no encontré categorías hojas para este documento, retorno null.
            if ($leafs === 0) {
                return null;
            }
            
            $category->orderBy('id_view ASC');
             
            return $category;
        } else {
            return null;
        }
     }
     
     function getAddedCategories($fromView = false) {

        if(!defined(DSE_DOCUMENT_TABLEID)) return null;
        $id_document = DSE_DOCUMENT_TABLEID;

        $c = DB_DataObject :: factory('dse_category');
        $dc = DB_DataObject :: factory('dse_document_category');
        $dc->id_document = $this->$id_document;
        $dc->added = 1;
        $c->joinAdd($dc);
        
         if ($fromView !== false) {
            $c->id_view = $fromView;
        }
        
        if (dse_useInny()) {
        	$c->index1 = Inny :: getLoggedIdSitio();
        }
        
        $category = DB_DataObject :: factory('dse_category');
        $category->whereAdd();
        
        if ($c->find()) {
            while($c->fetch()) {
                # Cuento la cantidad de hojas encontradas y lo agrego al query
                # por ID.
				$category->whereAdd('id_category=' . $c->id_category,'OR');
            }
            $category->orderBy('id_view ASC');
            return $category;
        } else {
            return null;
        }
     }
     
    /**
     * Agrega el documento a la categoría.
     * @param int $id_category el id de la categoría a la que se quiere agregar el
     *                         documento.
     * @return mixed El id de la relación en caso de éxito. false en otro caso.
     */
     function addCategory($id_category) {
        $category = DB_DataObject :: factory('dse_category');
        if ($category->get($id_category)) {
            $id_document = DSE_DOCUMENT_TABLEID;
            $relation = DB_DataObject :: factory('dse_document_category');
            $relation->id_document = $this->$id_document;
            $relation->id_category = $category->id_category;
            $relation->id_parent = $category->id_parent;
            $relation->index1 = $category->index1;
            $relation->index2 = $category->index2;
            $relation->index3 = $category->index3;
            $relation->added = 1;
            $result = $relation->insert();
            $relation->free();
            $category->free();
            if (dse_checkDirective('__DSE_DISABLE_ADDCATEGORY_AUTOUPDATE')) {
	        	$this->category_ids .= ' ' . $result . ' ';
	    	}
            return $result;
        }
        $category->free();
        return false;
    }
    /**
     * Remueve el documento de la categoría.
     * @param int $id_category el id de la categoría de la que se quiere remover el
     *                         documento.
     * @return mixed true en caso de exito, false en otro caso.
     */
     function removeCategory($id_category) {
        
        $category = DB_DataObject :: factory('dse_category');
        if ($category->get($id_category)) {
            //Lo borro de la NxN
            $id_document = DSE_DOCUMENT_TABLEID;
            $relation = DB_DataObject :: factory('dse_document_category');
            $relation->id_document = $this->$id_document;
            $relation->id_category = $category->id_category;
            $relation->id_parent = $category->id_parent;
            $relation->index1 = $category->index1;
            $relation->index2 = $category->index2;
            $relation->index3 = $category->index3;
            //Steo el flag a 0 y actualizo por si fue agregada 
            // a mitad del path
            if ($relation->find(true)) {
            	$relation->added = 0;
            	$relation->update();
            }
            $result = $relation->delete();
            $relation->free();
            $category->free();
            return $result;
        }
        $category->free();
        return false;
    }
    /**
     * Insert extended.
     * Stemmiza el valor de cada campo en los mappings, y lo cola en su
     * correspondiente campo buscable.
     */
    function insert() {
        
        if ($this->DSE_INDEX_DATA) {
            global $DSE_DOCUMENT_FIELD_MAPPINGS;
            dse_includeStemms();
            foreach ($DSE_DOCUMENT_FIELD_MAPPINGS as $sfield => $dfield) {
                $this->$sfield = Stemmer :: stemm($this->$dfield);
            }
        }
        
        return parent :: insert();
    }
    /**
     * Update extended.
     * Stemmiza el valor de cada campo en los mappings, y lo cola en su
     * correspondiente campo buscable.
     */
    function update($dataObject = false) {
        
        if ($this->DSE_INDEX_DATA) {
            global $DSE_DOCUMENT_FIELD_MAPPINGS;
            dse_includeStemms();
            
            // Itero sobre todos los mappings.
            foreach ($DSE_DOCUMENT_FIELD_MAPPINGS as $sfield => $dfield) {
                if (!is_bool($dataObject)) {
                    //Si no es un booleano, supongo que es un DataObject
                    if (isset($dataObject->$dfield) && $this->$dfield !== $dataObject->$dfield) {
                        //Actualizo el campo solo si cambió de valor
                        $this->$sfield = Stemmer :: stemm($this->$dfield);
                    }
                } else {
                    //Actualizo el campo.
                    $stemmedWord = Stemmer :: stemm($this->$dfield);
                    //Si es el string vacío, le pongo un espacio para que el update
                    //actualice en caso de que el campo tuviera valor y se desea asignarle
                    //vacío.
                    if (!$stemmedWord) {
                        $stemmedWord = ' ';
                    }
                    $this->$sfield = $stemmedWord;
                }
            }
         }
         
        return parent :: update($dataObject);
    }
    
    /**
     * Borra el documento y sus relaciones con las categorías.
     */
    function delete($useWhere = false) {
        //Obtengo todas las categorías hojas.
        //Elimino el documento desde ahí ya que el delete de dse_document_category
        //se encarga de borrar para arriba.
        $category = $this->getLeafCategories();
        
        if ( $category !== null ) {
            $id_document = DSE_DOCUMENT_TABLEID;
            while ($category->fetch()) {
                $relation = DB_DataObject :: factory(dse_document_category);
                $relation->id_category = $category->id_category;
                $relation->id_document = $this->$id_document;
                $relation->id_parent = $category->id_parent;
                //Llamo con false, así no pierde tiempo en actualizar los campos category_names
                //y category_ids ya que de cualquier modo lo voy a borrar.
                $relation->delete(false);
                $relation->free();
            }
        }
        //Borro el documento.
        return parent :: delete($useWhere);
        
    }
    /**
     * Le dice a esta instancia del DAO que no indexe este documento al
     * ser insertado.
     * 
     * @return void
     */
    function doNotIndex() {
        $this->DSE_INDEX_DATA = false;
    }
    
    /**
     * Reindexa un documento.
     * Actualiza los campos buscables de las categorías (categor_ynames y
     * category_ids)y los campos buscables (field_0,field_1 y field_2).
     * @return datatype description
     */
    function reindex() {
        
        if ($this->DSE_INDEX_DATA) {
            dse_includeStemms();
            //Obtengo todas las categorías a las que pertenece este documento.
            $category = $this->getCategories();
            
            //Vacío los campos buscables de categorías
            $this->category_names = '';
            $this->category_ids = '';
            
            if ($category !== null) {
                while ($category->fetch()) {
                    //Agrego el id
                    $this->category_ids .= ' ' .  DSE_CATEGORY_PREFIX . $category->id_category;
                    //Si no es raíz (vista) agrego el nombre stemmizado
                    if ($category->id_parent != null) {
                        $stemedWords = Stemmer :: stemm($category->name);
                        $this->category_names .= ' ' .  $stemedWords . ' ';
                    }
                }
                $category->free();
            }
            
            return $this->update();
        }
    }

}
