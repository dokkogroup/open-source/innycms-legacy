<?php
/**
 * Table Definition for document_category
 */
require_once 'DB/DataObject.php';

class DataObjects_Dse_document_category extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'dse_document_category';  // table name
    public $id_document_category;               // int(10)  not_null primary_key unsigned auto_increment
    public $id_category;                        // int(10)  not_null multiple_key unsigned
    public $id_document;                        // int(10)  not_null multiple_key unsigned
    public $id_parent;                          // int(10)  not_null multiple_key unsigned
    public $refs;                               // int(10)  not_null multiple_key unsigned
    public $added;								// int(10)  not_null multiple_key unsigned
    public $index1;                               // int(10)  not_null multiple_key unsigned
    public $index2;                               // int(10)  not_null multiple_key unsigned
    public $index3;                               // int(10)  not_null multiple_key unsigned
    
    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
    
    /**
     * Insert Overload.
     * Inserta un documento en todo el path.
     * 
     * IMPORTANTE:  Es recursivo hacia arriba, es decir, inserta al documento en la
     *              categor�a especificada y en todos sus predecesores.
     *              
     */
    function insert() {
        
        $category = DB_DataObject :: factory('dse_category');
        
        $catIds='';
        
        //Subo recursivamente hasta la ra�z para insertar el documento en 
        //todo el path 
        if ($category->get($this->id_category) && $category->id_parent != null) {
            $document_category = DB_DataObject :: factory('dse_document_category');
            $document_category->id_document = $this->id_document;
            $document_category->id_category = $category->id_parent;
            $document_category->index1 = $category->index1;
            $document_category->index2 = $category->index2;
            $document_category->index3 = $category->index3;
            
            $parent = DB_DataObject :: factory('dse_category');
            $parent->get($category->id_parent);
            $document_category->id_parent = $parent->id_parent; 
            $parent->free();
            
            $catIds = $document_category->insert();
            $document_category->free();
        }
        
        if (dse_checkDirective('__DSE_DISABLE_ADDCATEGORY_TESTDOUBLEINSERTION')) {
        	parent :: insert();
        } else {
        	
	        //Inserto la categor�a actual, solo si el documento no pertenece ya a la categor�a.
	        if (!$this->find(true)) {
	            
	            $result = parent :: insert();
	            
	            if (!dse_checkDirective('__DSE_DISABLE_ADDCATEGORY_UPDATECATEGORYCOUNTS')) {
	            	//Actualizo la cuenta de documentos que pertenecen a esta categoria
	        		$category->docscount += 1;
	        		$category->update();
	            }
	            
	        } else {
	            //Si el documento pertenece a la categor�a, entonces, esta tupla ya existe en
	            //la base de datos, por lo que se actualiza el contador de referencias.
	            $this->refs += 1;
	            $result =  $this->update();
	        }
        }
        
        $category->free();
        
        //Agrego la categor�a en el campo categories de la tabla document y tb
        //el campo field_categorynames 
        if (!dse_checkDirective('__DSE_DISABLE_ADDCATEGORY_AUTOUPDATE')) {
	        $document = DB_DataObject :: factory(DSE_DOCUMENT_TABLENAME);
	        if ($document->get($this->id_document)) {
	            if (strstr($document->category_ids,' ' . DSE_CATEGORY_PREFIX . $this->id_category.' ') === false) {
	                $document->category_ids .= ' ' . DSE_CATEGORY_PREFIX . $this->id_category . ' ';
	                    
	                $category = DB_DataObject :: factory('dse_category');
	                $category->get($this->id_category);
	                //Agrego el nombre de la categor�a al campo solo si no es ra�z (nombre de vista).
	                dse_includeStemms();
	                    
	                $stemedWords = Stemmer :: stemm($category->name);
	                if ($category->id_parent != null) {
	                    $document->category_names .= ' ' . $stemedWords . ' ';
	                }
	                $category->free();
	                
	                $document->update();
	            }
	        }
	        $document->free();
        }
        
        if($catIds != ''){
	    	$catIds.=' ';
		}
        
        return $catIds.DSE_CATEGORY_PREFIX . $this->id_category;
        
    }
    
    /**
     * Borra un documento de todo el path.
     * IMPORTANTE: Es recursivo hacia arriba, es decir, borra todas las relaciones de la
     *             categor�a especificada, y de sus predecesores.
     *             Si se quiere borrar solo esta instancia usar deleteNotRecursive.
     * 
     * @param boolean $removeFromDocumentTable Decide si elimina el nombre de las categor�as
     *                                          del campo category_names y los ids de las 
     *                                          categor�as del campo category_ids en el path.
     *                                          Por defecto, true.
     */
    function delete($removeFromDocumentTable=true) {
        $category = DB_DataObject :: factory('dse_category');
        
        //Subo recursivamente hasta la ra�z para insertar el documento en 
        //todo el path 
        if ($category->get($this->id_category) && $category->id_parent != null) {
            $document_category = DB_DataObject :: factory('dse_document_category');
            $document_category->id_document = $this->id_document;
            $document_category->id_category = $category->id_parent;
            $document_category->index1 = $category->index1;
            $document_category->index2 = $category->index2;
            $document_category->index3 = $category->index3;
            
            $parent = DB_DataObject :: factory('dse_category');
            $parent->get($category->id_parent);
            $document_category->id_parent = $parent->id_parent; 
            $parent->free();
            
            $document_category->delete();
            $document_category->free();
        }
        
        //Remuevo la categor�a del campo categories de la tabla document y tb
        //el campo field_categorynames si el flag $removeFromDocumentTable es true.
        if ($removeFromDocumentTable) {
            $document = DB_DataObject :: factory(DSE_DOCUMENT_TABLENAME);
            if ($document->get($this->id_document)) {
                
                $patterns = array(   '/^'.preg_quote(DSE_CATEGORY_PREFIX . $this->id_category, '/').' /',
                                    '/ '.preg_quote(DSE_CATEGORY_PREFIX . $this->id_category, '/').' /',
                                    '/ '.preg_quote(DSE_CATEGORY_PREFIX . $this->id_category, '/').'$/');
                $replacements = array(' ' ,' ',' ');
                $document->category_ids = preg_replace($patterns,$replacements,$document->category_ids);
                    
                $category = DB_DataObject :: factory('dse_category');
                $category->get($this->id_category);
                    
                //Remuevo el nombre de la categor�a del campo solo si no es ra�z (nombre de vista).
                dse_includeStemms();
                $stemedWords = Stemmer :: stemm($category->name);
                if ($category->id_parent != null) {
                    $patterns = array(  '/^'.preg_quote($stemedWords, '/').' /',
                                        '/ '.preg_quote($stemedWords, '/').' /',
                                        '/ '.preg_quote($stemedWords, '/').'$/');
                    $replacements = array(' ' ,' ',' ');
                    $document->category_names = preg_replace($patterns,$replacements,$document->category_names);
                }
                $category->free();
                    
                $document->update();
            }
            $document->free();
        }
        
        //Si el contador de referencias es mayor que uno, significa que el documento est�
        //en alguna subcategor�a, en lugar de borrarlo, actualizo el contador de
        //referencias.
        $this->find(true);
        if ($this->refs > 1) {
            $this->refs -= 1;
            $result = $this->update();
        } else {
            //Actualizo la cuenta de documentos que pertenecen a esta categoria
	        $category->docscount -= 1;
    	    $category->update();
        
            //Es la �nica referencia, la borro.
            $result = parent :: delete();    
        }
        
        $category->free();
        
        return $result;
        
    }
    
    /**
     * Borra solo esta relacion entre la categoria y el documento.
     * 
     * @param boolean $removeFromDocumentTable Decide si elimina el nombre de las categor�as
     *                                          del campo category_names y los ids de las 
     *                                          categor�as del campo category_ids en el path.
     *                                          Por defecto, true.
     * NOTA: Este m�todo no tiene en cuenta el contador de referencias, por lo que
     *       si hay documentos que est�n en m�s de una categor�a hoja de la misma
     *       vista, se borrar�n TODAS.
     */
    function deleteNotRecursive($removeFromDocumentTable=true) {
        
        //Remuevo la categor�a del campo categories de la tabla document y tb
        //el campo field_categorynames si el flag $removeFromDocumentTable es true.
        if ($removeFromDocumentTable) {
            $document = DB_DataObject :: factory(DSE_DOCUMENT_TABLENAME);
            if ($document->get($this->id_document)) {
                
                $patterns = array(   '/^'.preg_quote(DSE_CATEGORY_PREFIX . $this->id_category, '/').' /',
                                    '/ '.preg_quote(DSE_CATEGORY_PREFIX . $this->id_category, '/').' /',
                                    '/ '.preg_quote(DSE_CATEGORY_PREFIX . $this->id_category, '/').'$/');
                $replacements = array(' ' ,' ',' ');
                $document->category_ids = preg_replace($patterns,$replacements,$document->category_ids);
                 
                $category = DB_DataObject :: factory('dse_category');
                $category->get($this->id_category);
                    
                //Remuevo el nombre de la categor�a del campo solo si no es ra�z (nombre de vista).
                dse_includeStemms();
                $stemedWords = Stemmer :: stemm($category->name);
                 
                if ($category->id_parent != null) {
                    $patterns = array(  '/^'.preg_quote($stemedWords, '/').' /',
                                        '/ '.preg_quote($stemedWords, '/').' /',
                                        '/ '.preg_quote($stemedWords, '/').'$/');
                    $replacements = array(' ' ,' ',' ');
                    $document->category_names = preg_replace($patterns,$replacements,$document->category_names);
                }
                
                $document->update();
                
                //Actualizo la cuenta de documentos que pertenecen a esta categoria
        		$category->docscount -= 1;
        		$category->update();
                
                $category->free();
                
            }
            $document->free();
        }
        $result = parent :: delete();
        return $result;
    }
    
    /**
     * Construye el url de la categor�a de acuerdo a la vista.
     */
     function buildCategoryUrl() {
        $category = DB_DataObject :: factory('dse_category');
        if ($category->get($this->id_category)) {
            return $category->buildCategoryUrl();
        } else {
            return '';
        }
     }
     
     function getCategory() {
         $category = DB_DataObject :: factory('dse_category');
         $category->get($this->id_category);
         return $category;
     }
}
