<?php
/**
 * Table Definition for document
 */
require_once 'DB/DataObject.php';

class DataObjects_Dse_synonim extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'dse_synonim';        // table name
    public $id_synonim;                     // int(10)  not_null primary_key unsigned auto_increment
    public $word;                           // string(20)
    public $synonims;                       // string(100)
    public $is_synonim_of;                  // int(10)
    
    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
    
    /**
     * Borra un sin�nimo pero con l�gica agregada.
     * Si, el que se borra es root y no tiene hijos: se borra y listo.
     * Si el que se borra es root, y tiene hijos: se escoge un hijo,
     * se lo hace root y se borra el root viejo.
     * Si el que se borra no es root, se elmina del root y la tupla.
     */
    function delete($deleteAll=false) {
        
        if (!$deleteAll) {
            $patterns = array('/,' . $this->word . ',/','/^' . $this->word . ',/','/,' . $this->word . '$/');
            $replacements = array(',','','');
            
            if (!$this->isRoot()) {
               $root = $this->getRoot();
               $root->synonims = preg_replace($patterns,$replacements,$root->synonims,1);
               $root->update(); 
            } elseif($children = $this->getChildren()) {
                $children->fetch();
                $newRoot = clone $children; 
                
                while ($children->fetch()) {
                    $children->is_synonim_of = $newRoot->id_synonim;
                    $children->update();    
                }
                
                $newRoot->synonims = preg_replace($patterns,$replacements,$this->synonims,1);
                $newRoot->is_synonim_of = '';
                $newRoot->update();
            }
        } else {
            $children = $this->getChildren();
            while ($children->fetch()) {
                $children->delete();    
            }
        }
        return parent :: delete();
    }
    
    function getSynonims($removeMyself=false) {
        $synonim = null;
        
        if (!$this->isRoot()) {
            $synonim = $this->getRoot();
            if ($synonim !== null) {
                if ($removeMyself){
                    $synonim->synonims = $this->removeMyself($synonim->synonims);
                }
                return $synonim->synonims;    
            } else {
                return $this->word;
            }
        } else {
            if ($removeMyself){
                $this->synonims = $this->removeMyself($this->synonims);
            }
            return $this->synonims;
        }
    }
    
    function isRoot() {
        return !$this->is_synonim_of;
    }
    
    function getRoot() {
        if ($this->isRoot()) {
            return $this;
        }
        $root = DB_DataObject :: factory('dse_synonim');
        if ($root->get($this->is_synonim_of)) {
            return $root;
        } else {
            return null;
        }
    }
    
    private function removeMyself($where) {
        $patterns = array('/,' . $this->word . ',/','/^' . $this->word . ',/','/,' . $this->word . '$/');
        $replacements = array(',','','');
        return preg_replace($patterns,$replacements,$where,1);
    }
    
    function getChildren() {
        $children = DB_DataObject :: factory('dse_synonim');
        $children->is_synonim_of = $this->id_synonim;
        if ($children->find()) {
            return $children;
        } else {
            return false;
        }
    }
        /**
     * Obtiene los sin�nimos de una frase stemmizada.
     * Esta funci�n puede ser llamada est�ticamente.
     */
    function synonimize($query) {
        
        $modifiedWords = '';
        $words = explode (' ',$query);
        foreach ($words as $word) {
            if (strlen($word) > 1) {
                $synonim = DB_DataObject :: factory('dse_synonim');
                $synonim->word = $word;
                if ($synonim->find(true)) {
                    $modifiedWords .= str_replace(',',' ',$synonim->getSynonims()) . ' ';
                } else {
                    $modifiedWords .= $word . ' ';
                }
            }
        }
        
        if (strpos($query,"\"") === FALSE) {
            return $modifiedWords;    
        } else {
            return ' " ' . $modifiedWords . ' " ';
        }
    }

}
