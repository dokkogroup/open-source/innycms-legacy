<?php
/**
 * Table Definition for empleado
 */
require_once '../DAOs/AudDataObject.php';

define('EMPLEADO_HABILITADO',1);
define('EMPLEADO_DESHABILITADO',0);

class DataObjects_Empleado extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'empleado';                        // table name
    public $id_empleado;                     // int(10)  not_null primary_key unsigned auto_increment
    public $id_ctacorriente;                 // int(10)  not_null multiple_key unsigned
    public $username;                        // string(100)  not_null
    public $password;                        // string(100)  not_null
    public $nombre;                          // string(100)  not_null
    public $apellido;                        // string(100)  not_null
    public $email;                           // string(100)  not_null
    public $telefono;                        // string(100)  not_null
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary
    public $habilitado;                      // int(1)  not_null

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     *
     *
     */
	public function insert(){
		$daoEmpleado = DB_DataObject::factory('empleado');
		$daoEmpleado->whereAdd('username = '."'".$this->username."'");
		$daoEmpleado->find(true);
        $this->habilitado=1;
		if(empty($daoEmpleado->username)){
			$dao_ctacorriente = DB_DataObject::factory('ctacorriente');
			$id_ctacorriente = $dao_ctacorriente->insert();
			$this->id_ctacorriente=$id_ctacorriente;
			return parent::insert();
		}else
			return -1;

	}
}
################################################################################