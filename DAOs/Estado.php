<?php
/**
 * Table Definition for estado
 */
require_once 'DB/DataObject.php';

/**
 * Constantes de las PK
 */
define('ESTADO_NOCOMENZADO',1);
define('ESTADO_ENPROCESO',2);
define('ESTADO_FINALIZADO',3);
define('ESTADO_ENPAUSA',4);
define('ESTADO_ANULADO',5);

class DataObjects_Estado extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'estado';                          // table name
    public $id_estado;                       // int(10)  not_null primary_key unsigned auto_increment
    public $nombre;                          // string(100)  not_null
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
################################################################################