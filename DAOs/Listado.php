<?php
require_once '../DAOs/AudDataObject.php';
require_once '../commons/inny/core.metadata.php';
require_once '../commons/inny/core.type.php';

/**
 * Table Definition for listado
 */
class DataObjects_Listado extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'listado';                         // table name
    public $id_listado;                      // int(10)  not_null primary_key unsigned auto_increment
    public $nombre;                          // string(100)  not_null
    public $id_sitio;                        // int(10)  not_null multiple_key unsigned
    public $metadata;                        // blob(65535)  blob
    public $filtro;                          // blob(65535)  blob
    public $admin_only;                      // string(1)  not_null
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     * @var array metadata parseada
     * @access public
     */
    public $parsedMetadata;

    /**
     * @var integer cantidad de archivos fijos
     * @access public
     */
    public $cantArchivosFijos;

    /**
     * @var array cache del listado
     * @access public
     */
    public $cache;

    /**
     * @var string prefijo de la cache
     * @access public
     * @static
     */
    public static $cache_prefix = 'INNY_CACHE_LISTADO_';

    /**
     * fetch
     * Se redefine para parsear la metadata
     *
     * @access public
     * @return boolean TRUE on success and FALSE on failure
     */
	public function fetch(){
	    $dao = parent::fetch();
	    if($dao){
	       $this->_parseMetadata();
	       $this->cache = array();
        }
        return $dao;
	}

	/**
	 * delete
	 * Se redefine para primero vaciar el listado antes de eliminarlo
	 *
	 * @access public
	 * @return mixed number of rows affected or FALSE on failure
	 */
	public function delete($useWhere = false){
	   $this->vaciar();
	   return parent::delete();
	}

	/**
	 * Retorna el DAO sitio al cual pertenece este listado
	 *
	 * @access public
	 * @return DataObjects_Sitio DAO Sitio
	 */
	public function getSitio(){
        return InnyCore_Dao::getDao('sitio',$this->id_sitio);
	}

	/**
	 * Obtiene los productos relacionados al listado
	 *
	 * @access public
	 * @return DataObjects_Producto o NULL en caso que no tenga relacionado producto alguno
	 */
	public function getProductos(){
        $daoProducto = DB_DataObject::factory('producto');
        $daoProducto->id_listado = $this->id_listado;
        return ($daoProducto->find()? $daoProducto : null);
	}

	/**
	 * Obtiene el DAO Producto, verificando que pertenezca al este DAO Listado
	 *
	 * @param integer $id_producto ID del DAO Producto
	 * @param array $debug_backtrace debug_backtrace de la funci�n que invoca
	 * @access public
	 * @return DataObjects_Producto si existe y pertenene al listado, NULL en caso contrario
	 */
	public function getProducto($id_producto,$debug_backtrace=null){

        # Obtengo el debug_backtrace
        if($debug_backtrace === null){
            $debug_backtrace = debug_backtrace();
        }

        # Verifico que este listado exista en la DB
        if(!InnyCore_Dao::isId($this->id_listado)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El ID del listado es nulo',$debug_backtrace);
            return null;
        }

        # Verifico que el producto exista en la DB
        $daoProducto = InnyCore_Dao::getDao('producto',$id_producto);
        if(empty($daoProducto)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El Producto con ID "'.$id_producto.'" no existe',$debug_backtrace);
            return null;
        }

        # Verifico que el producto pertenezca al listado
        if($daoProducto->id_listado != $this->id_listado){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El Producto ID "'.$id_producto.'" no pertenece al Listado "'.$this->nombre.'" (ID "'.$this->id_listado.'")',$debug_backtrace);
            return null;
        }

        # Retorno el DAO Producto
        return $daoProducto;
    }

    /**
     *
     *
     */
    public function getProductoPorPosicion($posicion){
        $daoProducto = DB_DataObject::factory('producto');
        $daoProducto->whereAdd('
            id_listado = '.$this->id_listado.' and posicion = '.$posicion.'
        ');
        return $daoProducto->find(true) ? $daoProducto : null;
    }

	/**
	 * Vac�a el listado, eliminando todos sus productos
	 *
	 * @access public
	 * @return void
	 */
	public function vaciar(){

	# Seteo tiempo ilimitado en la ejecucion del script
	set_time_limit(0);

	$daoProducto = $this->getProductos();
        if($daoProducto){
            while($daoProducto->fetch()){
                $daoProducto->forzarDelete();
            }
        }
	}

    /**
     * Retorna la cantidad de archivos fijos que tiene el listado
     *
     * @access public
     * @return integer
     */
    public function getCantidadArchivosFijos(){
        return $this->cantArchivosFijos;
    }

    /**
     * Crea un tipo de datos correspondiente a la metadata del listado
     *
     * @param string $modo tipo de contenido [texto|archivo]
     * @param integer $numero numero del contenido [1..10 en caso de texto|1.. n en caso de archivo]
     * @access public
     * @return InnyType
     */
    public function createInnyType($modo,$numero,$params=array()){
        # Obtengo el tipo del elemento
        if(!empty($params['type']) && Inny::isSupportedType(strtolower(trim($params['type'])))){
            $tipo = strtolower(trim($params['type']));
        }
        else{
            $tipo = isset($this->parsedMetadata[$modo][$modo.$numero]['tipo']) ? $this->parsedMetadata[$modo][$modo.$numero]['tipo'] : $this->getMetadataValue($modo,'tipo');
        }

		# Obtengo el nombre del elemento
        $nombre = isset($this->parsedMetadata[$modo][$modo.$numero]['nombre']) ? $this->parsedMetadata[$modo][$modo.$numero]['nombre'] : $this->getMetadataValue($modo,'nombre');
        
        $ayuda_gral=$this->getMetadataValue($modo,'ayuda');
        
        $ayuda = isset($this->parsedMetadata[$modo][$modo.$numero]['ayuda']) ? $this->parsedMetadata[$modo][$modo.$numero]['ayuda'] : (!empty($ayuda_gral)?$ayuda_gral:'');

        # Creo el elemento
        $innyType = InnyType::factory($tipo);

        # Obtengo la metadata del elemento y se la seteo
        $parametros = array();
        $keys = $innyType->getParamKeys();
        foreach($keys as $key){
            $value = isset($this->parsedMetadata[$modo][$modo.$numero]['parametros'][$key]) ? $this->parsedMetadata[$modo][$modo.$numero]['parametros'][$key] : $this->getMetadataValue($modo,'parametros',$key);
            if($value !== null){
                $parametros[$key] = $value;
            }
        }
        if (!empty($ayuda)){
        	$innyType->setMetadata(array( 'nombre' => $nombre, 'tipo' => $tipo, 'parametros' => $parametros, 'ayuda' => $ayuda));
    	}else{
        	$innyType->setMetadata(array( 'nombre' => $nombre, 'tipo' => $tipo, 'parametros' => $parametros ));
        }

        # Retorno el elemento
        return $innyType;
    }

    /**
     * Retorna la cantidad de productos que pertenecen al listado
     *
     * @access public
     * @return integer cantidad de productos que pertenecen al listado
     */
    public function getCantidadProductos($getDesdeCache=true){

        # En caso que la cantidad est� cacheada
        if($getDesdeCache && ($cantidad = $this->getValorEnCache(array('cantidad')) !== null)){
            return $cantidad;
        }

        # Obtengo la cantidad
        if(InnyCore_Dao::isEmpty($this->id_listado)){
            $cantidad = 0;
        }
        else{
            $daoProducto = DB_DataObject::factory('producto');
            $daoProducto->id_listado = $this->id_listado;
            $cantidad = $daoProducto->find();
        }

        # Cacheo la cantidad y la retorno
        $this->setValorEnCache(array('producto_cantidad'),$cantidad);
        return $cantidad;
    }

    /**
     * Obtiene informaci�n referente a los archivos ordenables
     *
     * @access public
     * @return array nombre, tipo y cantidad de archivos ordenables
     */
    public function getSortablesInfo(){
        $info = array();
        $archivo_cantidad = $this->getMetadataValue("archivo","cantidad");
        if($archivo_cantidad > 0){

            # Seteo la cantidad de archivos ordenables que tiene el listado
            $info['cantidad'] = $archivo_cantidad - $this->cantArchivosFijos;

            # Seteo el tipo de dato
            $innyFileType = $this->createInnyType('archivo',$this->cantArchivosFijos+1);
            $info['tipo'] = $innyFileType->getMetadataValue('tipo');

            # Seteo el nombre del archivo
            $info['nombre'] = $innyFileType->getMetadataValue('nombre');
        }
        else{
            $info['cantidad'] = 0;
            $info['nombre'] = $info['tipo'] = 'none';
        }
        return $info;
    }

    /**
     * Obtiene nombre y tipo de cada archivo relacionado a los productos del listado
     *
     * @access public
     * @return string informaci�n de los archivos en formato JSON
     */
    public function getFilesInfo(){
        $archivo_cantidad = $this->getMetadataValue("archivo","cantidad");
        if($archivo_cantidad == 0){
            return '[]';
        }

        $data = array();
        for($i = 0; $i < $archivo_cantidad; $i++){
            $innyFileType = $this->createInnyType('archivo',$i+1);
            $data[] = array(
                'nombre' => $innyFileType->getMetadataValue('nombre'),
                'tipo' => $innyFileType->getMetadataValue('tipo')
            );
        }
        Denko::arrayUtf8Encode($data);
        return json_encode($data);
    }

    /**
     * Verifica si un DAO Producto pertenece a un listado
     *
     * @param mixed $id_producto ID del DAO Producto, o arreglo de IDs
     * @param array $debug_backtrace debug_backtrace de la funci�n que invoca
     * @access public
     * @return boolean si el producto pertenece o no al listado
     */
    public function productosQuePertenecen($id_producto,$debug_backtrace=null){

        # Primero verifico si el listado al menos est� en DB
        if(!InnyCore_Dao::isId($this->id_listado)){
            return null;
        }

        # Verifico que el ID del producto no est� vac�o
        if(empty($id_producto)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El ID del DAO Producto es vac�o o nulo',$debug_backtrace);
            return null;
        }

        # === En caso que el ID sea un arreglo de IDs, verifico uno por uno == #
        if(is_array($id_producto)){

            # Verifico que el arreglo de IDs no est� vac�o
            if(count($id_producto) == 0){
                InnyModule_Reporter::core_error(E_USER_WARNING,'Debe haber al menos un ID de DAO Producto',$debug_backtrace);
                return null;
            }

            $ids = array();
            foreach($id_producto as $id){
                $pk = $this->productosQuePertenecen($id,$debug_backtrace);
                if($pk != null){
                    $ids[] = $pk;
                }
            }

            # En caso que no haya ning�n ID de DAO Producto que pertenezca al
            # sitio, retorno NULL
            return count($ids) > 0 ? $ids : null;
        }

        # =========== En caso que el ID del producto sea uno solo ============ #
        $id_producto = trim($id_producto);

        # Verifico que el DAO Producto exista
        $daoProducto = InnyCore_Dao::getDao('producto',$id_producto);
        if($daoProducto == null){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El DAO Producto con ID "'.$id_producto.'" no existe',$debug_backtrace);
            return null;
        }

        # Verifico que el DAO Producto pertenezca al DAO Listado
        if($daoProducto->id_listado != $this->id_listado){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El DAO Producto con ID "'.$id_producto.'" no pertenece al listado "'.$this->nombre.'"',$debug_backtrace);
            return null;
        }

        # En caso que el ID del producto sea v�lido
        return $daoProducto->id_producto;
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                MISC                                    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Setea un valor en cach�
     *
     * @param array $path ruta del valor
     * @param mixed $valor valor que se desea setear
     * @access public
     * @return boolean si se pudo cachear el valor
     */
    public function setValorEnCache($path,$valor){
        return InnyCore_Dao::isEmpty($this->id_listado) ? false : InnyCore_Dao::setValorEnCache(self::$cache_prefix.$this->id_listado,$path,$valor);
    }

    /**
     * Obtiene un valor de la cache
     *
     * @param array $path ruta del valor
     * @access public
     * @return mixed valor cacheado, NULL en caso que no exista el valor en cache
     */
    public function getValorEnCache($path){
        return InnyCore_Dao::isEmpty($this->id_listado) ? null : InnyCore_Dao::getValorEnCache(self::$cache_prefix.$this->id_listado,$path);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                               METADATA                                 //
    ////////////////////////////////////////////////////////////////////////////

    /**
	 * Parsea la metadata correspondiente al listado
	 *
	 * @access protected
	 * @return void
	 */
	protected function _parseMetadata(){

        # Inicializo el arreglo de la metadata
        $this->parsedMetadata = null;

        # Parseo la metadata JSON en un arreglo
        if($this->metadata){
            $this->parsedMetadata = json_decode($this->metadata,true);
            Denko::arrayUtf8Decode($this->parsedMetadata);

            # Cuento la cantidad de archivos fijo que tiene el listado
            # Se deduce que un archivo es fijo cuando tiene seteado al menos un nombre, tipo y/o par�metros
            $archivo_cantidad = $this->getMetadataValue('archivo','cantidad');
            $this->cantArchivosFijos = 0;
            $hasParam = false;
            if($archivo_cantidad > 0){
                for($i = 1; $i <= $archivo_cantidad; $i++){
                    if(!$hasParam && isset($this->parsedMetadata['archivo'.$i]['parametros'])){
                        $hasParam = true;
                    }
                    if(!empty($this->parsedMetadata['archivo']['archivo'.$i]['nombre']) || !empty($this->parsedMetadata['archivo']['archivo'.$i]['tipo']) || $hasParam) {
			   			$this->cantArchivosFijos++;
					}
               }
            }
        }
	}

    /**
     * Obtiene una configuraci�n de metadata seteada para el listado
     *
     * @access public
     * @return mixed
     */
    public function getMetadataValue(){

        # Obtengo el nro de par�metros
        $num_args = func_num_args();

        # En caso que no haya par�metros, no retorno valor alguno
        if($num_args == 0){
            return null;
        }

        # En caso excepcional que pidan el nombre
        $arg = func_get_arg(0);
        if($num_args == 1 && strtolower(trim($arg)) == 'nombre'){
            return isset($this->parsedMetadata['nombre']) ? $this->parsedMetadata['nombre'] : $this->nombre;
        }

        # Busco cual deber�a ser el �ndice que debo retornar
        $evalIndex = '';
        for($i = 0 ; $i < $num_args; $i++){
            $arg = func_get_arg($i);
            $evalIndex.='["'.strtolower(trim($arg)).'"]';
        }

        # Verifico si puedo obtener el valor de la configuraci�n
        eval('$metadata = isset($this->parsedMetadata'.$evalIndex.') ? $this->parsedMetadata'.$evalIndex.' : ( isset(InnyCore_Metadata::$defaultMetadata_Listado'.$evalIndex.') ? InnyCore_Metadata::$defaultMetadata_Listado'.$evalIndex.' : null );');

        # En caso que no lo obtenga y sea una configuraci�n de contador
        if($metadata == null && ($arg0 = strtolower(trim(func_get_arg(0)))) == 'contador'){
            $parametros_contador = func_get_args();
            array_shift($parametros_contador);
            return InnyModule_Contador::getMetadataValue($this,$parametros_contador);
        }

        # En caso que no lo obtenga, verifico si puedo obtener su default
        if($metadata == null && $num_args > 1 && (substr(strtolower(trim(func_get_arg(1))),0,7) == 'archivo' || substr(strtolower(trim(func_get_arg(1))),0,5) == 'texto')){
            $params = '\''.func_get_arg(0).'\'';
            for($i = 2 ; $i < $num_args; $i++){
                $params.= ',\''.func_get_arg($i).'\'';
            }
            eval('$metadata = $this->getMetadataValue('.$params.');');
        }

        # Retorno el valor de la metadata
        return $metadata;
    }

    /**
     * Obtiene el nombre del campo de texto con mayor prioridad
     *
     * @access public
     * @return string nombre del campo de texto con mayor prioridad, 'none' en caso de no existir
     */
    public function getCampoTextoPrincipal(){
        $texto_prioridad = $this->getMetadataValue('texto','prioridad');
        return ($texto_prioridad == 'none') ? $texto_prioridad : 'texto'.$texto_prioridad[0];
    }

    /**
     * Averigua si se alcanz� el limite m�ximo de elementos del listado
     *
     * @param integer $valor valor adicional que se suma a la cantidad de productos en el listado  
     * @access public
     * @return boolean si se alcanz� el limite m�ximo de elementos del listado
     */
    public function alcanzoLimiteProductosPermitido($valor = 0){
        $parametro_limit  = $this->getMetadataValue('parametros','limit');
        $listado_cantidad = $this->getCantidadProductos()+$valor;
        return ($parametro_limit != 'none' && InnyCore_Utils::is_integer($listado_cantidad) && $listado_cantidad >= $parametro_limit);
    }

    /**
     * Verifica si el listado tiene cierto permiso
     *
     * @param string $permiso permiso [ A | B | M | V | O ]
     * @access public
     * @return boolean si el listado tiene cierto permiso
     */
    public function tienePermiso($permiso){
        return InnyCore_Metadata::tienePermiso($this,$permiso);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                           CARDINALIDAD                                 //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Retorna las relaciones de cardinalidad hacia otros listados
     *
     * @access public
     * @return array nombres de los listados y sus l�mites, NULL en caso que no existan relaciones
     */
    public function getCardinalidadesHaciaOtrosListados(){

        # Obtengo el sitio al cual pertenece el listado
        $daoSitio = $this->getSitio();

        # En caso que el sitio no est� seteado, returno NULL
        if($daoSitio == null || ($cardinalidad = $daoSitio->getMetadataValue('cardinalidad')) == null){
            return null;
        }
        return isset($this->parsedMetadata['cardinalidad']) && count($this->parsedMetadata['cardinalidad']) > 0 ? $this->parsedMetadata['cardinalidad'] : null;
    }

    /**
     * Retorna las relaciones de cardinalidad desde otros listados
     *
     * @access public
     * @return array nombres de los listados y sus l�mites, NULL en caso que no existan relaciones
     */
    public function getCardinalidadesDesdeOtrosListados(){

        # Obtengo el sitio al cual pertenece el listado
        $daoSitio = $this->getSitio();

        # En caso que el sitio no est� seteado, returno NULL
        if($daoSitio == null){
            return null;
        }
        $arreglo_cardinalidades = array();
        $cardinalidad_sitio = $daoSitio->getMetadataValue('cardinalidad');
        if($cardinalidad_sitio !== null){
            foreach($cardinalidad_sitio as $listado_nombre => $listados_cardinales){
                foreach($listados_cardinales as $nombre_listado_cardinal => $limites){
                    if($nombre_listado_cardinal == $this->nombre){
                        $arreglo_cardinalidades[$listado_nombre] = $limites;
                    }
                }
            }
        }
        return count($arreglo_cardinalidades) > 0 ? $arreglo_cardinalidades : null;
    }

    /**
     * Averigua si el listado tiene cardinalidades con otros listados
     *
     * @access public
     * @return boolean si el listado tiene cardinalidades con otros listados
     */
    public function tieneCardinalidades(){
        return ($this->getCardinalidadesHaciaOtrosListados() != null || $this->getCardinalidadesDesdeOtrosListados() != null);
    }

    /**
     * Verifica si se respeta el l�mite superior de la cardinalidad
     *
     *  Asumo que el arreglo $id_producto_cardinal no est� vac�o, y est� compuesto
     *  de IDs que corresponden a productos que pertenecen todos a un mismo listado (el cardinal)
     *
     * @param DataObjects_Listado &$daoListado DAO Listado al que pertenecen los productos
     * @param mixed $id_producto ID del DAO Producto, o arreglo de IDs
     * @param array $debug_backtrace debug_backtrace de la funci�n que invoca
     * @access public
     * @return boolean si respeta la cardinalidad o no
     * @deprecated en favor de InnyModule_Cardinal::verificarCardinalidadEntreListados
     */
    public function respetaLimitesCardinalidad(&$daoProducto,$ids_producto_cardinal_categorizado,$debug_backtrace=null){

        # Verifico que el DAO Producto pertenezca al listado
        if($this->id_listado != $daoProducto->id_listado){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El producto con ID "'.$daoProducto->id_producto.'" no pertenece al listado "'.$this->nombre.'"',$debug_backtrace);
            return null;
        }

        # Obtengo el Listado Cardinal, que el listado al que pertenecen los
        # productos que se quieren relacionar al producto del Listado Actual
        $daoProductoCardinal = InnyCore_Dao::getDao('producto',$ids_producto_cardinal_categorizado['insert'][0]);
        $daoListadoCardinal = InnyCore_Dao::getDao('listado',$daoProductoCardinal->id_listado);

        # ==================================================================== #

        # Verifico si la cantidad de relaciones que tiene actualmente el producto
        # no sea el m�ximo
        $limite_superior_actual = $this->getMetadataValue('cardinalidad',$daoListadoCardinal->nombre,'sup');
        if($limite_superior_actual != null && InnyCore_Utils::is_integer($limite_superior_actual)){

            # - En caso que se desee sincronizar, solo deben tenerse en cuenta
            #   las relaciones que se mantienen y las nuevas, ya que las que no
            #   se tienen en cuenta se asume que se eliminar�n
            # - En caso de no sincronizar, se toman en cuenta
            #   - las nuevas relaciones
            #   - las relaciones actuales
            #   - las relaciones que se omiten
            $cantidad_relaciones_actuales = (
                InnyModule_Cardinal::getSetting('sincronizar') ?
                    count($ids_producto_cardinal_categorizado['update'])
                    :
                    $daoProducto->getCantidadDeRelacionesConListado($daoListadoCardinal)
            );

            if(InnyModule_Cardinal::compare($cantidad_relaciones_actuales,$limite_superior_actual) == 0){
                InnyModule_Reporter::core_error(E_USER_WARNING,'No puede relacionarse el producto ID "'.$daoProducto->id_producto.'" del listado "'.$this->nombre.'" a un producto del listado "'.$daoListadoCardinal->nombre.'" porque alcanz� su m�xima cantidad de relaciones ('.$limite_superior_actual.')',$debug_backtrace);
                return null;
            }
        }

        # ==================================================================== #

        # Obtengo la cardinalidad seteada en el listado cadinal
        # En caso que exista, verifico que no exceda el l�mite superior de la cardinalidad
        #
        # Verifico bandas por banda que no hayan alcanzado su cupo m�ximo de
        # bateristas Las bandas que puede asignarse este baterista, son apartadas
        # en el arreglo $ids_producto_valido
        $limite_superior_cardinal = $daoListadoCardinal->getMetadataValue('cardinalidad',$this->nombre,'sup');
        if($limite_superior_cardinal != null && InnyCore_Utils::is_integer($limite_superior_cardinal)){
            $ids_producto_valido = array();
            foreach($ids_producto_cardinal_categorizado['insert'] as $id_producto_cardinal){

                # Obtengo la cantidad de relaciones que tiene con productos de este listado
                $daoProductoCardinal = InnyCore_Dao::getDao('producto',$id_producto_cardinal);
                $cantidadProductosRelacionados = $daoProductoCardinal->getCantidadDeRelacionesConListado($this);

                # En caso que la
                if(InnyModule_Cardinal::compare($cantidadProductosRelacionados+1,$limite_superior_cardinal) == 1){
                    InnyModule_Reporter::core_error(E_USER_WARNING,'Al relacionar un producto del listado "'.$this->nombre.'" al listado "'.$daoListadoCardinal->nombre.'" se excede el l�mite de la cardinalidad (max: '.$limite_superior_cardinal.')',$debug_backtrace);
                    continue;
                }
                $ids_producto_valido[] = $id_producto_cardinal;
            }

            # En caso que no haya ningun producto con el cual se pueda
            # establecer una relaci�n, retorno NULL
            if(count($ids_producto_valido) == 0){
                return null;
            }
        }

        # En caso que no haya cardinalidad seteada, asumo que todos los IDs de
        # productos v�lidos son todos
        #
        # Asumo que una banda puede tener N bateristas
        else{
            $ids_producto_valido = $ids_producto_cardinal_categorizado['insert'];
        }

        # ==================================================================== #

        # Verifico en el listado actual si hay seteada una cardinalidad hacia el listado cardinal
        # En caso que est� seteada, solo es necesario comparar mientras que el
        # l�mite no sea N (es decir, que el l�mite sea un n�mero entero)
        #
        # En caso que haya que asignar un baterista a una banda, me fijo si el
        # baterista alcanz� su m�ximo cupo de bandas
        $limite_superior_actual = $this->getMetadataValue('cardinalidad',$daoListadoCardinal->nombre,'sup');
        if($limite_superior_actual != null && InnyCore_Utils::is_integer($limite_superior_actual)){

            # Obtengo la cantidad total de relaciones que tendr�a estableciendo
            # las nuevas relaciones
            $cantidad_relaciones = count($ids_producto_valido) + $cantidad_relaciones_actuales;

            # En caso que puedan establecerse s�lo algunas relaciones, solo tengo
            # en cuenta las que pueda relacionar, hasta llegar al m�ximo
            if(InnyModule_Cardinal::compare($cantidad_relaciones,$limite_superior_actual) == 1){
                $ids_validos = array();
                while(count($ids_validos) + $cantidad_relaciones_actuales < $limite_superior_actual){
                    $ids_validos[] = array_shift($ids_producto_valido);
                }
                InnyModule_Reporter::core_error(E_USER_WARNING,'Solo pueden relacionarse al listado "'.$daoListadoCardinal->nombre.'" los productos ID = {'.implode(',',$ids_validos).'} (los productos ID = {'.implode(',',$ids_producto_valido).'} quedar�n excluidos)',$debug_backtrace);
                return $ids_validos;
            }
        }

        # Retorno el los IDs de los productos que pueden relacionarse
        return $ids_producto_valido;
    }
}
################################################################################