<?php

# Archivos requeridos
require_once '../DAOs/Dse_document.php';
require_once '../commons/DFM.php';
require_once '../commons/inny.core.php';

/**
 * Table Definition for producto
 */
class DataObjects_Producto extends DataObjects_Dse_document
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'producto';                        // table name
    public $id_producto;                     // int(10)  not_null primary_key unsigned auto_increment

    public $texto1;                          // blob(65535)  blob binary
    public $texto2;                          // blob(65535)  blob binary
    public $texto3;                          // blob(65535)  blob binary
    public $texto4;                          // blob(65535)  blob
    public $texto5;                          // blob(65535)  blob
    public $texto6;                          // blob(65535)  blob
    public $texto7;                          // blob(65535)  blob
    public $texto8;                          // blob(65535)  blob
    public $texto9;                          // blob(65535)  blob
    public $texto10;                         // blob(65535)  blob
    public $texto11;                         // blob(65535)  blob
    public $texto12;                         // blob(65535)  blob
    public $texto13;                         // blob(65535)  blob
    public $texto14;                         // blob(65535)  blob
    public $texto15;                         // blob(65535)  blob
    public $texto16;                         // blob(65535)  blob
    public $texto17;                         // blob(65535)  blob
    public $texto18;                         // blob(65535)  blob
    public $texto19;                         // blob(65535)  blob
    public $texto20;                         // blob(65535)  blob

    public $contador1;                       // int(10)  not_null unsigned
    public $contador2;                       // int(10)  not_null unsigned
    public $contador3;                       // int(10)  not_null unsigned
    public $contador4;                       // int(10)  not_null unsigned
    public $contador5;                       // int(10)  not_null unsigned

    public $indice1;                         // int(10)  not_null multiple_key unsigned
    public $indice2;                         // int(10)  not_null multiple_key unsigned
    public $indice3;                         // int(10)  not_null multiple_key unsigned
    public $indice4;                         // int(10)  not_null multiple_key unsigned
    public $indice5;                         // int(10)  not_null multiple_key unsigned

    public $posicion;                        // int(10)  not_null unsigned
    public $id_listado;                      // int(10)  not_null multiple_key unsigned
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     * Se redefine el m�todo para asignarle la posici�n al final del listado.
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function insert(){
        if($this->id_listado){
            $daoProducto = DB_DataObject::factory('producto');
            $daoProducto->id_listado = $this->id_listado;
            $count = $daoProducto->find();
            $this->posicion = $count + 1;
            $daoProducto->free();
        }
        $this->setIndexMode();
        return parent::insert();
    }

    /**
     * Se redefine para indicar si debe indexarse con el DSE o no
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function update($dataObject = false){
        $this->setIndexMode();
        return parent::update();
    }

    /**
     * Se redefine el delete para eliminar sus archivos, y sincronizar las posiciones
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function delete($useWhere = false){

        # Elimino las relaciones en la tabla "producto_producto"
        $this->eliminarRelacionesConProductos();

        # Elimino los archivos relacionados al producto
        $this->eliminarArchivos();

        # En caso que el listado sea searchable, ni me caliento por updatear las posiciones
        $daoListado = $this->getListado();
        $listado_searchable = $daoListado->getMetadataValue("parametros","searchable");
        if($listado_searchable == 'true'){
            return parent::delete();
        }

        # En caso que el listado sea posicionable, sincronizo las posiciones de los productos
        $id_listado = $this->id_listado;
        $posicion = $this->posicion;
        $delete = parent::delete();
        if($delete){
			$this->query('update producto set posicion=posicion-1 where id_listado='.$id_listado.' and posicion > '.$posicion);
        }
        return $delete;
    }

    /**
     * Fuerza el delete, salteando algunos pasos (como el cambio de orden)
     *
     * @access public
     * @return mixed integer cantidad de DAOs eliminados, boolean FALSE en caso de fallo
     */
    public function forzarDelete(){

        # Elimino las relaciones en la tabla "producto_producto"
        $this->eliminarRelacionesConProductos();

        # Elimino los archivos relacionados al producto
        $this->eliminarArchivos();

        # Elimino el DAO
        return parent::delete();
    }

    /**
     * Retorna el listado al que pertenece el Sitio
     *
     * @access public
     * @return DataObjects_Sitio DAO Sitio al que pertenece el producto
     */
    public function getSitio(){
        $daoListado = $this->getListado();
        $daoSitio = $daoListado->getSitio();
        return $daoSitio;
    }

    /**
     * Retorna el listado al que pertenece el Producto
     *
     * @access public
     * @return DataObjects_Listado DAO Sitio al que pertenece el producto
     */
    public function getListado(){
        return InnyCore_Dao::getDao('listado',$this->id_listado);
    }

    /**
	 * Retorna las relaciones en la tabla "producto_temporal" que posee el producto
     *
     * @param integer $posicion posici�n/n�mero de archivo
     * @access public
     * @return DataObjects_Producto_temporal relaciones en la tabla "producto_temporal" que posee el producto
	 */
    public function getProductoTemporal($posicion = null){

        # En caso que el producto a�n no haya sido insertado
        if(!InnyCore_Dao::isId($this->id_producto)){
            return null;
        }

		$daoProductoTemporal = DB_DataObject::factory('producto_temporal');
		$daoProductoTemporal->id_producto = $this->id_producto;

		# En caso que est� definida la posici�n, solo retorno el DAO al que
		# corresponde esa posici�n
		if($posicion){
            $daoProductoTemporal->posicion = $posicion;
            return $daoProductoTemporal->find(true) ? $daoProductoTemporal : null;
        }

        # En caso contrario retorno todos los DAOS, ordenados por posici�n
        $daoProductoTemporal->orderBy('posicion');
        return $daoProductoTemporal->find() ? $daoProductoTemporal : null;
    }

    /**
     * Obtiene el DAO de la tabla 'producto_temporal' relacionado al producto
     *
     * @param integer $posicion posici�n del archivo
     * @access public
     * @return DataObjects_Producto_temporal
     */
    public function getCachedProductoTemporal($posicion){

        # Armo la clave de la variable global de la cach�
        $key = (Inny::$daoCacheKey).'PRODUCTO_TEMPORAL_'.$this->id_producto.'_'.$posicion;

        # En caso que el DAO ya est� cacheado
        if(empty($GLOBALS[$key])){
            if(empty($this->id_producto) || $this->id_producto == 'null'){
                return null;
            }
            $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
            $daoProductoTemporal->id_producto = $this->id_producto;
            $daoProductoTemporal->posicion = $posicion;

            # Si existe la relaci�n la guardo en cach�.
            # En caso contrario, guardo la string 'null', indicando que no existe
            if(!$daoProductoTemporal->find(true)){
                return null;
            }
            $GLOBALS[$key] = $daoProductoTemporal;
        }

        # Retorno el resultado en cach�
        return $GLOBALS[$key];
    }

     /**
      * Retorna la cantidad de temporales que tiene asociado el producto.
      *
      * @access public
      * @return integer
      */
	 public function getCantidadTemporales(){
	 	$daoProductoTemporal = DB_DataObject::factory('producto_temporal');
	 	$daoProductoTemporal->id_producto = $this->id_producto;
	 	return $daoProductoTemporal->find();
	 }

	/**
	 * Retorna si el campo tiene seteado valor aguno
	 *
	 * @param string $tipo tipo de campo [archivo|texto]
	 * @param integer $numero numero de campo
	 * @access public
	 * @return boolean
	 */
    public function hasContent($tipo,$numero){

        # En caso que el tipo de campo sea archivo, me fijo si tiene asignado
        # archivo alguno en esa posici�n
        if($tipo == 'archivo'){
            return ($this->getProductoTemporal($numero) != null);
        }

        # En caso que sea text, verifico si tiene seteado texto alguno en ese campo
        $campo = $tipo.$numero;
        return !InnyCore_Dao::isEmpty($this->$campo);
     }

    /**
     * Retorna si el producto pertenece determinado Sitio
     */
    public function perteneceAlSitio($id_sitio){
        $daoListado = DB_DataObject::factory('listado');
        $daoListado->selectAdd();
        $daoListado->selectAdd('id_listado,id_sitio');
        $daoListado->id_listado = $this->id_listado;
        $daoListado->find(true);
        return ($daoListado->id_sitio == $id_sitio);
    }

    /**
     * Decrementa la posici�n del producto en su listado
     *
     * @access public
     * @return si se decrement� la posici�n del producto en su listado
     */
    public function bajarPosicion(){
        $daoProducto = DB_DataObject::factory('producto');
        $daoProducto->whereAdd('id_listado = '.$this->id_listado.' and posicion = '.($this->posicion+1));
        if($daoProducto->find()){
            $daoProducto->fetch();
            $daoProducto->posicion = $this->posicion;
            $this->posicion = $this->posicion + 1;
            return ($daoProducto->update() !== false && $this->update() !== false);
        }
        return true;
    }

    /**
     * Incrementa la posici�n del producto en su listado
     *
     * @access public
     * @return boolean si se increment� la posici�n del producto en su listado
     */
    public function subirPosicion(){
        if($this->posicion > 1){
            $daoProducto = DB_DataObject::factory('producto');
            $daoProducto->whereAdd('id_listado = '.$this->id_listado.' and posicion = '.($this->posicion - 1));
            if($daoProducto->find()){
                $daoProducto->fetch();
                $daoProducto->posicion = $this->posicion;
                $this->posicion = ($this->posicion - 1);
                return ($this->update() !== false && $daoProducto->update() !== false);
            }
        }
        return true;
    }

    /**
     * Elimina todos los archivos relacionados al producto
     *
     * @access public
     * @return void
     */
    public function eliminarArchivos(){

        # Obtengo las relaciones producto_temporal relacionadas al producto
    	$daoProductoTemporal = $this->getProductoTemporal();

    	# Elimino los archivos relacionados al producto
    	if($daoProductoTemporal){
        	while($daoProductoTemporal->fetch()){
        	   $daoProductoTemporal->normalDelete();
        	}
        }
    }

    /**
     * Sincroniza los archivos posicionables
     *
     * @access public
     * @return void
     */
    public function sincronizarArchivosPosicionables(){

        $daoListado = $this->getListado();
        $cantidadArchivosFijos = $daoListado->getCantidadArchivosFijos();
        $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
		$daoProductoTemporal->id_producto = $this->id_producto;
		$daoProductoTemporal->whereAdd('
            id_producto = '.$this->id_producto.'
            and posicion > '.$cantidadArchivosFijos.'
        ');
        $daoProductoTemporal->orderBy('posicion');
        if($daoProductoTemporal->find()){
            $posicion = $cantidadArchivosFijos + 1;
            while($daoProductoTemporal->fetch()){
                $daoProductoTemporal->posicion = $posicion++;
                $daoProductoTemporal->update();
            }
        }
    }

    /**
     * Mantiene sincronizadas las categor�as del producto con las que est�n en el arreglo
     *
     * @param array $ids_categoria categor�as con las que sincronizar
     * @access public
     * @return void
     */
    public function sincronizarCategorias($ids_categoria){

        # Arreglos de categor�as que se eliminar�n y agregar�n
        $id_categoria_delete = array();
        $id_categoria_insert = array();

        # Obtengo las categor�as que ya ten�a asignadas
        $daoDseCategory = $this->getAddedCategories();
        if($daoDseCategory && $daoDseCategory->find()){
            $id_categoria_actuales = array();
            while($daoDseCategory->fetch()){
                $id_categoria_actuales[] = $daoDseCategory->id_category;
            }

            # Obtengo cuales debo eliminar
            foreach($id_categoria_actuales as $id_categoria_actual){
                if(!in_array($id_categoria_actual,$ids_categoria)){
                    $id_categoria_delete[] = $id_categoria_actual;
                }
            }

            # Obtengo las nuevas relaciones
            foreach($ids_categoria as $id_categoria){
                if(!in_array($id_categoria,$id_categoria_actuales)){
                    $id_categoria_insert[] = $id_categoria;
                }
            }
        }
        else{
            unset($id_categoria_insert);
            $id_categoria_insert = $ids_categoria;
        }

        # Elimino las categor�as que ya no est�n asociadas al documento
        foreach($id_categoria_delete as $id_categoria){
            $this->removeCategory($id_categoria);
        }

        # Agrego las categor�as nuevas que se definieron para el producto
        foreach($id_categoria_insert as $id_categoria){
            $this->addCategory($id_categoria);
        }
    }

    /**
     * Setea si debe indexarse el producto
     *
     * @access protected
     * @return void
     */
    protected function setIndexMode(){
        $daoListado = $this->getListado();
        $listado_searchable = $daoListado->getMetadataValue('parametros','searchable');
        if(!$listado_searchable || strtolower($listado_searchable) != 'true'){
            $this->doNotIndex();
        }
    }

    /**
     * Ordena los archivos de un producto de forma consecutiva
     *
     * @access public
     * @return void
     */
    public function sortFiles(){
        $files=DB_DataObject::factory('producto_temporal');
    	$files->id_producto=$this->id_producto;
    	$files->orderBy('posicion');
    	$c=$files->find();
    	$pos=0;
    	while($files->fetch()){
    	    $pos++;
    	    if($files->posicion != $pos){
    		$files->posicion=$pos;
    		$files->update();
    	    }
    	}
    	$files->free();
    }

    ////////////////////////////////////////////////////////////////////////////
    //                            CARDINALIDAD                                //
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Retorna la cantidad o los DAOs, que pertenecen a un listado, y que no se relacionan al producto
     *
     * @param DataObjects_Listado &$daoListado DAO Listado
     * @param boolean $relacionados indica si se escogen los relacionados o no relacionados
     * @param boolean $retornarSoloCantidad indica si se retorna la cantidad o los DAOs
     * @param array $opciones arreglo de opciones
     * @access protected
     * @return mixed cantidad de productos no relacionados, o los DAOs Producto que no se relacionan
     */
    protected function _getProductosRelacionados(&$daoListadoCardinal,$relacionados=true,$retornarSoloCantidad=false,$opciones=null){

        # Verifico que el sitio al menos haya sido insertado
        if(!InnyCore_Dao::isId($this->id_listado) || ($relacionados === true && !InnyCore_Dao::isId($this->id_producto))){
            return $retornarSoloCantidad ? 0 : null;
        }

        # Obtengo los productos del listado que no se relacionen con este ($this) producto
        $daoProducto = DB_DataObject::factory('producto');

        #
        if($relacionados === false && !InnyCore_Dao::isId($this->id_producto)){
            $whereAdd = '
                id_listado = '.$daoListadoCardinal->id_listado.'

            ';
        }

        #
        else{
            $whereAdd = '
                id_listado = '.$daoListadoCardinal->id_listado.'
                and id_producto '.($relacionados ? '' : 'not ').'in (
                    select id_producto'.($this->id_listado <= $daoListadoCardinal->id_listado ? '2' : '1').'
                    from producto_producto
                    where id_listado'.($this->id_listado <= $daoListadoCardinal->id_listado ? '1' : '2').' = '.$this->id_listado.(InnyCore_Dao::isId($this->id_producto) ? (' and id_producto'.($this->id_listado <= $daoListadoCardinal->id_listado ? '1' : '2').' = '.$this->id_producto) : '').'
                )
            ';
        }

        # Seteo el whereAdd
        $daoProducto->whereAdd($whereAdd);

        if ($opciones !==null && isset($opciones['limit'])){
        	$daoProducto->limit($opciones['limit']['start'],$opciones['limit']['count']);
        }
        
        # Verifico si le debo agregar un "like"
        if($opciones !== null && isset($opciones['like']) && ($texto_primario = $daoListadoCardinal->getCampoTextoPrincipal()) != 'none'){
            $listado_searchable = $daoListadoCardinal->getMetadataValue('parametros','searchable');
            if($listado_searchable == 'true'){
                unset($daoProducto);
                $query = $opciones['like'];
                $daoProducto = dse_search($query,null,null,$whereAdd);
            }
            else{
                $daoProducto->whereAdd($texto_primario.' like \'%'.$opciones['like'].'%\'');
            }
        }
        # Obtengo la cantidad
        $cantidad = $daoProducto->find();
        
        # Retorno el resultado
        return $retornarSoloCantidad ? $cantidad : ($cantidad > 0 ? $daoProducto : null);
    }

    /**
     * Retorna los productos que pertenecen a un listado, y que si se relacionan al producto
     *
     * @param DataObjects_Listado &$daoListadoCardinal DAO Listado
     * @param array $opciones arreglo de opciones
     * @access public
     * @return DataObjects_Producto DAOs Producto que si se relacionan, o NULL en caso que no haya
     */
    public function getProductosConRelacion(&$daoListadoCardinal,$opciones=null){
        return $this->_getProductosRelacionados($daoListadoCardinal,true,false,$opciones);
    }

    /**
     * Retorna los productos que pertenecen a un listado, y que no se relacionan al producto
     *
     * @param DataObjects_Listado &$daoListadoCardinal DAO Listado
     * @param array $opciones arreglo de opciones
     * @access public
     * @return DataObjects_Producto DAOs Producto que no se relacionan, o NULL en caso que no haya
     */
    public function getProductosSinRelacion(&$daoListadoCardinal,$opciones=null){
        return $this->_getProductosRelacionados($daoListadoCardinal,false,false,$opciones);
    }

    /**
     * Retorna la cantidad de productos que pertenecen a un listado, y que no se relacionan al producto
     * @param DataObjects_Listado &$daoListadoCardinal DAO Listado
     * @access public
     * @return integer cantidad de productos que no se relacionan a este producto
     */
    public function getCantProductosSinRelacion(&$daoListadoCardinal){
        return $this->_getProductosSinRelacion($daoListadoCardinal,false,true);
    }

    /**
     * Retorna la cantidad de productos que pertenecen a un listado, y que si se relacionan al producto
     *
     * @param DataObjects_Listado &$daoListadoCardinal DAO Listado
     * @access public
     * @return integer cantidad de productos que si se relacionan a este producto
     */
    public function getCantProductosConRelacion(&$daoListadoCardinal){
        return $this->_getProductosSinRelacion($daoListadoCardinal,true,true);
    }

    /**
     * Retorna la cantidad o los DAOs que relaciona el producto con el listado
     *
     * @param DataObjects_Listado &$daoListado DAO Listado
     * @param boolean $retornarSoloCantidad indica si se retorna la cantidad o los DAOs
     * @access protected
     * @return mixed cantidad de relaciones o relaciones de la tabla "producto_producto"
     */
    protected function _getRelacionesConListado(&$daoListado,$retornarSoloCantidad=false){

        # Verifico que el sitio al menos haya sido insertado
        if(!InnyCore_Dao::isId($this->id_producto)){
            return $retornarSoloCantidad ? 0 : null;
        }

        # Preparo el "where add"
        if($this->id_listado <= $daoListado->id_listado){
            $whereAdd = '
                id_producto1 = '.$this->id_producto.'
                and id_listado1 = '.$this->id_listado.'
                and id_listado2 = '.$daoListado->id_listado.'
            ';
        }
        else{
            $whereAdd = '
                id_producto2 = '.$this->id_producto.'
                and id_listado2 = '.$this->id_listado.'
                and id_listado1 = '.$daoListado->id_listado.'
            ';
        }

        # Obtengo las relaciones y retorno el DAO
        $daoProductoProducto = DB_DataObject::factory('producto_producto');
        $daoProductoProducto->whereAdd($whereAdd);
        $cantidad = $daoProductoProducto->find();
        return $retornarSoloCantidad ? $cantidad : ($cantidad > 0 ? $daoProductoProducto : null);
    }

    /**
     * Obtiene las relaciones que tiene con un DAO Listado
     *
     * @param DataObjects_Listado &$daoListado DAO Listado
     * @access public
     * @return DataObjects_Producto_producto relaciones que tiene el producto con el listado
     */
    public function getRelacionesConListado(&$daoListado){
        return $this->_getRelacionesConListado($daoListado,false);
    }

    /**
     * Obtiene la cantidad de relaciones que tiene con un DAO Listado
     *
     * @param DataObjects_Listado &$daoListado DAO Listado
     * @access public
     * @return integer cantidad de relaciones que tiene el producto con el listado
     */
    public function getCantidadDeRelacionesConListado(&$daoListado){
        return $this->_getRelacionesConListado($daoListado,true);
    }

    /**
     * Obtiene el DAO Producto_Producto que contiene la relaci�n con otro producto
     *
     * @param string $action accion que se desea ejecutar [insert|delete]
     * @param integer $id_producto ID del DAO Producto con el que mantiene la relaci�n
     * @access public
     * @return DataObjects_Producto_producto relaci�n entre los productos, NULL en caso de no existir tal relaci�n
     */
    public function insertarEliminarRelacionConProducto($action,$id_producto){

        # Obtengo el Producto
        $daoProducto = InnyCore_Dao::getDao('producto',$id_producto);

        # Como norma, en los �ndices 1 seteo el producto que tenga menor o igual
        # valor que el ID del listado
        $daoProductoProducto = DB_DataObject::factory('producto_producto');
        if($this->id_listado <= $daoProducto->id_listado){
            $daoProductoProducto->id_producto1 = $this->id_producto;
            $daoProductoProducto->id_listado1  = $this->id_listado;
            $daoProductoProducto->id_producto2 = $daoProducto->id_producto;
            $daoProductoProducto->id_listado2  = $daoProducto->id_listado;
        }
        else{
            $daoProductoProducto->id_producto1 = $daoProducto->id_producto;
            $daoProductoProducto->id_listado1  = $daoProducto->id_listado;
            $daoProductoProducto->id_producto2 = $this->id_producto;
            $daoProductoProducto->id_listado2  = $this->id_listado;
        }

        switch($action){
            case 'insert': return $daoProductoProducto->find(true) ? true : ($daoProductoProducto->insert() != 0);
            case 'delete': return $daoProductoProducto->find(true) ? ($daoProductoProducto->delete() != false) : false;
            default: return false;
        }
    }

    /**
     * Elimina las relaciones con otros productos
     *
     * @access public
     * @return integer cantidad de relaciones con productos eliminadas
     */
    public function eliminarRelacionesConProductos(){
        $daoProductoProducto = DB_DataObject::factory('producto_producto');
        $daoProductoProducto->whereAdd('
            id_producto1 = '.$this->id_producto.' or id_producto2 = '.$this->id_producto.'
        ');
        $relaciones_eliminadas = 0;
        if($daoProductoProducto->find()){
            while($daoProductoProducto->fetch()){
                if($daoProductoProducto->delete()){
                    $relaciones_eliminadas++;
                }
            }
        }
        return $relaciones_eliminadas;
    }

    /**
     *
     *
     * @param DataObjects_Producto
     * @access public
     * @return boolean si existe la relaci�n con el producto
     */
    public function existeRelacionConProducto(&$daoProducto){

        # Verifico que el producto al menos haya sido insertado
        if(!InnyCore_Dao::isId($this->id_producto) || !InnyCore_Dao::isId($daoProducto->id_producto)){
            return false;
        }

        # Verifico que el producto tenga seteado el listado al que pertenece
        if(!InnyCore_Dao::isId($this->id_listado) || !InnyCore_Dao::isId($daoProducto->id_listado)){
            return false;
        }

        # Verifico que el producto exista en la DB
        $daoProductoProducto = DB_DataObject::factory('producto_producto');
        $daoProductoProducto->whereAdd(
            $this->id_listado <= $daoProducto->id_listado ?
                'id_producto1 = '.$this->id_producto.' and id_producto2 = '.$daoProducto->id_producto :
                'id_producto1 = '.$daoProducto->id_producto.' and id_producto2 = '.$this->id_producto
        );

        return ($daoProductoProducto->find() > 0);
    }

    /**
     * Verifica si un producto puede relacionarse con un listado cardinal
     *
     * @param DataObjects_Listado &$daoListadoCardinal Listado con cual puede relacionarse o no
     * @access public
     * @return boolean si puede relacionarse con el listado
     */
    public function puedeRelacionarseConListado(&$daoListadoCardinal){

        # Verifico que el producto al menos haya sido insertado
        if(!InnyCore_Dao::isId($this->id_producto)){
            return false;
        }

        # Obtengo el listado al que pertenece este producto
        $daoListado = $this->getListado();
        if($daoListado === null){
            return false;
        }

        # Verifico si existe una l�mite (cardinalidad superior) de cantidad de
        # relaciones con el listado. Y verifico si actuamente alcanz� ese l�mite
        $cardinalidad_superior = $daoListado->getMetadataValue('cardinalidad',$daoListadoCardinal->nombre,'sup');

        # El producto puede relacionarse si lo permite la cardinalidad
        return ($cardinalidad_superior === null || InnyModule_Cardinal::compare($this->getCantidadDeRelacionesConListado($daoListadoCardinal),$cardinalidad_superior) == -1);
    }

    /**
     * Verifica si un producto puede perder la relacion con un listado cardinal
     *
     * @param DataObjects_Listado &$daoListadoCardinal Listado con cual puede relacionarse o no
     * @access public
     * @return mixed boolean si puede perder la relacion con un listado cardinal, NULL en caso de fallo
     */
    public function puedePerderRelacionConListado(&$daoListadoCardinal){

        # Obtengo el listado al que pertenece este producto
        $daoListado = $this->getListado();
        if($daoListado === null){
            return null;
        }

        # Obtengo la cantidad de relaciones que tiene el producto actualmente con el listado
        $cantidad_relaciones = $this->getCantidadDeRelacionesConListado($daoListadoCardinal);
        if($cantidad_relaciones == 0){
            return true;
        }

        # Obtengo el l�mite inferior de cardinalidad
        $cardinalidad_inferior = $daoListado->getMetadataValue('cardinalidad',$daoListadoCardinal->nombre,'inf');
        if($cardinalidad_inferior !== null && $cardinalidad_inferior > 0 && InnyModule_Cardinal::compare($cantidad_relaciones,$cardinalidad_inferior) == 0){
            return false;
        }

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    //                              CONTADORES                                //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Verifica que el n�mero de contador y el valor que se setee sean v�lidos
     *
     * @param integer $contador_numero n�mero de contador
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @access public
     * @return boolean si el n�mero de contador y el valor que se setee son v�lidos
     */
    public function verificarContador($contador_numero,$debug_backtrace=null){

        # Obtengo el DAO Listado al que pertenece el DAO Producto
        $daoListado = $this->getListado();
        if($daoListado === null){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El producto ID "'.$this->id_producto.'" no tiene listado asociado',$debug_backtrace);
            return false;
        }

        # Retorno la verificaci�n si el n�mero de contador es v�lido
        return InnyModule_Contador::esContadorValido($daoListado,$contador_numero,$debug_backtrace);
    }

    /**
     * Setea un valor en uno de los contadores
     *
     * @param integer $contador_numero n�mero de contador
     * @param integer $contador_value valor que se desea setear en el contador
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @access public
     * @return boolean si pudo setear el valor
     */
    public function contadorSet($contador_numero,$contador_value,$debug_backtrace=null){

        # Obtengo el debug_backtrace
        if($debug_backtrace === null){
            $debug_backtrace = debug_backtrace();
        }

        # En caso que el numero de contador sea v�lido...
        if(InnyModule_Contador::esContadorValido($this->getListado(),$contador_numero,$debug_backtrace)){

            # Verifico que el valor del contador no est� vac�o
            $contador_value = trim($contador_value);
            if($contador_value === ''){
                InnyModule_Reporter::core_error(E_USER_WARNING,'El valor del contador "'.$contador_numero.'" no debe ser nulo o vac�o',$debug_backtrace);
                return false;
            }

            # Verifico que el valor del contador sea un entero v�lido
            if(!InnyCore_Utils::is_integer($contador_value)){
                InnyModule_Reporter::core_error(E_USER_WARNING,'El valor del contador "'.$contador_numero.'" debe ser un entero v�lido',$debug_backtrace);
                return false;
            }

            # Seteo el valor en el contador
            $contador_campo = 'contador'.trim($contador_numero);
            $this->$contador_campo = $contador_value;

            # Retorno que el seteo fu� exitoso
            return true;
        }

        # En caso que no se haya podido setear el valor
        return false;
    }

    /**
     * Incrementa el valor en uno de los contadores
     *
     * @param integer $contador_numero n�mero de contador
     * @param integer $contador_value valor que se desea incrementar en el contador
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @access public
     * @return boolean si pudo incrementar el valor
     */
    public function contadorIncrementar($contador_numero,$contador_value,$debug_backtrace=null){

        # Obtengo el debug_backtrace
        $debug_backtrace = ($debug_backtrace !== null) ? $debug_backtrace : debug_backtrace();

        # En caso que el numero de contador sea v�lido...
        if(InnyModule_Contador::esContadorValido($this->getListado(),$contador_numero,$debug_backtrace)){

            # Verifico que el valor del contador no est� vac�o
            $contador_value = trim($contador_value);
            if($contador_value === ''){
                InnyModule_Reporter::core_error(E_USER_WARNING,'El valor del contador "'.$contador_numero.'" no debe ser nulo o vac�o',$debug_backtrace);
                return false;
            }

            # Verifico que el valor del contador sea un entero v�lido
            if(!InnyCore_Utils::is_integer($contador_value)){
                InnyModule_Reporter::core_error(E_USER_WARNING,'El valor del contador "'.$contador_numero.'" debe ser un entero v�lido',$debug_backtrace);
                return false;
            }

            # Seteo el valor en el contador
            $contador_campo = 'contador'.trim($contador_numero);
            $this->$contador_campo = $this->$contador_campo + $contador_value;

            # Retorno que el seteo fu� exitoso
            return true;
        }

        # En caso que no se haya podido setear el valor
        return false;
    }

    /**
     * Avanza el valor en uno de los contadores
     *
     * @param integer $contador_numero n�mero de contador
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @access public
     * @return boolean si pudo avanzar el valor
     */
    public function contadorStep($contador_numero,$debug_backtrace=null){

        $daoListado = $this->getListado();
        if(empty($daoListado)){
            return false;
        }

        # Retorno el resultado del incremento
        return $this->contadorIncrementar(
            $contador_numero,
            $daoListado->getMetadataValue('contador','contador'.trim($contador_numero),'parametros','step'),
            $debug_backtrace ? $debug_backtrace : debug_backtrace()
        );
    }

    /**
     * Retrocede el valor en uno de los contadores
     *
     * @param integer $contador_numero n�mero de contador
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @access public
     * @return boolean si pudo retroceder el valor
     */
    public function contadorStepBack($contador_numero,$debug_backtrace=null){

        $daoListado = $this->getListado();
        if(empty($daoListado)){
            return false;
        }

        # Retorno el resultado del incremento
        return $this->contadorIncrementar(
            $contador_numero,
            $daoListado->getMetadataValue('contador','contador'.trim($contador_numero),'parametros','step') * -1,
            $debug_backtrace ? $debug_backtrace : debug_backtrace()
        );
    }

    /**
     * Resetea el valor en uno de los contadores
     *
     * @param integer $contador_numero n�mero de contador
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @access public
     * @return boolean si pudo resetar el valor
     */
    public function contadorReset($contador_numero,$debug_backtrace){

        # Obtengo el debug_backtrace
        if($debug_backtrace === null){
            $debug_backtrace = debug_backtrace();
        }

        # En caso que el numero de contador sea v�lido...
        $daoListado = $this->getListado();
        if(InnyModule_Contador::esContadorValido($daoListado,$contador_numero,$debug_backtrace)){

            # Avanzo el valor del contador
            $contador_campo = 'contador'.trim($contador_numero);
            $this->$contador_campo = $daoListado->getMetadataValue('contador',$contador_campo,'parametros','default');

            # Indico que se pudo resetar el valor
            return false;
        }

        # En caso que no se haya podido restear el contador
        return false;
    }

    /**
     * Obtiene los valores de los campos del Producto
     *
     * @access public
     * @return array valores de los campos del Producto
     */
    public function getValores(){

        # Obtengo los nombre de las columnas de la tabla producto, adem�s de sus archivos
    	$atributos = array('id_producto','posicion','id_listado','aud_ins_date','aud_upd_date');

    	# Genero los campos de los textos
        for($i = 1; $i <= InnyCore_Producto::$texto_cantidad; $i++){
            $atributos[] = InnyCore_Producto::$texto_prefijo.$i;
        }

        # Genero los campos de los contadores
        for($i = 1; $i <= InnyCore_Producto::$contador_cantidad; $i++){
            $atributos[] = InnyCore_Producto::$contador_prefijo.$i;
        }

        # Genero los campos de los �ndices
        for($i = 1; $i <= InnyCore_Producto::$indice_cantidad; $i++){
            $atributos[] = InnyCore_Producto::$indice_prefijo.$i;
        }

        # Seteo los atributos del Producto
        $result = array();
		foreach($atributos as $var){
			$result[$var] = $this->$var;
		}

        # Obtengo los IDs de los archivos asociados al producto
        $id_archivos = InnyCore_File::getIdsArchivo($this->id_producto);
		if($id_archivos !== null){
    		$result['ids_archivo'] = $id_archivos;
    		$result['id_archivo']  = $id_archivos[1];
        }

        # Retorno el conjunto de datos del Producto
        return $result;
    }
}
################################################################################
