<?php
/**
 * Table Definition for producto_producto
 */
require_once '../DAOs/AudDataObject.php';

class DataObjects_Producto_producto extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'producto_producto';               // table name
    public $id_producto1;                    // int(10)  not_null primary_key multiple_key unsigned
    public $id_producto2;                    // int(10)  not_null primary_key multiple_key unsigned
    public $id_listado1;                     // int(10)  not_null multiple_key unsigned
    public $id_listado2;                     // int(10)  not_null multiple_key unsigned
    public $data;                            // blob(65535)  blob binary
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
################################################################################