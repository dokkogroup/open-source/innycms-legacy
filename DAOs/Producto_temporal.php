<?php
require_once '../commons/DFM.php';
require_once '../DAOs/AudDataObject.php';

/**
 * Table Definition for producto_temporal
 */
class DataObjects_Producto_temporal extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'producto_temporal';               // table name
    public $id_producto;                     // int(10)  not_null primary_key multiple_key unsigned
    public $id_temporal;                     // int(10)  not_null primary_key multiple_key unsigned
    public $posicion;                        // int(11)
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     * Redefino para setear la posici�n al producto
     *
     * @access public
     * @return integer|boolean
     */
    public function insert(){

        # Antes de insertar, cuento cuantos archivos ya tiene asignado el producto
        $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
        $daoProductoTemporal->id_producto = $this->id_producto;
        $count = $daoProductoTemporal->find();

        # Luego, en base a la cantidad de archivos, seteo la posici�n
        #
        # TODO: TENER EN CUENTA LOS ARCHIVOS CON POSICIONES FIJA
        $this->posicion = $count + 1;

        # Por �ltimo inserto en la DB
        return parent::insert();
    }

    /**
     * Redefino para modificar las posiciones del resto de los archivos
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function delete($useWhere = false){

        $posicion = $this->posicion;
        $daoProducto = $this->getProducto();

        # Elimino el DAO
        $delete = $this->normalDelete();
        if($delete){

            # Obtengo la cantidad de archivos fijos
            $daoListado = $daoProducto->getListado();
            $cantidadArchivosFijos = $daoListado->getCantidadArchivosFijos();

            # Si la posici�n del archivo corresponde a uno posicionable, modifico
            # la posici�n de los archivos anteriores
            if($posicion > $cantidadArchivosFijos){

                $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
                $daoProductoTemporal->whereAdd('
                    id_producto = '.$daoProducto->id_producto.'
                    and posicion > '.$posicion.'
                ');
                $daoProductoTemporal->orderBy('posicion ASC');
                if($daoProductoTemporal->find()){
                    while($daoProductoTemporal->fetch()){
                        $daoProductoTemporal->posicion = $posicion++;
                        $daoProductoTemporal->update();
                    }
                }
            }
        }

        # Retorno el resultado del delete
        return $delete;
    }

    /**
     *
     */
    function insertWithUpdPos(){
    	return parent::insert();
    }

    /**
     *
     */
    function deleteWithUpdPos(){
    	return parent::delete();
    }

    /**
     * Elimina el DAO, sin modificar su posici�n y eliminando tambi�n el archivo relacionado
     *
     * @access public
     * @return integer|boolean:FALSE
     */
    public function normalDelete(){
        $delete = parent::delete();
        if($delete && $this->id_temporal && $this->id_temporal != 'null'){
            DFM::delete($this->id_temporal);
        }
        return $delete;
    }

    /**
     * Inserta de manera normal, sin redefinir nada
     *
     * @static
     * @access public
     * @return integer|boolean:FALSE
     */
    public function normalInsert(){
        return parent::insert();
    }

    /**
     * Retorna el DAO Producto relacionado
     *
     * @access public
     * @return DataObjects_Sitio
     */
    public function getProducto(){
        require_once '../commons/inny.core.php';
        return InnyCore_Dao::getDao('producto',$this->id_producto);
    }

    /**
     * Decrementa la posici�n del archivo
     *
     * @access public
     * @return boolean
     */
    public function bajarPosicion(){
        $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
        $daoProductoTemporal->whereAdd('
            id_producto = '.$this->id_producto.'
            and posicion = '.($this->posicion+1).'
        ');
        if($daoProductoTemporal->find(true)){

            # Seteo el proximo con la posicion del actual
            $daoProductoTemporal->posicion = $this->posicion;
            $daoProductoTemporal->update();

            # Al actual le incremento la posici�n
            $this->posicion = $this->posicion + 1;
            $this->update();
            return true;
        }

        return false;
    }

    /**
     * Incremeta la posici�n del archivo
     *
     * @access public
     * @return boolean
     */
    public function subirPosicion(){
        $daoProducto = $this->getProducto();
        $daoListado = $daoProducto->getListado();
        if(($this->posicion-1) <= $daoListado->getCantidadArchivosFijos()){
            return false;
        }

        $daoProductoTemporalAnterior = DB_DataObject::factory('producto_temporal');
        $daoProductoTemporalAnterior->whereAdd('
            id_producto = '.$this->id_producto.'
            and posicion = '.($this->posicion-1).' and posicion
        ');

        if($daoProductoTemporalAnterior->find(true)){

            # Seteo el proximo con la posicion del actual
            $daoProductoTemporalAnterior->posicion = $this->posicion;
            $daoProductoTemporalAnterior->update();

            # Al actual le incremento la posici�n
            $this->posicion = $this->posicion - 1;
            $this->update();
            return true;
        }

        return false;
    }
}
################################################################################