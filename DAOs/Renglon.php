<?php
/**
 * Table Definition for renglon
 */
require_once '../DAOs/AudDataObject.php';

class DataObjects_Renglon extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'renglon';                         // table name
    public $id_renglon;                      // int(10)  not_null primary_key unsigned auto_increment
    public $id_ctacorriente;                 // int(10)  not_null primary_key multiple_key unsigned
    public $id_tarea;                        // int(10)  multiple_key unsigned
    public $monto;                           // int(10)  not_null unsigned
    public $fecha_creacion;                  // datetime(19)  not_null binary
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     *
     */
    public function insert(){
        $this->fecha_creacion = date('Y-m-d H:i:s');
        return parent::insert();
    }

    /**
     *
     */
    public function getTarea(){
        $index = 'DOKKO_TRACKER_TAREA_'.$this->id_tarea;
        if(empty($GLOBALS[$index])){
            $daoTarea = DB_DataObject::factory('tarea');
    	    $daoTarea->get($this->id_tarea);
            $GLOBALS[$index] = $daoTarea;
        }
        return $GLOBALS[$index];
    }
}
################################################################################