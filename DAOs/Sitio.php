<?php
/**
 *
 */
require_once '../DAOs/AudDataObject.php';
require_once '../commons/inny.core.php';

/**
 * Valores de los flags
 */
define('SITIO_HABILITADO',1);
define('SITIO_DESHABILITADO',0);
define('SITIO_SIPUEDECREAR_LISTADO',1);
define('SITIO_NOPUEDECREAR_LISTADO',0);
define('SITIO_EN_PRODUCCION',0);
define('SITIO_EN_MANTENIMIENTO',1);

/**
 * Table Definition for sitio
 */
class DataObjects_Sitio extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'sitio';                           // table name
    public $id_sitio;                        // int(10)  not_null primary_key unsigned auto_increment
    public $nombre_sitio;                    // string(100)  not_null
    public $url;                             // string(200)  not_null
    public $id_ctacorriente;                 // int(10)  not_null multiple_key unsigned
    public $nombre_encargado;                // string(100)  not_null
    public $apellido;                        // string(50)  not_null
    public $telefono;                        // string(50)  not_null
    public $direccion;                       // string(100)  not_null
    public $email;                           // string(100)
    public $username;                        // string(100)  not_null unique_key
    public $password;                        // string(45)  not_null
    public $ftp;                             // string(100)
    public $ftp_username;                    // string(45)
    public $ftp_password;                    // string(45)
    public $cvs;                             // string(45)  not_null
    public $smtp;                            // string(250)
    public $idioma_codigo;                   // string(5)  not_null
    public $ga_codigo;                       // string(200)
    public $metadata;                        // blob(65535)  blob
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary
    public $habilitado;                      // int(1)  not_null
    public $mantenimiento;                   // int(1)
    public $version;                         // string(45)  not_null
    public $log_errors;                      // string(1)  not_null
    public $crea_listados;                   // string(1)  not_null
    public $dynamic_metadata;                        // blob(65535)  blob

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     * @var array arreglo con las configuraciones del SMTP
     * @access public
     */
    public $smtp_data;

    /**
     * @var array arreglo con las configuraciones de la metadata
     * @access public
     */
    public $parsed_metadata;

    /**
     * @var string �ndice de la cache de listados obtenidos por nombre del sitio
     * @static
     */
    protected static $cacheListadosPorNombre = 'INNY_DAO_CACHE_LISTADO_NOMBRE';

    /**
     * insert
     * Se redefine para guardar la metadata parseada
     *
     * @access public
     * @return mixed Id or key
     */
    public function insert(){

        # Genero la metadata
        $this->array2metadata();

        # Verifico que el nombre no exista en la DB
    	$daoSitio = DB_DataObject::factory('sitio');
		$daoSitio->whereAdd('nombre_sitio = '."'".$this->nombre_sitio."'");
		if($daoSitio->find(true)){
            return -1;
        }

    	# Creo una cuenta corriente para el sitio antes de agregarlo a la DB
    	$dao_ctacorriente = DB_DataObject::factory('ctacorriente');
    	$id_ctacorriente = $dao_ctacorriente->insert();
    	$this->mantenimiento = '0';
    	$this->id_ctacorriente = $id_ctacorriente;
    	$this->version = Inny::$version;
        return parent::insert();
    }

    /**
     * fetch
     * Se redefine para guardar la metadata parseada
     *
     * @access public
     * @return mixed number of rows affected or FALSE on failure
     */
    public function update($dataObject = false){
        $this->array2metadata();
        parent::update();
    }

    /**
     * Fetch. Se redefine para parsear la metadata
     *
     * @access public
     * @return boolean TRUE on success and FALSE on failure.
     */
    public function fetch(){
        $fetch = parent::fetch();
        if($fetch){
            $this->metadata2array();
        }
        return $fetch;
    }

    /**
     * Delete. Se redefine para eliminar todos sus contenidos y listados
     *
     * @access public
     * @return mixed integer cantidad de DAOs eliminados, boolean FALSE en fallo
     */
    public function delete($useWhere = false){

        # Elimino los listados del sitio
        $daoListados = $this->getListados();
        if(!empty($daoListados)){
            while($daoListados->fetch()){
                $daoListados->delete();
            }
        }

        # Elimino los contenidos relacionados al sitio
        $daoContenidos = $this->getContenidos();
        if(!empty($daoContenidos)){
            require_once '../DAOs/Contenido.php';
            while($daoContenidos->fetch()){
                $contenido = new Contenido($daoContenidos->nombre,$this->id_sitio);
                $contenido->delete();
            }
        }

        # TODO: eliminar las tareas relacionadas al sitio
        $daoTareas = $this->getTareas();
        if(!empty($daoTareas)){
            while($daoTareas->fetch()){
                $daoTareas->delete();
            }
        }

        # Elimino la Cuenta Corriente
        $daoCtaCorriente = $this->getCtaCorriente();
        $daoCtaCorriente->delete();

        # Elimino el sitio
        return parent::delete();
    }

    ////////////////////////////////////////////////////////////////////////////
    //                             METADATA                                   //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Parseo la informaci�n del SMTP y la agrego al arreglo $smtp_data
     *
     * @access public
     * @return void
     */
    public function metadata2array(){

        # Parseo la metadata correspondiente al SMTP
        $this->smtp_data = array();
        if($this->smtp && $this->smtp != 'null'){
            $smtp_array = explode("\1",$this->smtp);
            foreach($smtp_array as $data){
                $dataValue = explode("\2",$data);
                $this->smtp_data[$dataValue[0]] = $dataValue[1];
            }
        }

        # Parseo la metadata correspondiente al campo 'metadata'
        if($this->metadata == null){
            $this->parsed_metadata = null;
        }
        else{
            $this->parsed_metadata = json_decode($this->metadata,true);
            Denko::arrayUtf8Decode($this->parsed_metadata);
        }
    }

    /**
     * Restaura la metadata, que anteriromente fue parseada, en el DAO Sitio
     *
     * @access public
     * @return void
     */
    public function array2metadata(){

        # Convierto la metadata correspondiente al SMTP
        if(count($this->smtp_data) > 0){
            $smtp_data = array();
            foreach($this->smtp_data as $key => $value){
                $smtp_data[] = $key."\2".$value;
            }
            $this->smtp = implode("\1",$smtp_data);
        }
        else{
            $this->smtp = 'null';
        }

        # Convierto la metadata
        # Parseo la metadata correspondiente al campo 'metadata'
        if($this->parsed_metadata == null){
            $this->metadata = 'null';
        }
        else{
            Denko::arrayUtf8Encode($this->parsed_metadata);
            $this->metadata = json_encode($this->parsed_metadata);
        }
    }

    /**
     * Obtiene una valor de metadata
     *
     * @access public
     * @return mixed
     */
    public function getMetadataValue(){

        # Obtengo el nro de par�metros
        $num_args = func_num_args();

        # En caso que no haya par�metros, no retorno valor alguno
        if($num_args == 0){
            return null;
        }

        # Busco cual deber�a ser el �ndice que debo retornar
        $evalIndex = '';
        for($i = 0 ; $i < $num_args; $i++){
            $arg = func_get_arg($i);
            $evalIndex.='["'.trim($arg).'"]';
        }

        # Verifico si puedo obtener el valor de la configuraci�n
        eval('$metadata = isset($this->parsed_metadata'.$evalIndex.') ? $this->parsed_metadata'.$evalIndex.' : null;');

        if($metadata != null){
            return $metadata;
        }

        # En caso que pidan la metadata principal
        $arg0 = func_get_arg(0);
        if($num_args == 1){
            if(isset(InnyCore_Metadata::$defaultMetadataSitio[strtoupper($arg0)])){
                return InnyCore_Metadata::$defaultMetadataSitio[strtoupper($arg0)];
            }
            return null;
        }

        # En caso que pidan de alguna secci�n en particular
        switch(strtolower($arg0)){

            case 'dse':
                eval('$metadata = isset(InnyCore_Metadata::$defaultMetadataDSE'.$evalIndex.') ? InnyCore_Metadata::$defaultMetadataDSE'.$evalIndex.' : null;');
                return $metadata;

            case 'multilang':
                eval('$metadata = isset(InnyModule_Multilang::$metadata_settings'.$evalIndex.'["default"]) ? InnyModule_Multilang::$metadata_settings'.$evalIndex.'["default"] : null;');
                return $metadata;

            default:
                return null;
        }
    }

    /**
     * Retorna una configuraci�n de la metadata
     *
     * @param string $name nombre de la configuraci�n
     * @access public
     * @return string
     */
    public function getConfig($name){

        # En caso que ni siquiera existan par�metros
        if(!isset($this->parsed_metadata)){
            return null;
        }

        # En caso que la configuraci�n est� seteada
        $config_name = InnyCore_Utils::upper($name);
        if(isset($this->parsed_metadata[$config_name])){
            return $this->parsed_metadata[$config_name];
        }

        # Verifico si existe valor por defecto para esa configuraci�n
        if(isset(InnyCore_Metadata::$defaultMetadataSitio[$config_name])){
            return InnyCore_Metadata::$defaultMetadataSitio[$config_name];
        }

        # En caso que la configuraci�n no exista
        return null;
    }

    /**
     * Setea una configuraci�n en la metadata
     *
     * @param string $name nombre de la configuraci�n
     * @param string $value valor de la configuraci�n
     * @access public
     * @return void
     */
    public function setConfig($name,$value){
        if($this->parsed_metadata === null){
            $this->parsed_metadata = array();
        }
        $this->parsed_metadata[$name] = $value;
    }

    /**
     * Vac�a todas las configuraciones
     *
     * @access pubic
     * @return void
     */
    public function unsetConfigs(){
        unset($this->parsed_metadata);
        $this->parsed_metadata = null;
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                  SMTP                                  //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Obtiene un valor de la configuraci�n SMTP
     *
     * @param string $key nombre de la configuraci�n
     * @access public
     * @return mixed valor de la configuraci�n o NULL si la configuraci�n no existe
     */
    public function getSmtpValue($key){

        # Si el valor existe y tiene valor seteado, lo retorno
        $value = (isset($this->smtp_data[$key]) && trim($this->smtp_data[$key]) != '') ? $this->smtp_data[$key] : null;
        if($value != null){
            return ($key == 'protocol') ? strtolower($value) : $value;
        }

        # En caso que no tenga valor seteado, retorno el valor por default
        switch($key){
            case 'protocol_host':

                # En caso que la URL ya tenga el protocolo, no se lo pongo
                if(!empty($this->smtp_data['protocol']) && !empty($this->smtp_data['host'])){
                    return strtolower($this->smtp_data['protocol']).'://'.$this->smtp_data['host'];
                }
                if(empty($this->smtp_data['protocol']) && !empty($this->smtp_data['host'])){
                    return $this->smtp_data['host'];
                }

                # En caso que no tenga protocolo ni host, seteo el que tenga el
                # admin por defecto
                $protocol = strtolower(getConfig('SMTPMAIL_PROTOCOL'));
                $host     = getConfig('SMTPMAIL_HOST');
                return $protocol ? ($protocol.'://'.$host) : $host;

            case 'host':     return getConfig('SMTPMAIL_HOST');
            case 'protocol': return strtolower(getConfig('SMTPMAIL_PROTOCOL'));
            case 'port':     return getConfig('SMTPMAIL_PORT');
            case 'username': return getConfig('SMTPMAIL_USERNAME');
            case 'password': return getConfig('SMTPMAIL_PASSWORD');
            case 'fromname': return $this->nombre_sitio;
            default:         return null;
        }
    }

    /**
     * Setea un valor en la configuraci�n del SMTP
     *
     * @param string $key nombre de la configuraci�n
     * @param mixed $value valor de la configuraci�n
     * @access public
     * @return void
     */
    public function setSmtpValue($key,$value){
        $this->smtp_data[$key] = $value;
    }

    /**
     * Chequea los valores que deber�an estar completos de un sitio.
     * @return null|string
     */
    public function checkAlertValues(){
        $error=array();
        if ($this->smtp_data['host']=='local://sendmail'){
            if (empty($this->smtp['contacto']) || empty($this->smtp_data['fromname'])){
                $error[]='SMTP: Falta completar datos.';
            }
        }elseif (in_array('',$this->smtp_data)){
            $error[]='SMTP: Falta completar datos.';
        }
        if (empty($this->ga_codigo)){
            $error[]='Analitycs: Falta el c�digo.';
        }
        return count($error)>0?implode('<br/>',$error):null;
    }

    ////////////////////////////////////////////////////////////////////////////
    //                               LISTADOS                                 //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Obtiene todos los listados del sitio
     *
     * @access public
     * @return DataObjects_Listado listados del sitio
     */
    public function getListados(){
        $daoListado = DB_DataObject::factory('listado');
        $daoListado->whereAdd('
            id_sitio = '.$this->id_sitio
        );
        return $daoListado->find() ? $daoListado : null;
    }

    /**
     * Obtiene un DAO Listado, si es que pertenece al sitio
     *
     * @param integer $id_listado ID del DAO Listado
     * @param array $debug_backtrace debug_backtrace de la funci�n que invoca
     * @access public
     * @return DataObjects_Listado DAO Listado si existe y pertenece al sitio, NULL en caso contrario
     */
    public function getListado($id_listado,$debug_backtrace=null){

        $debug_backtrace = $debug_backtrace !== null ? $debug_backtrace : debug_backtrace();

        # Verifico que el Sitio exista en la DB
        if(!InnyCore_Dao::isId($this->id_sitio)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El ID del Sitio es nulo',$debug_backtrace);
            return null;
        }

        # Verifico que el listado exista en la DB
        $daoListado = InnyCore_Dao::getDao('listado',$id_listado);
        if(empty($daoListado)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El Listado con ID "'.$id_listado.'" no existe',$debug_backtrace);
            return null;
        }

        # Verifico que el listado pertenezca al sitio
        if($daoListado->id_sitio != $this->id_sitio){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El Listado "'.$daoListado->nombre.'" (ID "'.$id_listado.'") no pertenece al Sitio "'.$this->nombre_sitio.'" (ID "'.$this->id_sitio.'")',$debug_backtrace);
            return null;
        }

        # Retorno el DAO Listado
        return $daoListado;
    }

    /**
     * Retorna el listado con tal nombre, y que pertenezca al sitio
     *
     * @param string $nombre_listado nombre del listado
     * @access public
     * @return DataObjects_Listado
     */
    public function getListadoPorNombre($nombre_listado){

        # Verifico si el listado no est� cacheado
        if(!isset($GLOBALS[self::$cacheListadosPorNombre])){
            $GLOBALS[self::$cacheListadosPorNombre] = array();
            $GLOBALS[self::$cacheListadosPorNombre][$this->id_sitio] = array();
        }

        # Verifico si el listado ya est� cacheado
        if(!isset($GLOBALS[self::$cacheListadosPorNombre][$this->id_sitio][$nombre_listado])){
            $daoListado = DB_DataObject::factory('listado');
            $daoListado->whereAdd('
                nombre = \''.$nombre_listado.'\'
                and id_sitio = '.$this->id_sitio.'
            ');
            $GLOBALS[self::$cacheListadosPorNombre][$this->id_sitio][$nombre_listado] = ($daoListado->find(true) ? $daoListado : InnyCore_Dao::$null);
        }

        # En caso que el listado no exista, retorno NULL
        if($GLOBALS[self::$cacheListadosPorNombre][$this->id_sitio][$nombre_listado] == InnyCore_Dao::$null){
            return null;
        }

        # Si existe, retorno lo que est� en cache
        return $GLOBALS[self::$cacheListadosPorNombre][$this->id_sitio][$nombre_listado];
    }

    /**
     * Retorna un arreglo ordenado de los nombres de los listados pertenecientes al sitio
     *
     * @param boolean $ignorarListadoUsuarios indica si obtiene tambi�n el listado de usuarios
     * @access public
     * @return array nombre de los listados
     */
    public function getListadosPorNombre($ignorarListadoUsuarios=true,$ignorar_ocultos=true){
        $daoListado = DB_DataObject::factory('listado');
        $daoListado->id_sitio = $this->id_sitio;
        if($ignorarListadoUsuarios){
            $daoListado->whereAdd('
                nombre != \''.Inny::$registeredUsersListName.'\''
                .($ignorar_ocultos ? ' and admin_only = \'0\'' : '').'
            ');
        }
        $daoListado->find();
        $listados = array();
        while($daoListado->fetch()){
            $listados[$daoListado->id_listado] = strtolower($daoListado->getMetadataValue('nombre'));
        }
        asort($listados);
        return $listados;
    }

    /**
     * Retorna el listado de usuarios del sitio
     *
     * @access public
     * @return DataObjects_Listado
     */
    public function getListadoUsuariosRegistrados(){
        $daoListado = DB_DataObject::factory('listado');
        $daoListado->whereAdd('nombre = \''.(Inny::$registeredUsersListName).'\' and id_sitio = '.$this->id_sitio);
        return $daoListado->find(true) ? $daoListado : null;
    }

    /**
     * Crea el listado de usuarios registrados
     *
     * @access public
     * @return integer
     */
    public function crearListadoUsuariosRegistrados(){
        if($this->getListadoUsuariosRegistrados() === null){
            $daoListado = DB_DataObject::factory('listado');
            $daoListado->nombre = Inny::$registeredUsersListName;
            $daoListado->id_sitio = $this->id_sitio;
            return $daoListado->insert() ? true : false;
        }
        return true;
    }

    /**
     * Indica si el sitio posee un listado con ese nombre
     *
     * @param string $nombre_listado nombre del listado
     * @access public
     * @return boolean si el sitio posee un listado con ese nombre
     */
    public function perteneceListado($nombre_listado){
        if(!InnyCore_Dao::isId($this->id_sitio)){
            return false;
        }

        $daoListado = DB_DataObject::factory('listado');
        $daoListado->whereAdd('
            nombre = \''.InnyCore_Utils::lower($nombre_listado).'\'
            and id_sitio = '.$this->id_sitio.'
        ');
        return ($daoListado->find() > 0);
    }

    /**
     * Retorna el DAO Producto correspondiente al ID, y que pertenezca al sitio
     *
     *
     * @return DataObjects_Producto DAO Producto si existe el producto, o NULL en caso contrario
     */
    public function getProducto($id_producto){

        # Verifico que el sitio al menos haya sido insertado
        if(!InnyCore_Dao::isId($this->id_sitio)){
            return null;
        }

        # Obtengo el producto que tenga el ID
        $daoProducto = InnyCore_Dao::getDao('producto',$id_producto);

        # En caso que ni siquiera exista producto con ese ID, no retorno nada
        if($daoProducto == null){
            return null;
        }

        # Verifico que el producto pertenezca al Sitio
        $daoListado = DB_DataObject::factory('listado');
        $daoListado->whereAdd('
            id_listado = '.$daoProducto->id_listado.'
            and id_sitio = '.$this->id_sitio.'
        ');

        # En caso que el producto pertenezca, lo retorno. Sin�, entrego NULL
        return $daoListado->find() ? $daoProducto : null;
    }

    ////////////////////////////////////////////////////////////////////////////
    //                               DSE                                      //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Retorna si la categor�a pertenece al sitio
     *
     * @param integer $id_categoria ID de la categor�a
     * @access public
     * @return boolean
     */
    public function perteneceCategoria($id_categoria){
        $daoDseCategory = InnyCore_Dao::getDao('dse_category',$id_categoria,array('primary_key'=>'id_category'));
        return ($daoDseCategory->index1 == $this->id_sitio);
    }

    /**
     * Indica si el sitio soporta DSE
     *
     * @access public
     * @return boolean
     */
    public function soportaDSE(){
        $dse_settings = $this->getConfig('dse');
        return (isset($dse_settings['enabled']) && $dse_settings['enabled'] == '1');
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                                                        //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Obtiene los contenidos del sitio
     *
     * @access public
     * @return DataObjects_Configuracion contenidos del sitio
     */
    public function getContenidos(){
        $daoContenido = DB_DataObject::factory('configuracion');
        $daoContenido->whereAdd('
            indice1 = '.$this->id_sitio.'
        ');
        return $daoContenido->find() ? $daoContenido : null;
    }

    /**
     * Retorna la Cuenta Corriente a la que hace referencia la Tarea
     *
     * @access public
     * @return DataObjects_Ctacorriente
     */
    public function getCtaCorriente(){
        return InnyCore_Dao::getDao('ctacorriente',$this->id_ctacorriente);
    }

    /**
     * Obtiene las tareas relacionadas al sitio
     *
     * @access public
     * @return DataObjects_Tarea tareas relacionadas al sitio
     */
    public function getTareas(){
        $daoTarea = DB_DataObject::factory('tarea');
        $daoTarea->whereAdd('
            id_sitio = '.$this->id_sitio.'
        ');
        return $daoTarea->find() ? $daoTarea : null;
    }
}
################################################################################