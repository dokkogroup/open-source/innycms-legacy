<?php
require_once '../DAOs/AudDataObject.php';
require_once '../DAOs/Estado.php';
require_once '../dokkoMonitorLight/dokkoMonitorLight.php';
require_once '../commons/inny.core.php';

/**
 * Table Definition for tarea
 */
class DataObjects_Tarea extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tarea';                           // table name
    public $id_tarea;                        // int(10)  not_null primary_key unsigned auto_increment
    public $id_sitio;                        // int(10)  not_null multiple_key unsigned
    public $id_estado;                       // int(10)  not_null multiple_key unsigned
    public $id_empleado;                     // int(10)  multiple_key unsigned
    public $fecha_inicio;                    // datetime(19)  not_null binary
    public $fecha_fin;                       // datetime(19)  not_null binary
    public $fecha_creacion;                  // datetime(19)  not_null binary
    public $descripcion;                     // blob(65535)  not_null blob binary
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     * Se redefine el insert para agregar fechas y notificación de tareas
     *
     * @access public
     * @return integer
     */
    public function insert(){
        $this->fecha_creacion = date('Y-m-d H:i:s');
        $this->fecha_inicio = $this->id_estado == ESTADO_ENPROCESO ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00';
        $this->fecha_fin = $this->id_estado == ESTADO_FINALIZADO ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00';

        $insert = parent::insert();
        if($insert){
            $daoSitio = $this->getSitio();
        	dokkoMonitorLight::sendAlert('Pedido de -'.$daoSitio->nombre_sitio.'- (nro. '.$daoSitio->id_tarea.')','warning','ServiciosWeb');
        }
    	return $insert;
    }

    /**
     * Se redefine para setear las fechas en los cambios de estado
     *
     * @access public
     * @return integer number of rows affected | boolean FALSE on failure
     */
    public function update($dataObject = false){

        # Evaluando el cambio de estado, veo si modifico las fechas de la Tarea
        $daoSelfTarea = DB_DataObject::factory('tarea');
        $daoSelfTarea->get($this->id_tarea);
        if($this->id_estado == ESTADO_ENPROCESO && $daoSelfTarea->id_estado == ESTADO_NOCOMENZADO){
            $this->fecha_inicio = date('Y-m-d H:i:s');
        }
        elseif($this->id_estado == ESTADO_FINALIZADO && ($daoSelfTarea->id_estado == ESTADO_ENPROCESO || $daoSelfTarea->id_estado == ESTADO_ENPAUSA)){
            $this->fecha_fin = date('Y-m-d H:i:s');
        }

        # Retorno el update del parent
        return parent::update();
    }

    /**
     * Se redefine el delete para eliminar las relaciones con otras tablas
     *
     * @access public
     * @return integer|boolean
     */
    public function delete($useWhere = false){

        # Elimino las relaciones en la tabla 'tarea_temporal'
        $daoTareaTemporal = $this->getTareaTemporal();
        if(!empty($daoTareaTemporal)){
            while($daoTareaTemporal->fetch()){
                DFM::delete($daoTareaTemporal->id_temporal);
                $daoTareaTemporal->delete();
            }
        }

        # Elimino las relaciones en la tabla 'renglon'
        $daoRenglones = $this->getRenglones();
        if(!empty($daoRenglones)){
            while($daoRenglones->fetch()){
                $daoRenglones->delete();
            }
        }

        # Elimino los comentarios realizados en esta tarea
        $daoComentario = $this->getComentarios();
        if(!empty($daoComentario)){
            while($daoComentario->fetch()){
                $daoComentario->delete();
            }
        }

        # Por último invoco el delete de la clase parent
        return parent::delete();
    }

    /**
     * Retorna el Sitio al que hace referencia la Tarea
     *
     * @access public
     * @return DataObjects_Sitio
     */
    public function getSitio(){
        require_once '../commons/inny.core.php';
        return InnyCore_Dao::getDao('sitio',$this->id_sitio);
    }

    /**
     * Retorna el Empleado al que hace referencia la Tarea
     *
     * @access public
     * @return DataObjects_Empleado
     */
    public function getEmpleado(){
        require_once '../commons/inny.core.php';
        return InnyCore_Dao::getDao('empleado',$this->id_empleado);
    }

    /**
     * Retorna el Estado al que hace referencia la Tarea
     *
     * @access public
     * @return DataObjects_Estado
     */
    public function getEstado(){
        require_once '../commons/inny.core.php';
        return InnyCore_Dao::getDao('estado',$this->id_estado);
    }

    /**
     * Obtiene los comentarios realizados en esta Tarea
     *
     * @access public
     * @return DataObjects_Comentario
     */
    public function getComentarios(){
        if(!$this->id_tarea){
            return null;
        }
        $daoComentario = DB_DataObject::factory('comentario');
        $daoComentario->id_tarea = $this->id_tarea;
        return $daoComentario->find() ? $daoComentario : null;
    }

    /**
     * Obtiene los DAOs de la tabla 'tarea_temporal' relacionada con la tarea
     *
     * @access public
     * @return DataObjects_Tarea_temporal
     */
    public function getTareaTemporal(){
        if(!$this->id_tarea){
            return null;
        }
        $daoTareaTemporal = DB_DataObject::factory('tarea_temporal');
        $daoTareaTemporal->id_tarea = $this->id_tarea;
        return ($daoTareaTemporal->find())? $daoTareaTemporal : null;
    }

    /**
     * Retorna los DAOs de la tabla 'renglon' que corresponden a esta tarea
     *
     * @access public
     * @return DataObjects_Renglon
     */
    public function getRenglones(){
        $daoRenglon = DB_DataObject::factory('renglon');
        $daoRenglon->id_tarea = $this->id_tarea;
        return $daoRenglon->find() ? $daoRenglon : null;
    }

    /**
     * Obtiene el renglon relacionado con esta Tarea
     *
     * @access public
     * @return DataObjects_Renglon
     */
    public function getRenglon(){
        $daoRenglon = DB_DataObject::factory('renglon');
        $daoRenglon->id_tarea = $this->id_tarea;
        if($daoRenglon->find(true)){
            return $daoRenglon;
        }
        return null;
    }

    /**
     * Obtiene la Cuenta Corriente relacionada con esta Tarea
     *
     * @access public
     * @return DataObjects_Ctacorriente
     */
    public function getCtaCorriente(){
        $daoSitio = $this->getSitio();
        return $daoSitio->getCtaCorriente();
    }

    /**
     * Actualiza el monto de la tarea.
     *
     * @param integer $monto monto de la Tarea
     * @access public
     * @return void
     */
    public function updateMonto($monto){
        $daoRenglon = $this->getRenglon();
        if($daoRenglon){
            $daoRenglon->monto = $monto;
            $daoRenglon->update();
        }
        else{
            $daoRenglon = DB_DataObject::factory('renglon');
            $daoCtaCorriente = $this->getCtaCorriente();
            $daoRenglon->id_ctacorriente = $daoCtaCorriente->id_ctacorriente;
            $daoRenglon->id_tarea = $this->id_tarea;
            $daoRenglon->monto = $monto;
            $daoRenglon->insert();
        }
    }

    /**
     * Obtiene la información de los archivos
     *
     */
    public function getInfoArchivos(){

        # Verifico que la tarea exista en la DB
        if(!InnyCore_Dao::isId($this->id_tarea)){
            return null;
        }

        $daoTemporal = DB_DataObject::factory('temporal');
        $daoTemporal->selectAdd();
        $daoTemporal->selectAdd('id_temporal,metadata,size,name');
        $daoTemporal->whereAdd('
            id_temporal in (
                select id_temporal
                from tarea_temporal
                where id_tarea = '.$this->id_tarea.'
            )
        ');
        if(!$daoTemporal->find()){
            return null;
        }

        # Obtengo la información de los archivos adjuntos a la tarea
        $infoArchivos = array();
        while($daoTemporal->fetch()){
            $metadata = json_decode($daoTemporal->metadata,true);
            Denko::arrayUtf8Decode($metadata);
            $infoArchivos[] = array(
                'id_temporal' => $daoTemporal->id_temporal,
                'size'        => $daoTemporal->size,
                'filename'    => $daoTemporal->name,
                'mime'        => $metadata['mime']
            );
            unset($metadata);
        }

        # Retorno la información de los archivos
        return $infoArchivos;
    }
}
################################################################################