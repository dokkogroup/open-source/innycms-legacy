<?php
/**
 * Table Definition for tarea_temporal
 */
require_once '../DAOs/AudDataObject.php';

class DataObjects_Tarea_temporal extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tarea_temporal';                  // table name
    public $id_tarea;                        // int(10)  not_null primary_key multiple_key unsigned
    public $id_temporal;                     // int(10)  not_null primary_key multiple_key unsigned
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     *
     *
     */
    public function getTarea(){
        return InnyCore_Dao::getDao('tarea',$this->id_tarea);
    }

    /**
     *
     *
     */
    public function delete($useWhere = false){
        $id_temporal = $this->id_temporal;
        $delete = parent::delete();
        if($delete){
            DFM::delete($id_temporal);
        }
        return $delete;
    }
}
################################################################################