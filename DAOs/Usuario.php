<?php
/**
 * Table Definition for usuario
 */
require_once '../DAOs/AudDataObject.php';

define('USUARIO_HABILITADO',1);
define('USUARIO_DESHABILITADO',0);

class DataObjects_Usuario extends AudDataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'usuario';                         // table name
    public $id_usuario;                      // int(10)  not_null primary_key unsigned auto_increment
    public $id_sitio;                        // int(10)  not_null multiple_key unsigned
    public $username;                        // string(20)  not_null
    public $password;                        // string(100)  not_null
    public $nombre;                          // string(100)  not_null
    public $apellido;                        // string(100)  not_null
    public $direccion;                       // string(100)
    public $telefono;                        // string(100)
    public $email;                           // string(100)
    public $habilitado;                      // string(1)  not_null
    public $aud_ins_date;                    // datetime(19)  not_null binary
    public $aud_upd_date;                    // datetime(19)  not_null binary

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    /**
     *
     *
     */
    public function getDaoSitio(){
        return InnyCore_Dao::getDao('sitio',$this->id_sitio);
    }
}
################################################################################