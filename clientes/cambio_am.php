<?php
/**
 * Project: Inny Clientes
 * File: cambio_am.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

function validateTareaData(&$smarty,&$daoTarea,$action){

    # Verifico que la descripci�n no est� vac�a
    if(InnyCore_Utils::cleanWysiwygContent($_POST['descripcion']) == ''){
        Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'tarea_descripcion'));
    }

    # Chequeo los archivos adjuntos
    foreach($_FILES as $name => $filedata){
        $archivo_error = InnyCore_File::getErrorArchivoAdjunto($filedata);
        switch($archivo_error){
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE: Denko::addErrorMessage('archivo_errorTooBigToServer'); break;
            case UPLOAD_ERR_PARTIAL: Denko::addErrorMessage('archivo_errorOnlyPartUpload'); break;
            default: break;
        }
    }

    # En caso que haya errores
    if(Denko::hasErrorMessages()){
        return false;
    }

    # En caso que deba agregar la Tarea a la DB
    if($action == 'add'){
        $daoSitio = Inny_Clientes::getSitioLogueado();
        $daoTarea->id_estado = ESTADO_NOCOMENZADO;
        $daoTarea->id_sitio = $daoSitio->id_sitio;
        $daoTarea->id_empleado = 'null';
        $daoTarea->descripcion = $_POST['descripcion'];
        $id_tarea = $daoTarea->insert();
    }

    # Edito la tarea y la actualizo en la DB
    else{
        $daoTarea->descripcion = $_POST['descripcion'];
        $daoTarea->update();
        $id_tarea = $daoTarea->id_tarea;
    }

    # Si ha adjuntado archivos, los guardo en la tabla Temporal
    foreach($_FILES as $name => $value){
        if(!empty($_FILES[$name]['tmp_name'])){
            $daoTareaTemporal = DB_DataObject::factory('tarea_temporal');
            $daoTareaTemporal->id_tarea = $id_tarea;
            $daoTareaTemporal->id_temporal = DFM::setFromfile($_FILES[$name]['tmp_name']);
            $daoTareaTemporal->insert();
            InnyCore_File::actualizarInfoArchivo(
                $daoTareaTemporal->id_temporal,
                array(
                    'name' => $_FILES[$name]['name'],
                    'metadata' => array('mime' => $_FILES[$name]['type']),
                    'size' => $_FILES[$name]['size']
                )
            );
        }
    }

    # Me redirijo a la p�gina de cambios y muestro el mensaje de ok
    Denko::redirect(basename($_SERVER['PHP_SELF']).'?action='.($action == 'add' ? 'addok' : 'editok'));
}

################################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que el Inny no est� en Tiny Mode
Inny_Clientes::verificarTinyMode($smarty);

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('add','edit','cancel','addok','editok','delfile'));

# En caso de borrar un archivo adjunto
$action = InnyCore_Utils::getParamValue('action');
if($_SERVER['REQUEST_METHOD'] == 'GET' && $action == 'delfile'){

    $id_temporal = InnyCore_Utils::getParamValue('id_temporal');
    Inny_Clientes::verificarTemporalValido($smarty,$id_temporal);
    $daoTareaTemporal = DB_DataObject::factory('tarea_temporal');
    $daoTareaTemporal->id_temporal = $id_temporal;
    $daoTareaTemporal->find(true);
    $daoTarea = $daoTareaTemporal->getTarea();
    if($daoTarea->id_estado != ESTADO_NOCOMENZADO){
        Inny_Common::errorCritico('tarea','tarea_errorEliminarArchivo');
    }
    $daoTareaTemporal->delete();
    Denko::redirect($_SERVER['HTTP_REFERER']);
}

# Verifico que la tarea sea v�lida
$daoTarea = DB_DataObject::factory('tarea');
if(in_array($action,array('edit','cancel'))){
    $id_tarea = InnyCore_Utils::getParamValue('id_tarea');
    $daoTarea = Inny_Common::verificarDao($smarty,'tarea',$id_tarea);
    $daoTarea->fetch();
    if($daoTarea->id_estado != ESTADO_NOCOMENZADO){
        Inny_Common::errorCritico('tarea','tarea_errorTareaComenzada');
    }
}

# En caso de agregar/editar una tarea
if($_SERVER['REQUEST_METHOD'] == 'POST' && ($action == 'add' || $action == 'edit')){
    validateTareaData($smarty,$daoTarea,$action);
}

switch($action){

    # En caso de agregar una tarea
    case 'add':
        $smarty->display('cambio_am.tpl');
        break;

    # En caso de editar una tarea
    case 'edit':
        $smarty->assign('daoTarea',$daoTarea);
        $smarty->display('cambio_am.tpl');
        break;

    # En caso de anular una tarea
    case 'cancel':
        $daoTarea->id_estado = ESTADO_ANULADO;
        $daoTarea->update();
        Denko::redirect($_SERVER['HTTP_REFERER']);
        break;

    # Si la tarea se agreg�/edit� bien, muestro el mensaje de ok
    case 'addok':
    case 'editok':
        $smarty->display('cambio_ok.tpl');
        break;
    #
    default: break;
}

################################################################################