CKEDITOR.editorConfig = function(config){

    config.language = 'es';

    config.toolbar = 'InnyToolbar';
    config.toolbar_InnyToolbar = [
        ['Source','-','Bold','Italic','Underline','Strike','NumberedList','BulletedList','-','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Link','Unlink']
    ];
};