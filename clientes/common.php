<?php
/**
 * Project: Inny Clientes
 * File: common.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once '../commons/inny.clientes.php';

################################################################################
function get_urlback() {

    # Obtengo la URL por default
    $default = 'index.php';

    # Obtengo el referer
    $referer = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;

    # En caso que no haya referer, retorno la URL por default
    if($referer == null){
    	return $default;
    }

    # Obtengo el host del referer
    preg_match('@^(?:http://)?([^/]+)@i',$referer,$coincidencias);

    # En caso que el referer provenga del mismo host
    $url = ($_SERVER['HTTP_HOST'] == $coincidencias[1]) ? $referer : $default;
    $urlArray = explode('error',$url);
    $urlArray = explode('send',$urlArray[0]);
    return $urlArray[0];
}