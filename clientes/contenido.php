<?php
/**
 * Project: Inny Clientes
 * File: contenido.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

# Eliminar contenido v�a AJAX
if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['deleteContent'])){

    # Obtengo el contenido
    $id_contenido = InnyCore_Utils::getParamValue('id_contenido');
    $contenido = new Contenido($_GET['id_contenido']);

    # Verifico que el contenido tenga permisos para eliminarse su valor
    if($contenido->tienePermiso('B')){

        # Elimino el valor del contenido
    	$contenido->unsetMetadata('parametros','filename');
    	$contenido->unsetMetadata('parametros','mime');
    	$contenido->valor = 'NULL';
    	$res = $contenido->update();

        # Elimino el archivo de la DB
        $id_temporal = InnyCore_Utils::getParamValue('id_temporal');
    	$daoTemporal = DB_DataObject::factory('temporal');
    	$daoTemporal->get($id_temporal);
    	$daoTemporal->delete();

        # Env�o la respuesta al request
    	echo 'response = "'.($res ? 'OK' : 'MAL').'";';
    	exit(0);
    }

    # En caso que haya error
    echo 'response = "MAL";';
    exit(1);
}

################################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('edit','view','delete'));

# Verifico que la configuraci�n sea v�lida
$id_configuracion = InnyCore_Utils::getParamValue('id_configuracion');
Inny_Clientes::verificarConfiguracionValida($smarty,$id_configuracion);

# Obtengo el Contenido de la DB
$contenido = new Contenido($id_configuracion);

////////////////////////////////////////////////////////////////////////////////

# Verifico los permisos

$action = InnyCore_Utils::getParamValue('action');
switch($action){

    case 'view':
        if(!$contenido->tienePermiso('V')){
            Inny_Common::errorCritico('contenido','contenido_errorParametroPermisoVer');
        }
        break;

    case 'edit':
        if(!$contenido->tienePermiso('M')){
            Inny_Common::errorCritico('contenido','contenido_errorParametroPermisoModificar');
        }
        break;

    case 'delete':
        if(!$contenido->tienePermiso('B')){
            Inny_Common::errorCritico('contenido','contenido_errorParametroPermisoEliminar');
        }
        break;
}

////////////////////////////////////////////////////////////////////////////////

$linkBackContenidos = new Inny_LinkBackContenidos();
$linkBack = $linkBackContenidos->getLinkBack();

////////////////////////////////////////////////////////////////////////////////

# En caso que haya variables en POST, verfifico si son v�lidas y actualizo el DAO
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    # Arreglo con el valor
    $data = array();

    # Obtengo el tipo de contenido
    $tipo = $contenido->getMetadataValue('tipo');

    # En caso que el contenido sea del tipo archivo
    if(Inny::isSupportedFileType($tipo)){
        $data['valor'] = $_FILES['valor'];
    }

    # En caso que el contenido sea texto
    else{

        # En caso que sea multilenguaje
        if($contenido->esMultilenguaje()){
            $data['valor'] = array();
            $daoSitio  = Inny_Clientes::getSitioLogueado();
            $languages = $daoSitio->getMetadataValue('multilang','languages');
            foreach($languages as $language_code => $language_label){
                $data['valor'][$language_code] = $_POST['valor_'.$language_code];
            }
        }

        # En caso que sea texto simple
        else{
            $data['valor'] = $_POST['valor'];
        }
    }

    # Seteo que debo mostrar los mensajes de error
    InnyCore_Contenido::mostrarMensajesDeError();

    # Verifico si se puede editar el valor de este contenido. Si se puede, lo edito
    if(InnyCore_Contenido::editarContenido($contenido->nombre,$data)){
        Denko::redirect($linkBack);
    }
}

////////////////////////////////////////////////////////////////////////////////

switch($action){

    case 'view':
    case 'edit':

        # Asigno el contenido al template
        $smarty->assign('contenido',$contenido);

        # Asigno el Sitio al template
        $smarty->assign('daoSitio',Inny_Clientes::getSitioLogueado());

        # Asigno el link de volver al listado al template
        $smarty->assign('linkBack',$linkBack);

        # Muestro el template
        $smarty->display($action == 'view' ? 'contenido_view.tpl' : 'contenido_edit.tpl');

        break;

    # En caso que haya que eliminarse
    case 'delete':
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            $contenido->deleteValue();
            Denko::redirect($linkBack);
        }
        break;
}

################################################################################