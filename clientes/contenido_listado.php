<?php
/**
 * Project: Inny Clientes
 * File: contenido_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Seteo si debo mostrar los contenidos privados
Inny_Clientes::setMostrarContenidosPrivados();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico si debo redirigirme a la url cacheada del listado de contenidos
$linkBackContenidos = new Inny_LinkBackContenidos();
$linkBackContenidos->verifyRedirect();

# Seteo la url del listado, para no perder los parametros GET.
$linkBackContenidos->setLinkBack();

# Asigno las variables al template
$daoSitio = Inny_Clientes::getSitioLogueado();
$smarty->assign('daoSitio',$daoSitio);
$smarty->assign('daolisterQuery','
    indice1 = '.$daoSitio->id_sitio.(Inny_Clientes::mostrarContenidosPrivados() ? '' : ' and indice2 = 0').'
');

# Muestro el template
$smarty->display('contenido_listado.tpl');

################################################################################