<?php
/**
 * Project: Inny Clientes
 * File: dse_category_abm.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que pueda administrar categor�as
Inny_Clientes::verificarAdminstracionDeCategorias($smarty);

# En caso que haya que eliminar una categor�a
if(!empty($_GET['action']) && $_GET['action'] == 'delete'){

    # Verifico que la categor�a sea v�lida
    $daoSitio = Inny_Clientes::getSitioLogueado();
    $id_categoria = !empty($_GET['id_category']) ? $_GET['id_category'] : null;
    if(!Inny_DSE::verificarCategoriaValida($daoSitio->id_sitio,$id_categoria)){
        Inny_Common::mostrarErrorCritico($smarty);
    }

    # En caso que todo est� bien, asigno el DAO de la categor�a al template
    $smarty->assign('daoDseCategory',Inny_DSE::getCategoria($id_categoria));
}

# Muestro el template
$smarty->display((!empty($_GET['action']) && $_GET['action'] == 'delete') ? 'dse_category_confirm.tpl' : 'dse_category_abm.tpl');

################################################################################