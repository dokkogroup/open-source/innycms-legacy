<?php
/**
 * Project: Inny Clientes
 * File: generar_manual_sitio.php
 *
 * @copyright 2007-2010 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
require_once '../mpdf/mpdf.php';

###############################################################################
function generarPDF(&$smarty,$filename){

    $mpdf = new mPDF('', 'A4', 10, 'Arial', 0, 0, 33, 25, 0, 0);
    $mpdf->pagenumPrefix = utf8_encode('P�gina ');
    $mpdf->pagenumSuffix = '';
    $mpdf->nbpgPrefix = ' de ';
    $mpdf->nbpgSuffix = '              ';
    $smarty->assign('numero_pagina','{PAGENO}{nbpg}');   
    
    $tapa = utf8_encode($smarty->fetch('manual_pdf_tapa.tpl'));
    $html = utf8_encode($smarty->fetch('manual_pdf.tpl'));
    $header = utf8_encode($smarty->fetch('manual_pdf_header.tpl'));
    $footer = utf8_encode($smarty->fetch('manual_pdf_footer.tpl'));
    
    
    $mpdf->WriteHTML($tapa);
    $mpdf->AddPage('','NEXT-ODD','1','','','','','','','','', 'header','header');
    $mpdf->DefHTMLHeaderByName('header',$header);
    $mpdf->SetHTMLHeaderByName('header','',1);
    $mpdf->SetHTMLFooter($footer);
	$mpdf->WriteHTML($html);
    
    
	$mpdf->Output($filename, 'I');
}

###############################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

$tipos = array( 'integer'=>'Num�rico Entero',
                'richtext'=>'Texto Enriquecido',
				'text'=>'Texto Corto',
				'flash'=>'Flash',
				'file'=>'Archivo',
				'image'=>'Imagen',
                'date'=>'Fecha',
				'float'=>'Num�rico Real',
				'checkbox'=>'Chequeable',
				'radio'=>'Opci�n',
				'email'=>'Email',
				'select'=>'Selecci�n',
				'textarea'=>'Texto Largo',
                'sound'=>'Sonido',
               );
$daoSitio = Inny_Clientes::getSitioLogueado();
$smarty->assign('daoSitio',$daoSitio);
$smarty->assign('tipos',$tipos);

$filename = $daoSitio->nombre_sitio.'.pdf';
$filename = str_replace(' ','_',strtolower($filename));
generarPDF($smarty,$filename);

exit;
################################################################################