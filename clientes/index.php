<?php
/**
 * Project: Inny Clientes
 * File: index.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
include_once 'common.php';

################################################################################

function validateLogin(&$smarty){

    # Obtengo las variables de POST
    $username = strtolower(trim($_POST['username']));
    $password = $_POST['password'];

    # Verifico que haya ingresado el username
    if($username == ''){
        Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo'=>'login_username'));
    }

    # Verifico que haya ingresado el password
    if($password == ''){
        Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo'=>'login_password'));
    }

    # En caso que no haya errores, verifico que los datos sean v�lidos
    if(!Denko::hasErrorMessages()){

        # En caso que sea uno de los usuarios de una p�gina
		if(stripos($username, '@') !== false){
			$flag = false;
			$usuarioSitio = explode('@', $username);

            # Verifico que el Sitio exista
			$daoSitio = DB_DataObject::factory('sitio');
			$daoSitio->username = $usuarioSitio[1];

			# En caso que el sitio no exista, lanzo un mensaje de error
			if(!$daoSitio->find(true)){
				Denko::addErrorMessage('login_errorUsuarioPaswordIncorrecto');
				$flag = true;
			}

            # En caso que el sitio exista, verifico que exista el usuario
			else{

                $daoUsuario = DB_DataObject::factory('usuario');
    			$daoUsuario->username = $usuarioSitio[0];
    			$daoUsuario->id_sitio = $daoSitio->id_sitio;
    			$daoUsuario->habilitado = '1';

    			# En caso que el usuario exista, verifico su password
                if($daoUsuario->find(true)){

                    # Si el password es incorrecto, lanzo un mensaje de error
                    if($daoUsuario->password != $password && !$flag){
                        Denko::addErrorMessage('login_errorUsuarioPaswordIncorrecto');
                    }
                }

                # Si el usuario no existe, lanzo un mensaje de error
                else{
                    Denko::addErrorMessage('login_errorUsuarioPaswordIncorrecto');
                }
            }
		}

        # En caso que sea el �nico usuario de la p�gina
        else{

	        # Verifico que el usuario est� en la DB
	        $daoSitio = DB_DataObject::factory('sitio');
	        $daoSitio->username = $username;

	        # En caso que el usuario NO est� en la DB, lanzo el error
	        if(!$daoSitio->find()){
	            Denko::addErrorMessage('login_errorUsuarioPaswordIncorrecto');
	        }

            # Verifico que el password sea correcto
	        else{
	            $daoSitio->fetch();
	            if($daoSitio->password != $password){
	                Denko::addErrorMessage('login_errorUsuarioPaswordIncorrecto');
	            }
	        }
		}
    }

    # En caso que est� todo OK
	if(!Denko::hasErrorMessages()){

        # Seteo las variables en sesion y la cookie en el navegador
        Inny_Clientes::setSitioLogueado($daoSitio);
        setcookie('SW_USERNAME',$username,time()+31536000);

        # Actualizo la metadata del sitio
        if(!InnyCore_Metadata::update($daoSitio->id_sitio)){
            $smarty->display('metadata_warnings.tpl');
            exit(0);
        }

        # Redirijo a la p�gina principal
        Denko::redirect('misitio.php');
    }

    # Si hubo errores, retorno false
    return false;
}

################################################################################

# En caso de logout
if(isset($_GET['logout'])){
    Denko::sessionDestroy();
    Denko::sessionStart();
}

# Si el usuario ya est� logueado, lo redirijo a la p�gina principal
if(Inny_Clientes::isLoggedUser()){
    Denko::redirect('misitio.php');
}

# Creo la instancia de Smarty y cargo el outputfilter para enviar los includes en el header
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# En caso que haya variables en POST, las chequeo
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    validateLogin($smarty);
}

# Muestro el template
$smarty->display('login.tpl');

################################################################################