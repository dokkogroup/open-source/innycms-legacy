<?php

/**
 *
 */
require_once '../commons/inny.common.php';
require_once '../commons/inny.clientes.php';

################################################################################

/**
 * Headers para que el IE no cahee los resultados
 */
header("Last-Modified: ".gmdate('D, d M Y H:i:s')." GMT");
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

################################################################################

# En caso que las acciones sean por POST
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(!empty($_POST['action'])){
        switch($_POST['action']){

            # En caso de agregar una categor�a
            case 'add_category':

                $daoDseCategory = DB_DataObject::factory('dse_category');
                $daoDseCategory->name = urldecode($_POST['name']);
                $daoDseCategory->index1 = $_POST['id_sitio'];

                # En caso que deba agregar una vista
                if($_POST['level'] == '1'){
                    $daoDseCategory->id_view = 'null';
                    $daoDseCategory->id_parent = 'null';
                    $id_category = $daoDseCategory->insert();
                    $daoDseCategory->id_view = $id_category;
                    $daoDseCategory->update();
                }

                # Sin�, agrego la categor�a de manera com�n y corriente
                else{
                    $daoDseCategory->id_parent = $_POST['id_parent'];
                    $id_category = $daoDseCategory->insert();
                }

                echo '{"id_category":"'.$id_category.'"}';
                exit(0);

            # En caso de cargar las subcategor�as de una categor�a
            case 'load_categories':
                echo json_encode(Inny_DSE::getSubcategorias($_POST['id_sitio'],(!empty($_POST['id_parent']) ? $_POST['id_parent'] : null),true));
                exit(0);

            # En cualquier otro caso, retorno NULL
            default: echo 'null'; exit(0);
        }
    }
}

# En caso que las acciones sean por GET
if(!empty($_GET['action'])){
    switch($_GET['action']){
        case 'category_delete':

            # Incluyo los archivos
            require_once 'common.php';
            require_once '../commons/inny.dse.php';

            # Verifico que la categor�a sea v�lida
            $daoSitio = Inny_Clientes::getSitioLogueado();
            $id_categoria = !empty($_GET['id_category']) ? $_GET['id_category'] : null;
            if(!Inny_DSE::verificarCategoriaValida($daoSitio->id_sitio,$id_categoria)){
                $smarty = new Smarty();
                Inny_Common::initialize($smarty);
                Inny_Common::mostrarErrorCritico($smarty);
            }

            # En caso que todo est� bien, elimino la categor�a y todos sus hijos
            $daoDseCategory = Inny_DSE::getCategoria($id_categoria);
            $daoDseCategory->deleteSubTree();
            Denko::redirect('dse_category_abm.php');
            break;
    }
}

################################################################################