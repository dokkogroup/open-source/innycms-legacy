function dk_setAutoCompEvents(id_input){
    document.getElementById(id_input).focused = false;
    document.getElementById(id_input).clicked = false;

    document.getElementById('dkAutoComp_list'+id_input).focused = false;
    document.getElementById(id_input).onfocus = function(){
        if(this.focused == false){
            this.focused = true;
            document.getElementById('dkAutoComp_div'+id_input).style.display='block';
            document.getElementById('dkAutoComp_maindiv'+id_input).style.zIndex = '100';
        }
    }
    document.getElementById(id_input).onclick = function(){
        if(this.focused == true && this.clicked == false){
            this.clicked = true;
        }else if(this.clicked == true){
            document.getElementById('dkAutoComp_div'+id_input).style.display='none';
            document.getElementById('dkAutoComp_maindiv'+id_input).style.zIndex = '0';
        }
    }
    document.getElementById(id_input).onblur = function(){
        if(document.getElementById('dkAutoComp_list'+id_input).focused == false){
            document.getElementById('dkAutoComp_div'+this.id).style.display='none';
            document.getElementById('dkAutoComp_maindiv'+id_input).style.zIndex = '0';
            this.focused = false;
            this.clicked = false;
        }
    }
    document.getElementById(id_input).onchange = function(){
        document.getElementById('dkAutoComp_div'+id_input).style.display='none';
        document.getElementById('dkAutoComp_maindiv'+id_input).style.zIndex = '100';
    }
    document.getElementById('dkAutoComp_list'+id_input).onmouseover = function(){
        this.style.cursor = 'pointer';
        this.focused = true;
    }
    document.getElementById('dkAutoComp_list'+id_input).onmouseout = function(){
        this.style.cursor = 'auto';
        this.focused = false;
    }
    var elements = document.getElementById('dkAutoComp_list'+id_input).getElementsByTagName('LI');
    for(i = 0; i < elements.length; i++){
        elements[i].onmouseover = function(){
            this.style.backgroundColor='#316ac5';
            this.style.color='#ffffff';
        }
        elements[i].onmouseout = function(){
            this.style.backgroundColor='white';
            this.style.color='#000000';
        }
        elements[i].onclick = function(){
            document.getElementById(id_input).value = this.innerHTML;
            document.getElementById('dkAutoComp_div'+id_input).style.display='none';
            document.getElementById('dkAutoComp_maindiv'+id_input).style.zIndex = '0';
            document.getElementById('dkAutoComp_list'+id_input).focused = false;
            document.getElementById(id_input).focused = false;
            document.getElementById(id_input).clicked = false;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////