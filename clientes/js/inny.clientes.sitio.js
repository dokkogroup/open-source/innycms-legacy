/**
 *
 *
 */
var Inny_Sitio = {

    /**
     *
     */
    init : function(settings){

        // Configuraciones
        this.settings = settings;

        // Focus
        if (this.getAdminConfig() == 1){
            document.getElementById('nombre_sitio').focus();
            document.getElementById('nombre_sitio').select();
        }
        else{
            document.getElementById("apellido").focus();
            document.getElementById("apellido").select();
        }

    },

    /**
     *
     *
     */
    getAdminConfig : function(){
        return this.settings.admin_configs;
    },

    /**
     *
     */
    validate : function(){

        var validacionDatosComunes = (

            // Verifico que el campo 'Apellido' tenga valor seteado
            Inny_ValidateForm.verificarCampoRequerido("apellido") &&

            // Verifico que el campo 'Nombres' tenga valor seteado
            Inny_ValidateForm.verificarCampoRequerido("nombre_encargado") &&

            // Verifico que el campo 'Tel�fono' tenga valor seteado
            Inny_ValidateForm.verificarCampoRequerido("telefono") &&

            // Verifico que el campo 'Direcci�n' tenga valor seteado
            Inny_ValidateForm.verificarCampoRequerido("direccion") &&

            // Verifico que si el campo 'E-mail' tiene valor seteado, sea v�lido
            Inny_ValidateForm.verificarCampoEmail("email",false)

        );

        /////////////////////// CONFIGURACIONES DE ADMIN ///////////////////////

        var validacionDatosAdmin = true;
        if(validacionDatosComunes && this.getAdminConfig() == 1){

            var validacionDatosAdmin = (

                // Verifico que el campo 'Sitio' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("nombre_sitio") &&

                // Verifico que el campo 'URL' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("url") &&

                // SMTP
                // Verifico que el campo 'E-mail de Contacto' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("smtp_contacto") &&

                // Verifico que el campo 'E-mail de Contacto' tenga formato v�lido
                Inny_ValidateForm.verificarCampoEmail("smtp_contacto",true) &&

                // Verifico que el campo 'SMTP FromName' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("smtp_fromname") &&

                // Verifico que el campo 'SMTP Host' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("smtp_host") &&

                // Verifico que el campo 'SMTP Username' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("smtp_username") &&

                // Verifico que el campo 'SMTP Password' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("smtp_password") &&

                // Verifico que el campo 'SMTP Protocol' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("smtp_protocol") &&

                // Verifico que el campo 'SMTP Port' tenga valor seteado
                Inny_ValidateForm.verificarCampoRequerido("smtp_port")

            );
        }

        // Retorno el resultado de la validaci�n
        return (validacionDatosComunes && validacionDatosAdmin);
    },

    /**
     *
     *
     */
    validarCambioPassword : function(){
        return (

            // Verifico que el campo 'Contrase�a Actual' tenga valor seteado
            Inny_ValidateForm.verificarCampoRequerido("password") &&

            // Verifico que el campo 'Nueva Contrase�a' y 'Confirmar Nueva Contrase�a'
            // est�n seteados y sean equivalentes
            Inny_ValidateForm.verificarCamposNuevoPassword("newpass","confirm_newpass")
        );
    }
};
