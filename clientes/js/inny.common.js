////////////////////////////////////////////////////////////////////////////////
//                               btnEnter                                     //
////////////////////////////////////////////////////////////////////////////////

// JavaScript Document
function mouseOver (object) {
	object.style.cursor='pointer';
	object.style.backgroundImage='url(images/btnenter_over.gif)';
	object.style.backgroundRepeat='repeat-x';
	object.style.color='#1474B8';
}
function mouseOut (object) {
	object.style.backgroundImage='url(images/btnenter_normal.gif)';
	object.style.backgroundRepeat='repeat-x';
	object.style.color='#000000';
}
// ---------------------------------------------------------------------
var ACTUAL_SECTION = null;
var SELECTED_TD = null;
function init (actualSection, selectedTd) {
	document.getElementById(actualSection).style.display='block';
	document.getElementById(selectedTd).className='btnSelected';
	ACTUAL_SECTION = actualSection;
	SELECTED_TD = selectedTd;
}
// ---------------------------------------------------------------------
function btnMouseOver (object, idArrow) {
	if (object.className == 'btnSelected'){
		object.style.cursor='default';
	}else{
		if (idArrow != '') {
		document.getElementById (idArrow).style.display='block';
		}
		object.style.cursor='pointer';
		object.style.backgroundImage='url(images/btnmenu_over.gif)';
		object.style.backgroundRepeat='repeat-x';
		object.style.color='#000000';
	}
}
function btnMouseOut (object, idArrow) {
	if (object.className == 'btnSelected'){
		object.style.cursor='default';
	}else{
		if (idArrow != '') {
		document.getElementById (idArrow).style.display='none';
		}
		object.style.backgroundImage='url(images/btnenter_normal.gif)';
		object.style.backgroundRepeat='repeat-x';
		object.style.color='#1474B8';
	}
}

////////////////////////////////////////////////////////////////////////////////
//                                 LISTADO                                    //
////////////////////////////////////////////////////////////////////////////////
/**
 *
 *
 */
var Inny_Listado = {

    /**
     *
     *
     */
    cambiarDeListado : function(){
    	var selectListado = document.getElementById('listados');
    	var id_listado = selectListado.options[selectListado.selectedIndex].value;
    	document.location.href = 'producto_listado.php?id_listado='+id_listado;
    }
};


