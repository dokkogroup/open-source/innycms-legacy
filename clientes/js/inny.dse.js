var Inny_DSE = {

    /**
     * Arreglo de prefijos de los IDs de los elementos HTML
     * @final
     */
    prefixes : {
        'select' : 'dse_category_level',
        'add_input' : 'dse_category_addinput',
        'add_button' : 'dse_category_addbutton',
        'delete_button' : 'dse_category_deletebutton',
        'select_button' : 'dse_category_selectbutton'
    },

    /**
     * URL para las conexiones AJAX
     * @final
     */
    ajaxUrl : 'inny.dse_category.php',

    /**
     * URL donde muestra la confirmaci�n de eliminar categor�a
     * @final
     */
    deleteUrl : 'dse_category_abm.php',

    /**
     * Separador de los breadcrumbs
     * @final
     */
    breadcrumbSeparator : '�',

    /**
     * Inicializa la clase
     *
     * @param {Array} json hash con seteos
     */
    init : function(json){
        this.abmMode = json['abmMode'];
        this.id_sitio = json['id_sitio'];
        this.maxLevelsCategories = json['maxLevels'] ;
        this.speechEmpty = json['speech']['empty'];
        this.speechLoading = json['speech']['loading'];
        this.speech = json['speech'];
    },

    /**
     * Carga las categor�as y sus subcategor�as
     *
     * @param {Number} level nivel del grupo de categor�as
     */
    loadCategories : function(level){

        var parentLevel = level - 1;

        // Verifico que el Grupo de Categor�as padre tenga categor�as
        var parentGroup = this.getCategoryGroup(parentLevel);
        if(parentGroup == null || this.isEmptyCategoryGroup(parentGroup)){
            return false;
        }

        // Obtengo el ID de la categor�a padre
        var parentIndex = (parentGroup.selectedIndex > -1) ? parentGroup.selectedIndex : 0;
        var parentCategoryId = parentGroup.options[parentIndex].value;

        // Creo el objecto xmlhttp para el AJAX
        var xmlhttp = initAjax();
        var sendString = 'action=load_categories&id_sitio='+this.id_sitio+'&id_parent='+parentCategoryId;
        xmlhttp.open('POST',this.ajaxUrl,true);

        // setRequestHeader must be called only after an open() call.
        // (see relevant comments in the Mozilla source)
        xmlhttp.setRequestHeader('If-Modified-Since','Thu, 1 Jan 1970 00:00:00 GMT');
        xmlhttp.setRequestHeader('Cache-Control','no-cache');
        xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        xmlhttp.setRequestHeader('Content-length',sendString.length);
        xmlhttp.setRequestHeader('Connection','close');

        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4){


                // ======== En caso que se haya establecido conexi�n ======== //
                if (xmlhttp.status == 200){

                    eval('var json_categories = '+xmlhttp.responseText+';');
                    var group = Inny_DSE.getCategoryGroup(level);

                    // En caso que haya categor�as que agregar
                    if(!Inny_DSE.isEmptyHash(json_categories)){

                        // Vac�o el grupo de categor�as
                        group.options.length = 0;

                        // Cargo las categor�as
                        for(var i in json_categories){
        	               group.options[group.options.length] = new Option(Inny_DSE.unescapeJsonString(json_categories[i]),i);
                        }

                        // Marco como seleccionada la 1er categor�a
                        group.selectedIndex = 0;

                        // Cargo el grupo que viene
                        return Inny_DSE.loadCategories(level+1);
                    }

                    // En caso que no haya subcategor�as que agregar
                    else{
                        Inny_DSE.enableAllCategoryGroups();
                    }
                }

                // ============ En caso que no haya conexi�n ================ //
                else{
                    Inny_DSE.displayAjaxError();
                }
            }
        }

        // Esto tiene que ir ac� por el IE6
        xmlhttp.send(sendString);
    },

    /**
     * Carga las subcategor�as de una categor�a.
     * Previamente deshabilita los inputs para evitar conflictos
     *
     * @param {Number} level nivel del grupo de categor�as
     */
    loadSubcategories : function(level){
        var nextLevel = level + 1;
        for(var i = 1; i <= this.maxLevelsCategories; i++){
            var group = this.getCategoryGroup(i);
            if(i >= nextLevel){
                group.options.length = 0;
                group.options[0] = new Option(this.speechLoading,'null');
            }
            group.disabled = true;
            if(this.abmMode){
                this.enableCategoryInputs(i,false);
            }
        }
        this.enableLeafCategoryButton(false);
        this.loadCategories(nextLevel);
    },

    /**
     * Obtiene un grupo de categor�as
     *
     * @param {Number} level nivel del grupo de categor�as
     * @return grupo de categor�as
     * @type {HTMLSelectElement}
     */
    getCategoryGroup : function(level){
        return document.getElementById(this.prefixes['select']+level);
    },

    /**
     * Callback que se utiliza para ordenar un grupo de categor�as
     * Compara dos textos de option de categor�a
     *
     * @param {Array} a arreglo con valor y texto de un option de categor�a
     * @param {Array} b arreglo con valor y texto de un option de categor�a
     * @return {Number}
     */
    sortCategoryGroupCallback : function(a,b){
        return (a.text < b.text) ? -1 : ((a.text > b.text) ? 1 : 0);
    },

    /**
     *
     * @access public
     * @return void
     */
    sortCategoryGroup : function(categoryGroup){

        // Paso los options de las categor�as al arreglo ordenable
        var sortable = [];
        for(var i = 0; i < categoryGroup.options.length; i++){
            sortable.push(categoryGroup.options[i]);
        }

        // Ordeno el arreglo donde envi� los options con las categor�as
        sortable.sort(this.sortCategoryGroupCallback);

        // Vac�o el grupo de categor�as
        categoryGroup.options.length = 0;

        // Paso los options ordenados al grupo de categor�as
        for(var i = 0; i < sortable.length; i++){
            categoryGroup.options[i] = sortable[i];
        }
    },

    /**
     * Averigua si un grupo de categor�as est� vac�o
     *
     * @param {Number} level nivel del grupo de categor�as
     * @return NULL si la categor�a no existe, o boolean en otro caso
     * @type {Boolean}
     * @type {NULL}
     */
    isEmptyCategoryGroup : function(categoryGroup){
        if(categoryGroup == null){
            return null;
        }
        return (categoryGroup.options[0].value == 'null');
    },

    /**
     * Prepara el Grupo de Categor�as
     *
     * @param {Number} level nivel del grupo de categor�as
     * @return {Boolean}
     */
    setupCategoryGroup : function(level){
        var group = this.getCategoryGroup(level);
        if(group == null){
            return false;
        }
        if(group.options.length > 0){
            group.selectedIndex = 0;
            group.disabled = false;
        }
        else{
            group.disabled = true;
        }
        return true
    },

    /**
     * Retorna si un arreglo hash est� vac�o
     *
     * @param {Array} hash arreglo hash
     * @return {Boolean}
     */
    isEmptyHash : function(hash){
        for(var i in hash){
            return false;
        }
        return true;
    },

    /**
     * Habilita los grupos de categor�as y sus inputs
     */
    enableAllCategoryGroups : function(){
        for(var i = 1; i <= this.maxLevelsCategories; i++){
            var group = this.getCategoryGroup(i);
            if(group.options[0].value != 'null'){
                group.disabled = false;
                if(this.abmMode == true){
                    this.enableCategoryInputs(i,true);
                }
            }
            else{
                group.options[0].text = this.speechEmpty;
                group.disabled = true;
                if(this.abmMode == true && i > 1 && !this.isEmptyCategoryGroup(this.getCategoryGroup(i-1))){
                    this.enableCategoryInputs(i,true);
                    document.getElementById(this.prefixes['delete_button']+i).disabled = true;
                }
            }

            if(this.abmMode){
                if(i == 0){
                    this.enableCategoryInputs(i,true);
                }
            }
            else{
                var selectButton = this.getSelectCategoryButton(i)
                if(selectButton != null){
                    selectButton.disabled = this.isEmptyCategoryGroup(group);
                }
            }
        }
        this.enableLeafCategoryButton(true);
    },

    /**
     * Deshabilita todos los grupos de categor�as y sus inputs
     */
    disableAll : function(){
        for(var i = 1; i <= this.maxLevelsCategories; i++){
            for(var prefix in this.prefixes){
                var element = document.getElementById(this.prefixes[prefix]+i);
                if(element != null){
                    element.disabled = true;
                }
            }
        }
    },

    /**
     * Decodifica los caracteres especiales a los verdaderos
     *
     * @param {String} string
     * @return {String}
     */
    unescapeJsonString : function(string){
        return(unescape(string.replace(/\+/g,' ')));
    },

    /**
     * Agrega una categor�a al grupo de categor�as
     *
     * @param {Number} level nivel del grupo de categor�as
     * @return {Boolean}
     */
    addCategory : function(level){

        // Verifico que el nombre de la categor�a sea v�lido
        var add_input = document.getElementById(this.prefixes['add_input']+level);
        var category_name = ((add_input != null) ? add_input.value.trim() : '');
        if(category_name.length == 0){
            alert(this.speech['errorRequiredCategoryName']);
            return false;
        }

        // Verifico que el nombre no exista previamente
        var group = this.getCategoryGroup(level);
        var lowercaseName = category_name.toLowerCase();
        for(var i = 0; i < group.options.length; i++){
            if(group.options[i].text.toLowerCase() == lowercaseName){
                alert(this.speech['errorExistsCategoryName']);
                return false;
            }
        }

        // Desabilito todos los componentes
        this.disableAll();

        // Obtengo el select padre
        var parentLevel = level - 1;
        var parentGroup = this.getCategoryGroup(level - 1);

        // Creo el objecto xmlhttp para el AJAX
        var xmlhttp = initAjax();
        var sendString = 'level='+level+'&action=add_category&id_sitio='+this.id_sitio+'&name='+escape(category_name)+'&id_view='+this.getIdView(level)+'&id_parent='+((parentGroup != null) ? parentGroup.options[parentGroup.selectedIndex].value : 'null');
        xmlhttp.open('POST',this.ajaxUrl,true);

        // setRequestHeader must be called only after an open() call.
        // (see relevant comments in the Mozilla source)
        xmlhttp.setRequestHeader('If-Modified-Since','Thu, 1 Jan 1970 00:00:00 GMT');
        xmlhttp.setRequestHeader('Cache-Control','no-cache');
        xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        xmlhttp.setRequestHeader('Content-length',sendString.length);
        xmlhttp.setRequestHeader('Connection','close');

        // Defino que tiene que hacer al insertar la categor�a
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4){

                // En caso que haya agregado la categor�a
                if (xmlhttp.status == 200){
                    eval('var json = '+xmlhttp.responseText+';');
                    var group = Inny_DSE.getCategoryGroup(level);
                    if(Inny_DSE.isEmptyCategoryGroup(group)){
                        group.options.length = 0;
                    }
                    group.options[group.options.length] = new Option(category_name,json['id_category']);
                    if(group.options.length == 1){
                        group.options[0].selected = true;
                        group.selectedIndex = 0;
                    }
                    Inny_DSE.sortCategoryGroup(group);
                    Inny_DSE.enableAllCategoryGroups();
                }

                // En caso de no poder establecer conexi�n
                else{
                    Inny_DSE.displayAjaxError();
                }
            }
        }

        // Esto tiene que ir ac� sin� el IE no lo interpreta bien
        xmlhttp.send(sendString);
    },

    /**
     * Elimina una categor�a
     *
     * @param {Number} level nivel del grupo de categor�as
     * @param {Number} level nivel del grupo de categor�as
     */
    deleteCategory : function(level){
        var group = this.getCategoryGroup(level);
        if(group.selectedIndex < 0){
            alert(this.speech["errorSeleccionarCategoriaParaEliminar"]);
            return false;
        }
        Denko.redirect(this.deleteUrl+'?action=delete&id_category='+group.options[group.selectedIndex].value);
    },

    /**
     * Habilita/deshabilita los inputs para agregar/eliminar una categor�a
     *
     * @param {Number} level nivel del grupo de categor�as
     * @param {Boolean} enable indica si se habilita [true] o deshabilita [false]
     */
    enableCategoryInputs : function(level,enable){
        document.getElementById(this.prefixes['add_input']+level).disabled = !enable;
        document.getElementById(this.prefixes['add_button']+level).disabled = !enable;
        document.getElementById(this.prefixes['delete_button']+level).disabled = !enable;
    },

    /**
     * Muestra el box de confirmaci�n en caso que se pierda la conexi�n por AJAX
     */
    displayAjaxError : function(){
        if(confirm(this.speech['errorAjaxConexion'])){
            window.location.reload();
        }
    },

    /**
     *
     */
    getIdView : function(level){
        if(level == 1){
            return 'null';
        }
        var group = this.getCategoryGroup(1);
        return group.options[group.selectedIndex].value;
    },

    ////////////////////////////////////////////////////////////////////////////

    enableLeafCategoryButton : function(enable){
        var leafCategoryButton = document.getElementById('dse_category_select_leaf');
        if(leafCategoryButton != null){
            leafCategoryButton.disabled = !enable;
        }
    },

    /**
     *
     */
    getSelectedCategories : function(){
        return document.getElementById('dse_category_selecteds');
    },

    /**
     *
     */
    getSelectCategoryButton : function(level){
        return document.getElementById(this.prefixes['select_button']+level);
    },


    /**
     *
     *
     * @param {HTMLSelectElement} categoryGroup Grupo de categor�as
     * @param {Number} level nivel del grupo de categor�as
     * @return true en caso de agregar la categor�a. false en caso contrario
     * @type {Boolean}
     */
    addSelectedCategory : function(categoryGroup,level){

        // Verifico que la categor�a no est� previamente seleccionada
        var selectedCategory = categoryGroup.options[categoryGroup.selectedIndex];
        if(this.existsSelectedCategory(selectedCategory.value)){
            alert(this.speech['errorCategoriaYaSeleccionada']);
            return false;
        }

        //
        var selectedsCategories = this.getSelectedCategories();
        if(this.isEmptyCategoryGroup(selectedsCategories)){
            selectedsCategories.options.length = 0;
        }

        //
        var breadcrumbs = this.getParentBreadcrumbText(level);
        selectedsCategories.disabled = false;
        selectedsCategories.options[selectedsCategories.options.length] = new Option(breadcrumbs + this.breadcrumbSeparator+ ' ' + selectedCategory.text,selectedCategory.value);
        this.sortCategoryGroup(selectedsCategories);
        return true;
    },

    /**
     *
     *
     * @param {Number} level nivel del grupo de categor�as
     * @return true si puede agregarla al conjunto de categor�as seleccionadas. false en caso contrario
     * @type {Boolean}
     */
    selectCategory : function(level){

        // Verifico que el grupo de categor�as no est� vac�o
        var group = this.getCategoryGroup(level);
        if(this.isEmptyCategoryGroup(group)){
            return false;
        }

        // Verifico que tenga alguna categor�a seleccionada
        if(group.selectedIndex < 0){
            return false;
        }

        // Agrego la categor�a seleccionada
        return this.addSelectedCategory(group,level);
    },

    /**
     *
     */
    selectLeafCategory :function(){
        for(var i = this.maxLevelsCategories; i >= 1; i--){
            var group = this.getCategoryGroup(i);
            if(group!= null && !this.isEmptyCategoryGroup(group) && group.selectedIndex >= 0){
                return this.addSelectedCategory(group,i);
            }
        }
        return false;
    },

    /**
     *
     */
    existsSelectedCategory : function(id_category){
        var selectedsCategories = this.getSelectedCategories();
        if(this.isEmptyCategoryGroup(selectedsCategories)){
            return false;
        }

        for(var i = 0; i < selectedsCategories.options.length; i++){
            if(selectedsCategories.options[i].value == id_category){
                return true;
            }
        }
        return false;

    },

    /**
     * Remueve categor�as del listado de categor�as seleccionadas
     *
     * @return cantidad de categor�as que se remueven
     * @type {Number}
     */
    removeSelectedCategories : function(){
        var selectedsCategories = this.getSelectedCategories();
        if(this.isEmptyCategoryGroup(selectedsCategories)){
            return 0;
        }

        // Escojo las categor�a que no fueron seleccionadas para removerse
        var unselectedCategories = [];
        for(var i = 0; i < selectedsCategories.options.length; i++){
            if(!selectedsCategories.options[i].selected){
                unselectedCategories.push(selectedsCategories.options[i]);
            }
        }

        // Restauro las categor�as no seleccionadas para eliminarse al listado de seleccionadas
        selectedsCategories.options.length = 0;

        // En caso que a�n haya categor�as en el listado de categor�as seleccionadas
        if(unselectedCategories.length > 0){
            for(var i = 0; i < unselectedCategories.length; i++){
                selectedsCategories.options[i] = unselectedCategories[i];
            }
        }

        // En caso que el listado de categor�as seleccionadas est� vac�o
        else{
            selectedsCategories.options[0] = new Option(this.speech['noSelectedCategories'],'null');
            selectedsCategories.disabled = true;
        }

        // Retorno la cantidad de elementos que quedaron en el listado de categor�as seleccionadas
        return unselectedCategories.length;
    },

    /**
     * Remueve todas categor�as del listado de categor�as seleccionadas
     */
    removeAllSelectedCategories : function(){
        var selectedsCategories = this.getSelectedCategories();
        if(!this.isEmptyCategoryGroup(selectedsCategories)){
            selectedsCategories.options.length = 0;
            selectedsCategories.options[0] = new Option(this.speech['noSelectedCategories'],'null');
            selectedsCategories.disabled = true;
        }
    },

    /**
     * Retorna los breadcrums padre de una categor�a, en forma de texto
     *
     * @param {Number} level nivel de la categor�a
     * @return breadcrums padres en forma de texto de una categor�a
     * @type {String}
     */
    getParentBreadcrumbText : function(level){
        if(level <= 1){
            return '';
        }
        var breadcrumbs = '';
        for(var i = 1; i < level; i++){
            var group = this.getCategoryGroup(i);
            breadcrumbs = breadcrumbs + this.breadcrumbSeparator + ' ' + group.options[group.selectedIndex].text + ' ';
        }
        return breadcrumbs;
    },

    /**
     *
     */
    onSubmit : function(){
        var selectedCategories = this.getSelectedCategories();
        for(var i = 0; i < selectedCategories.options.length; i++){
            selectedCategories.options[i].selected = true;
        }
        return true;
    }

};