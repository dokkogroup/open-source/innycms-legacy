var Inny_ValidateForm = {

    /**
     *
     *
     */
    cargarSpeechs : function(speechs){
        this.speechs = speechs;
    },

    /**
     *
     *
     */
    alertarCampoRequerido : function(id_campo){
        try{
            document.getElementById(id_campo).focus();
            document.getElementById(id_campo).select();
        }
        catch(error){}
        alert(
            this.speechs.message["error_campoRequerido"].replace(
                "%nombre_campo",
                this.speechs.input[id_campo].capitalize()
            )
        );
        return false;
    },

    /**
     *
     *
     */
    alertarCampoNoValido : function(id_campo){
        document.getElementById(id_campo).focus();
        document.getElementById(id_campo).select();
        alert(
            this.speechs.message["error_campoNoValido"].replace(
                "%nombre_campo",
                this.speechs.input[id_campo].capitalize()
            )
        );
        return false;
    },

    /**
     *
     *
     */
    verificarCampoRequerido : function(id_campo){
        var campo = document.getElementById(id_campo).value.trim();
        return (campo.length == 0) ? this.alertarCampoRequerido(id_campo) : true;
    },

    /**
     *
     *
     */
    verificarCampoEmail : function(id_campo,multiple){
        var email = document.getElementById(id_campo).value.trim();
        if(email.length > 0){

            // En caso que el campo soporte m�ltiples emails
            if(multiple == true){
                var emails = email.split(',');
                for(var i = 0; i < emails.length; i++){
                    var email = emails[i].trim();
                    if(email.length > 0 && !hasEmailFormat(email)){
                        return this.alertarCampoNoValido(id_campo);
                    }
                }
            }

            // En caso que el campo sea s�lo de un email
            else{
                if(!hasEmailFormat(email)){
                    return this.alertarCampoNoValido(id_campo);
                }
            }
        }

        // En caso que el campo sea v�lido
        return true;
    },

    /**
     *
     *
     */
    verificarCamposNuevoPassword : function(id_nueva,id_confirmar){

        // Verifico que los campos para la nueva contrase�a est�n seteados
        var verificacionCamposRequeridos = (
            this.verificarCampoRequerido(id_nueva) &&
            this.verificarCampoRequerido(id_confirmar)
        );
        if(!verificacionCamposRequeridos){
            return false;
        }

        // Verifico que los valores sean iguales
        var valorNueva = document.getElementById(id_nueva).value.trim();
        var valorConf = document.getElementById(id_confirmar).value.trim();

        if(valorNueva != valorConf){
            document.getElementById(id_nueva).focus();
            document.getElementById(id_confirmar).focus();
            document.getElementById(id_nueva).select();
            alert(
                this.speechs.message["error_camposIguales"].replace(
                    "%campo_nueva",
                    this.speechs.input[id_nueva].capitalize()
                ).replace(
                    "%campo_confirmar",
                    this.speechs.input[id_confirmar].capitalize()
                )
            );
            return false;
        }

        // En caso que todo est� OK
        return true;
    },

    /**
     *
     *
     */
    verificarCampoWysiwygRequerido : function(id_campo){
        return true;
        /*
        var contenido = document.getElementById('wysiwyg'+id_campo).contentWindow.document.body.innerHTML;
        var cleanContenido = contenido.replace(/<([^ ]+)>/g,'').replace(/\s<([^ ]+)>/g,'').replace(/\s+/g,'').replace(/&nbsp;/g,'');
        return (cleanContenido.length == 0) ? this.alertarCampoRequerido(id_campo) : true;
        */
    }

};