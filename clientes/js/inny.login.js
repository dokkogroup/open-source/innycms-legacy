/**
 *
 *
 */
var Inny_Login = {

    /**
     *
     *
     */
    init : function(){
	   document.getElementById("username").focus();
	   document.getElementById("username").select();
    },

    /**
     *
     *
     */
    validar : function (){
        return (
            Inny_ValidateForm.verificarCampoRequerido("username") &&
            Inny_ValidateForm.verificarCampoRequerido("password")
        );
    }
};