////////////////////////////////////////////////////////////////////////////////
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

var INNY_ACTION_MOVE_UP = 1;
var INNY_ACTION_MOVE_DOWN = 2;
var INNY_SORTABLE_FILES = 1;
var INNY_FIXED_FILES = 2;

var Inny_Producto = {

    /**
     *
     *
     */
    fileInputNamePrefix : "archivo",

    /**
     *
     *
     */
    trClasses : ['file_content','file_data','file_actions'],

    /**
     *
     *
     */
    rowFixedPrefix : "fixedFiles_",

    /**
     *
     *
     */
    rowSortablePrefix : "sortableFiles_",

    /**
     *
     *
     */
    tableSortablesId : 'tableSortableFiles',

    /**
     *
     *
     */
    tableFixedId : 'tableFixedFiles',

    /**
     *
     *
     */
    searchable : false,

    /**
     *
     *
     */
    relacionable : false,

    /**
     *
     *
     */
    settings : null,

    /**
     *
     *
     */
    init : function(settings){
        this.settings = settings;
    },

    /**
     *
     *
     */
    setSettings : function(key,value){
        this.settings[key] = value;
    },

    /**
     *
     */
    setSearchable : function(bool){
        this.searchable = bool;
    },

    /**
     *
     *
     */
    setRelacionable : function(boolean){
        this.relacionable = boolean;
    },

    /**
     *
     */
    validate : function(){
        return (this.settings != null && this.settings.contador_cantidad > 0) ? InnyProducto_Contador.validate() : true;
    },

    /**
     *
     *
     */
    onSubmit : function(){
        if(this.validate()){
            var validate = false;

            // En caso que sea un listado buscable, lo verifico
            if(this.searchable && !Inny_DSE.onSubmit()){
                return false;
            }

            // En caso que sea un listado relacionable, lo verifico
            if(this.relacionable && !InnyCardinal.onSubmit()){
                return false;
            }

            // Si todo sali� bien
            return true;
        }
        return false;
    },

    /**
     *
     */
	displayCSVForm : function(){
        var csv_form = document.getElementById('csv_form');
        csv_form.style.display = (csv_form.style.display == 'block')? 'none' : 'block';
    },

    /**
     *
     *
     */
    moveUp : function(id){
        this._move(id,INNY_ACTION_MOVE_UP);
    },

    /**
     *
     *
     */
    moveDown : function(id){
        this._move(id,INNY_ACTION_MOVE_DOWN);
    },

    /**
     *
     *
     */
    _move : function(id_archivo,dir){

        var tableSortables = this._getSortableTable();

        // Obtengo el �ndice
        var index = this._getSortableFileIndex(id_archivo);

        // En caso que el �ndice no exista en la tabla o no pueda subirse de posici�n
        if(index == null || (index == 0 && dir == INNY_ACTION_MOVE_UP)){
            return;
        }

        // Obtengo el otro �ndice con cual tengo que intercambiar el orden
        var otherIndex = (dir == INNY_ACTION_MOVE_UP ? (index - 1) : (index + 1));

        // Verifico que no deba intercambiar el orden con un archivo vac�o
        if(tableSortables.rows[otherIndex] == null || tableSortables.rows[otherIndex].id == this.rowSortablePrefix+"none"){
            return;
        }
        var xmlhttp = initAjax();
        xmlhttp.open("GET","producto_am.php?action=ajax_"+(dir == INNY_ACTION_MOVE_UP ? 'up' : 'down')+"&id_archivo="+id_archivo,true);
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){
                    eval('var response = "'+xmlhttp.responseText+'";');
                    if(response == "OK"){

                        // Clono los contenidos
                        var previousContent = Inny_Producto._cloneFileTDsData(tableSortables.rows[otherIndex]);
                        var actualContent = Inny_Producto._cloneFileTDsData(tableSortables.rows[index])

                        // Vac�o los tds
                        while(tableSortables.rows[index].cells.length > 0){
                            tableSortables.rows[otherIndex].deleteCell(0);
                            tableSortables.rows[index].deleteCell(0);
                        }

                        //
                        for(var i = 0; i < previousContent.length; i++){
                            tableSortables.rows[index].insertCell(i);
                            tableSortables.rows[index].cells[i].className = Inny_Producto.trClasses[i];
                            for(var j = 0; j < previousContent[i].length; j++){
                                tableSortables.rows[index].cells[i].appendChild(previousContent[i][j]);
                            }
                        }

                        //
                        for(var i = 0; i < actualContent.length; i++){
                            tableSortables.rows[otherIndex].insertCell(i);
                            tableSortables.rows[otherIndex].cells[i].className = Inny_Producto.trClasses[i];
                            for(var j = 0; j < actualContent[i].length; j++){
                                tableSortables.rows[otherIndex].cells[i].appendChild(actualContent[i][j]);
                            }
                        }

                        //
                        var actualRowId = tableSortables.rows[index].id;
                        tableSortables.rows[index].id = tableSortables.rows[otherIndex].id;
                        tableSortables.rows[otherIndex].id = actualRowId;
                    }
                }

                // En caso de error de conexi�n
                else{

                }
            }
        }

        // IE SUCKS: no mover esto de ac�
        xmlhttp.send(null);
    },

    /**
     *
     *
     */
    _cloneFileTDsData : function(row){
        var contents = [];
        for(var i = 0; i < row.cells.length; i++){
            var td_contents = [];
            for(var j = 0; j < row.cells[i].childNodes.length; j++){
                td_contents.push(row.cells[i].childNodes[j].cloneNode(true));
            }
            contents.push(td_contents);
        }
        return contents;
    },

    /**
     *
     *
     */
    _getSortableFileIndex : function(id_archivo){
        return this._getFileIndex(id_archivo,INNY_SORTABLE_FILES);
    },

    /**
     *
     *
     */
    _getFixedFileIndex : function(id_archivo){
        return this._getFileIndex(id_archivo,INNY_FIXED_FILES);
    },

    /**
     *
     *
     */
    _getFileIndex : function(id_archivo,mode){
        switch(mode){
            case INNY_FIXED_FILES:
                var table = this._getFixedTable();
                var prefix = this.rowFixedPrefix;
                break;
            case INNY_SORTABLE_FILES:
                var table = this._getSortableTable();
                var prefix = this.rowSortablePrefix;
                break;
            default: break;
        }
        for(var i = 0; i < table.rows.length; i++){
            if(table.rows[i].id == prefix+id_archivo){
                return i;
            }
        }
        return null;

    },

    /**
     *
     *
     */
    _getFixedTable : function(){
        return document.getElementById(this.tableFixedId);
    },

    /**
     *
     *
     */
    _getSortableTable : function(){
        return document.getElementById(this.tableSortablesId);
    },

    /**
     *
     *
     */
    deleteSortableFile : function(id_archivo){
        var tableSortables = this._getSortableTable();
        var index = this._getSortableFileIndex(id_archivo);

        // En caso que el �ndice no exista el �ndice
        if(index == null){
            return;
        }

        if(confirm(this.settings.speechs.confirmDelete)){
            var xmlhttp = initAjax();
            xmlhttp.open("GET","producto_am.php?action=ajax_delete&id_archivo="+id_archivo,true);
            xmlhttp.onreadystatechange = function(){
                if (xmlhttp.readyState == 4){
                    if(xmlhttp.status == 200){
                        var response = null;
                        eval('var response = "'+xmlhttp.responseText+'";');
                        if(response == "OK"){
                            tableSortables.deleteRow(index);
                            Inny_Producto._attachSortableFileEmptyRow();

                            // Actualizo los nombres de los inputs de los archivos
                            var countFixedFiles = document.getElementById(Inny_Producto.tableFixedId).rows.length;
                            for(var i = 0; i < tableSortables.rows.length; i++){
                                for(var j = 0; j < tableSortables.rows[i].cells[1].childNodes.length; j++){
                                    if(tableSortables.rows[i].cells[1].childNodes[j].nodeName.toLowerCase() == "input"){
                                        tableSortables.rows[i].cells[1].childNodes[j].setAttribute("name",Inny_Producto.fileInputNamePrefix+(countFixedFiles+i+1));
                                        break;
                                    }
                                }
                            }
                        }

                        // En caso que haya error en el delete
                        else{
                        }
                    }

                    // En caso que haya un error de conexi�n
                    else{

                    }
                }
            }

            // IE SUCKS: no mover esto de ac�
            xmlhttp.send(null);
        }
    },

    /**
     *
     *
     */
    _attachSortableFileEmptyRow : function(){
        var tableSortables = this._getSortableTable();
        tableSortables.insertRow(tableSortables.rows.length);
        var row_index = tableSortables.rows.length-1;
        tableSortables.rows[row_index].id = this.rowSortablePrefix+"none";

        // Inserto las celdas del TR
        for(var i = 0; i < 3; i++){
            tableSortables.rows[row_index].insertCell(i);
            tableSortables.rows[row_index].cells[i].className = this.trClasses[i];
        }

        // Reseteo el contenido
        this._resetFileContent(INNY_SORTABLE_FILES,row_index);
    },

    /**
     *
     *
     */
    deleteFixedFile : function(id_archivo){
        var fixedTable = this._getFixedTable();

        // En caso que ni siquiera haya archivos que puedan eliminarse
        if(fixedTable == null){
            return;
        }

        // Busco en que �ndice de la tabla est� el archivo que deseo eliminar
        var index = this._getFixedFileIndex(id_archivo);

        // Si ni siquiera lo encuentro, no hago nada
        if(index == null){
            return;
        }

        // Primero confirmo si realmente desea eliminar el archivo
        if(confirm(this.settings.speechs.confirmDelete)){
            var xmlhttp = initAjax();
            xmlhttp.open("GET","producto_am.php?action=ajax_delete&id_archivo="+id_archivo,true);
            xmlhttp.onreadystatechange = function(){
                if (xmlhttp.readyState == 4){
                    if(xmlhttp.status == 200){
                        var response = null;
                        eval('var response = "'+xmlhttp.responseText+'";');
                        if(response == "OK"){
                            Inny_Producto._resetFileContent(INNY_FIXED_FILES,index);
                        }

                        // En caso que haya error en el delete
                        else{
                        }
                    }

                    // En caso que haya un error de conexi�n
                    else{

                    }
                }
            }

            // IE SUCKS: no mover esto de ac�
            xmlhttp.send(null);
        }
    },

    /**
     *
     *
     */
    _resetFileContent : function(where,index){
        var table = (where == INNY_FIXED_FILES ? this._getFixedTable() : this._getSortableTable());

        // Vac�o todo el contenido referente al archivo
        for(var i = 0; i < 3; i++){
            while(table.rows[index].cells[i].hasChildNodes()){
                table.rows[index].cells[i].removeChild(table.rows[index].cells[i].firstChild);
            }
        }

        var archivo_numero = index + (where == INNY_FIXED_FILES ? 0 : this._getFixedTable().rows.length) + 1;
        var archivo_tipo = this.settings.filesInfo[archivo_numero-1].tipo;

        // Agrego el contenido de la 1er celda
        element = new Image();
        element.src = "images/list"+archivo_tipo+"_off.gif";
        table.rows[index].cells[0].appendChild(element);

        // Agrego el contenido de la 2da celda
        var span = document.createElement('span');
        span.className = "filetype";
        span.appendChild(document.createTextNode(this.settings.filesInfo[archivo_numero-1].nombre));
        table.rows[index].cells[1].appendChild(span);
        table.rows[index].cells[1].appendChild(document.createElement('br'));
        var input = document.createElement('input');
        input.setAttribute('type','file');
        input.setAttribute('name','archivo'+archivo_numero);
        table.rows[index].cells[1].appendChild(input);
        table.rows[index].cells[1].appendChild(document.createElement('br'));
        table.rows[index].cells[1].appendChild(document.createElement('br'));

        // Agrego el contenido de la 3era celda
        table.rows[index].cells[2].appendChild(document.createElement('br'));
    }
};


/**
 * Clase para el manejo de los contadores de un producto
 *
 * @author Dokko Group
 */
var InnyProducto_Contador = {

    /**
     * Inicializa la clase
     *
     * @param {Array} settings seteos para la inicializaci�n
     */
    init : function(settings){
        this.settings = settings;
    },

    /**
     * Incrementa el valor de un contador
     *
     * @param {Integer} numero_contador n�mero de contador
     */
    stepUp : function(numero_contador){
        this.setValueContador(numero_contador,this.getValueContador(numero_contador) + this.settings.config[numero_contador-1].parametros.step);
    },

    /**
     * Decrementa el valor de un contador
     *
     * @param {Integer} numero_contador n�mero de contador
     */
    stepDown : function(numero_contador){
        this.setValueContador(numero_contador,this.getValueContador(numero_contador) - this.settings.config[numero_contador-1].parametros.step);
    },

    /**
     * Reseta el valor de un contador
     *
     * @param {Integer} numero_contador n�mero de contador
     */
    reset : function(numero_contador){
        this.setValueContador(numero_contador,this.settings.config[numero_contador-1].parametros["default"]);
    },

    /**
     * Retorna el valor de un contador
     *
     * @param {Integer} numero_contador n�mero de contador
     * @type Integer
     * @return valor de un contador
     */
    getValueContador : function(numero_contador){
        var value = parseInt(document.getElementById("contador_numero"+numero_contador).value);
        return !isNaN(value) ? value : this.settings.config[numero_contador-1].parametros["default"];
    },

    /**
     * Setea un valor en un contador
     *
     * @param {Integer} numero_contador n�mero de contador
     * @param {Integer} value valor del contador
     */
    setValueContador : function(numero_contador,value){
        document.getElementById("contador_numero"+numero_contador).value = value;
    },

    /**
     * Verifica los valores de los contadores
     *
     * @type Boolean
     * @return validaci�n de los valores de los contadores
     */
    validate : function(){
        for(var i = 0; i < this.settings.config.length; i++){

            var input = document.getElementById("contador_numero"+(i+1));
            var contador_valor = input.value.trim();

            // Verifico que el valor del contador est� seteado
            if(contador_valor.length == 0){
                alert(this.settings.speechs.requiredValue.replace("%nombre",this.settings.config[i].nombre));
                input.focus();
                input.select();
                return false;
            }

            // Verifico que el valor sea un entero v�lido
            else if(isNaN(contador_valor)){
                alert(this.settings.speechs.invalidFormat.replace("%nombre",this.settings.config[i].nombre));
                input.focus();
                input.select();
                return false;
            }
        }
        return true;
    },

    /**
     *
     *
     */
    getForm : function(){
        return document.getElementById("producto_am_form");
    }
};

////////////////////////////////////////////////////////////////////////////////
//                            CARDINALIDADES                                  //
////////////////////////////////////////////////////////////////////////////////

var LISTADO_CON_RELACION = 111;
var LISTADO_SIN_RELACION = 222;
var RESULTADOS_POR_PAGINA = 15;

var InnyCardinal = {

    /**
     *
     *
     */
    listadoPrefix : "innyCardinalListadoTabla",

    /**
     *
     *
     */
    listadoContainerPrefix : "innyCardinalListadoContainer",

    /**
     *
     *
     */
    listadosCardinalesCargados : [],

	/**
	*
	*
	*/
    init : function(settings){
        this.settings = settings;
        Inny_Producto.setRelacionable(true);
        for(var nombre_listado in this.settings.listados){
            this.listadosCardinalesCargados.push({"listado":nombre_listado,"cargado":false});
        }
        this.mostrarSolapaRelacionListados(this.listadosCardinalesCargados[0].listado);
        document.onkeypress = this.searchOnKeyPress;
    },

    /**
     *
     *
     */
    vaciarElementoDom : function(elemento){
        while(elemento.hasChildNodes()){
            elemento.removeChild(elemento.firstChild);
        }
    },

    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    getListado : function(nombre_listado,cual){
        return document.getElementById(this.listadoPrefix + (cual == LISTADO_CON_RELACION ? "Con" : "Sin") + "Rel_" + nombre_listado);
    },

    /**
     *
     *
     */
    getListadoContainer : function(nombre_listado,cual){
        return document.getElementById(this.listadoContainerPrefix + (cual == LISTADO_CON_RELACION ? "Con" : "Sin") + "Rel_" + nombre_listado);
    },

    /**
     *  NUEVA FUNCION
     *
     */
    getListadoPaginador : function(nombre_listado){
        return document.getElementById(this.listadoPrefix + "Paginador_" + nombre_listado);
    },

    /**
    *  NUEVA FUNCION
    *
    */
    getListadoPaginadorContainer : function(nombre_listado){
    	return document.getElementById(this.listadoContainerPrefix + "Paginador_" + nombre_listado);
    },
    
    /**
     *
     *
     */
    getPalabraBusqueda : function(nombre_listado,cual){
        return document.getElementById("innyCardinalSearchInput" + (cual == LISTADO_CON_RELACION ? "Con" : "Sin") + "Rel_" + nombre_listado).value.trim();
    },

    /**
     *
     *
     */
    crearListado : function(nombre_listado,cual){
        var table = document.createElement('table');
        table.setAttribute('id',this.listadoPrefix + (cual == LISTADO_CON_RELACION ? "Con" : "Sin") + "Rel_" + nombre_listado);
        table.className = "listElement";
        table.cellPadding = 3;
        table.cellSpacing = 0;
        return table;
    },
    
    /**
    * NUEVA FUNCION
    *
    */
   crearPaginador : function(nombre_listado){
       var table = document.createElement('table');
       table.setAttribute('id',this.listadoPrefix + "Paginador_" + nombre_listado);
       table.setAttribute('align','center');
       table.className = "listPaginador";
       table.cellPadding = 3;
       table.cellSpacing = 0;
       return table;
   },    

    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    setImagenDeCarga : function(nombre_listado,cual){
        var listadoContainer = this.getListadoContainer(nombre_listado,cual);
        this.vaciarElementoDom(listadoContainer);
        var imagen = new Image();
        imagen.src = 'images/loading_cardinal.gif';
        listadoContainer.appendChild(imagen);
    },

    /**
     *
     *
     */
    setListadoVacio : function(nombre_listado,cual){
        var listadoContainer = this.getListadoContainer(nombre_listado,cual);
        this.vaciarElementoDom(listadoContainer);
        var speech = this.settings.speech.emptyList.replace("%item_cardinal",this.getNombreItemListado(nombre_listado));
        speech = speech.replace("%item_actual",this.settings.speech.item_actual);
        listadoContainer.appendChild(document.createTextNode(speech));
    },
	
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    debeCrearseLink : function(data){
        return (data.pcr == "0" && data.pr == "0") || (data.pcr == "1" && data.pd == "0") ? false : true;
    },

    /**
     *
     *
     */
    setearElementoLink : function(row,elemento){
        row.cells[3].appendChild(elemento);
    },

    /**
     *
     *
     */
    setearDatosLink : function(row,data){
        var link = row.cells[3].firstChild;
        link.className = data.className;
        if(data.href){
            link.setAttribute("href",data.href);
        }
        if(data.text){
            if(!link.hasChildNodes()){
                link.appendChild(document.createTextNode(data.text));
            }
            else{
                link.firstChild.nodeValue = data.text;
            }
        }
    },

    /**
     *
     *
     */
    agregarIconoAlerta : function(row,nombre_listado,data){
        row.cells[1].className = "icono";
        if((data.pcr == "0" && data.pr == "0") || (data.pcr == "1" && data.pd == "0")){
            row.alerIcon = new InnyAlertIcon({
                "id_producto" : data.id,
                "nombre_listado" : nombre_listado,
                "data" : data
            });

            // Agrego la imagen
            row.cells[1].appendChild(row.alerIcon.getIcon());
        }
        else{
            row.cells[1].appendChild(document.createTextNode(" "));
        }
    },

    /**
     *
     *
     */
    agregarProductoEnListadoCommon : function(nombre_listado,listado,data){
        var index = listado.rows.length;
        listado.insertRow(index);
        var row = listado.rows[index];

        // Seteo el ID
        row.setAttribute('id_producto',data.id);

        // Agrego el texto
        row.insertCell(0);
        row.cells[0].appendChild(document.createTextNode(data.txt));
        row.cells[0].className = "texto";

        // Agrego la celda para el �cono de alerta
        row.insertCell(1);
        this.agregarIconoAlerta(row,nombre_listado,data);

        // Agrego la celda para el �cono de informaci�n
        row.infoIcon = new InnyInfoIcon({
            "id_producto" : data.id,
            "nombre_listado" : nombre_listado,
            "images" : { "off_src":"images/info_off.gif", "loading_src":"images/info_loading.gif", "on_src":"images/info_on.gif" }
        });
        row.insertCell(2);
        row.cells[2].className = "icono";
        row.cells[2].appendChild(row.infoIcon.getIcon());

        // Agrego la celda para el link de agregar/eliminar
        row.insertCell(3);
        row.cells[3].className = "link";

        // Retorno el row
        return row;
    },

    /**
     *  NUEVA FUNCION
     *
     */
    insertarLink : function (numero_pagina, row, texto, nombre_listado,cantidad_paginas){
    	var link = document.createElement('a');
    	var pos = row.cells.length;
    	
    	row.insertCell(pos);
    	
    	row.cells[pos].appendChild(link);
    	row.cells[pos].className = "link";
    	
    	link.appendChild(document.createTextNode(texto));
        link.className = 'links';
        link.setAttribute("href","javascript:InnyCardinal.cambiarPagina('" + nombre_listado + "'," + numero_pagina + "," + cantidad_paginas + ");");
    },
    
    /**
	*
	*  NUEVA FUNCION
	*
	*/
    agregarLinksPaginador : function(nombre_listado,listado,pagina_actual,cantidad_paginas){
        var index = listado.rows.length;
        var row = listado.insertRow(index);
        var buttonsTolerance=5;
        var ini=(pagina_actual-buttonsTolerance)<1?1:(pagina_actual-buttonsTolerance);
        var top=(pagina_actual+buttonsTolerance)>cantidad_paginas?cantidad_paginas:(pagina_actual+buttonsTolerance);
        
        // Seteo el ID
        row.setAttribute('id_producto',"paginadorLinks");
        
        if (pagina_actual > 1){
    		this.insertarLink(pagina_actual-1, row, '� Anterior', nombre_listado,cantidad_paginas);
        }
        
        // Agrego el texto
        for(var i = ini; i <= top; i++){
        	this.insertarLink(i, row, i, nombre_listado,cantidad_paginas);
        }
        if (pagina_actual < cantidad_paginas){
    		this.insertarLink(pagina_actual+1, row, 'Siguiente �', nombre_listado,cantidad_paginas);
        }   

    },    

    /**
     *
     *
     */
    agregarProductoEnListadoSinRelacion : function(nombre_listado,listado,data){
        // Agrego los datos que tienen en comun todos los rows
        var row = this.agregarProductoEnListadoCommon(nombre_listado,listado,data);

        // Agrego el link
        //if(data.pr == '1'){
        if(this.debeCrearseLink(data)){
        	var accion;
        	if (InnyCardinal.perteneceAListadoConRelacion(data.id,nombre_listado)){
    			accion = 'eliminar';
        	}else{
        		accion = 'agregar';
        	}
            var link = document.createElement('a');
            this.setearElementoLink(row,link);
            this.setearDatosLink(row,{
                "href" : ("javascript:InnyCardinal."+ accion +"('"+nombre_listado+"',"+data.id+");"),
                "text" : (accion == "eliminar" ? ("� " + this.settings.speech["delete"]) : (this.settings.speech["add"] + " �")),
                "className" : accion
            });

        }
        else{
            var linkText = (data.pcr == "1" && data.pd == "0") ? ("� " + this.settings.speech["delete"]) : (this.settings.speech["add"] + " �");
            this.setearElementoLink(row,document.createTextNode(linkText));
        }
    },

    /**
     *
     *
     */
    agregarProductoEnListadoConRelacion : function(nombre_listado,listado,data){

        // Agrego los datos que tienen en comun todos los rows
        var row = this.agregarProductoEnListadoCommon(nombre_listado,listado,data);

        var linkText = "� " + this.settings.speech["delete"];

        // Agrego el link
        if(this.debeCrearseLink(data)){
            var link = document.createElement('a');
            this.setearElementoLink(row,link);
            this.setearDatosLink(row,{
                "href" : ("javascript:InnyCardinal.eliminar('"+nombre_listado+"',"+data.id+");"),
                "text" : linkText,
                "className" : "eliminar"
            });
        }

        // O agrego el texto
        else{
            this.setearElementoLink(row,document.createTextNode(linkText));
        }
    },

    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    buscar : function(nombre_listado,cual){
        if(cual == null){
            cual = LISTADO_SIN_RELACION;
        }
        var url = "action=ajax_cardinalidadProdsFull";
        url += "&id_listado="+this.settings.id_listado;
        url += "&id_producto="+(this.settings.id_producto != null ? this.settings.id_producto : "null");
        url += "&nombre_listado_cardinal="+nombre_listado;
        url += "&juntarListados=true";
        url += "&paginador="+true;
        url += "&resultsPerPage=";
        url += "&paginaActual=1";
        url += "&search="+encodeURIComponent(this.getPalabraBusqueda(nombre_listado,cual));

        // Traigo el contenido de los listados por AJAX
        var xmlhttp = initAjax();
        xmlhttp.open("GET","producto_am.php?"+url,true);
        xmlhttp.setRequestHeader('If-Modified-Since','Thu, 1 Jan 1970 00:00:00 GMT');
        xmlhttp.setRequestHeader('Cache-Control','no-cache');
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){
                    var json_response = null;
                    eval('var json_response = '+xmlhttp.responseText+';');
                    if(json_response != null){
                        
                    	//InnyCardinal.recargarResultadosBusqueda(nombre_listado,cual,json_response);
                    	InnyCardinal.cargarListadoSinRelacion(nombre_listado,json_response,parseInt(json_response.length/RESULTADOS_POR_PAGINA)+1,1);

                    }
                }

                // En caso que haya un error de conexi�n
                else{
                    InnyCardinal.ajaxError();
                }
            }
        }
        xmlhttp.send(null);
    },
   
    /**
     *
     *
     */
    recargarResultadosBusqueda : function(nombre_listado,cual,data){

        this.setImagenDeCarga(nombre_listado,cual);

        // En caso que no haya nada que cargar, indico que el listado est� vac�o
        if(data.length == 0){
            this.setListadoVacio(nombre_listado,cual);
        }

        //
        else{
            listadoSinRelacion = this.crearListado(nombre_listado,cual);
            var listadoConRelacion = this.getListado(nombre_listado,LISTADO_CON_RELACION);
            for(var i = 0; i < data.length; i++){
                data[i].rel = (this.estaSeleccionadoProducto(data[i].id,nombre_listado,LISTADO_CON_RELACION) ? "1" : "0");
                this.agregarProductoEnListadoSinRelacion(nombre_listado,listadoSinRelacion,data[i]);
            }
            var containerListado = this.getListadoContainer(nombre_listado,cual);
            this.vaciarElementoDom(containerListado);
            containerListado.appendChild(listadoSinRelacion);
        }
    },

    /**
     *
     *
     */
    searchOnKeyPress : function(event){
        var event = event ? event : window.event;
        var keycode = event.keyCode ? event.keyCode : event.which;
        var target  = event.srcElement ? event.srcElement : event.target;
        if(target.name == "innyCardinalSearchInput" && keycode == 13){
            var prefix = "innyCardinalSearchInputSinRel_";
            var nombre_listado = target.id.substring(prefix.length);
            InnyCardinal.buscar(nombre_listado);
            return false;
        }
    },

    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    yaFueCargadaLaSolapa : function(nombre_listado){
        for(var i = 0; i < this.listadosCardinalesCargados.length; i++){
            if(this.listadosCardinalesCargados[i].listado == nombre_listado){
                return this.listadosCardinalesCargados[i].cargado;
            }
        }
        return false;
    },

    /**
     *
     *
     */
    marcarListadoComoCargado : function(nombre_listado,cargado){
        for(var i = 0; i < this.listadosCardinalesCargados.length; i++){
            if(this.listadosCardinalesCargados[i].listado == nombre_listado){
                this.listadosCardinalesCargados[i].cargado = (cargado == null ? true : cargado);
            }
        }
    },

    /**
     *
     *
     */
    ajaxError : function(){
        if(confirm(this.settings.speech.ajax_error)){
            window.location.reload();
        }
    },

    /**
     *
     *
     */
    mostrarSolapaRelacionListados : function(nombre_listado){
        for(var k in this.settings.listados){
            document.getElementById('abmTabs_'+k).className = (k == nombre_listado ? 'selected' : '');
            document.getElementById('innyCardinalSuperContainer_'+k).style.display = (k == nombre_listado ? 'block' : 'none');
        }
        if(!this.yaFueCargadaLaSolapa(nombre_listado)){
            this.cargarListadosPor1eraVez(nombre_listado,null);
            this.marcarListadoComoCargado(nombre_listado);

            // Adjunto el input al formulario
            var input = document.createElement("input");
            input.setAttribute("type","hidden");
            input.setAttribute("id","input_cardinal_"+nombre_listado);
            input.setAttribute("name","cardinal_"+nombre_listado);
            input.setAttribute("value","");
            document.getElementById("cardinal_hidden_inputs").appendChild(input);
        }
    },
    
    cargarListadosPor1eraVez : function(nombre_listado,buscar_palabra){
	   this.dataListadoConRelacion(nombre_listado);
    },
   
    dataListadoSinRelacion : function(nombre_listado,actual,cant_paginas){
       	// Armo la URL
        
    	var url = "action=ajax_cardinalidadProdsFull";
        url += "&id_listado="+this.settings.id_listado;
        url += "&id_producto="+(this.settings.id_producto != null ? this.settings.id_producto : "null");
        url += "&nombre_listado_cardinal="+nombre_listado;
        url += "&juntarListados=true";
        if (cant_paginas!=0){
 	       url += "&paginador="+true;
 	       url += "&resultsPerPage="+RESULTADOS_POR_PAGINA;
 	       url += "&paginaActual="+actual;
        }
        // Traigo el contenido de los listados por AJAX
        var xmlhttp = initAjax();
        xmlhttp.open("GET","producto_am.php?"+url,true);
        xmlhttp.setRequestHeader('If-Modified-Since','Thu, 1 Jan 1970 00:00:00 GMT');
        xmlhttp.setRequestHeader('Cache-Control','no-cache');
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){
                    var response = null;
                    eval('var json_response = '+xmlhttp.responseText+';');
                    if(json_response != null){
                 	   InnyCardinal.cargarListadoSinRelacion(nombre_listado,json_response,cant_paginas,actual);
                    }
                }

                // En caso que haya un error de conexi�n
                else{
                    InnyCardinal.ajaxError();
                }
            }
        }
        xmlhttp.send(null);
    },
    
    dataListadoConRelacion : function(nombre_listado){
       	// Armo la URL
        var url = "action=ajax_cardinalidadProdsFull";
        url += "&id_listado="+this.settings.id_listado;
        url += "&id_producto="+(this.settings.id_producto != null ? this.settings.id_producto : "null");
        url += "&nombre_listado_cardinal="+nombre_listado;
        url += "&listado=1";

        // Traigo el contenido de los listados por AJAX
        var xmlhttp = initAjax();
        xmlhttp.open("GET","producto_am.php?"+url,true);
        xmlhttp.setRequestHeader('If-Modified-Since','Thu, 1 Jan 1970 00:00:00 GMT');
        xmlhttp.setRequestHeader('Cache-Control','no-cache');
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){
                    var response = null;
                    eval('var json_response = '+xmlhttp.responseText+';');
                    if(json_response != null){
                 	   InnyCardinal.cargarListadoConRelacion(nombre_listado,json_response);
                    }
                }

                // En caso que haya un error de conexi�n
                else{
                    InnyCardinal.ajaxError();
                }
            }
        }
        xmlhttp.send(null);
    },
    
    obtenerCantidadResultados : function(nombre_listado){
       	// Armo la URL
        var url = "action=ajax_cardinalidadProdsFull";
        url += "&id_listado="+this.settings.id_listado;
        url += "&id_producto="+(this.settings.id_producto != null ? this.settings.id_producto : "null");
        url += "&nombre_listado_cardinal="+nombre_listado;
        url += "&juntarListados=true";

        // Traigo el contenido de los listados por AJAX
        var xmlhttp = initAjax();
        xmlhttp.open("GET","producto_am.php?"+url,true);
        xmlhttp.setRequestHeader('If-Modified-Since','Thu, 1 Jan 1970 00:00:00 GMT');
        xmlhttp.setRequestHeader('Cache-Control','no-cache');
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){
                    var response = null;
                    eval('var json_response = '+xmlhttp.responseText+';');
                    eval('var resultados = (parseInt(json_response.length / RESULTADOS_POR_PAGINA))+1');
                    if(json_response != null){
                		InnyCardinal.dataListadoSinRelacion(nombre_listado,1,resultados);
                    }
                }

                // En caso que haya un error de conexi�n
                else{
                    InnyCardinal.ajaxError();
                }
            }
        }
        xmlhttp.send(null);
    },
    
    /**
     * NUEVA FUNCION
     *
     */
    perteneceAListadoConRelacion : function(id,nombre_listado){
    	// Obtengo los datos del listado con relaci�n
    	dataConRelacion = this.getListado(nombre_listado, LISTADO_CON_RELACION);
    	
    	if (dataConRelacion){
	    	for (var i = 0; i < dataConRelacion.rows.length; i++){
	    		if (dataConRelacion.rows[i].getAttribute("id_producto") == id){
	    			return true;
	    		}
	    	}
    	}
    	return false;
    },
    	
    /**
     * NUEVA FUNCION
     *
     */
    cargarListadoConRelacion : function(nombre_listado,data){

    	var listadoConRelacion = this.crearListado(nombre_listado,LISTADO_CON_RELACION);
        
        for(var i = 0; i < data.length; i++){
        	this.agregarProductoEnListadoConRelacion(nombre_listado,listadoConRelacion,data[i]);
        }

        // Muestro el listado de productos sin relacion
        var containerConRelacion = this.getListadoContainer(nombre_listado,LISTADO_CON_RELACION);
        this.vaciarElementoDom(containerConRelacion);
        var div = document.createElement('div');
        div.className = 'divCardinalidades';
        div.appendChild(listadoConRelacion);
        containerConRelacion.appendChild(div);
        
        this.obtenerCantidadResultados(nombre_listado);
    },
    
    cargarListadoSinRelacion : function(nombre_listado,data,cant_paginas,actual){

        var listadoSinRelacion = this.crearListado(nombre_listado,LISTADO_SIN_RELACION);
        var containerPaginador = this.getListadoPaginadorContainer(nombre_listado);
		this.vaciarElementoDom(containerPaginador);
        if (cant_paginas > 1){
    		var paginadorListado = this.crearPaginador(nombre_listado);
    		this.agregarLinksPaginador(nombre_listado, paginadorListado, actual, cant_paginas);

    		// Muestro paginador de listados
            var containerPaginador = this.getListadoPaginadorContainer(nombre_listado);
            this.vaciarElementoDom(containerPaginador);
            containerPaginador.appendChild(paginadorListado);   
    	}else{
    		var containerPaginador = this.getListadoPaginadorContainer(nombre_listado);
    		this.vaciarElementoDom(containerPaginador);
    	}
        for(var i = 0; i < data.length; i++){
    		this.agregarProductoEnListadoSinRelacion(nombre_listado,listadoSinRelacion,data[i]);
        }
        // Muestro el listado de productos sin relacion
        var containerSinRelacion = this.getListadoContainer(nombre_listado,LISTADO_SIN_RELACION);
        this.vaciarElementoDom(containerSinRelacion);
        if (data=='0'){
        	var div = document.createElement('div');
        	div.innerHTML='No existen elementos en <b>'+nombre_listado+'</b>';
        	containerSinRelacion.appendChild(div);
        	return;
        }
        
      	containerSinRelacion.appendChild(listadoSinRelacion);
    },

    /**
    * NUEVA FUNCION
    *
    */
    cambiarPagina : function(nombre_listado,actual,cant_paginas){

       var url = "action=ajax_cardinalidadProdsFull";
       url += "&id_listado="+this.settings.id_listado;
       url += "&id_producto="+(this.settings.id_producto != null ? this.settings.id_producto : "null");
       url += "&nombre_listado_cardinal="+nombre_listado;
       url += "&juntarListados=true";
       url += "&paginador="+true;
       url += "&resultsPerPage="+RESULTADOS_POR_PAGINA;
       url += "&paginaActual="+actual;
       url += "&search="+encodeURIComponent(this.getPalabraBusqueda(nombre_listado,LISTADO_SIN_RELACION));

       // Traigo el contenido de los listados por AJAX
       var xmlhttp = initAjax();
       xmlhttp.open("GET","producto_am.php?"+url,true);
       xmlhttp.setRequestHeader('If-Modified-Since','Thu, 1 Jan 1970 00:00:00 GMT');
       xmlhttp.setRequestHeader('Cache-Control','no-cache');
       xmlhttp.onreadystatechange = function(){
           if (xmlhttp.readyState == 4){
               if(xmlhttp.status == 200){
                   var response = null;
                   eval('var json_response = '+xmlhttp.responseText+';');
                   if(json_response != null){
                	   InnyCardinal.cargarListadoSinRelacion(nombre_listado, json_response, cant_paginas, actual);
                   }
               }

               // En caso que haya un error de conexi�n
               else{
                   InnyCardinal.ajaxError();
               }
           }
       }
       xmlhttp.send(null);
   },
    
    /**
     *
     *
     */
    agregar : function(nombre_listado,id_producto){

        if(this.puedoRelacionarProducto(nombre_listado)){
            var listadoConRelacion = this.getListado(nombre_listado,LISTADO_CON_RELACION);
            if(listadoConRelacion == null){
                var containerListadoConRelacion = this.getListadoContainer(nombre_listado,LISTADO_CON_RELACION);
                this.vaciarElementoDom(containerListadoConRelacion);
                listadoConRelacion = this.crearListado(nombre_listado,LISTADO_CON_RELACION);
                containerListadoConRelacion.appendChild(listadoConRelacion);
            }

            // Busco el elemento que tengo que agregar
            var listadoSinRelacion = this.getListado(nombre_listado,LISTADO_SIN_RELACION);
            for(var i = 0; i < listadoSinRelacion.rows.length; i++){
                if(parseInt(listadoSinRelacion.rows[i].getAttribute('id_producto')) == id_producto){
                    // Agrego el link en el listado de productos relacionados
                    this.agregarProductoEnListadoConRelacion(nombre_listado,listadoConRelacion,{"id":id_producto,"txt":listadoSinRelacion.rows[i].cells[0].firstChild.nodeValue});

                    // Cambio el link de agregar por el de eliminar
                    this.setearDatosLink(listadoSinRelacion.rows[i],{
                        "href" : ("javascript:InnyCardinal.eliminar('"+nombre_listado+"',"+id_producto+");"),
                        "text" : ("� " + this.settings.speech["delete"]),
                        "className" : "eliminar"
                    });
                    break;
                }
            }
        }
    },

    /**
     *
     *
     */
    eliminar : function(nombre_listado,id_producto){
        var listadoConRelacion = this.getListado(nombre_listado,LISTADO_CON_RELACION);
        for(var i = 0; i < listadoConRelacion.rows.length; i++){
            if(parseInt(listadoConRelacion.rows[i].getAttribute('id_producto')) == id_producto){
                listadoConRelacion.deleteRow(i);
                if(listadoConRelacion.rows.length == 0){
                    this.setListadoVacio(nombre_listado,LISTADO_CON_RELACION);
                }
                var listadoSinRelacion = this.getListado(nombre_listado,LISTADO_SIN_RELACION);
                for(var j = 0; j < listadoSinRelacion.rows.length; j++){
                    if(parseInt(listadoSinRelacion.rows[j].getAttribute('id_producto')) == id_producto){
                        this.setearDatosLink(listadoSinRelacion.rows[j],{
                            "href" : ("javascript:InnyCardinal.agregar('"+nombre_listado+"',"+id_producto+");"),
                            "text" : (this.settings.speech["add"] + " �"),
                            "className" : "agregar"
                        });
                        break;
                    }
                }
                break;
            }
        }
    },

    /**
     *
     *
     */
    estaSeleccionadoProducto : function(id_producto,nombre_listado,cual){
        var listado = this.getListado(nombre_listado,cual);
        if(listado != null){
            for(var i = 0; i < listado.rows.length; i++){
                if(parseInt(listado.rows[i].getAttribute('id_producto')) == parseInt(id_producto)){
                    return true;
                }
            }
        }
        return false;
    },

    /**
     *
     *
     */
    puedoRelacionarProducto : function(nombre_listado){
        var listadoConRelacion = this.getListado(nombre_listado,LISTADO_CON_RELACION);
        var cardinalidadSup = this.getCardinalidadSuperior(nombre_listado);
        if(listadoConRelacion == null || cardinalidadSup == "N" || listadoConRelacion.rows.length < cardinalidadSup){
            return true;
        }
        return false;
    },

    /**
     *
     *
     */
    getCardinalidadSuperior : function(nombre_listado){
        return this.settings.listados[nombre_listado].sup;
    },

    /**
     *
     *
     */
    getNombreItemListado : function(nombre_listado){
        return this.settings.listados[nombre_listado].item;
    },

    /**
     *
     *
     */
    onSubmit : function(){
        for(var nombre_listado in this.settings.listados){
            var listado = this.getListado(nombre_listado,LISTADO_CON_RELACION);
            if(listado != null && listado.rows.length > 0){
                var ids = "";
                for(var i = 0; i < listado.rows.length; i++){
                    ids = ids + (ids == "" ? "" : ",") + listado.rows[i].getAttribute('id_producto');
                }
                document.getElementById("input_cardinal_"+nombre_listado).value = ids;
            }
        }
        return true;
    }
};

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

/**
 *
 *
 */
function InnyInfoIcon(settings){
    this.settings = settings;
    this.speech = null;

    // Carga de im�genes
    ////////////////////////////////////////////////////////////////////////////
    this.image_off = new Image();
    this.image_off.src = this.settings.images.off_src;
    this.image_off.infoIcon = this;
    this.image_off.title = InnyCardinal.settings.speech.overlibInfoHelp.replace("%item_cardinal",InnyCardinal.getNombreItemListado(this.settings.nombre_listado));
    this.image_off.onclick = function(){
        this.infoIcon.loadInfo();
    }
    this.image_off.style.cursor = "pointer";
    ////////////////////////////////////////////////////////////////////////////
    this.image_loading = new Image();
    this.image_loading.src = this.settings.images.loading_src;
    ////////////////////////////////////////////////////////////////////////////
    this.image_on = new Image();
    this.image_on.src = this.settings.images.on_src;
    this.image_on.style.cursor = "auto";
    ////////////////////////////////////////////////////////////////////////////
    this.estadoActual = 0;
}

/**
 *
 *
 */
InnyInfoIcon.prototype.getIcon = function(){
    switch(this.estadoActual){
        case 0: return this.image_off;
        case 1: return this.image_loading;
        case 2: return this.image_on;
        default: return null;
    }
}

/**
 *
 *
 */
InnyInfoIcon.prototype.loadInfo = function(){
    if(this.estadoActual == 0){
        this.image_off.parentNode.replaceChild(this.image_loading,this.image_off);
        this.estadoActual = 1;
        var objThis = this;
        var url = "producto_am.php?action=ajax_cardinalidadProdInfo&id_producto="+this.settings.id_producto+"&id_listado_cardinal="+InnyCardinal.settings.id_listado;
        var xmlhttp = initAjax();
        xmlhttp.open("GET",url,true);
        xmlhttp.setRequestHeader("If-Modified-Since","Thu, 1 Jan 1970 00:00:00 GMT");
        xmlhttp.setRequestHeader("Cache-Control","no-cache");
        xmlhttp.onreadystatechange = function(){
            if(xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){
                    eval("var response = "+xmlhttp.responseText+";");
                    objThis.setInfo(response);
                }
                else{
                    InnyCardinal.ajaxError();
                }
            }
        }
        xmlhttp.send(null);
    }
}

/**
 *
 *
 */
InnyInfoIcon.prototype.setInfo = function(data){
    this.image_loading.parentNode.replaceChild(this.image_on,this.image_loading);
    this.estadoActual = 2;
    var objThis = this;
    this.image_on.onmouseover = function(){
        if(objThis.speech == null){
            var speech = null;
            if(data.length > 0){
                var listados = "";
                for(var i = 0; i < data.length; i++){
                    listados += "<li>"+data[i]+"</li>";
                }
                speech = InnyCardinal.settings.speech.overlibInfoConRel.replace("%item_cardinal",InnyCardinal.getNombreItemListado(objThis.settings.nombre_listado));
                speech = speech + ":<ul>"+listados+"</ul>";
            }
            else{
                speech = InnyCardinal.settings.speech.overlibInfoSinRel.replace("%item_cardinal",InnyCardinal.getNombreItemListado(objThis.settings.nombre_listado));
                speech = speech.replace("%item_actual",InnyCardinal.settings.speech.item_actual);
            }
            objThis.speech = speech;
        }
        return overlib(objThis.speech);
    }
    this.image_on.onmouseout = function(){
        return nd();
    }
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

/**
 *
 *
 */
function InnyAlertIcon(settings){
    this.settings = settings;
    this.estadoActual = 0;
    this.speech = null;

    this.iconImage = new Image();
    this.iconImage.alertInfo = this;
    this.iconImage.src = "images/info_alert.gif";
    this.iconImage.title = InnyCardinal.settings.speech.overlibInfoAlert.replace("%item_cardinal",InnyCardinal.getNombreItemListado(this.settings.nombre_listado));

    this.iconImage.onclick = function(){
        this.alertInfo.loadInfo();
    }
    this.iconImage.style.cursor = "pointer";
}

/**
 *
 *
 */
InnyAlertIcon.prototype.getIcon = function(){
    return this.iconImage;
}

/**
 *
 *
 */
InnyAlertIcon.prototype.loadInfo = function(){
    if(this.estadoActual == 0){
        this.estadoActual = 1;
        this.iconImage.onclick = null;
        this.iconImage.style.cursor = "auto";
        var objThis = this;
        var url = "producto_am.php?action=ajax_cardinalidadProdInfo&id_producto="+this.settings.id_producto+"&id_listado_cardinal="+InnyCardinal.settings.id_listado;
        var xmlhttp = initAjax();
        xmlhttp.open("GET",url,true);
        xmlhttp.setRequestHeader("If-Modified-Since","Thu, 1 Jan 1970 00:00:00 GMT");
        xmlhttp.setRequestHeader("Cache-Control","no-cache");
        xmlhttp.onreadystatechange = function(){
            if(xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){
                    eval("var response = "+xmlhttp.responseText+";");
                    objThis.setInfo(response);
                }
                else{
                    InnyCardinal.ajaxError();
                }
            }
        }
        xmlhttp.send(null);
    }
}

/**
 *
 *
 */
InnyAlertIcon.prototype.setInfo = function(data){
    this.estadoActual = 2;
    var objThis = this;
    this.iconImage.removeAttribute("title");
    this.iconImage.onmouseover = function(){
        if(objThis.speech == null){

            var dataProducto = objThis.settings.data;

            // Completo el listado de productos relacionados
            var listados = "";
            for(var i = 0; i < data.length; i++){
                listados += "<li>"+data[i]+"</li>";
            }
            listados = "<ul>"+listados+"</ul>";

            // Seteo el speech
            var speech = null;
            // En caso que el producto no tenga relaci�n
            if(dataProducto.pcr == "0"){
                speech = InnyCardinal.settings.speech.overlibAlertInfoSinRelacion.replace("%item_cardinal",InnyCardinal.getNombreItemListado(objThis.settings.nombre_listado));
                speech = speech.replace("%item_actual",InnyCardinal.settings.speech.item_actual);
                speech = speech.replace("%listado_cardinal",listados);
                speech = speech.replace("%listado_actual",InnyCardinal.settings.nombre_listado);
                speech = speech.replace("%limite_sup",InnyCardinal.settings.listados_cardinalidad[objThis.settings.nombre_listado].sup);
            }

            // En caso que el producto tenga relaci�n
            else{
                speech = InnyCardinal.settings.speech.overlibAlertInfoConRelacion.replace("%item_cardinal",InnyCardinal.getNombreItemListado(objThis.settings.nombre_listado));
                speech = speech.replace("%listado_cardinal",listados);
                speech = speech.replace("%listado_actual",InnyCardinal.settings.nombre_listado);
                speech = speech.replace("%limite_inf",InnyCardinal.settings.listados_cardinalidad[objThis.settings.nombre_listado].inf);

            }
            objThis.speech = speech;
        }
        return overlib(objThis.speech);
    }
    this.iconImage.onmouseout = function(){
        return nd();
    }
}