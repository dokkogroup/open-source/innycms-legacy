<?php
/**
 * Project: Inny Clientes
 * File: listado_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

function comparar_datos_listado($a,$b){
    return strcmp($a['nombre'],$b['nombre']);
}

################################################################################

# Verifico que el usuario esté logueado
Inny_Clientes::verificarSitioLogueado();

# Seteo si debo mostrar los listados privados
Inny_Clientes::setMostrarContenidosPrivados();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Obtengo el sitio actualmente logueado
$daoSitioLogueado = Inny_Clientes::getSitioLogueado();

# Armo el query para el daolister
# Verifico si debo mostrar los listados solo de administración
$daolisterQuery = '
    id_sitio = '.$daoSitioLogueado->id_sitio.'
    and nombre != \''.Inny::$registeredUsersListName.'\'
    '.(!Inny_Clientes::mostrarContenidosPrivados() ? 'and admin_only = \'0\'' : '').'
';

# Armo el query para filtrar los listados por nombre
if($_SERVER['REQUEST_METHOD'] == 'POST' && ($nombre_listado = trim($_POST['nombreListado'])) != ''){
    $daoListado = DB_DataObject::factory('listado');
	$daoListado->selectAdd('id_listado,id_sitio,nombre');
	$daoListado->id_sitio = $daoSitioLogueado->id_sitio;
	if($daoListado->find()){
        $ids = array();
        while($daoListado->fetch()){
            $pos = stripos($daoListado->getMetadataValue('nombre'), $_POST['nombreListado']);
            if($pos !== false){
                $ids[] = $daoListado->id_listado;
            }
        }
        $daolisterQuery.= ' and id_listado IN ('.(count($ids) > 0 ? implode(',',$ids) : 'NULL').')';
    }
}

# Genero el arreglo con los datos para el listado de listados
$resultados = array();
$daoListado = DB_DataObject::factory('listado');
$daoListado->whereAdd($daolisterQuery);
if($daoListado->find()){
    while($daoListado->fetch()){
        $daoProductos = $daoListado->getProductos();
        $resultados[] = array(
            'id_listado' => $daoListado->id_listado,
            'nombre' => $daoListado->getMetadataValue('nombre'),
            'cant' => $daoProductos ? $daoProductos->count() : 0,
            'privado' => $daoListado->getMetadataValue('parametros','private')
        );
    }
}
usort($resultados,'comparar_datos_listado');

# Asigno las variables al template
$smarty->assign('resultados',$resultados);
$smarty->assign('daoSitioLogueado',$daoSitioLogueado);

# Muestro el template
$smarty->display('listado_listado.tpl');

################################################################################