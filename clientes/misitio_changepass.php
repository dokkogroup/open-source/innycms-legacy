<?php
/**
 * Project: Inny Clientes
 * File: misitio_changepass.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

###############################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# En caso que haya variables en POST, verifico que sean v�lidas y cambio la contrase�a
if($_SERVER['REQUEST_METHOD'] == 'POST' && Inny_Clientes::validarCambioPasswordSitio($smarty)){
    Denko::redirect('misitio_edit.php?ok=pass');
}

# Muestro el template
$smarty->display('misitio_changepass.tpl');

###############################################################################