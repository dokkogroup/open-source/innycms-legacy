<?php
/**
 * Project: Inny Clientes
 * File: misitio_edit.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

function validateMiSitioData(&$smarty){

    # Verifico que el campo 'Apellido' tenga valor seteado
    $apellido = !empty($_POST['apellido']) ? trim($_POST['apellido']) : '';
    if($apellido == ''){
        Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_encargadoApellido'));
    }

    # Verifico que el campo 'Nombres' tenga valor seteado
    $nombre_encargado = !empty($_POST['nombre_encargado']) ? trim($_POST['nombre_encargado']) : '';
    if($nombre_encargado == ''){
        Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_encargadoNombre'));
    }

    # Verifico que el campo 'Tel�fono' tenga valor seteado
    $telefono = !empty($_POST['telefono']) ? trim($_POST['telefono']) : '';
    if($telefono == ''){
        Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_telefono'));
    }

    # Verifico que el campo 'Direcci�n' tenga valor seteado
    $direccion = !empty($_POST['direccion']) ? trim($_POST['direccion']) : '';
    if($direccion == ''){
        Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_direccion'));
    }

    # Verifico que si el campo 'E-mail' tiene valor seteado, sea v�lido
    $email = !empty($_POST['email']) ? strtolower(trim($_POST['email'])) : '';
    if($email != '' && !Denko::hasEmailFormat($email)){
        Denko::addErrorMessage('error_campoNoValido',array('%nombre_campo' => 'sitio_email'));
    }

    #
    $daoSitioLogueado = Inny_Clientes::getSitioLogueado();
    if($daoSitioLogueado->getConfig('admin_configs') == 1){

        # Verifico que el campo 'Sitio' tenga valor seteado
        $nombre_sitio = !empty($_POST['nombre_sitio']) ? trim($_POST['nombre_sitio']) : '';
        if($nombre_sitio == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_nombreSitio'));
        }

        # Verifico que el campo 'URL' tenga valor seteado
        $url = !empty($_POST['url']) ? trim($_POST['url']) : '';
        if($url == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_url'));
        }

        # Verifico que el campo 'E-mail de Contacto' tenga valor seteado y tenga
        # formato v�lido
        $smtp_contacto = !empty($_POST['smtp_contacto']) ? trim($_POST['smtp_contacto']) : '';
        if($smtp_contacto == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_emailContacto'));
        }
        else{
            $contactos = explode(',',$smtp_contacto);
            foreach($contactos as $contacto){
                $trimedContacto = trim($contacto);
                if($trimedContacto == '' || !Denko::hasEmailFormat($trimedContacto)){
                    Denko::addErrorMessage('error_campoNoValido',array('%nombre_campo' => 'sitio_emailContacto'));
                }
            }
        }

        # Verifico que el campo 'SMTP FromName' tenga valor seteado
        $smtp_fromname = !empty($_POST['smtp_fromname']) ? trim($_POST['smtp_fromname']) : '';
        if($smtp_fromname == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_smtpFromName'));
        }

        # Verifico que el campo 'SMTP Host' tenga valor seteado
        $smtp_host = !empty($_POST['smtp_host']) ? trim($_POST['smtp_host']) : '';
        if($smtp_host == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_smtpHost'));
        }

        # Verifico que el campo 'SMTP Username' tenga valor seteado
        $smtp_username = !empty($_POST['smtp_username']) ? trim($_POST['smtp_username']) : '';
        if($smtp_username == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_smtpUsername'));
        }

        # Verifico que el campo 'SMTP Password' tenga valor seteado
        $smtp_password = !empty($_POST['smtp_password']) ? trim($_POST['smtp_password']) : '';
        if($smtp_password == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_smtpPassword'));
        }

        # Verifico que el campo 'SMTP Protocol' tenga valor seteado
        $smtp_protocol = !empty($_POST['smtp_protocol']) ? trim($_POST['smtp_protocol']) : '';
        if($smtp_protocol == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_smtpProtocol'));
        }

        # Verifico que el campo 'SMTP Port' tenga valor seteado
        $smtp_port = !empty($_POST['smtp_port']) ? trim($_POST['smtp_port']) : '';
        if($smtp_port == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'sitio_smtpPort'));
        }
    }

    # En caso que haya errores, retorno FALSE
    if(Denko::hasErrorMessages()){
        return false;
    }

    # Seteo los campos del Sitio
    $daoSitioLogueado->apellido = $apellido;
    $daoSitioLogueado->nombre_encargado = $nombre_encargado;
    $daoSitioLogueado->telefono = $telefono;
    $daoSitioLogueado->direccion = $direccion;
    $daoSitioLogueado->email = $email != '' ? $email : 'null';

    # En caso que haya que configurar opciones de Administrador
    if($daoSitioLogueado->getConfig('admin_configs') == 1){

        # Opciones del sitio
        $daoSitioLogueado->nombre_sitio = $nombre_sitio;
        $daoSitioLogueado->url = $url;
        $daoSitioLogueado->ga_codigo = trim($_POST['ga_codigo']);
        $daoSitioLogueado->mantenimiento = !empty($_POST['mantenimiento']) ? 1 : 0;

        # Configuraciones de SMTP
        $daoSitioLogueado->setSmtpValue('contacto',$smtp_contacto);
        $daoSitioLogueado->setSmtpValue('fromname',$smtp_fromname);
        $daoSitioLogueado->setSmtpValue('host',$smtp_host);
        $daoSitioLogueado->setSmtpValue('username',$smtp_username);
        $daoSitioLogueado->setSmtpValue('password',$smtp_password);
        $daoSitioLogueado->setSmtpValue('protocol',$smtp_protocol);
        $daoSitioLogueado->setSmtpValue('port',$smtp_port);
    }

    # Actualizo el DAO
    $daoSitioLogueado->update();

    # Me redirijo a la vista de Sitio
    Denko::redirect('misitio_edit.php?ok=data');
}
################################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# En caso que haya variables en POST, verifico los datos del formulario
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    validateMiSitioData($smarty);
}

# En caso que haya que mostrar un mensaje de confirmaci�n
if(!empty($_GET['ok'])){
    switch($_GET['ok']){
        case 'data':
            Denko::addOkMessage('sitio_okEditarDatos');
            break;
        case 'pass':
            Denko::addOkMessage('password_okEditado');
            break;
        default: break;
    }
}

# Muestro el template
$smarty->display('misitio_edit.tpl');

################################################################################