<?php
/**
 * Project: Inny Clientes
 * File: producto_am.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

################################################################################
#                                   AJAX                                       #
################################################################################
if(
    !empty($_GET['action'])
    && in_array(
        $_GET['action'],
        array(

            # Carga de informaci�n de productos de listados cardinales
            'ajax_cardinalidadProdInfo',

            # Carga de productos de listados cardinales
            'ajax_cardinalidadProdsFull',

            # Archivos
            'ajax_up',
            'ajax_down',
            'ajax_delete'
        )
    )
){

    switch($_GET['action']){

        # Carga de informaci�n de producto de listado cardinal
        case 'ajax_cardinalidadProdInfo': echo InnyModule_Cardinal::ajax_cardinalidadProdInfo(); exit(0);

        # Carga de productos de listados cardinales
        case 'ajax_cardinalidadProdsFull': echo InnyModule_Cardinal::ajax_getProductosListadoCardinal(false); exit(0);
        		
        # Archivos
        default:
            $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
            $daoProductoTemporal->id_temporal = $_GET['id_archivo'];
            if($daoProductoTemporal->find(true)){
                switch($_GET['action']){

                    # En caso de eliminar un archivo
                    case 'ajax_delete': echo $daoProductoTemporal->delete() ? 'OK' : 'FAIL'; exit(0);

                    # En caso de subir la posici�n de un archivo
                    case 'ajax_up': echo $daoProductoTemporal->subirPosicion() ? 'OK' : 'FAIL'; exit(0);

                    # En caso de bajar la posici�n de un archivo
                    case 'ajax_down': echo $daoProductoTemporal->bajarPosicion() ? 'OK' : 'FAIL'; exit(0);
                }
            }
            exit(1);
    }
}

################################################################################
#                                    MAIN                                      #
################################################################################

# Creo la instancia de Smarty
$smarty = new Smarty();

# Cargo los filtros para los includes
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('add','edit','delete','setPos','up','down'));

# Si la acci�n es 'edit' o 'delete', verifico que el producto exista en la DB y
# pertenezca al Sitio actualmente logueado
$daoSitioActual = Inny_Clientes::getSitioLogueado();

////////////////////////////////////////////////////////////////////////////////

# En caso que la acci�n sea agregar:
#
# - la validez del par�metro 'id_listado' (en GET o POST),
# - que el listado exista en la DB
# - que el listado pertenezca al Sitio logueado
# - que no se exceda el l�mite de productos
$action = InnyCore_Utils::getParamValue('action');
if($action == 'add'){

    # Verifico que el listado exista en la DB
    $id_listado = InnyCore_Utils::getParamValue('id_listado');
    $daoListado = Inny_Common::verificarDao($smarty,'listado',$id_listado);
    $daoListado->fetch();

    # Verifico que el listado pertenezca al sitio logueado
    if($daoListado->id_sitio != $daoSitioActual->id_sitio){
        Inny_Common::errorCritico('listado','listado_errorListadoNoExiste',array('%id'=>$id_listado));
    }

    # Verifico que el listado no haya alcanzado o superado el l�mite permitido
    if($daoListado->alcanzoLimiteProductosPermitido()){
        Inny_Common::errorCritico(
            'listado',
            'listado_errorParametroLimitExcedido',
            array(
                '%listado_nombre'          => $daoListado->getMetadataValue('nombre'),
                '%listado_parametro_limit' => $daoListado->getMetadataValue('parametros','limit')
            )
        );
    }

    # Creo el DAO Producto
    $daoProducto = DB_DataObject::factory('producto');
}

# En caso que la acci�n sea distinta a 'add'
else{

    # Verifico que el producto exista en la DB
    $id_producto = InnyCore_Utils::getParamValue('id_producto');
    $daoProducto = Inny_Common::verificarDao($smarty,'producto',$id_producto);
    $daoProducto->fetch();

    # Verifico que el producto pertenezca al Sitio logueado.
    # Si no existe, lanzo el mensaje de error correspondiente
    if(!$daoProducto->perteneceAlSitio($daoSitioActual->id_sitio)){
        Inny_Common::errorCritico('producto','producto_errorNoExiste',array('%id'=>$id_producto));
    }

    # En caso que la acci�n sea distinta a add (insertar), obtengo el listado v�a el producto
    $daoListado = $daoProducto->getListado();
}

////////////////////////////////////////////////////////////////////////////////

# Verifico los permisos

switch($action){

    # Verifico si hay permisos para agregar items
    case 'add':
        if(!$daoListado->tienePermiso('A')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoAgregar');
        }
        break;

    # Verifico si hay permisos para editar items
    case 'edit':
        if(!$daoListado->tienePermiso('M')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoEditar');
        }
        break;

    # Verifico si hay permisos para eliminar items
    case 'delete':
        if(!$daoListado->tienePermiso('B')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoEliminar');
        }
        break;

    # Verifico si hay permisos para ordenar items
    case 'setPos':
    case 'up':
    case 'down':
        if(!$daoListado->tienePermiso('O')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoOrdenar');
        }
        break;
}

////////////////////////////////////////////////////////////////////////////////

# Seteo el link de volver
$linkBackProductos = new Inny_LinkBackProductos($daoListado->id_listado);

# En caso que haya que eliminar el producto, o cambiar su posici�n en el listado
if($action == 'delete' || $action == 'up' || $action == 'down' || $action == 'setPos' ){
    $id_listado = $daoProducto->id_listado;
    switch($action){

        # Elimino el producto
        case 'delete': InnyCore_Producto::eliminar($daoSitioActual,$daoListado->nombre,$daoProducto->id_producto); break;

        # Subo el orden del producto
        case 'up': InnyCore_Producto::subirPosicion($daoSitioActual,$daoListado->nombre,$daoProducto->id_producto); break;

        # Bajo el orden del producto
        case 'down': InnyCore_Producto::bajarPosicion($daoSitioActual,$daoListado->nombre,$daoProducto->id_producto); break;

        # Seteo la posicion del producto
        case 'setPos':
			if(empty($_GET['pos'])) break;
			if(!is_numeric($_GET['pos'])) break;
			$newPos = (int) $_GET['pos'];
			for($i=abs($daoProducto->posicion-$newPos); $i>0; $i--){
				if($newPos>$daoProducto->posicion) InnyCore_Producto::bajarPosicion($daoSitioActual,$daoListado->nombre,$daoProducto->id_producto);
				else InnyCore_Producto::subirPosicion($daoSitioActual,$daoListado->nombre,$daoProducto->id_producto);
			}
			break;
        # Esto no pasa nunca...
        default: break;
    }
    Denko::redirect($linkBackProductos->getLinkBack());
}

# En caso que la acci�n sea 'add' o 'edit', haya variables en POST y los datos sean v�lidos
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $id_producto = InnyCore_Producto::agregarEditarDesdePost($action,$daoSitioActual,$daoListado,$daoProducto);
    if($id_producto !== false){

        # En caso que deba seguir agregando productos al listado
        if(!empty($_POST['continue'])){
            Denko::redirect(basename($_SERVER['PHP_SELF']).'?action=add&id_listado='.$daoListado->id_listado.'&continue=true');
        }

        # En caso que mostrar que el producto se edit� correctamente
        Denko::redirect('producto_view.php?id_producto='.$id_producto.'&action='.($_POST['action']=='add'?'addok':'editok'));
    }
}

////////////////////////////////////////////////////////////////////////////////

# Asigno los DAOs al template
$smarty->assign('daoListado',$daoListado);
$smarty->assign('daoProducto',$daoProducto);

# Asigno el link de retorno al template
$smarty->assign('linkBack',$linkBackProductos->getLinkBack());

# Asigno la informaci�n de los archivos al template
$smarty->assign('filesInfo',$daoListado->getFilesInfo());

# Muestro el template
$smarty->display('producto_am.tpl');

################################################################################
