<?php
/**
 * Project: Inny Clientes
 * File: producto_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

function setearQueryListadoProductos(&$smarty,&$daoListado){

    $listado_query = 'id_listado = '.$daoListado->id_listado;
    $search_nombre = isset($_GET['sbt']) ? trim($_GET['sbt']) : '';

    if($search_nombre !== ''){

        # Verifico si debo buscar por ID
        $buscarPorID = (isset($_GET['sbi']) && $_GET['sbi'] == 'on');

        if($buscarPorID){
            $listado_query.= ' and id_producto = '.$search_nombre;
        }

        # En caso que tenga que buscar por texto
        else{

            # Obtengo el campo principal
            $campo_texto_principal = $daoListado->getCampoTextoPrincipal();

            # En caso que tenga el DSE
            if($daoListado->getMetadataValue('parametros','searchable') == 'true'){
                $daoProductosDSE = dse_search($search_nombre,null,null,$listado_query);
                $daoProductosDSE->orderBy($campo_texto_principal);
                $smarty->assign('daoProductosDSE',$daoProductosDSE);
            }

            # En caso que sea un listado normal
            else{
                $listado_query.= ' and '.$campo_texto_principal.' like \'%'.$search_nombre.'%\'';
            }
        }
    }

    # Seteo el query
    $smarty->assign('listado_query',$listado_query);
}

################################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty
$smarty = new Smarty();

# Cargo los filtros para los includes
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que el par�metro id_listado en el GET sea v�lido, y que el listado
# exista en la DB
$id_listado = !empty($_GET['id_listado'])?$_GET['id_listado']:null;
$daoListado = Inny_Common::verificarDao($smarty,'listado',$id_listado);

# Verifico que el listado pertenezca al Sitio logueado
$daoListado->fetch();
$daoSitioActual = Inny_Clientes::getSitioLogueado();
if($daoListado->id_sitio != $daoSitioActual->id_sitio){
    Inny_Common::errorCritico('listado','listado_errorListadoNoExiste',array('%id' => $id_listado));
}

# Verifico si debo redirigirme a la url cacheada del listado
$linkBackProductos = new Inny_LinkBackProductos($id_listado);
$linkBackProductos->verifyRedirect();
$linkBackProductos->setLinkBack();

////////////////////////////////////////////////////////////////////////////////

# En caso que existan variables en POST, verifico si los datos para agregar el
# archivo CSV son v�lidos
if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['action']) && $_POST['action'] == 'loadcsv'){
    InnyCore_Producto::insertarProductosDesdeCSV($daoListado);
}

////////////////////////////////////////////////////////////////////////////////

# En caso que quieran eliminar todos los productos del listado
if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['action'])){

    # Verifico si tiene permisos para eliminar elementos del listado
    if(!$daoListado->tienePermiso('B')){
        Inny_Common::errorCritico('listado','listado_errorParametroPermisoEliminar');
    }

    switch($_GET['action']){

        # Borrado de algunos productos (multidelete)
        case 'multi_delete':
            foreach($_GET['id'] as $id){
                InnyCore_Producto::eliminar($daoSitioActual,$daoListado->nombre,$id);
            }
            break;

        # Borrado de todos los productos del listado
        case 'deleteall': $daoListado->vaciar(); break;

        # En caso que la acci�n sea desconocida
        default: break;
    }

    # Ne redirijo al listado de productos
    Denko::redirect('producto_listado.php?id_listado='.$_GET['id_listado']);
}

////////////////////////////////////////////////////////////////////////////////

# Cargo los nombre de los listados con sus ID para el select
$smarty->assign('listados',$daoSitioActual->getListadosPorNombre());

# Seteo el query para el listado
setearQueryListadoProductos($smarty,$daoListado);

# Si est� todo OK, Muestro la p�gina
$smarty->assign('daoListado',$daoListado);
$smarty->assign('daoSitioActual',$daoSitioActual);
$smarty->display('producto_listado.tpl');

################################################################################