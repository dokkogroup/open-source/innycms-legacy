<?php
/**
 * Project: Inny Clientes
 * File: producto_view.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty
$smarty = new Smarty();

# Cargo los filtros para los includes
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

////////////////////////////////////////////////////////////////////////////////

# Si la tarea se agreg� o edit� bien, muestro el mensaje de ok
# Primero, verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('addok','editok','view'));

# Verifico que el producto exista en la DB
$id_producto = InnyCore_Utils::getParamValue('id_producto');
$daoProducto = Inny_Common::verificarDao($smarty,'producto',$id_producto);

# Verifico que el producto sea del sitio del usuario logueado
# En caso que no pertenezca al sitio, lanzo el mensaje de error
$daoProducto->fetch();
$daoSitioActual = Inny_Clientes::getSitioLogueado();
if(!$daoProducto->perteneceAlSitio($daoSitioActual->id_sitio)){
    Inny_Common::errorCritico('producto','producto_errorNoExiste',array('%id' => $id_producto));
}

# Verifico que existan permisos para ver el elemento
$daoListado = $daoProducto->getListado();
if(!$daoListado->tienePermiso('V')){
    Inny_Common::errorCritico('listado','listado_errorParametroPermisoVer');
}

////////////////////////////////////////////////////////////////////////////////

# Asigno el DAO del Producto al template
$smarty->assign('daoProducto',$daoProducto);

# Asigno el link de volver al listado al template
$linkBackProductos = new Inny_LinkBackProductos($daoProducto->id_listado);
$smarty->assign('linkBack',$linkBackProductos->getLinkBack());

# Muestro el template
$smarty->display('producto_view.tpl');

################################################################################