{inny_speech_load section="botonera"}
{inny_speech_load section="seccion"}
{inny_get_sitio_logueado assign="daoSitio"}
{* ========================================================================== *}

<!-- ============================= BOTONERA ================================ -->
<table class="botonera" cellpadding="0" cellspacing="0">
    <tbody>
    	<tr>
    		<td id="sitioBtn" class="btn" onmouseover="javascript:btnMouseOver(this,'sitio');" onmouseout="javascript:btnMouseOut(this,'sitio');">
    			<a class="headerLink linkBotonera" href="misitio.php" title="{#seccion_miSitio#}">
    				<div class="btnContainer" style="cursor:pointer;">
    					<div class="icon"><img src="images/sitio.gif" alt="{#seccion_miSitio#}" /></div>
    					<div class="label">{#seccion_miSitio#|dk_ucfirst}</div>
    					<div id="sitio" class="arrow"><img src="images/selected.gif" alt="{#seccion_miSitio#}" /></div>
    				</div>
    			</a>
    		</td>
    	</tr>
    	<tr>
    		<td id="editarBtn" class="btn" onmouseover="javascript:btnMouseOver(this,'editar');" onmouseout="javascript:btnMouseOut(this,'editar');">
    			<a class="headerLink linkBotonera" href="contenido_listado.php" title="{#seccion_misContenidos#}">
    				<div class="btnContainer" style="cursor:pointer;">
    					<div class="icon"><img src="images/editar.gif" alt="{#seccion_misContenidos#}" /></div>
    					<div class="label">{#seccion_misContenidos#|dk_ucfirst}</div>
    					<div id="editar" class="arrow"><img src="images/selected.gif" alt="{#seccion_misContenidos#}" /></div>
    				</div>
    			</a>
    		</td>
    	</tr>
    	<tr>
    		<td id="productoBtn" class="btn" onmouseover="javascript:btnMouseOver(this,'producto');" onmouseout="javascript:btnMouseOut(this,'producto');">
    			<a class="headerLink linkBotonera" href="listado_listado.php" title="{#seccion_misListados#}">
    				<div class="btnContainer" style="cursor:pointer;">
    					<div class="icon"><img src="images/producto.gif" alt="{#seccion_misListados#}" /></div>
    					<div class="label">{#seccion_misListados#|dk_ucfirst}</div>
    					<div id="producto" class="arrow"><img src="images/selected.gif" alt="{#seccion_misListados#}" /></div>
    				</div>
    			</a>
    		</td>
    	</tr>
    	{if $daoSitio->soportaDSE()}
        	<tr>
        		<td id="categoriasBtn" class="btn" onmouseover="javascript:btnMouseOver(this,'categorias');" onmouseout="javascript:btnMouseOut(this,'categorias');">
        			<a class="headerLink linkBotonera" href="dse_category_abm.php" title="{#seccion_categorias#}">
        				<div class="btnContainer" style="cursor:pointer;">
        					<div class="icon"><img src="images/categories.gif" alt="{#seccion_categorias#}" /></div>
        					<div class="label">{#seccion_categorias#|dk_ucfirst}</div>
        					<div id="categorias" class="arrow"><img src="images/selected.gif" alt="{#seccion_categorias#}" /></div>
        				</div>
        			</a>
        		</td>
        	</tr>
    	{/if}

    	{if $daoSitio->getConfig("register_users") == "1"}
    	   <tr>
        	   <td id="usuariosBtn" class="btn" onmouseover="javascript:btnMouseOver(this,'usuarios');" onmouseout="javascript:btnMouseOut(this,'usuarios');">
        			<a class="headerLink linkBotonera" href="usuario_registrado_listado.php" title="{#seccion_usuarios#}">
        				<div class="btnContainer" style="cursor:pointer;">
        					<div class="icon"><img src="images/users.gif" alt="{#seccion_usuarios#}" /></div>
        					<div class="label">{#seccion_usuarios#|dk_ucfirst}</div>
        					<div id="usuarios" class="arrow"><img src="images/selected.gif" alt="{#seccion_usuarios#}" /></div>
        				</div>
        			</a>
        		</td>
        	</tr>
    	{/if}
    	{if $daoSitio->getConfig("tiny_mode") != "1" && false}
            <tr>
        		<td id="solicitarBtn" class="btn" onmouseover="javascript:btnMouseOver(this,'solicitar');" onmouseout="javascript:btnMouseOut(this,'solicitar');">
        			<a class="headerLink linkBotonera" href="cambio_am.php?action=add" title="{#seccion_solicitarCambios#}">
        				<div class="btnContainer" style="cursor:pointer;">
        					<div class="icon"><img src="images/solicitar.gif" alt="{#seccion_solicitarCambios#}" /></div>
        					<div class="label">{#seccion_solicitarCambios#|dk_ucfirst}</div>
        					<div id="solicitar" class="arrow"><img src="images/selected.gif" alt="{#seccion_solicitarCambios#}" /></div>
        				</div>
        			</a>
        		</td>
        	</tr>
        	<tr>
        		<td id="estadoBtn" class="btn" onmouseover="javascript:btnMouseOver(this,'estado');" onmouseout="javascript:btnMouseOut(this,'estado');">
        			<a class="headerLink linkBotonera" href="cambios.php" title="{#seccion_estadoCambios#}">
        				<div class="btnContainer" style="cursor:pointer;">
        					<div class="icon"><img src="images/estado.gif" alt="{#seccion_estadoCambios#}" /></div>
        					<div class="label">{#seccion_estadoCambios#|dk_ucfirst}</div>
        					<div id="estado" class="arrow"><img src="images/selected.gif" alt="{#seccion_estadoCambios#}"></div>
        				</div>
        			</a>
        		</td>
        	</tr>
        	<tr>
        		<td id="cuentacorrBtn" class="btn" onmouseover="javascript:btnMouseOver(this,'cuentacorr');" onmouseout="javascript:btnMouseOut(this,'cuentacorr');">
        			<a href="ctacorriente_listado.php?action=add" class="headerLink linkBotonera" title="{#seccion_cuentaCorriente#}">
        				<div class="btnContainer" style="cursor:pointer;">
        					<div class="icon"><img src="images/cuentacorr.gif" alt="{#seccion_cuentaCorriente#}" /></div>
        					<div class="label">{#seccion_cuentaCorriente#|dk_ucfirst}</div>
        					<div id="cuentacorr" class="arrow"><img src="images/selected.gif" alt="{#seccion_cuentaCorriente#}" /></div>
        				</div>
        			</a>
        		</td>
        	</tr>

    	{/if}

        <!-- ========================== LOGOUT ============================= -->
        <tr>
            <td class="btn" onmouseover="javascript:btnMouseOver(this,'');" onmouseout="javascript:btnMouseOut(this,'');">
                <a href="index.php?logout" class="headerLink linkBotonera" title="{#seccion_logout#}">
                    <div class="btnContainer" style="cursor:pointer;">
                        <div class="icon"><img src="images/logout.gif" alt="{#seccion_logout#}" /></div>
                        <div class="label">{#seccion_logout#|dk_ucfirst}</div>
                    </div>
                </a>
            </td>
        </tr>
    </tbody>
</table>

<!-- ========================== GADGET ============================= -->
{inny_get_gadget assign="gadget_content"}
{if !empty($gadget_content)}
    <br />
    <table class="botonera" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td class="btn">
                    <div class="btnContainer">
                        <div class="icon"><img src="images/sitio.gif" alt="{#seccion_gadget#}" /></div>
                        <div class="label">{#seccion_gadget#|dk_ucfirst}</div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #b6bfd0;">
                    {$gadget_content}
                </td>
            </tr>
        </tbody>
    </table>
{/if}
<div style="text-align:center;width:142px;padding-top:20px;">
    <a href="generar_manual_sitio.php"><img src="images/manual_PDF/btn_descargar_{$daoSitio->idioma_codigo}.jpg" alt="" title="" /></a>
</div>
<br/>
