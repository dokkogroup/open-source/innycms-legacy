{inny_speech_load section="tarea"}
{inny_speech_load section="common"}
{inny_speech_load section="error"}
{* ========================================================================== *}
{assign var="id_tarea" value=$smarty.post.id_tarea|default:$smarty.get.id_tarea}
{assign var="action" value=$smarty.post.action|default:$smarty.get.action}
{if $action == "add"}
    {assign var="pageTitle" value=#tarea_agregar#|dk_capitalize}
{else}
    {assign var="pageTitle" value=#tarea_editar#|dk_capitalize}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle}
{dk_include file="styles/dokko_botones.css" inline="false"}
{dk_include file="js/inny.form.validate.js" inline="false" compress="false"}
{dk_include file="js/inny.tarea.js" inline="false" compress="false"}
{* ========================================================================== *}
{dk_hasmessages assign="hasMessages"}
{if $hasMessages}{include file="messagebox_errormessages.tpl"}{/if}
{* ========================================================================== *}
<br />
<form name="cambio" method="post" action="{$smarty.server.PHP_SELF|dk_basename}" enctype="multipart/form-data" onsubmit="javascript:return Inny_Tarea.validar();">
    <input type="hidden" name="action" value="{$action}" />
    <input type="hidden" name="id_tarea" value="{$id_tarea}" />
    <table class="changeReq" cellpadding="0" cellspacing="0">
    <tbody>

        <!-- =================== DESCRIPCION DE LA TAREA =================== -->
        <tr>
            <td class="description" colspan="2">{#tarea_descripcion#}</td>
        </tr>
        <tr>
            <td style="padding:4px;" colspan="2">
                {inny_wysiwyg name="descripcion" value=$smarty.post.descripcion|default:$daoTarea->descripcion}
            </td>
        </tr>

        <!-- ===================== ARCHIVOS ADJUNTOS ======================= -->
        {if $action == "edit"}

            <tr>
                <td class="description" colspan="2">{#tarea_archivos#}</td>
            </tr>
            <tr>
                <td class="content" colspan="2">
                {assign var="infoArchivos" value=$daoTarea->getInfoArchivos()}
                {if !empty($infoArchivos)}
                    <ul>
                    {foreach from=$infoArchivos item="archivo_info"}
                        <li>
                            <a class="download" href="download/{$archivo_info.id_temporal}/{$archivo_info.filename}" title="{#tarea_archivoDownloadLinkTitle#|replace:"%nombre_archivo":$archivo_info.filename}">{$archivo_info.filename}</a>
                            <a class="delete" href="javascript:confirmAction('{#tarea_archivoConfirmacionEliminar#}','cambio_am.php?action=delfile&id_temporal={$archivo_info.id_temporal}') ;" title="{#tarea_archivoEliminarLinkTitle#|replace:"%nombre_archivo":$archivo_info.filename}">[{#common_delete#}]</a>
                        </li>
                    {/foreach}
                    </ul>
                {else}
                    {#tarea_sinArchivos#}
                {/if}
                </td>
            </tr>
        {/if}

        <!-- ===================== ADJUNTAR ARCHIVOS ======================= -->
        <tr>
            <td class="description" colspan="2">{#tarea_archivosAdjuntar#}</td>
        </tr>
        <tr>
            {section name="archivos_adjuntos" loop=6}
                {assign var="index" value=$smarty.section.archivos_adjuntos.index+1}
                {if $index == 1 || $index == 4}<td class="inputs" style="vertical-align:top;">{/if}
                <br />
                {#tarea_archivoAdjuntoNro#|replace:"%nro":$index|dk_capitalize}
                <br />
                <input type="file" name="adjunto_{$index}">
                {if $index == 3 || $index == 6}</td>{/if}
            {/section}
        </tr>

        <!-- =========================== SUBMIT ============================ -->
        <tr>
            <td class="buttons" align="right" colspan="2">
                <div class="contentBtnContainer" align="right">
                    <div class="btnChangeReqIcon" align="center" style="cursor:default;"><img src="images/solicitar_ok.gif" /></div>
                    <div class="contentBtnChangeReq"><input type="submit" value="{$pageTitle}" style="cursor:pointer;"></div>
                </div>
            </td>
        </tr>
    </tbody>
    </table>
</form>
<script type="text/javascript">
//<[CDATA[
    Inny_ValidateForm.cargarSpeechs({ldelim}
        "input" : {ldelim}
            "descripcion" : "{#tarea_descripcion#}"
        {rdelim},
        "message" : {ldelim}
            "error_campoRequerido" : "{#error_campoRequerido#}"
        {rdelim}
    {rdelim});
//]]>
</script>
{* ========================================================================== *}
{include file="footer.tpl" section="solicitar"}