{inny_speech_load section="tarea"}
{inny_speech_load section="sitio"}
{* ========================================================================== *}
{if $smarty.get.action == "addok"}
    {assign var="pageTitle" value=#tarea_agregarOKtitulo#}
    {assign var="message" value=#tarea_agregarOKmensaje#}
    {assign var="buttonValue" value=#tarea_agregarOKsolicitarOtro#}
    {assign var="buttonURL" value="cambio_am.php?action=add"}
{else}
    {assign var="pageTitle" value=#tarea_editarOKtitulo#}
    {assign var="message" value=#tarea_editarOKmensaje#}
    {assign var="buttonValue" value=#tarea_editarOKestadoCambios#}
    {assign var="buttonURL" value="cambios.php"}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle}
{dk_include file="styles/dokko_botones.css" inline="false"}
{* ========================================================================== *}
<table class="changeAddedOk" cellpadding="0" cellspacing="0">
<tbody>

    <!-- ======================== DESCRIPCION ============================== -->
    <tr>
        <td class="description">{$message}</td>
    </tr>

    <!-- =========================== ACCIONES ============================== -->
    <tr>
        <td class="buttons">
            <div class="contentBtnContainer" align="right">

                <!-- SOLICITAR OTRO CAMBIO / ESTADO DE CAMBIOS -->
                <div class="btnReqAnotherIcon" align="center" onclick="javascript:Denko.redirect('{$buttonURL}');" style="cursor:pointer"><img src="images/solicitarotro.gif" /></div>
                <div class="contentbtnReqAnother"><input type="button" value="{$buttonValue}" onclick="javascript:Denko.redirect('{$buttonURL}');" style="text-transform:capitalize; cursor:pointer" /></div>

                <!-- MI SITIO -->
                <div class="btnMySiteIcon" align="center" onclick="javascript:Denko.redirect('misitio.php');" style="cursor:pointer"><img src="images/solicitarotro.gif" /></div>
                <div class="contentBtnMySite"><input type="button" value="{#sitio_tituloPagina#}" onclick="javascript:Denko.redirect('misitio.php');" style="text-transform:capitalize; cursor:pointer" /></div>
            </div>
        </td>
    </tr>
</tbody>
</table>
{* ========================================================================== *}
{include file="footer.tpl" section="solicitar"}