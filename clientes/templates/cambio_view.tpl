{inny_speech_load section="tarea"}
{inny_speech_load section="common"}
{* ========================================================================== *}
{assign var="daoEstado" value=$daoTarea->getEstado()}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#tarea#|dk_capitalize}
{dk_include file="styles/dokko_botones.css" inline="false"}
{* ========================================================================== *}
<table class="contentView" cellpadding="0" cellspacing="0">
<tbody>

    <!-- ===================== DESCRIPCION ================================= -->
	<tr>
		<td class="description" colspan="2" style="text-transform:capitalize;"><b>{#tarea_descripcion#}:</b></td>
	</tr>
	<tr>
		<td colspan="2" style="padding:30px; font-family:arial,helvetica,sans-serif; font-size:12px;">{$daoTarea->descripcion}</td>
	</tr>

    <!-- ======================== ARCHIVOS ADJUNTOS ======================== -->
    <tr>
	    <td class="content" colspan="2" style="border-bottom:none; border-right:none; border-top:solid 1px #f5f5f5;">
	       <b style="text-transform:capitalize;">{#tarea_archivos#}:</b>
            {assign var="infoArchivos" value=$daoTarea->getInfoArchivos()}
            {if !empty($infoArchivos)}
                <ul>
                {foreach from=$infoArchivos item="archivo_info"}
                    <li><a class="download" href="download/{$archivo_info.id_temporal}/{$archivo_info.filename}" title="{#tarea_archivoDownloadLinkTitle#|replace:"%nombre_archivo":$archivo_info.filename}">{$archivo_info.filename}</a></li>
                {/foreach}
                </ul>
            {else}
                {#tarea_sinArchivos#}
            {/if}
	    </td>
	</tr>

    <!-- ================== ESTADO Y FECHA DE CREACION ===================== -->
	<tr>
		<td class="{if $daoTarea->id_estado == $smarty.const.ESTADO_ENPROCESO}contentStarted{else}content{/if}" style="vertical-align:top; border-bottom:none; border-top:solid 1px #f5f5f5;">
    		<b style="text-transform:capitalize;">{#tarea_estado#}:</b>
            <br />
    		<span class="status">{$daoEstado->nombre|capitalize}</span>
		</td>
		<td class="description" style="border-bottom:none; border-top:solid 1px #f5f5f5;">
    		<b style="text-transform:capitalize;">{#tarea_fechaCreacion#}:</b>
            <br />
    		{$daoTarea->fecha_creacion}
		</td>
	</tr>

	<!-- ============================= ACCIONES ============================ -->
	<tr>
		<td class="buttons" colspan="2">
			<div class="contentBtnContainer" align="right">

                <!-- EDITAR -->
                {capture assign="accion_modificar"}javascript:Denko.redirect('{$smarty.server.HTTP_REFERER|dk_basename|default:"cambios.php"}');{/capture}
				<div class="btnBackIcon2" onclick="{$accion_modificar}" style="right:150px; cursor:pointer;" align="center"><img src="images/go_back.gif" /></div>
				<div class="contentBtnBack2" style="right:90px;"><input type="button" value="{#common_back#}" onclick="{$accion_modificar}" style="text-transform:capitalize; cursor:pointer;" /></div>

                {if $daoTarea->id_estado == $smarty.const.ESTADO_NOCOMENZADO}
				    <!-- VOLVER -->
				    {capture assign="accion_volver"}javascript:Denko.redirect('cambio_am.php?action=edit&id_tarea={$daoTarea->id_tarea}');{/capture}
					<div class="btnModifyIcon" onclick="{$accion_volver}" style="right:250px; cursor:pointer;" align="center" ><img src="images/modify.gif" /></div>
					<div class="contentBtnModify" style="right:180px;"><input type="button" value="{#tarea_accionModificar#}" onclick="{$accion_volver}" style="text-transform:capitalize; cursor:pointer;" /></div>

                    <!-- ANULAR TAREA-->
					{capture assign="accion_anular"}javascript:confirmAction('{#tarea_confirmacionAnular#}','cambio_am.php?action=cancel&id_tarea={$daoTarea->id_tarea}');{/capture}
					<div class="btnDiscardIcon" onclick="{$accion_anular}" style="right:60px; cursor:pointer;" align="center"><img src="images/cancel.gif" /></div>
					<div class="contentBtnDiscard" style="right:0px;"><input type="button" value="{#tarea_accionAnular#}" onclick="{$accion_anular}" style="text-transform:capitalize; cursor:pointer;" /></div>
				{/if}
			</div>
		</td>
	</tr>
</tbody>
</table>
{* ========================================================================== *}
{include file="footer.tpl" section="estado"}