{inny_speech_load section="tarea"}
{* ========================================================================== *}
{inny_get_sitio_logueado assign="daoSitio"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#tarea_tareas#|dk_capitalize}
{* ========================================================================== *}
{dk_daolister name="tr" table="tarea" resultsPerPage=10 query="id_sitio = "|cat:$daoSitio->id_sitio orderBy="fecha_creacion desc"}
    {dkp_ini}
    {if $dkp_results == "0"}
        <br />
        <span class="emptyListMessage">{#tarea_sinTareas#}</span>
    {else}
        <table class="editContent" cellpadding="0" cellspacing="0">
            <tbody>

            <!-- =================== CAMPOS DEL LISTADO ==================== -->
			<tr style="background-color: #DDDDDD;">
				<td class="header" style="font-size:11px;width:110px; text-transform:capitalize;">{#tarea_fechaCreacion#}</td>
				<td class="header" style="font-size:11px; text-transform:capitalize;">{#tarea_descripcion#}</td>
				<td class="header" style="font-size:11px;width:110px; text-transform:capitalize;">{#tarea_estado#}</td>
				<td class="headerLast" style="width:50px; text-transform:capitalize;">{#tarea_acciones#}</td>
			</tr>

			<!-- ============================================================-->
			{dk_lister export="id_tarea,id_estado,descripcion,fecha_creacion"}
			    {dkl_getdao assign="daoTarea"}
			    {assign var="daoEstado" value=$daoTarea->getEstado()}
                    <tr>
                        {if $id_estado == $smarty.const.ESTADO_ENPROCESO}
				           <td class="contentStarted" style="vertical-align:top;">
                        {else}
    					   <td class="content" style="vertical-align:top;">
    					{/if}
                            <span style="font-size:12px;font-weight:bold;">{$fecha_creacion|date_format:"%d/%m/%Y"}</span>
                            <br/>
                            <span style="font-size:12px;">{$fecha_creacion|date_format:"%H:%M:%S"}</span>
                        </td>

                        <td {if $id_estado == $smarty.const.ESTADO_ENPROCESO}class="contentStarted"{else}class="content"{/if} style="vertical-align:top;">
                            <div style="position:relative;width:100%;height:auto;z-index:2000;">
                                <div style="position:relative;z-index:2000;width:100%;height:inherit;">{$descripcion|strip_tags|truncate:80}</div>
                                <div id="verArrow{$id_tarea}" class="bkgDiv">Ver cambio</div>
                                <div id="modArrow{$id_tarea}" class="bkgDiv">Modificar cambio</div>
                                <div id="anulArrow{$id_tarea}" class="bkgDiv">Anular cambio</div>
                            </div>
                        </td>

                        <td {if $id_estado == $smarty.const.ESTADO_ENPROCESO}class="contentStarted"{else}class="content"{/if} valign="top">
    						<span class="status">{$daoEstado->nombre|capitalize}</span>
    					</td>

                        <!-- ================== ACCIONES =================== -->
                        <td class="contentLast" align="center" style="vertical-align:top">
                            <a class="button" href="{dkc_url url="cambio_view.php"}" title="{#tarea_linkTitleVer#}"><div style="text-transform:capitalize;">{#tarea_accionVer#}</div></a>
                            {if $id_estado == $smarty.const.ESTADO_NOCOMENZADO}
                                <a class="button" href="{dkc_url url="cambio_am.php" action="edit"}" title="{#tarea_linkTitleModificar#}"><div style="text-transform:capitalize;">{#tarea_accionModificar#}</div></a>
                                <a class="button" href="{dkc_url url="cambio_am.php" action="cancel" confirmMessage=#tarea_confirmacionAnular#}" title="{#tarea_linkTitleAnular#}"><div class="red" style="text-transform:capitalize;">{#tarea_accionAnular#}</div></a>
                            {/if}
                        </td>
                    </tr>
			{/dk_lister}

			</tbody>
        </table>
        {include file="paginador.tpl"}
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl" section="estado"}