{**
 * Muestra un tipo de dato
 *
 * @param InnyType $innyType elemento tipo de dato
 * @param value valor del dato
 * @param size tama�o del preview [tiny | medium |normal]
 *}
{dk_include file="styles/inny.clientes.common_preview.css" inline="false"}
{* ========================================================================== *}
{inny_speech_load section="common"}
{inny_speech_load section="archivo"}
{* ========================================================================== *}
{assign var="size" value=$size|default:"normal"}
{assign var="contenido_tipo" value=$innyType->getMetadataValue("tipo")}
{inny_is_supported_file_type type=$contenido_tipo assign="is_supported_file_type"}
{inny_type_view assign="preview_data" innyType=$innyType value=$value size=$size}
{* ========================================================================== *}
{if isset($preview_data)}

    {* ============================== ARCHIVOS =============================  *}
    {if $is_supported_file_type}

        {* ==================== EN CASO QUE SEA UNA IMAGEN ================== *}
        {if $contenido_tipo == "image"}
            {assign var="contenido_nombre" value=$innyType->getMetadataValue("nombre")}
            {if $size == "tiny"}
                <img src="{$preview_data.url_thumb}" alt="{$contenido_nombre}" />
            {else}
                {dk_include file="js/mootools-1.2.1-core-yc.js" inline="false" compress="false"}
                {dk_include file="js/smoothbox.js" inline="false" }
                {dk_include file="styles/smoothbox.css" inline="false"}
                <a class="smoothbox" title="{$preview_data.filename}" href="{$preview_data.url_image}"><img style="border: 1px solid #dddddd; padding: 1px;" src="{$preview_data.url_thumb}" alt="{$contenido_nombre}" /></a>
                <br />
                <br />
                <span style="font: 11px arial,tahoma;">{$preview_data.filename}</span>
                <br />
                <a style="font: 11px arial,tahoma;" class="smoothbox" title="{$preview_data.filename}" href="{$preview_data.url_image}" id="contenido_link">[{#common_zoom#}]</a>
                &nbsp;
                <a style="font: 11px arial,tahoma;" id="contenido_link" href="{$preview_data.url_download}" title="{#common_download#}">[{#common_download#}]</a>
            {/if}

        {* =================== EN CASO QUE SEA UNA FLASH ==================== *}
        {elseif $contenido_tipo == "flash"}
            {if $size == "tiny"}
                <embed class="contenido_flash" type="application/x-shockwave-flash" src="{$preview_data.url_flash}" wmode="transparent" quality="high" style="width:80px; height:80px; " />
            {else}
                {dk_include file="js/mootools-1.2.1-core-yc.js" inline="false" compress="false"}
                {dk_include file="js/smoothbox.js" inline="false"}
                {dk_include file="styles/smoothbox.css" inline="false"}
                {assign var="id_flash" value="video_"|cat:$value}
                <embed class="contenido_flash" type="application/x-shockwave-flash" src="{$preview_data.url_flash}" wmode="transparent" quality="high" />
                <div id="{$id_flash}" style="display:none;">
                    <embed src="{$preview_data.url_flash}" allowfullscreen="false" allowscriptaccess="always" flashvars="width=600&height=450&shuffle=false&repeat=list&autostart=true&file={$preview_data.url_flash}" style="width:600px;height:450px;" />
                </div>
                <br />
                <br />
                <span style="font: 11px arial,tahoma;">{$preview_data.filename}</span>
                <br />
                <a style="font: 11px arial,tahoma;" class="smoothbox" title="{$preview_data.filename}" id="contenido_link" href="#TB_inline?&height=450&width=600&inlineId={$id_flash}">[{#common_zoom#}]</a>
                &nbsp;
                <a style="font: 11px arial,tahoma;" id="contenido_link" href="{$preview_data.url_download}" title="{#common_download#}">[{#common_download#}]</a>
            {/if}

        {* ============== EN CASO QUE SEA UNA ARCHIVO DE AUDIO ============== *}
        {elseif $contenido_tipo == "sound"}
            {if $size == "tiny"}
                <embed class="contenido_sound" type="application/x-shockwave-flash" src="player/dewplayer-mini.swf?mp3={$preview_data.url_sound}.sound&showtime=0" allowScriptAccess="sameDomain" name="dewplayer" wmode="transparent" style="width:160px; height:20px;" />
            {else}
                <embed class="contenido_sound" type="application/x-shockwave-flash" src="player/dewplayer.swf?mp3={$preview_data.url_sound}.sound" allowScriptAccess="sameDomain" quality="high" name="dewplayer" wmode="transparent" />
                <br />
                <br />
                <span style="font: 11px arial,tahoma;">{$preview_data.filename}</span>
                <br />
                <a style="font: 11px arial,tahoma;" id="contenido_link" href="{$preview_data.url_download}" title="{#common_download#}">[{#common_download#}]</a>
            {/if}

        {* ============== EN CASO QUE SEA OTRO TIPO DE ARCHIVO ============== *}
        {else}
            {if $size == "tiny"}
                <img src="images/listfile_on.gif" alt="{#archivo#}" />
            {else}
                <img src="images/listfile_on.gif" alt="{#archivo#}" />
                <br />
                <span style="font: 11px arial,tahoma;">{$preview_data.filename}</span>
                <br />
                <a style="font: 11px arial,tahoma;" id="contenido_link" href="{$preview_data.url_download}" title="{#common_download#}">[{#common_download#}]</a>
            {/if}
        {/if}
    {else}
        {$preview_data}
    {/if}
{else}
    {capture assign="defaultValue"}<span class="contenido_default" style="font-family:arial,helvetica,sans-serif; font-size:12px;">{$innyType->getDefaultValue()}</span>{/capture}
    {if $is_supported_file_type && $size == "tiny"}
        {if $contenido_tipo == "image"}
            <img src="images/listimage_off.gif" />
        {elseif $contenido_tipo == "file"}
            <img src="images/listfile_off.gif" />
        {elseif $contenido_tipo == "flash"}
            <img src="images/listflash_off.gif" />
        {elseif $contenido_tipo == "sound"}
            <img src="images/listimage_off.gif" />
        {else}
            {$defaultValue}
        {/if}
    {else}
        {$defaultValue}
    {/if}
{/if}
