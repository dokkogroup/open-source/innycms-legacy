{inny_speech_load section="common"}
{inny_speech_load section="contenido"}
{* ========================================================================== *}
{assign var="id_configuracion" value=$smarty.get.id_configuracion|default:$smarty.post.id_configuracion}
{assign var="innyType" value=$contenido->createInnyType()}
{assign var="contenido_tipo" value=$innyType->getMetadataValue("tipo")}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#contenido_editarTituloPagina#|dk_capitalize}
{dk_include file="styles/dokko_botones.css" inline="false"}
{dk_include file="js/overlib.js" inline="false"}
{* ========================================================================== *}
{dk_hasmessages assign="hasMessages"}
{if $hasMessages}{include file="messagebox_errormessages.tpl"}{/if}
{* ========================================================================== *}
<br />
<form name="contenido" method="post" action="contenido.php" enctype="multipart/form-data">
    <input type="hidden" name="action" value="edit" />
	<input type="hidden" name="id_configuracion" value="{$id_configuracion}" />
    <table class="contentView" cellpadding="0" cellspacing="0">
    	<tbody>

    	   <!-- ============================================================ -->
        	<tr>
        		<td class="description" style="text-align:left; padding: 5px;">
        			<div style="position:relative;width:auto;height:auto;">
	                    <span style="font-weight:bold; color:#666666; text-transform:capitalize;">{$contenido->getMetadataValue("nombre")|dk_lower}</span>
						{assign var="ayuda" value=$contenido->getMetadataValue('ayuda')}
						{if !empty($ayuda)}
							<div style="position:absolute;right:0px;top:0px;cursor:pointer;" onMouseOver="return overlib('{$ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();"><img src="images/help_btn.gif" alt="" title=""></div>
						{/if}
					</div>
                </td>
        	</tr>

            <!-- =========================================================== -->
        	<tr>
                <td style="padding:15px; font-family:arial,helvetica,sans-serif; font-size:12px;">
                    {inny_is_supported_file_type type=$contenido_tipo assign="is_supported_file_type"}
                    {if $is_supported_file_type}
                        {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor size="normal"}
                        <br />
                        <br />
                    {/if}
                    {assign var="innyType" value=$contenido->createInnyType()}
                    {if $daoSitio->getMetadataValue("multilang","enabled") == "1" && $innyType->getMetadataValue("parametros","multilang") == "true"}
                        {foreach from=$daoSitio->getMetadataValue("multilang","languages") key="language_code" item="language_label"}
                            {assign var="contenido_default" value=$contenido->valor|sw_translate:$language_code}
                            {assign var="post_varname" value="valor_"|cat:$language_code}
                            <b>{$language_label}</b>
                            <br />
                            {inny_type_html_input innyType=$innyType name=$post_varname value=$smarty.post.$post_varname|default:$contenido_default}
                            <br />
                        {/foreach}
                    {else}
                        {inny_type_html_input innyType=$innyType name="valor" value=$smarty.post.valor|default:$contenido->valor}
                    {/if}
                </td>
        	</tr>

        	<!-- ======================== BOTONES ========================== -->
        	<tr>
        		<td class="buttons">
                    <div class="contentBtnContainer" align="right">
                        <div class="dateInformation">
                            {if !empty($contenido->aud_ins_date)}<p><span>{#common_date_ins#}</span>: {$contenido->aud_ins_date}.</p>{/if}
                            {if !empty($contenido->aud_upd_date) && $contenido->aud_upd_date!='0000-00-00 00:00:00'}<p><span>{#common_date_upd#}</span>: {$contenido->aud_upd_date}.</p>{/if}
                        </div>
                        <!-- BOTON DE CANCELAR -->
                        {capture assign="link_cancelar"}javascript:Denko.redirect('{$linkBack}');{/capture}
        				<div class="btnCancelIcon" onclick="{$link_cancelar}" style="text-align:center; cursor:pointer;"><img src="images/cancel.gif" alt="{#common_cancel#}" /></div>
        				<div class="contentBtnCancel"><input type="button" value="{#common_cancel#}" onclick="{$link_cancelar}" style="cursor:pointer;" /></div>

                        <!-- BOTON DE ACEPTAR -->
        				<div class="btnChangeOkIcon" style="text-align:center; cursor:pointer;"><img src="images/change_ok.gif" alt="{#common_edit#}" /></div>
        				<div class="contentBtnChangeOk"><input type="submit" value="{#common_edit#}" style="cursor:pointer;" /></div>
        			</div>
        		</td>
        	</tr>
    	</tbody>
    </table>
</form>
{* ========================================================================== *}
{include file="footer.tpl" section="editar"}