{inny_speech_load section="common"}
{inny_speech_load section="contenido"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#contenido_listadoTituloPagina#|dk_capitalize}
{dk_include file="styles/dokko_botones.css" inline="false"}
{* ========================================================================== *}
{dk_daolister name="ec" table="configuracion" resultsPerPage="20" orderBy="descripcion" query=$daolisterQuery}
    {dkp_ini}
    {if $dkp_results == "0"}
        <br />
        <span class="emptyListMessage">{#contenido_listadoVacio#}</span>
    {else}
        <table class="editContent" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <td class="header">{#contenido_listadoDescripcion#}</td>
                    <td class="header">{#contenido_listadoValor#}</td>
                    <td class="headerLast">{#contenido_listadoOpciones#}</td>
                </tr>
            </thead>
            <tbody>
            {dk_lister export="id_configuracion,valor,descripcion,tipo,nombre"}
                {inny_get_contenido assign="contenido" id_configuracion=$id_configuracion}
                {assign var="innyType" value=$contenido->createInnyType()}
                <tr {if $contenido->getMetadataValue("parametros","private") == "true"}class="privado"{/if}>
                    <td class="content" style="vertical-align:top;">{$descripcion|dk_capitalize}</td>
                    <td class="content" style="vertical-align:top;">
                        <div style="position:relative;width:100%;height:auto;">
                            <div style="position:relative; height:inherit; padding:10px;">
                                {if $daoSitio->getMetadataValue("multilang","enabled") == "1" && $innyType->getMetadataValue("parametros","multilang") == "true"}
                                    {foreach from=$daoSitio->getMetadataValue("multilang","languages") key="language_code" item="language_label"}
                                        <b>{$language_label}:</b>
                                        <div style="border: 1px solid #cccccc; padding:5px;">
                                            {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor|sw_translate:$language_code size="tiny"}
                                        </div>
                                        <br />
                                    {/foreach}
                                {else}
                                    {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor size="tiny"}
                                {/if}
                            </div>
                            <div id="verArrow{$id_configuracion}" class="bkgDiv">{#contenido_listadoFondoVer#}</div>
                            <div id="modArrow{$id_configuracion}" class="bkgDiv">{#contenido_listadoFondoEditar#}</div>
                        </div>
                    </td>

                    {* ACCIONES *}
                    <td class="contentLast" style="vertical-align:top;">
                        {if $contenido->tienePermiso("V") || $contenido->tienePermiso("M") || $contenido->tienePermiso("B")}
                            <center>

                                {* VER CONTENIDO *}
                                {if $contenido->tienePermiso("V")}
                                    <a class="button" href="{dkc_url url="contenido.php" action="view"}"><div>{#common_view#}</div></a>
                                {/if}

                                {* MODIFICAR CONTENIDO *}
                                {if $contenido->tienePermiso("M")}
                                    <a class="button" href="{dkc_url url="contenido.php" action="edit"}"><div>{#common_edit#}</div></a>
                                {/if}

                                {* BORRAR CONTENIDO *}
                                {if $contenido->tienePermiso("B") && $contenido->getMetadataValue("parametros","required") == "false" && $contenido->hasContent()}
                                    <a class="button" style="text-transform:capitalize;" href="{dkc_url url="contenido.php" action="delete" confirmMessage=#contenido_confirmacionEliminar#}"><div>{#common_erase#}</div></a>
                                {/if}
                            </center>
                        {else}
                            &nbsp;
                        {/if}
                    </td>
                </tr>
            {/dk_lister}
            </tbody>
        </table>
        {include file="paginador.tpl"}
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl" section="editar"}