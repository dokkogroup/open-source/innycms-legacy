{inny_speech_load section="common"}
{inny_speech_load section="contenido"}
{* ========================================================================== *}
{assign var="contenido_tipo" value=$contenido->getMetadataValue("tipo")}
{assign var="innyType" value=$contenido->createInnyType()}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#contenido_verTituloPagina#|dk_capitalize}
{dk_include file="styles/dokko_botones.css" inline="false"}
{* ========================================================================== *}
{capture assign="view_botonera"}
	<div class="contentBtnContainer" style="text-align:right;">
        <div class="dateInformation">
            {if !empty($contenido->aud_ins_date)}<p><span>{#common_date_ins#}</span>: {$contenido->aud_ins_date}.</p>{/if}
            {if !empty($contenido->aud_upd_date) && $contenido->aud_upd_date!='0000-00-00 00:00:00'}<p><span>{#common_date_upd#}: {$contenido->aud_upd_date}.</p>{/if}
        </div>
        <!-- VOLVER -->
		<div class="btnBackIcon" onclick="javascript:Denko.redirect('{$linkBack}');"><img src="images/go_back.gif" alt="{#common_back#}" /></div>
		<div class="contentBtnBack"><input type="button" value="{#common_back#}" onclick="javascript:Denko.redirect('{$linkBack}');" /></div>

		<!-- EDITAR -->
		{capture assign="link_editar"}javascript:Denko.redirect('contenido.php?action=edit&id_configuracion={$contenido->id_configuracion}');{/capture}
		<div class="btnChangeIcon" onclick="{$link_editar}" /><img src="images/editar_btn.gif" alt="{#common_edit#}" /></div>
		<div class="contentBtnChange"><input type="button" value="{#common_edit#}" onclick="{$link_editar}" /></div>
	</div>
{/capture}
{* ========================================================================== *}
<table class="contentView" cellpadding="0" cellspacing="0">
    <tbody>

        <!-- =========================== TITULO ============================ -->
    	<tr>
    		<td class="description"><span style="font-weight:bold; color:#666666; text-transform:capitalize;">{$contenido->getMetadataValue("nombre")}</span></td>
    	</tr>

    	<!-- =========================== BOTONERA ========================== -->
    	<tr>
    	   <td class="buttons arriba">{$view_botonera}</td>
        </tr>

    	<!-- ========================= CONTENIDO =========================== -->
	    <tr>
            <td style="padding:15px; font-family:arial,helvetica,sans-serif; font-size:12px;">
                {if $daoSitio->getMetadataValue("multilang","enabled") == "1" && $innyType->getMetadataValue("parametros","multilang") == "true"}
                    {foreach from=$daoSitio->getMetadataValue("multilang","languages") key="language_code" item="language_label"}
                        <b>{$language_label}</b>
                        <div style="border: 1px solid #cccccc; padding:5px;">
                            {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor|sw_translate:$language_code size="normal"}
                        </div>
                        <br />
                    {/foreach}
                {else}
                    {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor size="normal"}
                {/if}
    		</td>
    	</tr>

        <!-- =========================== BOTONERA ========================== -->
    	<tr>
    	   <td class="buttons">{$view_botonera}</td>
        </tr>
    </tbody>
</table>
{* ========================================================================== *}
{include file="footer.tpl" section="editar"}