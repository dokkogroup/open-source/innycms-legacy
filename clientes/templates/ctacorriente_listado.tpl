{inny_speech_load section="renglon"}
{inny_speech_load section="tarea"}
{inny_speech_load section="ctacorriente"}
{* ========================================================================== *}
{inny_get_sitio_logueado assign="daoSitio"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#renglon_title#}
{* ========================================================================== *}
{dk_daolister name="cc" table="renglon" resultsPerPage=10 orderBy="fecha_creacion desc" query="id_tarea in (select id_tarea from tarea where id_sitio = "|cat:$daoSitio->id_sitio|cat:")"}
    {dkp_ini}
    {if $dkp_results == "0"}
        <br />
        <span class="emptyListMessage">{#ctacorriente_sinMovimientos#}</span>
    {else}
        <table class="editContent" cellpadding="0" cellspacing="0">
        <tbody>

            <!-- ================= ENCABEZADO DEL LISTADO ================== -->
			<tr style="background-color: #dddddd;">
			    <td class="header" style="font-size:11px;">{#tarea#}</td>
			    <td class="header" style="font-size:11px;width:110px;">{#renglon_estado#}</td>
			    <td class="header" style="font-size:11px;width:50px;">{#renglon_monto#}</td>
				<td class="header" style="font-size:11px;width:80px;">{#renglon_fechaCreacion#}</td>
			</tr>

			<!-- ================= CONTENIDO DEL LISTADO =================== -->
			{dk_lister export="id_renglon,id_tarea,monto,fecha_creacion"}
			    {dkl_getdao assign="daoRenglon"}
			    {assign var="daoTarea" value=$daoRenglon->getTarea()}
			    {assign var="daoEstado" value=$daoTarea->getEstado()}
                <tr>
                    <td class="content" style="vertical-align:top;"><a href="cambio_view.php?id_tarea={$id_tarea}">{$daoTarea->descripcion|strip_tags|truncate:80}</a></td>
                    <td class="content" style="vertical-align:top; text-align:center;">{$daoEstado->nombre}</td>
                    <td class="content" style="vertical-align:top; text-align:center;">${$monto}</td>
                    <td class="content" style="vertical-align:top;">
                        <span style="font-size:12px;font-weight:bold;">{$fecha_creacion|date_format:"%d/%m/%Y"}</span>
                        <br />
                        <span style="font-size:12px;">{$fecha_creacion|date_format:"%H:%M:%S"}</span>
                    </td>
                </tr>
			{/dk_lister}
		<tbody>
        </table>
        {include file="paginador.tpl"}
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl" section="cuentacorr"}