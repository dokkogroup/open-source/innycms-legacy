{inny_speech_load section="common"}
{inny_speech_load section="dse"}
{inny_speech_load section="error"}
{* ========================================================================== *}
{inny_get_sitio_logueado assign="daoSitio"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#dse_seleccionarCategoriaTituloPagina#|dk_ucfirst}
{dk_include file="styles/inny.dse.css" inline="false"}
{dk_include file="js/inny.dse.js" inline="false"}
{* ========================================================================== *}
<br />
<table class="dse_categories" cellpadding="0" cellspacing="0" style="border: 1px solid #dddddd; padding: 15px;">
    <tbody>
        <tr>
            <td class="HTMLSelectCanvas">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td>
                                {inny_dse_options_category assign="inny_dse_options_level1" id_sitio=$daoSitio->id_sitio}
                                <select id="dse_category_level1" name="dse_category_level1" size="18" disabled="disabled" onchange="javascript:Inny_DSE.loadSubcategories(1);">
                                    {if !empty($inny_dse_options_level1)}
                                        {html_options options=$inny_dse_options_level1}
                                    {else}
                                        <option value="null" label="{#dse_sinCategorias#}">{#dse_sinCategorias#}</option>
                                    {/if}
                                </select>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="abm">
                                <input class="text" type="text" id="dse_category_addinput1" name="dse_category_addinput1" />
                                <input class="button" type="button" id="dse_category_addbutton1" name="dse_category_addbutton1" value="{#common_add#}" onclick="javascript:Inny_DSE.addCategory(1);" />
                                <input class="button" type="button" id="dse_category_deletebutton1" name="dse_category_deletebutton1" disabled="disabled" value="{#common_delete#}" onclick="javascript:Inny_DSE.deleteCategory(1);" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <script type="text/javascript">
                //<[CDATA[
                    Inny_DSE.init(
                    {ldelim}
                        "abmMode" : true,
                        "id_sitio" : "{$daoSitio->id_sitio}",
                        "maxLevels" : "{$smarty.const.INNY_DSE_MAX_LEVEL_CATEGORY}",
                        "speech" : {ldelim}
                            "empty" : "{#dse_sinSubcategorias#}",
                            "loading" : "{#common_loading#}...",
                            "errorRequiredCategoryName" : "{#dse_errorNombreCategoriaRequerido#}",
                            "errorExistsCategoryName" : "{#dse_errorExisteCategoriaMismoNombre#}",
                            "errorSeleccionarCategoriaParaEliminar" : "{#dse_errorSeleccionarCategoriaParaEliminar#}",
                            "errorAjaxConexion" : "{#error_ajaxConexion#}"
                        {rdelim}
                    {rdelim}
                    );
                    Inny_DSE.setupCategoryGroup(1);
                //]]>
                </script>
            </td>

            <td class="HTMLSelectCanvas">
                {section name="dse_subcategories" start="2" loop=$smarty.const.INNY_DSE_MAX_LEVEL_CATEGORY+1 step="1"}
                    {assign var="index" value=$smarty.section.dse_subcategories.index}
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                                    <select id="dse_category_level{$index}" name="dse_category_level{$index}" size="5" style="width:250px;" disabled="disabled" {if $index < $smarty.const.INNY_DSE_MAX_LEVEL_CATEGORY} onchange="javascript:Inny_DSE.loadSubcategories({$index});"{/if}>
                                        <option value="null" label="{#dse_cargando#}">{#dse_cargando#}</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="abm">
                                    <input class="text" type="text" id="dse_category_addinput{$index}" name="dse_category_addinput{$index}" disabled="disabled" />
                                    <input class="button" type="button" id="dse_category_addbutton{$index}" name="dse_category_addbutton{$index}" disabled="disabled" value="{#common_add#}" onclick="javascript:Inny_DSE.addCategory({$index});" />
                                    <input class="button" type="button" id="dse_category_deletebutton{$index}" name="dse_category_deletebutton{$index}" disabled="disabled" value="{#common_delete#}" onclick="javascript:Inny_DSE.deleteCategory({$index});" />
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <br />
                {/section}
                <script type="text/javascript">
                //<[CDATA[
                    Inny_DSE.loadCategories(2);
                //]]>
                </script>
            </td>
        </tr>
    </tbody>
</table>
<br />
{* ========================================================================== *}
{include file="footer.tpl" section="categorias"}