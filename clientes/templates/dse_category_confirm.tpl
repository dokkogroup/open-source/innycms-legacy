{inny_speech_load section="common"}
{inny_speech_load section="dse"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#dse_eliminarTituloPagina#|dk_capitalize}
{dk_include file="styles/common.css" inline="false"}
{* ========================================================================== *}
<div id="centralContent">
    <center>
        <div class="box alert">
            <h1>{#dse_confirmarEliminarTitulo#|replace:"%nombre":$daoDseCategory->name}</h1>
            <br />
            {#dse_confirmarEliminarSpeech1#}
            <br />
            <br />
            {#dse_confirmarEliminarSpeech2#}
        </div>
        <br />
        <div>
            <a class="boton enter" href="inny.dse_category.php?action=category_delete&id_category={$daoDseCategory->id_category}" title="{#dse_confirmarEliminarTextoBoton#}" style="text-transform:capitalize;">{#dse_confirmarEliminarTextoBoton#}</a>
            &nbsp;&nbsp;
            <a class="boton enter" href="dse_category_abm.php" title="{#common_cancel#}" style="text-transform:capitalize;">{#common_cancel#}</a>
        </div>

    </center>
</div>
{* ========================================================================== *}
{include file="footer.tpl" section="categorias"}