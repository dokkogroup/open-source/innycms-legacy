{inny_speech_load section="application"}
{inny_speech_load section="header"}
{* ========================================================================== *}
{inny_get_sitio_logueado assign="daoSitio"}
{* ========================================================================== *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-AR" lang="es-AR">
    <head>
        <title>{#application_name#} | {$pageTitle}</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="content-language" content="es-ar" />
        <meta name="copyright" content="Copyright 2007-{$smarty.now|date_format:"%Y"}. Dokko Group All Rights Reserved." />
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
        {dk_include file="styles/dokko_header.css"}
        {dk_include file="styles/dokko_content.css"}
        {dk_include file="js/dk.common.js"}
        {dk_include file="js/inny.common.js"}
        {dk_include file="js/inny.pdf.js"}
        {dk_include_target}
    </head>
    <body>
    	<center>
        <table class="header" cellpadding="0" cellspacing="0" align="center">
        	<tr>
        		<td class="logo"></td>
        		<td class="header_image_{$daoSitio->idioma_codigo} header_bar" style="vertical-align:middle;" align="right">
        			<br />
                    <br />
                    <br />
                    <br />
                    <br />
        			<img src="images/sitio.gif" style="width:12px; height:12px;" />
                    &nbsp;
        			<a class="headerLink" href="{$daoSitio->url|dk_urlformat}" title="{#header_irAMiSitio#}" target="_blank">{#header_irAMiSitio#}</a>
                    &nbsp;&nbsp;
                </td>
        	</tr>
        </table>
        <table class="siteContent" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td style="width:150px; padding-left:10px; padding-top:10px;vertical-align:top;" align="left">
                    {include file="botonera.tpl"}
                </td>
                <td style="width:610px;padding-left:10px;padding-top:10px;padding-right:10px; vertical-align:top;" align="left">
                    <h1>{$pageTitle}</h1>
<!-- =========================== CONTENT =================================== -->
