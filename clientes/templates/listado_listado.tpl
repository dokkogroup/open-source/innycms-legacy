{inny_speech_load section="common"}
{inny_speech_load section="producto"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#producto_misListado#|dk_capitalize}
{dk_include file="styles/dokko_botones.css" inline="false"}
{* ========================================================================== *}
<br />
<form name="busq_listado" action="{$smarty.server.PHP_SELF|dk_basename}" method="post">
    <div style="position:relative;color:#777777;font-family:Arial,Helvetica,sans-serif;font-size:13px;">
        {#producto_nombreListado#|dk_capitalize}:
        <input type="text" name="nombreListado" id="nombreListado" value="{$smarty.post.nombreListado}" style="font-size:12px; color:#808080; border:1px solid #7f9db9; " />
        &nbsp;
        <input type="submit" class="headButton" style="text-transform:capitalize;"  value="{#common_search#}" />
    </div>
</form>
<br />
{if $resultados|@count == 0}
    <br />
    <span class="emptyListMessage">{#producto_sinListados#}</span>
{else}
    <div style="position:relative; height:315px; overflow-x:hidden; overflow-y:auto;">
        <table class="editContent" border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr style="background-color: #dddddd;">
                    <td class="header" style="font-size:11px;">&nbsp;</td>
                </tr>
            </thead>
            <tbody>
            {foreach from=$resultados item="resultado"}
                <tr class="{if $resultado.privado == "true"}privado{/if}" >
                    <td class="content" style="vertical-align:top; text-align:left; cursor:pointer;"  >
                    	<a href="producto_listado.php?id_listado={$resultado.id_listado}" style="text-decoration:none;">
	                        <div style="position:relative; height:auto; z-index:2000; cursor:pointer;">
	                            <div style="position:relative; z-index:2000; width:100%; height:inherit;">{$resultado.nombre|dk_capitalize} ({if $resultado.cant > 0}<span style="font-weight:bold;color:#000000;">{$resultado.cant}</span>{else}0{/if})</div>
	                            <div id="verArrow{$resultado.id_listado}" class="bkgDiv">{#producto_verProductosDeListado#}</div>
	                        </div>
	                    </a>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
    <br />
{/if}
{* ========================================================================== *}
{include file="footer.tpl" section="producto"}