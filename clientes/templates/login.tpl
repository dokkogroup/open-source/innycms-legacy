<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    {inny_speech_load section="application"}
    {inny_speech_load section="footer"}
    {inny_speech_load section="login"}
    {inny_speech_load section="error"}
    <head>
        <title>{#application_name#} | {#login_tituloPagina#|dk_capitalize}</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="content-language" content="es-ar" />
        <meta name="copyright" content="Copyright 2007-{$smarty.now|date_format:"%Y"}. Dokko Group All Rights Reserved." />
        <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
        {dk_include file="styles/dokko_header.css"}
        {dk_include file="styles/inny.clientes.login.css"}
        {dk_include file="js/dk.common.js"}
        {dk_include file="js/inny.common.js"}
        {dk_include file="js/inny.form.validate.js"}
        {dk_include file="js/inny.login.js"}
    </head>
    <body>
        <!-- HEADER -->
        <table class="loginHeader" cellpadding="0" cellspacing="0">
            <tbody>
            	<tr>
            		<td class="logo"><img src="images/inny_logo.jpg" alt="logo" /></td>
            		<td class="bar"><img src="images/header_bar.gif" alt="header" /></td>
            	</tr>
        	</tbody>
        </table>

        <!-- CONTENT -->
        <table cellpadding="0" cellspacing="0" class="content" style="margin: 0 auto;">
            <tbody>
            	<tr>
            		<td class="messages" style="text-align:center; vertical-align:bottom;">
            			{dk_showmessages}
            				<br />
            				{$dkm_message}
            			{/dk_showmessages}
            		</td>
            	</tr>
            	<tr>
            		<td style="text-align:center; vertical-align:top; height:300px;">
            			<table cellpadding="10" cellspacing="0" class="loginForm" style="margin:0 auto;">
                            <tbody>
                    			<form name="login" action="index.php?login" method="post" onsubmit="javascript: return Inny_Login.validar();">
                    				<tr>
                    					<td rowspan="3"><img src="images/login_lock.gif" alt="login" style="width:131px; height:131px;" /></td>
                    					<td class="label">{#login_username#}:</td>
                    					<td class="input" style="text-align: left;"><input type="text" id="username" name="username" value="{$smarty.post.username|default:$smarty.cookies.SW_USERNAME}" /></td>
                    				</tr>
                    				<tr>
                    					<td class="label">{#login_password#}:</td>
                    					<td class="input" style="text-align: left;"><input type="password" id="password" name="password" /></td>
                    				</tr>
                    				<tr>
                    					<td colspan="2" style="text-align: right;">
                    					<div class="btnContainer">
                    						<div class="btnEnter">
                                                <input type="submit" value="{#login_submit#}" onmouseover="javascript:mouseOver(this);" onmouseout="javascript:mouseOut(this);" style="text-transform:capitalize;" />
                                            </div>
                    					</div>
                    					</td>
                    				</tr>
                    			</form>
                            </tbody>
            			</table>
            		</td>
            	</tr>
            </tbody>
        </table>
        <script type="text/javascript">
		//<[CDATA[
            Inny_ValidateForm.cargarSpeechs({ldelim}
                "input" : {ldelim}
                    "username" : "{#login_username#}",
                    "password" : "{#login_password#}"
                {rdelim},
                "message" : {ldelim}
                    "error_campoRequerido" : "{#error_campoRequerido#}"
                {rdelim}
            {rdelim});
            Inny_Login.init();
		//]]>
		</script>

        <!-- FOOTER -->
        <div class="pageFooter" style="margin: 0 auto;">
            {assign var="ano_actual" value=$smarty.now|date_format:"%Y"}
            {#footer_copyright#|replace:"%a�o_actual":$ano_actual|replace:"%dokkoweb":"Dokko Group"}
            <div class="dokko_footer"><a href="http://www.dokkogroup.com.ar" title="Dokko Group" target="_blank"><img src="images/dokko_link.jpg" alt="Dokko Group" title="Dokko Group"></a></div>
        </div>
		<script type="text/javascript">
		//<[CDATA[
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		//]]>
		</script>
		<script type="text/javascript">
		//<[CDATA[
			var pageTracker = _gat._getTracker("UA-2550049-1");
			pageTracker._initData();
			pageTracker._trackPageview();
		//]]>
		</script>
    </body>
</html>
