    {inny_speech_load section="manual_pdf"}
    
    <table border="0">
    <tr>
        <td style="padding:0px 30px;">
            <span style="font-size:16px;font-weight:bold;">{inny_get_speech seccion="manual_pdf" clave="pdf_primeros_pasos"}</span><br />
            {inny_get_speech seccion="manual_pdf" clave="pdf_primeros_pasos_1"}
        </td>
    </tr>
    <tr>
        <td style="padding:0px 30px;text-align:center;font-weight:bold">
            http://{$daoSitio->url|replace:'http://':''}
        </td>
    </tr>
    <tr>
        <td style="padding:5px 50px;">
            <img src="images/manual_PDF/inny_login.jpg" width="680" />
        </td>
    </tr>
    <tr>
        <td style="padding:0px 30px;">
            {inny_get_speech seccion="manual_pdf" clave="pdf_primeros_pasos_2"}
        </td>
    </tr>    
    <tr>
        <td style="padding:5px 50px;">
            <img src="images/manual_PDF/inny_misitio.jpg" width="680" />
        </td>
    </tr> 
    <tr>
        <td style="padding:0px 30px;">
            {inny_get_speech seccion="manual_pdf" clave="pdf_primeros_pasos_3"}
        </td>
    </tr> 
    <tr>
        <td style="padding:5px 50px;">
            <img src="images/manual_PDF/inny_misitio_interfaz.jpg" width="680" />
        </td>
    </tr>    
    <tr>
        <td style="padding:0px 30px;">
            <span style="font-size:16px;font-weight:bold;">{inny_get_speech seccion="manual_pdf" clave="pdf_menu"}
            {inny_get_speech seccion="manual_pdf" clave="pdf_menu_1"}
        </td>
    </tr>
    <tr>
        <td style="padding:5px 50px;">
            <img src="images/manual_PDF/inny_menu.jpg" width="680" />
        </td>
    </tr>     
    <tr>
        <td style="padding:10px 30px;">
            <table>
            <tr>
                <td style="padding-left:30px;vertical-align:top;">
                    <img src="images/manual_PDF/inny_boton_usuarios.jpg" width="200" />
                </td>
                <td style="padding-right:30px;">
                    {inny_get_speech seccion="manual_pdf" clave="pdf_menu_2"}
                </td>                
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding:10px 30px;">
            <table>
            <tr>
                <td style="padding-left:30px;vertical-align:top;">
                    <img src="images/manual_PDF/inny_boton_categorias.jpg" width="200" />
                </td>
                <td style="padding-right:30px;">
                    {inny_get_speech seccion="manual_pdf" clave="pdf_menu_3"}
                </td>                
            </tr>
            </table>
        </td>
    </tr>     
    <tr>
        <td style="padding:5px 50px;">
            <img src="images/manual_PDF/inny_categorias.jpg" width="680" />
        </td>
    </tr>  
    <tr>
        <td style="padding:0px 30px;">
            {inny_get_speech seccion="manual_pdf" clave="pdf_menu_4"}
        </td>
    </tr>        
    <tr>
        <td style="padding:10px 30px;">
            <table>
            <tr>
                <td style="padding-left:30px;vertical-align:top;">
                    <img src="images/manual_PDF/inny_boton_servicios.jpg" width="200" />
                </td>
		        <td style="padding-right:30px;">
		            {inny_get_speech seccion="manual_pdf" clave="pdf_menu_5"}
		        </td>                
            </tr>
            </table>
        </td>
    </tr> 
    <tr>
        <td style="padding:0px 30px;">
            <span style="font-size:16px;font-weight:bold;">{inny_get_speech seccion="manual_pdf" clave="pdf_mis_contenidos"}</span><br />
            {inny_get_speech seccion="manual_pdf" clave="pdf_mis_contenidos_1"}
        </td>
    </tr>
    <tr>
        <td style="padding:20px 50px;">
            <img src="images/manual_PDF/inny_contenidos.jpg" width="680" />
        </td>
    </tr>
    <tr>
        <td style="padding:0px 30px;">
            {inny_get_speech seccion="manual_pdf" clave="pdf_mis_contenidos_2"}
        </td>
    </tr>
    <tr>
        <td style="padding:0px 30px;">
            {inny_get_speech seccion="manual_pdf" clave="pdf_mis_contenidos_3"}
        </td>
    </tr>    
    <tr>    
        <td style="padding:20px 30px;">
            <span style="font-size:16px;font-weight:bold;">{inny_get_speech seccion="manual_pdf" clave="pdf_mis_contenidos_sitio"}</span><br />
        </td> 
    </tr> 
    <tr>
        <td style="padding:0px 30px 20px 30px;">
            {inny_get_contenidos idSitio=$daoSitio->id_sitio assign="contenidos"}
		    {foreach from=$contenidos item="contenido"}
		        <b>{$contenido->getMetadataValue('nombre')}</b><br />
		        {assign var="tipo" value=$contenido->getMetadataValue('tipo')}
                {inny_get_speech seccion="manual_pdf" clave="pdf_mis_contenidos_sitio_tipo"}{$tipos[$tipo]}<br />    
		        {$contenido->getMetadataValue('ayuda')}<br /><br />
		    {/foreach}            
        </td>
    </tr>    
    <tr>
        <td style="padding:0px 30px;">
            <span style="font-size:16px;font-weight:bold;">{inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados"}</span><br />
            {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_1"}
        </td>
    </tr>
    <tr>
        <td style="padding:20px 50px;">
            <img src="images/manual_PDF/inny_listados.jpg" width="680" />
        </td>
    </tr>    
    <tr>
        <td style="padding:0px 30px;">
            {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_2"}
        </td>
    </tr>   
    <tr>
        <td style="padding:20px 50px;">
            <img src="images/manual_PDF/inny_listados_elementos.jpg" width="680" />
        </td>
    </tr>    
    <tr>
        <td style="padding:20px 50px;">
            <img src="images/manual_PDF/inny_listados_opciones.jpg" width="680" />
        </td>
    </tr> 
    <tr>
        <td style="padding:0px 30px;">
            {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_3"}
        </td>
    </tr>     
    <tr>
        <td style="padding:0px 30px;">
            {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_4"}
        </td>
    </tr>   
    <tr>    
        <td style="padding:20px 30px;">
            <span style="font-size:16px;font-weight:bold;">{inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_sitio"}</span><br />
        </td> 
    </tr> 
    {inny_get_listados idSitio=$daoSitio->id_sitio assign="listados"}
	{foreach from=$listados item="listado"}
	    <tr>
	        <td style="padding:20px 30px 0px 30px;">
	            {assign var="nombre_listado" value=$listado->getMetadataValue("nombre")}
		        <b><u>{$nombre_listado}</u></b>
		    </td>
		</tr>
		<tr>
            <td style="padding:0px 50px 20px 50px;">
		        {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_sitio_elemento"}
		        <i>{$listado->getMetadataValue("item")|dk_capitalize}</i><br />
		        {assign var="ayuda" value=$listado->getMetadataValue("ayuda")}
		        {if !empty($ayuda)}- {$ayuda}<br />{/if}
		        {assign var="parametros" value=$listado->getMetadataValue("parametros")}
		        {if $parametros|count > 0}
	                {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_sitio_parametros"}<br />
	                {if isset($parametros.limit)}{assign var="limit" value=true}{else}{assign var="limit" value=false}{/if}
	                {foreach from=$parametros key="clave" item="valor"}
	                    {if $clave == 'searchable'}{inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_sitio_searchable"}<br />
	                    {/if}
	                    {if $clave == 'limit'}
	                        {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_sitio_cantidad"}
	                        {if $valor == 'none'}{inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_sitio_cantidad_ilimitada"}{else}{$valor}{/if}<br />
	                    {/if}
	                {/foreach}
	                {if !$limit}
	                    {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_sitio_cantidad"}{inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_sitio_cantidad_ilimitada"}<br /><br />
	                {/if}
		        {/if}
		        {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_elemento_campos" assign="campos"}
	        </td>
	    </tr>
	    <tr>            
	        <td style="padding:0px 50px;">        	        
		        {$campos|replace:'%listado':$nombre_listado}
	        </td>
	    </tr>
	    {section name="archivos" start="0" step="1" loop=$listado->getMetadataValue("archivo","cantidad")}
	        <tr>        	
	            <td style="padding:10px 50px;">        
		            {assign var="i" value=$smarty.section.archivos.index+1}
		            {assign var="archivo_numero" value="archivo"|cat:$i}
		            {assign var="innyType" value=$listado->createInnyType("archivo",$i)}
		            {assign var="archivo_ayuda" value=$innyType->getMetadataValue("ayuda")}
		            <b>{$innyType->getMetadataValue("nombre")}</b><br />
		            {assign var="tipo" value=$innyType->getMetadataValue("tipo")}
	                {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_elemento_campos_tipo"}{$tipos[$tipo]}<br />	            
		            {if !empty($archivo_ayuda)}{$archivo_ayuda}<br />{/if}
		            <br />
		       </td> 
	        </tr>
	    {/section}
	    {section name="textos" start="0" step="1" loop=$listado->getMetadataValue("texto","cantidad")}
	        <tr>	     
	            <td style="padding:10px 50px;"> 
		            {assign var="i" value=$smarty.section.textos.index+1}
		            {assign var="texto_numero" value="texto"|cat:$i}
		            {assign var="innyType" value=$listado->createInnyType("texto",$i)}
		            {assign var="texto_ayuda" value=$innyType->getMetadataValue("ayuda")}
		            <b>{$innyType->getMetadataValue("nombre")}</b><br />
	                {assign var="tipo" value=$innyType->getMetadataValue("tipo")}	            
		            {inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_elemento_campos_tipo"}{$tipos[$tipo]}<br />
		            {if !empty($texto_ayuda)}<i>{$texto_ayuda}</i><br />{/if}
		            <br />
	            </td>  
	        </tr>
	    {/section}
	    {if $listado->getMetadataValue("contador","cantidad") > 0}
	        <tr>            
	            <td style="padding:0px 50px;">                  
	                <b>{inny_get_speech seccion="manual_pdf" clave="pdf_mis_listados_elemento_contadores"}</b>
	            </td>
	        </tr>	    
	    {/if}
	    {section name="contadores" start="1" step="1" loop=$listado->getMetadataValue("contador","cantidad")+1}
	        <tr>         
	            <td style="padding:5px 50px 10px 70px;"> 	        
		            {assign var="i" value=$smarty.section.contadores.index}
		            {assign var="contador_numero" value="contador"|cat:$i}
		            {assign var="contador_ayuda" value=$listado->getMetadataValue("contador",$contador_numero,"ayuda")}
		            <b>{$listado->getMetadataValue("contador",$contador_numero,"nombre")}</b><br />
		            {if !empty($contador_ayuda)}{$contador_ayuda}<br />{/if}
	                <br />
	            </td>	        
	        </tr>  
	    {/section}
    {/foreach}
    </table>