<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-AR" lang="es-AR">
    {inny_speech_load section="application"}
    {inny_speech_load section="metadata"}
    {inny_speech_load section="common"}
    <head>
        <title>{#application_name#} | {#metadata_tituloPagina#}</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="content-language" content="es-ar" />
        <meta name="copyright" content="Copyright 2007-{$smarty.now|date_format:"%Y"}. Dokko Group All Rights Reserved." />
        {dk_include file="js/dk.common.js"}
        {dk_include file="js/inny.common.js"}
        {dk_include_target}
    </head>
    <body>
        <center>
        <div id="mainContainer">
            <!-- ========================= HEADER ========================== -->
            <div id="div_header">
                <div id="header_logo"><a href="http://clientes.dokkogroup.com.ar/" title="{#application_name#}"><img src="images/inny_logo.jpg" alt="{#application_name#}"></a></div>
                <div id="header_section"><img src="images/header_bar.gif" alt="header"></div>
            </div>

            <!-- ======================== CONTENT ========================== -->
            <div id="centralContent">
                <center>
                {dk_include file="styles/common.css" inline="false"}

                <div class="box alert">
                    {#metadata_alertaMensaje#}
                    <br />
                    <br />
                    {dk_hasmessages type="ERROR" assign="hasErrorMessages"}
                    {if $hasErrorMessages}
                        <ul>
                        {dk_showmessages type="ERROR"}
                            <li class="{$dkm_type|lower}">{$dkm_message}</li>
                        {/dk_showmessages}
                        </ul>
                        <br />
                    {/if}
                    {dk_hasmessages type="WARNING" assign="hasWarningMessages"}
                    {if $hasWarningMessages}
                        <ul>
                        {dk_showmessages type="WARNING"}
                            <li class="{$dkm_type|lower}">{$dkm_message}</li>
                        {/dk_showmessages}
                        </ul>
                    {/if}
                </div>
                <br />
                <a class="boton enter" href="misitio.php" title="{#common_continue#}">{#common_continue#}</a>
                <br />
                <br />
                </center>
            </div>
            <!-- ============================ FOOTER =========================== -->
            <div id="mainFooter">
                {assign var="ano_actual" value=$smarty.now|date_format:"%Y"}
                {capture assign="dokkoweb"}<a href="http://www.dokkogroup.com.ar" title="Dokko Group" target="_blank">Dokko Group</a>{/capture}
                {#footer_copyright#|replace:"%a�o_actual":$ano_actual|replace:"%dokkoweb":$dokkoweb}
            </div>
        </div>
        </center>
        <script type="text/javascript">
        //<![CDATA[
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        //]]>
        </script>
        <script type="text/javascript">
        //<![CDATA[
            var pageTracker = _gat._getTracker("UA-2550049-1");
            pageTracker._initData();
            pageTracker._trackPageview();
        //]]>
        </script>
    </body>
</html>