{inny_speech_load section="common"}
{inny_speech_load section="sitio"}
{* ========================================================================== *}
{inny_get_sitio_logueado assign="daoSitio"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#sitio_tituloPagina#|dk_capitalize}
{dk_include file="styles/misitio.css" inline="false"}
{* ========================================================================== *}
<br />
<table class="misitio" cellpadding="4" cellspacing="0">
    <tbody>
        <tr>
            <td class="label">{#sitio_nombreSitio#}:</td>
            <td class="data">{$daoSitio->nombre_sitio}</td>
        </tr>
        <tr>
            <td class="label">{#sitio_url#}:</td>
            <td class="data">{$daoSitio->url}</td>
        </tr>
        <tr>
            <td class="label">{#sitio_username#}:</td>
            <td class="data">{$daoSitio->username}</td>
        </tr>
        <tr>
            <td class="label">{#sitio_encargadoNyA#}:</td>
            <td class="data">{$daoSitio->apellido}, {$daoSitio->nombre_encargado}</td>
        </tr>
        <tr>
            <td class="label">{#sitio_telefono#}:</td>
            <td class="data">{$daoSitio->telefono}</td>
        </tr>
        <tr>
            <td class="label">{#sitio_direccion#}:</td>
            <td class="data">{$daoSitio->direccion}</td>
        </tr>
        <tr>
            <td class="label">{#sitio_email#}:</td>
            <td class="data">{$daoSitio->email|default:#sitio_defaultEmail#}</td>
        </tr>
        {if $daoSitio->getConfig("admin_configs") == "1"}
            <tr>
                <td class="label">{#sitio_gaCodigo#}:</td>
                <td class="data">{$daoSitio->ga_codigo|default:#sitio_defaultGaCodigo#}</td>
            </tr>
            <tr>
                <td class="label">{#sitio_enMantenimiento#}:</td>
                <td class="data">{if $daoSitio->mantenimiento == "1"}{#common_yes#}{else}{#common_no#}{/if}</td>
            </tr>
            <tr class="borderTop">
                <td class="label">{#sitio_smtpFromName#}:</td>
                <td class="data" style="text-align:left;">{$daoSitio->smtp_data.fromname|default:#common_empty#}</td>
            </tr>
            <tr>
                <td class="label">{#sitio_emailContacto#}:</td>
                <td class="data">{$daoSitio->smtp_data.contacto|default:#common_empty#}</td>
            </tr>
            <tr>
                <td class="label">{#sitio_smtpHost#}:</td>
                <td class="data">{$daoSitio->smtp_data.host|default:#common_empty#}</td>
            </tr>
            <tr>
                <td class="label">{#sitio_smtpUsername#}:</td>
                <td class="data">{$daoSitio->smtp_data.username|default:#common_empty#}</td>
            </tr>
            <tr>
                <td class="label">{#sitio_smtpProtocol#}:</td>
                <td class="data">{$daoSitio->smtp_data.protocol|default:#common_empty#}</td>
            </tr>
            <tr>
                <td class="label">{#sitio_smtpPort#}:</td>
                <td class="data">{$daoSitio->smtp_data.port|default:#common_empty#}</td>
            </tr>
        {/if}
    </tbody>

    <!-- ============================== BOTONES ============================ -->
    <tfoot>
        <tr class="buttons borderTop">
            <td colspan="2" class="submit">
                <input type="button" class="submit" value="{#sitio_editarDatos#}" onclick="javascript:Denko.redirect('misitio_edit.php');" />
            </td>
        </tr>
    </tfoot>
</table>
<br />
<br />
{* ========================================================================== *}
{include file="footer.tpl" section="sitio"}