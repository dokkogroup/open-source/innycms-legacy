{inny_speech_load section="common"}
{inny_speech_load section="sitio"}
{inny_speech_load section="password"}
{inny_speech_load section="error"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#sitio_tituloPagina#|dk_capitalize}
{dk_include file="styles/misitio.css" inline="false"}
{dk_include file="js/inny.form.validate.js" inline="false"}
{dk_include file="js/inny.clientes.sitio.js" inline="false"}
{* ========================================================================== *}
{dk_hasmessages assign="hasMessages"}
{if $hasMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{* ========================================================================== *}
<br />
<form name="misitio" action="misitio_changepass.php" method="post" onsubmit="javascript: return Inny_Sitio.validarCambioPassword();">
    <table class="misitio" cellpadding="4" cellspacing="0">
        <tr>
            <td class="label">{#password_actual#}:</td>
            <td class="data"><input type="password" class="text" id="password" name="password" /></td>
        </tr>
        <tr>
            <td class="label">{#password_nueva#}:</td>
            <td class="data"><input type="password" class="text" id="newpass" name="newpass" /></td>
        </tr>
        <tr>
            <td class="label">{#password_confirmarNueva#}:</td>
            <td class="data"><input type="password" class="text" id="confirm_newpass" name="confirm_newpass" /></td>
        </tr>
        <tr class="buttons">
            <td class="cancel"><input type="button" class="cancel" value="{#common_cancel#}" onclick="javascript:Denko.redirect('misitio_edit.php');" /></td>
            <td class="submit"><input type="submit" class="submit" value="{#password_editar#}" /></td>
        </tr>
    </table>
</form>

<script type="text/javascript">
//<![CDATA[
    Inny_ValidateForm.cargarSpeechs({ldelim}
        "input" : {ldelim}
            "password" : "{#password_actual#}",
            "newpass" : "{#password_nueva#}",
            "confirm_newpass" : "{#password_confirmarNueva#}"
        {rdelim},
        "message" : {ldelim}
            "error_campoRequerido" : "{#error_campoRequerido#}",
            "error_camposIguales" : "{#password_errorCamposIguales#}"
        {rdelim}
    {rdelim});
    document.getElementById("password").focus();
//]]>
</script>
{* ========================================================================== *}
{include file="footer.tpl" section="sitio"}