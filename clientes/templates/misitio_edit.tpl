{inny_speech_load section="common"}
{inny_speech_load section="sitio"}
{inny_speech_load section="password"}
{inny_speech_load section="error"}
{* ========================================================================== *}
{inny_get_sitio_logueado assign="daoSitio"}
{assign var="admin_configs" value=$daoSitio->getConfig("admin_configs")}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#sitio_tituloPagina#|dk_capitalize}
{dk_include file="styles/misitio.css" inline="false"}
{dk_include file="js/inny.clientes.sitio.js" inline="false"}
{dk_include file="js/inny.form.validate.js" inline="false"}
{* ========================================================================== *}
{dk_hasmessages assign="hasOkMessages" type="OK"}
{if $hasOkMessages}
    {include file="messagebox_okmessages.tpl"}
{/if}
{dk_hasmessages assign="hasErrorMessages" type="ERROR"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{* ========================================================================== *}
<br />
<form name="misitio" action="misitio_edit.php" method="post" onsubmit="javascript: return Inny_Sitio.validate();">
    <table class="misitio" cellpadding="4" cellspacing="0">
        <tbody>
            {if $admin_configs == "1"}
                <tr>
                    <td class="label">{#sitio_nombreSitio#}:</td>
                    <td class="data"><input type="text" class="text" id="nombre_sitio" name="nombre_sitio" value="{$daoSitio->nombre_sitio}" /></td>
                </tr>
                <tr>
                    <td class="label">{#sitio_url#}:</td>
                    <td class="data"><input type="text" class="text" id="url" name="url" value="{$daoSitio->url}" /></td>
                </tr>
            {/if}
            <tr>
                <td class="label">{#sitio_encargadoApellido#}:</td>
                <td class="data"><input type="text" class="text" id="apellido" name="apellido" value="{$smarty.post.apellido|default:$daoSitio->apellido}" /></td>
            </tr>
            <tr>
                <td class="label">{#sitio_encargadoNombre#}:</td>
                <td class="data"><input type="text" class="text" id="nombre_encargado" name="nombre_encargado" value="{$smarty.post.nombre_encargado|default:$daoSitio->nombre_encargado}" /></td>
            </tr>
            <tr>
                <td class="label">{#password#}:</td>
                <td class="data"><a href="misitio_changepass.php" title="{#password_cambiar#}">{#password_cambiar#}</td>
            </tr>
            <tr>
                <td class="label">{#sitio_telefono#}:</td>
                <td class="data"><input type="text" class="text" id="telefono" name="telefono" value="{$smarty.post.telefono|default:$daoSitio->telefono}" /></td>
            </tr>
            <tr>
                <td class="label">{#sitio_direccion#}:</td>
                <td class="data"><input type="text" class="text" id="direccion" name="direccion" value="{$smarty.post.direccion|default:$daoSitio->direccion}" /></td>
            </tr>
            <tr>
                <td class="label">{#sitio_email#}:</td>
                <td class="data"><input type="text" class="text" id="email" name="email" value="{$smarty.post.email|default:$daoSitio->email}" /></td>
            </tr>

            {if $admin_configs == "1"}
                <tr>
                    <td class="label">{#sitio_gaCodigo#}:</td>
                    <td class="data"><input type="text" class="text" id="ga_codigo" name="ga_codigo" value="{$smarty.post.ga_codigo|default:$daoSitio->ga_codigo}" /></td>
                </tr>
                <tr>
                    <td class="label">{#sitio_enMantenimiento#}:</td>
                    <td class="data"><input type="checkbox" id="mantenimiento" name="mantenimiento" {if $daoSitio->mantenimiento == "1"}checked="checked"{/if} /></td>
                </tr>


                <!-- ========================= SMTP ======================== -->
                <tr class="borderTop">
                    <td class="label">{#sitio_emailContacto#}:</td>
                    <td class="data"><input type="text" class="text" id="smtp_contacto" name="smtp_contacto" value="{$smarty.post.smtp_contacto|default:$daoSitio->smtp_data.contacto}" /></td>
                </tr>
                <tr>
                    <td class="label">{#sitio_smtpFromName#}:</td>
                    <td class="data"><input type="text" class="text" id="smtp_fromname" name="smtp_fromname" value="{$smarty.post.smtp_fromname|default:$daoSitio->smtp_data.fromname}" /></td>
                </tr>
                <tr>
                    <td class="label">{#sitio_smtpHost#}:</td>
                    <td class="data"><input type="text" class="text" id="smtp_host" name="smtp_host" value="{$smarty.post.smtp_host|default:$daoSitio->smtp_data.host}" /></td>
                </tr>
                <tr>
                    <td class="label">{#sitio_smtpUsername#}:</td>
                    <td class="data"><input type="text" class="text" id="smtp_username" name="smtp_username" value="{$smarty.post.smtp_username|default:$daoSitio->smtp_data.username}" /></td>
                </tr>
                <tr>
                    <td class="label">{#sitio_smtpPassword#}:</td>
                    <td class="data"><input type="text" class="text" id="smtp_password" name="smtp_password" value="{$smarty.post.smtp_password|default:$daoSitio->smtp_data.password}" /></td>
                </tr>
                <tr>
                    <td class="label">{#sitio_smtpProtocol#}:</td>
                    <td class="data"><input type="text" class="text" id="smtp_protocol" name="smtp_protocol" value="{$smarty.post.smtp_protocol|default:$daoSitio->smtp_data.protocol}" /></td>
                </tr>
                <tr>
                    <td class="label">{#sitio_smtpPort#}:</td>
                    <td class="data"><input type="text" class="text" id="smtp_port" name="smtp_port" value="{$smarty.post.smtp_port|default:$daoSitio->smtp_data.port}" /></td>
                </tr>
            {/if}

        </tbody>

        <!-- ============================ BOTONES ========================== -->
        <tfoot>
            <tr class="buttons borderTop">
                <td colspan="2" class="submit">
                    <input type="submit" class="submit" value="{#sitio_editarDatos#}" />
                    <input type="button" class="cancel" value="{#common_cancel#}" onclick="javascript:Denko.redirect('misitio.php');" />
                </td>
            </tr>
        </tfoot>
    </table>
</form>
<script type="text/javascript">
//<![CDATA[
    Inny_ValidateForm.cargarSpeechs({ldelim}

        "input" : {ldelim}

            // Datos b�sicos
            "nombre_sitio" : "{#sitio_nombreSitio#}",
            "url" : "{#sitio_url#}",
            "apellido" : "{#sitio_encargadoApellido#}",
            "nombre_encargado" : "{#sitio_encargadoNombre#}",
            "telefono" : "{#sitio_telefono#}",
            "direccion" : "{#sitio_direccion#}",
            "email" : "{#sitio_email#}",

            {if $admin_configs}

            // SMTP
            "smtp_contacto" : "{#sitio_emailContacto#}",
            "smtp_fromname" : "{#sitio_smtpFromName#}",
            "smtp_host" : "{#sitio_smtpHost#}",
            "smtp_username" : "{#sitio_smtpUsername#}",
            "smtp_password" : "{#sitio_smtpPassword#}",
            "smtp_protocol" : "{#sitio_smtpProtocol#}",
            "smtp_port" : "{#sitio_smtpPort#}"
            {/if}

        {rdelim},

        "message" : {ldelim}

            "error_campoRequerido" : "{#error_campoRequerido#}",
            "error_campoNoValido" : "{#error_campoNoValido#}"

        {rdelim}

    {rdelim});

    Inny_Sitio.init({ldelim}
        "admin_configs" : {$admin_configs},
    {rdelim});
//]]>
</script>
<br />
{* ========================================================================== *}
{include file="footer.tpl" section="sitio"}