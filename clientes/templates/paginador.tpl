{inny_speech_load section="paginador"}
{* ========================================================================== *}
<table class="paginador" cellpadding="0" cellspacing="0">
<tbody>
    <tr>
        {capture assign="paginador_begin"}<b>{$dkp_begin}</b>{/capture}
        {capture assign="paginador_end"}<b>{$dkp_end}</b>{/capture}
        {capture assign="paginador_results"}<b>{$dkp_results}</b>{/capture}
        <td class="cantidad">{#paginador_resultados#|replace:"%inicio":$paginador_begin|replace:"%fin":$paginador_end|replace:"%total":$paginador_results|dk_ucfirst}</td>
		<td class="select">
			{#paginador_paginas#|dk_capitalize}:
            {if $dkp_actual > 1}
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{dkp_hrefpage number=$dkp_actual-1}">&laquo;&nbsp;{#paginador_anterior#|dk_capitalize}</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{dkp_hrefpage number=1}">1</a>
            {/if}
            {if $dkp_actual > 3}
            &nbsp;...&nbsp;
            {/if}
            {if $dkp_actual > 2}
            &nbsp;<a href="{dkp_hrefpage number=$dkp_actual-1}">{$dkp_actual-1}</a>&nbsp;
            {/if}
            &nbsp;<b>{$dkp_actual}</b>&nbsp;
            {if $dkp_actual < $dkp_last-0} &nbsp;<a href="{dkp_hrefpage number=$dkp_actual+1}">{$dkp_actual+1}</a>&nbsp; {/if}
            {if $dkp_actual < $dkp_last-3}
            ...&nbsp;<a href="{dkp_hrefpage number=$dkp_last}">{$dkp_last}</a>&nbsp;
            {else}
            {if $dkp_actual < $dkp_last-1} &nbsp;<a href="{dkp_hrefpage number=$dkp_actual+2}">{$dkp_actual+2}</a>&nbsp; {/if}
            {if $dkp_actual < $dkp_last-2} &nbsp;<a href="{dkp_hrefpage number=$dkp_actual+3}">{$dkp_actual+3}</a>&nbsp; {/if}
            {/if}
            {if $dkp_actual < $dkp_last}
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="{dkp_hrefpage number=$dkp_actual+1}">{#paginador_siguiente#|dk_capitalize}&nbsp;&raquo;</a>
            {/if}
        </td>
    </tr>
</tbody>
</table>
{* ========================================================================== *}