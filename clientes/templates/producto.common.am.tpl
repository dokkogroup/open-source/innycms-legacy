{inny_speech_load section="producto"}
{inny_speech_load section="dse"}
{inny_speech_load section="error"}
{inny_speech_load section="archivo"}
{* ========================================================================== *}
{* JS *}
{dk_include file="js/inny.producto.js" inline="false"}
{dk_include file="js/overlib.js" inline="false"}

{* CSS *}
{dk_include file="styles/dokko_botones.css" inline="false"}
{dk_include file="styles/inny.producto_abm.css" inline="false"}
{dk_include file="styles/inny.clientes.common_preview.css" inline="false"}
{dk_include file="styles/inny.abm.css" inline="false"}
{* ========================================================================== *}
{dk_hasmessages assign="hasMessages"}
{if $hasMessages}{include file="messagebox_errormessages.tpl"}{/if}
{* ========================================================================== *}
<br />
<form id="producto_am_form" name="cambio" action="{$smarty.server.PHP_SELF|dk_basename}" method="post" enctype="multipart/form-data" onsubmit="javascript:return Inny_Producto.onSubmit();">

    <input type="hidden" name="action" value="{$action}" />
    {if $action == "add"}
        <input type="hidden" name="id_listado" value="{$daoListado->id_listado}" />
    {elseif $action == "edit"}
        <input type="hidden" name="id_producto" value="{$daoProducto->id_producto}" />

        {* PATCH PARA EL ABM DE USUARIOS *}
        {if $tipo_producto == "usuario"}
            <input type="hidden" name="id_usuario" value="{$daoProducto->id_producto}" />
        {/if}
    {/if}

<!-- ============================ SUBMIT =================================== -->

{include file="producto_am.submit.tpl" continueButton=false}

<!-- ===================== SPEECH CAMPOS REQUERIDOS ======================== -->

{capture assign="html_asterisco"}<span style="color:#d02d2d; font-size:12px;">(*)</span>{/capture}
<span style="font: normal 11px tahoma,arial;">{#producto_speechRequerido#|replace:"%asterisco":$html_asterisco}</span>

<!-- =========================== INICIALIZACION ============================ -->

<script type="text/javascript">
//<![CDATA[
    Inny_Producto.init({ldelim}
        "contador_cantidad" : {$contador_cantidad}
    {rdelim});
//]]>
</script>

<!-- ================================= ARCHIVOS =========================== -->
{assign var="cantArchivos" value=$daoListado->getMetadataValue("archivo","cantidad")}
{if $cantArchivos > 0}

    <div id="producto_archivos">

        {assign var="cantArchFijos" value=$daoListado->cantArchivosFijos}
        {assign var="sortablesInfo" value=$daoListado->getSortablesInfo()}
        {assign var="cantArchAttach" value=$daoProducto->getCantidadTemporales()}
        {assign var="archivo_tipo_capitalized" value=$sortablesInfo.tipo|dk_capitalize}

        {* ================= JS PARA ELIMINAR/CAMBIAR POSICION ================== *}
        {if $action == "edit"}
        <script type="text/javascript">
        //<![CDATA[
            Inny_Producto.setSettings("filesInfo",{$filesInfo});
            Inny_Producto.setSettings("sortableFilesInfo",{ldelim}"cantidad" : {$sortablesInfo.cantidad},"tipo" : "{$sortablesInfo.tipo}","nombre" : "{$sortablesInfo.nombre}"{rdelim});
            Inny_Producto.setSettings("speechs",{ldelim}"confirmDelete":"{#archivo_confirmacionEliminar#}"{rdelim});
        //]]>
        </script>
        {/if}

        {* ================= ARCHIVOS CON POSICI�N EST�TICA ================= *}
        {if $cantArchFijos > 0}

            <table id="tableFixedFiles" class="files" cellpadding="0" cellspacing="0">
                <tbody>
                {section name="fixblock" loop=$cantArchFijos+1 start="1" step="1"}
                    {assign var="indice" value=$smarty.section.fixblock.index}
                    {assign var="archivo_numero" value="archivo"|cat:$indice}
                    {assign var="innyTypeFile" value=$daoListado->createInnyType("archivo",$indice)}
                    {assign var="archivo_nombre" value=$innyTypeFile->getMetadataValue("nombre")}
                    {assign var="archivo_ayuda" value=$innyTypeFile->getMetadataValue("ayuda")}
                    {assign var="archivo_requerido" value=$innyTypeFile->getMetadataValue("parametros","required")}
                    {assign var="daoProductoTemporal" value=$daoProducto->getProductoTemporal($indice)}
                    {if !empty($daoProductoTemporal)}
                        {assign var="id_archivo" value=$daoProductoTemporal->id_temporal}
                        {assign var="id_temporal" value=$daoProductoTemporal->id_temporal}
                        {inny_type_get_abm_speech assign="innyTypeFileAbmSpeech" innyType=$innyTypeFile}
                        {swp_get_fileinfo id=$daoProductoTemporal->id_temporal assign="fileinfo"}
                    {else}
                        {assign var="id_archivo" value=$indice}
                        {assign var="id_temporal" value=null}
                        {assign var="fileinfo" value=""}
                    {/if}

                    {assign var="archivo_tipo" value=$innyTypeFile->getMetadataValue("tipo")}
                    {inny_get_nombre_tipo type=$archivo_tipo assign="archivo_tipo_nombre"}
                    {assign var="contenido_tipo" value=$innyTypeFile->getMetadataValue("tipo")}
                    {inny_type_view assign="preview_data" innyType=$innyTypeFile value=$id_temporal size="tiny"}
                    {include file="producto_am_fileinput.tpl" archivo_fijo="true"  indice=$indice id_archivo=$id_temporal}
                {/section}
                </tbody>
            </table>
            <br />
        {/if}

        {* ================= ARCHIVOS CON POSICI�N VARIABLE ================= *}
        {assign var="cantArchivosOrdenables" value=$cantArchivos-$cantArchFijos}
        {if $cantArchivosOrdenables > 0}

            <table id="tableSortableFiles" class="files" cellpadding="0" cellspacing="0">
                <tbody>
                {assign var="sortables_counter" value=1}
                {section name="commonblock" loop=$cantArchivos+1 start=$cantArchFijos+1 step="1"}
                    {assign var="indice" value=$smarty.section.commonblock.index}
                    {assign var="innyTypeFile" value=$daoListado->createInnyType("archivo",$indice)}
                    {assign var="archivo_numero" value="archivo"|cat:$indice}
                    {assign var="archivo_tipo" value=$innyTypeFile->getMetadataValue("tipo")}
                    {assign var="archivo_nombre" value=$innyTypeFile->getMetadataValue("nombre")}
                    {assign var="archivo_ayuda" value=$innyTypeFile->getMetadataValue("ayuda")}
                    {assign var="archivo_requerido" value=$innyTypeFile->getMetadataValue("parametros","required")}
                    {assign var="daoProductoTemporal" value=$daoProducto->getProductoTemporal($indice)}
        			{if !empty($daoProductoTemporal)}
                        {assign var="id_archivo" value=$daoProductoTemporal->id_temporal}
                        {assign var="id_temporal" value=$daoProductoTemporal->id_temporal}
                        {inny_type_get_abm_speech assign="innyTypeFileAbmSpeech" innyType=$innyTypeFile}
                        {swp_get_fileinfo id=$daoProductoTemporal->id_temporal assign="fileinfo"}
                    {else}
                        {assign var="id_archivo" value=$indice}
                        {assign var="id_temporal" value=null}
                        {assign var="fileinfo" value=""}
                    {/if}

                    {assign var="archivo_tipo" value=$innyTypeFile->getMetadataValue("tipo")}
                    {inny_get_nombre_tipo type=$archivo_tipo assign="archivo_tipo_nombre"}
                    {assign var="contenido_tipo" value=$innyTypeFile->getMetadataValue("tipo")}
                    {inny_type_view assign="preview_data" innyType=$innyTypeFile value=$id_temporal size="tiny"}
                    {include file="producto_am_fileinput.tpl" archivo_fijo="false"  indice=$indice id_archivo=$id_temporal cant_archivos=$cantArchivos-$cantArchFijos}
                    {assign var="sortables_counter" value=$sortables_counter+1}
                {/section}
                </tbody>
            </table>
            <br />
        {/if}
    </div>
{/if}

<!-- ======================================================================= -->

<script type="text/javascript">
//<![CDATA[
    Inny_Producto.setSearchable({if $daoSitio->soportaDSE() && $listado_searchable == "true"}true{else}false{/if});
//]]>
</script>

<!-- ================================ TEXTOS =============================== -->
{assign var="texto_cantidad" value=$daoListado->getMetadataValue("texto","cantidad")}
{if $texto_cantidad > 0}
    <table class="changeReq" cellpadding="0" cellspacing="5">
        <tbody>
		{section name="textos" start="1" loop=$texto_cantidad+1}
    		{assign var="indice_orden" value=$smarty.section.textos.index-1} 
    		{assign var="i" value=$textos_ordenables[$indice_orden]|default:$smarty.section.textos.index}
            {assign var="texto_numero" value="texto"|cat:$i}
            {assign var="innyType" value=$daoListado->createInnyType("texto",$i)}
            {assign var="innyTypeValue" value=$smarty.post.$texto_numero|default:$daoProducto->$texto_numero}
            {assign var="texto_ayuda" value=$innyType->getMetadataValue("ayuda")}
            <tr>
                <td class="description" style="text-transform:capitalize;">
                    <div class="description_div">
                    {$innyType->getMetadataValue("nombre")|dk_lower} 
                    {if $innyType->getMetadataValue("parametros","required") == "true"}&nbsp;<span style="font: normal 10px tahoma,helvetica,verdana,arial; color:#d02d2d;">(*){/if}
                    {if $texto_ayuda}<div class="btn_ayuda" onMouseOver="return overlib('{$texto_ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();"><img src="images/help_btn.gif" alt="" title=""></div>{/if}
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding:4px;">
                    {if $daoSitio->getMetadataValue("multilang","enabled") == "1" && $innyType->getMetadataValue("parametros","multilang") == "true"}
                        {foreach from=$daoSitio->getMetadataValue("multilang","languages") key="language_code" item="language_name"}
                            {assign var="post_varname" value=$texto_numero|cat:"_"|cat:$language_code}
                            {assign var="texto_default" value=$daoProducto->$texto_numero|sw_translate:$language_code}
                            {assign var="texto_value" value=$smarty.post.$post_varname|default:$texto_default}
                            <span style="font:bold 12px arial; ">{$language_name}:</span>
                            <br />
                            {inny_type_html_input innyType=$innyType name=$post_varname value=$texto_value class="abmInput text" style="width:100%;"}
                            <br />
                        {/foreach}
                    {else}
                        {$innyType->setValue($innyTypeValue)}
                        {inny_type_html_input innyType=$innyType name=$texto_numero value=$smarty.post.$texto_numero|default:$innyType->value class="abmInput text" style="width:100%;"}
                        <br />
                    {/if}
                </td>
            </tr>
        {/section}
        </tbody>
    </table>
{/if}

<!-- ================= SELECION DE CATEGOR�AS PARA EL DSE ================= -->

{if $daoSitio->soportaDSE() && $listado_searchable == "true"}
    {dk_include file="styles/inny.dse.css" inline="false"}
    {dk_include file="js/inny.dse.js" inline="false"}
    <table class="changeReq" cellpadding="0" cellspacing="5">
        <tbody>
            <tr>
                <td class="description" style="text-transform:capitalize;">{#dse_categorias#}</td>
            </tr>
            <tr>
                <td>
                {assign var="dse_settings" value=$daoSitio->getConfig("dse")}
                {if !empty($dse_settings.so_field0.values)}
                    <div style="position:relative;width:auto;height:auto;padding:10px 20px 5px 20px;">
                        <span style="text-transform:capitalize; color:#999999; font: bold 12px arial,tahoma,verdana;">{$dse_settings.so_field0.name|default:#dse_soFieldNombreDefault#}:&nbsp;</span>
                        <select id="so_field_0" name="so_field_0" style="font: normal 11px tahoma,arial; border: 1px solid #dddddd;">
                            {inny_dse_htmloptions_so_field nro="0" selected=$daoProducto->so_field_0}
                        </select>
                        {if !empty($dse_settings.so_field0.ayuda)}<div class="btn_ayuda" style="top:10px;right:5px;" onMouseOver="return overlib('{$dse_settings.so_field0.ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();"><img src="images/help_btn.gif" alt="" title=""></div>{/if}                       
                    </div>
                    <br />
                {/if}
                {if !empty($dse_settings.so_field1.values)}
                    <div style="position:relative;width:auto;height:auto;padding:10px 20px 5px 20px;">
                        <span style="text-transform:capitalize; color:#999999; font: bold 12px arial,tahoma,verdana;">{$dse_settings.so_field1.name|default:#dse_soFieldNombreDefault#}:&nbsp;</span>
                        <select id="so_field_1" name="so_field_1" style="font: normal 11px tahoma,arial; border: 1px solid #dddddd;">
                            {inny_dse_htmloptions_so_field nro="1" selected=$daoProducto->so_field_1}
                        </select>
                        {if !empty($dse_settings.so_field1.ayuda)}<div class="btn_ayuda" style="top:10px;right:5px;" onMouseOver="return overlib('{$dse_settings.so_field1.ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();"><img src="images/help_btn.gif" alt="" title=""></div>{/if}
                    </div>
                    <br />
                {/if}

                <br />
                <table class="dse_categories" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="HTMLSelectCanvas">
                                <table cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                {inny_dse_options_category assign="inny_dse_options_level1" id_sitio=$daoSitio->id_sitio}
                                                <select id="dse_category_level1" name="dse_category_level1" size="18" disabled="disabled" onchange="javascript:Inny_DSE.loadSubcategories(1);">
                                                    {if !empty($inny_dse_options_level1)}
                                                        {html_options options=$inny_dse_options_level1}
                                                    {else}
                                                        <option value="null" label="{#dse_sinCategorias#}">{#dse_sinCategorias#}</option>
                                                    {/if}
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                //<[CDATA[
                                    Inny_DSE.init(
                                    {ldelim}
                                        "abmMode" : false,
                                        "id_sitio" : "{$daoSitio->id_sitio}",
                                        "maxLevels" : "{$smarty.const.INNY_DSE_MAX_LEVEL_CATEGORY}",
                                        "speech":{ldelim}
                                            "empty" : "{#dse_sinSubcategorias#}",
                                            "loading" : "{#common_loading#}...",
                                            "errorRequiredCategoryName" : "{#dse_errorNombreCategoriaRequerido#}",
                                            "errorExistsCategoryName" : "{#dse_errorExisteCategoriaMismoNombre#}",
                                            "errorCategoriaYaSeleccionada" : "{#dse_errorCategoriaYaSeleccionada#}",
                                            "noSelectedCategories" : "{#dse_sinCategoriasSeleccionadas#}",
                                            "errorSeleccionarCategoriaParaEliminar" : "{#dse_errorSeleccionarCategoriaParaEliminar#}",
                                            "errorAjaxConexion" : "{#error_ajaxConexion#}"
                                        {rdelim}
                                    {rdelim}
                                    );
                                    Inny_DSE.setupCategoryGroup(1);
                                //]]>
                                </script>
                            </td>

                            <td class="HTMLSelectCanvas">
                                {section name="dse_subcategories" start="2" loop=$smarty.const.INNY_DSE_MAX_LEVEL_CATEGORY+1 step="1"}
                                    {assign var="index" value=$smarty.section.dse_subcategories.index}
                                    <table cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <select id="dse_category_level{$index}" name="dse_category_level{$index}" size="5" style="width:250px;" disabled="disabled" {if $index < $smarty.const.INNY_DSE_MAX_LEVEL_CATEGORY} onchange="javascript:Inny_DSE.loadSubcategories({$index});"{/if}>
                                                        <option value="null" label="{#common_loading#}...">{#common_loading#}...</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br />
                                {/section}
                                <script type="text/javascript">
                                //<[CDATA[
                                    Inny_DSE.loadCategories(2);
                                //]]>
                                </script>
                            </td>
                        </tr>
                        {inny_dse_options_selected_categories assign="options_selected_categories" id_producto=$daoProducto->id_producto}
                        <tr>
                            <td colspan="2" class="abm" style="margin: 0 auto;">
                                <div style="padding:0px 20px 0px 20px;">
                                    <div style="text-align:right;">
                                        <input id="dse_category_select_leaf" class="button" type="button" value="{#dse_seleccionarCategoria#} &raquo;" style="font-weight:bold;" disabled="disabled" onclick="javascript:Inny_DSE.selectLeafCategory();" />
                                    </div>
                                    <br />
                                    <div style="text-align:right;">
                                        <select id="dse_category_selecteds" name="dse_category_selecteds[]" style="width:100%;" size="10" {if empty($options_selected_categories)}disabled="disabled"{/if} multiple="multiple">
                                            {if !empty($options_selected_categories)}
                                                {html_options options=$options_selected_categories}
                                            {else}
                                                <option value="null">{#dse_sinCategoriasSeleccionadas#}</option>
                                            {/if}
                                        </select>
                                    </div>
                                    <br />
                                    <div style="text-align:center;">
                                        <input class="button" type="button" value="{#dse_eliminarCategoria#}" style="font-weight:bold;" onclick="javascript:Inny_DSE.removeSelectedCategories();" />
                                        &nbsp;&nbsp;
                                        <input class="button" type="button" value="{#dse_eliminarTodaCategoria#}" style="font-weight:bold;" onclick="javascript:Inny_DSE.removeAllSelectedCategories();" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                </td>
            </tr>
        </tbody>
    </table>
{/if}

<!-- ============================ CARDINALIDADES ========================== -->

{foreach from=$tiene_cardinalidades key="nombre_listado_cardinal" item="metadata"}
	{if $metadata.display == 'true' || !isset($metadata.display)}
		{assign var="mostrar_cardinalidades" value=true}
	{/if}
{/foreach}
{if !empty($mostrar_cardinalidades)}
<div class="abmCanvas">
    <h2>{#producto_cardinalTitulo#|dk_capitalize}</h2>
    <div class="canvasContent">
        <br />
        <div class="abmTabs">
            <div id="cardinal_hidden_inputs" style="display:none; visibility:hidden;"></div> <!-- IE SUCKS PATCH -->
            <div class="abmTabs_head">
                {assign var="nombre_primer_listado" value=null}
                <ul class="tabs">
                {assign var="nombres_listados_cardinales" value=""}
                {foreach from=$tiene_cardinalidades key="nombre_listado_cardinal" item="metadata"}
                	{assign var="daoListadoCardinal" value=$daoSitio->getListadoPorNombre($nombre_listado_cardinal)}
                	{if $metadata.display == 'true' || !isset($metadata.display)}
	                	{if empty($nombre_primer_listado)}{assign var="nombre_primer_listado" value=$nombre_listado_cardinal}{/if}
	                    <li id="abmTabs_{$nombre_listado_cardinal}"><a href="javascript:InnyCardinal.mostrarSolapaRelacionListados('{$nombre_listado_cardinal}');">{$nombre_listado_cardinal}</a></li>
	                    {if !empty($nombres_listados_cardinales)}{assign var="nombres_listados_cardinales" value=$nombres_listados_cardinales|cat:","}{/if}
	                    {assign var="nombres_listados_cardinales" value=$nombres_listados_cardinales|cat:'"'|cat:$nombre_listado_cardinal|cat:'"'}
					{/if}
                {/foreach}
                </ul>
            </div>
            <div class="abmTabs_content">
                {assign var="relacion_seleccionada" value="true"}
                {foreach from=$tiene_cardinalidades key="nombre_listado_cardinal" item="metadata"}
                	{assign var="daoListadoCardinal" value=$daoSitio->getListadoPorNombre($nombre_listado_cardinal)}
                	{if $metadata.display == 'true' || !isset($metadata.display)}                
	                    <div id="innyCardinalSuperContainer_{$nombre_listado_cardinal}" style="display:{if $relacion_seleccionada == "true"}block{else}none{/if};">
	                        <table class="innerTable" id="abmTabs_contentTable_{$nombre_listado_cardinal}" cellpadding="2" cellspacing="2">
	                            <tbody>
	                                <tr>
	                                    <td>
	                                        <input class="abmTabs_inputText" name="innyCardinalSearchInput" type="text" id="innyCardinalSearchInputSinRel_{$nombre_listado_cardinal}" value="" />
	                                        &nbsp;
	                                        <input class="abmTabs_inputButton" type="button" id="innyCardinalSearchButtonSinRel_{$nombre_listado_cardinal}" value="{#common_search#}" onclick="javascript:InnyCardinal.buscar('{$nombre_listado_cardinal}');" />
	                                    </td>
	                                    <td>&nbsp;</td>
	                                </tr>
	                                <tr>
	                                    <td class="listContainer" id="innyCardinalListadoContainerSinRel_{$nombre_listado_cardinal}"><img src="images/loading_cardinal.gif" /></td>
	                                    <td class="listContainer" id="innyCardinalListadoContainerConRel_{$nombre_listado_cardinal}" style="height:340px;">
	                                    	<div style="position:relative;width:auto;height:340px;overflow-y:scroll;"><img src="images/loading_cardinal.gif" /></div>
                                  		</td>
	                                </tr>
                                    <tr>
                                        <td colspan="2" class="listContainer" id="innyCardinalListadoContainerPaginador_{$nombre_listado_cardinal}"><img src="images/loading_cardinal.gif" /></td>
                                    </tr>	                                
	                            </tbody>
	                        </table>
	                    </div>
                    {/if}
                    {assign var="relacion_seleccionada" value="false"}
                {/foreach}

                {* ARMO LA INFORMACION DE LOS LISTADOS PARA EL JAVASCRIPT *}
                {assign var="informacion_listados" value=""}
                {assign var="js_listados_cardinalidad" value=""}
                {assign var="cantidad_listados_cardinales" value=$tiene_cardinalidades|@count}
                {assign var="index" value="0"}
                {foreach from=$tiene_cardinalidades key="nombre_listado_cardinal" item="cardinalidades"}
                	{if $cardinalidades.display == 'true' || !isset($cardinalidades.display)}
	                    {assign var="daoListadoCardinal" value=$daoSitio->getListadoPorNombre($nombre_listado_cardinal)}
	                    {capture assign="js_code"}"{$nombre_listado_cardinal}" : {ldelim}"item":"{$daoListadoCardinal->getMetadataValue("item")}","inf":{$cardinalidades.inf},"sup":"{$cardinalidades.sup}"{rdelim}{/capture}
	                    {assign var="informacion_listados" value=$informacion_listados|cat:$js_code}
	                    {assign var="index" value=$index+1}
	                    {if $index < $cantidad_listados_cardinales}
	                        {assign var="informacion_listados"     value=$informacion_listados|cat:" , "}
	                    {/if}
	                    {if $daoListadoCardinal->getMetadataValue("cardinalidad",$daoListado->nombre)}
	                        {if $js_listados_cardinalidad != ""}
	                            {assign var="js_listados_cardinalidad" value=$js_listados_cardinalidad|cat:","}
	                        {/if}
	                        {capture assign="js_listados_cardinalidad_captured"}"{$daoListadoCardinal->nombre}":{ldelim}"inf":"{$daoListadoCardinal->getMetadataValue("cardinalidad",$daoListado->nombre,"inf")}","sup":"{$daoListadoCardinal->getMetadataValue("cardinalidad",$daoListado->nombre,"sup")}"{rdelim}{/capture}
	                        {assign var="js_listados_cardinalidad" value=$js_listados_cardinalidad|cat:$js_listados_cardinalidad_captured}
	                    {/if}
					{/if}
                {/foreach}

                <script type="text/javascript">
                //<[CDATA[
                    InnyCardinal.init({ldelim}
                        "id_producto" : {if !empty($daoProducto->id_producto)}{$daoProducto->id_producto}{else}null{/if},
                        "id_listado" : {$daoListado->id_listado},
                        "nombre_listado" : "{$daoListado->getMetadataValue("nombre")}",
                        "listados" : {ldelim}{$informacion_listados}{rdelim},
                        "listados_cardinalidad" : {ldelim}{$js_listados_cardinalidad}{rdelim},

                        "speech" : {ldelim}
                            "item_actual" : "{$listado_item}",
                            "delete" : "{#common_erase#}",
                            "add" : "{#common_add#}",
                            "ajax_error" : "{#error_ajaxConexion#}",
                            "emptyList" : "{#producto_cardinalListadoVacio#}",
                            "overlibInfoConRel" : "{#producto_cardinalProductoInfoConRelaciones#}",
                            "overlibInfoSinRel" : "{#producto_cardinalProductoInfoSinRelaciones#}",
                            "overlibInfoHelp" : "{#producto_cardinalProductoInfoAyuda#}",
                            "overlibInfoAlert" : "{#producto_cardinalProductoInfoAlerta#}",
                            "overlibAlertInfoSinRelacion" : "{#producto_cardinalAlertaInfoSinRelacion#}",
                            "overlibAlertInfoConRelacion" : "{#producto_cardinalAlertaInfoConRelacion#}"
                        {rdelim}
                    {rdelim});
                //]]>
                </script>
            </div>
        </div>
    </div>
</div>
<br />
{/if}

<!-- ============================== CONTADORES ============================ -->

{if $contador_cantidad > 0}
    <div class="abmCanvas">
        <h2>{#producto_contadores#|dk_capitalize}</h2>
        <div class="canvasContent">
        <table class="contadoresCanvas" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                {section name="contadores" start="1" loop=$contador_cantidad+1}
                    <td>
                    {assign var="contador_index" value=$smarty.section.contadores.index}
                    {assign var="contador_numero" value="contador"|cat:$contador_index}
                    {assign var="contador_valor" value=$daoProducto->$contador_numero|default:$daoListado->getMetadataValue("contador",$contador_numero,"parametros","default")}
                    {assign var="contador_ayuda" value=$daoListado->getMetadataValue("contador",$contador_numero,"ayuda")}
                    <span class="contadorLabel">{$daoListado->getMetadataValue("contador",$contador_numero,"nombre")}:</span>
                    {if $contador_ayuda}<a href="javascrip:void();" onMouseOver="return overlib('{$contador_ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();"><img src="images/help_btn.gif" alt="" title=""></a>{/if}
                    <table class="section_contadores" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td class="numberResetCanvas">
                                    <input class="numero" type="text" name="{$contador_numero}" value="{$smarty.post.$contador_numero|default:$contador_valor}" id="contador_numero{$contador_index}"/>
                                    <input class="reset" type="button" value="reset" onclick="javascript:InnyProducto_Contador.reset({$contador_index});" />
                                </td>
                                <td class="stepButtonCanvas">
                                    <a href="javascript:InnyProducto_Contador.stepUp({$contador_index});"><img src="images/arrow_up.gif" /></a><span style="font-size:4px;">&nbsp;&nbsp;</span><a href="javascript:InnyProducto_Contador.stepDown({$contador_index});"><img src="images/arrow_down.gif" /></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                {/section}
            </tr>
            </tbody>
        </table>
        </div>
    </div>
    <script type="text/javascript">
    //<![CDATA[
        InnyProducto_Contador.init({ldelim}
        "speechs" : {ldelim}
            "requiredValue" : "{#producto_errorContadorValorRequerido#}",
            "invalidFormat" : "{#producto_errorContadorValorNoValido#}"
        {rdelim},
        "config" : [
        {section name="contadores" start="1" loop=$contador_cantidad+1}
            {assign var="contador_index" value=$smarty.section.contadores.index}
            {assign var="contador_numero" value="contador"|cat:$contador_index}
            {assign var="contador_valor" value=$daoProducto->$contador_numero|default:$daoListado->getMetadataValue("contador",$contador_numero,"parametros","default")}

            {ldelim}
                'nombre': '{$daoListado->getMetadataValue("contador",$contador_numero,"nombre")}',
                'value': {$smarty.post.$contador_numero|default:$contador_valor},
                'parametros': {ldelim}'default':{$daoListado->getMetadataValue("contador",$contador_numero,"parametros","default")},'step':{$daoListado->getMetadataValue("contador",$contador_numero,"parametros","step")}{rdelim}
            {rdelim}
            {if $contador_index < $contador_cantidad},{/if}
        {/section}]
        {rdelim});
    //<![CDATA[
    //]]>
    </script>
    <br />
{/if}

<!-- ============================== SUBMIT ================================= -->

{include file="producto_am.submit.tpl" continueButton=true}

</form>