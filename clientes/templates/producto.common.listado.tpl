{inny_speech_load section="producto"}
{inny_speech_load section="listado_"|cat:$tipo_listado}
{* ========================================================================== *}
{assign var="daoSitio" value=$daoListado->getSitio()}
{* ========================================================================== *}
{assign var="listado_item" value=$daoListado->getMetadataValue("item")|lower}
{assign var="archivo_cantidad" value=$daoListado->getMetadataValue("archivo","cantidad")}
{assign var="texto_cantidad" value=$daoListado->getMetadataValue("texto","cantidad")}
{assign var="texto_prioridad" value=$daoListado->getMetadataValue("texto","prioridad")}
{* Obtengo la cantidad de textos que mostrar *}
{if $texto_prioridad == "none"}
    {assign var="texto_cantidad_mostrar" value=0}
{elseif $texto_cantidad < $texto_prioridad|@count}
    {assign var="texto_cantidad_mostrar" value=$texto_cantidad}
{else}
    {assign var="texto_cantidad_mostrar" value=$texto_prioridad|@count}
{/if}
{if $daoListado->tienePermiso("O") || $daoListado->tienePermiso("V") || $daoListado->tienePermiso("M") || $daoListado->tienePermiso("B")}
    {assign var="puede_aplicarse_accion_a_producto" value="true"}
{else}
    {assign var="puede_aplicarse_accion_a_producto" value="false"}
{/if}
{* ========================================================================== *}
{if $tipo_listado == "producto"}
    {assign var="speech_linkTitleVer" value=#listadoProducto_linkTitleVer#|replace:"%item":$listado_item}
    {assign var="speech_linkTitleEditar" value=#listadoProducto_linkTitleEditar#|replace:"%item":$listado_item}
    {assign var="speech_linkTitleEliminar" value=#listadoProducto_linkTitleEliminar#|replace:"%item":$listado_item}
    {assign var="speech_confirmarEliminar" value=#listadoProducto_confirmarEliminar#|replace:"%item":$listado_item}
{else}
    {assign var="speech_linkTitleVer" value=#listadoProducto_linkTitleVer#}
    {assign var="speech_linkTitleEditar" value=#listadoProducto_linkTitleEditar#}
    {assign var="speech_linkTitleEliminar" value=#listadoProducto_linkTitleEliminar#}
    {assign var="speech_confirmarEliminar" value=#listadoProducto_confirmarEliminar#}
{/if}
{* ========================================================================== *}
<table class="listado" cellpadding="0" cellspacing="0">

    <!-- =========================================================== -->
    <thead>
        <tr class="head">

            {* CHECKBOX DE MULTIACTION *}
            {if $daoListado->tienePermiso("B")}
                <td style="width:20px;">&nbsp;</td>
            {/if}

            {* VER LOS PRODUCTOS CON DISTINTO ORDEN *}
            <td class="producto_orden centered">
            {if $dkp_results > 1}
                <center>
                <div class="ordenamientos">
                    <div class="floatLeft"><a class="botonLink" href="{dko_url name="order_by_orden" order="asc"}"><img src="images/arrow_up.gif" class="orderArrow actionButton" /></a></div>
                    <div class="floatRight"><a class="botonLink" href="{dko_url name="order_by_orden" order="desc"}"><img src="images/arrow_down.gif" class="orderArrow actionButton" /></a></div>
                </div>
                </center>
            {else}
                &nbsp;
            {/if}
            </td>

            {* DETALLE DE LOS TEXTOS DEL PRODUCTO *}
            {if $texto_cantidad_mostrar > 0}
                <td>{#producto_listadoTextos#}</td>
            {/if}

            {* PREVIEW DEL ARCHIVO PRINCIPAL DEL PRODUCTO *}
            {if $archivo_cantidad > 0}
                {assign var="innyTypeFile" value=$daoListado->createInnyType("archivo",1)}
                <td class="producto_archivo {if $daoListado->getMetadataValue("archivo","archivo1","tipo") == "sound"}sound{/if}">{$innyTypeFile->getMetadataValue("nombre")|dk_lower|truncate:16}</td>
            {/if}

            {* ACCIONES DEL PRODUCTO *}
            {if $puede_aplicarse_accion_a_producto == "true"}
                <td class="acciones">{#common_options#}</td>
            {/if}

        </tr>
    </thead>

    <!-- =========================================================== -->
    <tbody>
    {dk_lister export="id_producto,texto1,texto2,posicion"}
        {dkl_getdao assign="daoProducto"}
        {assign var="daoProductoTemporal" value=$daoProducto->getProductoTemporal(1)}
        {if !empty($daoProductoTemporal)}
            {assign var="id_temporal" value=$daoProductoTemporal->id_temporal}
        {else}
            {assign var="id_temporal" value=0}
        {/if}
        <tr class="body">

            {* CHECKBOX DE MULTIACTION *}
            {if $daoListado->tienePermiso("B")}
                <td style="width:20px;">{dkma_checkbox}</td>
            {/if}

            {* NRO DE POSICI�N E ID DEL PRODUCTO *}
            <td class="producto_orden">
                {$posicion}
                <br />
                <br />
                <span class="id_producto">[ID: {$daoProducto->id_producto}]</span>
            </td>

            {* DETALLE DE LOS TEXTOS DEL PRODUCTO *}
            {if $texto_cantidad_mostrar > 0}
                <td class="producto_texto">
                    <ul class="detalles">
                    {section name="textos" loop=$texto_cantidad_mostrar}
                        {assign var="texto_campo" value="texto"|cat:$texto_prioridad[$smarty.section.textos.index]}
                        {assign var="innyTypeText" value=$daoListado->createInnyType("texto",$texto_prioridad[$smarty.section.textos.index])}
                        <li>
                            <b>{$innyTypeText->getMetadataValue("nombre")|dk_capitalize|truncate:25}:</b>
                            {if $daoSitio->getMetadataValue("multilang","enabled") == "1" && $innyTypeText->getMetadataValue("parametros","multilang") == "true"}
                                {assign var="language_code" value=$daoSitio->getMetadataValue("multilang","default")}
                                {assign var="innyTypeTextValue" value=$daoProducto->$texto_campo|sw_translate:$language_code}
                            {else}
                                {assign var="innyTypeTextValue" value=$daoProducto->$texto_campo}
                            {/if}
                            {include file="common_type_view.tpl" innyType=$innyTypeText value=$innyTypeTextValue size="tiny"}
                        </li>
                    {/section}
                    </ul>
                </td>
            {/if}

            {* PREVIEW DEL ARCHIVO PRINCIPAL DEL PRODUCTO *}
            {if $archivo_cantidad > 0}
                <td class="producto_archivo">
                    {include file="common_type_view.tpl" innyType=$innyTypeFile value=$id_temporal size="tiny"}
                </td>
            {/if}

            {* ACCIONES DEL PRODUCTO *}
            {if $puede_aplicarse_accion_a_producto == "true"}
                <td class="acciones centered">
                    <center>

                    {* CAMBIO DE POSICI�N DEL PRODUCTO EN EL LISTADO *}
                	{if $daoListado->tienePermiso("O") && $dkp_results > 1}
            	       {if $listado_parametro_order == "asc"}
                            {capture assign="sort_up"}href="{$url_action}?id_{$tipo_listado}={$id_producto}&action=up" title="{#common_up#}"{/capture}
                            {capture assign="sort_down"}href="{$url_action}?id_{$tipo_listado}={$id_producto}&action=down" title="{#common_down#}"{/capture}
                        {else}
                            {capture assign="sort_up"}href="{$url_action}?id_{$tipo_listado}={$id_producto}&action=down" title="{#common_down#}"{/capture}
                            {capture assign="sort_down"}href="{$url_action}?id_{$tipo_listado}={$id_producto}&action=up" title="{#common_up#}"{/capture}
                        {/if}
						{capture assign="sort_set_pos"}href="#" onclick="setNewPos('id_{$tipo_listado}','{$id_producto}','{$posicion}');"{/capture}
                        <div class="ordenamientos accion">
                            <div class="floatLeft"><a class="botonLink" {$sort_up}><img src="images/arrow_up.gif" alt="{#common_up#}" class="orderArrow actionButton" /></a></div>
                            <div class="floatLeft" style="text-align:center;width:16px;" ><a style="font-size:16px;color:#999;text-decoration:none;" {$sort_set_pos}>&#9688;</a></div>
                            <div class="floatRight"><a class="botonLink" {$sort_down}><img src="images/arrow_down.gif" alt="{#common_down#}" class="orderArrow actionButton" /></a></div>
                        </div>
                    {/if}

                    {* VER PRODUCTO *}
                    {if $daoListado->tienePermiso("V")}
                        <a class="botonLink" href="{$url_view}?action=view&id_{$tipo_listado}={$daoProducto->id_producto}" title="{$speech_linkTitleVer}"><div>{#common_view#}</div></a>
                    {/if}

                    {* EDITAR PRODUCTO *}
                    {if $daoListado->tienePermiso("M")}
                        <a class="botonLink" href="{$url_action}?id_{$tipo_listado}={$daoProducto->id_producto}&action=edit" title="{$speech_linkTitleEditar}"><div>{#common_edit#}</div></a>
                    {/if}

                    {* ELIMINAR PRODUCTO *}
                    {if $daoListado->tienePermiso("B")}
                        {capture assign="link_delete"}javascript:confirmAction('{$speech_confirmarEliminar}','{$url_action}?id_{$tipo_listado}={$daoProducto->id_producto}&action=delete');"{/capture}
                        <a class="botonLink" href="{$link_delete}" title="{$speech_linkTitleEliminar}"><div class="red">{#common_delete#}</div></a>
                    {/if}

                    </center>
                </td>
            {/if}

        </tr>
    {/dk_lister}
    </tbody>
</table>
{* ========================================================================== *}
{literal}
<script>
	function setNewPos(fieldName,id,pos){
		var newPos=prompt('INGRESE LA NUEVA POSICION:',pos);
		if(newPos=='' || newPos==null || newPos==pos) return;
		if(newPos<1) return;
		var url='{/literal}{$url_action}{literal}?'+fieldName+'='+id+'&action=setPos&pos='+newPos;
		window.location.href=url;
	}
</script>
{/literal}
{include file="paginador.tpl"}
