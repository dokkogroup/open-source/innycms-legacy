
<!-- =============================== ARCHIVO =============================== -->

{if $archivo_cantidad > 0}

    {section name="archivos" start="1" loop=$archivo_cantidad+1}
        {assign var="innyTypeFile" value=$daoListado->createInnyType("archivo",$smarty.section.archivos.index)}
        {assign var="daoProductoTemporal" value=$daoProducto->getProductoTemporal($smarty.section.archivos.index)}
        {if !empty($daoProductoTemporal)}
            {assign var="id_temporal" value=$daoProductoTemporal->id_temporal}
        {else}
            {assign var="id_temporal" value=null}
        {/if}
        <tr>
            <td class="description"><span style="font-weight:bold;color:#666666; text-transform:capitalize;">{$innyTypeFile->getMetadataValue("nombre")|dk_lower}</span></td>
        </tr>
        <tr>
            <td style="padding:10px; text-align:center; vertical-align:top;">{include file="common_type_view.tpl" innyType=$innyTypeFile value=$id_temporal size="medium"}</td>
        </tr>
    {/section}
{/if}

<!-- ================================ TEXTOS =============================== -->

{if $texto_cantidad > 0}
    {section name="textos" start="1" loop=$texto_cantidad+1}
        {assign var="indice_orden" value=$smarty.section.textos.index-1} 
        {assign var="i" value=$textos_ordenables[$indice_orden]|default:$smarty.section.textos.index}
		{assign var="texto_campo" value="texto"|cat:$i}
        {assign var="innyTypeText" value=$daoListado->createInnyType("texto",$i)}
        <tr>
            <td class="description">
                <span style="font-weight:bold; color:#666666; text-transform:capitalize;">{$innyTypeText->getMetadataValue("nombre")|dk_lower}</span>
            </td>
        </tr>

        <tr>
            <td style="padding:15px; font:normal 12px arial,tahoma;">
                {if $daoSitio->getMetadataValue("multilang","enabled") == "1" && $innyTypeText->getMetadataValue("parametros","multilang") == "true"}
                    {foreach from=$daoSitio->getMetadataValue("multilang","languages") key="language_code" item="language_label"}
                        {assign var="innyTypeValue" value=$daoProducto->$texto_campo|sw_translate:$language_code}
                        <b>{$language_label}:</b>
                        <div style="border: 1px solid #cccccc; padding:5px;">
                            {include file="common_type_view.tpl" innyType=$innyTypeText value=$innyTypeValue size="normal"}
                        </div>
                        <br />
                    {/foreach}
                {else}
                    {include file="common_type_view.tpl" innyType=$innyTypeText value=$daoProducto->$texto_campo size="normal"}
                {/if}
                <br />
            </td>
        </tr>
    {/section}
{/if}

<!-- ======================== CATEGORĶAS ASIGNADAS ========================= -->

{if $daoSitio->soportaDSE() && $listado_searchable == "true"}
    {inny_speech_load section="dse"}
    <tr>
        <td class="description">
            <span style="font-weight:bold;color:#666666; text-transform:capitalize;">{#dse_categoriasAsignadas#|dk_capitalize}</span>
        </td>
    </tr>
    <tr>
        <td style="padding:15px; font:normal 12px arial,tahoma; vertical-align:top;">
            {inny_dse_get_selected_categories assign="arreglo_categorias" id_producto=$daoProducto->id_producto}
            {if isset($arreglo_categorias) && $arreglo_categorias|@count > 0}
                {foreach from=$arreglo_categorias key="k" item="path_categoria"}
                    {$path_categoria}<br />
                {/foreach}
            {else}
                <span style="color:#dc3e63;">{#dse_sinCategoriasAsignadas#}</span>
            {/if}
        </td>
    </tr>
{/if}