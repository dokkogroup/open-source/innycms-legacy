<table class="changeReq" cellpadding="0" cellspacing="5">
<tbody>
    <tr>
        {if $action == "add" && $continueButton && !$daoListado->alcanzoLimiteProductosPermitido(1)}
            <td style="font: 11px arial,tahoma;">
                <input type="checkbox" name="continue" {if (isset($smarty.get.continue) && $smarty.get.continue == "true") || (!empty($smarty.post.continue) && $smarty.post.continue == "on")}checked="checked" {/if} />
                {#producto_agregarContinuar#|replace:"%item":$listado_item}
            </td>
        {/if}
        <td class="buttons" align="right" style="border:none;">
            {if (!$continueButton)}
                <div class="dateInformation">
                    {if !empty($daoProducto->aud_ins_date)}<p><span>{#common_date_ins#}</span>: {$daoProducto->aud_ins_date}.</p>{/if}
                    {if !empty($daoProducto->aud_upd_date) && $daoProducto->aud_upd_date!='0000-00-00 00:00:00'}<p><span>{#common_date_upd#}</span>: {$daoProducto->aud_upd_date}.</p>{/if}
                </div>
            {/if}
            <div class="contentBtnContainer" style="width:150px;" >

                <!-- BOTON CANCELAR -->
                <div class="btnDiscardIcon" style="right:60px; cursor:pointer; text-align:center;"><img src="images/cancel.gif" alt="{#common_cancel#}" /></div>
                <div class="contentBtnDiscard" style="right:0px; cursor:pointer;"><input type="button" value="{#common_cancel#}" onclick="javascript:Denko.redirect('{$linkBack}');" /></div>

                <!-- BOTON SUBMIT -->
                <div class="btnModifyIcon" style="right:170px; cursor:pointer; text-align:center;"><img src="images/modify.gif" /></div>
                <div class="contentBtnModify" style="right:100px; cursor:pointer;"><input type="submit" value="{$submitValue}" style="text-transform:capitalize;"/></div>
            </div>
        </td>
    </tr>
</tbody>
</table>