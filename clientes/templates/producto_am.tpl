{inny_speech_load section="common"}
{inny_speech_load section="producto"}
{* ========================================================================== *}
{inny_get_sitio_logueado assign="daoSitio"}
{assign var="action" value=$smarty.get.action|default:$smarty.post.action}
{assign var="listado_item" value=$daoListado->getMetadataValue("item")|dk_lower}
{assign var="listado_searchable" value=$daoListado->getMetadataValue("parametros","searchable")}
{assign var="textos_ordenables" value=$daoListado->getMetadataValue("texto","orden")}
{assign var="contador_cantidad" value=$daoListado->getMetadataValue("contador","cantidad")}
{assign var="tiene_cardinalidades" value=$daoListado->getCardinalidadesHaciaOtrosListados()}
{* ========================================================================== *}
{if $action == "add"}
    {assign var="id_listado" value=$smarty.post.id_listado|default:$smarty.get.id_listado}
    {assign var="pageTitle" value=#producto_agregar#|replace:"%item":$listado_item}
    {assign var="submitValue" value=#common_add#}
{elseif $action == "edit"}
    {assign var="id_producto" value=$smarty.post.id_producto|default:$smarty.get.id_producto}
    {assign var="pageTitle" value=#producto_editar#|replace:"%item":$listado_item}
    {assign var="submitValue" value=#common_edit#}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle|dk_capitalize}
{include file="producto.common.am.tpl" tipo_producto = "producto"}
{include file="footer.tpl" section="producto"}
{* ========================================================================== *}