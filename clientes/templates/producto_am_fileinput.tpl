{inny_speech_load section="archivo"}
{* ========================================================================== *}
<tr id="{if $archivo_fijo == "true"}fixed{else}sortable{/if}Files_{if !empty($id_archivo)}{$id_archivo}{else}none{/if}">
    <td class="file_content">

        {* EN CASO QUE EL TIPO DE ARCHIVO SEA FLASH *}
        {if $contenido_tipo == "flash"}
            {if isset($preview_data)}
                <embed class="contenido_flash" type="application/x-shockwave-flash" src="{$preview_data.url_flash}" wmode="transparent" quality="high" style="width:80px; height:80px; " />
            {else}
                <img src="images/listflash_off.gif" alt="{$innyTypeFile->getDefaultValue()}" />
            {/if}

        {* EN CASO QUE EL TIPO DE ARCHIVO SEA IMAGE *}
        {elseif $contenido_tipo == "image"}
            {if isset($preview_data)}
                <img src="{$preview_data.url_thumb}" alt="{$contenido_nombre}" />
            {else}
                <img src="images/listimage_off.gif" alt="{$innyTypeFile->getDefaultValue()}" />
            {/if}

        {* EN CASO QUE EL TIPO DE ARCHIVO SEA SOUND *}
        {elseif $contenido_tipo == "sound"}
            {if isset($preview_data)}
                <embed class="contenido_sound" type="application/x-shockwave-flash" src="player/dewplayer-mini.swf?mp3={$preview_data.url_sound}.sound&showtime=0" allowScriptAccess="sameDomain" name="dewplayer" wmode="transparent" style="width:160px; height:20px;" />
            {else}
                <img src="images/listsound_off.gif" alt="{$innyTypeFile->getDefaultValue()}" />
            {/if}

        {* EN CASO QUE EL TIPO DE ARCHIVO SEA CUALQUIER OTRO *}
        {else}
            {if !empty($id_archivo)}
                <img src="images/listfile_on.gif" alt="{#archivo#}" />
            {else}
                <img src="images/listfile_off.gif" alt="{$innyTypeFile->getDefaultValue()}" />
            {/if}
        {/if}
    </td>
    <td class="file_data" style="vertical-align:middle; padding:5px;">

        {* NOMBRE DEL CAMPO + INPUT *}
        <span class="filetype">{$archivo_nombre|lower} {if $archivo_requerido == "true"}{$html_asterisco}{/if}</span><br />
        <input type="file" name="{$archivo_numero}" />

        {* NOMBRE DEL ARCHIVO *}
        {if isset($preview_data)}
            <br />
            <span class="filename">{$fileinfo.filename|truncate:35:"...":false:true} {if !empty($fileinfo.size)}[{$fileinfo.size|dk_byteformat}]{/if}</span>
        {/if}

        {* DESCRIPCION DEL ARCHIVO *}
        {assign var="archivo_parametro_description" value=$innyTypeFile->getMetadataValue("parametros","description")}
        {if $archivo_parametro_description != "none"}
            <div style="margin-top:7px;">
                <span class="filetype">{$archivo_parametro_description}:</span>
                <br />
                <input type="text" name="archivo_{$indice}_description" value="{if isset($fileinfo.description)}{$fileinfo.description}{/if}" style="font:normal 12px arial,tahoma; padding:0; margin:0; border:1px solid #7F9DB9; width:100%" />
            </div>
        {/if}
    </td>
    <td class="file_actions">
        {if !empty($id_archivo)}
			{if $archivo_fijo == "false"}
                <ul>
                    {if $cant_archivos > 1}
                        <li class="moveUp"><a href="javascript:Inny_Producto.moveUp({$id_archivo});" title="{#common_up#}">{#common_up#}</a></li>
                        <li class="moveDown"><a href="javascript:Inny_Producto.moveDown({$id_archivo});" title="{#common_down#}">{#common_down#}</a></li>                        
                    {/if}
                    <li class="delete"><a href="javascript:Inny_Producto.deleteSortableFile({$id_archivo});" title="{#common_delete#}">{#common_delete#}</a></li>
                    {if $archivo_ayuda}<li class="ayuda"><a onMouseOver="return overlib('{$archivo_ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();">Ayuda</a></li>{/if}
                </ul>
            {else}
                {if $archivo_requerido == "false"}
                    <ul>
                        <li class="delete"><a href="javascript:Inny_Producto.deleteFixedFile({$id_archivo});" title="{#common_delete#}">{#common_delete#}</a></li>
                        {if $archivo_ayuda}<li class="ayuda"><a onMouseOver="return overlib('{$archivo_ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();">Ayuda</a></li>{/if}
                    </ul>
                {else}
                    {if $archivo_ayuda}<ul><li class="ayuda"><a onMouseOver="return overlib('{$archivo_ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();">Ayuda</a></li></ul>{/if}
                {/if}
            {/if}

        {else}
			{if $archivo_ayuda}<ul><li class="ayuda"><a onMouseOver="return overlib('{$archivo_ayuda}', FGCOLOR, '#CCCCFF', BGCOLOR, '#333399', TEXTCOLOR, '#000000', ABOVE, LEFT, WIDTH, 200, CELLPAD, 10);" onMouseOut="return nd();">Ayuda</a></li></ul>{/if}
        {/if}
    </td>
</tr>