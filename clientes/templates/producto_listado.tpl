{inny_speech_load section="common"}
{inny_speech_load section="producto"}
{inny_speech_load section="filter"}
{* ========================================================================== *}
{assign var="daoSitio" value=$daoListado->getSitio()}
{assign var="pageTitle" value=$daoListado->getMetadataValue("nombre")}
{assign var="pageTitle" value=#producto_listadoTituloPagina#|replace:"%nombre_listado":$daoListado->getMetadataValue("nombre")|dk_ucfirst}
{* ========================================================================== *}
{assign var="listado_item" value=$daoListado->getMetadataValue("item")|lower}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle|dk_capitalize}
{dk_include file="styles/dokko_botones.css" inline="false"}
{dk_include file="styles/inny.listado.css" inline="false"}
{dk_include file="js/inny.producto.js" inline="false"}
{* ========================================================================== *}
{dk_hasmessages assign="hasMessages"}
{if $hasMessages}{include file="messagebox_errormessages.tpl"}{/if}
{* ========================================================================== *}
{assign var="listado_parametro_limit" value=$daoListado->getMetadataValue("parametros","limit")|lower}
{assign var="listado_parametro_order" value=$daoListado->getMetadataValue("parametros","order")|lower}
{* ========================================================================== *}
{dk_daolister name="ps" table="producto" dao=$daoProductosDSE resultsPerPage="20" query=$listado_query orderBy="posicion "|cat:$listado_parametro_order}
    {dko_declare
        name    = "order_by_orden"
        onAsc   = "posicion asc"
        onDesc  = "posicion desc"
        default = $listado_parametro_order
    }
    {dkma_declare
        url    = $smarty.server.PHP_SELF|dk_basename
        delete = #common_delete#|cat:"|"|cat:#producto_listadoEliminarConfirm#
    }
    {dkp_ini}

    {* =============== ACCIONES SOBRE EL LISTADO DE PRODUCTOS =============== *}

    {if ($daoListado->tienePermiso("B") && $dkp_results > 0) || $daoListado->tienePermiso("A") || $daoListado->tienePermiso("M")}
        <br />
        <div class="panelContainer">
            <table cellpadding="0" cellspacing="0" style="width:100%">
            <tbody>
            <tr>

                {* LINKS DE SELECCI�N DE PRODUCTOS *}
                {if $daoListado->tienePermiso("B") && $dkp_results > 0}
                    <td>
                        {capture assign="dkma_delete"}javascript:dkma_exec('multi_delete','{#producto_listadoEliminarConfirm#}','dkma_ps','ps','{#filter_dkma_seleccionarAlMenosUnElemento#}','{$smarty.server.PHP_SELF|dk_basename}');{/capture}
                        {#filter_select#|dk_capitalize}:
                        <a class="filterAction" href="{dkma_url type="all"}" title="{#filter_all#}">[{#filter_all#}]</a>
                        <a class="filterAction" href="{dkma_url type="none"}" title="{#filter_none#}">[{#filter_none#}]</a>
                        &nbsp;
                        <input type="button" class="boton panel" value="{#common_delete#}" onclick="{$dkma_delete}" />
                    </td>
                {/if}

                <td style="text-align:right;">

                    {* CSV *}
                    {if $daoListado->tienePermiso("A") || $daoListado->tienePermiso("M")}
                        <input type="button" class="boton panel" value="{#producto_csvAgregar#}" onclick="javascript:Inny_Producto.displayCSVForm();" />
                    {/if}

                    {* VACIAR LISTADO *}
                    {if $daoListado->tienePermiso("B") && $dkp_results > 0}
                        &nbsp;
                        <input type="button" class="boton panel" value="{#producto_listadoEliminarTodos#}" onclick="javascript:confirmAction('{#producto_listadoConfirmarEliminarTodos#}','producto_listado.php?id_listado={$smarty.get.id_listado}&action=deleteall');" />
                    {/if}

                    {* AGREGAR PRODUCTO *}
                    {if $daoListado->tienePermiso("A") && !$daoListado->alcanzoLimiteProductosPermitido()}
                        &nbsp;
                        <input type="button" class="boton panel" value="{#producto_agregar#|replace:"%item":$listado_item}" onclick="javascript:Denko.redirect('producto_am.php?action=add&id_listado={$smarty.get.id_listado}');" />
                    {/if}

                </td>
            </tr>
            </tbody>
            </table>

            {* FORMULARIO PARA ARCHIVO CSV *}
            {if $daoListado->tienePermiso("A") || $daoListado->tienePermiso("M")}
                <div id="csv_form" class="csv_form">
                    <br />
                    <form action="producto_listado.php?id_listado={$smarty.get.id_listado}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="delimiter" value=";" />
                        <input type="hidden" name="action" value="loadcsv" />
                        <b>{#producto_archivoCSV#}:</b>
                        <br />
                        <input class="filePanel" type="file" name="csvfile" class="file" />
                        <br />
                        <input type="checkbox" class="checkbox" name="rewrite" {if $smarty.post.rewrite}checked="checked"{/if} />&nbsp;{#producto_archivoCSVtextoReemplazarProductos#}
                        <br />
                        <input type="checkbox" class="checkbox" name="hasheader" {if $smarty.post|@count == 0 || $smarty.post.hasheader}checked="checked"{/if} />&nbsp;{#producto_archivoCSVtextoArchivoTieneEncabezado#}
                        <br />
                        <br />
                        <input type="submit" class="boton panel" value="{#producto_archivoCSVtextoCargarArchivo#}" />
                    </form>
                </div>
            {/if}
        </div>
    {/if}

    {* ========================= FILTROS DEL LISTADO ======================== *}

    <div style="border:1px solid #dddddd; margin-top:10px; padding:5px;">
        <form method="get" action="{$smarty.server.PHP_SELF|dk_basename}">
            {dk_hidden_inputs ignore="sbt,sbi"}
            <input type="text" name="sbt" value="{$smarty.get.sbt}" style="font-size:12px; font-family:arial; border: 1px solid #dadfe7;" />
            &nbsp;
            <input type="submit" value="{#common_search#}" style="color:#000000; font-size:12px; font-weight: bold; font-family:arial; border:1px solid #dadfe7; background-image:url(images/btnenter_normal.gif);" />
            &nbsp;&nbsp;
            <input type="checkbox" name="sbi" {if $smarty.get.sbi == "on"}checked="checked"{/if} style="margin:0px; padding:0px; vertical-align:middle;" />
            <span style="margin-left:3px; font-size:11px; font-family:arial; color:#808080">{#producto_buscadorPorId#}</span>
        </form>
    </div>
    <br />
    <div class="listado_headerActions">
        <div class="floatLeft back"><a href="listado_listado.php" title="{#producto_listadoLinkTitleVolver#}">&laquo; {#producto_listadoLinkVolver#}</a></div>
        <div class="floatRight">
            {#producto_listadoCambiarDeListado#}:&nbsp;
    		<select class="panel" name="listados" id="listados" onchange="javascript:Inny_Listado.cambiarDeListado();">
            	{foreach name="nombre_listados" from=$listados key="select_id_listado" item="select_nombre_listado"}
            		<option value="{$select_id_listado}" {if $select_id_listado == $smarty.get.id_listado}selected="selected"{/if}>{$select_nombre_listado|dk_lower}</option>
            	{/foreach}
            </select>
        </div>
    </div>

    {* ======================== LISTADO DE PRODUCTOS ======================== *}

    {if $dkp_results == "0"}
        <br />
        <span class="emptyListMessage">{#producto_listadoVacio#|dk_ucfirst}</span>
    {else}
        {include
            file         = "producto.common.listado.tpl"
            tipo_listado = "producto"
            url_view     = "producto_view.php"
            url_action   = "producto_am.php"
        }
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl" section="producto"}