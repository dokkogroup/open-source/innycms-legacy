{inny_speech_load section="common"}
{inny_speech_load section="producto"}
{* ========================================================================== *}
{assign var="daoListado" value=$daoProducto->getListado()}
{assign var="daoSitio" value=$daoListado->getSitio()}
{* ========================================================================== *}
{assign var="listado_nombre"      value=$daoListado->getMetadataValue("nombre")|dk_capitalize}
{assign var="listado_item"        value=$daoListado->getMetadataValue("item")|dk_lower|default:#producto_item#}
{assign var="listado_searchable"  value=$daoListado->getMetadataValue("parametros","searchable")}
{assign var="archivo_cantidad"    value=$daoListado->getMetadataValue("archivo","cantidad")}
{assign var="texto_cantidad"      value=$daoListado->getMetadataValue("texto","cantidad")}
{assign var="textos_ordenables" value=$daoListado->getMetadataValue("texto","orden")}

{* ========================================================================== *}
{include file="header.tpl" pageTitle=#producto_verTituloPagina#|replace:"%item":$listado_item}
{dk_include file="styles/dokko_botones.css" inline="false"}
{* ========================================================================== *}
{if !empty($smarty.get.action) && ($smarty.get.action == "addok" || $smarty.get.action == "editok")}
    <br />
    <table class="changeAddedOk" cellpadding="0" cellspacing="0">
    <tbody>
    	<tr>
    		<td class="description">
                {if $smarty.get.action == "addok"}
                    {#producto_okAgregar#|replace:"%nombre_listado":$daoListado->nombre|dk_ucfirst}
                {elseif $smarty.get.action == "editok"}
                    {#producto_okEditar#|replace:"%item":$listado_item|dk_ucfirst}
                {/if}
            </td>
    	</tr>
	</tbody>
    </table>
{/if}
{* ========================================================================== *}
{* BOTONERA *}
{capture assign="view_botonera"}
<div style="border: 1px solid #dddddd; padding:7px; margin-bottom:10px;">
    <div class="contentBtnContainer" align="right">
        <div class="dateInformation">
            {if !empty($daoProducto->aud_ins_date)}<p><span>{#common_date_ins#}</span>: {$daoProducto->aud_ins_date}.</p>{/if}
            {if !empty($daoProducto->aud_upd_date) && $daoProducto->aud_upd_date!='0000-00-00 00:00:00'}<p><span>{#common_date_upd#}</span>: {$daoProducto->aud_upd_date}.</p>{/if}
        </div>
        {* BOTON VOLVER *}
        {capture assign="link_volver"}javascript:Denko.redirect('{$linkBack}');{/capture}
	    <div class="btnBackIcon" align="center" onclick="{$link_volver}" style="cursor:pointer;"><img src="images/go_back.gif" /></div>
		<div class="contentBtnBack"><input type="button" value="{#common_back#}" onclick="{$link_volver}" style="cursor:pointer;" /></div>

        {* BOTON EDITAR *}
        {if $daoListado->tienePermiso("M")}
            {capture assign="link_editar"}javascript:Denko.redirect('producto_am.php?action=edit&id_producto={$daoProducto->id_producto}');{/capture}
    		<div class="btnChangeIcon" align="center" onclick="{$link_editar}" style="cursor:pointer;"><img src="images/editar_btn.gif" /></div>
    		<div class="contentBtnChange"><input type="button" value="{#common_edit#}" onclick="{$link_editar}" style="cursor:pointer;" /></div>
    	{/if}

        {* BOTON ELIMINAR *}
        {if $daoListado->tienePermiso("B")}
            {capture assign="link_eliminar"}javascript:confirmAction('{#producto_confirmacionEliminar#|replace:"%item":$listado_item|replace:"%nombre_listado":$listado_nombre}','producto_am.php?action=delete&id_producto={$daoProducto->id_producto}');{/capture}
    		<div class="btnDiscardIcon" align="center" onclick="{$link_eliminar}" style="cursor:pointer;"><img src="images/cancel.gif" /></div>
    		<div class="contentBtnDiscard"><input type="button" value="{#common_delete#}" onclick="{$link_eliminar}" style="cursor:pointer;" /></div>
    	{/if}

    </div>
</div>
{/capture}
{* ========================================================================== *}
<br />
{$view_botonera}
<table class="contentView" cellpadding="0" cellspacing="0">
<tbody>
    {include file="producto.common.view.tpl"}
</tbody>
</table>
{$view_botonera}
<br />
{* ========================================================================== *}
{include file="footer.tpl" section="producto"}