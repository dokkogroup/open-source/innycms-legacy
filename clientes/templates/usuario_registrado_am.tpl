{inny_speech_load section="common"}
{inny_speech_load section="usuario"}
{* ========================================================================== *}
{inny_get_sitio_logueado assign="daoSitio"}
{assign var="action" value=$smarty.get.action|default:$smarty.post.action}
{assign var="listado_item" value=#usuario#}
{assign var="listado_searchable"   value=$daoListadoUsuariosRegistrados->getMetadataValue("parametros","searchable")}
{assign var="contador_cantidad"    value=$daoListadoUsuariosRegistrados->getMetadataValue("contador","cantidad")}
{assign var="tiene_cardinalidades" value=$daoListadoUsuariosRegistrados->getCardinalidadesHaciaOtrosListados()}
{* ========================================================================== *}
{if $action == "add"}
    {assign var="pageTitle" value=#usuario_agregarTituloPagina#}
    {assign var="submitValue" value=#common_add#}
{elseif $action == "edit"}
    {assign var="pageTitle" value=#usuario_editarTituloPagina#}
    {assign var="submitValue" value=#common_edit#}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle|dk_capitalize}
{include file="producto.common.am.tpl" daoListado=$daoListadoUsuariosRegistrados daoProducto=$daoUsuarioRegistrado tipo_producto="usuario"}
{include file="footer.tpl" section="usuarios"}
{* ========================================================================== *}