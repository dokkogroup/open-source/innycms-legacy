{inny_speech_load section="common"}
{inny_speech_load section="filter"}
{inny_speech_load section="usuario"}
{inny_speech_load section="producto"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#usuario_listadoTitulo#|dk_capitalize}
{dk_include file="styles/inny.listado.css" inline="false"}
{* ========================================================================== *}
{assign var="listado_parametro_limit" value=$daoListadoUsuariosRegistrados->getMetadataValue("parametros","limit")|lower}
{assign var="listado_parametro_order" value=$daoListadoUsuariosRegistrados->getMetadataValue("parametros","order")|lower}
{* ========================================================================== *}
{dk_daolister name="ps" table="producto" resultsPerPage="20" query=$listado_query orderBy="posicion "|cat:$listado_parametro_order}
    {dko_declare
        name = "order_by_orden"
        onAsc = "posicion asc"
        onDesc = "posicion desc"
        default = "asc"
    }
    {dkma_declare
        url    = "usuario_registrado_abm.php"
        delete = #common_delete#|cat:"|"|cat:#usuario_listadoEliminarConfirm#
    }
    {dkp_ini}

    {* ========== ACCIONES SOBRE EL LISTADO DE USUARIOS REGISTRADOS ========= *}

    {if ($daoListadoUsuariosRegistrados->tienePermiso("B") && $dkp_results > 0) || ($daoListadoUsuariosRegistrados->tienePermiso("A") && !$daoListadoUsuariosRegistrados->alcanzoLimiteProductosPermitido())}
        <br />
        <div class="panelContainer">
            <table cellpadding="0" cellspacing="0" style="width:100%">
            <tbody>
            <tr>

                {* LINKS DE SELECCI�N DE USUARIOS *}
                {if $daoListadoUsuariosRegistrados->tienePermiso("B") && $dkp_results > 0}
                    <td>
                        {capture assign="dkma_delete"}javascript:dkma_exec('multi_delete','{#usuario_listadoEliminarConfirm#}','dkma_ps','ps','{#filter_dkma_seleccionarAlMenosUnElemento#}','usuario_registrado_abm.php');{/capture}
                        {#filter_select#|dk_capitalize}:
                        <a class="filterAction" href="{dkma_url type="all"}" title="{#filter_all#}">[{#filter_all#}]</a>
                        <a class="filterAction" href="{dkma_url type="none"}" title="{#filter_none#}">[{#filter_none#}]</a>
                    	&nbsp;
                        <input type="button" class="boton panel" value="{#common_delete#}" onclick="{$dkma_delete}" />
                    </td>
                {/if}

                <td style="text-align:right;">

                    {* VACIAR LISTADO *}
                    {if $daoListadoUsuariosRegistrados->tienePermiso("B") && $dkp_results > 0}
                        <input type="button" class="boton panel" value="{#usuario_eliminarTodos#}" onclick="javascript:confirmAction('{#usuario_confirmacionEliminarTodos#}','usuario_registrado_abm.php?action=deleteall');" />
                    {/if}

                    {* AGREGAR USUARIO *}
                    {if $daoListadoUsuariosRegistrados->tienePermiso("A") && !$daoListadoUsuariosRegistrados->alcanzoLimiteProductosPermitido()}
                        &nbsp;
                        <input type="button" class="boton panel" value="{#usuario_agregar#}" onclick="javascript:Denko.redirect('usuario_registrado_abm.php?action=add');" />
                    {/if}

                </td>
            </tr>
            </tbody>
            </table>
        </div>
    {/if}

    {* ========================= FILTROS DEL LISTADO ======================== *}

    <div style="border:1px solid #dddddd; margin-top:10px; padding:5px;">
        <form method="get" action="{$smarty.server.PHP_SELF|dk_basename}">
            {dk_hidden_inputs ignore="sbt,sbi"}
            <input type="text" name="sbt" value="{$smarty.get.sbt}" style="font-size:12px; font-family:arial; border: 1px solid #dadfe7;" />
            &nbsp;
            <input type="submit" value="{#common_search#}" style="color:#000000; font-size:12px; font-weight: bold; font-family:arial; border:1px solid #dadfe7; background-image:url(images/btnenter_normal.gif);" />
            &nbsp;&nbsp;
            <input type="checkbox" name="sbi" {if $smarty.get.sbi == "on"}checked="checked"{/if} style="margin:0px; padding:0px; vertical-align:middle;" />
            <span style="margin-left:3px; font-size:11px; font-family:arial; color:#808080">{#producto_buscadorPorId#}</span>
        </form>
    </div>
    
    {* ========================= LISTADO DE USUARIOS ======================== *}

    <br />
    {if $dkp_results == "0"}
        <br />
        <span class="emptyListMessage">{#usuario_listadoVacio#}</span>
    {else}
        {include
            file         = "producto.common.listado.tpl"
            daoListado   = $daoListadoUsuariosRegistrados
            tipo_listado = "usuario"
            url_view     = "usuario_registrado_abm.php"
            url_action   = "usuario_registrado_abm.php"
        }
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl" section="usuarios"}