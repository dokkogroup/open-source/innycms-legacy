{inny_speech_load section="common"}
{inny_speech_load section="usuario"}
{* ========================================================================== *}
{assign var="daoSitio" value=$daoListadoUsuariosRegistrados->getSitio()}
{assign var="texto_cantidad" value=$daoListadoUsuariosRegistrados->getMetadataValue("texto","cantidad")}
{assign var="archivo_cantidad" value=$daoListadoUsuariosRegistrados->getMetadataValue("archivo","cantidad")}
{assign var="listado_searchable"  value=$daoListadoUsuariosRegistrados->getMetadataValue("parametros","searchable")}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#usuario_verTituloPagina#}
{dk_include file="styles/dokko_botones.css" inline=false}
{* ========================================================================== *}
{if !empty($smarty.get.actionok)}
    <br />
    <table class="changeAddedOk" cellpadding="0" cellspacing="0">
    <tbody>
    	<tr>
    		<td class="description">
                {if $smarty.get.actionok == "add"}
                    {#usuario_okAgregar#|dk_ucfirst}
                {elseif $smarty.get.actionok == "edit"}
                    {#usuario_okEditar#|dk_ucfirst}
                {/if}
            </td>
    	</tr>
	</tbody>
    </table>
{/if}
{* ========================================================================== *}
{* BOTONERA *}
{capture assign="view_botonera"}
<div style="border: 1px solid #dddddd; padding:7px; margin-bottom:10px;">
    <div class="contentBtnContainer" align="right">

        {* BOTON VOLVER *}
        {capture assign="link_volver"}javascript:Denko.redirect('{$linkBack}');{/capture}
	    <div class="btnBackIcon" align="center" onclick="{$link_volver}" style="cursor:pointer;"><img src="images/go_back.gif" border="0" /></div>
		<div class="contentBtnBack"><input type="button" value="{#common_back#}" onclick="{$link_volver}" style="cursor:pointer;" /></div>

        {* BOTON EDITAR *}
        {if $daoListadoUsuariosRegistrados->tienePermiso("M")}
            {capture assign="link_editar"}javascript:Denko.redirect('{$smarty.server.PHP_SELF|dk_basename}?action=edit&id_usuario={$daoUsuarioRegistrado->id_producto}');{/capture}
			<div class="btnChangeIcon" align="center" onclick="{$link_editar}" style="cursor:pointer;"><img src="images/editar_btn.gif" border="0" /></div>
			<div class="contentBtnChange"><input type="button" value="{#common_edit#}" onclick="{$link_editar}" style="cursor:pointer;" /></div>
		{/if}

        {* BOTON ELIMINAR *}
        {if $daoListadoUsuariosRegistrados->tienePermiso("B")}
            {capture assign="link_eliminar"}javascript:confirmAction('{#usuario_confirmacionEliminar#}','{$smarty.server.PHP_SELF|dk_basename}?action=delete&id_usuario={$daoUsuarioRegistrado->id_producto}');{/capture}
			<div class="btnDiscardIcon" onclick="{$link_eliminar}" align="center" style="cursor:pointer;"><img src="images/cancel.gif" border="0" /></div>
			<div class="contentBtnDiscard"><input type="button" value="{#common_delete#}" onclick="{$link_eliminar}" style="cursor:pointer;" /></div>
		{/if}

	</div>
</div>
{/capture}
{* ========================================================================== *}
<br />
{$view_botonera}
<table class="contentView" cellpadding="0" cellspacing="0">
<tbody>
    {include
        file           = "producto.common.view.tpl"
        daoListado     = $daoListadoUsuariosRegistrados
        daoProducto    = $daoUsuarioRegistrado
        listado_nombre = #usuario_listadoTitulo#
        listado_item   = #usuario#
    }
</tbody>
</table>
{$view_botonera}
<br />
{* ========================================================================== *}
{include file="footer.tpl" section="usuarios"}