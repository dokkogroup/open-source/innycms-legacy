<?php
/**
 * Project: Inny Clientes
 * File: usuario_registrado_abm.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
################################################################################
# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty
$smarty = new Smarty();

# Cargo los filtros para los includes
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que el sitio pueda administrar usuarios registrados
Inny_Clientes::verificarRegistroDeUsuarios($smarty);

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('add','edit','view','delete','up','down','multi_delete','deleteall'));

# Obtengo el sitio logueado
$daoSitioLogueado = Inny_Clientes::getSitioLogueado();

# Obtengo el usuario logueado
$daoUsuarioRegistrado = DB_DataObject::factory('producto');

# Obtengo el Listado de Usuarios Registrados pertenecientes a este sitio
$daoListadoUsuariosRegistrados = $daoSitioLogueado->getListadoUsuariosRegistrados();

////////////////////////////////////////////////////////////////////////////////

# En caso que la acci�n sea distinta a 'add' (agregar usuario), verifico que el
# usuario pertenezca al listado
$action = InnyCore_Utils::getParamValue('action');
if(!in_array($action,array('add','multi_delete','deleteall'))){

    $id_usuario = InnyCore_Utils::getParamValue('id_usuario');
    Inny_Common::verificarUsuarioRegistrado($smarty,$id_usuario,$daoListadoUsuariosRegistrados);
    $daoUsuarioRegistrado->get($id_usuario);
}

# Verifico que el listado no haya alcanzado o superado el l�mite permitido
if($action == 'add' && $daoListadoUsuariosRegistrados->alcanzoLimiteProductosPermitido()){
    Inny_Common::errorCritico(
        'listado',
        'listado_errorParametroLimitExcedido',
        array(
            '%listado_nombre'          => $daoListadoUsuariosRegistrados->getMetadataValue('nombre'),
            '%listado_parametro_limit' => $daoListadoUsuariosRegistrados->getMetadataValue('parametros','limit')
        )
    );
}

////////////////////////////////////////////////////////////////////////////////

# Verifico los permisos

switch($action){

    # Verifico si hay permisos para agregar items
    case 'add':
        if(!$daoListadoUsuariosRegistrados->tienePermiso('A')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoAgregar');
        }
        break;

    # Verifico si hay permisos para editar items
    case 'edit':
        if(!$daoListadoUsuariosRegistrados->tienePermiso('M')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoEditar');
        }
        break;

    # Verifico si hay permisos para ver items
    case 'view':
        if(!$daoListadoUsuariosRegistrados->tienePermiso('V')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoVer');
        }
        break;

    # Verifico si hay permisos para ordenar items
    case 'up':
    case 'down':
        if(!$daoListadoUsuariosRegistrados->tienePermiso('O')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoOrdenar');
        }
        break;

    # Verifico si hay permisos para eliminar items
    case 'delete':
    case 'multi_delete':
    case 'deleteall':
        if(!$daoListadoUsuariosRegistrados->tienePermiso('B')){
            Inny_Common::errorCritico('listado','listado_errorParametroPermisoEliminar');
        }
        break;
}

////////////////////////////////////////////////////////////////////////////////

# Seteo el link de volver
$linkBackUsuariosRegistrados = new Inny_LinkBackUsuariosRegistrados();
$linkBack = $linkBackUsuariosRegistrados->getLinkBack();

# En caso que haya que eliminar al usuario o a todos los usuarios de un listado
if(in_array($action,array('delete','up','down','multi_delete','deleteall'))){

    switch($action){

        # En caso de eliminar el usuario registrado
        case 'delete': InnyCore_Producto::eliminar($daoSitioLogueado,$daoListadoUsuariosRegistrados->nombre,$daoUsuarioRegistrado->id_producto); break;

        # Subo el orden del usuario
        case 'up': InnyCore_Producto::subirPosicion($daoSitioLogueado,$daoListadoUsuariosRegistrados->nombre,$daoUsuarioRegistrado->id_producto); break;

        # Bajo el orden del usuario
        case 'down': InnyCore_Producto::bajarPosicion($daoSitioLogueado,$daoListadoUsuariosRegistrados->nombre,$daoUsuarioRegistrado->id_producto); break;

        # Borrado de algunos usuarios (multidelete)
        case 'multi_delete':
        foreach($_GET['id'] as $id){
            InnyCore_Producto::eliminar($daoSitioLogueado,$daoListadoUsuariosRegistrados->nombre,$id);
        }
        break;

        # En caso de eliminar TODOS los usuarios registrados
        case 'deleteall': $daoListadoUsuariosRegistrados->vaciar(); break;

        # En cualquier otro caso...
        default: break;
    }

    # Y luego me redirijo al listado de usuarios registrados
    Denko::redirect($linkBack);
}

# En caso que la acci�n sea 'add' o 'edit', haya variables en POST y los datos sean v�lidos
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $id_usuario = InnyCore_Producto::agregarEditarDesdePost($action,$daoSitioLogueado,$daoListadoUsuariosRegistrados,$daoUsuarioRegistrado);
    if($id_usuario){

        # En caso que deba seguir agregando usuarios al listado
        if(!empty($_POST['continue'])){
            Denko::redirect(basename($_SERVER['PHP_SELF']).'?action=add&continue=true');
        }

        # En caso que mostrar que el usuario se edit� correctamente
        Denko::redirect('usuario_registrado_abm.php?action=view&id_usuario='.$id_usuario.'&actionok='.($action == 'add' ? 'add' : 'edit'));
    }
}

////////////////////////////////////////////////////////////////////////////////

# Asigno los DAOs al template
$smarty->assign('daoListadoUsuariosRegistrados',$daoListadoUsuariosRegistrados);
$smarty->assign('daoUsuarioRegistrado',$daoUsuarioRegistrado);

# Asigno el link de retorno al template
$smarty->assign('linkBack',$linkBack);

# Asigno la informaci�n de los archivos al template
$smarty->assign('filesInfo',$daoListadoUsuariosRegistrados->getFilesInfo());

# Muestro el template
$smarty->display($action == 'view' ? 'usuario_registrado_view.tpl' : 'usuario_registrado_am.tpl');

################################################################################