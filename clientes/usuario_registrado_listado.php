<?php
/**
 * Project: Inny Clientes
 * File: usuario_registrado_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';

################################################################################

function setearQueryListadoProductos(&$smarty,&$daoListado){
    $listado_query = 'id_listado = '.$daoListado->id_listado;
    $search_nombre = isset($_GET['sbt']) ? trim($_GET['sbt']) : '';

    if($search_nombre !== ''){

        # Verifico si debo buscar por ID
        $buscarPorID = (isset($_GET['sbi']) && $_GET['sbi'] == 'on');

        if($buscarPorID){
            $listado_query.= ' and id_producto = '.$search_nombre;
        }

        # En caso que tenga que buscar por texto
        else{

            # Obtengo el campo principal
            $campo_texto_principal = $daoListado->getCampoTextoPrincipal();

            # En caso que tenga el DSE
            if($daoListado->getMetadataValue('parametros','searchable') == 'true'){
                $daoProductosDSE = dse_search($search_nombre,null,null,$listado_query);
                $daoProductosDSE->orderBy($campo_texto_principal);
                $smarty->assign('daoProductosDSE',$daoProductosDSE);
            }

            # En caso que sea un listado normal
            else{
                $listado_query.= ' and '.$campo_texto_principal.' like \'%'.$search_nombre.'%\'';
            }
        }
    }
    # Seteo el query
    $smarty->assign('listado_query',$listado_query);
}

################################################################################

# Verifico que el usuario est� logueado
Inny_Clientes::verificarSitioLogueado();

# Creo la instancia de Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que el sitio pueda administrar usuarios registrados
Inny_Clientes::verificarRegistroDeUsuarios($smarty);

# Seteo la url del listado, para no perder los parametros GET.
$linkBackUsuariosRegistrados = new Inny_LinkBackUsuariosRegistrados();
$linkBackUsuariosRegistrados->verifyRedirect();
$linkBackUsuariosRegistrados->setLinkBack();

# Asigno las variables al template
$daoSitioLogueado = Inny_Clientes::getSitioLogueado();
$daoListadoUsuariosRegistrados = $daoSitioLogueado->getListadoUsuariosRegistrados();
$smarty->assign('daoListadoUsuariosRegistrados',$daoListadoUsuariosRegistrados);

# Seteo el query para el listado
setearQueryListadoProductos($smarty,$daoListadoUsuariosRegistrados);

# Muestro el template
$smarty->display('usuario_registrado_listado.tpl');

################################################################################