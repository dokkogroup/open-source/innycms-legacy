<?php
###############################################################################
function evaluateConfigurationExpression($expression, $indice1 = null, $indice2 = null) {
    $aux = ereg_replace("GET-(([a-zA-Z0-9_/-])*)-", ' (isset($_GET[\'\\1\'])?$_GET[\'\\1\']:\'\') ', $expression);
    $aux = ereg_replace("SES-(([a-zA-Z0-9_/-])*)-", ' ($_SESSION[\'\\1\']?$_SESSION[\'\\1\']:\'\') ', $aux);
    
    // se pasa false como tercer parametro para evitar que se creen configuraciones con indices solo por preguntar si se deben mostrar o no. OJO SI SE SACA
    $aux = ereg_replace("DBC-(([a-zA-Z0-9_/-])*)-", ' getConfigByHierarchy(\'\\1\',$indice1,$indice2,false) ', $aux);
    // se pasa false como tercer parametro para evitar que se creen configuraciones con indices solo por preguntar si se deben mostrar o no. OJO SI SE SACA
    // ..Fede..
     
    $aux = '$res = (' . $aux . ') == true;';
    $res = '-||-ERROR-||-';
    @ eval ($aux . ";");
    if ($res === '-||-ERROR-||-') {
        return null;
    }
    return $res;
}
###############################################################################
function getConfigByHierarchy($name, $indice1 = null, $indice2 = null, $autoCreateConfig = true) {
    $res = null;
    if ($indice1 !== null || $indice2 !== null) {
        $res = getConfig($name, $indice1, $indice2, $autoCreateConfig);
    }
    if ($res == null) {
        $res = getConfig($name, null, null, $autoCreateConfig);
    }
    return $res;
}
###############################################################################
function getConfig($name, $indice1 = null, $indice2 = null, $autoCreateConfig = true) {
    global $SETTINGS, $DBSETTINGS;
    $claveCache = $name . '-' . $indice1;
    if (!isset ($DBSETTINGS[$claveCache])) {
        $conf = DB_DataObject :: factory("configuracion");
        $conf->nombre = $name;
        if ($indice1 === null) {
            $conf->whereAdd('indice1 is null');
        } else {
            $conf->indice1 = $indice1;
        }
        if ($indice2 === null) {
            $conf->whereAdd('indice2 is null');
        } else {
            $conf->indice2 = $indice2;
        }

        if ($conf->find(true)) {
            $DBSETTINGS[$claveCache] = ($conf->estado == 1) ? $conf->valor : '---null---';
        } else {
            $DBSETTINGS[$claveCache] = '---null---';
            if ($autoCreateConfig){ 
                $conf = DB_DataObject :: factory("configuracion");
                $conf->nombre = $name;
                $conf->descripcion = $name;
                $conf->estado = '0';
                $conf->valor = '';
                $conf->indice1 = $indice1;
                $conf->indice2 = $indice2;
                if ($indice1 !== null) {
                    $masterConf = DB_DataObject :: factory("configuracion");
                    $masterConf->nombre = $name;
                    $masterConf->whereAdd('indice1 is null');
                    if ($indice2 === null) {
                        $masterConf->whereAdd('indice2 is null');
                    } else {
                        $masterConf->indice2 = $indice2;
                    }
                    if ($masterConf->find(true)) {
                        $conf->tipo = $masterConf->tipo;
                        $conf->id_tipoconfiguracion = $masterConf->id_tipoconfiguracion;
                        $conf->descripcion = $masterConf->descripcion;
                        $conf->metadata = $masterConf->metadata;
                        $conf->filtro = $masterConf->filtro;
                    }
                }
                $conf->insert();
            }
        }
    }
    return ($DBSETTINGS[$claveCache] == '---null---') ? null : $DBSETTINGS[$claveCache];
}
###############################################################################
function setConfig($name, $value, $indice1 = null, $indice2 = null) {
    global $SETTINGS, $DBSETTINGS;
    $claveCache = $name . '-' . $indice1;
    if (!isset ($DBSETTINGS[$claveCache])) {
        unset ($DBSETTINGS[$claveCache]);
    }
    $conf = DB_DataObject :: factory("configuracion");
    $conf->nombre = $name;
    if ($indice1 === null) {
        $conf->whereAdd('indice1 is null');
    } else {
        $conf->indice1 = $indice1;
    }
    if ($indice2 === null) {
        $conf->whereAdd('indice2 is null');
    } else {
        $conf->indice2 = $indice2;
    }

    if (!$conf->find(true)) {
        return false;
    }
    $conf->valor = $value;
    $conf->estado = 1;
    return $conf->update();
}
###############################################################################