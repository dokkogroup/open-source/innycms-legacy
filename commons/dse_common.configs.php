<?php


// +----------------------------------------------------------------------+
// |                               DSE                                    |
// +----------------------------------------------------------------------+
// |                          2009 Dokko Group                            |
// +----------------------------------------------------------------------+
// |  Copyright (c) 2009 Dokko Group                                      |
// |  Tandil, Buenos Aires, 7000, Argentina                               |
// |  All Rights Reserved.                                                |
// |                                                                      |
// | This software is the confidential and proprietary information of     |
// | Dokko Group. You shall not disclose such Confidential Information    |
// | and shall use it only in accordance with the terms of the license    |
// | agreement you entered into with Dokko Group.                         |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Author: Dokko Group.                                                 |
// +----------------------------------------------------------------------+
//

################################ CONSTANTES ###############################
define('DSE_DEBUG_FILENAME', '../test/data/queries.log');
define('DSE_WILDCARDS_PATTERN', '/[+*-<>~]/');
define('DSE_CATEGORY_PREFIX', 'categ');
################################ CONSTANTES ###############################
/**
 * Carga y asigna los valores de las configuraciones de DSE.
 * @param string $fileName El nombre del archivo de las configuraciones de.
 */
function dse_loadConfigs($fileName = '../commons/dse.configs.ini') {
    if (is_file($fileName)) {
        $configs = parse_ini_file($fileName, true);
    } else {
        $configs = array ();
    }
    ############################## CONFIGURACIONES ############################
    /**
     * Especifica si el DSE se est� usando dentro del Inny.
     */
    if (!empty ($configs['externals']['dse.useInny'])) {
        define('DSE_USE_INNY', $configs['externals']['dse.useInny']);
    } else {
        define('DSE_USE_INNY', false);
    }
    /**
     * Nombre de la tabla de documentos de dse. Esta configuraci�n se usa
     * cuando se integra dse con otra tabla en la base de datos.
     */
    if (!empty ($configs['table-mappings']['mapping.documentTableName'])) {
        define('DSE_DOCUMENT_TABLENAME', $configs['table-mappings']['mapping.documentTableName']);
    } else {
        define('DSE_DOCUMENT_TABLENAME', 'dse_document');
    }

    /**
     * Nombre del campo id en la tabla especificada DSE_DOCUMENT_TABLENAME
     * documentos de dse. Esta configuraci�n se usa cuando se integra dse
     * con otra tabla en la base de datos.
     */
    if (!empty ($configs['table-mappings']['mapping.documentTableId'])) {
        define('DSE_DOCUMENT_TABLEID', $configs['table-mappings']['mapping.documentTableId']);
    } else {
        define('DSE_DOCUMENT_TABLEID', 'id_document');
    }

    /**
     * Mapping de campos. Este mapping define la relaci�n entre los campos
     * buscables y sus correspondientes campos mostrables.
     * Si se instal� dse sobre otra tabla, ac� se debe definir que campos de
     * la tabla se corresponden con los campos buscables.
     * Ejemplo: tabla productos (   id_producto INT,
     *                              nombre_producto VARCHAR,
     *                              descripci�n_producto TINYTEXT,
     *                              precio_producto INT
     *                          )
     * Si se instala DSE sobre esta tabla, y se desea que los campos sobre los cuales
     * se desea buscar son nombre_producto y descripcion_producto, el arreglo de mappings
     * debe ser de la siguiente manera:
     * $DSE_DOCUMENT_FIELD_MAPPINGS = array (   
     *                                          'field_0' => 'nombre_producto', 
     *                                          'field_1' => 'descripcion_producto', 
     *                                          'field_2' => ''
     *                                      );
     * IMPORTANTE: Tener en cuenta los tama�os de cada campo, es decir no hacer un mapping
     * de un TEXT a un TINYTEXT o VARCHAR.
     */
    global $DSE_DOCUMENT_FIELD_MAPPINGS;
    if (DSE_USE_INNY) {
            //Si se est� usando desde el inny, los mapeos vienen de la metadata del sitio.
            //Obtengo del Inny la metadata del sitio actual
            $metadata = Inny :: getDseMetadata();

            //Armo los mapeos con la metadata dada por el inny.
            $DSE_DOCUMENT_FIELD_MAPPINGS['field_0'] = isset ($metadata['field_mappings_0']) ? $metadata['field_mappings_0'] : 'texto1';
            $DSE_DOCUMENT_FIELD_MAPPINGS['field_1'] = isset ($metadata['field_mappings_1']) ? $metadata['field_mappings_1'] : 'texto2';
            $DSE_DOCUMENT_FIELD_MAPPINGS['field_2'] = isset ($metadata['field_mappings_2']) ? $metadata['field_mappings_2'] : 'texto3';
    } elseif (!empty ($configs['field-mappings'])) {
            //Si no se esta usando desde el inny y existen mapeos en el archivo de configuraciones,
            //cargo los mapeos de ah�.
            foreach ($configs['field-mappings'] as $fieldName => $fieldMapping) {
                $DSE_DOCUMENT_FIELD_MAPPINGS[$fieldName] = $fieldMapping;
            }
    } else {
        //En caso de no estar seteados los mapeos en las configuraciones setearlas por defecto.
        $DSE_DOCUMENT_FIELD_MAPPINGS = array (
            'field_0' => 'd_field_0',
            'field_1' => 'd_field_1',
            'field_2' => 'd_field_2'
        );
    }
    define('DSE_DOCUMENT_FIELD_MAPPINGS_STR', implode(',', $DSE_DOCUMENT_FIELD_MAPPINGS));

    /**
     * Decide si aplicar stems o sin�nimos no a los queries.
     */
    if (!empty ($configs['query-transform']['transform.stemms'])) {
        define('DSE_APPLY_STEMS', $configs['query-transform']['transform.stemms']);
    } else {
        define('DSE_APPLY_STEMS', true);
    }
    if (!empty ($configs['query-transform']['transform.synonims'])) {
        define('DSE_APPLY_SYNONIMS', $configs['query-transform']['transform.synonims']);
    } else {
        define('DSE_APPLY_SYNONIMS', false);
    }
    /**
     * Decide si usar o no la cl�usula WITH QUERY EXPANSION en los queries
     * sobre los documentos en caso de no ser una b�squeda booleana.
     * La expansi�n del query significa que MYSQL har� la b�squeda dos veces:
     * La primer b�squeda es una b�squeda normal. Igual a la primera pero
     * adem�s se le agregan.
     */
    if (!empty ($configs['query-transform']['transform.synonims'])) {
        define('DSE_USE_MYSQL_QUERY_EXPANSION_MODE', $configs['query-transform']['transform.useMySqlQueryExpansion']);
    } else {
        define('DSE_USE_MYSQL_QUERY_EXPANSION_MODE', false);
    }

    /**
     * Nombre que tendr� el bloque dse_search. Todos los par�metros GET de dse
     * estar�n encabezados por este nombre, seguido de su correspondiente
     * significado.
     * Ejemplos
     *      El query ser� "dseq"
     *      El drilldown en una categor�a de la vista 20 ser� "dsec20"
     *      El nro de p�gina ser� 
     */
    if (!empty ($configs['internals']['dse.pluginBlockName'])) {
        define('DSE_PLUGIN_BLOCK_NAME', $configs['internals']['dse.pluginBlockName']);
    } else {
        define('DSE_PLUGIN_BLOCK_NAME', 'dse');
    }
    /**
     * Pone a las funciones de b�squeda en modo debug. Las b�squedas son
     * m�s lentas, pero proveen m�s informaci�n acerca de cada una.
     */
    if (!empty ($configs['internals']['dse.debug'])) {
        define('DSE_DEBUG_MODE', $configs['internals']['dse.debug']);
    } else {
        define('DSE_DEBUG_MODE', false);
    }
    /**
     * Define el c�digo de lenguaje para el idioma de la aplicaci�n.
     * El valor por defecto es "es" (Espa�ol). Otro posible valor es en "English".
     * Es usado para saber que algoritmo de stemming aplicar.
     */
    if (!empty ($configs['internals']['dse.dataLangCode'])) {
        define('DSE_DATA_LANGCODE', $configs['internals']['dse.dataLangCode']);
    } else {
        define('DSE_DATA_LANGCODE', 'es');
    }
    /**
     * Define si aproximar la cuenta de categorias cuando pasa cierto umbral
     */
    if (!empty ($configs['internals']['dse.approximateCategoryCounts'])) {
        define('DSE_APPROXIMATE_CATEGORY_COUNTS', $configs['internals']['dse.approximateCategoryCounts']);
    } else {
        define('DSE_APPROXIMATE_CATEGORY_COUNTS', false);
    }

}