<?php


// +----------------------------------------------------------------------+
// |                               DSE                                    |
// +----------------------------------------------------------------------+
// |                          2009 Dokko Group                            |
// +----------------------------------------------------------------------+
// |  Copyright (c) 2009 Dokko Group                                      |
// |  Tandil, Buenos Aires, 7000, Argentina                               |
// |  All Rights Reserved.                                                |
// |                                                                      |
// | This software is the confidential and proprietary information of     |
// | Dokko Group. You shall not disclose such Confidential Information    |
// | and shall use it only in accordance with the terms of the license    |
// | agreement you entered into with Dokko Group.                         |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Author: Dokko Group.                                                 |
// +----------------------------------------------------------------------+
//
################################ CONSTANTES ###############################
require_once '../commons/dse_common.configs.php';

###########################################################################
require_once '../denko/dk.denko.php';

Denko :: openDB();
###########################################################################
###########################################################################
/**
 * Loguea los queries realizados en el archivo especificado por la constante
 * DSE_DEBUG_FILENAME solo si la constante DSE_DEBUG_MODE est� en true.
 */
function dse_logQueries() {
	if (DSE_DEBUG_MODE) {
		$handler = fopen(DSE_DEBUG_FILENAME, 'a');
		fwrite($handler, 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "\n");
		fclose($handler);
	}
}
###########################################################################
/**
 * Verifica si se est� usando dentro del InnyCMS.
 * SI es as�, incluye la clase Inny.class para que pueda ser llamada.
 */
function dse_useInny() {
	if (DSE_USE_INNY && is_file("../commons/inny.core.php")) {
		require_once "../commons/inny.core.php";
		return true;
	}
	return false;
}
###########################################################################
/**
 * Verifica si el query (puede ser un array si es fielded o un string
 * si es normal) tien funciones booleanas o no.
 * En caso de ser un fielded query se considerar� booleano si el query
 * contra uno de los campos es booleano.
 *
 *  @param mixed $query El query a verificar
 * @return boolean true en caso de tener funciones booleanas. false en otro caso
 */
function dse_isBooleanQuery($query) {
	if (is_array($query)) {
		foreach ($query as $fvalue) {
			if ($fvalue !== '' && preg_match(DSE_WILDCARDS_PATTERN, $fvalue) !== 0) {
				return true;
			}
		}
	} else {
		return preg_match(DSE_WILDCARDS_PATTERN, $query) !== 0;
	}
}
###########################################################################
/**
 * Escapa las comillas en los queries.
 *
 * @param mixed $query El query a escapar
 * @return void
 */
function dse_escapeQuotes(& $query) {
	if (is_array($query)) {
		foreach ($query as $fname => $fvalue) {
			$query[$fname] = str_replace("'", '\\\'', $fvalue);
		}
	} else {
		$query = str_replace("'", '\\\'', $query);
	}
}
###########################################################################
/**
 * Incluye el stemmer para el idioma seteado.
 * Si no se asign� ning�n lenguaje � el stemmer no existe para ese idioma,
 * usa el definido en la constante DSE_DATA_LANGCODE.
 * Si el definido por la constante no se encuentra, no incluye ning�n stemmer.
 *
 * @param mixed $language El lenguaje para el cual obtener el stemmer
 * @return boolean true si encontr� el stemmizador, false en otro caso
 */
function dse_includeStemms($language = null) {

	$langCode = strtolower($language);

	$lc = isset ($language) ? strtolower($language) : strtolower(DSE_DATA_LANGCODE);

	$fileName = '../commons/dse_stems/dse_stemmer.' . $lc . '.php';
	//Si no se encuentra el stemmer para el idioma, retorno el query
	//original
	if (is_file($fileName)) {
		require_once $fileName;
		return true;
	} else {
		$fileName = '../commons/dse_stems/dse_stemmer.' . strtolower(DSE_DATA_LANGCODE) . '.php';
		if (is_file($fileName)) {
			require_once $fileName;
			return true;
		}
	}
	return false;
}
###########################################################################
/**
 * Aplica stemming al query.
 * @param mixed $query El query a stemizar
 * @return mixed El query stemizado
 */
function dse_transformStems($query) {
	//Si el query es booleano, no stemmizar
	/*if (dse_isBooleanQuery($query)) {
		return $query;
	}*/
	$langCode = strtolower(DSE_DATA_LANGCODE);
	$fileName = '../commons/dse_stems/dse_stemmer.' . $langCode . '.php';
	//Si no se encuentra el stemmer para el idioma, retorno el query
	//original
	if (is_file($fileName)) {
		require_once $fileName;

		if (is_array($query)) {
			foreach ($query as $fname => $fvalue) {
				//$fvalue = str_replace("\"", " \" ", $fvalue);
				$query[$fname] = Stemmer :: stemm($fvalue,true);
			}
			return $query;
		} else {
			//$query = str_replace("\"", " \" ", $query);
			return Stemmer :: stemm($query,true);
		}
	} else {
		return $query;
	}
}
###########################################################################
/**
 * Aplica sin�nimos al query.
 * @param mixed $query El query a sinonimizar
 * @param boolean $stemify Si debe o no stemizar el query antes de aplicar
 *                         sin�nimos. Los sin�nimos est�n stemmificados,
 *                         por lo que si el query no est� stemmificado no
 *                         se encontrar�n sin�nimos.
 *                         Por defecto true.
 * @return mixed El query sinonimizado
 */
function dse_transformSynonims($query, $stemify = true) {
	//TODO: Tener en cuenta las expresiones en los queries.
	if (dse_isBooleanQuery($query)) {
		return $query;
	}
	if ($stemify) {
		$query = dse_transformStems($query);
	}
	require_once '../DAOs/Dse_synonim.php';
	if (is_array($query)) {
		foreach ($query as $fname => $fvalue) {
			$query[$fname] = DataObjects_Dse_synonim :: synonimize($fvalue);
		}
		return $query;
	} else {
		return DataObjects_Dse_synonim :: synonimize($query);
	}

}
###########################################################################
/**
 * Andoriza una frase.
 * @param string $phrase La frase a andorizar
 * @return string La frase andorizada 
 */
 function dse_andorize($phrase) {
 
 	$words = explode(' ',$phrase);
 	
 	if ($phrase == "" || count($words) <= 0) {
 		return $phrase;	
 	}
 	
 	$andorizedPhrase= '';
 	
 	foreach ( $words as $w ) {
		if ($andorizedPhrase != '') {
			$andorizedPhrase .= ' ';
		}
		$andorizedPhrase .= '+' . $w;
 	}
	
	return $andorizedPhrase;
 	
 } 	
###########################################################################
/**
 * Fuerza un query a hacer un and entre los terminos de busqueda.
 * @param mixed $query El query a andorizar
 * @return mixed El query sinonimizado
 */
function dse_transformAnd($query) {
	
	if (dse_isBooleanQuery($query)) {
		return $query;
	}
	
	if (is_array($query)) {
		foreach ($query as $fname => $fvalue) {
			$query[$fname] = dse_andorize($fvalue);
		}
		return $query;
	} else {
		return dse_andorize($query);
	}
}
###########################################################################
/**
 * Valida un query, y arregla errores en caso de que los haya.
 * @param mixed $query El query a validar
 * @param array $drilldowns Los drilldowns a validar
 * @return void
 */
function dse_validateQuery(& $query, & $drilldowns) {
	
	# Validacion de drilldowns
	
	if (empty($drilldowns) || count($drilldowns) === 0) {
		# No hay drilldowns
		return void;
	}
	
	# Clono el arreglo para poder modificar el original
	$categories = $drilldowns;
	
	foreach ($categories as $id_view => $id_category) {
		# Verifico que la categoria seleccionada pertenezca a la vista
		if ($id_category == '') {
			# Drilldown vacio, continuo con los demas
			continue;
		}
		
		$category = DB_DataObject :: factory('dse_category');
		
		# Verifico que la categoria exista
		
		if (!$category->get($id_category)) {
			# La categoria no existe: remuevo el drilldown
			$drilldowns[$id_view] = null;
			continue;
		}
		
		# Verifico que pertenezca a la vista correspondiente.
		if ($category->id_view != $id_view) {
			# Verifico que no haya otro drilldown en la misma vista
			if (isset($drilldowns[$category->id_view])) {
				# Hay otro drilldown en la misma vista: remuevo el drilldown
				$drilldowns[$id_view] = null;
			} else {
				# No hay otro drilldown en la misma vista: lo corrijo
				$drilldowns[$id_view] = null;
				$id_view = $category->id_view;
				$drilldowns[$id_view] = $id_category;
			}				
		}
		
		# Si se esta usando dse dentro del Inny, verifico que la categoria
		# pertenece al sitio
		if (dse_useInny() && $category->index1 != Inny :: getActualSitio()) {
			# La categoria pertenece a otro sitio: remuevo el drilldown
			$drilldowns[$id_view] = null;
		}			
		
	}
	
	return void;	
}
###########################################################################
###########################################################################

###########################################################################
############################## CREACION ###################################
/**
 * Crea un documento en la base de datos. Usar esta interfaz en vez de
 * usar los DAOs directamente.
 * 
 * @param string $field_0 texto del campo 0
 * @param string $field_1 texto del campo 1
 * @param string $field_2 texto del campo 2
 * 
 * @return mixed el id privado en la base de datos del documento. false en
 *         en caso de falla.
 */
function dse_createDocument($field_0, $field_1 = '', $field_2 = '', $so_field_0 = 0, $so_field_1 = 0) {

	global $DSE_DOCUMENT_FIELD_MAPPINGS;

	$d = DB_DataObject :: factory(DSE_DOCUMENT_TABLENAME);
	$d-> $DSE_DOCUMENT_FIELD_MAPPINGS['field_0'] = $field_0;
	$d-> $DSE_DOCUMENT_FIELD_MAPPINGS['field_1'] = $field_1;
	$d-> $DSE_DOCUMENT_FIELD_MAPPINGS['field_2'] = $field_2;
	$d->so_field_0 = $so_field_0;
	$d->so_field_1 = $so_field_1;
	$d->category_ids = '';
	$d->category_names = '';

	$return = $d->insert();
	$d->free();

	return $return;
}
###########################################################################
/**
 * Crea una categor�a en la base de datos. Usar esta interfaz en vez de
 * usar los DAOs directamente.
 * 
 * @param string $name nombre de la categor�a
 * @param string $id_view Id de vista a la cual pertenece la categor�a
 * @param string $id_parent Id privado en la base de datos del padre de la
 *               categor�a. Si es null se asume que la categor�a a crear es
 *               ra�z (vista).
 * @param string $code c�digo p�blico de la categor�a
 * @param string $metadata metadata de la categor�a
 * 
 * @return mixed el id privado en la base de datos de la categor�a o false en
 *         en caso de falla.
 */
function dse_createCategory($name, $id_view, $id_parent = null, $code = '', $metadata = '') {

	$c = DB_DataObject :: factory('dse_category');
	$c->name = $name;
	$c->id_view = $id_view;
	$c->id_parent = $id_parent;
	$c->code = ($code !== '') ? $code : $name . '_' . $id_view;
	$c->metadata = $metadata;

	$return = $c->insert();
	$c->free();

	return $return;
}

###########################################################################
############################## ACCESO #####################################
/**
 * Obtiene una categor�a por id.
 * 
 * @param int $id_category el id de la categor�a a obtener
 * 
 * @return Dao El dao de la categor�a si existe. null en caso de que no
 *             exista
 */
function dse_getCategoryById($id_category) {
	$c = DB_DataObject :: factory('dse_category');
	if ($c->get($id_category)) {
		return $c;
	} else {
		$c->free();
		return null;
	}
}
###########################################################################
/**
 * Obtiene una categor�a por el campo code.
 * 
 * @param string $code el c�digo de la categor�a a obtener
 * 
 * @return Dao El dao de la categor�a si existe. null en caso de que no
 *             exista
 */
function dse_getCategoryByCode($code) {
	$c = DB_DataObject :: factory('dse_category');
	$c->code = $code;
	if ($c->find(true)) {
		return $c;
	} else {
		$c->free();
		return null;
	}
}
###########################################################################
/**
 * Obtiene un documento por id.
 * 
 * @param string $id_document el id del documento a obtener
 * 
 * @return Dao El dao del documento si existe. null en caso de que no
 *             exista
 */
function dse_getDocumentById($id_document) {
	$d = DB_DataObject :: factory(DSE_DOCUMENT_TABLENAME);
	if ($d->get($id_document)) {
		return $d;
	} else {
		$d->free();
		return null;
	}
}

###########################################################################
############################ BUSQUEDA #####################################

/**
 * Busca en la base de datos de DSE.
 * 
 * @param string $query el query para realizar la b�squeda
 * @param array $categories categor�as a las que pertenece el documento.
 * @param string $sort_string Un string indicando el ordenamiento de los
 *                            documentos. La sintaxis es al estilo SQL
 *                            (field [ASC|DESC], ...).
 *                            Los posibles campos a ordenar provistos por
 *                            dse son: field_0, field_1, field_2, score,
 *                            so_field_0 y so_field_2.
 * @param string $whereAdd Permite filtrar los documentos por alg�n campo dentro
 *                         de la tabla. Por defecto no se aplica.
 * 
 * @return Dao El Dao listo para ser usado con el DaoLister
 */
function dse_search(& $query, $categories = null, $sort_string = null, $whereAdd = false) {

	$d = DB_DataObject :: factory(DSE_DOCUMENT_TABLENAME);
	
	//Aplico stemms al query.
	if (DSE_APPLY_STEMS) {
		$query = dse_transformStems($query);
	}

	//Aplico sin�nimos al query.
	if (DSE_APPLY_SYNONIMS) {
		$query = dse_transformSynonims($query);
	}

	//Escapo las comillas
	dse_escapeQuotes($query);

	//Agrego los campos que quiero seleccionar.
	$d->selectAdd();
	$d->selectAdd('*');

	$d->whereAdd();

	//Si el query es vac�o null, no agrego restricciones sobre los campos
	if ($query !== '' && $query !== null) {
		$isBooleanQuery = dse_isBooleanQuery($query);

		if (is_array($query)) {
			//Fielded query.
			//Agrego la condici�n sobre los fields en los que se desea buscar campos.
			foreach ($query as $fnumber => $fvalue) {

				if (is_numeric($fnumber) && $fvalue !== '') {
					if (!$isBooleanQuery && DSE_USE_MYSQL_QUERY_EXPANSION_MODE) {
						$d->whereAdd('MATCH(field_' . $fnumber . ') AGAINST(\'' . $fvalue . '\' WITH QUERY EXPANSION)');
					} else {
						if ($isBooleanQuery) {
							$d->whereAdd('MATCH(field_' . $fnumber . ') AGAINST(\'' . $fvalue . '\' IN BOOLEAN MODE)');
						} else {
							$d->whereAdd('MATCH(field_' . $fnumber . ') AGAINST(\'' . $fvalue . '\')');
						}
					}

				}
			}
		} else {
			//Agrego la condici�n sobre todos los campos.
			if (!$isBooleanQuery && DSE_USE_MYSQL_QUERY_EXPANSION_MODE) {
				$d->whereAdd('MATCH(field_0,field_1,field_2,category_names) AGAINST(\'' . $query . '\' WITH QUERY EXPANSION)');
			} else {
				if ($isBooleanQuery) {
					$d->whereAdd('MATCH(field_0,field_1,field_2,category_names) AGAINST(\'' . $query . '\' IN BOOLEAN MODE)');
				} else {
					$d->whereAdd('MATCH(field_0,field_1,field_2,category_names) AGAINST(\'' . $query . '\' IN BOOLEAN MODE)');
				}
			}
		}
	}

	//Agrego las condiciones sobre las categor�as si las hay.
	if ($categories !== null && count($categories) > 0) {
		foreach ($categories as $id_category) {
			//Pregunto esto para que el arreglo de categor�as se corresponda con
			//las vista; es decir $categories[0] es el drilldown en la vista cero,
			//$categories[1] es el drilldown en la vista 1, etc.
			if (!empty ($id_category)) {
				$d->whereAdd('MATCH(category_ids) AGAINST(\'' . DSE_CATEGORY_PREFIX . $id_category . '\' IN BOOLEAN MODE)');
			}
		}
	}

	//Agrego el ordenamiento si lo hay. El valor por defecto (es decir cuando tanto
	//$sort_field como $sort_order son null) no es relevancia, debido a que esta
	//funci�n se llama para construir el Dao para la cuenta de categor�as, y
	//debido a que no se requiere ordenamiento cuando se cuentan categor�as, no se
	//ordenan para acelerar el query.
	if ($sort_string !== null) {
		if ($query !== '' && $query !== null) {

			if ($sort_string !== '') {
				if (is_array($query)) {
					//Es un fielded query. El ordenamiento por defecto esta dado por
					//la suma de los match de cada campo
					$select_str = '';
					foreach ($query as $fnumber => $fvalue) {
						if (is_numeric($fnumber) && $fvalue !== '') {
							if ($select_str != '') {
								$select_str .= ' + ';
							}
							$select_str .= 'MATCH(field_' . $fnumber . ') AGAINST(\'' . $fvalue . '\' IN BOOLEAN MODE)';
						}
					}
					$d->selectAdd('( ' . $select_str . ' ) AS score');
					$d->orderBy($sort_string);
				} else {
					//Es un query simple. El ordenamiento por defecto esta dado por
					// el match de los tres campos juntos
					$query_no_boolean = preg_replace(DSE_WILDCARDS_PATTERN, '', $query);
					$d->selectAdd('MATCH(field_0,field_1,field_2,category_names) AGAINST(\'' . $query_no_boolean . '\') AS score');
					$d->orderBy($sort_string);
				}
			} else {
				$query_no_boolean = preg_replace(DSE_WILDCARDS_PATTERN, '', $query);
				$d->selectAdd('MATCH(field_0,field_1,field_2,category_names) AGAINST(\'' . $query_no_boolean . '\') AS score');
				$d->orderBy('score DESC');
			}
		} else {
			//Como el query es vac�o, no hay score.
			//Si se intent� ordenar por score, lo borro.
			$sort_string = preg_replace('/^score [ASCDE]*,*|score [ASCDE]*,|,[ ]*score [ASCDE]*$/', '', $sort_string);
			$d->orderBy($sort_string);
		}
	}

	//Agrego la restricci�n whereAdd si la hay.
	if ($whereAdd !== false) {
		$d->whereAdd($whereAdd);
	}

	return $d;

}
###########################################################################
/**
 * Obtiene la cuenta de categor�as.
 * 
 * @param string $query El query ejecutado
 * @param array $categories Categor�as donde se hizo un drilldown. Si no hay 
 *                          drilldown, asignar null o un arreglo vac�o
 * @param int $id_category Categor�a padre sobre la cual se desea obtener la cuenta 
 *                         de resultados por categor�as de sus categor�as hijas.
 *                         Si es null se obtienen las cuentas de categor�as de
 *                         las categor�as hijas de la ra�z
 * @param int $id_view Este par�metro no es necesario si se asigna valor
 *                     a $id_category. Se utiliza para obtener la categor�a ra�z
 *                     de una vista.
 * @param string $whereAdd Permite filtrar los documentos por alg�n campo dentro
 *                         de la tabla. Por defecto no se aplica.
 * @param integer $resultsCount Cuenta de resultados que se obtuvieron con la busqueda.
 * @return Dao Un dao listo para ser usado con el daolister
 */
function dse_get_categories($query, $categories, $id_category, $id_view, $getAll = false, $whereAdd = false, $resultsCount = false) {

	if ($resultsCount && DSE_APPROXIMATE_CATEGORY_COUNTS && $resultsCount >= DSE_APPROXIMATE_CATEGORY_COUNTS) {
		//La cantidad de resultados es mayor o igual a la cantidad estipulada por el umbral, por lo
		//que hay que aproximar la cuenta de categorias.
		$dc = DB_DataObject :: factory('dse_category');
		$dc->selectAdd();
		
		$view = $dc->getView($id_view);
		
		$dc->selectAdd('*,CONCAT(ROUND(docscount/'.$view->docscount.'*' . $resultsCount . '),\'~\')  AS rcount, ROUND(docscount/'.$view->docscount.'*' . $resultsCount . ') as intrcount');
		$dc->whereAdd('docscount > 0');
		//Agrego el group y el order by para la cuenta de categor�as.
		$dc->orderBy('intrcount DESC');
		//Asigno este flag para denotar que la cuenta de categorias fue redondeada.
		
	} elseif (!$getAll) {
		$dc = DB_DataObject :: factory('dse_document_category');

		//Agrego los campos que quiero seleccionar.
		$dc->selectAdd();
		$dc->selectAdd('id_category,COUNT(*) AS rcount');
		//Agrego el group y el order by para la cuenta de categor�as.
		$dc->groupBy('id_category');
		$dc->orderBy('rcount DESC');

		//Obtengo el query realizado y hago el join. 
		$d = dse_search($query, $categories, null, $whereAdd);
		$dc->joinAdd($d, 'INNER');
	} else {
		//Se desean todas las categor�as, hago el query sobre la tabla category.
		$dc = DB_DataObject :: factory('dse_category');
	}
	
	if ($id_category !== null) {
		//Si hay drilldownn, obtengo las sub categor�as de la categor�a
		//dada por $id_category.
		$dc->whereAdd('id_parent=' . $id_category);
	} else {
		//Si no hay drilldownn, obtengo las sub categor�as de la vista
		//dada por $id_view.
		$root = DB_DataObject :: factory('dse_category');
		$root->id_view = $id_view;
		$root->whereAdd();
		$root->whereAdd('id_parent is null');
		
		# Si estoy en el inny la categoria debe tener el index1 el ID sitio
		if (dse_useInny()) {
            $root->whereAdd('index1 = ' . Inny :: getActualSitio());
        }
         
		if ($root->find(true)) {
			$dc->whereAdd('id_parent = ' . $root->id_category);
		} else {
			return null;
		}
	}
	return $dc;

}
###########################################################################
/**
 * Obtiene el path completo desde la ra�z a la categor�a dada por
 * $id_categor�a.
 * 
 * @param int $id_category id de la categor�a para la cual se quiere recuperar el path
 * @return array un arreglo conteniendo todos los Daos desde la ra�z a la categor�a.
 *               null en caso de que la categor�a no exista o sea ra�z.
 */
function dse_get_categorypath($id_category, $includeView = false) {

	$category = DB_DataObject :: factory('dse_category');

	if ($category->get($id_category)) {
		return $category->getPath($includeView);
	}

	return null;
}
###########################################################################
/**
 * Obtiene todas las vistas seg�n los par�metros especificados.
 * 
 * @return Dao eL Dao Category representando la o las vistas en cuesti�n. null
 *         en caso de que no halla sido encontrada. 
 */
function dse_get_views($index1 = false, $index2 = false, $index3 = false) {

	$category = DB_DataObject :: factory('dse_category');

	if ($index1 !== false) {
		$category->index1 = $index1;
	}

	if ($index2 !== false) {
		$category->index2 = $index2;
	}

	if ($index3 !== false) {
		$category->index3 = $index3;
	}

	$category->whereAdd('id_parent is null');

	if ($category->find()) {
		return $category;
	}

	return null;
}
###########################################################################
/**
 * Obtiene una vista por id_view.
 * 
 * @param int $id_view el id de la vista a obtener.
 * @return Dao eL Dao Category representando a la vista en cuesti�n. null
 *         en caso de que no halla sido encontrada. 
 */
function dse_get_view($id_view, $index1 = null, $index2 = null, $index3 = null) {

	$category = DB_DataObject :: factory('dse_category');
	$category->id_view = $id_view;
	$category->whereAdd('id_parent is null');
	
	if ($index1 !== null) {
		$category->whereAdd('index1 = ' . $index1);
	}
	
	if ($index2 !== null) {
		$category->whereAdd('index2 = ' . $index2);
	}
	
	if ($index3 !== null) {
		$category->whereAdd('index3 = ' . $index3);
	}		

	if ($category->find(true)) {
		return $category;
	}

	return null;
}
###########################################################################
/**
 * Chequea si existe una directiva de dse.
 * Por convencion los no,brs de las directivas empiezan con __DSE.
 * 
 * @param string $directiveName Nombre de la directiva a chquear
 * @param mixed $value Valor a verficar en la directiva
 * 
 * @return boolean 	si la directiva esta definida y el parametro $value esta
 * 					ausente y el valor de la directiva es true, retorna true.
 * 					si la directiva esta definida el parametro $value esta
 * 					presente y el valor de la directiva es === $value retorna
 * 					true.
 * 					false en cualquier otro caso.
 * 						
 */
function dse_checkDirective($directiveName, $value = true) {
	return defined($directiveName) && constant($directiveName) === $value;
}	