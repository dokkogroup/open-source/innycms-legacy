<?php
/**
 * Esto es en caso que se use el Tiny Mode
 */
$path = '../CONFIG.ini';
$file_path = file_exists($path.'.local') ? $path.'.local' : (file_exists($path) ? $path : null);
if($file_path){
    $configs = parse_ini_file($file_path,true);
    if(isset($configs['TINY_MODE']) && $configs['TINY_MODE'] == '1'){
        ini_set('include_path', '../../pear' . PATH_SEPARATOR . ini_get('include_path'));
    }
}

/**
 * Inclusi�n de archivos del Inny
 *
 * @ignore
 */
require_once '../commons/inny.common.php';
require_once '../commons/inny.tracker.php';


/**
 * Inclusi�n de DAOs
 *
 * @ignore
 */
require_once '../DAOs/Empleado.php';
require_once '../DAOs/Estado.php';
require_once '../DAOs/Configuracion.php';

/**
 *
 */
class Inny_Clientes{

    /**
     * @var string nombre de la variable de sesi�n que tendr� el ID del sitio logueado
     */
    public static $session_idSitio = 'INNY_CLIENTES_ID_SITIO';

    /**
     * @var string variable de sesi�n que determinar� si se mostrar� el contenido privado o no
     */
    public static $session_superadmin = 'INNY_CLIENTES_SUPERADMIN';

    /**
     * Averigua si el usuario est� logueado
     *
     * @static
     * @access public
     * @return boolean
     */
    public static function isLoggedUser(){
        return !empty($_SESSION[self::$session_idSitio]);
    }

    /**
     * Retorna el ID del sitio logueado actualmente en el sistema
     *
     * @static
     * @access public
     * @return integer
     */
    public static function getIdSitioLogueado(){
        return isset($_SESSION[self::$session_idSitio]) ? $_SESSION[self::$session_idSitio] : null;
    }

    /**
     * Retorna el DAO del sitio logueado actualmente en el sistema
     *
     * @static
     * @access public
     * @return DataObjects_Sitio
     */
    public static function getSitioLogueado(){
        return InnyCore_Dao::getDao('sitio',self::getIdSitioLogueado());
    }

    /**
     * Setea en sessi�n el ID del DAO, para luego verificar si el sitio est� logueado
     *
     * @param DataObjects_Sitio &$daoSitio DAO del sitio que se loguea
     * @static
     * @access public
     * @return void
     */
    public static function setSitioLogueado(&$daoSitio){
        $_SESSION[self::$session_idSitio] = $daoSitio->id_sitio;
    }

    /**
     * Verifica si el Sitio est� logueado
     *
     * @static
     * @access public
     * @return void
     */
    public static function verificarSitioLogueado(){
        if(!self::isLoggedUser()){
            Denko::redirect('index.php?logout');
        }
    }

    /**
     * Verifica que una Configuraci�n sea v�lida
     *
     * @param object &$smarty instancia de Smarty
     * @param integer $id_configuracion ID de la configuraci�n
     * @static
     * @access public
     * @return boolean
     */
    public static function verificarConfiguracionValida(&$smarty,$id_configuracion){

        # Verifico que la configuraci�n exista
        $daoConfig = Inny_Common::verificarDao($smarty,'configuracion',$id_configuracion);
        $daoConfig->fetch();

        # Verifico que la configuraci�n pertenezca al sitio
        $daoSitioActual = self::getSitioLogueado();

        # En caso que no pertenezca, lanzo un mensaje de error
        if($daoConfig->indice1 != $daoSitioActual->id_sitio){
            Inny_Common::errorCritico('contenido','contenido_errorNoExiste',array('%id' => $id_configuracion));
        }
        return true;
    }

    /**
     * Verifica que una Tarea sea v�lida
     *
     * @param object &$smarty instancia de Smarty
     * @param integer $id_tarea ID de la tarea
     * @static
     * @access public
     * @return DataObjects_Tarea
     */
    public static function verificarTareaValida(&$smarty,$id_tarea){

        # Verifico que la tarea exista
        $daoTarea = Inny_Common::verificarDao($smarty,'tarea',$id_tarea);
        $daoTarea->fetch();

        # Verifico que la tarea pertenezca al sitio
        $daoSitioActual = self::getSitioLogueado();

        # En caso que no pertenezca, lanzo un mensaje de error
        if($daoTarea->id_sitio != $daoSitioActual->id_sitio){
            Inny_Common::errorCritico('tarea','tarea_errorNoExiste',array('%id'=>$id_tarea));
        }
        return $daoTarea;
    }

    /**
     * Verifica que un Temporal sea v�lido
     *
     * @param object &$smarty instancia de Smarty
     * @param integer $id_temporal ID del temporal
     * @static
     * @access public
     * @return void
     */
    public static function verificarTemporalValido(&$smarty,$id_temporal){

        # Verifico que la relaci�n exista
        $daoTareaTemporal = checkDaoValido($smarty,'tarea_temporal',$id_temporal,'id_temporal');
        $daoTareaTemporal->fetch();

        # Verifico que la tarea pertenezca al sitio logueado actualmente
        $daoTarea = $daoTareaTemporal->getTarea();
        $daoSitioActual = self::getSitioLogueado();

        # En caso que no pertenezca, lanzo un mensaje de error
        if($daoTarea->id_sitio != $daoSitioActual->id_sitio){
            Inny_Common::errorCritico('archivo','archivo_errorNoExiste',array('%id'=>$id_temporal));
        }
    }

    /** Verifica que un id de archivo temporal pertenezca al sitio actual
     * @param $id_temporal
     * @return bool
     */
    public static function temporalValidoSitio($id_temporal){
        /** @var $producto_temporal DataObjects_Producto_temporal */
        $producto_temporal = Denko :: daoFactory('Producto_temporal');
        $producto_temporal->id_temporal=$id_temporal;
        if ($producto_temporal->find(true)){
            $producto=$producto_temporal->getProducto();
            $sitio=$producto->getSitio();
            if(self::getIdSitioLogueado()==$sitio->id_sitio){ return true;}
        }
        /** @var $config DataObjects_Configuracion */
        $config = Denko :: daoFactory('Configuracion');
        $config->valor=$id_temporal;
        if ($config->find(true)){
            if(self::getIdSitioLogueado()==$config->indice1){ return true;}
        }
        return false;
    }


    /**
     * Verifica que el sitio est� en Tiny Mode. Se utiliza para restringir funcionalidades
     *
     * @param object &$smarty instancia de Smarty
     * @static
     * @access public
     * @return void
     */
    public static function verificarTinyMode(&$smarty){
        $daoSitioLogueado = self::getSitioLogueado();
        if($daoSitioLogueado->getConfig('tiny_mode') == '1'){
            Inny_Common::errorCritico('seccion','seccion_errorDeshabilitada');
        }
    }

    /**
     * Verifica que el sitio pueda administrar categor�as
     *
     * @param object &$smarty instancia de Smarty
     * @static
     * @access public
     * @return void
     */
    public static function verificarAdminstracionDeCategorias(&$smarty){
        $daoSitioLogueado = self::getSitioLogueado();
        if(!$daoSitioLogueado->soportaDSE()){
            Inny_Common::errorCritico('seccion','seccion_errorDeshabilitada');
        }
    }

    /**
     * Verifica que el sitio pueda registrar usuarios
     *
     * @param object &$smarty instancia de Smarty
     * @static
     * @access public
     * @return void
     */
    public static function verificarRegistroDeUsuarios(&$smarty){
        $daoSitioLogueado = self::getSitioLogueado();
        if($daoSitioLogueado->getConfig('register_users') == '0'){
            Inny_Common::errorCritico('seccion','seccion_errorDeshabilitada');
        }
    }

    /**
     * Setea si se deben mostrar los contenidos y listados privados
     *
     * @param boolean $opcion opcion que setear. por default asume el param GET superadmin
     * @static
     * @access public
     * @return void
     */
    public static function setMostrarContenidosPrivados($opcion=null){
        if((isset($_GET['superadmin']) && ($_GET['superadmin'] == 'true' || $_GET['superadmin'] == 'false')) || is_bool($opcion)){
            if((isset($_GET['superadmin']) && $_GET['superadmin'] == 'true') || $opcion === true){
                $_SESSION[self::$session_superadmin] = 'true';
            }
            else{
                unset($_SESSION[self::$session_superadmin]);
            }
        }
    }

    /**
     * Verifica si se deben mostrar los contenidos y listados privados
     *
     * @static
     * @access public
     * @return boolean
     */
    public static function mostrarContenidosPrivados(){
        return (isset($_SESSION[self::$session_superadmin]) && $_SESSION[self::$session_superadmin] == 'true');
    }

    ////////////////////////////////////////////////////////////////////////////
    //                         DATOS DEL SITIO                                //
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    public static function validarCambioPasswordSitio(&$smarty){

        # Cargo los speechs
        InnyCMS_Speech::config_load($smarty,'error');
        InnyCMS_Speech::config_load($smarty,'password');

        # Obtengo el Sitio Logueado
        $daoSitioLogueado = self::getSitioLogueado();

        # Verifico que el campo 'Contrase�a Actual' est� seteado
        $password = !empty($_POST['password'])? $_POST['password'] : '';
        if($password == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'password_actual'));
        }

        # Verifico que la contrase�a sea correcta
        elseif($password != $daoSitioLogueado->password){
            Denko::addErrorMessage('password_errorPasswordIncorrecto');
            return false;
        }

        # Verifico que el campo 'Nueva Contrase�a' est� seteado
        $newpass = !empty($_POST['newpass'])? $_POST['newpass'] : '';
        if($newpass == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'password_nueva'));
        }

        # Verifico que el campo 'Confirmar Nueva Contrase�a' no sea nulo
        $confirm_newpass = !empty($_POST['confirm_newpass'])? $_POST['confirm_newpass'] : '';
        if($confirm_newpass == ''){
            Denko::addErrorMessage('error_campoRequerido',array('%nombre_campo' => 'password_confirmarNueva'));
        }

        # Verifico los campos 'Nueva Contrase�a' y 'Confirmar Nueva Contrase�a' sean iguales.
        elseif($newpass != $confirm_newpass){
            Denko::addErrorMessage('password_errorCamposIguales',array('%campo_nueva' => 'password_nueva', '%campo_confirmar' => 'password_confirmarNueva'));
        }

        # En caso que est� todo OK
        if(!Denko::hasErrorMessages()){
            $daoSitioLogueado->password = $newpass;
            $daoSitioLogueado->update();
            return true;
        }

        # En caso que haya errores
        return false;
    }

}
################################################################################