<?php
/**
 * Project: Inny CMS
 * File: common.php
 *
 * @copyright 2007-2008 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
################################################################################
require_once '../libs/Smarty.class.php';
require_once '../denko/dk.denko.php';
require_once '../DAOs/Contenido.php';
require_once '../commons/dokko_configurator.php';
require_once '../commons/DFM.php';
require_once '../commons/fecha.php';
require_once '../commons/inny.core.php';
require_once '../commons/inny.dse.php';
require_once '../commons/inny.linkback.php';
require_once '../commons/dse_common.php';
################################################################################
# Seteo el charset ISO-8859-1 (para request de AJAX)
header('content-type: text/html; charset=iso-8859-1');

# Seteo el locale
setlocale(LC_ALL,'es_AR');

# Se corrigen URLs
Denko::fixUrl();

# Se abre la DB
Denko::openDB();
Denko::daoFactory('sitio')->getDatabaseConnection()->query('SET CHARSET latin1;');

# Verifico si se debe abrir la session
if(!isset($INNY_START_SESSION) || $INNY_START_SESSION === true){
    Denko::sessionStart();
}

# Cargo las configuraciones del DSE
Inny::dse_loadConfigs();

# Seteo el source para el DFM
DFM::setSource(DFM_DB,'temporal');
################################################################################
/**
 *
 */
class Inny_Common{

    /**
     * @var object instancia de smarty
     * @static
     * @access public
     */
    public static $smarty = null;

    /**
     *
     *
     */
    public static function initialize(&$smarty){
        self::$smarty = $smarty;
        InnyCMS_Speech::init();
    }

    /**
     * Revisa si el archivo adjunto es v�lido
     *
     * @param array $filedata datos del upload (�ndice de $_FILES)
     * @param string $filetype tipo de archivo
     * @param $required indica si el archivo adjunto es requerido
     * @param
     */
    public static function validateAttachedFile($filedata,$filetype='file',$required=true){

        # Si el archivo no es adjunto
        # Si es requerido, retorno el c�digo de error. Sin�, retorno que todo est� OK
        if(empty($filedata['tmp_name'])){
            return $required ? UPLOAD_ERR_NO_FILE : UPLOAD_ERR_OK;
        }

        else{

            # En caso que sea imagen, verifico que sea v�lida
            if($filetype == 'image'){
                $validate = Inny::validateAttachedImageFile($filedata,$errorCode);
            }

            # En caso que no sea imagen, verifico que los datos del upload sean
            # correctos
            else{
                $validate = Inny::validateAttachedFile($filedata,$errorCode);
            }

            # En caso que haya errores
            return $validate ? UPLOAD_ERR_OK : $errorCode;
        }
    }

    /**
     * Verifica si existe un DAO con cierto ID.
     * Si existe, retorna el DAO. Sin�, muestra una p�gina de error
     *
     * @param object &$smarty instancia de Smarty
     * @param string $table nombre de la tabla en la DB
     * @param integer $id_dao ID del DAO
     * @static
     * @access public
     * @return DB_DataObject
     */
    public static function verificarDao(&$smarty,$table,$id_dao,$pk_name=null){

        # Cargo los mensajes de error
        InnyCMS_Speech::config_load($smarty,'error');

        # Verifico que el ID est� seteado
        if(empty($id_dao)){
            Denko::addErrorMessage('error_parametroRequerido',array('%nombre_parametro' => 'id_'.$table));
            self::mostrarErrorCritico($smarty);
        }

        # Verifico que el ID sea un n�mero entero:
        elseif(!Denko::isInt($id_dao)){
            Denko::addErrorMessage('error_parametroNoEsNumeroEntero',array('%nombre_parametro' => 'id_'.$table));
            self::mostrarErrorCritico($smarty);
    	}

        # Verifico que el DAO exista en la DB
        $dao = DB_DataObject::factory($table);
        $id =  ($pk_name == null ? 'id_'.$table : $pk_name);
        $dao->$id = $id_dao;
        if(!$dao->find()){
            Denko::addErrorMessage('error_dbDaoNoExiste',array('%nombre_tabla' => $table,'%id' => $id_dao));
            self::mostrarErrorCritico($smarty);
        }

        # Retorno el DAO
        return $dao;
    }

    /**
     * Verifica que una acci�n sea v�lida
     *
     * @param object &$smarty instancia Smarty
     * @param string $action acci�n que chequear
     * @param array $actions arreglo de acciones v�lidas
     * @static
     * @access public
     * @return void
     */
    public static function verificarAccion($actions){

        # Nombre del par�metro
        $param_name = 'action';

        # Obtengo el valor del par�metro
        $action = InnyCore_Utils::getParamValue($param_name);

        # En caso que la acci�n no est� seteada
        if($action === '' || $action === null){
            self::errorCritico('error','error_parametroRequerido',array('%nombre_parametro' => $param_name));
        }

        # En caso que la acci�n no est� contemplada
        if(!in_array($action,$actions)){
            self::errorCritico('error','error_parametroNoValido',array('%nombre_parametro' => $param_name));
        }

        # En caso que est� todo OK
        return true;
    }

    /**
     * Verifica que un DAO sea v�lido
     *
     * @static
     * @access public
     * @return integer c�digo de error
     */
    public static function verificarDaoCommon($table,$id_dao,$pk_name=null){

        # Verifico que el ID est� seteado
        if(empty($id_dao)){
            return INNY_ERROR_MISSING_PARAM;
        }

        # Verifico que el ID sea un n�mero entero:
        if(!Denko::isInt($id_dao)){
            return INNY_ERROR_NAN;
    	}

    	# Verifico que el DAO exista en la DB
        $dao = DB_DataObject::factory($table);
        $id = ($pk_name == null ? 'id_'.$table : $pk_name);
        $dao->$id = $id_dao;
        if(!$dao->find()){
            return INNY_ERROR_DB_NO_EXISTS;
        }

        # En caso que todo est� OK
        return INNY_ERROR_OK;
    }

    /**
     * Verifica que el Usuario Registrado sea v�lido
     *
     * @param Smarty &$smarty instancia de Smarty
     * @param integer $id_usuario_registrado ID del usuario registrado
     * @param DataObjects_Listado &$daoListadoUsuariosRegistrados listado de usuarios registrados
     * @static
     * @access public
     * @return boolean
     */
    public static function verificarUsuarioRegistrado(&$smarty,$id_usuario_registrado,&$daoListadoUsuariosRegistrados){

        # Cargo los mensajes de error
        InnyCMS_Speech::config_load($smarty,'error');

        # Hago la verificaci�n del DAO
        $error_code = self::verificarDaoCommon('producto',$id_usuario_registrado);

        # Veo que hago, en funci�n del c�digo de error obtenido
        switch($error_code){

            # En caso que el par�metro '$id_usuario_registrado' sea nulo
            case INNY_ERROR_MISSING_PARAM:
                Denko::addErrorMessage('error_parametroRequerido',array('%nombre_parametro' => 'id_usuario'));
                self::mostrarErrorCritico($smarty);
                break;

            # En caso que el par�metro '$id_usuario_registrado' no sea un n�mero entero v�lido
            case INNY_ERROR_NAN:
                Denko::addErrorMessage('error_parametroNoEsNumeroEntero',array('%nombre_parametro' => 'id_usuario'));
                self::mostrarErrorCritico($smarty);
                break;

            # En caso que el id_usuario no corresponda a un usuario registrado en la DB
            case INNY_ERROR_DB_NO_EXISTS:
                Denko::addErrorMessage('error_dbDaoNoExiste',array('%nombre_tabla' => 'usuario', '%id' => $id_usuario_registrado));
                self::mostrarErrorCritico($smarty);
                break;

            # En caso que todo est� OK
            default: break;
        }

        # Verifico que el usuario pertenezca registrado pertenezca al sitio
        $daoUsuarioRegistrado = DB_DataObject::factory('producto');
        $daoUsuarioRegistrado->get($id_usuario_registrado);
        if($daoUsuarioRegistrado->id_listado != $daoListadoUsuariosRegistrados->id_listado){
            Denko::addErrorMessage('usuarioRegistrado_error_dbNoExists',array('%id_usuario'=>$id_usuario_registrado));
            self::mostrarErrorCritico($smarty);
        }

        # En caso que todo est� OK
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    //                           MANEJO DE ERRORES                            //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Muestra la pantalla de error ante un error cr�tico
     *
     * @param Smarty &$smarty instancia de smarty
     * @static
     * @access public
     * @return void
     */
    public static function mostrarErrorCritico(&$smarty){
        $smarty->display('error.tpl');
        exit(0);
    }

    /**
     *
     *
     */
    public static function errorCritico($seccion,$mensaje,$reemplazos=null){
        InnyCMS_Speech::config_load(self::$smarty,$seccion);
        Denko::addErrorMessage($mensaje,$reemplazos);
        self::mostrarErrorCritico(self::$smarty);
    }
}
################################################################################
