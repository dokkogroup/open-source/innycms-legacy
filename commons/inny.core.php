<?php
/**
 * Project: Inny CMS
 * File: inny.core.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */

////////////////////////////////////////////////////////////////////////////////

# Core
require_once '../denko/dk.denko.php';
require_once '../denko/dk.file.php';
require_once '../DAOs/Contenido.php';
require_once '../commons/fecha.php';
require_once '../commons/DFM.php';
require_once '../commons/inny/core.speech.php';

# Elementos principales
require_once '../commons/inny/core.elemento.php';
require_once '../commons/inny/core.contenido.php';
require_once '../commons/inny/core.producto.php';

# Modulos y core
require_once '../commons/inny/core.utils.php';
require_once '../commons/inny/core.type.php';
require_once '../commons/inny/core.image.php';
require_once '../commons/inny/core.dao.php';
require_once '../commons/inny/core.file.php';
require_once '../commons/inny/core.metadata.php';

# M�dulos
require_once '../commons/inny/module.cardinalidad.php';
require_once '../commons/inny/module.reporter.php';

////////////////////////////////////////////////////////////////////////////////

# Constantes para los tipos de datos
define('INNY_TIPO_CONTENIDO','contenido');
define('INNY_TIPO_PRODUCTO','producto');
define('INNY_IS_REQUIRED','@REQUIRED@');

# C�digos de error
define('INNY_ERROR_OK',0);
define('INNY_ERROR_MISSING_PARAM',1);
define('INNY_ERROR_NAN',2);
define('INNY_ERROR_DB_NO_EXISTS',3);

# Constantes del DSE
define ('INNY_DSE_SO_FIELDS',2);
define ('INNY_DSE_FIELD_MAPPINGS',3);

////////////////////////////////////////////////////////////////////////////////

/**
 * Clase principal del framework.
 *
 * @package Inny
 */
class Inny{

    /**
     *
     */
    public static $version = '0.1.8';

    /**
     *
     */
    public static $configKey = 'INNY_PARSED_CONFIG';

    /**
     *
     */
    public static $configPath = '../CONFIG.ini';

    /**
     *
     */
    public static $supportedDataTypes =  array('text','textarea','richtext','string','integer','float','date','time','datetime','select','radio','checkbox','email');

    /**
     *
     */
    public static $supportedFileTypes =  array('file','image','sound','flash');

    /**
     *
     *
     */
    public static $textTypes = array('text','string','textarea','richtext');

    /**
     *
     */
    public static $daoCacheKey = 'INNY_CACHE_DAO_';

    /**
     *
     */
    public static $registeredUsersListName = 'usuarios_registrados';

    /**
     *
     */
    public static $url_gadget = 'inny.gadget.php';

    /**
     * @var string variable de sesi�n que determinar� si se mostrar� el contenido privado o no
     * @access public
     */
    public static $session_superadmin = 'INNY_SUPERADMIN';

    /**
     * Muestra un mensaje de error ante un error cr�tico
     *
     * @param string $message mensaje de error
     * @static
     * @access public
     * @return void
     */
    public static function criticalError($message){
        echo '<b>INNY ERROR:</b> '.$message;
        exit;
    }

    /**
     * Obtiene las configuraciones del sitio del archivo de configuraciones INI
     *
     * @static
     * @access public
     * @return string
     */
    public static function getConfigIniData(){

        # 1ero verifico que las configuraciones no est�n cacheadas
        if(!isset($GLOBALS[self::$configKey])){

            # Verifico cual de los archivos de configuraci�n debo leer
            $configPath = self::$configPath;
            $filename = file_exists($configPath.'.local') ? ($configPath.'.local') : $configPath;

            # Verifico que el archivo de configuraci�n exista
            if(!file_exists($filename)){
                self::criticalError('El archivo de configuraciones '.$filename.' no existe.');
            }

            # Retorno el archivo de configuraciones parseado
            $GLOBALS[self::$configKey] = parse_ini_file($filename,true);
        }

        # Retorno el arreglo con el archivo de configuraciones parseado
        return $GLOBALS[self::$configKey];
    }

    /**
     * Verifica si el sitio est� en mantenimiento
     *
     * @static
     * @access public
     * @return void
     */
    public static function verifyMaintenance(){
        $daoSitio = self::getDaoSitio();
        if($daoSitio && $daoSitio->mantenimiento == '1'){
            $smarty = new Smarty();
            $smarty->display('mantenimiento.tpl');
            exit(0);
        }
    }

    /**
     * Retorna el path al archivo de configuraciones del sitio
     *
     * @static
     * @access public
     * @return string path al archivo de configuraciones del sitio
     */
    public static function getConfigFilePath(){
        return file_exists(self::$configPath.'.local') ? self::$configPath.'.local' : (file_exists(Inny::$configPath) ? Inny::$configPath : null);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                              TIPOS DE DATOS                            //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Verifica que un tipo de dato exista
     *
     * @param string $type tipo de dato
     * @static
     * @access public
     * @return boolean si el tipo de dato existe
     */
    public static function isSupportedType($type){
        return (!empty($type) && (in_array($type,self::$supportedDataTypes) || in_array($type,self::$supportedFileTypes)));
    }

    /**
     * Verifica si el tipo de texto existe
     *
     * @param string $type tipo de dato
     * @static
     * @access public
     * @return boolean resultado de la verificacion
     */
    public static function isSupportedDataType($type){
        return in_array(strtolower($type),self::$supportedDataTypes);
    }

    /**
     * Verifica si el tipo de archivo existe
     *
     * @param string $type tipo de archivo
     * @static
     * @access public
     * @return boolean resultado de la verificacion
     */
    public static function isSupportedFileType($type){
        $type = strtolower(trim($type));
        return !empty($type) && in_array($type,self::$supportedFileTypes);
    }

    /**
     * Verifica que el tipo de dato sea del subtipo texto
     *
     * @param string $type tipo de dato de texto
     * @static
     * @access public
     * @return boolean resultado de la verificacion
     */
    public static function isTextType($type){
        return in_array($type,self::$textTypes);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                 DSE                                    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Retorna las configuraciones del DSE para el sitio
     *
     * @static
     * @access public
     * @return array configuraciones del DSE para el sitio
     */
    public static function getDseMetadata(){
        if(isset($GLOBALS[self::$configKey])){
            $id_sitio = self::getActualSitio();
        }
        else{
            $id_sitio = isset($_SESSION['INNY_CLIENTES_ID_SITIO']) ? $_SESSION['INNY_CLIENTES_ID_SITIO'] : null;
        }
        if($id_sitio){
            $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);
            return $daoSitio->getConfig('dse');
        }
        return null;
    }

    /**
     * Carga las configuraciones del DSE
     *
     * @static
     * @access public
     * @return void
     */
    public static function dse_loadConfigs(){
        $id_sitio = self::getIdSitioActual();
        if($id_sitio){
            $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);
            $dse_configs = $daoSitio->getConfig('dse');
            if(!empty($dse_configs['enabled']) && $dse_configs['enabled'] == '1'){
                dse_loadConfigs();
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                 SITIO                                  //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Retorna el ID del Sitio con cual se est� trabajando
     *
     * @static
     * @return integer ID del DAO Sitio actual
     */
    public static function getActualSitio(){

        # Obtengo los datos del archivo de configuraciones
        $config = self::getConfigIniData();

        # Retorno el ID del sitio
        return !empty($config['ID_SITIO']) ? $config['ID_SITIO'] : null;
    }

    /**
     * Setea el ID del Sitio con cual se est� trabajando
     *
     * @static
     * @return void
     */
    public static function setActualSitio(){

        # Obtengo los datos del archivo de configuraciones
        $config = self::getConfigIniData();

        # Verifico que el ID del Sitio este seteado
        $id_sitio = isset($config['ID_SITIO']) ? trim($config['ID_SITIO']) : '';
        if($id_sitio == ''){
            self::criticalError('El ID del Sitio es requerido en el archivo de configuraciones.');
        }

        # Verifico que el sitio exista en la DB
        $daoSitio = DB_DataObject::factory('sitio');
        if(!$daoSitio->get($id_sitio)){
            self::criticalError('El Sitio con ID '.$id_sitio.' no existe en el sistema.');
        }
    }

    /**
     * Verifica si actualmente se est� trabajando en un sitio
     *
     * @static
     * @access public
     * @return boolean si actualmente se est� trabajando en un sitio
     */
    public static function haySitioLogueado(){
        return isset($GLOBALS[self::$configKey]['ID_SITIO']);
    }

    /**
     * Retorna el ID del Sitio actual, ya sea en el CMS o sitio web normal
     *
     * @static
     * @access public
     * @return integer ID del Sitio actual, ya sea en el CMS o sitio web normal
     */
    public static function getIdSitioActual(){
        return isset($GLOBALS[self::$configKey]) ? self::getActualSitio() : (isset($_SESSION['INNY_CLIENTES_ID_SITIO']) ? $_SESSION['INNY_CLIENTES_ID_SITIO'] : null);
    }

    /**
     * Retorna el DAO de un Sitio. Por defecto, retorna el del sitio actual
     *
     * @param integer $id_sitio ID del Sitio que se quiere obtener
     * @static
     * @access public
     * @return DataObjects_Sitio
     */
    public static function getDaoSitio($id_sitio=null){
        return InnyCore_Dao::getDao('sitio',$id_sitio ? $id_sitio : self::getIdSitioActual());
    }

    /**
     * Retorna el DAO (cacheado) del Sitio logueado actualmente.
     *
     * @static
     * @access public
     * @return DataObjects_Sitio DAO del Sitio logueado actualmente
     */
    public static function getDaoSitioActual(){
        return self::getDaoSitio();
    }

    /**
     * Alias de la funci�n getIdSitioActual()
     *
     * @deprecated
     * @see Inny::getIdSitioActual()
     * @static
     * @access public
     * @return integer ID del Sitio actual, ya sea en el CMS o sitio web normal
     */
    public static function getLoggedIdSitio(){
        return self::getIdSitioActual();
    }

    /**
     * Alias de getDaoSitioActual()
     *
     * @deprecated
     * @see Inny::getDaoSitioActual()
     * @static
     * @access public
     * @return DataObjects_Sitio DAO del Sitio logueado actualmente
     */
    public static function getCachedDaoSitio(){
        return self::getDaoSitioActual();
    }

    ////////////////////////////////////////////////////////////////////////////
    //                               PRODUCTOS                                //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Retorna el producto referente al id_producto y listado correspondiente
     *
     * @param integer $id_producto ID de producto
     * @param string $nombre_listado nombre del listado
     * @static
     * @access public
     * @return DataObjects_Producto
     */
    public static function getProducto($id_producto,$nombre_listado){
        if(empty($id_producto) || empty($nombre_listado)) return null;
        $daoProducto = DB_DataObject::factory('producto');
        $daoProducto->id_producto=$id_producto;
        $daoProducto->whereAdd('
            id_listado = (
                select id_listado
                from listado
                where nombre = \''.$nombre_listado.'\'
                and id_sitio = '.self::getIdSitioActual().'
            )
        ');
        return $daoProducto->find(true) ? $daoProducto : null;
    }

    /**
     * Retorna un producto de un listado que corresponda a una posici�n dada
     *
     *
     * @param string $nombre_listado nombre de listado
     * @param integer $posicion posici�n del producto en el listado
     * @static
     * @access public
     * @return DataObjects_Producto DAO Producto
     */
    public static function getProductoPorPosicion($nombre_listado,$posicion){

        # Verifico que la posici�n sea un n�mero entero v�lido
        $posicion = trim($posicion);
        if(!InnyCore_Utils::is_integer($posicion)){
            InnyModule_Reporter::api_error(E_USER_WARNING,'La posici�n debe ser un n�mero entero v�lido',debug_backtrace());
            return null;
        }

        $daoListado = self::_getListadoPorNombre($nombre_listado,debug_backtrace());
        if(empty($daoListado)){
            return null;
        }

        # Obtengo el Producto que corresponda a la posici�n dada
        $daoProducto = $daoListado->getProductoPorPosicion($posicion);
        if(empty($daoProducto)){
            InnyModule_Reporter::api_error(E_USER_WARNING,'No exite en el listado "'.$nombre_listado.'" producto en la posici�n '.$posicion.'',debug_backtrace());
            return null;
        }

        # Retorno el producto
        return $daoProducto;
    }

    /**
     * Inserta un producto, y setea las relaciones con otros productos
     *
     * @param array $data contenido del producto
     * @param string $nombre_listado nombre del listado
     * @param array $settings seteos
     * @static
     * @access public
     * @return mixed cantidad de productos insertados, FALSE en caso de error
     */
    public static function insertarProducto($data,$nombre_listado,$settings=null){

        # Verifico que haya sitio logueado
        if(!self::haySitioLogueado()){
            return false;
        }

        # Obtengo el DAO del Sitio actualmente logueado
        $daoSitio = self::getCachedDaoSitio();

        # Retorno el resultado de la inserci�n
        return InnyCore_Producto::insertar($daoSitio,$data,$nombre_listado,$settings,debug_backtrace());
    }

    /**
     * Edita un producto, y setea las relaciones con otros productos
     *
     * @param array $data contenido del producto
     * @param string $nombre_listado nombre del listado
     * @param integer $id_producto ID del producto que se desea editar
     * @param array $settings seteos
     * @static
     * @access public
     * @return mixed cantidad de productos editados, FALSE en caso de error
     */
    public static function editarProducto($data,$nombre_listado,$id_producto,$settings=null){

        # Verifico que haya sitio logueado
        if(!self::haySitioLogueado()){
            return false;
        }

        # Obtengo el DAO del Sitio actualmente logueado
        $daoSitio = self::getCachedDaoSitio();

        # Retorno el resultado de la edici�n
        return InnyCore_Producto::editar($daoSitio,$data,$nombre_listado,$id_producto,$settings,debug_backtrace());
    }

    /**
     * Elimina un producto
     *
     * @param string $nombre_listado nombre del listado al que pertenece el producto
     * @param integer $id_producto ID del DAO Producto
     * @static
     * @access public
     * @return mixed numero de productos eliminados, o FALSE en caso de error
     */
    public static function eliminarProducto($nombre_listado,$id_producto){

        # Verifico que haya sitio logueado
        if(!self::haySitioLogueado()){
            return false;
        }

        # Obtengo el DAO del Sitio actualmente logueado
        $daoSitio = self::getCachedDaoSitio();

        # Retorno el resultado de la eliminaci�n
        return InnyCore_Producto::eliminar($daoSitio,$nombre_listado,$id_producto,debug_backtrace());
    }

    ////////////////////////////////////////////////////////////////////////////
    //                               LISTADO                                  //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Funci�n auxiliar. Obtiene el DAO Listado correspondiente a tal nombre
     *
     * @param string $nombre_listado nombre del listado
     * @static
     * @access protected
     * @return DataObjects_Listado DAO Listado
     */
    protected static function _getListadoPorNombre($nombre_listado,$debug_backtrace){

        # Verifico que el tipo de dato sea string
        if(!is_string($nombre_listado)){
            InnyModule_Reporter::api_error(E_USER_WARNING,'El par�metro "$nombre_listado" es incorrecto: debe ser tipo string',$debug_backtrace);
            return null;
        }

        # Verifico que el nombre del listado no est� vac�o
        $nombre_listado = trim($nombre_listado);
        if(empty($nombre_listado)){
            InnyModule_Reporter::api_error(E_USER_WARNING,'El par�metro "$nombre_listado" es requerido',$debug_backtrace);
            return null;
        }

        # Formateo el nombre del listado
        $nombre_listado = Denko::lower($nombre_listado);

        # Verifico que el listado exista en la DB
        $daoSitio = self::getDaoSitioActual();
        if(empty($daoSitio)){
            InnyModule_Reporter::api_error(E_USER_WARNING,'No puede obtenerse el listado "'.$nombre_listado.'": actualmente no se est� trabajando con sitio alguno',$debug_backtrace);
            return null;
        }

        # Obtengo el listado con nombre $nombre_listado
        $daoListado = $daoSitio->getListadoPorNombre($nombre_listado);

        # Si no existe listado con ese nombre, lanzo el error
        if(empty($daoListado)){
            InnyModule_Reporter::api_error(E_USER_WARNING,'No existe listado con nombre "'.$nombre_listado.'" para el sitio "'.$daoSitio->nombre_sitio.'"',$debug_backtrace);
        }

        # Retorno el DAO Listado
        return $daoListado;
    }

    /**
     * Retorna el ID del listado correspondiente al nombre y al sitio actual
     *
     * @param string $nombre_listado nombre del listado
     * @static
     * @access public
     * @return integer en caso que exista o NULL si no existe
     */
    public static function getIdListadoPorNombre($nombre_listado){
        $daoListado = self::_getListadoPorNombre($nombre_listado,debug_backtrace());
        return !empty($daoListado) ? $daoListado->id_listado : null;
    }

    /**
     * Obtiene el DAO Listado correspondiente a tal nombre
     *
     * @param string $nombre_listado nombre del listado
     * @static
     * @access public
     * @return DataObjects_Listado DAO Listado
     */
    public static function getListadoPorNombre($nombre_listado){
        return self::_getListadoPorNombre($nombre_listado,debug_backtrace());
    }

    /**
     * Retorna el ID del listado correspondiente al nombre y al sitio actual
     *
     * @deprecated
     * @see Inny::getIdListadoPorNombre()
     * @param string $nombre_listado nombre del listado
     * @static
     * @access public
     * @return integer en caso que exista o NULL si no existe
     */
    public static function getIdListadoByName($nombre_listado){
        return self::getIdListadoPorNombre($nombre_listado);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                              CONTENIDOS                                //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Edita el valor de un contenido
     *
     * @param string $contenido_nombre nombre del contenido
     * @param array $data datos que ser�n seteados en el contenido
     * @static
     * @access public
     * @return boolean si pudo editarse el valor del contenido
     */
    public static function editarContenido($contenido_nombre,$data){

        # Seteo el debug_backtrace para el logueo de errores
        InnyCore_Contenido::setDebugBacktrace(debug_backtrace());

        # Edito el contenido
        return InnyCore_Contenido::editarContenido($contenido_nombre,$data);
    }

    /**
     * Borra el valor de un contenido
     *
     * @param string $contenido_nombre nombre del contenido
     * @static
     * @access public
     * @return boolean si pudo eliminarse el valor del contenido
     */
    public static function borrarContenido($contenido_nombre){

        # Seteo el debug_backtrace para el logueo de errores
        InnyCore_Contenido::setDebugBacktrace(debug_backtrace());

        # Borro el valor del contenido
        return InnyCore_Contenido::borrarContenido($contenido_nombre);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                               PERMISOS                                 //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Indica que actualmente se tiene permiso para ejecutar acciones solo
     * permitidas en el tracker
     *
     * @static
     * @access public
     * @return void
     */
    public static function setPermisosTracker($forzar=null){
        if((is_bool($forzar) && $forzar === true) || (isset($_GET['superadmin']) && $_GET['superadmin'] == 'true')){
            $_SESSION[self::$session_superadmin] = true;
        }
    }

    /**
     * Averigua si actualmente se tiene permiso para ejecutar acciones solo
     * permitidas en el tracker
     *
     * @static
     * @access public
     * @return boolean si actualmente se tiene permiso para ejecutar acciones solo permitidas en el tracker
     */
    public static function getPermisosTracker(){
        return (isset($_SESSION[self::$session_superadmin]) && $_SESSION[self::$session_superadmin] === true);
    }
}
################################################################################
