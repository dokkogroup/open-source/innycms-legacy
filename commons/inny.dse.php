<?php
/**
 *
 */
require_once '../commons/inny.core.php';

/**
 *
 */
define('INNY_DSE_MAX_LEVEL_CATEGORY',4);

/**
 *
 */
class Inny_DSE{

    /**
     * Obtiene las subcategor�as de una categor�a o vista
     *
     * @params integer $id_sitio
     * @params integer $id_parent_category=null
     * @params boolean $urlencode
     * @static
     * @access public
     * @return array
     */
    public static function getSubcategorias($id_sitio,$id_parent_category=null,$urlencode=false){

        $id_parent = !empty($id_parent_category) ? $id_parent_category : null;

        # Creo la instancia del DAO
        $daoDseCategory = DB_DataObject::factory('dse_category');

        # Seteo que s�lo necesito traer el ID y el nombre
        $daoDseCategory->selectAdd();
        $daoDseCategory->selectAdd('id_category,name');

        # Indico que me retorne las categor�as ordenadas por nombre
        $daoDseCategory->orderBy('name');

        # En caso que el parent sea NULL, obtengo las categor�as principales
        if($id_parent == null){
            $daoDseCategory->whereAdd('id_parent is null and index1 = '.$id_sitio);
        }

        # Sin�, obtengo las subcategor�as
        else{
            $daoDseCategory->whereAdd('id_parent = '.$id_parent);
        }

        # En caso que no haya categor�as, retorno NULL
        if(!$daoDseCategory->find()){
            return null;
        }

        # Obtengo las categor�as
        $subcategorias = array();
        while($daoDseCategory->fetch()){
            $subcategorias[$daoDseCategory->id_category] = $urlencode ? urlencode($daoDseCategory->name) : $daoDseCategory->name;
        }

        # Retorno el arreglo con las categor�as
        return $subcategorias;
    }

    /**
     *
     * @param integer $id_sitio ID del Sitio
     * @param integer $id_categoria ID de la categor�a
     * @
     */
    public static function verificarCategoriaValida($id_sitio,$id_categoria){

        # Verifico que el par�metro 'id_category' exista y sea v�lido
        if($id_categoria == null){
            Denko::addErrorMessage('error_parametroRequerido',array('%nombre_parametro' => 'id_category'));
            return false;
        }

        # Verifico que sea un entero v�lido
        if(!Denko::isInt($id_categoria)){
            Denko::addErrorMessage('error_parametroNoValido',array('%nombre_parametro' => 'id_category'));
            return false;
        }

        # Verifico que el la categor�a exista en la DB
        $daoDseCategory = self::getCategoria($id_categoria);
        if($daoDseCategory == null){
            Denko::addErrorMessage('dse_errorNoExisteCategoria',array('%id' => $id_categoria));
            return false;
        }

        # Verifico que la categor�a pertenezca al sitio
        $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);
        if(!$daoSitio->perteneceCategoria($id_categoria)){
            Denko::addErrorMessage('dse_errorNoExisteCategoria',array('%id' => $id_categoria));
            return false;
        }

        # En caso que todo est� bien
        return true;
    }

    /**
     * Obtiene una el DAO de una categor�a
     *
     * @param integer $id_categoria ID del DAO dse_category
     * @static
     * @access public
     * @return DataObjects_Dse_category DAO categor�a si existe, NULL en caso contrario
     */
    public static function getCategoria($id_categoria){
        return InnyCore_Dao::getDao('dse_category',$id_categoria,array('primary_key'=>'id_category'));
    }
}
################################################################################