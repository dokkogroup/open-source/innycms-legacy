<?php

/**
 *
 */
require_once '../denko/dk.denko.php';

/**
 *
 */
class Inny_LinkBack{

    /**
     *
     */
    public $session_key;

    /**
     *
     */
    public $script_default;

    /**
     *
     */
    public function __construct($session_key,$script_default){
        $this->session_key = $session_key;
        $this->script_default = $script_default;
    }

    /**
     *
     */
    public function verifyRedirect(){
        if(isset($_GET['redirect']) && $_GET['redirect'] == 'true'){
            Denko::redirect($this->getCachedLinkBack($this->session_key,$this->script_default));
        }
    }

    /**
     * Setea el link para volver a la p�gina actual
     */
    public function setLinkBack(){
        $_SESSION[$this->session_key] = basename($_SERVER['REQUEST_URI']);
    }

    /**
     * Retorna el link a la p�gina, manteniendo los �ltimos par�metros GET
     */
    public function getCachedLinkBack(){
        return !empty($_SESSION[$this->session_key]) ? $_SESSION[$this->session_key] : $this->script_default;
    }

    /**
     *
     */
    public function getLinkBack(){
        return $this->script_default.'?redirect=true';
    }
}

/**
 *
 */
class Inny_LinkBackProductos extends Inny_LinkBack{

    /**
     *
     */
    public $id_listado;

    /**
     *
     */
    public function __construct($id_listado){
        parent::__construct('listado_'.$id_listado,'producto_listado.php');
        $this->id_listado = $id_listado;
    }

    /**
     *
     */
    public function getLinkBack(){
        return parent::getLinkBack().'&id_listado='.$this->id_listado;
    }
}

/**
 *
 */
class Inny_LinkBackContenidos extends Inny_LinkBack{
    public function __construct(){
        parent::__construct('contenidos','contenido_listado.php');
    }
}

/**
 *
 */
class Inny_LinkBackUsuariosRegistrados extends Inny_LinkBack{
    public function __construct(){
        parent::__construct('usuarios_registrados','usuario_registrado_listado.php');
    }
}

/**
 *
 */
class Inny_LinkBackSitios extends Inny_LinkBack{
    public function __construct(){
        parent::__construct('sitios','sitio_listado.php');
    }
}

/**
 *
 */
class Inny_LinkBackListados extends Inny_LinkBack{
    public function __construct(){
        parent::__construct('listados','listado_listado.php');
    }
}

/**
 *
 */
class Inny_LinkBackUsuarios extends Inny_LinkBack{
    public function __construct(){
        parent::__construct('usuarios','usuario_listado.php');
    }
}

/**
 *
 */
class Inny_LinkBackTareas extends Inny_LinkBack{
    public function __construct(){
        parent::__construct('tareas','tarea_listado.php');
    }
}

/**
 *
 */
class Inny_LinkBackEmpleados extends Inny_LinkBack{
    public function __construct(){
        parent::__construct('empleados','empleado_listado.php');
    }
}











/*

getLinkVolver
setlinkvolver

*/