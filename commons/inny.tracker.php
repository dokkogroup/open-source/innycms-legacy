<?php

define('USUARIO_ADMIN',1);
define('USUARIO_EMPLEADO',2);

class Inny_Tracker{

    /**
     *
     *
     */
    public static $cms_languages = array('es_ar' => 'espa�ol [es_AR]' , 'en_uk' => 'ingl�s [en_UK]');

    /**
     *
     */
    public static $scriptDefault = 'sitio_listado.php';

    /**
     *
     */
    public static $session_tipoUsuario = 'INNY_TRACKER_USERTYPE';

    /**
     *
     */
    public static $session_idEmpleado = 'INNY_TRACKER_ID_EMPLEADO';

    /**
     *
     */
    public static function setTipoUsuario($usertype){
        $_SESSION[self::$session_tipoUsuario] = $usertype;
    }

    /**
     *
     */
    public static function getTipoUsuario(){
        return !empty($_SESSION[self::$session_tipoUsuario]) ? $_SESSION[self::$session_tipoUsuario] : null;
    }

    /**
     *
     */
    public static function setIdEmpleadoLogueado($id_empleado){
        $_SESSION[self::$session_idEmpleado] = $id_empleado;
    }

    /**
     *
     */
    public static function getIdEmpleadoLogueado(){
        return !empty($_SESSION[self::$session_idEmpleado]) ? $_SESSION[self::$session_idEmpleado] : null;
    }

    /**
     *
     */
    public static function getDaoEmpleadoLogueado(){

        # Verifico si el DAO del empleado esta cacheado
        $cacheKey = 'INNY_TRACKER_DAO_EMPLEADO_LOGUEADO';
        if(!isset($GLOBALS[$cacheKey])){

            # Obtengo el ID del empleado logueado
            $id_empleado = self::getIdEmpleadoLogueado();

            # En caso que no haya empleado logueado, retorno NULL
            if(!$id_empleado){
                return null;
            }

            # Obtengo el DAO del empleado y lo cacheo
            $daoEmpleado = DB_DataObject::factory('empleado');
            $daoEmpleado->get($id_empleado);
            $GLOBALS[$cacheKey] = $daoEmpleado;
        }

        # Retorno el DAO en cach�
        return $GLOBALS[$cacheKey];
    }

    /**
     *
     */
    public static function verificarUsuarioLogueado(){
        if(!self::hayUsuarioLogueado()){
            self::logout();
        }
        return true;
    }

    /**
     *
     */
    public static function verificarAdminLogueado(){
        if(self::getTipoUsuario() != USUARIO_ADMIN){
            self::logout();
        }
        return true;
    }

    /**
     *
     */
    public static function verificarEmpleadoLogueado(){
        if(self::getTipoUsuario() != USUARIO_EMPLEADO){
            self::logout();
        }
        return true;
    }

    /**
     *
     */
    public static function hayUsuarioLogueado(){
        return !empty($_SESSION[self::$session_tipoUsuario]);
    }

    /**
     *
     */
    public static function logout(){
        Denko::redirect('index.php?logout');
    }

    /**
     *
     */
    public static function verificarCambiarPassword(&$smarty,$passOriginal=null){

        # Cargo los mensajes de error
        $smarty->config_load('speech.conf','error');
        $smarty->config_load('speech.conf','password');

        if($passOriginal){
            # Verifico que el campo 'Contrase�a' tenga valor seteado
            $password = !empty($_POST['password']) ? $_POST['password'] : '';
            if($password == ''){
                Denko::addErrorMessage('error_requiredField',array('%field' => 'password_label'));
            }

            # Verifico que la contrase�a original sea la correcta
            elseif($password != $passOriginal) {
                Denko::addErrorMessage('password_error');
            }
        }

        # Verifico que el campo 'Nueva Contrase�a' tenga valor seteado
        $newpassword = !empty($_POST['newpassword']) ? $_POST['newpassword'] : '';
        if($newpassword == ''){
            Denko::addErrorMessage('error_requiredField',array('%field' => 'password_newPassword'));
        }

        # Verifico que el campo 'Confirmar Nueva Contrase�a' tenga valor seteado
        $confirmnewpass = !empty($_POST['confirmnewpass']) ? $_POST['confirmnewpass'] : '';
        if($confirmnewpass == ''){
            Denko::addErrorMessage('error_requiredField',array('%field' => 'password_confirmNewPass'));
        }

        # Verifico que los campos 'Nueva Contrase�a' y 'Confirmar Nueva Contrase�a' sean iguales
        if($newpassword != $confirmnewpass){
            Denko::addErrorMessage('error_equalsFields',array('%field1' => 'password_newPassword' , '%field2' => 'password_confirmNewPass'));
        }
    }

    /**
     * Obtiene los lenguajes del CMS
     *
     * @static
     * @access public
     * @return array lenguajes del CMS
     */
    public static function getIdiomasCMS(){
        return self::$cms_languages;
    }

};
