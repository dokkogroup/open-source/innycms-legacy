<?php
/**
 * Project: Inny CMS
 * File: core.contenido.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */

/**
 * Clase para manipular los productos de los listados
 *
 * @package Inny
 */
class InnyCore_Contenido extends InnyCore_Elemento{



    ////////////////////////////////////////////////////////////////////////////
    //                                                                        //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Verifica que el valor de un contenido sea v�lido
     *
     * @static
     * @access public
     * @return boolean
     */
    protected static function _verificarValor(&$innyType,$valor){

        # Seteo el valor del contenido
        $innyType->setValue($valor);

        # Obtengo si es requerido
        $requerido = $innyType->getMetadataValue('parametros','required');

        # Verifico que el contenido no est� vac�o
        # Si no est� vac�o y no es v�lido, seteo el mensaje de error
        if(!$innyType->is_empty()){
            if(!$innyType->validate()){
                return self::addErrorMessage('input_error','inputError_invalidValue');
            }
        }

        # En caso que el valor del Contenido est� vac�o y sea requerido
        elseif($requerido == 'true'){
            return self::addErrorMessage('input_error','inputError_requiredValue');
        }

        # En caso que todo est� OK
        return true;
    }

    /**
     *
     *
     */
    public static function verificarContenido($contenido_nombre){

        # Obtengo el ID del sitio logueado actualmente
        $id_sitio = Inny::getIdSitioActual();

        # En caso que no exista sitio logueado
        if(empty($id_sitio)){
            return self::addErrorMessage('sitio','sitio_errorNoHaySitioActual');
        }

        # Verifico que el nombre del contenido sea v�lido
        $contenido_nombre = Denko::lower(Denko::trim($contenido_nombre));
        if(empty($contenido_nombre)){
            return self::addErrorMessage('contenido','contenido_errorNombreRequerido');
        }

        # Verifico que el contenido exista en la DB
        $contenido = new Contenido($contenido_nombre,$id_sitio);

        # Verifico que el contenido exista en la DB
        if(empty($contenido)){
            return self::addErrorMessage('contenido','contenido_errorNoExiste',array('%contenido_nombre' => $contenido_nombre));
        }

        # En caso que el contenido exista, lo retorno
        return $contenido;
    }

    /**
     * Edita los valores de un contenido
     *
     * @param string $contenido_nombre nombre del contenido
     * @param array $data arreglo con los datos para el contenido
     * @static
     * @access public
     *
     */
    public static function editarContenido($contenido_nombre,$data){

        # Verifico que el contenido exista en la DB
        $contenido = self::verificarContenido($contenido_nombre);
        if(empty($contenido)){
            return false;
        }

        if(isset($data['valor'])){

            # Obtengo el tipo de contenido
            $tipo = $contenido->getMetadataValue('tipo');

            # Creo el tipo de dato
            $innyType = $contenido->createInnyType();

            # Valor del contenido
            $contenido_valor = isset($data['valor']) ? $data['valor'] : null;

            //////////////////// CONTENIDO DE TIPO ARCHIVO /////////////////////

            # En caso que el tipo sea un archivo, verifico si es v�lido
            if(Inny::isSupportedFileType($tipo)){

                # Valido el archivo adjunto
                $error_code = $innyType->validate($contenido_valor);

                # En caso que haya error, cargo el mensaje correspondiente
                if($error_code != UPLOAD_ERR_OK){

                    # Speech del error
                    $errorSpeech = null;

                    # Obtengo si el valor del contenido es requerido
                    $requerido = $innyType->getMetadataValue('parametros','required');

                    switch($error_code){

                        # En caso que el archivo exceda el tama�o m�ximo permitido para el upload
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE: $errorSpeech = 'file_errorTooBigToServer'; break;

                        # En caso que no se haya podido uplodear el contenido completo del archivo
                        case UPLOAD_ERR_PARTIAL: $errorSpeech = 'file_errorOnlyPartUpload'; break;

                        # En caso que no se haya adjuntado archivo
                        # S�lo si el archivo es requerido asumo que es un error
                        case UPLOAD_ERR_NO_FILE:
                            if($requerido == 'true'){
                                $errorSpeech = 'file_errorNoFileToUpload';
                            }
                            break;

                        # En caso que el formato no sea v�lido, escojo el mensaje que mostrar
                        case UPLOAD_ERR_EXTENSION:
                            switch($tipo){

                                # En caso que sea imagen
                                case 'image': $errorSpeech = 'file_errorImageInvalidFormat'; break;

                                # En caso que sea imagen
                                case 'flash': $errorSpeech = 'file_errorFlashInvalidFormat'; break;

                                # En caso que sea imagen
                                case 'sound': $errorSpeech = 'file_errorSoundInvalidFormat'; break;

                                # En otro caso
                                default:      $errorSpeech = 'file_errorFileInvalidFormat'; break;
                            }
                            break;
                    }

                    # Agrego el mensaje de error
                    return self::addErrorMessage('file',$errorSpeech);
                }

                # En caso que todo est� OK, seteo el contenido
                if(!empty($contenido_valor['tmp_name'])){

                    # Elimino el archivo anterior
                    $contenido->deleteFile();

                    # Asigno el ID del archivo agregado al valor del contenido
                    $contenido->valor = $innyType->setFile($contenido_valor);
                }
            }

            ///////////////////// CONTENIDO DE TIPO TEXTO //////////////////////

            else{

                # Obtengo el sitio a donde pertenece el contenido
                $daoSitio = $contenido->getSitio();

                # En caso que el contenido sea multilenguaje
                if($contenido->esMultilenguaje()){

                    # Obtengo los lenguajes del sitio
                    $languages = $daoSitio->getMetadataValue('multilang','languages');

                    # Valido el valor del contenido por cada lenguaje
                    foreach($languages as $language_code => $language_label){
                        if(!self::_verificarValor($innyType,isset($contenido_valor[$language_code]) ? $contenido_valor[$language_code] : null)){
                            return false;
                        }
                    }

                    # En caso que los textos sean v�lidos
                    $texto_valor = array();
                    foreach($languages as $language_code => $language_label){

                        # Seteo el valor del texto
                        $innyType->setValue(isset($contenido_valor[$language_code]) ? $contenido_valor[$language_code] : null);

                        # Seteo el valor en el contenido
                        $texto_valor[$language_code] = $innyType->postdata2dbdata();
                    }

                    # Codifico el contenido del arreglo a UTF-8
                    Denko::arrayUtf8Encode($texto_valor);

                    # Guardo los valores de la configuraci�n en notaci�n JSON
                    $contenido->valor = json_encode($texto_valor);
                }

                # En caso que el contenido sea de un solo lenguaje
                else{

                    # Verifico que el valor sea v�lido
                    if(!self::_verificarValor($innyType,$contenido_valor)){
                        return false;
                    }

                    # En caso que sea v�lido
                    # Seteo el valor del texto
                    $innyType->setValue($contenido_valor);

                    # Seteo el valor en el contenido
                    $contenido->valor = $innyType->postdata2dbdata();
                }
            }
        }

        # Seteo el filtro del contenido
        $contenido_filtro = isset($data['filtro']) ? Denko::trim($data['filtro']) : '';
        if($contenido_filtro !== ''){
            $contenido->filtro = $contenido_filtro;
        }

        # Seteo se debe ser un contenido privado
        $contenido->indice2 = (isset($data['privado']) && is_bool($data['privado']) ? ($data['privado'] ? 1 : 0) : $contenido->indice2);

        # Actualizo el contenido. Si no se actualiza, agrego un mensaje de error
        return ($contenido->update() !== false) ? true : self::addErrorMessage('input_error','inputError_dbUpdate');
    }

    /**
     * Borra el valor de un contenido
     *
     * @param string $contenido_nombre nombre del contenido
     * @static
     * @access public
     * @return boolean si pudo eliminarse el valor del contenido
     */
    public static function borrarContenido($contenido_nombre){

        # Verifico que el contenido exista en la DB
        $contenido = self::verificarContenido($contenido_nombre);

        # Si existe, retorno el resultado de eliminar su valor
        return empty($contenido) ? false : ($contenido->deleteValue() !== false);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                          MENSAJES Y NOTIFICACIONES                     //
    ////////////////////////////////////////////////////////////////////////////

/*
    public static function initializeSpeech(){
        if(empty(self::$innySpeech)){
            self::$innySpeech = new InnyCore_Speech();
        }
    }


    public static function setDebugBacktrace($debug_backtrace){
        self::$debug_backtrace = $debug_backtrace;
    }


    public static function mostrarMensajesDeError(){
        self::$mostrar_mensajes_error = true;
        self::initializeSpeech();
    }


    public static function addErrorMessage($section,$message,$replaces=array()){

        # Inicializo el speech
        self::initializeSpeech();

        # Mensaje de error
        $error_message = self::$innySpeech->getSpeech('input_error','inputError').self::$innySpeech->getSpeech($section,$message,$replaces);

        # Logueo el error en la DB
        InnyModule_Reporter::core_error(E_USER_WARNING,$error_message,self::$debug_backtrace);

        # Agrego el error para mostrarlo en pantalla
        if(self::$mostrar_mensajes_error){
            Denko::addErrorMessage($error_message);
        }
        return false;
    }
*/
}

################################################################################