<?php
/**
 *
 *
 */
class InnyCore_Dao{

    /**
     * @var string prefijo para los DAOs en cache
     * @static
     */
    public static $cache_prefix = 'INNY_CACHE_DAO_';

    /**
     * @var string alias de NULL
     * @static
     */
    public static $null = '@NULL@';

    /**
     * Verifica si un numero es un ID de base de datos  v�lido
     *
     * @param mixed $id ID que verificar
     * @static
     * @access public
     * @return boolean
     */
    public static function isId($id){
        if(empty($id)){
            return false;
        }
        $id = trim($id);
        return (!empty($id) && is_numeric($id) && intval($id) == $id && $id > 0);
    }

    /**
     * Indica si un valor del DAO es vac�o o con contenido nulo
     *
     * @param mixed &$data valor de un campo del DAO
     * @static
     * @access public
     * @return boolean
     */
    public static function isEmpty(&$data){
        return ($data == null || $data == '' || $data == 'null');
    }

    /**
     * Obtiene un DAO, cacheando su resultado
     *
     * @param $table nombre de la tabla
     * @param $id id
     * @param $params par�metros extra
     * @static
     * @access public
     * @return DataObjects DAO
     */
    public static function getDao($table,$id,$params=array()){

        # Verifico que el nombre de la tabla sea v�lido
        $table = !empty($table) ? trim($table) : null;
        if(empty($table)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'el nombre de la tabla "'.$table.'" no es v�lido.');
            return null;
        }

        # Verifico que el ID sea v�lido
        $id = !empty($id) ? trim($id) : null;
        if(!self::isId($id)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'el ID "'.$id.'" no es v�lido.');
            return null;
        }

        # En caso que la info no est� cacheada, la cacheo
        $cachekey = self::$cache_prefix.strtoupper($table).'_'.$id;
        if(empty($GLOBALS[$cachekey])){

            # Obtengo la clave primaria
            $dao_pk = !empty($params['primary_key']) ? $params['primary_key'] : 'id_'.strtolower($table);

            # Verifico que el DAO exista
            $dao = DB_DataObject::factory($table);
            if(!empty($params['selectAdd'])){
                $dao->selectAdd();
                $dao->selectAdd($params['selectAdd']);
            }
            $dao->$dao_pk = $id;
            if(!$dao->find(true)){
                $GLOBALS[$cachekey] = self::$null;
                InnyModule_Reporter::core_error(E_USER_WARNING,'El DAO con ID "'.$id.'" de la tabla "'.$table.'" no existe.');
            }
            else{
                $GLOBALS[$cachekey] = $dao;
            }
        }

        # Retorno el DAO cacheado
        return (is_string($GLOBALS[$cachekey]) && $GLOBALS[$cachekey] == self::$null) ? null : $GLOBALS[$cachekey];
    }

    /**
     * Verifica que exista el Temporal con cierto ID
     *
     * @deprecated en favor de InnyCore_File::chequearTemporal()
     * @see InnyCore_File::chequearTemporal()
     * @param integer $id_archivo ID del Temporal
     * @static
     * @access public
     * @return boolean resultado de la verificaci�n
     */
    public static function chequearTemporal($id_archivo){
        return InnyCore_File::chequearTemporal($id_archivo);
    }

    /**
     * Mantiene en cache una variable relacionada al contenido
     *
     * @param array $path ruta del valor
     * @param mixed $valor valor que se desea setear
     * @access public
     * @return boolean si se pudo cachear el valor
     */
    public static function setValorEnCache($cache_clave,$path,$valor){

        # Armo el sub�ndice
        $index = '';
        foreach($path as $key){
            $index.= "['$key']";
        }

        # Seteo el valor en cach�
        eval('$GLOBALS[\''.$cache_clave.'\']'.$index.' = $valor;');
        return true;
    }

    /**
     * Obtiene un valor de la cache
     *
     * @param array $path ruta del valor
     * @access public
     * @return mixed valor cacheado, NULL en caso que no exista el valor en cache
     */
    public static function getValorEnCache($cache_clave,$path){

        # Armo el sub�ndice
        $index = '';
        foreach($path as $key){
            $index.= "['$key']";
        }

        # Obtengo el valor de la cache
        $value = null;
        eval('$value = isset($GLOBALS[\''.$cache_clave.'\']'.$index.') ? $GLOBALS[\''.$cache_clave.'\']'.$index.' : null;');
        return $value;
    }
}
################################################################################