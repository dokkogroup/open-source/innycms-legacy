<?php
/**
 * Project: Inny CMS
 * File: core.elemento.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */

/**
 * Clase para manipular elementos principales del Inny
 *
 * @package Inny
 */
class InnyCore_Elemento{

    /**
     * @var boolean indica si se agregan los mensajes de error
     * @static
     * @access public
     */
    public static $mostrar_mensajes_error = false;

    /**
     *
     *
     */
    public static $debug_backtrace = null;

    /**
     *
     *
     */
    public static $innySpeech = null;

    ////////////////////////////////////////////////////////////////////////////
    //                          MENSAJES Y NOTIFICACIONES                     //
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    public static function initializeSpeech(){
        if(empty(self::$innySpeech)){
            self::$innySpeech = new InnyCore_Speech();
        }
    }

    /**
     * Setea el debug_backtrace para el logueo de errores
     *
     * @param array $debug_backtrace debug_backtrace
     * @static
     * @access public
     * @return void
     */
    public static function setDebugBacktrace($debug_backtrace){
        self::$debug_backtrace = $debug_backtrace;
    }

    /**
     * Activa la opci�n de mostrar los mensajes de error cuando se agrega/edita un producto
     *
     * @static
     * @access public
     * @return void
     */
    public static function mostrarMensajesDeError(){
        self::$mostrar_mensajes_error = true;
        self::initializeSpeech();
    }

    /**
     * Agrega un mensaje de error para luego mostrarse por pantalla
     *
     * @param string $message mensaje de error
     * @param array $replaces remplazos de variables en el mensaje
     * @static
     * @access public
     * @return void
     */
    public static function addErrorMessage($section,$message,$replaces=array(),$prefix=null){

        # Inicializo el speech
        self::initializeSpeech();

        # Mensaje de error
        $error_message = ($prefix ? $prefix : self::$innySpeech->getSpeech('input_error','inputError')).self::$innySpeech->getSpeech($section,$message,$replaces);

        # Logueo el error en la DB
        InnyModule_Reporter::core_error(E_USER_WARNING,$error_message,self::$debug_backtrace);

        # Agrego el error para mostrarlo en pantalla
        if(self::$mostrar_mensajes_error){
            Denko::addErrorMessage($error_message);
        }
        return false;
    }
}

################################################################################