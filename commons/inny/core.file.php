<?php
/**
 * Project: Inny CMS
 * File: core.file.php
 * Purpose: Core para ABM de productos
 *
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 * @copyright 2007-2009 Dokko Group
 * @package core
 */

/**
 *
 *
 */
class InnyCore_File{

    /**
     *
     *
     */
    public static function getErrorArchivoAdjunto($filedata){

        switch($filedata['error']){

            # En caso que est� OK, verifico si realmente fu� subido el archivo
            case UPLOAD_ERR_OK: return ($filedata['error']['size'] == '0') ? UPLOAD_ERR_NO_FILE : UPLOAD_ERR_OK;

            # En caso que el archivo exceda el tama�o m�ximo permitido
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE: return UPLOAD_ERR_INI_SIZE;

            # En cualquier otro caso
            default: return $filedata['error'];
        }
    }

    /**
     * Obtiene un DAO Temporal dado su ID en la tabla
     *
     * @param integer $id_temporal id del archivo
     * @static
     * @access public
     * @return DataObjects_Temporal DAO Temporal
     */
    public static function getTemporal($id_temporal){
        return InnyCore_Dao::getDao('temporal',$id_temporal,array('selectAdd'=>'id_temporal,metadata,size,name'));
    }

    /**
     * Almacena un archivo en la DB
     *
     * @param array $filedata datos del archivo (�ndice de $_FILES)
     * @static
     * @access public
     * @return integer ID del archivo en la tabla Temporal
     */
    public static function almacenarArchivo($filedata){

        # Almaceno el archivo en la DB
        $id_archivo = DFM::setFromFile($filedata['tmp_name']);

        # Seteo la informaci�n del archivo en la DB
        $info = array(
            'name' => $filedata['name'],
            'metadata' => array('mime' => $filedata['type']),
            'size' => $filedata['size']
        );

        # Seteo la informaci�n del archivo en la tabla Temporal
        self::actualizarInfoArchivo($id_archivo,$info);

        # Retorno el ID del archivo en la tabla Temporal
        return $id_archivo;
    }

    /**
     *
     *
     * @static
     */
    public static function actualizarInfoArchivo($id_archivo,$info){

        # Verifico que el ID sea v�lido
        if(!InnyCore_Dao::isId($id_archivo)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El par�metro $id_archivo no es un ID de archivo v�lido',debug_backtrace());
            return false;
        }

        # Obtengo el Temporal correspondiente al ID del archivo
        $daoTemporal = self::getTemporal($id_archivo);
        if(empty($daoTemporal)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El Temporal con ID '.$id_archivo.' no existe en la DB',debug_backtrace());
            return false;
        }

        # Seteo el tama�o del archivo
        if(isset($info['size'])){
            $daoTemporal->size = $info['size'];
        }

        # Seteo el nombre del archivo
        if(isset($info['name'])){
            $daoTemporal->name = $info['name'];
        }

        # Seteo la metadata (merge)
        if(isset($info['metadata'])){
            if(!empty($daoTemporal->metadata)){
                $metadata = json_decode($daoTemporal->metadata,true);
                if(is_array($metadata)){
                    Denko::arrayUtf8Decode($metadata);
                    foreach($metadata as $key => $value){
                        if(!isset($info['metadata'][$key])){
                            $info['metadata'][$key] = $value;
                        }
                    }
                }
                unset($metadata);
            }
            Denko::arrayUtf8Encode($info['metadata']);
            $daoTemporal->metadata = json_encode($info['metadata']);
        }

        # Retorno el resultado del update del DAO
        return ($daoTemporal->update() !== false);
    }

    /**
     * Obtiene la informaci�n de archivo de la tabla temporal
     *
     * @param integer $id_temporal id del archivo
     * @static
     * @access public
     * @return array informaci�n del archivo ['metadata','size','name']
     */
    public static function getTemporalInfo($id_temporal){
        $cachekey = 'INNY_TEMPORAL_INFO_'.$id_temporal;
        if(empty($GLOBALS[$cachekey])){

            # Obtengo el archivo temporal
            $daoTemporal = self::getTemporal($id_temporal);

            # En caso que exista la informaci�n, la seteo
            if($daoTemporal !== null){

                # Obtengo la metadata y la convierto a arreglo
                $metadata = json_decode($daoTemporal->metadata,true);
                Denko::arrayUtf8Decode($metadata);

                # Seteo la informaci�n del archivo
                $GLOBALS[$cachekey] = array(
                    'metadata' => $metadata,
                    'size' => $daoTemporal->size,
                    'name' => $daoTemporal->name
                );
            }

            # Si no existe la informaci�n, lo indico en la cache
            else{
                $GLOBALS[$cachekey] = InnyCore_Dao::$null;
            }
        }

        # Retorno la informaci�n cacheada
        return ($GLOBALS[$cachekey] != InnyCore_Dao::$null) ? $GLOBALS[$cachekey] : null;
    }

    /**
     * Retorna los IDs de los archivos asociados con un Producto
     *
     * @param integer $id_producto ID del Temporal
     * @static
     * @return array ids de los archivos relacionados al producto
     */
    public static function getIdsArchivo($id_producto){

        # En caso que no sea un ID v�lido
        if(!InnyCore_Dao::isId($id_producto)){
            return null;
        }

        # Verifico si el arreglo est� en cach�
        $cache_key = 'INNY_IDS_ARCHIVOS_PRODUCTO_'.$id_producto;
        if(empty($GLOBALS[$cache_key])){
            $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
        	$daoProductoTemporal->selectAdd();
        	$daoProductoTemporal->selectAdd('id_temporal,posicion');
        	$daoProductoTemporal->id_producto = $id_producto;
        	$daoProductoTemporal->orderBy('posicion');
        	if($daoProductoTemporal->find()){
                $files = array();
            	while($daoProductoTemporal->fetch()){
            		$files[$daoProductoTemporal->posicion] = $daoProductoTemporal->id_temporal;
            	}
            	$GLOBALS[$cache_key] = $files;
            }
            else{
                $GLOBALS[$cache_key] = InnyCore_Dao::$null;
            }
        }

        # Retorno el valor en cache
        return $GLOBALS[$cache_key] == InnyCore_Dao::$null ? null : $GLOBALS[$cache_key];
    }

    /**
     * Verifica que exista el Temporal con cierto ID
     *
     * @param integer $id_archivo ID del Temporal
     * @static
     * @return boolean resultado de la verificaci�n
     */
    public static function chequearTemporal($id_archivo){
    	$daoTemporal = self::getTemporal($id_archivo);
    	if(!$daoTemporal){
            InnyModule_Reporter::core_error(E_USER_ERROR,'El archivo con ID '.$id_archivo.' no existe.');
            return false;
        }
        return true;
    }

}
