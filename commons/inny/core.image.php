<?php
/**
 *
 *
 */
class InnyCore_Image{

    /**
     *
     *
     */
    public static $allowedMimeTypes = array(
        'image/jpg'    // IE7 ?
        ,'image/jpeg'  // FF
        ,'image/pjpeg' // IE
        ,'image/gif'   // TODOS
        ,'image/png'   // TODOS
        
        );

    /**
     * Retorna los mime de imagenes soportados por el CMS
     *
     * @access public
     * @static
     * @return array
     * @todo cambiar nombre a getAllowedMimeTypes()
     */
    public static function getAllowedImageMime(){
        return self::$allowedMimeTypes;
    }

    /**
     * Retorna el mime de un archivo de imagen analizando su contenido, no su
     * extensi�n
     *
     * @param string $filepath ruta del archivo
     * @access public
     * @static
     * @return string|boolean
     * @todo cambiar nombre a getMimeTypeFromFileContent
     */
    public static function getImageMimeFromFileContent($filepath){

        # Intento abrir el archivo
        $resource = fopen($filepath,'rb');

        # En caso que el archivo no exista, retorno false
        if($resource === false){
            return false;
        }

        # Leo el header del archivo
        $headerFlag = fread($resource,10);

        # Verifico si el archivo es un JPEG [Exif: standar de c�maras]
        if(substr($headerFlag,6,4) == 'Exif'){
            return 'image/jpeg';
        }

        # Verifico si el archivo es un JPEG
        if(substr($headerFlag,6,4) == 'JFIF'){
            return 'image/jpeg';
        }

        # Verifico si el archivo es un gif
        if(substr($headerFlag,0,3) == 'GIF'){
            return 'image/gif';
        }
        # Verifico si el archivo es un png
            if(substr($headerFlag,1,3) == 'PNG'){
                return 'image/png';
            }

        return false;
    }

    /**
     * Dado un MIME, retorna la extensi�n que tiene que tener el archivo
     *
     * @param string $mime MIME
     * @static
     * @access public
     * @return string
     */
    public static function getFileExtensionByMime($mime){
        switch($mime){
            case 'image/jpg':
            case 'image/jpeg':
            case 'image/pjpeg': return 'jpg';
            case 'image/gif': return 'gif';
            case 'image/png': return 'png';
            default: return null;
        }
    }

    /**
     * Retorna los valores por defecto para los thumbs
     *
     * @param integer $width ancho de la imagen
     * @param integer $height alto de la imagen
     * @param integer $quality calidad de la imagen
     * @static
     * @access public
     * @return array
     */
    public static function getThumbDefaultValues($width=null,$height=null,$quality=null){

        # Verifico que los par�metros sean v�lidos
        $default = array();
        $default['width']  = (!empty($width) && Denko::isInt($width = trim($width))) ? $width : 'null';
        $default['height'] = (!empty($height) && Denko::isInt($height = trim($height))) ? $height : 'null';

        # En caso que no est�n seteada ninguna de las dimensiones
        if($width == null && $height == null){
            $default['width'] = '60';
        }

        # Seteo la calidad de la imagen
        $default['quality'] = (!empty($quality) && Denko::isInt($quality = trim($quality))) ? $quality : '92';

        # Retorno los valores por defecto
        return $default;
    }

}
