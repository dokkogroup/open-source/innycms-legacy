<?php
# Core
require_once '../commons/inny/core.dao.php';
require_once '../commons/inny/core.metadata.type.php';

# M�dulos
require_once '../commons/inny/module.multilang.php';
require_once '../commons/inny/module.cardinalidad.php';
require_once '../commons/inny/module.contador.php';


/**
 * Clase para parsear la metadata
 *
 * @package InnyCore
 */
class InnyCore_Metadata{

    /**
     * @var integer id del sitio
     * @static
     */
    public static $id_sitio = null;

    /**
     *
     *
     */
    public static $innySpeech = null;

    /**
     * @var array mensajes de alerta al parsear la metadata
     * @static
     */
    public static $warning_messages = array();

    /**
     * @var array mensajes de error al parsear la metadata
     * @static
     */
    public static $error_messages = array();

    /**
     * @var string prefijo de la seccion de contenido
     * @static
     */
    public static $prefix_contenido = 'contenido_';

    /**
     * @var string prefijo de la seccion de listado
     * @static
     */
    public static $prefix_listado = 'listado_';

    /**
     * @var string archivo que ejecuta el parseo de metadata
     * @static
     */
    public static $updateMetadataFile = 'update_metadata.php';

    /**
     * @var array valores por default para las configuraciones del sitio
     * @static
     */
    public static $defaultMetadataSitio = array(
        'TINY_MODE'        => '0',
        'ADMIN_CONFIGS'    => '0',
        'REGISTER_USERS'   => '0',
        'ADMIN_CATEGORIES' => '0'
    );

    /**
     * @var array valores por default para las configuraciones del DSE
     * @static
     */
    public static $defaultMetadataDSE = array(
        'enabled'          => '0',
        'field_mappings_1' => 'texto1',
        'field_mappings_2' => 'texto2',
        'field_mappings_3' => 'texto3'
    );

    /**
     * @var array valores por default para las configuraciones de un contenido
     * @static
     */
    public static $defaultMetadata_Contenido = array(

        # Tipo de dato
        'tipo' => 'richtext',

        # Par�metros
        'parametros' => array(
            'required'   => 'false',
            'private'    => 'false',
            'permission' => 'BMV'
        )
    );

    /**
     * @var array valores por default para las configuraciones de un listado
     * @static
     */
    public static $defaultMetadata_Listado = array(

        # Nombre del item
        'item' => 'elemento',

        # Par�metros
        'parametros' => array(
            'searchable' => 'false',
            'private'    => 'false',
            'order'      => 'asc',
            'limit'      => 'none',
            'permission' => 'ABMVO'
        ),

        # Seteos para los archivos
        'archivo' => array(
            'cantidad'   => 0,
            'nombre'     => 'archivo',
            'tipo'       => 'file',
            'parametros' => array(
                'description' => 'none'
            )
        ),

        # Seteos para los campos de texto
        'texto' => array(
            'cantidad'   => 3,
            'nombre'     => 'texto',
            'tipo'       => 'richtext',
            'prioridad'  => array(1,2,3),
            'parametros' => array(
                'unique' => 'no'
            )
        )
    );

    /**
     * @var nombre del htaccess con reglas base
     * @static
     * @access public
     */
    public static $htaccessBaseFilename = '.htaccess.base';
    public static $htaccessBaseCacheFilename = '.htaccess.base.cache';

    /**
     * @var nombre del htaccess con reglas locales
     * @static
     * @access public
     */
    public static $htaccessLocalFilename = '.htaccess.dev';

    /**
     *
     *
     */
    public static $postCallbacks = array();

    /**
     * Parsea la metadata del archivo de configuraciones y la setea en los contenidos y listados del sitio
     *
     * @param integer $id_sitio ID del sitio
     * @static
     * @access public
     * @return string
     */
    public static function parse($id_sitio){

        # Obtengo el sitio
        self::$id_sitio = $id_sitio;
        $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);

        # Obtengo el archivo de speechs
        $archivo_speechs = InnyCore_Speech::getNombreArchivoSpeechBaseSitio($daoSitio);

        # INIT
        self::$innySpeech = new InnyCore_Speech($archivo_speechs);

        # Verifico que el archivo exista
        $metadata_file = Inny::getConfigFilePath();
        if($metadata_file === null){
            $client_message_prefix = $log_message_prefix = self::$innySpeech->getSpeech('metadata','metadata_criticalError');
            self::_reportParamParseError(E_USER_ERROR,$client_message_prefix,$log_message_prefix,'metadata_errorMissingFile',array('%filename'=>Inny::$configPath));
            return '{"messages":{"errors" : '.json_encode(self::$error_messages).'}}';
        }

        # Verifico que el sitio exista
        $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);
        if($daoSitio == null){
            $client_message_prefix = $log_message_prefix = self::$innySpeech->getSpeech('metadata','metadata_criticalError');
            self::_reportParamParseError(E_USER_ERROR,$client_message_prefix,$log_message_prefix,'metadata_errorMissingIdWebSite',array('%id' => $id_sitio));
            return '{"messages":{"errors" : '.json_encode(self::$error_messages).'}}';
        }
         #Si todo est� OK, parseo el archivo que contiene la metadata, concatenando la metadata adicional del sitio.
        $metadata_string=file_get_contents($metadata_file);
        if (!empty($daoSitio->dynamic_metadata)){
            $metadata_string.=$daoSitio->dynamic_metadata;
        }
        $metadata_file_temp='templates_c/config.ini.temporal';
        @file_put_contents($metadata_file_temp, $metadata_string);
        $parsed_ini_file = parse_ini_file($metadata_file_temp,true);
        unlink($metadata_file_temp);  
              
        ////////////////////////////////////////////////////////////////////////
        //                        METADATA DEL SITIO                          //
        ////////////////////////////////////////////////////////////////////////

        # Genero la metadata para el sitio
        $daoSitio->unsetConfigs();
        foreach(self::$defaultMetadataSitio as $key => $value){

            # Obtengo el valor de la configuraci�n
            $trimmed_value = isset($parsed_ini_file[$key]) ? trim($parsed_ini_file[$key]) : '';

            # Si tiene seteado valor alguno, lo asigno
            if($trimmed_value !== ''){
                if($trimmed_value != '0' && $trimmed_value != '1'){
                    $message_prefix = str_replace('%config',$key ,self::$innySpeech->getSpeech('metadata','metadata_logWarningConfig'));
                    self::_reportParamParseError(E_USER_WARNING,$message_prefix,$message_prefix,'metadata_errorConfigInvalidValue',array('%values'=>'0|1'));
                }
                else{
                    $daoSitio->setConfig($key,$trimmed_value);
                }
            }
        }

        # Registro de usuarios
        if($daoSitio->getConfig('register_users') == '1' && !$daoSitio->crearListadoUsuariosRegistrados()){
            $message_prefix = str_replace('%config','REGISTER_USERS',self::$innySpeech->getSpeech('metadata','metadata_logWarningConfig'));
            self::_reportParamParseError(E_USER_WARNING,$message_prefix,$message_prefix,'metadata_errorCannotCreateUserList');
        }

        ////////////////////////////////////////////////////////////////////////
        //                         METADATA DEL DSE                           //
        ////////////////////////////////////////////////////////////////////////

        if(array_key_exists('dse',$parsed_ini_file)){

            $metadata_dse = array();

            # Seteo si el DSE estar� habilitado
            if(isset($parsed_ini_file['dse']['enabled']) && ($dse_enabled = trim($parsed_ini_file['dse']['enabled'])) !== ''){
                if($dse_enabled != '0' && $dse_enabled != '1'){
                    $client_message_prefix = str_replace('%section','[dse][enabled]',self::$innySpeech->getSpeech('metadata','metadata_sectionPrefix'));
                    $log_message_prefix = str_replace('%section','[dse][enabled]',self::$innySpeech->getSpeech('metadata','metadata_logWarning'));
                    self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_errorConfigInvalidValue',array('%values'=>'0|1'));
                }
                else{
                    $metadata_dse['enabled'] = $dse_enabled;
                }
            }

            # Seteo los valores para los so_fields
            for($i = 0; $i < INNY_DSE_SO_FIELDS; $i++){
                if(isset($parsed_ini_file['dse']['so_field'.$i.'_values']) && ($so_field_metadata = trim($parsed_ini_file['dse']['so_field'.$i.'_values'])) !== ''){

                    $metadata_dse['so_field'.$i] = array();

                    # Seteo los values del so_field
                    $metadata_dse['so_field'.$i]['values'] = self::params2array($so_field_metadata,'[dse][so_field'.$i.'_values]');

					# Seteo la ayuda del so_field si esta definida
					if (!empty($parsed_ini_file['dse']['so_field'.$i.'_ayuda'])){
						$metadata_dse['so_field'.$i]['ayuda'] = $parsed_ini_file['dse']['so_field'.$i.'_ayuda'];
					}

                    # Seteo el nombre del so_field
                    if(isset($parsed_ini_file['dse']['so_field'.$i.'_name']) && ($so_field_name = trim($parsed_ini_file['dse']['so_field'.$i.'_name'])) !== ''){
                        $metadata_dse['so_field'.$i]['name'] = $so_field_name;
                    }
                }
            }

            # Seteo cuales son los campos que se mapean
            for($i = 0; $i < INNY_DSE_FIELD_MAPPINGS; $i++){
                if(isset($parsed_ini_file['dse']['field_mappings_'.$i]) && ($field_mappings = trim($parsed_ini_file['dse']['field_mappings_'.$i])) !== ''){
                    $metadata_dse['field_mappings_'.$i] = $field_mappings;
                }
            }

            # Seteo la metadata del DSE
            $daoSitio->setConfig('DSE',$metadata_dse);
        }

        # METADATA PARA EL LENGUAJE
        InnyModule_Multilang::parseMetadata($daoSitio,$parsed_ini_file);

        # Seteo la versi�n del InnyCMS
        $daoSitio->version = Inny::$version;

        # Actualizo el DAO Sitio
        $daoSitio->update();

        ////////////////////////////////////////////////////////////////////////
        //                                                                    //
        ////////////////////////////////////////////////////////////////////////

        # Obtengo la longitud de los prefijos de secci�n
        $strlen_contenido = strlen(self::$prefix_contenido);
        $strlen_listado = strlen(self::$prefix_listado);

        #
        foreach($parsed_ini_file as $section_name => $section_values){

            ////////////////////////////////////////////////////////////////////
            //                PARSEO DE METADATA DE CONTENIDOS                //
            ////////////////////////////////////////////////////////////////////
            if(substr($section_name,0,$strlen_contenido) === self::$prefix_contenido){

                # Arreglo donde se va agregando la metadata parseada
                $contenido_metadata = array();

                # Verifico el tipo de dato del contenido
                if(isset($section_values['tipo'])){
                    self::_verificarParametro_Tipo($contenido_metadata,$section_values,$section_name,'tipo');
                }
				
                # Verifico el nombre del contenido
                if(isset($section_values['nombre'])){
                    self::_verificarParametro_Texto($contenido_metadata,$section_values,$section_name,'nombre');
                }

                # Verifico los par�metros del contenido
                if(isset($section_values['parametros'])){
                    self::_verificarParametro_Parametros($contenido_metadata,$section_values,$section_name,'parametros');
                }

                # Verifico si tiene ayuda el contenido
                if(isset($section_values['ayuda'])){
                    self::_verificarParametro_Ayuda($contenido_metadata,$section_values,$section_name,'ayuda');
                }
              
                # Grabo la metadata del contenido
                # Si el contenido no existe, lo creo
                Denko::arrayUtf8Encode($contenido_metadata);
                $contenido_label = InnyCore_Utils::lower(trim(substr($section_name,$strlen_contenido)));

                $daoConfiguracion = DB_DataObject::factory('configuracion');
                $daoConfiguracion->whereAdd('
                    nombre = \''.$contenido_label.'\'
                    and indice1 = '.$id_sitio.'
                ');

                # Datos de la metadata
                $contenido_jsonedMetadata = json_encode($contenido_metadata);
                $contenido_descripcion = !empty($contenido_metadata['nombre']) ? utf8_decode($contenido_metadata['nombre']) : $contenido_label;
                $contenido_adminOnly = isset($contenido_metadata['parametros']['private']) && $contenido_metadata['parametros']['private'] == 'true' ? 1 : 0;

                # En caso que el contenido ya exista en la DB, lo actualizo
                if($daoConfiguracion->find(true)){
                    $daoConfiguracion->metadata = $contenido_jsonedMetadata;
                    $daoConfiguracion->descripcion = $contenido_descripcion;
                    $daoConfiguracion->indice2 = $contenido_adminOnly;
					$daoConfiguracion->estado = 1;

                    # Actualizo el DAO en la DB
                    $daoConfiguracion->update();
                }

                # En caso que el contenido no exista, lo creo
                else{
                    $daoConfiguracion = DB_DataObject::factory('configuracion');
                    $daoConfiguracion->nombre = $contenido_label;
                    $daoConfiguracion->valor = 'null';
                    $daoConfiguracion->estado = 1;
                    $daoConfiguracion->indice1 = $id_sitio;

                    # A todo tipo de contenido se le setea que es del tipo 'configuracion_string'
                    # Motivo: as� se evitan validaciones innecesarias que puedan generar conflictos
                    $daoConfiguracion->tipo = configuracion_string;

                    $daoConfiguracion->metadata = $contenido_jsonedMetadata;
                    $daoConfiguracion->descripcion = $contenido_descripcion;
                    $daoConfiguracion->indice2 = $contenido_adminOnly;

                    # Inserto el DAO en la DB
                    $daoConfiguracion->insert();
                }
            }

            ////////////////////////////////////////////////////////////////////
            //                 PARSEO DE METADATA DE LISTADOS                 //
            ////////////////////////////////////////////////////////////////////
            elseif(substr($section_name,0,$strlen_listado) === self::$prefix_listado){

                # Arreglo donde se va agregando la metadata parseada
                $metadata_listado = array();

                # Verifico que el parametro "item" est� seteado
                if(isset($section_values['item'])){
                    self::_verificarParametro_Texto($metadata_listado,$section_values,$section_name,'item');
                }

                # Verifico que el parametro "nombre" est� seteado
                if(isset($section_values['nombre'])){
                    self::_verificarParametro_Texto($metadata_listado,$section_values,$section_name,'nombre');
                }

                # Verifico los par�metros del listado
                if(isset($section_values['parametros'])){
                    self::_verificarParametro_Parametros($metadata_listado,$section_values,$section_name,'parametros');
                }

                # Verifico si tiene ayuda el listado
                if(isset($section_values['ayuda'])){
                    self::_verificarParametro_Ayuda($metadata_listado,$section_values,$section_name,'ayuda');
                }
                # =============== METADATA PARA LOS ARCHIVOS ================= #

                # Cantidad de archivos
                if(isset($section_values['archivo_cantidad'])){
                    self::_verificarParametro_Cantidad($metadata_listado['archivo'],$section_values,$section_name,'archivo_cantidad','cantidad');
                }

                # Nombre de los archivos por defecto
                if(isset($section_values['archivo_nombre'])){
                    self::_verificarParametro_Texto($metadata_listado['archivo'],$section_values,$section_name,'archivo_nombre','nombre');
                }

                # Tipo de archivo por defecto
                if(isset($section_values['archivo_tipo'])){
                    self::_verificarParametro_Tipo($metadata_listado['archivo'],$section_values,$section_name,'archivo_tipo','file','tipo');
                }

                # Par�metros de los archivos por defecto
                if(isset($section_values['archivo_parametros'])){
                    self::_verificarParametro_Parametros($metadata_listado['archivo'],$section_values,$section_name,'archivo_parametros','parametros');
                }
                
                # Ayuda del campo texto
                if(isset($section_values['archivo_ayuda'])){
                    self::_verificarParametro_Ayuda($metadata_listado['archivo'],$section_values,$section_name,'archivo_ayuda',null,'ayuda');
                }  

                # Verifico los datos para cada archivo
                $archivo_cantidad = isset($metadata_listado['archivo']['cantidad']) ? $metadata_listado['archivo']['cantidad'] : self::$defaultMetadata_Listado['archivo']['cantidad'];
                for($i = 1; $i <= $archivo_cantidad; $i++){

                    # Nombre del archivo
                    if(isset($section_values['archivo_'.$i.'_nombre'])){
                        self::_verificarParametro_Texto($metadata_listado['archivo']['archivo'.$i],$section_values,$section_name,'archivo_'.$i.'_nombre','nombre');
                    }

                    # Tipo del archivo
                    if(isset($section_values['archivo_'.$i.'_tipo'])){
                        self::_verificarParametro_Tipo($metadata_listado['archivo']['archivo'.$i],$section_values,$section_name,'archivo_'.$i.'_tipo','file','tipo');
                    }

                    # Par�metros del archivo
                    if(isset($section_values['archivo_'.$i.'_parametros'])){
                        self::_verificarParametro_Parametros($metadata_listado['archivo']['archivo'.$i],$section_values,$section_name,'archivo_'.$i.'_parametros','parametros');
                    }
                    
	                # Ayuda del archivo
	                if(isset($section_values['archivo_'.$i.'_ayuda'])){
	                    self::_verificarParametro_Ayuda($metadata_listado['archivo']['archivo'.$i],$section_values,$section_name,'archivo_'.$i.'_ayuda',null,'ayuda');
	                }                    
                }

                # ================ METADATA PARA LOS TEXTOS ================== #

                # Cantidad de texto
                if(isset($section_values['texto_cantidad'])){
                    self::_verificarParametro_Cantidad($metadata_listado['texto'],$section_values,$section_name,'texto_cantidad','cantidad');
                }

                # Obtengo la cantidad de campos de texto que se adjuntar�n
                $texto_cantidad = isset($metadata_listado['texto']['cantidad']) ? $metadata_listado['texto']['cantidad'] : self::$defaultMetadata_Listado['texto']['cantidad'];

                # Prioridad de muestra de campos de texto
                if(isset($section_values['texto_prioridad'])){
                    self::_verificarParametro_Prioridad($metadata_listado['texto'],$section_values,$section_name,'texto_prioridad',$texto_cantidad,'prioridad');
                }
                # Orden de los campos de texto
                if(isset($section_values['texto_orden'])){
                    self::_verificarParametro_Orden($metadata_listado['texto'],$section_values,$section_name,'texto_orden',$texto_cantidad,'orden');
                }
                # Nombre de los archivos por defecto
                if(isset($section_values['texto_nombre'])){
                    self::_verificarParametro_Texto($metadata_listado['texto'],$section_values,$section_name,'texto_nombre','nombre');
                }

                # Tipo de archivo por defecto
                if(isset($section_values['texto_tipo'])){
                    self::_verificarParametro_Tipo($metadata_listado['texto'],$section_values,$section_name,'texto_tipo','text','tipo');
                }

                # Par�metros de los archivos por defecto
                if(isset($section_values['texto_parametros'])){
                    self::_verificarParametro_Parametros($metadata_listado['texto'],$section_values,$section_name,'texto_parametros','parametros');
                }
                
                # Ayuda del campo texto
                if(isset($section_values['texto_ayuda'])){
                    self::_verificarParametro_Ayuda($metadata_listado['texto'],$section_values,$section_name,'texto_ayuda',null,'ayuda');
                }   
                
                # Verifico los datos para cada archivo
                for($i = 1; $i <= $texto_cantidad; $i++){

                    # Nombre del campo de texto
                    if(isset($section_values['texto_'.$i.'_nombre'])){
                        self::_verificarParametro_Texto($metadata_listado['texto']['texto'.$i],$section_values,$section_name,'texto_'.$i.'_nombre','nombre');
                    }

                    # Tipo del campo de texto
                    if(isset($section_values['texto_'.$i.'_tipo'])){
                        self::_verificarParametro_Tipo($metadata_listado['texto']['texto'.$i],$section_values,$section_name,'texto_'.$i.'_tipo','text','tipo');
                    }

                    # Par�metros del campo de texto
                    if(isset($section_values['texto_'.$i.'_parametros'])){
                        self::_verificarParametro_Parametros($metadata_listado['texto']['texto'.$i],$section_values,$section_name,'texto_'.$i.'_parametros','parametros');
                    }
                    
	                # Ayuda del campo texto
	                if(isset($section_values['texto_'.$i.'_ayuda'])){
	                    self::_verificarParametro_Ayuda($metadata_listado['texto']['texto'.$i],$section_values,$section_name,'texto_'.$i.'_ayuda',null,'ayuda');
	                }                           
                }

                # METADATA PARA LAS CARDINALIDADES
                InnyModule_Cardinal::parseMetadata($section_name,$section_values,$metadata_listado);

                # METADATA PARA LOS CONTADORES
                InnyModule_Contador::parseMetadata($section_name,$section_values,$metadata_listado);

                # =========== GUARDAR METADATA EN EL DAO LISTADO ============= #

                # Convierto el arreglo a JSON
                Denko::arrayUtf8Encode($metadata_listado);
                $jsonEncoded_metadata = json_encode($metadata_listado);

                # Seteo la metadata del listado
                # Si el listado no existe, lo creo
                $listado_label = InnyCore_Utils::lower(trim(substr($section_name,$strlen_listado)));
                $listado_filtro = isset($metadata_listado['parametros']['filter']) ? utf8_decode($metadata_listado['parametros']['filter']) : 'null';
                $listado_adminOnly = isset($metadata_listado['parametros']['private']) && $metadata_listado['parametros']['private'] == 'true' ? '1' : '0';

                # Busco el listado al cual actualizarle la metadata
                $daoListado = DB_DataObject::factory('listado');
                $daoListado->whereAdd('
                    nombre = \''.$listado_label.'\'
                    and id_sitio = '.$id_sitio
                );

                # En caso que el listado exista en la DB, actualizo la metadata
                if($daoListado->find(true)){
                    $daoListado->metadata = $jsonEncoded_metadata;
                    $daoListado->filtro = $listado_filtro;
                    $daoListado->admin_only = $listado_adminOnly;
                    $daoListado->update();
                }

                # Sin�, agrego el listado
                else{
                    $daoListado->nombre = $listado_label;
                    $daoListado->id_sitio = $id_sitio;
                    $daoListado->metadata = $jsonEncoded_metadata;
                    $daoListado->filtro = $listado_filtro;
                    $daoListado->admin_only = $listado_adminOnly;
                    $daoListado->insert();
                }
            }
        }

        # Ejecuto los callbacks posteriores a la generaci�n de metadata
        if(count(self::$postCallbacks) > 0){
            foreach(self::$postCallbacks as $callback){
                //echo $callback;

                eval($callback);
            }
        }

        # Junto los htaccess
        self::updateHtaccess();

        # Convierto los mensajes del parseo
        if(count(self::$error_messages) > 0 || count(self::$warning_messages) > 0){
            $messages = array();
            $messages['messages'] = array();
            if(count(self::$error_messages) > 0){
                $messages['messages']['errors'] =  self::$error_messages;
            }
            if(count(self::$warning_messages) > 0){
                $messages['messages']['warnings'] =  self::$warning_messages;
            }
            return json_encode($messages);
        }

        # En caso que no haya errores
        return '{"messages" : "none"}';
    }

    /**
     * Loguea un mensaje de error referente al parseo de metadata
     *
     * @static
     * @access protected
     * @return void
     */
    protected static function _reportParamParseError($error_type,$client_message_prefix,$log_message_prefix,$config_vars,$replaces=array()){
        $message = self::$innySpeech->getSpeech('metadata',$config_vars);
        if($replaces !== null && count($replaces) > 0){
            foreach($replaces as $key => $value){
                $message = str_replace($key,$value,$message);
            }
        }

        # Agrego el mensaje de alerta/error
        if($error_type == E_USER_WARNING){
            self::$warning_messages[] = utf8_encode($client_message_prefix.' '.$message);
        }
        else{
            self::$error_messages[] = utf8_encode($client_message_prefix.' '.$message);
        }

        # Logueo el mensaje
        DokkoLogger::log($error_type,$log_message_prefix.' '.$message,null,null,self::$id_sitio);
    }

    /**
     * Reporta un error en el valor de la metadata
     *
     * @static
     * @access public
     * @return boolean
     */
    public static function reportParamValueError($error_type,$section,$config_vars,$replaces=array()){
        self::_reportParamParseError(
            $error_type,
            str_replace('%section',$section,self::$innySpeech->getSpeech('metadata','metadata_sectionPrefix')),
            str_replace('%section',$section,self::$innySpeech->getSpeech('metadata','metadata_logWarning')),
            $config_vars,
            $replaces
        );
    }

    /**
     * Verifica y parsea una configuraci�n cuyo valor es texto
     *
     * @static
     * @access protected
     * @return boolean
     */
    protected static function _verificarParametro_Texto(&$parsed_metadata,&$ini_section_values,$ini_section_name,$ini_param_name,$parsed_metadata_key = null){

        # Verifico que tenga seteado valor alguno
        if(($texto = trim($ini_section_values[$ini_param_name])) === ''){
            self::reportParamValueError(E_USER_WARNING,'['.$ini_section_name.']','metadata_isEmpty',array('%param' => $ini_param_name));
        }

        # En caso que el valor del par�metro no est� vac�o, lo agrego en la metadata
        else{
            if($parsed_metadata_key != null){
                $parsed_metadata[$parsed_metadata_key] = $texto;
            }
            else{
                $parsed_metadata[$ini_param_name] = $texto;
            }
            return true;
        }

        # En caso que el dato no se ingrese en la metadata, retorno FALSE
        return false;
    }

    /**
     * Verifica y parsea una cantidad de campos
     *
     * @static
     * @access protected
     * @return boolean
     */
    protected static function _verificarParametro_Cantidad(&$parsed_metadata,&$ini_section_values,$ini_section_name,$ini_param_name,$parsed_metadata_key = null){

        $section = '['.$ini_section_name.']';

        # Verifico que tenga seteado valor alguno
        if(($cantidad = trim($ini_section_values[$ini_param_name])) === ''){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_isEmpty',array('%param' => $ini_param_name));
        }

        # Verifico que sea un n�mero entero v�lido
        elseif(!InnyCore_Utils::is_integer($cantidad)){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_integerFormatError',array('%param' => $ini_param_name));
        }

        # Verifico que sea mayor igual a cero
        elseif($cantidad < 0){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_integerPositiveError',array('%param' => $ini_param_name));
        }

        # En caso que todo est� ok, lo agrego a la metadata
        else{
            if($parsed_metadata_key != null){
                $parsed_metadata[$parsed_metadata_key] = $cantidad;
            }
            else{
                $parsed_metadata[$ini_param_name] = $cantidad;
            }
            return true;
        }

        # En caso que el dato no se ingrese en la metadata, retorno FALSE
        return false;
    }

    /**
     * Verifica y parsea un tipo de dato
     *
     * @static
     * @access protected
     * @return boolean
     */
    protected static function _verificarParametro_Tipo(&$parsed_metadata,&$ini_section_values,$ini_section_name,$ini_param_name,$datatype = null,$parsed_metadata_key = null){
        $section = '['.$ini_section_name.']';

        # Verifico que tenga seteado valor alguno
        if(($tipo = trim($ini_section_values[$ini_param_name])) === ''){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_isEmpty',array('%param' => $ini_param_name));
        }

        # Verifico que sea un tipo v�lido
        elseif(!(empty($datatype) ? Inny::isSupportedType($tipo) : ($datatype == 'file' ? Inny::isSupportedFileType($tipo) : Inny::isSupportedDataType($tipo)))){
            self::reportParamValueError(E_USER_WARNING,$section,$datatype == 'file' ? 'metadata_fileTypeError' : 'metadata_textTypeError',array('%type' => $tipo));
        }

        # En caso que todo est� ok, lo agrego a la metadata
        else{
            if($parsed_metadata_key != null){
                $parsed_metadata[$parsed_metadata_key] = $tipo;
            }
            else{
                $parsed_metadata[$ini_param_name] = $tipo;
            }
            return true;
        }

        # En caso que el dato no se ingrese en la metadata, retorno FALSE
        return false;
    }

    /**
     * Verifica y parsea los par�metros de un listado y/o contenido
     *
     * @static
     * @access protected
     * @return boolean
     */
    protected static function _verificarParametro_Parametros(&$parsed_metadata,&$ini_section_values,$ini_section_name,$ini_param_name,$parsed_metadata_key = null){

        $section = '['.$ini_section_name.']['.$ini_param_name.']';
		
		# Verifico que tenga seteado valor alguno
        if(($unparsed_parametros = trim($ini_section_values[$ini_param_name])) === ''){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_isEmpty',array('%param' => $ini_param_name));
        }

        # Verifico que los par�metros tengan formato v�lido
        else{

            # Parseo los subpar�metros
            $parametros = self::params2array($unparsed_parametros,$section);
            
            # En caso que todo est� ok, agrego los subpar�metros a la metadata
            if(count($parametros) > 0){
                if($parsed_metadata_key !== null){
                    $parsed_metadata[$parsed_metadata_key] = $parametros;
                }
                else{
                    $parsed_metadata[$ini_param_name] = $parametros;
                }
                return true;
            }
        }

        # En caso que el dato no se ingrese en la metadata, retorno FALSE
        return false;
    }

    /**
     * Verifica y parsea los par�metros de un listado y/o contenido
     *
     * @static
     * @access protected
     * @return boolean
     */
    protected static function _verificarParametro_Ayuda(&$parsed_metadata,&$ini_section_values,$ini_section_name,$ini_param_name,$datatype = null,$parsed_metadata_key = null){
        $section = '['.$ini_section_name.']';

        # Verifico que tenga seteado valor alguno
        if(($ayuda = trim($ini_section_values[$ini_param_name])) === ''){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_isEmpty',array('%param' => $ini_param_name));
        }

        # En caso que todo est� ok, lo agrego a la metadata
        else{
            if($parsed_metadata_key != null){
                $parsed_metadata[$parsed_metadata_key] = $ayuda;
            }
            else{
                $parsed_metadata[$ini_param_name] = $ayuda;
            }
            return true;
        }
		
        # En caso que el dato no se ingrese en la metadata, retorno FALSE
        return false;
    }

    /**
     * Verifica y parsea los subvalores de la prioridad de campos
     *
     * @static
     * @access protected
     * @return boolean
     */
    protected static function _verificarParametro_Prioridad(&$parsed_metadata,&$ini_section_values,$ini_section_name,$ini_param_name,$cant_textos,$parsed_metadata_key = null){

        $section = '['.$ini_section_name.']['.$ini_param_name.']';

        # Verifico que tenga seteado valor alguno
        if(($unparsed_prioridad = trim($ini_section_values[$ini_param_name])) === ''){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_isEmpty',array('%param' => $ini_param_name));
        }

        # En caso que la configuraci�n tenga seteado valor alguno
        else{

            # Arreglo donde voy almacenando los valores de prioridad v�lidos
            $nros_validos = array();

            # Verifico si se indica que no deben seleccionarse par�metros
            if(strtolower($unparsed_prioridad) == 'none'){
                $nros_validos = 'none';
            }

            # En caso que se hayan seleccionado campos de texto, los verifico uno a uno
            else{
                # Separo los numeros de prioridad
                $prioridades = explode(',',$unparsed_prioridad);

                # Verifico que todos tengan valor alguno
                foreach($prioridades as $prioridad){

                    # Obtengo el n�mero de la prioridad
                    $nro_prioridad = trim($prioridad);

                    # Verifico que el n�mero no sea vac�o
                    if($nro_prioridad == ''){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_requiredSubvalue');
                    }

                    # Verifico que sea un n�mero entero v�lido
                    elseif(!InnyCore_Utils::is_integer($nro_prioridad)){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_invalidIntegerSubvalue');
                    }

                    # Verifico que sea mayor a cero
                    elseif($nro_prioridad <= 0){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_equalOrLessThanZeroSubvalue');
                    }

                    # Verifico que el n�mero de prioridad no exceda la cantidad de textos
                    elseif($nro_prioridad > $cant_textos){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_maximumFieldNumberExceeded');
                    }

                    # Verifico que el n�mero no exista previamente en los seleccionados
                    elseif(in_array($nro_prioridad,$nros_validos)){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_previouslySelected');
                    }

                    # Si es v�lido, lo agrego al conjunto de prioridades
                    else{
                        $nros_validos[] = $nro_prioridad;
                    }
                }
            }

            # En caso que todo est� ok, agrego los subpar�metros a la metadata
            if(count($nros_validos) > 0){
                if($parsed_metadata_key !== null){
                    $parsed_metadata[$parsed_metadata_key] = $nros_validos;
                }
                else{
                    $parsed_metadata[$ini_param_name] = $nros_validos;
                }
                return true;
            }
        }

        # En caso que el dato no se ingrese en la metadata, retorno FALSE
        return false;
    }

/**
     * Verifica y parsea los subvalores del orden de campos
     *
     * @static
     * @access protected
     * @return boolean
     */
    protected static function _verificarParametro_Orden(&$parsed_metadata,&$ini_section_values,$ini_section_name,$ini_param_name,$cant_textos,$parsed_metadata_key = null){

        $section = '['.$ini_section_name.']['.$ini_param_name.']';

        # Verifico que tenga seteado valor alguno
        if(($unparsed_orden = trim($ini_section_values[$ini_param_name])) === ''){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_isEmpty',array('%param' => $ini_param_name));
        }

        # En caso que la configuraci�n tenga seteado valor alguno
        else{

            # Arreglo donde voy almacenando los valores de orden v�lidos
            $nros_validos = array();

            # Verifico si se indica que no deben seleccionarse par�metros
            if(strtolower($unparsed_orden) == 'none'){
                $nros_validos = 'none';
            }

            # En caso que se hayan seleccionado campos de texto, los verifico uno a uno
            else{
                # Separo los numeros de prioridad
                $ordenes = explode(',',$unparsed_orden);

                # Verifico que todos tengan valor alguno
                foreach($ordenes as $orden){

                    # Obtengo el n�mero de orden
                    $nro_orden = trim($orden);

                    # Verifico que el n�mero no sea vac�o
                    if($nro_orden == ''){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_requiredSubvalue');
                    }

                    # Verifico que sea un n�mero entero v�lido
                    elseif(!InnyCore_Utils::is_integer($nro_orden)){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_invalidIntegerSubvalue');
                    }

                    # Verifico que sea mayor a cero
                    elseif($nro_orden <= 0){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_equalOrLessThanZeroSubvalue');
                    }

                    # Verifico que el n�mero de orden no exceda la cantidad de textos
                    elseif($nro_orden > $cant_textos){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_maximumFieldNumberExceeded');
                    }

                    # Verifico que el n�mero no exista previamente en los seleccionados
                    elseif(in_array($nro_orden,$nros_validos)){
                        self::reportParamValueError(E_USER_WARNING,$section,'metadata_previouslySelected');
                    }
    
                    # Si es v�lido, lo agrego al conjunto de ordenes
                    else{
                        $nros_validos[] = $nro_orden;
                    }
                }
            }

            # En caso que todo est� ok, agrego los subpar�metros a la metadata verifificando que 
            # la cantidad de nros de orden sea igual a la cantidad de textos.
            if(count($nros_validos) > 0 && count($nros_validos)==$cant_textos){
                if($parsed_metadata_key !== null){
                    $parsed_metadata[$parsed_metadata_key] = $nros_validos;
                }
                else{
                    $parsed_metadata[$ini_param_name] = $nros_validos;
                }
                return true;
            }else{
                self::reportParamValueError(E_USER_WARNING,$section,'metadata_numberOfFields');
            }
        }

        # En caso que el dato no se ingrese en la metadata, retorno FALSE
        return false;
    }
    /**
     * Parsea una string de subpar�metros
     *
     * @param string $metadata metadata que parsear
     * @param string &$warnings arreglo de mensajes de alerta
     * @param string $error_message_prefix prefijo que tendr�n los mensajes de alerta
     * @static
     * @access public
     * @return array
     */
    public static function params2array($metadata,$section){

        $parsed = array();

        # Mensajes de alerta
        $client_message_prefix = str_replace('%section',$section,self::$innySpeech->getSpeech('metadata','metadata_sectionPrefix'));
        $log_message_prefix = str_replace('%section',$section,self::$innySpeech->getSpeech('metadata','metadata_logWarning'));

        #
        $settings = explode('|',$metadata);
        foreach($settings as $setting){
            $sep_pos = strpos($setting,':');
            if($sep_pos === false){
                self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredNameValue');
                
                continue;
            }
            # Obtengo la clave
            $param_name = substr($setting,0,$sep_pos);
            if($param_name === false || ($param_name = trim($param_name)) === ''){
                self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredNameValue');
                continue;
            }

            # Obtengo el valor
            $param_value = substr($setting,$sep_pos+1);
            if($param_value === false || ($param_value = trim($param_value)) === ''){
                self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredParamValue',array('%param'=>$param_name));
                continue;
            }
            $strlen_value = strlen($param_value);
            switch($param_value[0]){

                # Subpar�metros representados como clave - valor
                case '{':
                    $parsed_subparam = array();

                    # Verifico que el cierre del conjunto de subpar�metros exista
                    if($param_value[$strlen_value-1] !== '}'){
                        self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredCloseHashSubparams',array('%param'=>$param_name));
                        break;
                    }

                    # Recorto la string de los subpar�metros, para ignorar las llaves
                    $param_value = trim(substr($param_value,1,$strlen_value-2));
                    if($param_value === ''){
                        self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredSubparams');
                        break;
                    }

                    # Verifico que los subpar�metros sean v�lidos
                    $subparams = explode(',',$param_value);
                    foreach($subparams as $subparam){
                        $trimmed_subparam = trim($subparam);

                        # Verifico que el subpar�metro no est� vac�o
                        if($trimmed_subparam === ''){
                            self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredSubparamNameValue');
                            break(1);
                        }

                        # Verifico que el subpar�metro tenga diferenciado la clave del valor
                        $seppos_subparam = strpos($trimmed_subparam,':');
                        if($seppos_subparam === false){
                            self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredSubparamValue');
                            break(1);
                        }

                        # Verifico que el nombre del subpar�metro sea requerido
                        $subparam_name = substr($trimmed_subparam,0,$seppos_subparam);
                        if($subparam_name === false || ($trimmed_subparam_name = trim($subparam_name)) === ''){
                            self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredSubparamName');
                            break(1);
                        }

                        # Verifico que el valor del subpar�metro sea requerido
                        $subparam_value = substr($trimmed_subparam,$seppos_subparam+1);
                        if($subparam_value === false || ($trimmed_subparam_value = trim($subparam_value)) === ''){
                            self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredSubparamValue');
                            break(1);
                        }

                        # En caso que todo est� bien, agrego el subpar�metro
                        $parsed_subparam[$trimmed_subparam_name] = $trimmed_subparam_value;
                    }

                    $parsed[$param_name] = $parsed_subparam;
                    break;

                # Subpar�metros representados unicamente con valor
                case '[':

                    $parsed_subparam = array();

                    # Verifico que el cierre del conjunto de subpar�metros exista
                    if($param_value[$strlen_value-1] !== ']'){
                        self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredCloseArraySubparams',array('%param'=>$param_name));
                        break;
                    }

                    # Recorto la string de los subpar�metros, para ignorar los corchetes
                    $param_value = trim(substr($param_value,1,$strlen_value-2));
                    if($param_value === ''){
                        self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredSubparams');
                        break;
                    }

                    # Verifico que haya al menos un par�metro
                    $subparams = explode(',',$param_value);
                    foreach($subparams as $subparam){

                        $trimmed_subparam = trim($subparam);

                        # Verifico que el subpar�metro no est� vac�o
                        if($trimmed_subparam === ''){
                            self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_requiredSubparamValue');
                        }

                        # En caso que todo est� bien, agrego el subpar�metro
                        else{
                            $parsed_subparam[] = $trimmed_subparam;
                        }
                    }

                    $parsed[$param_name] = $parsed_subparam;
                    break;

                # En caso que sean par�metros normales
                default:

                    $parsed[$param_name] = $param_value;
                    break;
            }

        }
        return $parsed;
    }

    /**
     * Actualiza la metadata de un sitio
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio
     * @static
     * @access protected
     * @return array logs de la actualizaci�n
     */
    protected static function _updateMetadataSitio(&$daoSitio){

        # En caso que el sitio a�n no tenga definida una URL
        if(empty($daoSitio->url)){
            DokkoLogger::log(E_USER_ERROR,self::$innySpeech->getSpeech('metadata','metadata_errorEmptyURL'),null,null,$daoSitio->id_sitio);
            return array('errors' => array(utf8_encode(self::$innySpeech->getSpeech('metadata','metadata_errorEmptyURL'))));
        }

        # Armo la URL al archivo del sitio
        $url = 'http://'.str_replace('http://','',$daoSitio->url).'/'.self::$updateMetadataFile;

        # En caso que la URL no exista, logueo el error
        # Para verificar que la URL exista, le agrego el par�metro "ping" para
        # indicar que solo necesito hacer un ping a esa url
        if(!Denko::url_exists($url.'?ping_only')){
            $url = 'https://'.str_replace('https://','',$daoSitio->url).'/'.self::$updateMetadataFile;
            if(!Denko::url_exists($url.'?ping_only')) {
                DokkoLogger::log(E_USER_ERROR, self::$innySpeech->getSpeech('metadata', 'metadata_errorConfigFileNotExists'), null, null, $daoSitio->id_sitio);
                return array('errors' => array(utf8_encode(self::$innySpeech->getSpeech('metadata', 'metadata_errorConfigFileNotExists'))));
            }
        }

        # Ejecuto el PHP que genera la metadata, y (si tiene) obtengo las alertas
        $json_response = file_get_contents($url);

        # Reporto que el parseo no tiene una respuesta v�lida
        if(empty($json_response)){
            DokkoLogger::log(E_USER_ERROR,self::$innySpeech->getSpeech('metadata','metadata_errorEmptyParsing'),null,null,$daoSitio->id_sitio);
            return array('errors' => array(utf8_encode(self::$innySpeech->getSpeech('metadata','metadata_errorEmptyParsing'))));
        }

        # Convierto los mensajes en arreglo
        $messages = json_decode($json_response,true);

        # Verifico que el json tenga formato v�lido
        if(empty($messages)){
            DokkoLogger::log(E_USER_ERROR,self::$innySpeech->getSpeech('metadata','metadata_errorParsingMessages'),null,null,$daoSitio->id_sitio);
            return array('errors' => array(utf8_encode(self::$innySpeech->getSpeech('metadata','metadata_errorParsingMessages'))));
        }

        # Retorno el arreglo de mensajes que entrega el parseador
        return $messages['messages'];
    }

    /**
     * Actualiza la metadata de un sitio
     *
     * @param integer $id_sitio ID del Sitio que actualiza la metadata
     * @static
     * @access public
     * @return boolean TRUE en caso que no haya alerta ni errores, FALSE en caso contrario
     */
    public static function update($id_sitio){

        # Obtengo el DAO del Sitio
        $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);

        if($daoSitio != null){

            # Obtengo el archivo de speechs
            $archivo_speechs = InnyCore_Speech::getNombreArchivoSpeechBaseSitio($daoSitio);

            # INIT
            self::$innySpeech = new InnyCore_Speech($archivo_speechs);

            # Obtengo los mensajes del parsing de la metadata
            $messages = self::_updateMetadataSitio($daoSitio);

            # En caso que no haya lanzado ning�n error ni alerta
            if(!empty($messages) && $messages == 'none'){
                return true;
            }

            # Verifico si el parseo lanz� mensajes de error
            if(!empty($messages['errors'])){
                foreach($messages['errors'] as $error){
                    Denko::addErrorMessage(utf8_decode($error));
                }
            }

            # Verifico si el parseo lanz� mensajes de alerta
            if(!empty($messages['warnings'])){
                foreach($messages['warnings'] as $warning){
                    Denko::addWarningMessage(utf8_decode($warning));
                }
            }
        }

        # En caso que el sitio no exista o no tenga seteada url alguna, retorno FALSE
        return false;
    }

    /**
     *
     *
     */
    public static function actualizarTodosLosSitios($habilitado=null){
        $logs = array();
        $daoSitio = DB_DataObject::factory('sitio');
        $daoSitio->orderBy('nombre_sitio');
        if($habilitado !== null){
            $daoSitio->habilitado = $habilitado;
        }
        if($daoSitio->find()){
            self::$innySpeech = new InnyCore_Speech();
            while($daoSitio->fetch()){
                $logs[$daoSitio->id_sitio] = array(
                    'nombre'   => $daoSitio->nombre_sitio,
                    'url'      => $daoSitio->url,
                    'messages' => self::_updateMetadataSitio($daoSitio)
                );
            }
        }
        return $logs;
    }

    /**
     * Genera el .htacces para el sitio, unificando reglas base con locales
     *
     * @static
     * @access public
     * @return boolean TRUE en caso de �xito, FALSE si no puede unir los archivos
     */
    public static function updateHtaccess(){

        # Obtengo el contenido de los htaccess
        if(file_exists(self::$htaccessBaseFilename) && file_exists(self::$htaccessBaseCacheFilename)){
            self::$error_messages[] = utf8_encode('Hay 2 archivos .htaccess.base - usando la versi�n SIN CACHE!');
        }
        if(file_exists(self::$htaccessBaseFilename)){
            $htaccessBase = file_get_contents(self::$htaccessBaseFilename);
        }else if(file_exists(self::$htaccessBaseCacheFilename)){
            $htaccessBase = file_get_contents(self::$htaccessBaseCacheFilename);
        }else{
            $htaccessBase = '';
        }

        $htaccessLocal = file_exists(self::$htaccessLocalFilename) ? file_get_contents(self::$htaccessLocalFilename) : '';

        # En caso que ambos no existan o est�n vac�os, no uno nada
        if($htaccessBase == '' && $htaccessLocal == ''){
            return true;
        }

        # Verifico que el archivo .htaccess tenga permisos de escritura
        $htaccess_path = '.htaccess';
        if(file_exists($htaccess_path) && is_file($htaccess_path) && !is_writable('.htaccess')){
            DokkoLogger::log(E_USER_ERROR,self::$innySpeech->getSpeech('metadata','metadata_errorNoWritableHtaccess'),null,null,self::$id_sitio);
            self::$error_messages[] = utf8_encode(self::$innySpeech->getSpeech('metadata','metadata_errorNoWritableHtaccess'));
            return false;
        }

        # Uno los contenidos de ambos archivos y lo encodeo a UTF-8
        $baseSeparador = '################################ BASE RULES ####################################';
        $localSeparador = '############################### LOCAL RULES ####################################';
        file_put_contents('.htaccess',utf8_encode($baseSeparador."\n\n".$htaccessBase."\n\n".$localSeparador."\n\n".$htaccessLocal));
        return true;
    }

    /**
     *
     *
     * @static
     * @access public
     * @return void
     */
    public static function validateDataType($type,$value){

        # Verifico que el tipo de dato est� seteado
        $type = trim($type);
        if($type == ''){
            $client_message_prefix = $log_message_prefix = self::$innySpeech->getSpeech('metadata','metadata_coreErrorDataType');
            self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_metadataType_requiredType');
        }

        # Verifico que el tipo de dato exista
        $className = 'InnyCore_MetadataType'.ucfirst(strtolower($type));
        if(class_exists($className)){
            eval('$validate = '.$className.'::validate($value);');
            return $validate;
        }

        # En caso que el tipo de dato no exista, lanzo el error
        $client_message_prefix = $log_message_prefix = self::$innySpeech->getSpeech('metadata','metadata_coreErrorDataType');
        self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,'metadata_metadataType_unknownType',array('%type'=>$type));
        return false;
    }

    /**
     *
     *
     * @static
     * @access public
     * @return void
     */
    public static function reportInvalidValue($section,$datatype){
        self::_reportValueWarning($section,$datatype,'metadata_errorConfigInvalidValue');
    }

    /**
     *
     * @static
     * @access public
     * @return void
     */
    public static function reportRequiredValue($section,$datatype){
        self::_reportValueWarning($section,$datatype,'metadata_errorConfigRequiredValue');
    }

    /**
     *
     *
     * @static
     * @access protected
     * @return void
     */
    protected static function _reportValueWarning($section,$datatype,$metadata_speech){
        $metadataType = InnyCore_MetadataType::factory($datatype);
        $client_message_prefix = str_replace('%section',$section,self::$innySpeech->getSpeech('metadata','metadata_sectionPrefix'));
        $log_message_prefix = str_replace('%section',$section,self::$innySpeech->getSpeech('metadata','metadata_logWarning'));
        self::_reportParamParseError(E_USER_WARNING,$client_message_prefix,$log_message_prefix,$metadata_speech,array('%values' => $metadataType->values()));
    }

    /**
     *
     *
     */
    public static function extraerNombreListado($nombre_seccion){
        return substr($nombre_seccion,strlen(self::$prefix_listado));
    }

    /**
     *
     *
     */
    public static function reportParseError($error_type,$prefix_logMessage,$prefix_cmsMessage,$error_message){

        # Preparo el mensaje
        $message = $prefix_cmsMessage.': '.$error_message;

        # En caso que el error sea de alerta
        if($error_type == E_USER_WARNING){
            self::$warning_messages[] = utf8_encode($message);
        }

        # En caso que el error sea de error
        else{
            self::$error_messages[] = utf8_encode($message);
        }

        # Logueo el mensaje
        DokkoLogger::log($error_type,$prefix_logMessage.': '.$message,null,null,self::$id_sitio);
    }

    /**
     *
     *
     */
    public static function addPostParseCallback($callback){
        self::$postCallbacks[] = $callback.';';
    }

    ////////////////////////////////////////////////////////////////////////////
    //                              SPEECH                                    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    public static function getSpeechText($section,$config_name){
        return self::$innySpeech->getSpeech($section,$config_name);
    }

    ////////////////////////////////////////////////////////////////////////////
    //                             DAO COMMON                                 //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Verifica si el listado tiene cierto permiso
     *
     * @param DataObjects &$dao DAO [DataObjects_Contenido | DataObjects_Listado]
     * @param string $permiso permiso [ A | B | M | V | O ]
     * @static
     * @access public
     * @return boolean si el listado tiene cierto permiso
     */
    public static function tienePermiso(&$dao,$permiso){

        # En caso que sea con permisos de tracker
        if(Inny::getPermisosTracker()){
            return true;
        }

        # Verifico si el valor del permiso existe en cache
        $permission = $dao->getValorEnCache(array('parametros','permission'));

        # En caso que no exista en la cache, lo obtengo y lo cacheo
        if(empty($permission)){
            $permission = strtoupper($dao->getMetadataValue('parametros','permission'));
            $permission = ($permission == 'NONE' ? 'none' : str_split($permission));
            $dao->setValorEnCache(array('parametros','permission'),$permission);
        }

        # En caso que no haya ning�n permiso
        if($permission == 'none'){
            return false;
        }

        # Verifico si el permiso existe
        $permiso = strtoupper($permiso);
        foreach($permission as $p){
            if($p == $permiso){
                return true;
            }
        }

        # En caso que no exista el permiso
        return false;
    }
}
################################################################################