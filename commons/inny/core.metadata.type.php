<?php
abstract class InnyCore_MetadataType{

    /**
     *
     *
     */
    public function __construct(){

    }

    /**
     *
     *
     */
    public static function factory($type){

        # Verifico que el tipo de dato est� seteado
        $type = trim($type);
        if($type == ''){
            DokkoLogger::log(E_USER_ERROR,'Error: el tipo de dato de metadata "'.$type.'" es requerido');
        }

        #
        $class = 'InnyCore_MetadataType'.ucwords(strtolower($type));
        if(class_exists($class)){
            eval('$datatype = new '.$class.'();');
            return $datatype;
        }

        # En caso que el tipo de dato no exista, retorno NULL
        DokkoLogger::log(E_USER_ERROR,'Error: el tipo de dato de metadata "'.$type.'" no est� implementado');
        return null;
    }

    /**
     *
     *
     */
    public abstract function values();
}

/**
 *
 *
 */
class InnyCore_MetadataTypeBinary extends InnyCore_MetadataType{

    /**
     *
     *
     */
    public static function validate($value){
        return ($value == '0' || $value == '1');
    }

    /**
     *
     *
     */
    public function values(){
        return '0,1';
    }
}
################################################################################
?>