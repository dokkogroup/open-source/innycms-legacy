<?php
/**
 * Project: Inny CMS
 * File: core.producto.php
 * Purpose: Core para ABM de productos
 *
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 * @copyright 2007-2009 Dokko Group
 * @package core
 */
class InnyCore_Producto extends InnyCore_Elemento{

    /**
     * @static
     * @access public
     * @var string prefijo de los campos de texto
     */
    public static $texto_prefijo = 'texto';

    /**
     * @static
     * @access public
     * @var integer cantidad m�xima de campos de texto por producto
     */
    public static $texto_cantidad = 20;

    /**
     * @static
     * @access public
     * @var string prefijo de los contenidos referentes a los archivos
     */
    public static $archivo_prefijo = 'archivo';

    /**
     * @static
     * @access public
     * @var string prefijo de los campos �ndice
     */
    public static $indice_prefijo = 'indice';

    /**
     * @static
     * @access public
     * @var integer cantidad de campos �ndice
     */
    public static $indice_cantidad = 5;

    /**
     * @static
     * @access public
     * @var string prefijo de los campos contador
     */
    public static $contador_prefijo = 'contador';

    /**
     * @static
     * @access public
     * @var integer cantidad de contadores
     */
    public static $contador_cantidad = 5;

    /**
     * Verifica que el nombre de un campo sea v�lido
     *
     * @param string $campo_nombre nombre del campo
     * @param string $campo_prefijo prefijo del tipo de campo
     * @param integer $campo_cantidad cantidad de campos permitidos
     * @static
     * @access protected
     * @return boolean resultado de la verificaci�n
     */
    protected static function verificarNombreCampoValido($campo_nombre,$campo_prefijo,$campo_cantidad){

        # Verifico que el nombre del campo de texto est� seteado
        $campo_nombre = trim($campo_nombre);
        if($campo_nombre === ''){
            return self::addErrorMessage('input_error','inputError_requiredFieldValue',array('%campo_nombre' => $campo_prefijo));
        }

        # Verifico que el nombre del campo de texto sea v�lido
        $campo_nombre = strtolower($campo_nombre);
        $strlen_campoPrefijo = strlen($campo_prefijo);
        if(substr($campo_nombre,0,$strlen_campoPrefijo) != $campo_prefijo){
            return self::addErrorMessage('producto','producto_errorNombreCampoNoValido',array('%campo_prefijo'=>$campo_prefijo,'%campo_nombre'=>$campo_nombre,'%campo_cantidad'=>$campo_cantidad));
        }

        # Verifico si est� seteado el n�mero de campo
        $campo_numero = substr($campo_nombre,$strlen_campoPrefijo);
        if($campo_numero === false){
            return self::addErrorMessage('producto','producto_errorNumeroCampoRequerido',array('%campo_prefijo'=>$campo_prefijo,'%campo_nombre'=>$campo_nombre));
        }

        # Verifico que el n�mero de campo sea un entero v�lido
        if(!InnyCore_Utils::is_integer($campo_numero)){
            return self::addErrorMessage('producto','producto_errorNumeroCampoNoValido',array('%campo_prefijo'=>$campo_prefijo,'%campo_nombre'=>$campo_nombre));
        }

        # Verifico que el n�mero est� dentro del m�nimo y m�ximo permitidos
        if($campo_numero < 1 || $campo_numero > $campo_cantidad){
            return self::addErrorMessage('producto','producto_errorNumeroCampoFueraRango',array('%campo_prefijo'=>$campo_prefijo,'%campo_nombre'=>$campo_nombre,'%campo_cantidad'=>$campo_cantidad));
        }

        # Retorno que el nombre del campo es v�lido
        return true;
    }

    /**
     * Verifica que los �ndices y sus valores sean v�lidos
     *
     * @param array $data_indices valores de los �ndices
     * @static
     * @access public
     * @return boolean si la verificaci�n de los �ndices es v�lida
     */
    protected static function validarIndices($data_indices){

        # Verifico cada �ndice y su valor
        foreach($data_indices as $indice_nombre => $indice_valor){

            # Verifico que el nombre del �ndice sea v�lido
            if(!self::verificarNombreCampoValido($indice_nombre,self::$indice_prefijo,self::$indice_cantidad)){
                return false;
            }

            # Verifico el valor del �ndice est� seteado
            $indice_valor = trim($indice_valor);
            if($indice_valor === ''){
                return self::addErrorMessage('producto','producto_errorIndiceRequerido',array('%indice_nombre'=>$indice_nombre));
            }

            # Verifico el valor del �ndice sea un n�mero entero v�lido
            if(!InnyCore_Utils::is_integer($indice_valor)){
                return self::addErrorMessage('producto','producto_errorIndiceNoValido',array('%indice_nombre'=>$indice_nombre));
            }
        }

        # En caso que todo est� OK
        return true;
    }

    /**
     * Verifica que el valor de un campo de texto sea v�lido
     *
     * @param &$daoListado
     * @param &$daoProducto
     * @param InnyType &$innyTextType
     * @param string $texto_campo nombre del campo de texto
     * @param mixed $texto_valor valor del campo de texto
     * @static
     * @access protected
     * @return boolean resultado de la verificaci�n
     */
    protected static function verificarCampoDeTexto(&$daoListado,&$daoProducto,&$innyTextType,$texto_campo,$texto_valor,$lenguaje=null){

        # Seteo el valor del texto
        $innyTextType->setValue($texto_valor);

        # Texto requerido
        $texto_requerido = $innyTextType->getMetadataValue('parametros','required');

        # Nombre del campo de texto
        $texto_nombre = Denko::capitalize($innyTextType->getMetadataValue('nombre'));

        # Prefijo para los mensajes de error
        $error_prefix = self::$innySpeech->getSpeech(
            'input_error',
            $lenguaje === null ? 'inputError_textSimplePrefix' : 'inputError_textMultiPrefix',
            array('%campo_nombre' => $texto_nombre, '%lenguaje' => $lenguaje)
        );

        ////////////////////////////////////////////////////////////////////////

        # Si el campo de texto est� vac�o, verifico que el texto no sea requerido
        if($innyTextType->is_empty()){
            if($texto_requerido == 'true'){
                return self::addErrorMessage('input_error','inputError_requiredValue');
            }
        }

        # En caso que el campo no est� vac�o, verifico si el valor del texto es v�lido
        elseif(!$innyTextType->validate()){
            return self::addErrorMessage('input_error','inputError_invalidValue');
        }

        # Verifico en caso que el valor deba ser �nico
        $parametro_unique = strtolower($innyTextType->getMetadataValue('parametros','unique'));
        $parametro_multilang = strtolower($innyTextType->getMetadataValue('parametros','multilang'));
        if($innyTextType->isUniqueTextType() && $parametro_multilang == 'false' && ($parametro_unique == 'cs' || $parametro_unique == 'ci')){

            $texto_valor = $innyTextType->trimmed();
            $daoProductoEnListado = DB_DataObject::factory('producto');
            $whereAdd = '
                id_listado = '.$daoListado->id_listado.'
                '.(InnyCore_Dao::isId($daoProducto->id_producto) ? 'and id_producto != '.$daoProducto->id_producto : '').'
            ';
            switch($parametro_unique){
                # Case Insensitive
                case 'ci':
                    $whereAdd.= ' and '.$texto_campo.' like \''.$innyTextType->getValue().'\'';
                    break;

                # Case Sensitive
                case 'cs':
                    $whereAdd.= ' and '.$texto_campo.' = cast(\''.$innyTextType->getValue().'\' as binary)';
                    break;
            }

            # Verifico si ya existe un producto as�
            $daoProductoEnListado->whereAdd($whereAdd);
            if($daoProductoEnListado->find()){
                return self::addErrorMessage('input_error','inputError_uniqueValue',array('%item' => Denko::capitalize($daoListado->getMetadataValue('item')),'%texto_nombre' => $texto_nombre, '%texto_valor' => $innyTextType->getValue()));
            }
        }

        # Retorno que todo est� OK
        return true;
    }

    /**
     * Verifica si los valores de los campos de texto para el Producto son v�lidos
     *
     * @param array $data_textos arreglo que contiene los datos de los textos
     * @param DataObjects_Listado &$daoListado
     * @param DataObjects_Producto &$daoProducto
     * @static
     * @access public
     * @return boolean resultado de la verificaci�n
     */
    protected static function validarCamposTexto($data_textos,&$daoListado,&$daoProducto){

        # Variable para la validaci�n de los campo de texto
        $validacion = true;

        # Obtengo el sitio al que pertenece el listado
        $daoSitio = $daoListado->getSitio();

        # Obtengo la cantidad de textos del producto
        $texto_cantidad = $daoListado->getMetadataValue('texto','cantidad');

        # Verifico que los campos y su contedido sea v�lido
        foreach($data_textos as $texto_nombre => $texto_valor){

            # Verifico que el nombre del campo de texto sea v�lido
            if(!self::verificarNombreCampoValido($texto_nombre,self::$texto_prefijo,$texto_cantidad)){
                $validacion = false;
                continue;
            }

            # Obtengo el n�mero de campo de texto
            $texto_numero = substr(trim($texto_nombre),strlen(self::$texto_prefijo));

            # Creo el tipo de dato
            $innyTextType = $daoListado->createInnyType('texto',$texto_numero);

            # Texto requerido
            $texto_requerido = $innyTextType->getMetadataValue('parametros','required');

            # Antes chequeo si el texto es multilenguaje
            if($daoSitio->getMetadataValue('multilang','enabled') == '1' && $innyTextType->getMetadataValue('parametros','multilang') == 'true'){

                # Verifico que los valores del texto est�n en un arreglo
                if(!is_array($texto_valor)){
                    $validacion = self::addErrorMessage('producto','producto_errorMultilangFormato',array('%texto_nombre'=>$texto_nombre));
                    continue;
                }

                # Obtengo los lenguajes que soporta el sitio
                $sitio_lenguajes = $daoSitio->getMetadataValue('multilang','languages');
                foreach($texto_valor as $codigo_lenguaje => $texto_multilang){

                    # Verifico que el c�digo del lenguaje pertenezca a los del sitio
                    if(!array_key_exists($codigo_lenguaje,$sitio_lenguajes)){
                        $validacion = self::addErrorMessage('producto','producto_errorMultilangCodigoNoValido',array('%codigo_lenguaje'=>$codigo_lenguaje));
                        continue;
                    }

                    # Verifico el campo de texto
                    if(!self::verificarCampoDeTexto($daoListado,$daoProducto,$innyTextType,$texto_nombre,$texto_multilang,$sitio_lenguajes[$codigo_lenguaje])){
                        $validacion = false;
                    }
                }
            }

            # En caso que el texto no sea multilenguaje, lo valido de forma normal
            else{
                $verificacionCampoDeTexto = self::verificarCampoDeTexto($daoListado,$daoProducto,$innyTextType,$texto_nombre,$texto_valor);
                if(!$verificacionCampoDeTexto){
                    $validacion = false;
                }
            }


            # Libero memoria
            unset($innyTextType);
        }

        # Retorno el resultado de la validaci�n
        return $validacion;
    }

    /**
     * Verifica que los archivos adjuntos sean v�lidos, y que los archivos requeridos est�n seteados
     *
     * @param array $data_archivos informaci�n de los archivos
     * @param DataObjects_Listado &$daoListado DAO Listado al que pertenece el producto
     * @param DataObjects_Producto &$daoProducto DAO Producto al que se le adjuntan los archivos
     * @static
     * @access protected
     * @return boolean resultado de la validaci�n
     */
    protected static function validarArchivosAdjuntos($data_archivos,&$daoListado,&$daoProducto,$debug_backtrace=null){

        # Obtengo el debug_backtrace
        if($debug_backtrace === null){
            $debug_backtrace = debug_backtrace();
        }

        # Verifico que haya archivos que adjuntar
        $archivo_cantidad = $daoListado->getMetadataValue('archivo','cantidad');

        # En caso que el listado posea productos que no adjuntan archivos,
        # no es necesario el chequeo
        if($archivo_cantidad == 0){
            return true;
        }

        # Si tengo que mostrar mensajes, cargo las secciones de los speechs
        $error_prefix = self::$innySpeech->getSpeech('input_error','inputError_fileSimplePrefix');

        # Inicializo las variables que voy a necesitar
        $result = true;
        $validacion = null;

        # Arreglo donde voy anotando el resultado de la evaluaci�n de los archivos
        $resultadoArchivosAdjuntos = array();

        # Hago la verificaci�n de todos los archivos adjuntos
        foreach($data_archivos as $archivo_nombre => $archivo_data){

            # Verifico que el nombre del campo del archivo sea v�lido
            if(!self::verificarNombreCampoValido($archivo_nombre,self::$archivo_prefijo,$archivo_cantidad)){
                return false;
            }

            $archivo_numero = substr(strtolower(trim($archivo_nombre)),strlen(self::$archivo_prefijo));

            # Verifico el tama�o y formato de los archivos
            if(!empty($archivo_data['tmp_name'])){

                $innyType = $daoListado->createInnyType('archivo',$archivo_numero);

                # Obtengo el tipo de archivo correspondiente a ese n�mero
                $archivo_tipo = $innyType->getMetadataValue('tipo');

                # Verifico que el archivo sea v�lido
                $error_code = $innyType->validate($archivo_data);

                # En caso que haya errores
                if($error_code != UPLOAD_ERR_OK){

                    # Seteo como falso el resultado de la validaci�n
                    $result = false;

                    # Obtengo el nombre del archivo, para mostrarlo en el mensaje de error
                    $archivo_nombre = $innyType->getMetadataValue('nombre');

                    switch($error_code){

                        # El archivo excede el l�mite de tama�o
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE: $errorSpeech = 'file_errorTooBigToServer'; break;

                        # S�lo una parte del archivo se ha adjuntado
                        case UPLOAD_ERR_PARTIAL: $errorSpeech = 'file_errorOnlyPartUpload'; break;

                        # No se ha adjuntado archivo alguno
                        case UPLOAD_ERR_NO_FILE: $errorSpeech = 'file_errorRequired'; break;

                        # El formato del archivo no es v�lido
                        case UPLOAD_ERR_EXTENSION:

                            # Agrego el error dependiendo el tipo de archivo
                            switch($archivo_tipo){

                                # En caso que sea imagen
                                case 'image': $errorSpeech = 'file_errorImageInvalidFormat'; break;

                                # En caso que sea un flash
                                case 'flash': $errorSpeech = 'file_errorFlashInvalidFormat'; break;

                                # En caso que sea un sonido
                                case 'sound': $errorSpeech = 'file_errorSoundInvalidFormat'; break;

                                # En otro caso
                                default: $errorSpeech = 'file_errorFileInvalidFormat'; break;
                            }
                            break;
                    }

                    # Agrego el mensaje de error
                    self::addErrorMessage('file',$errorSpeech,null,self::$innySpeech->getSpeech('input_error','inputError_fileSimplePrefix',array('%nombre_archivo' => Denko::capitalize($archivo_nombre))));

                    # Anoto que la verificaci�n fu� incorrecta
                    $resultadoArchivosAdjuntos[$archivo_numero] = $error_code;
                }

                # Anoto que la verificaci�n fu� correcta
                else{
                    $resultadoArchivosAdjuntos[$archivo_numero] = UPLOAD_ERR_OK;
                }

                # Libero memoria
                unset($innyType);
            }

            # Anoto que el archivo no existe
            else{
                $resultadoArchivosAdjuntos[$archivo_numero] = UPLOAD_ERR_NO_FILE;
            }
        }

        # Verifico que est�n todos los archivos requeridos
        # Un archivo que sea requerido puede no estar adjunto, ya que haya sido
        # agregado previamente a la DB.
        # Por eso debo verificar primero que est� en la DB. Si no est�, verifico que
        # est� adjunto
        for($i = 1; $i <= $archivo_cantidad; $i++){

            # Creo el elemento
            $innyType = $daoListado->createInnyType('archivo',$i);

            # En caso que el archivo sea requerido...
            if($innyType->getMetadataValue('parametros','required') == 'true'){

                # Inicializo la llave en false
                $tieneArchivoAdjunto = false;

                # Si el producto ya existe, entonces primero verifico que exista en la DB
                if($daoProducto->id_producto && $daoProducto->id_producto != 'null'){
                    $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
                    $daoProductoTemporal->id_producto = $daoProducto->id_producto;
                    $daoProductoTemporal->posicion = $i;
                    if($daoProductoTemporal->find()){
                        $tieneArchivoAdjunto = true;
                    }
                }

                # En caso que no tenga archivo adjunto, verifico si es requerido
                if(!$tieneArchivoAdjunto && isset($resultadoArchivosAdjuntos[$i]) && $resultadoArchivosAdjuntos[$i] != UPLOAD_ERR_OK){

                    # Agrego el mensaje de error. Indico en el resultado que hubo errores
                    $result = self::addErrorMessage('file','file_errorRequired',null,self::$innySpeech->getSpeech('input_error','inputError_fileSimplePrefix',array('%nombre_archivo' => Denko::capitalize($archivo_nombre))));
                }
            }

            # Libero memoria
            unset($innyType);
        }

        # Libero memoria
        unset($resultadoArchivosAdjuntos);

        # Retorno el resultado de la validaci�n
        return $result;
    }

    /**
     * Setea los valores de los campos de texto en el DAO Producto
     *
     * @param array $data_textos datos de los textos
     * @param DataObjects_Listado &$daoListado DAO Listado al que pertenece el producto
     * @param DataObjects_Producto &$daoProducto DAO Producto donde se setean los valores de los campos de texto
     * @static
     * @access protected
     * @return void
     */
    protected static function setearCamposDeTexto($data_textos,&$daoListado,&$daoProducto){

        # Obtengo el Sitio al cual pertenece el listado
        $daoSitio = $daoListado->getSitio();

        # Seteo los valores en cada campo de texto
        foreach($data_textos as $texto_campo => $texto_valor){

            # Obtengo el nombre del campo de texto
            $texto_campo = strtolower(trim($texto_campo));

            # Obtengo el n�mero de campo
            $texto_numero = substr($texto_campo,strlen(self::$texto_prefijo));

            # Obtengo el nombre de la columna, el tipo de dato y su valor en POST
            $innyTextType = $daoListado->createInnyType('texto',$texto_numero);

            # Obtengo el tipo de texto
            $texto_tipo = $innyTextType->getMetadataValue('tipo');

            # En caso que el campo de texto sea multilenguaje
            if($daoSitio->getMetadataValue('multilang','enabled') == '1' && $innyTextType->getMetadataValue('parametros','multilang') == 'true'){
                $arreglo_textos = array();
                foreach($texto_valor as $lang_code => $lang_value){
                    $innyTextType->setValue($lang_value);
                    $arreglo_textos[$lang_code] = $innyTextType->postdata2dbdata();
                }
                Denko::arrayUtf8Encode($arreglo_textos);
                $daoProducto->$texto_campo = json_encode($arreglo_textos);
                unset($arreglo_textos);
            }

            # En caso que el campo de texto sea de un solo lenguaje
            else{

                # Seteo el dato en el DAO Producto
                $innyTextType->setValue($texto_valor);
                $daoProducto->$texto_campo = $innyTextType->postdata2dbdata();
            }

            # Libero memoria
            unset($innyTextType);
        }
    }

    /**
     * Setea los archivos en el producto
     *
     * @param string $action acci�n que se realiza [insert|update]
     * @param array $data_archivos datos de los archivos
     * @param DataObjects_Producto &$daoProducto DAO Producto al que setean los archivos
     * @param DataObjects_Listado &$daoListado DAO Listado al que pertenece el producto
     * @static
     * @access protected
     * @return void
     */
    protected static function setearArchivosAdjuntos($action,$data_archivos,&$daoProducto,&$daoListado){

        # Seteo el �ndice para los archivos variables de posici�n
        # Sirve para evitar que haya posiciones no ocupadas entre archivos debido
        # a usar inputs file aleatorios para ingresar archivos posicionables
        foreach($data_archivos as $archivo_nombre => $archivo_data){

            # Obtengo el nro de archivo
            $archivo_numero = substr(strtolower(trim($archivo_nombre)),strlen(self::$archivo_prefijo));
            $innyTypeFile = $daoListado->createInnyType('archivo',$archivo_numero);

            if(!empty($archivo_data['tmp_name'])){

                # En caso que haya que editar el producto, verifico si
                # previamente existe un archivo en esa posici�n
                # En caso que exista, lo elimino
                if($action == 'update'){

                    # Verifico si en esa posici�n ya hab�a alg�n archivo previamente agregado
                    $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
                    $daoProductoTemporal->id_producto = $daoProducto->id_producto;
                    $daoProductoTemporal->posicion = $archivo_numero;

                    # En caso que ya exista un archivo en esa posici�n, lo elimino
                    if($daoProductoTemporal->find(true)){
                        $daoProductoTemporal->normalDelete();
                    }
                }

                # Agrego el archivo en la tabla Temporal
                $daoProductoTemporal = DB_DataObject::factory('producto_temporal');
                $daoProductoTemporal->id_producto = $daoProducto->id_producto;
                $daoProductoTemporal->posicion = $archivo_numero;
                $daoProductoTemporal->id_temporal = $innyTypeFile->setFile($archivo_data);

                # Agrego la relaci�n producto temporal
                $daoProductoTemporal->normalInsert();
            }

            # En caso que haya descripcion para el archivo adjunto
            if(!empty($archivo_data['description']) || $action=='update') {

                # Verifico que el archivo exista en la DB
                $daoT=Denko::daoFactory('producto_temporal');
				$daoT->id_producto=$daoProducto->id_producto;
				$daoT->posicion=$archivo_numero;
				if($daoT->find(true)){
					$daoTemporal = DB_DataObject::factory('temporal');
    	            $daoTemporal->selectAdd();
        	        $daoTemporal->selectAdd('id_temporal,metadata');
            	    $daoTemporal->id_temporal=$daoT->id_temporal;

  	              	# Si existe, actualizo su descripci�n
    	            if($daoTemporal->find(true)){
        	            $metadata = json_decode($daoTemporal->metadata,true);
                	    Denko::arrayUtf8Decode($metadata);
            	        $metadata['description'] = $archivo_data['description'];
                	    Denko::arrayUtf8Encode($metadata);
                    	$daoTemporal->metadata = json_encode($metadata);
              			$daoTemporal->update();
					}
                }
            }
        }
        //TODO: Eliminar este llamado luego del testing.
        # Sincronizo los archivos posicionables
       //$daoProducto->sincronizarArchivosPosicionables();
    }

    /**
     * Valida los datos de un DAO Producto
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio al que pertenece el DAO Producto
     * @param DataObjects_Producto &$daoProducto DAO Producto
     * @param string $nombre_listado nombre del listado al que pertenece el DAO Producto
     * @param array $data datos para el DAO Producto
     * @param array $debug_backtrace debug_backtrace de la funci�n principal
     * @static
     * @access protected
     * @return boolean resultado de la validaci�n
     */
    public static function _insertarEditarProducto($action,&$daoSitio,$nombre_listado,$data,$id_producto=null,$settings=null,$debug_backtrace=null){

        # Seteo el debug_backtrace
        self::setDebugBacktrace($debug_backtrace === null ? debug_backtrace() : $debug_backtrace);
        $debug_backtrace = self::$debug_backtrace;

        # Inicializo los speechs para los mensajes de error
        self::initializeSpeech();

        # === SETEOS EN EL DAO PRODUCTO LOS DATOS PREVIOS A LA VALIDACI�N ==== #
        switch($action){

            # Insert del producto
            case 'insert':

                # Seteo el resultado que se entrega en caso de fallo o error
                $resultado_error = 0;

                # Creo el DAO Producto
                $daoProducto = DB_DataObject::factory('producto');

                break;

            # Update del producto
            case 'update':

                # Seteo el resultado que se entrega en caso de fallo o error
                $resultado_error = false;

                # Verifico que el producto pertenezca al sitio
                $daoProducto = $daoSitio->getProducto($id_producto);
                if($daoProducto == null){
                    return $resultado_error;
                }
                break;

            # En caso de cualquier otra acci�n
            default: return null;

        }

        # ================== VALIDACIONES DEL LISTADO ======================== #

        # Verifico que el nombre del listado no sea nulo
        $nombre_listado = InnyCore_Utils::lower(trim($nombre_listado));
        if(empty($nombre_listado)){
            self::addErrorMessage('listado','listado_errorNombreRequerido');
            return $resultado_error;
        }

        # Verifico que exista listado en el sitio con ese nombre
        $daoListado = $daoSitio->getListadoPorNombre($nombre_listado);
        if($daoListado == null){
            self::addErrorMessage('listado','listado_errorNoExisteEnSitio',array('%listado_nombre' => $nombre_listado));
            return $resultado_error;
        }

        # En caso que se agregue un nuevo producto, verifico si el listado
        # alcanz� el m�ximo limite permitido de elementos
        if($action == 'insert' && $daoListado->alcanzoLimiteProductosPermitido()){
            self::addErrorMessage('listado','listado_errorLimiteExcedido',array('%listado_nombre'=>$daoListado->getMetadataValue('nombre'),'%listado_limit'=>$daoListado->getMetadataValue('parametros','limit')));
            return $resultado_error;
        }

        # Linkeo el producto con el listado
        $daoProducto->id_listado = $daoListado->id_listado;

        # ============ VALIDACIONES DEL CONTENIDO DEL PRODUCTO =============== #

        # Variable que me dir� si las verificaciones fueron v�lidas
        $validacion = true;

        # CAMPOS DE TEXTO: Verifico que los campos de texto sean v�lidos
        if(isset($data['texto']) && !self::validarCamposTexto($data['texto'],$daoListado,$daoProducto)){

            $validacion = false;
        }

        # ARCHIVOS: Verifico que los archivos sean v�lidos
        if(isset($data['archivo']) && !self::validarArchivosAdjuntos($data['archivo'],$daoListado,$daoProducto)){
            $validacion = false;
        }

        # CARDINALIDAD: Verifico si est�n seteados los datos para la cardinalidad
        $modulo_cardinalidad = $daoListado->tieneCardinalidades();
        if($modulo_cardinalidad){
            InnyModule_Cardinal::setearOpciones(isset($settings[InnyModule_Cardinal::$metadata_section]) ? $settings[InnyModule_Cardinal::$metadata_section] : null);
            $ids_cardinalidad_validos = InnyModule_Cardinal::validarDatosCardinalidad(
                $daoSitio,
                $daoProducto,
                $nombre_listado,
                $data,
                $debug_backtrace
            );
            if($ids_cardinalidad_validos === false){
                $validacion = false;
            }
            else{

                # Obtengo la opcion de sincronizaci�n de las cardinalidades
                $cardinalidad_sincronizar = InnyModule_Cardinal::getSetting('sincronizar');
            }
        }

        # CONTADOR: Verifico que los valores de los contadores sean v�lidos
        if(isset($data['contador']) && !InnyModule_Contador::verificarValores($daoListado,$data['contador'],$debug_backtrace)){

            $validacion = false;
        }

        # INDICES: Verifico que los �ndices sean v�lidos
        if(isset($data['indice']) && !self::validarIndices($data['indice'])){

            $validacion = false;
        }

        # Si las validaciones no fueron correctas, retorno FALSE
        if(!$validacion){
            return $resultado_error;
        }

        # ============================= SETEOS =============================== #

        # TEXTO: Seteo los valores de los campos de texto
        if(isset($data['texto']) && $daoListado->getMetadataValue('texto','cantidad') > 0){
            self::setearCamposDeTexto($data['texto'],$daoListado,$daoProducto);
        }

        # CONTADOR: Seteo los valores de los contadores
        if(isset($data['contador'])){
            InnyModule_Contador::setearValoresEnProducto($daoListado,$daoProducto,$data['contador'],$debug_backtrace);
        }

        # �NDICE: Seteo los valores de los �ndices
        if(isset($data['indice'])){
            foreach($data['indice'] as $indice_campo => $indice_valor){
                $indice_campo = strtolower(trim($indice_campo));
                $daoProducto->$indice_campo = trim($indice_valor);
            }
        }

        # DSE: Seteo los "so_fields"
        if($daoListado->getMetadataValue('parametros','searchable') == 'true'){
            if(isset($data['dse']['so_field']) && count($data['dse']['so_field']) > 0){
                for($i = 0; $i <= 1; $i++){
                    if(isset($data['dse']['so_field'][$i])){
                        $so_field = 'so_field_'.$i;
                        $daoProducto->$so_field = $data['dse']['so_field'][$i];
                    }
                }
            }
        }

        # Si el listado no es buscable, le seteo que no debe ser indexado el Producto
        else{
            $daoProducto->doNotIndex();
        }

        # Resultado del inser/update
        $resultado_am = null;

        # ============================ INSERT ================================ #

        if($action == 'insert'){

            # Inserto el DAO Producto
            $id_producto = $daoProducto->insert();

            # En caso que haya un error de DB al insertar el DAO Producto
            if($id_producto == 0){
                self::addErrorMessage('producto','producto_errorDBInsert');
                return $resultado_error;
            }

            # Mantengo el ID del DAO Producto insertado
            $resultado_am = $id_producto;
        }

        # ============================== UPDATE ============================== #

        elseif($action == 'update'){

            # Inserto el DAO Producto
            $update = $daoProducto->update();

            # En caso que haya un error de DB al insertar el DAO Producto
            if($update === false){
                self::addErrorMessage('producto','producto_errorDBUpdate',array('%producto_id'=>$daoProducto->id_producto));;
                return $resultado_error;
            }

            # Mantengo el ID del DAO Producto actualizado
            $resultado_am = $update;
        }

        # CARDINALIDAD: Verifico si debo hacer los linkeos de cardinalidades
        if($modulo_cardinalidad){
            InnyModule_Cardinal::actualizarRelacionesEntreProductos($daoProducto,$ids_cardinalidad_validos,$cardinalidad_sincronizar);
        }

        # DSE: Sincronizo las categor�as del producto
        if(isset($data['dse']['categories']) && $daoListado->getMetadataValue('parametros','searchable') == 'true'){
            $daoProducto->sincronizarCategorias($data['dse']['categories']);
        }

        # ARCHIVOS: Seteo los archivos adjuntos al producto
        if(isset($data['archivo'])){
            self::setearArchivosAdjuntos($action,$data['archivo'],$daoProducto,$daoListado);
        }

        # Libero memoria
        $daoProducto->free();
        $daoListado->free();

        # Retorno el resultado del insert/update
        return $resultado_am;

    }

    /**
     * Cambia la posici�n de un elemento en su listado
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio al que pertenece el DAO Producto
     * @param string $nombre_listado nombre del listado al que pertenece el DAO Producto
     * @param integer $id_producto ID del DAO Producto
     * @param boolean $subirPos indica si se incrementa o decrementa la posici�n del elemento en su listado
     * @param $debug_backtrace debug_backtrace de la funci�n principal
     * @static
     * @access protected
     * @return boolean si se pudo modificar la posici�n
     */
    protected static function _cambiarPosicion(&$daoSitio,$nombre_listado,$id_producto,$subirPos=true){

        # Verifico que el DAO Listado pertenezca al Sitio actual
        $daoListado = $daoSitio->getListadoPorNombre($nombre_listado);
        if($daoListado == null){
            return self::addErrorMessage('listado','listado_errorNoExisteEnSitio',array('%listado_nombre'=>$nombre_listado));
        }

        # Verifico que el DAO Producto exista en la DB
        $daoProducto = $daoSitio->getProducto($id_producto);
        if($daoProducto == null){
            return self::addErrorMessage('producto','producto_errorNoExiste',array('%id_producto'=>$id_producto));
        }

        # Subo el orden del producto
        return $subirPos ? $daoProducto->subirPosicion() : $daoProducto->bajarPosicion();
    }

    /**
     * Inserta y setea las relaciones de un DAO Producto
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio al que pertenece el DAO Producto
     * @param array $data datos del DAO Producto
     * @param $nombre_listado nombre del listado al que pertenece el DAO Producto
     * @param $debug_backtrace debug_backtrace de la funci�n principal
     * @static
     * @access public
     * @return integer ID del DAO Producto agregado, 0 en caso de error
     */
    public static function insertar(&$daoSitio,$data,$nombre_listado,$settings=null,$debug_backtrace=null){
        return self::_insertarEditarProducto('insert',$daoSitio,$nombre_listado,$data,null,$settings,$debug_backtrace ? $debug_backtrace : debug_backtrace());
    }

    /**
     * Edita el contenido y relaciones de un DAO Producto
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio al que pertenece el DAO Producto
     * @param array $data datos del DAO Producto
     * @param $nombre_listado nombre del listado al que pertenece el DAO Producto
     * @param $id_producto ID del DAO Producto
     * @param $debug_backtrace debug_backtrace de la funci�n principal
     * @static
     * @access public integer n�mero de tuplas afectadas o FALSE en caso de error
     */
    public static function editar(&$daoSitio,$data,$nombre_listado,$id_producto,$settings=null,$debug_backtrace=null){
        return self::_insertarEditarProducto('update',$daoSitio,$nombre_listado,$data,$id_producto,$settings,$debug_backtrace ? $debug_backtrace : debug_backtrace());
    }

    /**
     * Elimina un DAO Producto
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio al que pertenece el DAO Producto
     * @param $nombre_listado nombre del listado al que pertenece el DAO Producto
     * @param $id_producto ID del DAO Producto
     * @param $debug_backtrace debug_backtrace de la funci�n principal
     * @static
     * @access public
     * @return integer numero de tuplas afectadas o FALSE en caso de error
     */
    public static function eliminar(&$daoSitio,$nombre_listado,$id_producto,$debug_backtrace=null){

        # Seteo el debug_backtrace
        self::setDebugBacktrace($debug_backtrace === null ? debug_backtrace() : $debug_backtrace);

        # Verifico que el DAO Listado pertenezca al Sitio actual
        $daoListado = $daoSitio->getListadoPorNombre($nombre_listado);
        if($daoListado == null){
            return self::addErrorMessage('listado','listado_errorNoExisteEnSitio',array('%listado_nombre'=>$nombre_listado));
        }

        # Verifico que el DAO Producto exista en la DB
        $daoProducto = $daoSitio->getProducto($id_producto);
        if($daoProducto == null){
            return self::addErrorMessage('producto','producto_errorNoExiste',array('%id_producto'=>$id_producto));
        }

        # En caso que todo est� OK, retorno el resultado del delete
        return $daoProducto->delete();
    }

    /**
     * Incrementa la posici�n de un elemento en su listado
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio al que pertenece el DAO Producto
     * @param string $nombre_listado nombre del listado al que pertenece el DAO Producto
     * @param integer $id_producto ID del DAO Producto
     * @param $debug_backtrace debug_backtrace de la funci�n principal
     * @static
     * @access protected
     * @return boolean si se pudo incrementar la posici�n en el listado
     */
    public static function subirPosicion(&$daoSitio,$nombre_listado,$id_producto,$debug_backtrace=null){

        # Seteo el debug_backtrace
        self::setDebugBacktrace($debug_backtrace === null ? debug_backtrace() : $debug_backtrace);

        # Cambio la posici�n
        return self::_cambiarPosicion($daoSitio,$nombre_listado,$id_producto,true);
    }

    /**
     * Decrementa la posici�n de un elemento en su listado
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio al que pertenece el DAO Producto
     * @param string $nombre_listado nombre del listado al que pertenece el DAO Producto
     * @param integer $id_producto ID del DAO Producto
     * @param $debug_backtrace debug_backtrace de la funci�n principal
     * @static
     * @access protected
     * @return boolean si se pudo decrementar la posici�n en el listado
     */
    public static function bajarPosicion(&$daoSitio,$nombre_listado,$id_producto,$debug_backtrace=null){

        # Seteo el debug_backtrace
        self::setDebugBacktrace($debug_backtrace === null ? debug_backtrace() : $debug_backtrace);

        # Cambio la posici�n
        return self::_cambiarPosicion($daoSitio,$nombre_listado,$id_producto,false);
    }

    /**
     * Agrega/Edita un producto teniendo en cuenta las variables en POST
     *
     * @param string $action accion [add|edit]
     * @param DataObjects_Sitio &$daoSitio DAO Sitio donde pertenece el producto
     * @param DataObjects_Listado &$daoListado DAO Listado donde pertenece el producto
     * @param DataObjects_Producto &$daoProducto DAO Producto
     * @static
     * @access public
     * @return boolean si pudo agregar/editar el producto
     */
    public static function agregarEditarDesdePost($action,&$daoSitio,&$daoListado,&$daoProducto){

        # Indico que debo mostrar los mensajes de error
        self::mostrarMensajesDeError();

        # Seteos extra
        $settings = array(
            'cardinalidad' => array(
                'sincronizar' => true
            )
        );

        # Datos del producto
        $post_data = array();

        # Seteo los textos
        $texto_cantidad = $daoListado->getMetadataValue('texto','cantidad');
        $sitio_lenguajes = $daoSitio->getMetadataValue('multilang','languages');
        if($texto_cantidad > 0){
            $post_data['texto'] = array();
            for($i = 1; $i <= $texto_cantidad; $i++){

                $texto_multilang = $daoListado->getMetadataValue('texto','texto'.$i,'parametros','multilang');
                if($texto_multilang == 'true'){
                    $post_data['texto']['texto'.$i] = array();
                    foreach($sitio_lenguajes as $lang_code => $lang_label){
                        $key = 'texto'.$i.'_'.$lang_code;
                        $post_data['texto']['texto'.$i][$lang_code] = isset($_POST[$key]) ? $_POST[$key] : null;
                    }
                }
                else{
                    $post_data['texto']['texto'.$i] = isset($_POST['texto'.$i]) ? $_POST['texto'.$i] : ( $daoListado->getMetadataValue('texto','texto'.$i,'tipo') == 'checkbox' ? '0' : null );
                }
            }
        }

        # Seteo los archivos
        $archivo_cantidad = $daoListado->getMetadataValue('archivo','cantidad');
        if($archivo_cantidad > 0){
            $post_data['archivo'] = array();
            for($i = 1; $i <= $archivo_cantidad; $i++){
                $post_data['archivo']['archivo'.$i] = $_FILES['archivo'.$i];

                # Datos extra para los archivos
                if($daoListado->getMetadataValue('archivo','archivo'.$i,'parametros','description') != 'none'){
                    $post_data['archivo']['archivo'.$i]['description'] = $_POST['archivo_'.$i.'_description'];
                }
            }
        }

        # Seteo los contadores
        $contador_cantidad = $daoListado->getMetadataValue('contador','cantidad');
        if($contador_cantidad > 0){
            $post_data['contador'] = array();
            for($i = 1; $i <= $contador_cantidad; $i++){
                $post_data['contador'][$i] = array('action' => 'set', 'value' => $_POST['contador'.$i]);
            }
        }

        # Seteo de las cardinalidades
        if($daoListado->getCardinalidadesHaciaOtrosListados()){
            $post_data['cardinalidad'] = array();
            $cardinal_prefix = 'cardinal_';
            $cardinal_prefixLen = strlen($cardinal_prefix);
            foreach($_POST as $postvar_nombre => $postvar_valor){
                if(substr($postvar_nombre,0,$cardinal_prefixLen) == $cardinal_prefix){

                    # Obtengo el nombre del listado
                    $nombre_listado = substr($postvar_nombre,$cardinal_prefixLen);

                    # En caso que haya productos que relacionar, los relaciono
                    if(($postvar_valor = trim($postvar_valor)) !== ''){
                        $post_data['cardinalidad'][$nombre_listado] = explode(',',$postvar_valor);
                    }

                    # Si no hay productos que relacionar, deben eliminarse todas las
                    # relaciones que tiene el producto con este listado
                    else{
                        if($action == 'edit'){
                            $daoProducto->eliminarRelacionesConProductos(); // ??
                        }
                    }
                }
            }

            # Si no hay datos para ning�n listado, no seteo datos para la cardinalidad
            if(count($post_data['cardinalidad']) == 0){
                unset($post_data['cardinalidad']);
            }
        }

        # Seteo los valores para el DSE
        if($daoListado->getMetadataValue('parametros','searchable') == 'true'){

            # Seteo las categor�as
            $post_data['dse'] = array();
            $post_data['dse']['categories'] = isset($_POST['dse_category_selecteds']) ? $_POST['dse_category_selecteds'] : array();

            # Seteo los "so_fields"
            $post_data['dse']['so_field'] = array();
            for($i = 0; $i <= 1; $i++){
                if(isset($_POST['so_field_'.$i])){
                    $post_data['dse']['so_field'][$i] = $_POST['so_field_'.$i];
                }
            }
        }

        # En caso de agregar el producto
        if($action == 'add'){
            $id_producto = self::insertar($daoSitio,$post_data,$daoListado->nombre,$settings);
            return $id_producto !== 0 ? $id_producto : false;
        }

        # En caso de editar el producto
        return (self::editar($daoSitio,$post_data,$daoListado->nombre,$_POST['id_producto'],$settings) !== false) ? $_POST['id_producto'] : false;
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                   CSV                                  //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Verifica si el CSV adjunto es v�lido. En caso que lo sea, lo agrega sus items como productos del listado.
     *
     * @static
     * @access protected
     * @return boolean
     * @see InnyCore_Producto::insertarProductosDesdeCSV()
     */
    protected static function validarArchivoCSV($varname){

        # Obtengo el resultado de la verificaci�n
        switch(DK_File::verifyUploadedFile($varname)){

            # En caso que el archivo se haya adjuntado correctamente, verifico su extensi�n
            case UPLOAD_ERR_OK:
                if(strtolower(DK_File::extension($_FILES[$varname]['name'])) == 'csv'){
                    return true;
                }
                $error_speech = 'file_errorCsvInvalidFormat';
                break;

            # Verifico si el archivo es de un tama�o mayor al permitido por el server
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE: $error_speech = 'file_errorTooBigToServer'; break;

            # Verifico si el archivo lleg� en su totalidad
            case UPLOAD_ERR_PARTIAL: $error_speech = 'file_errorOnlyPartUpload'; break;

            # Verifico si adjunt� archivo alguno
            case UPLOAD_ERR_NO_FILE: $error_speech = 'file_errorNoFileToUpload'; break;

            # Algo que no pasa nunca, pero...
            default: return false;
        }

        # Reporto el error
        return self::addErrorMessage('file',$error_speech);
    }

    /**
     * Lee el archivo CSV con los datos de los productos y los inserta en la DB
     *
     * @static
     * @access protected
     * @return integer cantidad de productos editados y/o agregados
     * @see InnyCore_Producto::insertarProductosDesdeCSV()
     */
    protected static function cargarProductosDesdeCSV($fileName,&$daoListado,$delimiter,$hasheader,$action){

        # Obtengo la cantidad de campos de texto que tiene cada producto
        $texto_cantidad = $daoListado->getMetadataValue('texto','cantidad');

        # Si un listado no tiene textos asociados, retorno que no se agreg� ning�n texto
        if($texto_cantidad == 0){
            return 0;
        }

        # Si no puedo leer el archivo, cargo un error
        if (!$handle = fopen($fileName,'r')) {
            self::addErrorMessage('file','file_errorCannotOpen');
            return 0;
        }

        # En caso que el archivo tenga encabezado, lo ignoro
        if($hasheader){
            $data = fgetcsv($handle,1000,$delimiter);
        }

        # Comienzo a leer el CSV para obtener los productos
        $productos = array();
        while(($data = fgetcsv($handle,1000,$delimiter)) !== false){
            $productos[] = $data;
        }
        fclose($handle);

        # Obtengo el sitio actual
        $daoSitioActual = Inny::getDaoSitioActual();

        # Lineas donde se produjo error
        $error_lineas = array();

        ////////////////////////////////////////////////////////////////////////

        # Obtengo la cantidad de productos que tiene el CSV
        $productos_cantidad = count($productos);

        # Obtengo la cantidad de productos que tiene el listado
        $listado_cantidad = $daoListado->getCantidadProductos();

        # Obtengo la cantidad m�xima de elementos que se pueden editar
        $listado_cantidad_editable = $listado_cantidad;

        # Obtengo si el listado tiene un m�ximo de elementos permitidos
        $listado_parametro_limit = $daoListado->getMetadataValue('parametros','limit');
        $listado_parametro_limit = ($listado_parametro_limit != 'none' && InnyCore_Utils::is_integer($listado_parametro_limit) ? $listado_parametro_limit : null);

        # Verifico si el listado es s�lo para editarse
        $listado_solo_modificacion = ($action == 'update' && !$daoListado->tienePermiso('A') && $daoListado->tienePermiso('M'));

        # Cantidad de productos editados y/o agregados
        $productos_cantidad_agregados = 0;

        # Productos que se agregaron / editaron correctamente
        $productos_cantidad_am_ok = 0;

        ////////////////////////////////////////////////////////////////////////

        # Inserto los elementos en el listado
        for($l = 0; $l < $productos_cantidad; $l++){

            # Obtengo la acci�n que debo realizar en este momento
            $action_actual = ($action == 'insert' || $l >= $listado_cantidad ? 'insert' : 'update');

            # Cantidad de productos actualmente en el listado
            $listado_cantidad_actual = $listado_cantidad + ( $action_actual == 'insert' ? $productos_cantidad_agregados : ( ($l + 1) < $listado_cantidad ? 0 : $productos_cantidad_agregados ) );

            # Actualizo la cache de la cantidad de productos del listado
            $daoListado->setValorEnCache(array('producto_cantidad'),$listado_cantidad_actual);

            # Verifico que no se exceda el l�mite de elementos permitidos
            if(!empty($listado_parametro_limit) && $listado_cantidad_actual >= $listado_parametro_limit){
                self::addErrorMessage('file','file_errorCsvParametroLimiteExcedido',array('%listado_limit'=>$listado_parametro_limit,'%listado_nombre'=>$daoListado->getMetadataValue('nombre')));
                return $productos_cantidad_am_ok;
            }

            # En caso que s�lo se puedan editar los productos del listado
            # Verifico que no se exceda la cantidad de elementos editable
            if($listado_solo_modificacion && ($listado_cantidad_actual > $listado_cantidad)){
                self::addErrorMessage('file','file_errorCsvPermisoModificarExcedido',array('%listado_cantidad'=>$listado_parametro_limit,'%listado_nombre'=>$daoListado->getMetadataValue('nombre')));
                return $productos_cantidad_am_ok;
            }

            # En caso que la acci�n principalmente sea 'update', pero luego se
            # vayan agregando elementos al final del listado
            # caso: si s�lo se tiene permiso de Modificaci�n, y la cantidad de productos
            #       en el CSV supera la actual en el listado
            if($action_actual == 'insert' && !$daoListado->tienePermiso('A')){
                self::addErrorMessage('file','file_errorCsvPermisoAgregar');
                return 0;
            }

            ////////////////////////////////////////////////////////////////////

            # Preparo el arreglo de textos
            $data = array('texto'=>array());
            for($i = 1; $i <= $texto_cantidad; $i++){
                $data['texto']['texto'.$i] = isset($productos[$l][$i-1]) ? $productos[$l][$i-1] : '';
            }

            ////////////////////////////////////////////////////////////////////

            # En caso que haya que insertar un producto
            if($action_actual == 'insert'){
                $am_resultado = self::insertar($daoSitioActual,$data,$daoListado->nombre,null,$debug_backtrace);
                if(!empty($am_resultado)){
                    $productos_cantidad_agregados++;
                }
            }

            # En caso que haya que editar un producto existente
            # Obtengo el producto correspondiente a esa posici�n y lo edito
            else{
                $daoProducto = Inny::getProductoPorPosicion($daoListado->nombre,$l + 1);
                $am_resultado = !empty($daoProducto) && self::editar($daoSitioActual,$data,$daoListado->nombre,$daoProducto->id_producto,null,$debug_backtrace);
            }

            # Si lo edita, incremento el contador de productos
            if(empty($am_resultado)){
                $error_lineas[$l+1] = true;
            }

            # Libero memoria
            unset($data);
        }

        # Libero memoria
        unset($productos);

        # Si en alguna l�nea hay alg�n error, reporto en cuales
        if(count($error_lineas) > 0){
            self::addErrorMessage('file','file_errorCsvField',array('%lineas'=>implode(',',array_keys($error_lineas))));
        }

        # Retorno la cantidad de productos agregados
        return $productos_cantidad_agregados;
    }

    /**
     * Agrega los productos desde un listado CSV
     *
     * @static
     * @access public
     * @return integer cantidad de elementos agregados y/o editados
     */
    public static function insertarProductosDesdeCSV(&$daoListado){

        # Seteo tiempo ilimitado para la ejecucion de la insercion masiva
        set_time_limit(0);

        # Seteo que debe mostrar los mensajes de error
        self::mostrarMensajesDeError();

        # Obtengo el debug_backtrace
        self::setDebugBacktrace(debug_backtrace());

        # En caso que el archivo sea v�lido
        if(self::validarArchivoCSV('csvfile') && !Denko::hasErrorMessages()){

            # Averiguo si debo editar o agregar los productos
            $action = !empty($_POST['rewrite']) ? 'update' : 'insert';

            # Verifico que tenga permisos de modificaci�n de productos
            if($action == 'update' && !$daoListado->tienePermiso('M')){
                self::addErrorMessage('file','file_errorCsvPermisoEditar');
                return 0;
            }

            # Verifico que tenga permisos de agregar nuevos productos
            if($action == 'insert' && !$daoListado->tienePermiso('A')){
                self::addErrorMessage('file','file_errorCsvPermisoAgregar');
                return 0;
            }

            # Cargo los productos del CSV
            $cantidad_am_ok = self::cargarProductosDesdeCSV($_FILES['csvfile']['tmp_name'],$daoListado,';',!empty($_POST['hasheader']),$action);

            # Si est� todo OK, me redirijo al listado y evito las variables en POST
            if(!Denko::hasErrorMessages()){
                Denko::redirect(basename($_SERVER['PHP_SELF']).'?id_listado='.$daoListado->id_listado);
            }
            return $cantidad_am_ok;
        }
    }
}

################################################################################
