<?php
/**
 * Project: Inny CMS
 * File: core.speech.php
 * Purpose: M�dulo para obtener los speechs
 *
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 * @copyright 2007-2009 Dokko Group
 * @package Inny Core
 */

require_once '../libs/Config_File.class.php';

/**
 * M�dulo para obtener los speechs del CMS
 * @package Inny Core
 */
class InnyCore_Speech{

    /**
     *
     *
     */
    public static $idioma_default = 'es_ar';

    /**
     *
     *
     */
    protected static $key_idioma_global = 'INNY_CODIGO_IDIOMA';

    /**
     *
     *
     */
    protected static $config_path = 'configs';

    /**
     *
     *
     */
    public $speech_file;

    /**
     *
     *
     */
    public $config;

    /**
     *
     *
     */
    public function __construct($speech_file=null){
        $this->speech_file = empty($speech_file) ? self::getNombreArchivoSpeechBase() : $speech_file;
        $this->config = new Config_File(self::$config_path);
        $this->config->load_file($this->speech_file);
    }

    /**
     *
     *
     */
    public function getSpeech($section,$key,$replaces=null){
        if(isset($this->config->_config_data[self::$config_path.DIRECTORY_SEPARATOR.$this->speech_file]['sections'][$section]['vars'][$key])){
            $speech = $this->config->_config_data[self::$config_path.DIRECTORY_SEPARATOR.$this->speech_file]['sections'][$section]['vars'][$key];
            if($replaces !== null && is_array($replaces) && count($replaces) > 0){
                foreach($replaces as $key => $value){
                    $speech = str_replace($key,$value,$speech);
                }
            }
            return $speech;
        }
        return null;
    }

    /**
     *
     *
     */
    public static function setCodigoIdioma($codigo_idioma){
        $GLOBALS[self::$key_idioma_global] = $codigo_idioma;
    }

    /**
     *
     *
     */
    public static function getCodigoIdioma(){
        return !empty($GLOBALS[self::$key_idioma_global]) ? $GLOBALS[self::$key_idioma_global] : self::$idioma_default;
    }

    /**
     *
     *
     */
    public static function getNombreArchivoSpeechBase(){
        return 'base.'.self::getCodigoIdioma().'.conf';
    }

    /**
     *
     *
     */
    public static function getNombreArchivoSpeechBaseSitio(&$daoSitio){
        return 'base.'.(!empty($daoSitio) && !empty($daoSitio->idioma_codigo) ? $daoSitio->idioma_codigo : self::$idioma_default).'.conf';
    }
}

################################################################################
#                                                                              #
################################################################################

/**
 *
 *
 */
class InnyCMS_Speech{

    /**
     *
     *
     */
    protected static $innySpeech = null;

    /**
     *
     *
     */
    protected static $daoSitioActual = null;

    /**
     *
     *
     */
    protected static $lenguaje_default = 'es_ar';

    /**
     *
     *
     */
    public static function init(){

        if(empty(self::$daoSitioActual)){

            # Obtengo el sitio actualmente logueado
            self::$daoSitioActual = Inny::getDaoSitioActual();

            # Obtengo el c�digo de idioma
            $codigo_idioma = !empty(self::$daoSitioActual) ? self::$daoSitioActual->idioma_codigo : self::getLenguajeDefault();

            # Seteo el c�digo de idioma por default
            InnyCore_Speech::setCodigoIdioma($codigo_idioma);

            # Creo la instancia del InnySpeech
            self::$innySpeech = new InnyCore_Speech('speech.'.$codigo_idioma.'.conf');
        }
    }

    /**
     *
     *
     */
    public static function getSpeech($seccion,$clave,$reemplazos=null){
        if(empty(self::$innySpeech)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El m�dulo de speechs no ha sido inicializado');
            return null;
        }
        return self::$innySpeech->getSpeech($seccion,$clave,$reemplazos);
    }

    /**
     *
     *
     */
    public static function getLenguajeDefault(){
        return self::$lenguaje_default;
    }

    /**
     *
     *
     */
    public static function config_load(&$smarty,$section){
        self::init();
        $smarty->config_load(self::$innySpeech->speech_file,$section);
    }
}

################################################################################