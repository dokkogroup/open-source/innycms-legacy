<?php
/**
 * InnyType
 *
 * Tipo de dato del InnyCMS
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @copyright Copyright (c) 2009 Dokko Group.
 * @link http://www.dokkogroup.com.ar/
 * @package InnyType
 */
abstract class InnyType{

    /**
     * @var array nombres de los par�metros
     * @access protected
     * @static
     */
    protected static $paramkeys = array('required','multilang');

    /**
     * @var string nombre del tipo de dato
     */
    public $type;

    /**
     * @var mixed valor del tipo de dato
     */
    public $value;

    /**
     * @var array valores de la metadata
     */
    public $metadata;

    /**
     * @var object instancia de Smarty
     */
    public $smarty;

    /**
     * Constructora
     *
     * @access public
     * @return void
     */
    public function __construct(){
        $this->type = null;
        $this->value = null;
        $this->smarty = null;
        $this->metadata = null;
    }

    /**
     * Genera el HTML del input del tipo de dato
     *
     * @param array $params par�metros
     * @abstract
     * @access public
     * @return string
     */
    abstract public function htmlInput($params=array());

    /**
     * Crea una instancia de un tipo de dato
     *
     * @param string $type nombre del tipo de dato
     * @static
     * @access public
     * @return object
     */
    public static function factory($type){
        $type = strtolower(trim($type));
        $required_file = '../commons/inny/type.'.$type.'.php';

        if(!file_exists($required_file)){
            DokkoLogger::log(E_USER_ERROR,'Error: el tipo de dato "'.$type.'" no est� implementado');
            return null;
        }
        require_once $required_file;
        eval('$innyType = new InnyType_'.ucfirst($type).'();');
        return $innyType;
    }

    /**
     * Setea el valor al tipo de dato
     *
     * @param mixed $value valor del tipo de dato
     * @access public
     * @return void
     */
    public function setValue($value){
        $this->value = $value;
    }

    /**
     * Setea el valor al tipo de dato
     *
     * @access public
     * @return mixed
     */
    public function getValue(){
        return $this->value;
    }

    /**
     * Setea la metadata al tipo de dato
     *
     * @param array $metadata metadata en formateada en arreglo
     * @access public
     * @return void
     */
    public function setMetadata($metadata){
        $this->metadata = $metadata;
    }

    /**
     * Asigna la instancia de Smarty
     *
     * @param object &$smarty instancia de Smarty
     * @access public
     * @return void
     */
    public function setSmarty(&$smarty){
        $this->smarty = $smarty;
    }

    /**
     * Obtiene el valor de una configuraci�n seteada en la metadata
     *
     * @param string $key clave de la metadata
     * @access public
     * @return mixed en caso que est� seteado el valor, o NULL si no existe
     */
    public function getMetadataValue(){

        # Obtengo el nro de par�metros
        $num_args = func_num_args();

        # En caso que no haya par�metros, no retorno valor alguno
        if($num_args == 0){
            return null;
        }

        $arg1 = func_get_arg(0);
        if($arg1 == 'parametros'){
            $arg2 = strtolower(trim(func_get_arg(1)));
            return isset($this->metadata['parametros'][$arg2]) ? $this->metadata['parametros'][$arg2] : $this->getParamDefaultValue($arg2);
        }

        # Busco cual deber�a ser el �ndice que debo retornar
        $evalIndex = '';
        for($i = 0 ; $i < $num_args; $i++){
            $evalIndex.='["'.strtolower(trim(func_get_arg($i))).'"]';
        }
        eval('$value = (isset($this->metadata'.$evalIndex.') ? $this->metadata'.$evalIndex.' : null);');
        return $value;
    }

    /**
     * Verifica que el valor del tipo de dato sea v�lido
     *
     * @access public
     * @return boolean
     */
    public function validate($filedata = null){
        return true;
    }

    /**
     * Retorna el todos los par�metros correspondientes al tipo
     *
     * @static
     * @access public
     * @return array
     */
    public static function getParamKeys(){
        return self::$paramkeys;
    }

    /**
     * Retorna el valor por defecto de un par�metro
     *
     * @param string $paramname nombre del par�metro
     * @static
     * @access public
     * @return string
     */
    public static function getParamDefaultValue($paramname){
        switch($paramname){

            # Campo requerido
            case 'required': return 'false';

            # Campo multilenguaje
            case 'multilang': return 'false';

            # En cualquier otro caso
            default: return null;
        }
    }

    /**
     * Obtiene el valor por defecto de un tipo de dato
     *
     * @access public
     * @return string
     */
    public function getDefaultValue(){
        return InnyCMS_Speech::getSpeech('tipo','tipo_default');
    }

}
################################################################################