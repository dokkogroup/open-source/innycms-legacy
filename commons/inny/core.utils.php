<?php
/**
 *
 *
 */
class InnyCore_Utils{

    /**
     * Verifica si una cadena es un n�mero entero v�lido
     *
     * @param string $integer n�mero entero
     * @param string $thousands_sep separaci�n de miles
     * @static
     * @access public
     * @return boolean
     */
    public static function is_integer($integer,$thousands_sep=''){
        $pattern = '/^[-]{0,1}([0-9]+)'.($thousands_sep != '' ? '('.$thousands_sep.'[0-9][0-9][0-9])*' : '').'$/';
        return (preg_match($pattern,$integer) >= 1);
    }

    /**
     * Obtiene el valor entero de una variable
     *
     * @param string $integer n�mero entero
     * @param string $thousands_sep separaci�n de miles
     * @static
     * @access public
     * @return integer
     */
    public static function int_value($integer,$thousands_sep=''){
        return intval(($thousands_sep != '' ? str_replace($thousands_sep,'',$integer) : $integer),10);
    }

    /**
     * Verifica si una cadena es un n�mero entero v�lido
     *
     * @param string $integer n�mero entero
     * @param string $thousands_sep separaci�n de miles
     * @param string $dec_point punto de separaci�n decimal
     * @static
     * @access public
     * @return boolean
     */
    public static function is_float($integer,$thousands_sep='',$dec_point=','){
        $pattern = '/^[-]{0,1}([0-9]+)'.($thousands_sep != '' ? '('.$thousands_sep.'[0-9][0-9][0-9])*' : '').'(['.$dec_point.'][0-9]){0,1}([0-9]*)$/';
        return (preg_match($pattern,$integer) >= 1);
    }

    /**
     * Obtiene el valor flotante de una variable
     *
     * @param string $float n�mero flotante
     * @param string $thousands_sep separaci�n de miles
     * @param string $dec_point punto decimal
     * @static
     * @access public
     * @return integer
     */
    public static function float_value($float,$thousands_sep='',$dec_point=','){
        $value = ($thousands_sep != '' ? str_replace($thousands_sep,'',$float) : $float);
        $value = ($dec_point != '' ? str_replace($dec_point,'.',$value) : $value);
        return floatval($value);
    }

    /**
     * Retorna el contenido limpio del contenido de un WYSIWYG
     *
     * @param string $html contenido de un WYSIWYG
     * @static
     * @access public
     * @return string
     */
    public static function cleanWysiwygContent($html){

        # Elimino los Tags HTML
        $strip_tags = strip_tags($html);

        # Elimino las entidades HTML
        $html_entity_decode = html_entity_decode($strip_tags);

        # Aplico el trim
        return Denko::trim($html_entity_decode);
    }

    /**
     * Convierte los valores de un arreglo en par�metros
     *
     * @param array $array arreglo de par�metros clave-valor
     * @param array $ignore arreglo de par�metros que se ignorar�n
     * @static
     * @access public
     * @return string
     */
    public static function array2params($array,$ignore=array()){
        $params = '';
        foreach($array as $key => $value){
            if(in_array($key,$ignore)){
                continue;
            }
            $params .= $key.'="'.$value.'" ';
        }
        return $params;
    }

    /**
     * Convierte una cadena de texto a min�sculas
     *
     * @param string $string cadena de texto
	 * @param string $charset charset de la cadena de texto
     * @static
     * @access public
     * @return string
     */
    public static function lower($string,$charset='ISO-8859-1'){
        return mb_strtolower($string,$charset);
    }

    /**
     * Convierte una cadena de texto a may�sculas
     *
     * @param string $string cadena de texto
	 * @param string $charset charset de la cadena de texto
     * @static
     * @access public
     * @return string
     */
    public static function upper($string,$charset='ISO-8859-1'){
        return mb_strtoupper($string,$charset);
    }

    /**
     * Convierte los caracteres especiales de una cadena en entidades html
     *
     * @param string $string cadena de texto
	 * @param string $charset charset de la cadena de texto
     * @static
     * @access public
     * @return string
     */
    public static function escape_html($string,$charset='ISO-8859-1'){
        return htmlentities($string,ENT_COMPAT,$charset);
    }

    /**
     * Obtiene el valor de un par�metro GET o POST
     *
     * @param string $param_name nombre del par�metro
     * @static
     * @access public
     * @return mixed string en caso que el el par�metro exista, NULL en caso contrario
     */
    public static function getParamValue($param_name){
        return $_SERVER['REQUEST_METHOD'] == 'POST' ?
            (isset($_POST[$param_name]) ? Denko::trim($_POST[$param_name]) : null) :
            (isset($_GET[$param_name])  ? Denko::trim($_GET[$param_name]) : null);
    }
}
################################################################################
