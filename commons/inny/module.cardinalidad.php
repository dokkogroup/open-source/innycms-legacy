<?php
/**
 * Project: Inny CMS
 * File: module.cardinalidad.php
 * Purpose: M�dulo para manipular relaciones entre los productos
 *
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 * @copyright 2007-2009 Dokko Group
 * @package Inny Modulos
 */

/**
 * M�dulo para manipular relaciones entre los productos
 * @package Inny Modulos
 */
class InnyModule_Cardinal{

    /**
     * @static
     * @var array $metadata_cardinalidades arreglo donde se va agregando la metadata generada
     */
    protected static $metadata_cardinalidades = array();

    /**
     * @static
     * @var string $metadata_section nombre de la secci�n en el archivo de metadata
     */
    public static $metadata_section = 'cardinalidad';

    /**
     * @static
     * @var array $default_settings valores por default de los seteos
     */
    public static $default_settings = array(
        'respetarCadinalidad' => true,
        'sincronizar' => false,
    	'display' => true
    );

    /**
     * @static
     * @var array $settings seteos para la cardinalidad
     */
    public static $settings = null;

    ////////////////////////////////////////////////////////////////////////////
    //                GENERACI�N Y VALIDACI�N DE METADATA                     //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Parsea la metadata correspondiente a las cardinalidades
     *
     * @access public
     * @return boolean si la generaci�n de la metadata no tuvo errores
     */
    public static function parseMetadata($section_name,&$section_values,&$metadata_listado){

    	# En caso que no est� seteada la cardinalidad
        if(!isset($section_values[self::$metadata_section])){
            return true;
        }

        # Genero los prefijos para los mensajes de error
        $nombre_listado = InnyCore_Metadata::extraerNombreListado($section_name);
        $speech_messagePrefixLog = str_replace('%nivel_error',strtoupper(InnyCore_Metadata::getSpeechText('core_common','coreCommon_warning')),InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messagePrefixLog'));
        $speech_messagePrefixListado = str_replace('%nombre_listado',InnyCore_Metadata::extraerNombreListado($section_name),InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messagePrefixListado'));
        $speech_messageConfigName = str_replace('%nombre_configuracion',self::$metadata_section,InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messageConfigName'));

        $prefix_cmsMessage = $speech_messagePrefixListado.' - '.$speech_messageConfigName;
        $prefix_logMessage = $speech_messagePrefixLog;

        # Verifico que el par�metro no est� vac�o
        $cardinalidad_settings = $section_values[self::$metadata_section];
        if($cardinalidad_settings == ''){
            InnyCore_Metadata::reportParseError(E_USER_WARNING,$prefix_logMessage,$prefix_cmsMessage,InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_requiredValue'));
            return false;
        }

        # Parseo los seteos de las cardinalidades
        $parametros = InnyCore_Metadata::params2array($cardinalidad_settings,'['.$section_name.'][cardinalidad]');

        #
        if(count($parametros) > 0){

            $parametros_validos = array();
            $listado_cardinalidades = array();

            # Parseo los parametros de las cardinalidades en el caso que existan 
            if(isset($section_values['cardinalidad_parametros'])){
            	self::_verificarParametro_Parametros($metadata_cardinalidades,$section_values,$section_name,'cardinalidad_parametros','parametros');
            }      
            
            foreach($parametros as $listado => $cardinalidad_limites){

            	$listado = strtolower($listado);
	
                $limites = explode(',',$cardinalidad_limites);

                # Pregenero el mensaje de error para el par�metros actual
                $prefix_paramCmsMessage = $prefix_cmsMessage.' - '.str_replace('%nombre_parametro',$listado,InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messageParam'));

                # Verifico que la cantidad de par�metros sea la correcta
                if(count($limites) != 2){
                    InnyCore_Metadata::reportParseError(E_USER_WARNING,$prefix_logMessage,$prefix_paramCmsMessage,InnyCore_Metadata::getSpeechText('module_cardinal','moduleCardinal_invalidLimitsCount'));
                    continue;
                }

                # Verifico que el l�mite inferior est� seteado
                $limite_inferior = trim($limites[0]);
                if($limite_inferior == ''){
                    InnyCore_Metadata::reportParseError(E_USER_WARNING,$prefix_logMessage,$prefix_paramCmsMessage,InnyCore_Metadata::getSpeechText('module_cardinal','moduleCardinal_requiredInfLimit'));
                    continue;
                }

                # Verifico que el l�mite inferior est� seteado
                $limite_superior = strtoupper(trim($limites[1]));
                if($limite_superior == ''){
                    InnyCore_Metadata::reportParseError(E_USER_WARNING,$prefix_logMessage,$prefix_paramCmsMessage,InnyCore_Metadata::getSpeechText('module_cardinal','moduleCardinal_requiredSupLimit'));
                    continue;
                }

                # Verifico que los valores de los l�mites sean v�lidos
                if(!self::validarCardinalidad($limite_inferior,$limite_superior)){
                    InnyCore_Metadata::reportParseError(E_USER_WARNING,$prefix_logMessage,$prefix_paramCmsMessage,InnyCore_Metadata::getSpeechText('module_cardinal','moduleCardinal_invalidLimits'));
                    continue;
                }
				
                if (isset($metadata_cardinalidades['parametros']['display'])){
                	if (is_array($metadata_cardinalidades['parametros']['display']) && array_key_exists($listado, $metadata_cardinalidades['parametros']['display'])){
                		$display = $metadata_cardinalidades['parametros']['display'][$listado];
                	}
                	else 
                		if (is_array($metadata_cardinalidades['parametros']['display']) && count($metadata_cardinalidades['parametros']['display']) > 0)
                			$display = 'true';
                		else
                			$display = $metadata_cardinalidades['parametros']['display'];
                }
                # Guardo los par�metros v�lidos
                if (isset($display)){
	                $parametros_validos[$listado] = array(
	                    'inf' => $limite_inferior,
	                    'sup' => $limite_superior,
	                    'display' => $display
	                );
                }
				else{
					$parametros_validos[$listado] = array(
	                    'inf' => $limite_inferior,
	                    'sup' => $limite_superior,
						'display' => 'true'
	                );
           		}
                # Guardo las cardinalidades
                $listado_cardinalidades[$listado] = array(
                    'inf' => $limite_inferior,
                    'sup' => $limite_superior
                );                
            }
            
            # En caso que haya par�metros v�lidos, los agrego a la metadata
            if(count($parametros_validos) > 0){

                # Seteo la metadata de la cardinalidad en la metadata del listado
                $metadata_listado[self::$metadata_section] = $parametros_validos;
                if(!isset(self::$metadata_cardinalidades[$nombre_listado])){
                    self::$metadata_cardinalidades[$nombre_listado] = array();
                }

                # Combino los arreglos
                self::$metadata_cardinalidades[$nombre_listado] = array_merge(
                    self::$metadata_cardinalidades[$nombre_listado],
                    $listado_cardinalidades
                );

                # Este control lo hago para agregar s�lo una vez el callback post
                # generaci�n de metadata, y de esa manera que se ejecute solo una vez
                if(count(self::$metadata_cardinalidades) == 1){
                    InnyCore_Metadata::addPostParseCallback('InnyModule_Cardinal::postMetadataCallback()');
                }
            }
        }

        return true;
    }
    
    /**
     * Verifica y parsea los par�metros de una cardinalidad
     *
     * @static
     * @access protected
     * @return boolean
     */
    protected static function _verificarParametro_Parametros(&$parsed_metadata,&$ini_section_values,$ini_section_name,$ini_param_name,$parsed_metadata_key = null){

        $section = '['.$ini_section_name.']['.$ini_param_name.']';
		
		# Verifico que tenga seteado valor alguno
        if(($unparsed_parametros = trim($ini_section_values[$ini_param_name])) === ''){
            self::reportParamValueError(E_USER_WARNING,$section,'metadata_isEmpty',array('%param' => $ini_param_name));
        }

        # Verifico que los par�metros tengan formato v�lido
        else{

            # Parseo los subpar�metros
            $parametros = InnyCore_Metadata::params2array($unparsed_parametros,$section);

            # En caso que todo est� ok, agrego los subpar�metros a la metadata
            if(count($parametros) > 0){
                if($parsed_metadata_key !== null){
                    $parsed_metadata[$parsed_metadata_key] = $parametros;
                }
                else{
                    $parsed_metadata[$ini_param_name] = $parametros;
                }
                return true;
            }
        }

        # En caso que el dato no se ingrese en la metadata, retorno FALSE
        return false;
    }
    
    
    /**
     * Verifica si el l�mite inferior de una cardinalidad es v�lido
     *
     * @param integer $integer n�mero entero
     * @static
     * @access protected
     * @return boolean si el valor es v�lido o no
     */
    protected static function validarLimiteInferior($integer){
        return (InnyCore_Utils::is_integer($integer) && $integer >= 0);
    }

    /**
     * Verifica si el l�mite superior de una cardinalidad es v�lido
     *
     * @param mixed $integer n�mero entero o caracter N
     * @static
     * @access protected
     * @return boolean si el valor es v�lido o no
     */
    protected static function validarLimiteSuperior($integer){
        return (InnyCore_Utils::is_integer($integer) && $integer > 0) || (is_string($integer) && strtoupper($integer) == 'N');
    }

    /**
     * Verifica que los valores de una cardinalidad sean v�lidos
     *
     * @param integer $limite_inferior l�mite inferior de la cardinalidad
     * @param integer $limite_superior l�mite superior de la cardinalidad
     * @static
     * @access protected
     * @return boolean si los valores de una cardinalidad son v�lidos
     */
    protected static function validarCardinalidad($limite_inferior,$limite_superior){
        if(InnyCore_Utils::is_integer($limite_inferior) && InnyCore_Utils::is_integer($limite_superior)){
            return $limite_inferior <= $limite_superior;
        }
        return self::validarLimiteInferior($limite_inferior) && self::validarLimiteSuperior($limite_superior);
    }

    /**
     * Callback que se ejecuta cuando se termina de parsear la metadata general
     *
     * @static
     * @access public
     * @return void
     */
    public static function postMetadataCallback(){
        $daoSitio = DB_DataObject::factory('sitio');
        $daoSitio->get(InnyCore_Metadata::$id_sitio);

        # Verifico que los listados de las cardinalidades existan y pertenezca al sitio
        foreach(self::$metadata_cardinalidades as $listado => $listados){
            foreach($listados as $listado_relacionado => $cardinal_data){

                if(!$daoSitio->perteneceListado($listado_relacionado)){

                    # Elimino el listado de las relaciones
                    unset(self::$metadata_cardinalidades[$listado][$listado_relacionado]);

                    # En caso que el listado ya no tega cardinalidades correctas, lo vac�o
                    if(count(self::$metadata_cardinalidades[$listado]) == 0){
                        unset(self::$metadata_cardinalidades[$listado]);
                    }

                    # TODO: reportar el error
                }
            }
        }

        # Seteo la informaci�n de las cardinalidades de lis listados del sitio
        $daoSitio->setConfig(self::$metadata_section,self::$metadata_cardinalidades);
        $daoSitio->update();
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                 ABM                                    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     *
     */
    public static function setearOpciones($settings){
        self::$settings = $settings;
    }

    /**
     *
     *
     */
    public static function getSetting($key){
        if(!isset(self::$default_settings[$key])){
            return null;
        }
        switch($key){
            case 'respetarCadinalidad':
            case 'sincronizar': return isset(self::$settings[$key]) && is_bool(self::$settings[$key]) ? self::$settings[$key] : self::$default_settings[$key];
            default: return null;
        }
    }

    /**
     *
     *
     */
    public static function actualizarRelacionesEntreProductos(&$daoProducto,$ids_cardinalidad,$sincronizar=false){

        foreach($ids_cardinalidad as $nombre_listado => $ids_categorizados){

            # En caso de sincronizar, elimino las relaciones
            if($sincronizar && isset($ids_categorizados['delete'])){
                foreach($ids_categorizados['delete'] as $id_producto){
                    $daoProducto->insertarEliminarRelacionConProducto('delete',$id_producto);
                }
            }

            # Establezco las nuevas relaciones
            if(isset($ids_categorizados['insert'])){
                foreach($ids_categorizados['insert'] as $id_producto){
                    $daoProducto->insertarEliminarRelacionConProducto('insert',$id_producto);
                }
            }
        }
    }

    /**
     *
     *
     */
    public static function categorizarIdsProducto(&$daoProducto,&$daoListadoCardinal,$ids_producto){

        $analisis = array(
            'insert' => array(),
            'update' => array(),
            'delete' => array()
        );

        if(!InnyCore_Dao::isId($daoProducto->id_producto)){
            $analisis['insert'] = $ids_producto;
        }
        else{
            $daoProductoProducto = $daoProducto->getRelacionesConListado($daoListadoCardinal);
            if($daoProductoProducto == null){
                $analisis['insert'] = $ids_producto;
            }
            else{
                $ids_producto_relacionado = array();
                while($daoProductoProducto->fetch()){
                    $id_producto_relacionado = $daoProducto->id_listado == $daoProductoProducto->id_listado1 ? $daoProductoProducto->id_producto2 : $daoProductoProducto->id_producto1;

                    # En caso que el ID ya pertenezca a una relaci�n ya existente
                    if(in_array($id_producto_relacionado,$ids_producto)){
                        $analisis['update'][] = $id_producto_relacionado;
                    }

                    # En caso que el ID pertenezca en una relaci�n existente,
                    # pero no se tenga en cuenta.
                    # Esto sirve para la sincronizaci�n
                    else{
                        $analisis['delete'][] = $id_producto_relacionado;
                    }
                }

                # Ahora obtengo aquellos IDs que sean nuevos.
                # O sea, que representen nuevas relaciones
                foreach($ids_producto as $id_producto){
                    if(!in_array($id_producto,$analisis['update'])){
                        $analisis['insert'][] = $id_producto;
                    }
                }
            }
        }

        # Retorno el arreglo con los IDs categorizados
        return $analisis;
    }

    /**
     *
     *
     */
    public static function compare($a,$b){
        if(InnyCore_Utils::is_integer($a)){
            return InnyCore_Utils::is_integer($b) ? ( $a > $b ? 1 : ($b > $a ? -1 : 0) ) : -1;
        }
        return InnyCore_Utils::is_integer($b) ? 1 : 0;
    }

    /**
     *
     *
     */
    protected static function _productosRelacionadosEntreListados(&$daoListado1,&$daoListado2,$debug_backtrace=null,$retornarCantidad=false){

        # Verifico que el ID del DAO Listado 1 sea v�lido
        if(!InnyCore_Dao::isId($daoListado1->id_listado)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El listado "'.$daoListado1->nombre.'" a�n no ha sido agregado a la DB',$debug_backtrace);
            return $retornarCantidad ? 0 : null;
        }

        # Verifico que el ID del DAO Listado 2 sea v�lido
        if(!InnyCore_Dao::isId($daoListado2->id_listado)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El listado "'.$daoListado2->nombre.'" a�n no ha sido agregado a la DB',$debug_backtrace);
            return $retornarCantidad ? 0 : null;
        }

        # Seteo el "where add"
        $daoProductoProducto = DB_DataObject::factory('producto_producto');
        if($daoListado1->id_listado <= $daoListado2->id_listado){
            $daoProductoProducto->whereAdd('
                id_listado1 = '.$daoListado1->id_listado.' and id_listado2 = '.$daoListado2->id_listado.'
            ');
        }
        else{
            $daoProductoProducto->whereAdd('
                id_listado1 = '.$daoListado2->id_listado.' and id_listado2 = '.$daoListado1->id_listado.'
            ');
        }

        # Dependiendo de la opci�n $retornarCantidad, retorno el DAO
        $find = $daoProductoProducto->find();
        return $retornarCantidad ? $find : ($find ? $daoProductoProducto : null);
    }

    /**
     *
     *
     */
    public static function getProductosRelacionadosEntreListados(&$daoListado1,&$daoListado2,$debug_backtrace=null){
        return self::_productosRelacionadosEntreListados($daoListado1,$daoListado2,$debug_backtrace,false);
    }

    /**
     *
     *
     */
    public static function getCantidadProductosRelacionadosEntreListados(&$daoListado1,&$daoListado2,$debug_backtrace=null){
        return self::_productosRelacionadosEntreListados($daoListado1,$daoListado2,$debug_backtrace,true);
    }

    /**
     * Retorna las relaciones entre productos de dos listado
     *
     */
    public static function getRelacionesEntreProductos($id_listado1,$id_listado2){
        $daoProductoProducto = DB_DataObject::factory('producto_producto');
        if($id_listado1 <= $id_listado2){
            $daoProductoProducto->id_listado1 = $id_listado1;
            $daoProductoProducto->id_listado2 = $id_listado2;
        }
        else{
            $daoProductoProducto->id_listado1 = $id_listado2;
            $daoProductoProducto->id_listado2 = $id_listado1;
        }
        return $daoProductoProducto->find() ? $daoProductoProducto : null;
    }

    /**
     * Valida los datos de cardinalidad de un DAO Producto
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio
     * @param DataObjects_Producto &$daoProducto DAO Producto
     * @param string $nombre_listado nombre del listado al que pertenece el DAO Producto
     * @param array $data datos de cardinalidad para el DAO Producto
     * @param array $debug_backtrace debug_backtrace de la funci�n principal
     * @static
     * @access public
     * @return boolean resultado de la validaci�n
     */
    public static function validarDatosCardinalidad(&$daoSitio,&$daoProducto,$nombre_listado,$data,$debug_backtrace=null){

        # Variable que determinar� si el insert/update ser� v�lido
        $ids_validos = array();

        # Verifico si est�n seteados los datos de la cardinalidad
        if(isset($data[self::$metadata_section]) && is_array($data[self::$metadata_section])){

            # Obtengo el listado al que pertenece el DAO Producto que quiero agregar
            $daoListadoProductos = $daoSitio->getListadoPorNombre($nombre_listado);

            # ======= Verifico que los IDs de los productos sean v�lidos ========= #
            foreach($data[self::$metadata_section] as $nombre_listado_cardinal => $ids_producto){

                # Verifico que el listado hacia donde tiene cardinalidad tenga nombre v�lido
                $nombre_listado_cardinal = InnyCore_Utils::lower(trim($nombre_listado_cardinal));
                if(empty($nombre_listado_cardinal)){
                    InnyModule_Reporter::core_error(E_USER_WARNING,'El nombre del listado es requerido',$debug_backtrace);
                    continue;
                }

                # Verifico que el listado hacia donde tiene cardinalidad exista y pertenezca al sitio
                $daoListadoCardinal = $daoSitio->getListadoPorNombre($nombre_listado_cardinal);

                if($daoListadoCardinal == null){
                    InnyModule_Reporter::core_error(E_USER_WARNING,'El listado "'.$nombre_listado_cardinal.'" no existe o no pertenece al sitio "'.$daoSitio->nombre_sitio.'"',$debug_backtrace);
                    continue;
                }

                # Verifico que la variable $ids_producto no est� vac�a
                if((is_array($ids_producto) && count($ids_producto) == 0) || (!is_array($ids_producto) && empty($ids_producto))){
                    InnyModule_Reporter::core_error(E_USER_WARNING,'No hay IDS de productos del listado "'.$nombre_listado_cardinal.'" para establecer una relaci�n',$debug_backtrace);
                    continue;
                }

                # Como la variable $ids_producto puede no ser un arreglo, lo
                # convierto en arreglo en caso que no lo sea.
                # Adem�s, en caso que s� sea un arreglo, elimino los IDs que
                # pueden estar repetidos
                $ids_producto = is_array($ids_producto) ? array_unique($ids_producto) : array($ids_producto);

                # De los IDs de producto con los que se quiere establecer una
                # relaci�n, chequeo cuales realmente pertenecen al listado con
                # cual se quiere establecer las relaciones (listado cardinal)
                # Aquellos que pertenezcan, seguir�n formando parte del arreglo $ids_producto
                $ids_producto = $daoListadoCardinal->productosQuePertenecen($ids_producto,$debug_backtrace);

                # En caso que no haya IDs de Producto, no hago nada
                if($ids_producto == null){
                    continue;
                }

                # Categorizo los IDs de los productos del arreglo $ids_producto
                # La categorizaci�n se hace en base a:
                # - cuales son los IDs con los que se establecen nuevas relaciones (insert)
                # - cuales son los IDs con los que se mantienen las relaciones (update)
                # - cuales son los IDs con los que se no se mantienen m�s relaciones (delete)
                $ids_producto_categorizado = self::categorizarIdsProducto($daoProducto,$daoListadoCardinal,$ids_producto);

                # Verifico el
                self::verificarCardinalidadEntreListados($daoListadoProductos,$daoProducto,$daoListadoCardinal,$ids_producto_categorizado,$debug_backtrace);

                # En caso que la cardinalidad est� seteada en el listado donde se
                # agregan los productos, verifico se respete el l�mite superior
                $ids_validos[$nombre_listado_cardinal] = $ids_producto_categorizado;
            }
        }

        # Retorno el resultado de la validaci�n
        return $ids_validos;
    }

    /**
     * Verifica la cardinalidad entre listado
     *
     * @param DataObjects_Listado &$daoListado DAO Listado al que pertenece el producto
     * @param DataObjects_Producto &$daoProducto DAO Producto del listado $daoListado
     * @param DataObjects_Listado &$daoListadoCardinal DAO Listado al que se quiere relacionar $daoProducto
     * @param array &$ids_producto_categorizado IDs Producto del listado $daoListadoCardinal
     * @param array $debug_backtrace debug_backtrace de la funci�n que invoca
     * @static
     * @access public
     * @return boolean resultado de la verificaci�n
     */
    public static function verificarCardinalidadEntreListados(&$daoListadoProductos,&$daoProducto,&$daoListadoCardinal,&$ids_producto_categorizado,$debug_backtrace=null){

        # Variable donde se mantiene el resultado de la validaci�n
        $validacion = true;

        # Si hay que respetar la cardinalidad
        if(self::getSetting('respetarCadinalidad') == true){

            # =============== VERIFICO LOS L�MITES INFERIORES ================ #

            # En caso que se quiera eliminar una relaci�n con un producto
            # Y ese producto no pueda perder la relaci�n por cuesti�n de cardinalidad
            #
            # Ejemplo:
            # El listado "Bateristas" tiene una cardinalidad (1,2) con el listado "Bandas".
            #
            # Se desea desvincular al baterista "Hellhammer" de la banda "Mayhem"
            # Debe chequearse que el baterista siga manteniendo relaci�n
            # con al menos 1 banda (porque lo exige la cardinalidad)
            if(
                # Si hay que sincronizar
                self::getSetting('sincronizar') == true

                # y hay productos que desvincular
                && count($ids_producto_categorizado['delete']) > 0

                # Est� seteado una cardinalidad con el listado actual
                && ($limite_inferior_cardinal = $daoListadoCardinal->getMetadataValue('cardinalidad',$daoListadoProductos->nombre,'inf')) != null

                # Y ese l�mite inferior sea mayor a cero
                && $limite_inferior_cardinal > 0
            ){
                $new_update = $ids_producto_categorizado['update'];
                $new_delete = array();
                foreach($ids_producto_categorizado['delete'] as $id_producto){
                    $daoProductoQueDesvincular = InnyCore_Dao::getDao('producto',$id_producto);
                    $cant_relaciones_futuro = $daoProductoQueDesvincular->getCantidadDeRelacionesConListado($daoListadoProductos) - 1;

                    # Paso el ID del producto del arreglo 'delete' al arreglo 'update'
                    if($cant_relaciones_futuro < $limite_inferior_cardinal){
                        $new_update[] = $id_producto;

                        InnyModule_Reporter::core_error(
                            E_USER_WARNING
                            ,
                            'No puede desvincularse del listado "'.$daoListadoProductos->nombre.'" '.
                            'el producto ID "'.$id_producto.'" del listado "'.$daoListadoCardinal->nombre.'".'.
                            'Motivo: El producto debe mantener al menos '.$limite_inferior_cardinal.
                            'relaciones con el listado "'.$daoListadoProductos->nombre.'"'
                            ,
                            $debug_backtrace
                        );

                        # Indico que la validaci�n resulta incorrecta
                        $validacion = false;
                    }
                    else{
                        $new_delete[] = $id_producto;
                    }
                }
                unset($ids_producto_categorizado['update']);
                $ids_producto_categorizado['update'] = $new_update;
                unset($ids_producto_categorizado['delete']);
                $ids_producto_categorizado['delete'] = $new_delete;
            }

            ////////////////////////////////////////////////////////////////////

            # En caso que se quiera eliminar una relaci�n con un producto
            # Verifico que el listado actual respete la cardinalidad inferior
            #
            # Ejemplo:
            # Al listado "Bandas" tiene una relaci�n con el listado "Guitarrista"
            # de (2,4). Si quiero eliminar la relaci�n con algunos
            # guitarristas, debo chequear que siga teniendo relaci�n con al
            # menos 2 guitarristas (porque as� lo exige la cardinalidad)

            # Obtengo el l�mite inferior que se sete� en el listado actual
            $limite_inferior_actual = $daoListadoProductos->getMetadataValue('cardinalidad',$daoListadoCardinal->nombre,'inf');
            if($limite_inferior_actual != null && $limite_inferior_actual > 0){

                # Calculo
                # - la cantidad de relaciones que tengo con el listado ($cantidad_relaciones_actuales)
                # - la cantidad de relaciones que tendr� con el listado ($cantidad_relaciones_futuras)
                $cantidad_relaciones_actuales = count($ids_producto_categorizado['update']) + count($ids_producto_categorizado['delete']);
                $cantidad_relaciones_futuras =
                      count($ids_producto_categorizado['insert'])
                    + count($ids_producto_categorizado['update'])
                    + (self::getSetting('sincronizar') ? 0 : count($ids_producto_categorizado['delete']));

                # Hago la verificaci�n
                if(

                    # Y ya se haya cumplido la cantidad de relaciones m�nimas
                    $cantidad_relaciones_actuales >= $limite_inferior_actual

                    # Y se quiera decrementar esa cantidad de relaciones por debajo de
                    # lo permitido por el l�mite inferior de la cardinalidad
                    && $cantidad_relaciones_futuras < $limite_inferior_actual

                ){

                    # Reintegro al arreglo 'update' la cantidad de IDs Producto
                    # suficiente para que se respete el l�mite inferior de la cardinalidad
                    #
                    # Primero los reintegro en un arreglo auxiliar, para mostrar
                    # en el log de errores con cuales de los IDs se seguir� manteniendo
                    # relaci�n. Luego, lo mergeo con el subarreglo 'update'
                    $count_actual = count($ids_producto_categorizado['insert']) + count($ids_producto_categorizado['update']);
                    $array_update = array();
                    while(($count_actual + count($array_update)) < $limite_inferior_actual && count($ids_producto_categorizado['delete']) > 0){
                        $array_update[] = array_shift($ids_producto_categorizado['delete']);
                    }

                    # Reporto el error
                    InnyModule_Reporter::core_error(
                        E_USER_WARNING
                        ,
                        'No puede decrementarse la cantidad de productos del listado "'.$daoListadoProductos->nombre.'" '.
                        'relacionados al listado "'.$daoListadoCardinal->nombre.'" a una cantidad de '.
                        $cantidad_relaciones_futuras.' (m�nimo: '.$limite_inferior_actual.'). '.
                        'Los productos ID = {'.implode(',',$array_update) .'} del listado "'.$daoListadoCardinal->nombre.'" '.
                        'seguir�n manteniendo relacion con el listado "'.$daoListadoProductos->nombre.'"'
                        ,
                        $debug_backtrace
                    );

                    # Indico que la validaci�n resulta incorrecta
                    $validacion = false;

                    # Mergeo el arreglo de IDs con el subarreglo 'update'
                    $ids_producto_categorizado['update'] = array_merge($ids_producto_categorizado['update'],$array_update);

                    # Libero memoria
                    unset($array_update);
                }
            }

            # =============== VERIFICO LOS L�MITES SUPERIORES ================ #

            # S�lo deben chequearse que se respeten los l�mites superiores de
            # la cardinalidad siempre y cuando se agreguen nuevas relaciones
            # al producto
            if(count($ids_producto_categorizado['insert']) > 0){

                # Verifico si la cantidad de relaciones que tiene actualmente el producto
                # no sea el m�ximo
                $limite_superior_actual = $daoListadoProductos->getMetadataValue('cardinalidad',$daoListadoCardinal->nombre,'sup');
                if($limite_superior_actual != null && InnyCore_Utils::is_integer($limite_superior_actual)){

                    # - En caso que se desee sincronizar, solo deben tenerse en cuenta
                    #   las relaciones que se mantienen y las nuevas, ya que las que no
                    #   se tienen en cuenta se asume que se eliminar�n
                    # - En caso de no sincronizar, se toman en cuenta
                    #   - las nuevas relaciones
                    #   - las relaciones actuales
                    #   - las relaciones que se omiten
                    $cantidad_relaciones_actuales = (
                        self::getSetting('sincronizar') ?
                            count($ids_producto_categorizado['update'])
                            :
                            $daoProducto->getCantidadDeRelacionesConListado($daoListadoCardinal)
                    );

                    if(self::compare($cantidad_relaciones_actuales,$limite_superior_actual) == 0){
                        InnyModule_Reporter::core_error(
                            E_USER_WARNING
                            ,
                            'No puede relacionarse el producto ID "'.$daoProducto->id_producto.'" '.
                            'del listado "'.$daoListadoProductos->nombre.'" a un producto del '.
                            'listado "'.$daoListadoCardinal->nombre.'" porque alcanz� su m�xima '.
                            'cantidad de relaciones ('.$limite_superior_actual.')'
                            ,
                            $debug_backtrace
                        );
                        unset($ids_producto_categorizado['insert']);
                        $ids_producto_categorizado['insert'] = array();
                        return false;
                    }
                }

                ////////////////////////////////////////////////////////////////

                # Obtengo la cardinalidad seteada en el listado cadinal
                # En caso que exista, verifico que no exceda el l�mite superior de la cardinalidad
                #
                # Verifico bandas por banda que no hayan alcanzado su cupo m�ximo de
                # bateristas Las bandas que puede asignarse este baterista, son apartadas
                # en el arreglo $ids_producto_valido
                $limite_superior_cardinal = $daoListadoCardinal->getMetadataValue('cardinalidad',$daoListadoProductos->nombre,'sup');
                if($limite_superior_cardinal != null && InnyCore_Utils::is_integer($limite_superior_cardinal)){
                    $ids_producto_valido = array();
                    foreach($ids_producto_categorizado['insert'] as $id_producto_cardinal){

                        # Obtengo la cantidad de relaciones que tiene con productos de este listado
                        $daoProductoCardinal = InnyCore_Dao::getDao('producto',$id_producto_cardinal);
                        $cantidadProductosRelacionados = $daoProductoCardinal->getCantidadDeRelacionesConListado($daoListadoProductos);

                        # En caso que la
                        if(self::compare($cantidadProductosRelacionados+1,$limite_superior_cardinal) == 1){
                            InnyModule_Reporter::core_error(E_USER_WARNING,'Al relacionar un producto del listado "'.$daoListadoProductos->nombre.'" al listado "'.$daoListadoCardinal->nombre.'" se excede el l�mite de la cardinalidad (max: '.$limite_superior_cardinal.')',$debug_backtrace);
                            continue;
                        }
                        $ids_producto_valido[] = $id_producto_cardinal;
                    }
                }

                # En caso que no haya cardinalidad seteada, asumo que todos los IDs de
                # productos v�lidos son todos
                #
                # Asumo que una banda puede tener N bateristas
                else{
                    $ids_producto_valido = $ids_producto_categorizado['insert'];
                }

                # En caso que no haya ningun producto con el cual se pueda
                # establecer una relaci�n, lo indico en el arreglo 'insert'
                if(count($ids_producto_valido) == 0){
                    unset($ids_producto_categorizado['insert']);
                    $ids_producto_categorizado['insert'] = array();
                    return false;
                }

                ////////////////////////////////////////////////////////////////

                # Verifico en el listado actual si hay seteada una cardinalidad hacia el listado cardinal
                # En caso que est� seteada, solo es necesario comparar mientras que el
                # l�mite no sea N (es decir, que el l�mite sea un n�mero entero)
                #
                # En caso que haya que asignar un baterista a una banda, me fijo si el
                # baterista alcanz� su m�ximo cupo de bandas
                if($limite_superior_actual != null && InnyCore_Utils::is_integer($limite_superior_actual)){

                    # Obtengo la cantidad total de relaciones que tendr�a estableciendo
                    # las nuevas relaciones
                    $cantidad_relaciones = count($ids_producto_valido) + $cantidad_relaciones_actuales;

                    # En caso que puedan establecerse s�lo algunas relaciones, solo tengo
                    # en cuenta las que pueda relacionar, hasta llegar al m�ximo
                    if(self::compare($cantidad_relaciones,$limite_superior_actual) == 1){
                        $ids_validos = array();
                        while(count($ids_validos) + $cantidad_relaciones_actuales < $limite_superior_actual){
                            $ids_validos[] = array_shift($ids_producto_valido);
                        }
                        InnyModule_Reporter::core_error(
                            E_USER_WARNING
                            ,
                            'Solo pueden relacionarse al listado "'.$daoListadoCardinal->nombre.'"
                            los productos ID = {'.implode(',',$ids_validos).'}
                            (los productos ID = {'.implode(',',$ids_producto_valido).'} quedar�n excluidos)'
                            ,
                            $debug_backtrace
                        );

                        # Indico en el arreglo 'insert' cuales son v�lidos
                        unset($ids_producto_categorizado['insert']);
                        $ids_producto_categorizado['insert'] = $ids_validos;
                        return false;
                    }
                }

                # Retorno el los IDs de los productos que pueden relacionarse
                unset($ids_producto_categorizado['insert']);
                $ids_producto_categorizado['insert'] = $ids_producto_valido;
                return true;
            }

            # ================================================================ #

            # Retorno el resultado de la validaci�n
            return $validacion;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                AJAX                                    //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Compara dos indices de clave "txt" en un arreglo
     *
     * @param array $a arreglo que contiene texto en la clave "txt"
     * @param array $b arreglo que contiene texto en la clave "txt"
     * @static
     * @access protected
     * @return integer resultado de la comparaci�n
     * @see InnyModule_Cardinal::getJsonProductosRelacionados
     */
    protected static function _compareProductosRelacionados($a,$b){
        return strcmp($a['txt'],$b['txt']);
    }

    /**
     * Obtiene informaci�n de los productos relacionados a un producto
     * - pcr: producto con relacion [1: con relacion | 0: sin relacion]
     * - id: ID del producto
     * - txt: texto del producto
     * - pr: puede relacionarse el producto (por cuestiones de cardinalidad)
     * - rel: indica si el producto est� relacionado
     *
     * @param DataObjects_Producto &$daoProducto DAO Producto con el se quiere manipular las relaciones
     * @param DataObjects_Listado &$daoListadoCardinal DAO Listado cardinal al producto
     * @param array $opciones opciones
     * @static
     * @access protected
     * @return string datos de los productos en notaci�n JSON para enviarse por AJAX
     */
    protected static function getJsonProductosRelacionados(&$daoProducto,&$daoListadoCardinal,$opciones=null){

        # Verifico si el listado tiene texto que mostrar
        $texto_cantidad = $daoListadoCardinal->getMetadataValue('texto','cantidad');
        if($texto_cantidad <= 0){
            InnyModule_Reporter::core_error(
                E_USER_WARNING,
                'No pueden generarse los listados de productos relacionados porque '.
                'la cantidad de textos del listado "'.$daoListadoCardinal->nombre.'" es "'.$texto_cantidad.'"'
            );
            return null;
        }

        # Obtengo cual es el texto que debo mostrar, teniendo en cuenta su prioridad
        $texto_prioridad = $daoListadoCardinal->getMetadataValue('texto','prioridad');
        if($texto_prioridad == 'none'){
            InnyModule_Reporter::core_error(
                E_USER_WARNING,
                'No pueden generarse los listados de productos relacionados porque '.
                'el valor "prioridad" para los textos del listado "'.$daoListadoCardinal->nombre.'" es "none"'
            );
            return null;
        }

        # Obtengo el campo de texto de mayor prioridad
        $campo_texto = $daoListadoCardinal->getCampoTextoPrincipal();
        
        $daoListado = $daoProducto->getListado();

        $data_productos = array();
        
        if (!isset($_GET['listado']) || (isset($_GET['listado']) && $_GET['listado'] == 1)){
	        # Obtengo la informaci�n acerca de los productos relacionados
	        $daoProductosConRelacion = $daoProducto->getProductosConRelacion($daoListadoCardinal,$opciones);
	        if($daoProductosConRelacion !== null){
	            while($daoProductosConRelacion->fetch()){
	                $data_productos[] = array(
	                    'pcr' => 1, //producto con relacion
	                    'rel' => 1,
	                    'id'  => $daoProductosConRelacion->id_producto,
	                    'txt' => $daoProductosConRelacion->$campo_texto,
	                    'pr'  => 1,
	                    'pd'  => $daoProductosConRelacion->puedePerderRelacionConListado($daoListado) ? 1 : 0
	                );
	            }
	        }
	    }

        if (!isset($_GET['listado']) || (isset($_GET['listado']) && $_GET['listado'] == 0)){
	        # Obtengo los productos no relacionados
	        $daoProductosSinRelacion = $daoProducto->getProductosSinRelacion($daoListadoCardinal,$opciones);

	        if($daoProductosSinRelacion !== null){
	            while($daoProductosSinRelacion->fetch()){
	                $data_productos[] = array(
	                    'pcr' => 0, //producto sin relacion
	                    'rel' => 0,
	                    'id'  => $daoProductosSinRelacion->id_producto,
	                    'txt' => $daoProductosSinRelacion->$campo_texto,
	                    'pr'  => $daoProductosSinRelacion->puedeRelacionarseConListado($daoListado) ? 1 : 0
	                );
	            }
	        }
		}

        # Ordeno los productos
        usort($data_productos,array('InnyModule_Cardinal','_compareProductosRelacionados'));

        # Codifico a UTF-8 y a notaci�n JSON
        Denko::arrayUtf8Encode($data_productos);
        $json_encode = json_encode($data_productos);

        # Libero memoria
        unset($data_productos);

        # Retorno el JSON
        return $json_encode;
    }

    
    /*
     * 
     * 
     * */
    protected static function getJsonProductos(&$daoProducto,&$daoListadoCardinal,$opciones=null){

        # Verifico si el listado tiene texto que mostrar
        $texto_cantidad = $daoListadoCardinal->getMetadataValue('texto','cantidad');
        if($texto_cantidad <= 0){
            InnyModule_Reporter::core_error(
                E_USER_WARNING,
                'No pueden generarse los listados de productos relacionados porque '.
                'la cantidad de textos del listado "'.$daoListadoCardinal->nombre.'" es "'.$texto_cantidad.'"'
            );
            return null;
        }




        # Obtengo cual es el texto que debo mostrar, teniendo en cuenta su prioridad
        $texto_prioridad = $daoListadoCardinal->getMetadataValue('texto','prioridad');
        if($texto_prioridad == 'none'){
            InnyModule_Reporter::core_error(
                E_USER_WARNING,
                'No pueden generarse los listados de productos relacionados porque '.
                'el valor "prioridad" para los textos del listado "'.$daoListadoCardinal->nombre.'" es "none"'
            );
            return null;
        }

        # Obtengo el campo de texto de mayor prioridad
        $campo_texto = $daoListadoCardinal->getCampoTextoPrincipal();
    
		# Obtengo el listado al que pertenece el producto
        $daoListado = $daoProducto->getListado();

        # Obtengo todos los productos que pertenecen al listado relacionado
        $productos = $daoListadoCardinal->getProductos();
        if ($productos==null){return '0';}
        
        # Obtengo los todas las relaciones que tiene el producto
        $daoProductoRelacionados = $daoProducto->getProductosConRelacion($daoListadoCardinal);
        
        # Armo un arreglo con el id de todas las relaciones que tiene el producto
        $productos_relacionados = array();
        
        if (!empty($daoProductoRelacionados)){
	        while($daoProductoRelacionados->fetch()){
	        	$productos_relacionados[] = $daoProductoRelacionados->id_producto;
			}
        }
        
        # Si esta definido el campo opciones, seteo los valores
		if ($opciones !==null && isset($opciones['limit'])){
        	$productos->limit($opciones['limit']['start'],$opciones['limit']['count']);
        }
        
        if($opciones !== null && isset($opciones['like']) && ($texto_primario = $daoListadoCardinal->getCampoTextoPrincipal()) != 'none'){
            $listado_searchable = $daoListadoCardinal->getMetadataValue('parametros','searchable');
            if($listado_searchable == 'true'){
                unset($productos);
                $query = $opciones['like'];
                $productos = dse_search($query,null,null,$whereAdd);
            }
            else{
                $productos->whereAdd($texto_primario.' like \'%'.$opciones['like'].'%\'');
            }
        }
        if ($productos->find()){        
			while($productos->fetch()){
	        	$data_productos[] = array(
	        	'pcr' => (in_array($productos->id_producto,$productos_relacionados)) ? 1 : 0, //producto con relacion
	            'rel' => (in_array($productos->id_producto,$productos_relacionados)) ? 1 : 0,
	            'id'  => $productos->id_producto,
	            'txt' => $productos->$campo_texto,
	            'pr'  => 1,
	            'pd'  => (!empty($daoProductoRelacionados)) ? $productos->puedePerderRelacionConListado($daoProductoRelacionados) ? 1 : 0 : 1
	            );
			}
        }

        # Ordeno los productos
        usort($data_productos,array('InnyModule_Cardinal','_compareProductosRelacionados'));

        # Codifico a UTF-8 y a notaci�n JSON
        Denko::arrayUtf8Encode($data_productos);
        $json_encode = json_encode($data_productos);

        # Libero memoria
        unset($data_productos);

        # Retorno el JSON
        return $json_encode;
    }

    /**
     * Obtiene datos de los productos de listados cardinales a un producto
     *
     * @static
     * @access public
     * @return string datos de los productos de listados cardinales a un producto en notaci�n JSON
     */
    public static function ajax_getProductosListadoCardinal(){

        $error_prefijo = '[producto_am.php->action:ajax_cardinalListsContents] ';

        # Obtengo el sitio actualmente logueado
        $daoSitioActual = Inny_Clientes::getSitioLogueado();

        # Verifico que exista el par�metro "nombre_listado"
        $nombre_listado_cardinal = isset($_GET['nombre_listado_cardinal']) ? trim($_GET['nombre_listado_cardinal']) : '';

        # En caso que el nombre del listado no est� seteado, reporto el error
        if($nombre_listado_cardinal === ''){
            InnyModule_Reporter::core_error(
                E_USER_WARNING,
                $error_prefijo.'El par�metro GET "nombre_listado_cardinal" es requerido'
            );
            return 'null';
        }

        # Obtengo el listado al que hace referencia el nombre
        $daoListadoCardinal = $daoSitioActual->getListadoPorNombre($nombre_listado_cardinal);

        # En caso que no exista, reporto el error
        if($daoListadoCardinal === null){
            InnyModule_Reporter::core_error(
                E_USER_WARNING,
                $error_prefijo.'No existe listado de nombre "'.$nombre_listado_cardinal.'" en el sitio "'.$daoSitioActual->nombre_sitio.'"'
            );
            return 'null';
        }

        # Verifico que el par�metro "id_listado" exista
        $id_listado = isset($_GET['id_listado']) ? trim($_GET['id_listado']) : '';
        if($id_listado === ''){
            InnyModule_Reporter::core_error(E_USER_WARNING,$error_prefijo.'El par�metro "id_listado" es requerido');
            return 'null';
        }

        # Obtengo el DAO Listado
        $daoListado = $daoSitioActual->getListado($id_listado);
        if(empty($daoListado)){
            return 'null';
        }

        # Verifico que el par�metro "id_producto" est� seteado
        $id_producto = isset($_GET['id_producto']) ? trim($_GET['id_producto']) : '';
        if($id_producto === ''){
            InnyModule_Reporter::core_error(E_USER_WARNING,$error_prefijo.'El par�metro "id_producto" es requerido');
            return 'null';
        }

        # Creo la instancia del DAO Producto
        if($id_producto == 'null'){
            $daoProducto = DB_DataObject::factory('producto');
            $daoProducto->id_listado = $id_listado;
        }

        else{
            $daoProducto = $daoListado->getProducto($id_producto);
            if($daoProducto === null){
                return 'null';
            }
        }

        # Seteo las opciones para la funci�n "self::getJsonProductosRelacionados"
        $opciones = null;
        if(isset($_GET['search']) && ($search = trim(utf8_decode($_GET['search']))) !== ''){
            $opciones = array();
            $opciones['like'] = $search;
        }
        
        if(!empty($_GET['paginador'])){
        	$inicio = ($_GET['paginaActual'] * $_GET['resultsPerPage']) - $_GET['resultsPerPage'];
        	$opciones['limit']['start'] = $inicio;
        	$opciones['limit']['count'] = $_GET['resultsPerPage'];
        }

        # Retorno el JSON correspondiente
        if (isset($_GET['juntarListados'])) return self::getJsonProductos($daoProducto,$daoListadoCardinal,$opciones);
        else return self::getJsonProductosRelacionados($daoProducto,$daoListadoCardinal,$opciones);
    }

    /**
     * Retorna la informaci�n acerca de las relaciones de un producto
     *
     * @static
     * @access public
     * @return string informaci�n acerca de las relaciones de un producto en notaci�n JSON
     */
    public static function ajax_cardinalidadProdInfo(){

        # Obtengo y verifico el producto
        $daoProducto = InnyCore_Dao::getDao('producto',isset($_GET['id_producto']) ? trim($_GET['id_producto']) : null);
        if($daoProducto == null){
            return 'null';
        }

        # Obtengo y verifico el listado cardinal
        $daoListadoCardinal = InnyCore_Dao::getDao('listado',isset($_GET['id_listado_cardinal']) ? trim($_GET['id_listado_cardinal']) : null);
        if($daoListadoCardinal === null){
            return 'null';
        }

        $campo_texto = $daoListadoCardinal->getCampoTextoPrincipal();
        $productos_relacionados = array();
        $daoProductosRelacionados = $daoProducto->getProductosConRelacion($daoListadoCardinal);
        if($daoProductosRelacionados){
            while($daoProductosRelacionados->fetch()){
                $productos_relacionados[] = $daoProductosRelacionados->$campo_texto;
            }
            Denko::arrayUtf8Encode($productos_relacionados);
            return json_encode($productos_relacionados);
        }
        return '[]';
    }
}
################################################################################
?>