<?php
/**
 * Project: Inny CMS
 * File: module.contador.php
 * Purpose: M�dulo para los contadores de los productos
 *
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 * @copyright 2007-2009 Dokko Group
 * @package Inny Modulos
 */

/**
 * M�dulo para los contadores de los productos
 * @package Inny Modulos
 */
class InnyModule_Contador{

    /**
     * @var integer cantidad m�xima de contadores
     * @static
     * @access public
     */
    public static $maxContadores = 5;

    /**
     * @var array valores por defecto de los seteos de los contadores
     * @static
     * @access public
     */
    public static $defaultValues = array(

        # Cantidad de contadores
        'cantidad' => 0,

        # Nombre del contador
        'nombre' => 'contador',

        # Par�metros del contador
        'parametros' => array(

            # Numero de comienzo del contador
            'default' => 0,

            # Cantidad de n�meros que incrementa el contador en cada paso
            'step' => 1
        )
    );

    /**
     * @var array arreglo de acciones de los contadores
     * @static
     * @access public
     */
    public static $actions = array('set','reset','step','stepBack','inc','dec');

    ////////////////////////////////////////////////////////////////////////////
    //                         PARSEO DE METADATA                             //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Parsea la metadata correspondiente a los contadores
     *
     * @param string $section_name nombre de la secci�n en la metadata del sitio
     * @param array &$section_values arreglo con la metadata de esa secci�n
     * @param array &$metadata_listado arreglo con la metadata que se guardar�
     * @static
     * @access public
     * @return void
     */
    public static function parseMetadata($section_name,&$section_values,&$metadata_listado){

        # Obtengo la cantidad de contadores del producto
        $contador_cantidad = isset($section_values['contador_cantidad']) ? trim($section_values['contador_cantidad']) : self::$defaultValues['cantidad'];

        # En caso que la cantidad de contadores sea distinta a cero...
        if($contador_cantidad !== 0){

            $contador_metadata = array();

            # Obtengo el nombre del listado
            $nombre_listado = InnyCore_Metadata::extraerNombreListado($section_name);

            # Armo los prefijos de los mensajes de alerta
            $speech_messagePrefixListado = str_replace('%nombre_listado',InnyCore_Metadata::extraerNombreListado($section_name),InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messagePrefixListado'));
            $speech_messagePrefixLog     = str_replace('%nivel_error',strtoupper(InnyCore_Metadata::getSpeechText('core_common','coreCommon_warning')),InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messagePrefixLog'));

            # Verificaci�n de la configuraci�n "contador_cantidad"
            $contador_cantidad_valida = self::metadata_verificarConfiguracionCantidad($contador_cantidad,'contador_cantidad',$speech_messagePrefixListado,$speech_messagePrefixLog);
            if($contador_cantidad_valida){
                $contador_metadata['cantidad'] = $contador_cantidad;
            }

            # Obtengo el valor de la configuraci�n "contador_nombre"
            if(isset($section_values['contador_nombre'])){
                $contador_nombre = trim($section_values['contador_nombre']);

                # Verifico la configuracion "contador_nombre"
                if(self::metadata_verificarConfiguracionNombre($contador_nombre,'contador_nombre',$speech_messagePrefixListado,$speech_messagePrefixLog)){
                    $contador_metadata['nombre'] = $contador_nombre;
                }
            }

            # Obtengo el valor de la configuraci�n "contador_parametros"
            if(isset($section_values['contador_parametros'])){

                # Valido y obtengo los par�metros
                $contador_parametros = self::metadata_verificarConfiguracionParametros($section_name,'contador_parametros',$section_values['contador_parametros'],$speech_messagePrefixListado,$speech_messagePrefixLog);

                # En caso que hayan par�metros v�lidos
                if(count($contador_parametros) > 0){
                    $contador_metadata['parametros'] = $contador_parametros;
                }

                # Libero memoria
                unset($contador_parametros);
            }

            # Verifico la configuraci�n "nombre" y "par�metros" de cada contador
            if($contador_cantidad_valida){

                for($i = 1; $i <= $contador_cantidad; $i++){

                    $contador_i_metadata = array();

                    # ==== Verifico la configuraci�n "contador_i_nombre" ===== #
                    $contador_nombre_config = 'contador_'.$i.'_nombre';
                    if(isset($section_values[$contador_nombre_config])){
                        $contador_nombre = trim($section_values[$contador_nombre_config]);
                        if(self::metadata_verificarConfiguracionNombre($contador_nombre,$contador_nombre_config,$speech_messagePrefixListado,$speech_messagePrefixLog)){
                            $contador_i_metadata['nombre'] = $contador_nombre;
                        }
                    }

                    # ============== Verifico los par�metros ================= #
                    $contador_parametros_config = 'contador_'.$i.'_parametros';
                    if(isset($section_values[$contador_parametros_config])){

                        # Valido y obtengo los par�metros
                        $contador_parametros = self::metadata_verificarConfiguracionParametros($section_name,$contador_parametros_config,$section_values[$contador_parametros_config],$speech_messagePrefixListado,$speech_messagePrefixLog);

                        # En caso que hayan par�metros v�lidos
                        if(count($contador_parametros) > 0){
                            $contador_i_metadata['parametros'] = $contador_parametros;
                        }

                        # Libero memoria
                        unset($contador_parametros);
                    }

                    # ======================================================== #

                    # En caso que haya metadata que agregar
                    if(count($contador_i_metadata) > 0){
                        $contador_metadata['contador'.$i] = $contador_i_metadata;
                    }

                    # Libero memoria
                    unset($contador_i_metadata);
                }
            }

            # En caso que haya metadata que agregar
            if(count($contador_metadata) > 0){
                $metadata_listado['contador'] = $contador_metadata;
            }

            # Libero memoria
            unset($contador_metadata);
        }
    }

    /**
     * Verifica que la cantidad de contadores sea v�lida
     *
     * @param integer $contador_cantidad cantidad de contadores
     * @param string $config_name nombre de la configuracion en el listado
     * @param string $speech_messagePrefixListado prefijo del mensaje de error en la metadata
     * @param string $speech_messagePrefixLog prefijo del mensaje de error en el log
     * @static
     * @access protected
     * @return boolean si la cantidad de contadores es v�lida
     */
    protected static function metadata_verificarConfiguracionCantidad($contador_cantidad,$config_name,$speech_messagePrefixListado,$speech_messagePrefixLog){

        $speech_messageConfigName = str_replace('%nombre_configuracion',$config_name,InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messageConfigName'));

        # Verifico que tenga valor seteado
        if($contador_cantidad === ''){
            InnyCore_Metadata::reportParseError(
                E_USER_WARNING,
                $speech_messagePrefixLog,
                $speech_messagePrefixListado.' - '.$speech_messageConfigName,
                InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_requiredValue')
            );
            return false;
        }

        # Verifico que el valor sea un entero v�lido
        if(!InnyCore_Utils::is_integer($contador_cantidad)){
            InnyCore_Metadata::reportParseError(
                E_USER_WARNING,
                $speech_messagePrefixLog,
                $speech_messagePrefixListado.' - '.$speech_messageConfigName,
                InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_integerFormatError')
            );
            return false;
        }

        # Verifico que el valor sea un entero positivo
        elseif($contador_cantidad < 0){
            InnyCore_Metadata::reportParseError(
                E_USER_WARNING,
                $speech_messagePrefixLog,
                $speech_messagePrefixListado.' - '.$speech_messageConfigName,
                InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_integerPositiveError')
            );
            return false;
        }

        # Verifico que sea un n�mero entero menor/igual a 5
        elseif($contador_cantidad > self::$maxContadores){
            InnyCore_Metadata::reportParseError(
                E_USER_WARNING,
                $speech_messagePrefixLog,
                $speech_messagePrefixListado.' - '.$speech_messageConfigName,
                str_replace('%integer',self::$maxContadores,InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_integerLessThanError'))
            );
            return false;
        }

        # En caso que la cantidad de contadores sea v�lida, retorno TRUE
        return true;
    }

    /**
     * Verifica que el nombre del contador sea v�lido
     *
     * @param string $nombre valor de la variable
     * @param string $config_name nombre de la variable en la metadata
     * @param string $speech_messagePrefixListado prefijo del mensaje de error en la metadata
     * @param string $speech_messagePrefixLog prefijo del mensaje de error en el log
     * @static
     * @access protected
     * @return boolean resultado de la verificaci�n
     */
    protected static function metadata_verificarConfiguracionNombre($nombre,$config_name,$speech_messagePrefixListado,$speech_messagePrefixLog){

        # Verifico que el nombre asignado no est� vac�o
        if($nombre === ''){
            $speech_messageConfigName = str_replace('%nombre_configuracion',$config_name,InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messageConfigName'));
            InnyCore_Metadata::reportParseError(
                E_USER_WARNING,
                $speech_messagePrefixLog,
                $speech_messagePrefixListado.' - '.$speech_messageConfigName,
                InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_requiredValue')
            );
            return false;
        }

        # En caso que todo este OK
        return true;
    }

    /**
     * Verifica que los par�metros sean v�lidos
     *
     * @param string $section_name nombre de la secci�n en la metadata
     * @param string $nombre_config nombre de la configuraci�n que contiene los par�metros
     * @param string $contador_parametros_string par�metros sin parsear
     * @param string $speech_messagePrefixListado prefijo del mensaje de error en la metadata
     * @param string $speech_messagePrefixLog prefijo del mensaje de error en el log
     * @static
     * @access protected
     * @return array par�metros que se evaluaron como correctos
     */
    protected static function metadata_verificarConfiguracionParametros($section_name,$nombre_config,$contador_parametros_string,$speech_messagePrefixListado,$speech_messagePrefixLog){

        # Parseo los par�metros
        $contador_parametros = InnyCore_Metadata::params2array($contador_parametros_string,'['.$section_name.']['.$nombre_config.']');

        #
        $speech_messageConfigName = str_replace('%nombre_configuracion',$nombre_config,InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messageConfigName'));

        # En caso que se hayan parseado par�metros
        if(count($contador_parametros) > 0){

            # Verifico que el par�metro "default" sea un entero v�lido
            if(isset($contador_parametros['default']) && !InnyCore_Utils::is_integer($contador_parametros['default'])){

                # Elimino el par�metro
                unset($contador_parametros['default']);

                # Obtengo el nombre del par�metro
                $speech_nombreParam = str_replace('%nombre_parametro','default',InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messageParam'));

                # Logueo el mensaje de error
                InnyCore_Metadata::reportParseError(
                    E_USER_WARNING,
                    $speech_messagePrefixLog,
                    $speech_messagePrefixListado.' - '.$speech_messageConfigName.' - '.$speech_nombreParam,
                    InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_integerFormatError')
                );

            }

            # Verifico que el par�metro "step" sea un entero v�lido
            if(isset($contador_parametros['step']) && !InnyCore_Utils::is_integer($contador_parametros['step'])){

                # Elimino el par�metro
                unset($contador_parametros['step']);

                # Obtengo el nombre del par�metro
                $speech_nombreParam = str_replace('%nombre_parametro','step',InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_messageParam'));

                # Logueo el mensaje de error
                InnyCore_Metadata::reportParseError(
                    E_USER_WARNING,
                    $speech_messagePrefixLog,
                    $speech_messagePrefixListado.' - '.$speech_messageConfigName.' - '.$speech_nombreParam,
                    InnyCore_Metadata::getSpeechText('core_metadata','coreMetadata_integerFormatError')
                );
            }
        }

        # Retorno el resultado de la validaci�n
        return $contador_parametros;
    }

    ////////////////////////////////////////////////////////////////////////////
    //                                                                        //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Obtiene de un listado la configuracion referente a un contador
     *
     * @param DataObjects_Listado &$daoListado DAO Listado
     * @param array $parametros_contador configuraciones del contador
     * @static
     * @access public
     * @return mixed valor de la configuraci�n
     */
    public static function getMetadataValue(&$daoListado,$parametros_contador){

        # Verifico que el listado no sea nulo
        if(empty($daoListado)){
            return null;
        }

        $strlen_contador = strlen('contador');
        if(strlen($parametros_contador[0]) > $strlen_contador && substr($parametros_contador[0],0,$strlen_contador) == 'contador'){
            $count_parametros = count($parametros_contador);
            $prox_params = array('"contador"');
            for($i = 1; $i < $count_parametros; $i++){
                $prox_params[] = '"'.$parametros_contador[$i].'"';
            }
            $param_value = null;
            eval('$param_value = $daoListado->getMetadataValue('.implode(',',$prox_params).');');
            if($param_value !== null){
                return $param_value;
            }
        }

        $evalparams = '';
        foreach($parametros_contador as $parametro){
            $parametro = strtolower(trim($parametro));
            if(!empty($parametro)){
                $evalparams.= '["'.trim($parametro).'"]';
            }
        }
        $param_value = null;

        eval('$param_value = isset(self::$defaultValues'.$evalparams.') ? self::$defaultValues'.$evalparams.' : null;');
        return $param_value;
    }

    /**
     * Verifica que un contador sea v�lido
     *
     * @param DataObjects_Listado &$daoListado DAO Listado
     * @param integer $contador_numero n�mero de contador
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @static
     * @access public
     * @return boolean si el contador es v�lido
     */
    public static function esContadorValido(&$daoListado,$contador_numero,$debug_backtrace=null){

        # Obtengo el debug_backtrace
        if($debug_backtrace === null){
            $debug_backtrace = debug_backtrace();
        }

        # Verifico que el n�mero no est� vac�o o sea nulo
        $contador_numero = trim($contador_numero);
        if(empty($contador_numero)){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El numero de contador no debe ser nulo o vac�o',$debug_backtrace);
            return false;
        }

        # Verifico que el n�mero sea un entero v�ildo
        if(!InnyCore_Utils::is_integer($contador_numero)){

            InnyModule_Reporter::core_error(E_USER_WARNING,'El numero del contador no es un entero v�lido',$debug_backtrace);
            return false;
        }

        # Verifico que el contador sea un n�mero positivo
        if($contador_numero <= 0){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El numero del contador debe ser un n�mero positivo mayor a cero',$debug_backtrace);
            return false;
        }

        # Verifico que el listado no sea nulo
        if($daoListado === null){
            InnyModule_Reporter::core_error(E_USER_WARNING,'El DAO Listado es NULL',$debug_backtrace);
            return false;
        }

        # Obtengo la cantidad de contadores seteada para el listado
        $contador_cantidad = $daoListado->getMetadataValue('contador','cantidad');

        # Verifico que el n�mero del contador no exceda la cantidad de contadores del listado
        if($contador_numero > $contador_cantidad){
            InnyModule_Reporter::core_error(E_USER_WARNING,'el n�mero del contador ('.$contador_numero.') no debe exceder la cantidad de contadores ('.$contador_cantidad.') del listado "'.$daoListado->nombre.'"',$debug_backtrace);
            return false;
        }

        # Si pas� todos los chequeos, el n�mero contador es v�lido
        return true;
    }

    /**
     * Verifica que los valores para los contadores sean v�lidos
     *
     * @param DataObjects_Listado &$daoListado DAO Listado
     * @param array $data datos para los contadores
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @static
     * @access public
     * @return boolean
     */
    public static function verificarValores(&$daoListado,$data,$debug_backtrace=null){

        # Variable para la validaci�n
        $validacion = true;

        # Obtengo el debug_backtrace
        $debug_backtrace = ($debug_backtrace === null) ? debug_backtrace() : $debug_backtrace;

        # Obtengo la cantidad de contadores
        $contador_cantidad = $daoListado->getMetadataValue('contador','cantidad');
        if($contador_cantidad > 0 && is_array($data)){
            foreach($data as $contador_numero => $contador_settings){

                # Verifico que el valor sea un contador v�lido
                if(!self::esContadorValido($daoListado,$contador_numero,$debug_backtrace)){
                    $validacion = false;
                }

                # Verifico que los seteos est�n en un arreglo
                if(!is_array($contador_settings)){
                    InnyModule_Reporter::core_error(E_USER_WARNING,'[CONTADOR] Los par�metros del contador deben est�r en un arreglo',$debug_backtrace);
                    $validacion = false;
                }

                # Verifico que el par�metro "action" est� seteado
                elseif(empty($contador_settings['action'])){
                    InnyModule_Reporter::core_error(E_USER_WARNING,'[CONTADOR] El par�metro "action" es requerido',$debug_backtrace);
                    $validacion = false;
                }

                # Verifico que la acci�n sea v�lida
                elseif(!in_array($contador_settings['action'],self::$actions)){
                    InnyModule_Reporter::core_error(E_USER_WARNING,'[CONTADOR] El valor del par�metro "action" no es v�lido',$debug_backtrace);
                    $validacion = false;
                }

                # Verifico que el valor para el contador est� seteado
                elseif($contador_settings['action'] == 'set'){
                    $contador_valor = isset($contador_settings['value']) ? trim($contador_settings['value']) : '';

                    # Verifico que el valor para el contador est� seteado
                    if($contador_valor === ''){
                        InnyModule_Reporter::core_error(E_USER_WARNING,'[CONTADOR] El valor para el contador "'.$contador_numero.'" no debe ser vac�o',$debug_backtrace);
                        $validacion = false;
                    }

                    # Verifico que el valor para el contador sea v�lido
                    elseif(!InnyCore_Utils::is_integer($contador_valor)){
                        InnyModule_Reporter::core_error(E_USER_WARNING,'[CONTADOR] El valor para el contador "'.$contador_numero.'" no es un entero v�lido',$debug_backtrace);
                        $validacion = false;
                    }
                }
            }
        }

        # Retorno la validaci�n
        return $validacion;
    }

    /**
     * Setea los valores de los contadores en un DAO Producto
     *
     * @param DataObjects_Listado &$daoListado DAO Listado al que pertenece el DAO Producto
     * @param DataObjects_Producto &$daoProducto DAO Producto donde se setean los valores
     * @param array $data arreglo de datos, en donde est�n los seteos de los contadores
     * @param array $debug_backtrace debug_backtrace de la funci�n que lo invoca
     * @static
     * @access public
     * @return boolean si pudo setear los valores en el DAO Producto
     */
    public static function setearValoresEnProducto(&$daoListado,&$daoProducto,$data,$debug_backtrace=null){

        # Obtengo el debug_backtrace
        if($debug_backtrace === null){
            $debug_backtrace = debug_backtrace();
        }

        # Obtengo la cantidad de contadores
        $contador_cantidad = $daoListado->getMetadataValue('contador','cantidad');
        if($contador_cantidad > 0 && isset($data) && is_array($data)){

            foreach($data as $contador_numero => $contador_settings){

                # Verifico que el contador exista
                if(is_array($contador_settings) && !empty($contador_settings['action']) && self::esContadorValido($daoListado,$contador_numero,$debug_backtrace)){

                    switch($contador_settings['action']){

                        case 'set':
                        case 'inc':
                        case 'dec':

                            # Verifico que el par�metros 'value' exista
                            if(!isset($contador_settings['value'])){
                                InnyModule_Reporter::core_error(E_USER_WARNING,'El valor del par�metro "value" es requerido para la acci�n "'.$contador_settings['action'].'"',$debug_backtrace);
                            }
                            else{
                                switch($contador_settings['action']){
                                    case 'set':
                                        $daoProducto->contadorSet($contador_numero,$contador_settings['value'],$debug_backtrace);
                                        break;
                                    case 'inc':
                                        $daoProducto->contadorIncrementar($contador_numero,$contador_settings['value'],$debug_backtrace);
                                        break;
                                    case 'dec':
                                        $contador_valor = trim($contador_settings['value']);
                                        $daoProducto->contadorIncrementar($contador_numero,InnyCore_Utils::is_integer($contador_valor) ? $contador_valor * -1 : null,$debug_backtrace);
                                        break;
                                }
                            }
                            break;

                        case 'reset':
                            $daoProducto->contadorReset($contador_numero,$debug_backtrace);
                            break;

                        case 'step':
                            $daoProducto->contadorStep($contador_numero,$debug_backtrace);
                            break;

                        case 'stepBack':
                            $daoProducto->contadorStepBack($contador_numero,$debug_backtrace);
                            break;

                        default:
                            InnyModule_Reporter::core_error(E_USER_WARNING,'El numero del contador no es un entero v�lido',$debug_backtrace);
                            break;
                    }
                }
            }
        }
    }

    /**
     * Verifica que los valores de los contadores tengan formato v�lido
     *
     * @param DataObjects_Listado &$daoListado DAO Listado
     * @param array $contador_valores arreglo con los valores de los contadores
     * @static
     * @access public
     * @return boolean si los valores de los contadores tienen formato v�lido
     */
    public static function verificarValoresDesdeArreglo(&$daoListado,$contador_valores){

        $validacion = true;
        foreach($contador_valores as $contador_numero => $contador_valor){

            # Verifico que el valor este seteado
            $contador_valor = trim($contador_valor);
            if($contador_valor === ''){
                $validacion = false;
                Denko::addErrorMessage(
                    'producto_errorContadorRequiredValue',
                    array('%nombre' => $daoListado->getMetadataValue('contador',$contador_numero,'nombre'))
                );
            }

            # Verifico que el valor sea un entero v�lido
            elseif(!InnyCore_Utils::is_integer($contador_valor)){
                $validacion = false;
                Denko::addErrorMessage(
                    'producto_errorContadorInvalidFormat',
                    array('%nombre' => $daoListado->getMetadataValue('contador',$contador_numero,'nombre'))
                );
            }
        }

        # Retorno el resultado de la validaci�n
        return $validacion;
    }

}
################################################################################
?>