<?php
/**
 * Project: Inny CMS
 * File: module.multilang.php
 * Purpose: M�dulo para seteos de multidioma
 *
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 * @copyright 2007-2009 Dokko Group
 * @package Inny Modulos
 */

/**
 * M�dulo para seteos de multidioma
 * @package Inny Modulos
 */
class InnyModule_Multilang{

    /**
     * @var string prefijo para las carpetas con contenido multilenguaje
     * @static
     */
    public static $files_folder = 'images';

    /**
     * @var string $metadata_section nombre de la secci�n en el archivo de metadata
     * @static
     */
    public static $metadata_section = 'multilang';

    /**
     * @var array $metadata valores por default de la metadata
     * @static
     */
    public static $metadata_settings = array(
        'enabled' => array (
            'type' => 'binary',
            'default' => '0'
        ),
        'languages' => array(
            'type' => 'array',
            'default' => array(
                'en_US' => 'english',
                'es_AR' => 'espa�ol'
            )
        ),
        'default' => 'es_AR'
    );

    /**
     * Parsea y setea en el sitio la metadata correspondiente al multilenguaje
     *
     * @param DataObjects_Sitio &$daoSitio DAO Sitio
     * @param array &$parsed_ini_file metadata
     * @static
     * @access public
     * @return void
     */
    public static function parseMetadata(&$daoSitio,&$parsed_ini_file){

        # En caso que est� seteada metadata para el multilenguaje
        if(isset($parsed_ini_file[self::$metadata_section])){
            $metadata = $parsed_ini_file[self::$metadata_section];

            # Verifico el par�metro "enabled"
            if(isset($metadata['enabled'])){
                $section = '['.self::$metadata_section.'][enabled]';
                $datatype = self::$metadata_settings['enabled']['type'];

                # Verifico que el valor est� seteado
                if(($enabled = trim($metadata['enabled'])) == ''){
                    InnyCore_Metadata::reportRequiredValue($section,$datatype);
                    $enabled = self::$metadata_settings['enabled']['default'];
                }

                # Verifico que el valor sea v�lido
                elseif(!InnyCore_Metadata::validateDataType($datatype,$enabled)){
                    InnyCore_Metadata::reportInvalidValue($section,$datatype);
                    $enabled = self::$metadata_settings['enabled']['default'];
                }
            }
            else{
                $enabled = self::$metadata_settings['enabled']['default'];
            }

            # Verifico los idiomas
            if(isset($metadata['languages'])){
                $section = '['.self::$metadata_section.'][language]';
                $multilang_languages = InnyCore_Metadata::params2array($metadata['languages'],$section);
                if(count($multilang_languages) == 0){
                    $multilang_languages = self::$metadata_settings['languages']['default'];
                }
            }
            else{
                $multilang_languages = self::$metadata_settings['languages']['default'];
            }

            # Verifico el par�metros "default"
            $multilang_default = !empty($metadata['default']) && ($trimmed = trim($metadata['default'])) != '' ? $trimmed : self::$metadata_settings['default'];

            # Seteo la metadata en el Sitio
            $daoSitio->setConfig(
                self::$metadata_section,
                array(
                    'enabled' => $enabled,
                    'languages' => $multilang_languages,
                    'default' => $multilang_default
                )
            );
        }
    }

    /**
     * Retorna la traducci�n de un contenido
     *
     * @static
     * @access public
     * @return string cadena en caso de �xito, vac�o en caso de error
     */
    public static function getTranslation($string,$language_code){

        # En caso que no tenga contenido o no se setee el c�digo de idioma
        if(empty($language_code) || empty($language_code)){
            return '';
        }

        # Decodifico la cadena JSON y verifico que sea v�lida
        $translations = json_decode($string,true);
        if(is_bool($translations) && $translations == false){
            return $string;
        }

        # Decodifico los caracteres que fueron codificados en UTF-8
        Denko::arrayUtf8Decode($translations);

        # Verifico si est� el c�digo de idioma en el arreglo
        # Si no est�, retorno vac�o, sin� la traducci�n
        return isset($translations[$language_code]) && $translations[$language_code] != 'null'? $translations[$language_code] : '';
    }

    /**
     * Retorna la ruta del archivo correspondiente al c�digo de lenguaje
     *
     * @param string $filename nombre de archivo
     * @param string $language_code c�digo de lenguage
     * @static
     * @access public
     * @return string ruta de archivo
     */
    public static function getContentUrlTranslation($filename,$language_code){
        return (empty($filename) || empty($language_code)) ? '' : self::$files_folder.'/'.$language_code.'/'.$filename;
    }
}
################################################################################
?>