<?php
/**
 * Project: Inny CMS
 * File: module.reporter.php
 * Purpose: M�dulo para reporte de errores
 *
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 * @copyright 2007-2009 Dokko Group
 * @package Inny Modulos
 */
require_once '../commons/DokkoLogger.php';

/**
 * M�dulo para reporte de errores
 * @package Inny Modulos
 */
class InnyModule_Reporter{

    /**
     * Callback usado para definir la forma de gestionar errores en tiempo de ejecuci�n
     *
     * @param integer $tipo_error nivel del error generado
     * @param string $mensaje_error mensaje de error
     * @static
     * @access public
     * @return boolean
     */
    public static function error_handler($tipo_error,$mensaje_error){

        # Obtengo el tipo de error
        $mensaje_tipo = 'INNY ';
        switch($tipo_error){
            case E_USER_ERROR: $mensaje_tipo.= 'ERROR:'; break;
            case E_USER_WARNING: $mensaje_tipo.= 'WARNING:'; break;
            case E_USER_NOTICE: $mensaje_tipo.= 'NOTICE:'; break;
        }

        # Obtengo el ID del sitio logueado
        $id_sitio = Inny::getLoggedIdSitio();

        # En caso que haya que reportar un error en un sitio
        if(Inny::haySitioLogueado()){

            # El error siempre los logueo
            $id_sitio = Inny::getActualSitio();
            $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);
            DokkoLogger::log($tipo_error,$mensaje_tipo.' '.$mensaje_error,null,null,$id_sitio);

            # Verifico si debo mostrar el error en pantalla
            if($daoSitio->log_errors == '0'){
                echo '<b>'.$mensaje_tipo.'</b> '.$mensaje_error."<br />\n";
            }
        }

        # En caso que haya que reportar un error en el CMS
        else{
            DokkoLogger::log($tipo_error,$mensaje_tipo.' '.$mensaje_error,null,null,$id_sitio);
        }

        # Verifico si debo cortar la ejecuci�n del script
        if($tipo_error == E_USER_ERROR){
            exit(E_USER_ERROR);
        }
        return true;
    }

    /**
     * Muestra un error de core del Inny
     *
     * @param integer $tipo_error [E_USER_ERROR,E_USER_WARNING,E_USER_NOTICE]
     * @param string $mensaje_error mensaje de error
     * @param string $nombre_function nombre de la funci�n que lanza el error
     * @static
     * @access public
     * @return void
     */
    public static function core_error($tipo_error,$mensaje_error,$debug_backtrace=null){
        set_error_handler(array('InnyModule_Reporter','error_handler'));
        trigger_error(
            $mensaje_error.($debug_backtrace != null ? ' [function: '.$debug_backtrace[0]['class'].$debug_backtrace[0]['type'].$debug_backtrace[0]['function'].']' :''),
            $tipo_error
        );
        restore_error_handler();
    }

    /**
     * Muestra un error de API del Inny
     *
     * @param integer $tipo_error [E_USER_ERROR,E_USER_WARNING,E_USER_NOTICE]
     * @param string $mensaje_error mensaje de error
     * @param string $nombre_function nombre de la funci�n que lanza el error
     * @static
     * @access public
     * @return void
     */
    public static function api_error($tipo_error,$mensaje_error,$debug_backtrace){

        set_error_handler(array('InnyModule_Reporter','error_handler'));
        trigger_error(
            $mensaje_error.' [API function: '.$debug_backtrace[0]['class'].$debug_backtrace[0]['type'].$debug_backtrace[0]['function'].']',
            $tipo_error
        );
        restore_error_handler();
    }

    /**
     * Muestra un error de plugin Smarty del Inny
     *
     * @param integer $tipo_error [E_USER_ERROR,E_USER_WARNING,E_USER_NOTICE]
     * @param string $mensaje_error mensaje de error
     * @param string $plugin_name nombre del plugin Smarty
     * @static
     * @access public
     * @return void
     */
    public static function plugin_error($tipo_error,$mensaje_error,$plugin_name=null){
        set_error_handler(array('InnyModule_Reporter','error_handler'));
        trigger_error($mensaje_error.($plugin_name != null ? ' [plugin '.$plugin_name.']' : ''),$tipo_error);
        restore_error_handler();
    }
}
################################################################################