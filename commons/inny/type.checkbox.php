<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 *
 *
 */
class InnyType_Checkbox extends InnyTypeText{

    /**
     * @var array nombres de los par�metros
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     * Constructora
     *
     * @access public
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'checkbox';
    }

    /**
     * Retorna el valor del checkbox
     *
     * @access public
     * @return integer 1 en caso de estar checkeado, 0 en caso contrario
     */
    public function getValue(){
        return $this->is_checked() ? '1' : '0';
    }

    /**
     * Setea el valor del checkbox
     *
     * @param mixed valor del checkbox
     * @access public
     * @return void
     */
    public function setValue($value){
        $this->value = isset($value) ? (($value === 'on' || $value === 1 || $value === '1') ? '1' : '0') : ($this->getMetadataValue('parametros','checked') === 'true' ? '1' : '0');
    }

    /**
     * Retorna los par�metros correspondientes al checkbox
     *
     * @access public
     * @return array par�metros correspondientes al checkbox
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(parent::getParamKeys(),array('checked'));
        }
        return self::$paramkeys;
    }

    /**
     * Retorna el valor por defecto de un par�metro
     *
     * @param string $paramname nombre del par�metro
     * @access public
     * @return string valor por defecto del par�metro
     */
    public static function getParamDefaultValue($paramname){

        switch($paramname){

            # Checkeado
            case 'checked': return 'false';

            # Default
            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     * Retorna el valor trimmeado
     *
     * @access public
     * @return string valor trimeado
     */
    public function trimmed(){
        return $this->getValue();
    }

    /**
     * Indica si el contenido est� vac�o
     *
     * @access public
     * @return boolean FALSE, ya que el contenido nunca est� vac�o
     */
    public function is_empty(){
        return false;
    }

    /**
     * Retorna el texto que debe guardarse en DB
     *
     * @access public
     * @return string 1 en caso de estar checkeado, 0 en caso contrario
     */
    public function postdata2dbdata(){
        return $this->getValue();
    }

    /**
     * Retorna el c�digo HTML correspondiente al input del checkbox
     *
     * @param array $params par�metros para el input
     * @access public
     * @return string HTML input checkbox
     */
    public function htmlInput($params=array()){
        return '<input type="checkbox" name="'.$params['name'].'" '.($this->is_checked() ? 'checked="checked"' : '').' />';
    }

    /**
     * Preview del valor del checkbox
     *
     * @param array $params par�metros
     * @access public
     * @return string texto correspondiente a si est� checkeado o no el checkbox
     */
    public function preview($params=array()){
        return InnyCMS_Speech::getSpeech('tipo','tipo_checkbox'.($this->is_checked() ? 'Checked' : 'Unchecked'));
    }

    /**
     * Versi�n normal del preview del checkbox
     *
     * @access public
     * @return string versi�n normal del preview del checkbox
     */
    public function preview_normal(){
        return $this->preview();
    }

    /**
     * Versi�n resumida del preview del checkbox
     *
     * @param integer $length cantidad de caracteres que debe tener el resumen
     * @access public
     * @return string resumen del preview del checkbox
     */
    public function preview_summary($length){
        return $this->preview();
    }

    /**
     * Indica si el checkbox est� checkeado
     *
     * @access public
     * @return boolean si el checkbox est� checkeado
     */
    public function is_checked(){
        return isset($this->value) ? ($this->value === 'on' || $this->value === 1 || $this->value === '1') : ($this->getMetadataValue('parametros','checked') == 'true');
    }
}
################################################################################