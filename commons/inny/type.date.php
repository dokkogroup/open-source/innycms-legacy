<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 *
 *
 */
class InnyType_Date extends InnyTypeText{

    /**
     * @var array nombres de los parámetros
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'date';
    }

    /**
     *
     *
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(parent::getParamKeys(),array('format'));
        }
        return self::$paramkeys;
    }

    /**
     *
     *
     */
    public static function getParamDefaultValue($paramname){

        switch($paramname){

            # Formato de Fecha
            case 'format': return '%d-%m-%Y';

            # En cualquier otro caso
            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     *
     *
     */
    public function validate($filedata = null){
        $strptime = Fecha::dk_strptime(Denko::trim($this->value),$this->getMetadataValue('parametros','format'));
        return is_array($strptime);
    }

    /**
     *
     *
     */
    public function htmlInput($params=array()){

        #
        $name = $params['name'];
        $value = $params['value'];
        $format = $this->getMetadataValue('parametros','format');

        # Seteo el ID y Nombre del trigger
        $jsc_trigger = 'jsc_trigger_'.$name;

        # Agrego los includes
        require_once $this->smarty->_get_plugin_filepath('function','dk_include');
        require_once $this->smarty->_get_plugin_filepath('function','dhtml_calendar');
        smarty_function_dk_include(array('file'=>'jscalendar/skins/aqua/theme.css','ignoreVersion'=>'true','inline'=>'false'),$this->smarty);
        smarty_function_dk_include(array('file'=>'jscalendar/calendar.js','ignoreVersion'=>'true','inline'=>'false'),$this->smarty);
        smarty_function_dk_include(array('file'=>'jscalendar/lang/calendar-es.js','ignoreVersion'=>'true','inline'=>'false'),$this->smarty);
        smarty_function_dk_include(array('file'=>'jscalendar/calendar-setup.js','ignoreVersion'=>'true','inline'=>'false'),$this->smarty);

        # Retorno el HTML
        return '<input class="abmInput date" type="text" id="'.$name.'" name="'.$name.'" value="'.$value.'" />&nbsp;<input type="button" id="'.$jsc_trigger.'" name="'.$jsc_trigger.'" value="'.InnyCMS_Speech::getSpeech('tipo','tipo_dateCambiarFecha').'" class="boton normal" />'.
        smarty_function_dhtml_calendar(array('ifFormat'=>$format,'inputField'=>$name,'singleClick'=>true,'button'=>$jsc_trigger),$this->smarty);
    }
}
################################################################################