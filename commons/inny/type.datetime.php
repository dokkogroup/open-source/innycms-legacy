<?php
/**
 *
 *
 */
require_once '../commons/inny/type.date.php';

/**
 *
 *
 */
class InnyType_Datetime extends InnyTypeText{

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'datetime';
    }

    /**
     *
     *
     */
    public static function getParamKeys(){
        return InnyType_Date::getParamKeys();
    }

    /**
     *
     *
     */
    public static function getParamDefaultValue($paramname){

        switch($paramname){

            # Formato de Fecha
            case 'format': return '%d-%m-%Y %H:%M:%S';

            # En cualquier otro caso
            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     *
     *
     */
    public function validate($filedata = null){
        $strptime = Fecha::dk_strptime(Denko::trim($this->value),$this->getMetadataValue('parametros','format'));
        return is_array($strptime);
    }

}
################################################################################
?>