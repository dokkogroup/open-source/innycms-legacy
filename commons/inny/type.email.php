<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 *
 *
 */
class InnyType_Email extends InnyTypeText{

    /**
     * @var array nombres de los par�metros
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'email';
    }

    /**
     *
     *
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(parent::getParamKeys(),array('pattern','multiple'));
        }
        return self::$paramkeys;
    }

    /**
     *
     *
     */
    public static function getParamDefaultValue($paramname){

        switch($paramname){

            # Patron para el formato de email
            case 'pattern': return '/^[A-Z0-9._%\-]+@[A-Z0-9._%\-]+\.[A-Z]{2,4}$/i';

            # Indica si podr�n agregarse m�s de un email
            case 'multiple': return 'false';

            # En otro caso
            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     *
     *
     */
    public function validate($filedata = null){

        # Obtengo el patr�n de expresi�n regular
        $email_pattern = $this->getMetadataValue('parametros','pattern');

        # En caso que sean m�ltiples emails
        if($this->getMetadataValue('parametros','multiple') == 'true'){
            $emails = explode(',',$this->value);
            foreach($emails as $email){
                $trimmedEmail = Denko::trim($email);
                if($trimmedEmail != '' && !preg_match($email_pattern,$trimmedEmail)){
                    return false;
                }
            }
            return true;
        }

        # En caso que sea simplemente un email
        return (preg_match($email_pattern,Denko::trim($this->value)) >= 1);
    }
}
################################################################################
?>