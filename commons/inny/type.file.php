<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.file.php';
require_once '../commons/inny/type.image.php';

/**
 *
 *
 */
class InnyType_File extends InnyTypeFile{

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'file';
    }

    /**
     *
     *
     */
    public static function getContentUrl($id_temporal,$filename,$params=array()){
        return 'download/'.$id_temporal.'/'.$filename;
    }

    /**
     * Retorna todos los nombres de parámetros correspondientes al tipo "file"
     *
     * @static
     * @access public
     * @return array nombres de parámetros correspondientes al tipo "image"
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(

                # Parámetros heredados
                parent::getParamKeys(),

                # Parámetros del tipo imagen
                InnyType_Image::$paramkeysImage
            );
        }
        return self::$paramkeys;
    }

    /**
     *
     *
     */
    protected function _insertFile($filedata){

        # [UPGRADE] Verifico si el tipo de archivo es una imagen
        $mime = InnyCore_Image::getImageMimeFromFileContent($filedata['tmp_name']);
        if($mime && in_array($mime,InnyCore_Image::getAllowedImageMime())){
            $innyTypeImage = new InnyType_Image();
            $metadata = $this->metadata;
            
            # Si no esta definido el ancho ni el alto de la imagen, no se hacer el resize
            if (!$this->getMetadataValue('parametros','width') && !$this->getMetadataValue('parametros','height'))
				$metadata['parametros']['resize'] = 'false';
            		
            $innyTypeImage->setMetadata($metadata);
            return $innyTypeImage->_insertFile($filedata);
        }

        # En caso que sea un archivo normal
        return parent::_insertFile($filedata);
    }
}
################################################################################