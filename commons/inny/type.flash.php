<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.file.php';
require_once '../swfheader/swfheader.class.php';

/**
 *
 *
 */
class InnyType_Flash extends InnyTypeFile{

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'flash';
    }

    /**
     * validateFile
     * Verifico que sea un flash v�lido
     *
     * @link http://ar.php.net/features.file-upload.errors
     * @link http://www.phpclasses.org/browse/package/1653.html
     */
    public function validate($filedata = null){
        $validate = parent::validate($filedata);
        if($validate == UPLOAD_ERR_OK){
            $swfheader = new swfheader();
            if(!$swfheader->loadswf($filedata['tmp_name'])){
                return UPLOAD_ERR_EXTENSION;
            }
        }
        return $validate;
    }

    /**
     *
     *
     */
    public static function getContentUrl($id_temporal,$filename,$params=array()){
        return 'movie/'.$id_temporal.'/'.$filename;
    }

    /**
     *
     *
     */
    public function getViewLinks($id_temporal,$filename,$params=array()){
        return array_merge(
            parent::getViewLinks($id_temporal,$filename),
            array('url_flash' => $this->getContentUrl($id_temporal,$filename))
        );
    }

    /**
     *
     *
     */
    public function getDefaultValue(){
        return InnyCMS_Speech::getSpeech('tipo','tipo_defaultFlash');
    }

}
################################################################################