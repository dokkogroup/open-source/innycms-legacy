<?php
/**
 *
 *
 */
require_once '../commons/inny/type.integer.php';

/**
 *
 *
 */
class InnyType_Float extends InnyTypeText{

    /**
     * @var array nombres de los par�metros
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'float';
    }

    /**
     *
     *
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(InnyType_Integer::getParamKeys(),array('dec_point'));
        }
        return self::$paramkeys;
    }

    /**
     *
     *
     */
    public static function getParamDefaultValue($paramname){

        switch($paramname){

            # Caracter de separaci�n de decimales
            case 'dec_point': return ',';

            # En otro caso
            default: InnyType_Integer::getParamDefaultValue($paramname);
        }
    }

    /**
     *
     *
     */
    public function validate($filedata = null){

        # Obtengo el valor trimmeado
        $data = Denko::trim($this->value);

        # Obtengo la separaci�n de miles
        $thousands_sep = $this->getMetadataValue('parametros','thousands_sep');

        # Obtengo el punto decimal
        $dec_point = $this->getMetadataValue('parametros','dec_point');

        # Verifico que sea un entero v�lido
        if(!InnyCore_Utils::is_float($data,$thousands_sep,$dec_point)){
            return false;
        }

        # Verifico los si est� dentro de los rangos seteados
        $min = $this->getMetadataValue('parametros','min');
        $max = $this->getMetadataValue('parametros','max');
        if($min != '' || $max != ''){

            # Convierto a n�mero flotante el valor del dato
            $float_value = InnyCore_Utils::float_value($data,$thousands_sep,$dec_point);

            # Verifico si tiene la restricci�n de respetar un m�nimo
            if($min != '' && (InnyCore_Utils::float_value($min,$thousands_sep,$dec_point) > $float_value)){
                return false;
            }

            # Verifico si tiene la restricci�n de respetar un m�ximo
            if($max != '' && InnyCore_Utils::float_value($max,$thousands_sep,$dec_point) < $float_value){
                return false;
            }
        }

        # En caso que todo est� OK
        return true;
    }

}
################################################################################
?>