<?php
/**
 * Tipo de dato para archivos del tipo imagen
 *
 * @package InnyType
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @copyright Copyright (c) 2007-2009 Dokko Group.
 * @link http://www.dokkogroup.com.ar/
 */

/**
 * Archivos necesarios
 * @ignore
 */
require_once '../commons/inny/type.parent.file.php';
require_once '../commons/inny/core.image.php';

/**
 * @package InnyType
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 */
class InnyType_Image extends InnyTypeFile{

    /**
     * @var array nombres todos los parámetros válidos para el tipo imagen
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     * @var array nombres de los parámetros exclusivos del tipo imagen
     * @static
     * @access public
     */
    public static $paramkeysImage = array('width','height','quality','resize');

    /**
     * Constructora
     *
     * @access public
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'image';
    }

    /**
     * Retorna todos los nombres de parámetros correspondientes al tipo "image"
     *
     * @static
     * @access public
     * @return array nombres de parámetros correspondientes al tipo "image"
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(parent::getParamKeys(),self::$paramkeysImage);
        }
        return self::$paramkeys;
    }

    /**
     *
     *
     */
    public static function getParamDefaultValue($paramname){

        switch($paramname){

            # Ancho de la imagen
            case 'width': return 200;

            # Alto de la imagen
            case 'height': return 200;

            # Calidad de compresión
            case 'quality': return 90;

            # Indica si el archivo debe redimensionarse
            case 'resize': return 'true';

            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     * Verifico que el mime de la imagen sea válido y soportado por el CMS
     * validateFile
     *
     * @link http://ar.php.net/features.file-upload.errors
     */
    public function validate($filedata = null){
        $validate = parent::validate($filedata);
        if($validate == UPLOAD_ERR_OK){
            $mime = InnyCore_Image::getImageMimeFromFileContent($filedata['tmp_name']);
            if(!$mime || !in_array($mime,InnyCore_Image::getAllowedImageMime())){
                return UPLOAD_ERR_EXTENSION;
            }
        }
        return $validate;
    }

    /**
     *
     *
     */
    public static function getContentUrl($id_temporal,$filename,$params=array()){

        # En caso que sea un thumb
        if(!empty($params['thumb']) || (!empty($params['type']) && $params['type'] == 'thumb') || (!empty($params['type']) && $params['type'] == 'crop')){

            # Obtengo las dimensiones de la imagen
        	$thumbValues = InnyCore_Image::getThumbDefaultValues(
        	   !empty($params['width']) ? $params['width'] : null,
        	   !empty($params['height']) ? $params['height'] : null,
        	   !empty($params['quality']) ? $params['quality'] : null
            );

            # Retorno la url del thumb
            return 'thumb/'.$id_temporal.((!empty($params['type']) && $params['type'] == 'crop')?'-c':'').'/'.$thumbValues['width'].'-'.$thumbValues['height'].'-'.$thumbValues['quality'].'/'.$filename;
        }

        # En caso que se muestre la imagen de manera normal
        return 'image/'.$id_temporal.'/'.$filename;
    }

    /**
     *
     *
     */
    protected function _insertFile($filedata){

        # En caso que haya que agregar la imagen redimensionada
        if($this->getMetadataValue('parametros','resize') == 'true'){
            return DFM::set(
                Denko::createImage(file_get_contents($filedata['tmp_name']),
                    $this->getMetadataValue('parametros','width'),
                    $this->getMetadataValue('parametros','height'),
                    $this->getMetadataValue('parametros','quality'),
                    $filedata['type']
                )
            );
        }

        # Si no hay que dimensionar, agrego la imagen
        return parent::_insertFile($filedata);
    }

    /**
     *
     *
     */
    public function getViewLinks($id_temporal,$filename,$params=array()){
        $params['thumb'] = true;
        return array_merge(
            parent::getViewLinks($id_temporal,$filename),
            array(
                'url_image' => $this->getContentUrl($id_temporal,$filename),
                'url_thumb' => $this->getContentUrl($id_temporal,$filename,$params)
            )
        );
    }

    /**
     *
     *
     */
    public function getDefaultValue(){
        return InnyCMS_Speech::getSpeech('tipo','tipo_defaultImage');
    }
}
################################################################################
