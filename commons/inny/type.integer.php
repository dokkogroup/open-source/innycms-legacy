<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 *
 *
 */
class InnyType_Integer extends InnyTypeText{

    /**
     * @var array nombres de los par�metros
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'integer';
    }

    /**
     *
     *
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(parent::getParamKeys(),array('min','max','thousands_sep'));
        }
        return self::$paramkeys;
    }

    /**
     *
     *
     */
    public static function getParamDefaultValue($paramname){

        switch($paramname){

            # M�nimo
            case 'min': return '';

            # M�ximo
            case 'max': return '';

            # Separador de miles
            case 'thousands_sep': return '';

            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     *
     *
     */
    public function validate($validate = null){

        # Obtengo el valor trimmeado
        $data = Denko::trim($this->value);

        # Obtengo la separaci�n de miles
        $thousands_sep = $this->getMetadataValue('parametros','thousands_sep');

        # Verifico que sea un entero v�lido
        if(!InnyCore_Utils::is_integer($data,$thousands_sep)){
            return false;
        }

        # Verifico los si est� dentro de los rangos seteados
        $min = $this->getMetadataValue('parametros','min');
        $max = $this->getMetadataValue('parametros','max');
        if($min != '' || $max != ''){
            $int_value = InnyCore_Utils::int_value($data,$thousands_sep);
            if($min != '' && (InnyCore_Utils::int_value($min,$thousands_sep) > $int_value)){
                return false;
            }
            if($max != '' && InnyCore_Utils::int_value($max,$thousands_sep) < $int_value){
                return false;
            }
        }

        # En caso que todo est� OK
        return true;
    }

}
################################################################################
?>