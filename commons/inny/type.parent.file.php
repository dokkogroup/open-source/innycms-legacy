<?php
/**
 * Tipo de dato para archivos
 *
 * @package InnyType
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @copyright Copyright (c) 2007-2009 Dokko Group.
 * @link http://www.dokkogroup.com.ar/
 */

/**
 * Archivos necesarios
 * @ignore
 */
require_once '../commons/inny/core.type.php';

/**
 * @package InnyType
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @abstract
 */
abstract class InnyTypeFile extends InnyType{

    /**
     * @var array nombres de los par�metros
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     * Retorna todos los nombres de par�metros correspondientes al tipo "archivo"
     *
     * @static
     * @access public
     * @return array nombres de par�metros correspondientes al tipo archivo
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(parent::getParamKeys(),array('description'));
        }
        return self::$paramkeys;
    }

    /**
     * Retorna el valor por defecto de un par�metro
     *
     * @param string $paramname nombre del par�metro
     * @static
     * @access public
     * @return string valor por defecto del par�metro
     */
    public static function getParamDefaultValue($paramname){
        switch($paramname){

            # Descripci�n adjunta a un archivo
            case 'description': return 'none';

            # En cualquier otro caso...
            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     * Verifica si el archivo adjunto es v�lido
     *
     * @param array $filedata datos del upload (elemento del arreglo $_FILES)
     * @access public
     * @return integer
     */
    public function validate($filedata = null){

        # En caso que hayan adjuntado archivo alguno
        if(empty($filedata['tmp_name'])){
            return UPLOAD_ERR_NO_FILE;
        }

        # En caso que no haya error, verifico si el tama�o del archivo es v�lido
        if($filedata['error'] == UPLOAD_ERR_OK && $filedata['size'] == 0){
            return UPLOAD_ERR_NO_FILE;
        }

        # Retorno el c�digo de error
        return $filedata['error'];
    }

    /**
     * Retorna el HTML input
     *
     * @param array $params par�metros extra para el html
     * @access public
     * @return string
     */
    public function htmlInput($params=array()){
        return '<input name="'.$params['name'].'" type="file" />';
    }

    /**
     * Retorna la URL para visualizar el contenido del dato
     *
     * @param integer $id_temporal id del archivo en la tabla temporal
     * @param string $filename nombre del archivo
     * @param array $params par�metros extra
     * @abstract
     * @static
     * @access public
     * @return string
     */
    public static function getContentUrl($id_temporal,$filename,$params=array()){
        return '';
    }

    /**
     * Asigna un archivo al tipo de dato
     *
     * @param array $filedata datos del upload (elemento del arreglo $_FILES)
     * @access public
     * @return integer id del archivo en la tabla temporal
     */
    public function setFile($filedata){
        $id_temporal = $this->_insertFile($filedata);
        if(!empty($id_temporal)){

            # Seteo la informaci�n del archivo adjunto
            $data = array(
                'name' => $filedata['name'],
                'metadata' => array('mime' => $filedata['type']),
                'size' => $filedata['size']
            );

            # En caso que tenga descripci�n, se la agrego
            if($this->getMetadataValue('parametros','description') != 'none'){
                $data['metadata']['description'] = $filedata['description'];
            }

            # Actualizo la informaci�n del archivo adjunto
            InnyCore_File::actualizarInfoArchivo($id_temporal,$data);
        }
        return $id_temporal;
    }

    /**
     * Inserta el archivo adjunto en la tabla temporal
     *
     * @param array $filedata datos del arreglo $_FILES correspondientes al archivo
     * @access protected
     * @return integer en caso de insertar el archivo, NULL en caso de fallo
     */
    protected function _insertFile($filedata){
        return DFM::setFromFile($filedata['tmp_name']);
    }

    /**
     * Retorna los datos necesarios para generar la preview
     *
     * @param integer $id_temporal id del archivo en la tabla temporal
     * @param string $filename nombre del archivo
     * @param array $params par�metros extra
     * @access public
     * @return array
     */
    public function getViewLinks($id_temporal,$filename,$params=array()){
        return array(
            'filename' => $filename,
            'url_download' => 'download/'.$id_temporal.'/'.$filename
        );
    }

    /**
     * Retorna el valor por default en caso que no tenga contenido alguno
     *
     * @access public
     * @return string
     */
    public function getDefaultValue(){
        return InnyCMS_Speech::getSpeech('tipo','tipo_defaultFile');
    }
}
################################################################################