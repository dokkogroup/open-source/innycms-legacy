<?php
/**
 *
 *
 */
require_once '../commons/inny/core.type.php';
require_once '../commons/inny/core.utils.php';

/**
 *
 *
 */
class InnyTypeText extends InnyType{

    /**
     * @var array campos que pueden ser �nicos
     * @static
     * @access protected
     */
    protected static $uniqueTextTypes = array('text','integer','float','date','time','datetime','email');

    /**
     * @var array nombres de los par�metros
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     * Retorna todos los nombres de par�metros correspondientes al tipo "texto"
     *
     * @static
     * @access public
     * @return array nombres de par�metros correspondientes al tipo "texto"
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(parent::getParamKeys(),array('unique'));
        }
        return self::$paramkeys;
    }

    /**
     * Retorna el valor por defecto de un par�metro correspondiente al tipo "texto"
     *
     * @param string $paramname nombre del par�metro
     * @static
     * @access public
     * @return string valor por defecto del par�metro
     */
    public static function getParamDefaultValue($paramname){

        # En caso que no exista...
        switch($paramname){

            # Descripci�n adjunta a un archivo
            case 'unique': return 'no';

            # En cualquier otro caso...
            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     * Retorna el texto luego de aplicar el trim
     * Por defecto se aplica el trim normal
     *
     * @return string texto luego de aplicar el trim
     */
    public function trimmed(){
        return Denko::trim($this->value);
    }

    /**
     * Indica si el valor del texto est� vac�o
     * Por defecto pregunta si es una cadena vac�a
     *
     * @access public
     * @return boolean si el valor del texto est� vac�o
     */
    public function is_empty(){
        return (Denko::trim($this->value) == '');
    }

    /**
     * Retorna el valor del texto en forma de cadena para guardarse en la DB
     * Por defecto retorna el texto trimeado. En caso de estar vac�o, retorna NULL
     *
     * @access public
     * @return string texto en forma de cadena para guardarse en la DB
     */
    public function postdata2dbdata(){
        return $this->is_empty() ? 'null' : $this->trimmed();
    }

    /**
     * Retorna el c�digo HTML correspondiente al input del texto
     * Por defecto retorna un input tipo text
     *
     * @param array $params par�metros para el html del input
     * @access public
     * @return string c�digo HTML correspondiente al input del texto
     */
    public function htmlInput($params=array()){
        $html_params = InnyCore_Utils::array2params($params,array('value'));
        return '<input type="text" value="'.($this->value).'" '.$html_params.' />';
    }

    /**
     * Retorna el c�digo HTML para la preview del valor del texto
     *
     * @param array $params par�metros
     * @access public
     * @return string c�digo HTML para la preview del valor del texto
     */
    public function preview($params=array()){

        # Verifico si debo entregar el texto resumido
        $summary = (!empty($params['summary']) && ($params['summary'] == true || $params['summary'] == 'true'));

        # En caso de entregar el texto resumido
        return $summary ? $this->preview_summary(!empty($params['length']) ? $params['length'] : 100) : $this->preview_normal();
    }

    /**
     * Retorna la preview con el contenido completo del texto
     *
     * @access public
     * @return string preview con el contenido completo del texto
     */
    public function preview_normal(){
        return $this->getValue();
    }

    /**
     * Retorna la preview con el contenido resumido del texto
     *
     * @access public
     * @return string preview con el contenido resumido del texto
     */
    public function preview_summary($length){
        return $this->preview_normal();
    }

    /**
     * Obtiene si el tipo de datos puede ser �nico
     *
     * @access public
     * @return boolean si el tipo de datos puede ser �nico
     */
    public function isUniqueTextType(){
        return in_array($this->type,self::$uniqueTextTypes);
    }

}
################################################################################
?>