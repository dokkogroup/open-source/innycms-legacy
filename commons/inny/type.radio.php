<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 * @package InnyType
 */
class InnyType_Radio extends InnyTypeText{

    /**
     * Constuctora
     *
     * @access public
     * @return void
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'radio';
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getValue(){

        # Obtengo el conjunto de opciones posibles para el radio
        $options = $this->getMetadataValue('parametros','options');

        # Verifico si el valor est� seteado y adem�s es v�lido (o sea, pertenece al conjunto de opciones)
        if(!InnyCore_Dao::isEmpty($this->value) && array_key_exists($this->value,$options)){
            return $this->value;
        }

        # Verifico si existe valor por default y si adem�s es v�lido (o sea, pertenece al conjunto de opciones)
        $default = $this->getMetadataValue('parametros','default');
        return isset($default) && array_key_exists($default,$options) ? $default : null;
    }


    /**
     * Retorna los par�metros correspondientes al radio
     *
     * @access public
     * @return array
     */
    public static function getParamKeys(){
        return array_merge(parent::getParamKeys(),array('options','default'));
    }

    /**
     *
     *
     */
    public function trimmed(){
        return $this->value;
    }

    /**
     *
     *
     */
    public function is_empty(){
        return false;
    }

    /**
     *
     *
     */
    public function htmlInput($params=array()){

        # Obtengo los valores del radio
        $options = $this->getMetadataValue('parametros','options');

        #
        # Obtengo cual es el item seleccionado
        $selected = $this->getValue();
        $html = '';
        foreach($options as $key => $value){
            $html .= '
            <input type="radio" name="'.$params['name'].'" value="'.$key.'" '.($key == $selected? 'checked="checked" ' : '').'/> '.$value.'
            <br />';
        }
        return $html;
    }

    /**
     *
     *
     */
    public function preview_normal(){

        # Obtengo los options del select
        $options = $this->getMetadataValue('parametros','options');
        return InnyCore_Utils::escape_html($options[$this->getValue()]);
    }
}
################################################################################
?>