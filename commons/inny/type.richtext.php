<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 *
 *
 */
class InnyType_Richtext extends InnyTypeText{

    /**
     * @var array nombres de los parámetros
     * @static
     * @access protected
     */
    protected static $paramkeys = null;

    /**
     * Constructora
     *
     * @access public
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'richtext';
    }

    /**
     * Retorna los parámetros correspondientes al tipo richtext
     *
     * @access public
     * @return array parámetros correspondientes al checkbox
     */
    public static function getParamKeys(){
        if(self::$paramkeys === null){
            self::$paramkeys = array_merge(parent::getParamKeys(),array('toolbar'),array('plaintext'));
        }
        return self::$paramkeys;
    }

    /**
     * Retorna el valor por defecto de un parámetro
     *
     * @param string $paramname nombre del parámetro
     * @access public
     * @return string valor por defecto del parámetro
     */
    public static function getParamDefaultValue($paramname){

        switch($paramname){

            # Toolbar del WYSIWYG. Por defecto asume la toolbar de la configuración
            case 'toolbar': return false;
            case 'plaintext':return false;
            # Default
            default: return parent::getParamDefaultValue($paramname);
        }
    }

    /**
     *
     *
     */
    public function trimmed(){
        return $this->value;
    }

    /**
     *
     *
     */
    public function is_empty(){
        return (InnyCore_Utils::cleanWysiwygContent($this->value) == '');
    }

    /**
     *
     *
     */
    public function htmlInput($params=array()){
        require_once $this->smarty->_get_plugin_filepath('function','inny_wysiwyg');
        return smarty_function_inny_wysiwyg(array_merge($params,array('toolbar' => $this->getMetadataValue('parametros','toolbar')),array('plaintext' => $this->getMetadataValue('parametros','plaintext'))),$this->smarty);
    }

    /**
     *
     *
     */
    public function preview_summary($length){
        require_once $this->smarty->_get_plugin_filepath('modifier','sw_strip_tags');
        return smarty_modifier_sw_strip_tags($this->value,$length);
    }
}
################################################################################
?>