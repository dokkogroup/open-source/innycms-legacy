<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 *
 *
 */
class InnyType_Select extends InnyTypeText{

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'select';
    }

    /**
     * Obtiene la clave de la opci�n
     *
     * @access public
     * @return mixed clave de la opci�n escogida
     */
    public function getValue(){

        # Obtengo el conjunto de opciones posibles para el select
        $options = $this->getMetadataValue('parametros','options');

        # Verifico si la clave est� seteada y si adem�s es v�lida (o sea, pertenece al conjunto de opciones)
        if(!InnyCore_Dao::isEmpty($this->value) && array_key_exists($this->value,$options)){
            return $this->value;
        }

        # En caso que no est� seteado valor alguno, retorno el valor por default
        # Si tampoco est� seteado el valor por default, retorno NULL
        $default = $this->getMetadataValue('parametros','default');
        return (isset($default) && array_key_exists($default,$options) ? $default : null);
    }

    /**
     * Retorna los par�metros correspondientes al checkbox
     *
     * @access public
     * @return array
     */
    public static function getParamKeys(){
        return array_merge(parent::getParamKeys(),array('options','default'));
    }

    /**
     *
     *
     */
    public function trimmed(){
        return $this->value;
    }

    /**
     *
     *
     */
    public function is_empty(){
        return false;
    }

    /**
     *
     *
     */
    public function htmlInput($params=array()){

        # Obtengo los options del select
        $options = $this->getMetadataValue('parametros','options');

        # Obtengo cual es el item seleccionado
        $selected = $this->getValue();

        # Genero el HTML
        $html = '';
        foreach($options as $key => $value){
            $html .= '
            <option value="'.$key.'" label="'.$value.'" '.($key == $selected ? ' selected="selected"' : '').' >'.$value.'</option>';
        }
        return '<select name="'.$params['name'].'" id="'.$params['name'].'">'.$html.'
        </select>';
    }

    /**
     *
     *
     */
    public function preview_normal(){

        # Obtengo los options del select
        $options = $this->getMetadataValue('parametros','options');
        return InnyCore_Utils::escape_html($options[$this->getValue()]);
    }
}
################################################################################
?>