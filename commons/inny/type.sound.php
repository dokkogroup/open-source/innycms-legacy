<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.file.php';
require_once '../getid3/getid3/getid3.php';

/**
 *
 *
 */
class InnyType_Sound extends InnyTypeFile{

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'sound';
    }

    /**
     * validateFile
     * Verifico que sea un archivo de sonido v�lido
     *
     * @link http://ar.php.net/features.file-upload.errors
     * @link http://getid3.sourceforge.net/
     */
    public function validate($filedata){
        $validate = parent::validate($filedata);
        if($validate == UPLOAD_ERR_OK){
            $getID3 = new getID3;
            $fileInfo = $getID3->analyze($filedata['tmp_name']);
            if(isset($fileInfo['error']) && count($fileInfo['error'])){
                return UPLOAD_ERR_EXTENSION;
            }
        }
        return $validate;
    }

    /**
     *
     *
     */
    public static function getContentUrl($id_temporal,$filename,$params=array()){
        return 'download/'.$id_temporal.'/'.$filename;
    }

    /**
     *
     *
     */
    public function getViewLinks($id_temporal,$filename,$params=array()){
        return array_merge(
            parent::getViewLinks($id_temporal,$filename),
            array('url_sound' => $this->getContentUrl($id_temporal,$filename))
        );
    }

    /**
     *
     *
     */
    public function getDefaultValue(){
        return InnyCMS_Speech::getSpeech('tipo','tipo_defaultSound');
    }
}
################################################################################