<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 *
 *
 */
class InnyType_Text extends InnyTypeText{

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'text';
    }

    /**
     *
     *
     */
    public function preview_summary($length){
        require_once $this->smarty->_get_plugin_filepath('modifier','truncate');
        return InnyCore_Utils::escape_html(smarty_modifier_truncate($this->value,$length));
    }

    /**
     *
     *
     */
    public function preview_normal(){
        return InnyCore_Utils::escape_html($this->value);
    }

    /**
     *
     *
     */
    public function htmlInput($params=array()){
        $html_params = InnyCore_Utils::array2params($params,array('value'));
        return '<input type="text" value="'.InnyCore_Utils::escape_html($this->value).'" '.$html_params.' />';
    }
}
################################################################################
?>