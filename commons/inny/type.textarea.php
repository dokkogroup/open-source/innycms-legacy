<?php
/**
 *
 *
 */
require_once '../commons/inny/type.parent.text.php';

/**
 *
 *
 */
class InnyType_Textarea extends InnyTypeText{

    /**
     *
     *
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'textarea';
    }

    /**
     *
     *
     */
    public function trimmed(){
        return $this->value;
    }

    /**
     *
     *
     */
    public function htmlInput($params=array()){
        return '<textarea name="'.$params['name'].'" class="abmInput textarea" style="font:normal 12px arial,tahoma,verdana; width:100%; height:200px;">'.(!empty($params['value']) ? $params['value'] : '').'</textarea>';
    }

    /**
     *
     *
     */
    public function preview_normal(){
        require_once $this->smarty->_get_plugin_filepath('modifier','sw_text_to_html');
        return smarty_modifier_sw_text_to_html(InnyCore_Utils::escape_html($this->value));
    }

    /**
     *
     *
     */
    public function preview_summary($length){
        require_once $this->smarty->_get_plugin_filepath('modifier','truncate');
        return InnyCore_Utils::escape_html(smarty_modifier_truncate(str_replace("\r",'',str_replace("\n",' ',$this->value)),$length));
    }
}
################################################################################
?>