<?php
/**
 * Tipo de dato para archivos del tipo thumb
 *
 * @package InnyType
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @copyright Copyright (c) 2007-2009 Dokko Group.
 * @link http://www.dokkogroup.com.ar/
 */

/**
 * Archivos necesarios
 * @ignore
 */
require_once '../commons/inny/type.image.php';

/**
 * @package InnyType
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 */
class InnyType_Thumb extends InnyType_Image{

    /**
     * Constructora
     *
     * @access public
     */
    public function __construct(){
        parent::__construct();
        $this->type = 'image';
    }
}
################################################################################
?>