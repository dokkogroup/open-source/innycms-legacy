<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_categoriesiterator} block plugin
 *
 * Type: block
 * <br>
 * Name: dse_categoriesiterator
 * <br>
 * Purpose: Este bloque de Smarty se utiliza para iterar sobre las categor�as
 *          SIN haber realizado una b�squeda.
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *   - name: El nombre del bloque.
 * 
 * - Opcionales
 *   - id_view: El n�mero de la vista bajo la cual se encuentran las categor�as a iterar.
 *   - id_parent: El ID de la categor�a bajo la cual se encuentran las categor�as a iterar. 
 *   - index1: Valor del campo index1 (si se est� dentro del Inny el valor ser� sobreescrito
 *             con el id_sitio actual).
 *   - index2: Valor del campo index2.
 *   - index3: Valor del campo index3.
 *   - level: De que nivel son las categor�as a recuperar.
 *   - export: Los campos que se desean exportar. Por defecto, todos.
 *  
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_block_dse_categoriesiterator($params, $content, & $smarty, & $repeat) {

   # Verifico que el parametro 'name' est� seteado
    if(empty($params['name'])){
        Denko::plugin_fatal_error('El par&aacute;metro <b>name</b> es requerido','dse_categoriesiterator');
    }

    # Convierto el parametro 'name' a min�sculas, que es como se guarda el nombre
    # del listado en la DB
    $lowerName = strtolower($params['name']);

    #
    $validName = Denko::toValidTagName($params['name']);

    # Flag en variable global. Se utiliza para que esta parte (creacion y
    # configuracion del dao) se ejecute solo la primera vez que se invoca
    # este bloque.
    $dse_categoriesiteratorflag = 'DSE_CATEGORIESITERATOR_'.$validName;

    if (!isset($GLOBALS[$dse_categoriesiteratorflag])) {
        if($repeat == true){
            
            #Configuro el dao, seg�n par�metros.
            $settings = array();
            
            $settings['name'] = $lowerName;
            
            # La tabla a listar es 'dse_category'
            $settings['table'] = 'dse_category';
            
            #Si est� seteado el orderBY, se lo asigno a las configuraciones del DAOLister.
            $settings['orderBy'] = isset($params['orderBy']) ? $params['orderBy'] : '';
            
            $dao = DB_DataObject :: factory('dse_category');
            #Armo el DAO de acuerdo a los par�metros.
            if (isset($params['id_view'])) {
                $dao->id_view = $params['id_view'];
            }
            if (isset($params['id_parent'])) {
                $dao->id_parent = $params['id_parent'];
            }
            
            if (dse_useInny()) {
                $dao->index1 = Inny :: getLoggedIdSitio();
            }elseif (isset($params['index1'])) {
                $dao->index1 = $params['index1'];    
            }
            
            if (isset($params['index2'])) {
                $dao->index2 = $params['index2'];
            }
            if (isset($params['index3'])) {
                $dao->index3 = $params['index3'];
            }
            if (isset($params['level'])) {
                $dao->level = $params['level'];
            }
            $settings['dao'] = $dao;
            
            new DK_DAOLister($settings);
            $GLOBALS[$dse_categoriesiteratorflag] = true;
        }
    }

    # Comienza la parte de iteracion.
    if (!isset($params['export'])) {
        $params['export'] = 'id_category,name,code,id_view,metadata,id_parent,index1,index2,index3,level';
    }

    $daoLister = &$GLOBALS['DK_LISTER'][$validName];

    if(!$daoLister->fetch()){
        $repeat = false;
        unset($GLOBALS[$dse_categoriesiteratorflag]);
        return $content;
    }
    else{
        $array = explode(',',$params['export']);

        foreach($array as $var){
            $dao = $daoLister->getDao();
            $smarty->assign($var,$dao->$var);
        }
        $repeat = true;
        return $content;
    }
}
################################################################################
?>