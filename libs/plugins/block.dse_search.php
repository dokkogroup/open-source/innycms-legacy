<?php


/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_search_resultslister} block plugin
 *
 * Type: block
 * <br>
 * Name: dse_search
 * <br>
 * Purpose: Este bloque de Smarty se utiliza para realizar una b�squeda
 *          en dse.
 * <br>
 * Input:
 * <br>
 * - Opcionales
 *   - query = El query que se hace a dse. Puede ser un string (por ej 'pizza en tandil')
 *             o bien puede ser un arreglo donde cada posici�n especifica contra que
 *             campo se realizar� que query (por ejemplo {0 => 'pizza', 1 => '', 2 => 'tandil'}
 *             significa que se buscara la palabra 'pizza' en el campo 0 y la palabra 'tandil'
 *             en el campo 1).
 *   
 *   - categories = Un arreglo de ids de categor�as donde se hizo drilldown. Si es null dse
 *                  supone que no hay drilldowns.
 *   
 *   - sortString = Un string indicando el ordenamiento de los resultados al estilo sql. Los
 *                  posibles campos a usar son: field_0, field_1, field_2, score, so_field_0
 *                  y so_field_1. En este campo es v�lida cualquier combinaci�n entre ellos
 *                  seguida de los modificadores [ASC|DESC]. Si este par�metro est� ausente
 *                  o es false no se aplicar� orden alguno. Si es vac�o (el string vac�o "")o
 *                  true se ordenar� por relevancia (score) descendentemente.
 *   
 *   - resultsPerPage = Cantidad de resultados por p�gina. Si es vac�o se le asigna 10.
 *  
 *   - fromGet = Obtiene el query, el arreglo categories y el sortString del querystring. Este
 *               par�metro puede ser true o false.
 *  
 *   - views = IDs de vistas separadas por coma que este bloque usar�. Si este par�metro est�
 *             ausente se usar�n todas las vistas de la base de datos. Si la configuraci�n
 *             dse.useInny est� habilitada (true) solo se usar�n las vistas del sitio actual.
 *   
 *   - doNotSearchDocuments = No hace la b�squeda sobre los documentos. Se utiliza cuando se
 *                            necesita iterar sobre todas las categor�as de una o
 *                            todas las vistas. Posibles valores true o false.
 *                            Si este par�metro est� en false, no estar�n disponibles las
 *                            variables resultsCount, originalQuery, modifiedQuery ni searchTime.
 *                            Valor por defecto false.
 *   - getAllCategories = Trae todas las categor�as, independientemente del query que se halla
 *                        hecho. Es m�s r�pido que el m�todo com�n oprque no cuenta los resultados
 *                        de cada categor�a. Valor por defecto false.
 *   - whereAdd = Permite filtrar los documentos por alg�n campo de la tabla documentos.
 *   - innyNombreListado = Nombre del listado del Inny por el cual se desean filtrar los documentos.
 *                         Este par�metro ser� ignorado si la configuraci�n dse.useInny es false.
 *   - autoDrillDown = Habilita o deshabilita el auto drilldown en todas las vistas. Si ante una b�squeda,
 *                     en alguna de las vistas hay una sola categor�a por refinar, dse realizar� un drilldown
 *                     autom�tico en ella. Por defecto, false.
 * <br>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_block_dse_search($params, $content, & $smarty, & $repeat) {

    // Nombre del bloque dse en la clase Denko
    $dseSearchBlockName = (!empty($params["name"])) ? $params["name"] : DSE_PLUGIN_BLOCK_NAME;
    if ($repeat == true) {

        //Verifico si estoy dentro del Inny.
        //Si esta dentro del Inny asigno el id_sitio (sobre el cual se est�usando DSE) y el
        //id_listado (sobre el cual se est�n realizando las b�squedas).
        if (dse_useInny()) {

            if (isset ($params['innyNombreListado'])) {
                $innyId_listado = Inny :: getIdListadoByName($params['innyNombreListado']);
            }
            $innyId_sitio = Inny :: getLoggedIdSitio();
        }

        //Si no se asignaron vistas, traigo todas.
        if (empty ($params['views'])) {
            if (dse_useInny() && isset($innyId_sitio)) {
                $views = dse_get_views($innyId_sitio);
            } else {
                $views = dse_get_views();
            }
            $viewsStr = '';
            if ($views) {
                while ($views->fetch()) {
                    if ($viewsStr === '') {
                        $viewsStr .= $views->id_view;
                    } else {
                        $viewsStr .=  ','.$views->id_view;
                    }
                }
            }
            $params['views'] = $viewsStr;
        }

        if ($params['fromGet']) {
            block_dse_search_parseQuerystring($params);
        }
		
		//Clono el query para que no se vea modificado por las transformaciones
		$originalQuery = $params['query'];
				
		// Valido el query, si hay errores corregibles, los arreglo
		dse_validateQuery($params['query'],$params['categories']);
		
        //Si esta seteado forceAnd, agrego el modificador de and a todas las palabras
        if (isset($params['forceAnd']) &&  $params['forceAnd'] && !dse_isBooleanQuery($params['query'])) {
			$params['query'] = dse_transformAnd($params['query']);
        }
        
        //Armo el DAO de la tabla document.
        $modifiedQuery = $params['query'];
        
        $whereAdd = (isset ($params['whereAdd'])) ? $params['whereAdd'] : false;
        
        $document = dse_search($modifiedQuery, $params['categories'], $params['sortString'], $whereAdd);
        
        //INNY: Filtro los documentos por listado.
        if (isset ($innyId_listado)) {
            $document->whereAdd("id_listado = $innyId_listado");
        }

        //Armo los settings para crear el DAO lister.
        $settings = array ();
        $settings['name'] = $dseSearchBlockName;

        //Si no hay que buscar documentos, no completo estas variables
        if (!isset ($params['doNotSearchDocuments']) || !$params['doNotSearchDocuments']) {

            $settings['dao'] = $document;
            $settings['resultsPerPage'] = (!empty ($params['resultsPerPage']) && $params['resultsPerPage'] > 0) ? $params['resultsPerPage'] : 10;

            //Calculo el n�mero de p�gina para limitar el query.
            $resultsCount = $document->count();
            $pageGetName = 'dkp_' . $dseSearchBlockName;
            $pn = isset ($_GET[$pageGetName]) ? $_GET[$pageGetName] : 1;
            $rpp = $settings['resultsPerPage'];

            if ($pn == 1) {
                $start = 0;
            } else {
                $start = $pn * $rpp - $rpp;
            }

            $document->limit($start, $rpp);

            //Hago la b�squeda y asigno las variables de dse.
            $msInit = microtime(true);
            $count = $document->find();
            $msTotal = microtime(true) - $msInit;

            $dseVar = array ();
            $dseVar['resultsCount'] = $resultsCount;
            $dseVar['searchTime'] = $msTotal;
            $dseVar['originalQuery'] = $originalQuery;
            $dseVar['modifiedQuery'] = $modifiedQuery;
        }
        $dseVar['totalTime'] = (isset ($msTotal)) ? $msTotal : 0;

        new DK_DAOLister($settings);

        //Cuenta de categor�as
        $GLOBAL[$dseSearchBlockName . '__views'] = array ();
        $dseVar['views'] = array ();

        // Si el flag de getAllCategories est� en true, obtengo todas las categor�as.

        $getAllCategories = (isset ($params['getAllCategories']) && $params['getAllCategories'] === true) ? true : false;
        $autoDrillDown = isset ($params['autoDrillDown']) ? $params['autoDrillDown'] : false;
        $doNotCountCategories = isset ($params['doNotCountCategories']) ? $params['doNotCountCategories'] : false;

        if (is_array($params['categories']) && count($params['categories']) > 0 && !$doNotCountCategories) {
            foreach ($params['categories'] as $id_view => $id_category) {

                $category = dse_get_categories($params['query'], $params['categories'], $id_category, $id_view, $getAllCategories,$whereAdd,$dseVar['resultsCount']);

                //INNY: Filtro las categor�as por sitio (index1 == $id_sitio)
                if (isset ($innyId_sitio)) {
                    $category->whereAdd("index1 = $innyId_sitio");
                }

                if ($category) {
                    
                    $msInit = microtime(true);
                    //Si est� seteado autodrilldown, voy tantos niveles abajo hasta que no
                    //halla m�s categor�as o hasta que halla m�s de una categor�a.
                    if ($autoDrillDown) {
                        $count = $category->find();
                        if ($count === 1) {
                            while ($count === 1) {
                                $category->fetch();
                                $id_category = $category->id_category;
                                $params['categories'][$id_view] = $id_category;
                                $category = dse_get_categories($params['query'], $params['categories'], $id_category, $id_view, $getAllCategories,$whereAdd, $dseVar['resultsCount']);
                                //INNY: Filtro las categor�as por sitio (index1 == $id_sitio)
                                if (isset ($innyId_sitio)) {
                                    $category->whereAdd("index1 = $innyId_sitio");
                                }
                                $count = $category->find();
                            }
                        }
                    } else {
                        $count = $category->find();    
                    }
                    $msTotal = microtime(true) - $msInit;

                    $dseVar['views'][$id_view]['searchTime'] = $msTotal;
                    $dseVar['views'][$id_view]['categoryCount'] = $count;
                    $dseVar['totalTime'] += $msTotal;
                    $dseVar['views'][$id_view]['categoryId'] = $id_category;

                    // Creo las vistas
                    $GLOBALS[$dseSearchBlockName . '__views'][$id_view]['dao'] = $category;
                }
            }
        }
        $smarty->assign('dse', $dseVar);
    }

    return $content;

}
################################################################################
/**
 * Obtiene todos los par�metros necesarios del querystring.
 * Para usarla aplicar el par�metro fromGet=true ver lista de par�metros del
 * bloque para m�s ayuda.
 */
function block_dse_search_parseQuerystring(& $params) {

    if (!empty ($_GET[DSE_PLUGIN_BLOCK_NAME . 'q'])) {
        //Query normal
        $query = $_GET[DSE_PLUGIN_BLOCK_NAME . 'q'];
    }
    elseif (!empty ($_GET[DSE_PLUGIN_BLOCK_NAME . 'fq'])) {
        //Fielded query
        $query = array ();
        $query[0] = (!empty ($_GET[DSE_PLUGIN_BLOCK_NAME . 'f0'])) ? $_GET[DSE_PLUGIN_BLOCK_NAME . 'f0'] : '';
        $query[1] = (!empty ($_GET[DSE_PLUGIN_BLOCK_NAME . 'f1'])) ? $_GET[DSE_PLUGIN_BLOCK_NAME . 'f1'] : '';
        $query[2] = (!empty ($_GET[DSE_PLUGIN_BLOCK_NAME . 'f2'])) ? $_GET[DSE_PLUGIN_BLOCK_NAME . 'f2'] : '';
    } else {
        $query = '';
    }
    $params['query'] = & $query;

    /* Parseo las categor�as. */
    $categories = array ();
    if (isset ($params['views'])) {
        $views = explode(',', $params['views']);
        foreach ($views as $viewId) {
            if (isset ($_GET[DSE_PLUGIN_BLOCK_NAME . 'c' . $viewId]) && !empty ($_GET[DSE_PLUGIN_BLOCK_NAME . 'c' . $viewId])) {
                $categories[$viewId] = $_GET[DSE_PLUGIN_BLOCK_NAME . 'c' . $viewId];
            } else {
                $categories[$viewId] = null;
            }
        }
    }
    $params['categories'] = & $categories;
    /* Parseo los campos de ordenamiento*/
    //Si hay asignado un valor desde el plugin, no parseo los par�metros GET.
    if (!isset($params['sortString'])) {
        $sortString = '';
        
        if (isset ($_GET[DSE_PLUGIN_BLOCK_NAME . 'sf'])) {
            //Para mantener la compatibilidad con la coma (busco el separador).
            if (strpos($_GET[DSE_PLUGIN_BLOCK_NAME . 'sf'],',') > 0) {
                $separator = ',';
            } else {
                $separator = '-';
            }
            
            $sfs = explode($separator, $_GET[DSE_PLUGIN_BLOCK_NAME . 'sf']);
            $sos = (!empty ($_GET[DSE_PLUGIN_BLOCK_NAME . 'so'])) ? explode($separator, $_GET[DSE_PLUGIN_BLOCK_NAME . 'so']) : array ();
            
            foreach ($sfs as $key => $field) {
                if ($field === '0' || $field === '1' || $field === '2') {
                    $sortString .= ($sortString === '') ? 'field_' . $field : ', field_' . $field;
                }
                elseif ($field === 'sof0' || $field === 'sof1') {
                    $field = str_replace('sof', '', $field);
                    $sortString .= ($sortString === '') ? 'so_field_' . $field : ', so_field_' . $field;
                } else {
                    $sortString .= ($sortString === '') ? 'score' : ', score';
                }
    
                if (isset ($sos[$key]) && $sos[$key] === 'a') {
                    $sortString .= ' ASC';
                } else {
                    $sortString .= ' DESC';
                }
            }
        } else {
            $sortString = 'score DESC';
        }
        $params['sortString'] = $sortString;
    }
}