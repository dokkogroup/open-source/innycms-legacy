<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_search_breadcrumbslister} block plugin
 *
 * Type: block
 * <br>
 * Name: dse_search_breadcrumbslister
 * <br>
 * Purpose: Este bloque de Smarty se utiliza para iterar sobre los breadcrumbs
 *          de las categor�as de cada vista.
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *   - id_view : El n�mero de la vista de la cual se desean obtener los breadcrumbs.
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_block_dse_search_breadcrumbslister($params, $content, & $smarty, & $repeat) {

    // Nombre del bloque dse en la clase Denko
    $dseSearchBlockName = DSE_PLUGIN_BLOCK_NAME;
    
    //Los campos que voy a exportar
    $exportList = array('id_category','name','metadata');
    
    if (!isset($params['id_view'])) {
        Denko::plugin_fatal_error('el par�metro <b>id_view</b> es requerido','dse_search_breadcrumbslister');
    } else {
        $id_view = $params['id_view'];
    }
    
    //Creo el flag para que entre la primera vez.
    $dseBreadcrubmsBlockFlag = $dseSearchBlockName . '__breadcrumbs__' . $id_view;
    
    if (!isset($GLOBALS[$dseBreadcrubmsBlockFlag])) {
        $dseVar = & $smarty->_tpl_vars['dse'];
        if (isset($dseVar['views'][$id_view]['categoryId'])) {
            $id_category = $dseVar['views'][$id_view]['categoryId'];
        } else {
            //Denko::plugin_fatal_error('la vista <b>'.$id_view.'</b> no existe','dse_search_breadcrumbslister');
            $id_category = null;
        }
        
        # verifico si se quiere que la vista este presente en el path
        
        $includeView = false;
        if (isset($params['includeView']) && $params['includeView'] === true) {
        	$includeView = true;
        }
            	
        $path = & dse_get_categorypath($id_category, $includeView);
        
        if ($path === null) {
            $repeat = false;
            return $content;
        }
        $GLOBALS[$dseBreadcrubmsBlockFlag] = $path;
    }
    
    $path = & $GLOBALS[$dseBreadcrubmsBlockFlag];
    
    if (count($path) <= 0) {
        unset($GLOBALS[$dseBreadcrubmsBlockFlag]);
        $repeat = false;
        return $content;
    } else {
        $category = array_shift($path);
        $categoryParamName = DSE_PLUGIN_BLOCK_NAME . 'c';
        foreach ($exportList as $fieldName) {
            if ($fieldName == 'id_category') {
                $patterns = array('/&'.$categoryParamName . $id_view . '=[0-9]+&/','/^'.$categoryParamName . $id_view . '=[0-9]+&/','/&'. $categoryParamName . $id_view . '=[0-9]+$/');
                $replacements = array('&','','');
                $querystring = preg_replace($patterns,$replacements,$_SERVER['QUERY_STRING']);
                $base = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : basename($_SERVER['PHP_SELF']); 
                
                $url = $base . '?' . $querystring; 
                
                # Si el padre es vista, no pongo el drilldown, directamente lo elimino,
                # a menos que explicitamente me pidan dejarlo.
                $includeParentIfView = isset($params['includeParentIfView']) &&  $params['includeParentIfView'] ? true : false;
                                
	            $parent = $category->getParent();
	            if ($parent !== null && (!$parent->isView() || $includeParentIfView)) {
	            	$url .= '&'.$categoryParamName . $id_view . '=' . $category->id_parent;	
	            }
                
                $smarty->assign('url',  $url);     
            }
            $smarty->assign($fieldName, $category->$fieldName);    
        }
        $repeat = true;
        return $content;
    }
   
}
################################################################################