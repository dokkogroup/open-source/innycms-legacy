<?php


/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_search_categorieslister} block plugin
 *
 * Type: block
 * <br>
 * Name: dse_search_categorieslister
 * <br>
 * Purpose: Este bloque de Smarty se utiliza para iterar sobre las categor�as
 *          de una b�squeda.
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *   - id_view : El n�mero de la vista en la cual se encuentra las categor�as a iterar.
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
/**
 * Provee una interfaz de iteraci� de DBDataObject a un arreglo de DAOs.
 * Respeta la interfaz de DSE_Category (los campos, el m�todo fetch y el m�todo
 * getCategory).
 * Se utiliza �nicamente cuando hay que ordenar las categor�as por orden alfab�tico
 * o cuando hay que listar las categor�as por cuenta de resultados Y orden alfab�tico.
 */
class DSEFakeDBDataObject {
    private $objects;

    function DSEFakeDBDataObject($objects) {
        $this->objects = $objects;
    }

    function getCategory() {
        return $this;
    }
    function fetch() {
        $element = array_pop($this->objects);
        if (!$element) {
            return false;
        } else {
            foreach ($element as $property => $value) {
                $this-> $property = $value;
            }
            return true;
        }
    }
}
/**
 * Comparador de dos objetos.
 */
class CategoriesComparator {
    private $fieldNames = null;
    private $fieldOrder = null;

    /**
     * Constructor.
     * Cosntruye un comparador.
     * Si $fieldOrders es null, parsea un string al estilo SQL.
     * Si $fieldOrders no es null, supone que $fieldNames es un arreglo de nombres
     * de campos por los cuales ordenar y $fieldOrders es un arreglo de tipos de
     * ordenamiento (los valores posibles son asc o desc).
     * Ejemplos:
     *  $fieldNames = 'names asc, rcount desc'
     *  $fieldOrders = null
     *  
     *  $fieldNames = array ('name', 'rcount')
     *  $fieldOrders = array('asc', 'desc')
     */
    function CategoriesComparator($fieldNames, $fieldOrders = null) {
        if ($fieldOrders !== null) {
            $this->fieldNames = $fieldNames;
            $this->fieldOrder = $fieldOrders;
        } else {
            $this->fieldNames = array ();
            $this->fieldOrder = array ();
            #Parseo desde un string.
            $orderDeclaration = strtolower($fieldNames);
            $sorts = explode(',', $orderDeclaration);
            foreach ($sorts as $aSortDeclaration) {
                $aSortDeclaration = trim($aSortDeclaration);
                # Remuevo los espacios y tabs de m�s
                $aSortDeclaration = preg_replace('/[ \t]+/',' ',$aSortDeclaration);
                $exploded = explode(' ', $aSortDeclaration);
                if (count($exploded) === 2) {
                    $this->fieldNames[] = $exploded[0];
                    $this->fieldOrder[] = $exploded[1];
                }
            }
        }
    }
    
    /**
     * Funci�n de comparaci�n de dos objetos.
     * Si c1 == c2 se procede a comparar el siguiente campo.
     */
    function compare($c1, $c2, $level = 0) {

        $field = $this->fieldNames[$level];

        if ($level === count($this->fieldNames)) {
            return 0;
        }

        if ($c1-> $field == $c2-> $field) {
            return $this->compare($c1, $c2, $level +1);
        }

        if ($this->fieldOrder[$level] == 'desc') {
            return ($c1-> $field < $c2-> $field) ? -1 : 1;
        } else {
            return ($c1-> $field > $c2-> $field) ? -1 : 1;
        }
    }

}

################################################################################
function smarty_block_dse_search_categorieslister($params, $content, & $smarty, & $repeat) {

    // Nombre del bloque dse en la clase Denko
    $dseSearchBlockName = DSE_PLUGIN_BLOCK_NAME;

    //Los campos que voy a exportar
    $exportList = array (
        'id_category',
        'name',
        'metadata',
        'level',
        'index1',
        'index2',
        'index3'
    );

    if (!isset ($params['id_view'])) {
        Denko :: plugin_fatal_error('el par�metro <b>id_view</b> es requerido', 'dse_search_categorieslister');
    } else {
        $id_view = $params['id_view'];
    }

    # Obtengo el DAO sobre el cual vamos a iterar.

    $catResults = & $GLOBALS[$dseSearchBlockName . '__views'][$id_view]['dao'];

    # Flag, este flag es para que entre a esta parte la primera vez que se llama al
    # bloque
    if (!isset ($GLOBALS[$dseSearchBlockName . '__views'][$id_view]['flag'])) {
        $GLOBALS[$dseSearchBlockName . '__views'][$id_view]['flag'] = true;

        # Si no tengo un DAO para iterar, retorno vac�o.
        if (isset ($GLOBALS[$dseSearchBlockName . '__views'][$id_view]['dao'])) {

            #Si tengo un ordenamiento creo el DAO fake.
            if (isset ($params['orderBy'])) {

                $catArray = array ();

                #Itero sobre los DAOs del DBDataObject y los cargo en un arreglo.
                while ($catResults->fetch()) {
                    $aCategory = $catResults->getCategory();
                    $aCategory->rcount = $catResults->rcount;
                    $catArray[] = $aCategory;
                    unset ($aCategory);
                }

                # Ordeno el arreglo de DAOs seg�n el criterio especificado.
                //TODO: Permitir especificar m�s criterios.
                $comparator = new CategoriesComparator($params['orderBy']);
                
                usort($catArray, array (
                    $comparator,
                    'compare'
                ));
                # Creo el DAO fake.
                $dao = new DSEFakeDBDataObject($catArray);

                # Y se lo asigno a la variable global para poder accederlo en la pr�xima iteraci�n
                # del bloque.
                $GLOBALS[$dseSearchBlockName . '__views'][$id_view]['dao'] = $catResults = & $dao;
            }
        } else {
            return '';
        }
    }

    # Comienzo la iteraci�n.

    if (!$catResults->fetch()) {
        unset ($GLOBALS[$dseSearchBlockName . '__views'][$id_view]['flag']);
        $repeat = false;
        return $content;
    } else {

        $category = & $catResults->getCategory();
        $smarty->assign('rcount', $catResults->rcount);

        foreach ($exportList as $fieldName) {
            if ($fieldName == 'id_category') {

                # Si el campo actual es el id_category, armo el url para el drilldown.

                $categoryParamName = DSE_PLUGIN_BLOCK_NAME . 'c';
                
                # Si las vistas son exclusivas se remueven los drilldowns a otras vistas
                if (isset($params['viewsAreExclusive']) && $params['viewsAreExclusive']) {
                	$patterns = array (
	                    '/&' . $categoryParamName . "[0-9]" . '=[0-9]*&/',
	                    '/^' . $categoryParamName . "[0-9]" . '=[0-9]*&?/',
	                    '/&' . $categoryParamName . "[0-9]" . '=[0-9]*$/'
	                );
	                $replacements = array (
	                    '&',
	                    '',
	                    ''
	                );
                } else {                	
	                $patterns = array (
	                    '/&' . $categoryParamName . $id_view . '=[0-9]*&/',
	                    '/^' . $categoryParamName . $id_view . '=[0-9]*&?/',
	                    '/&' . $categoryParamName . $id_view . '=[0-9]*$/'
	                );
	                $replacements = array (
	                    '&',
	                    '',
	                    ''
	                );
                }
                
                $querystring = preg_replace($patterns, $replacements, $_SERVER['QUERY_STRING']);
                $base = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : basename($_SERVER['PHP_SELF']);
                $smarty->assign('url', $base . '?' . $querystring . '&' . $categoryParamName . $id_view . '=' . $category-> $fieldName);
            }
            $smarty->assign($fieldName, $category-> $fieldName);
        }
        $repeat = true;
        return $content;
    }

}
################################################################################