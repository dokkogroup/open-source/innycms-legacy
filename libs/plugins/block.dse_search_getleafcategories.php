<?php


/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_search_getleafcategories} function plugin
 *
 * Type: block
 * <br>
 * Name: dse_search_getleafcategories
 * <br>
 * Purpose: Itera sobre las categor�as de un documento. Importante: usarlo
 *          dentro del bloque dse_search_resultslister.
 * Input:
 *  export: Lista de nombres de campos a exportar de la tabla dse_category.
 *          Por defecto, todos.
 *  id_view: N�mero de vista de la cual se desean obtener las categor�as.
 *           Por defecto todas.
 *  index1: Valor del index1. Por defecto no se aplica (a menos que se est� usando
 *          desde el inny, en cuyo caso es el id_sitio).
 *  index2: Valor del index2. Por defecto no se aplica.
 *  index3: Valor del index3. Por defecto no se aplica.
 * <br>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return DAO
 */
function smarty_block_dse_search_getleafcategories($params, $content, & $smarty, & $repeat) {

    // Nombre del bloque dse en la clase Denko
    $dseSearchBlockName = DSE_PLUGIN_BLOCK_NAME;
    
    #Obtengo el DAO documento.
    $dao = & $GLOBALS['DK_LISTER'][$dseSearchBlockName]->getDao();

    $tableId = DSE_DOCUMENT_TABLEID;

    //Creo el flag para que entre la primera vez con el id_documento.
    $dseGetLeafCategoriesBlockFlag = $dseSearchBlockName . '__leafcategories__' . $dao-> $tableId;

    if (!isset ($GLOBALS[$dseGetLeafCategoriesBlockFlag])) {

        $fromView = (isset ($params['id_view'])) ? $params['id_view'] : false;

        $fromIndex1 = (isset ($params['index1'])) ? $params['index1'] : false;
        $fromIndex2 = (isset ($params['index2'])) ? $params['index2'] : false;
        $fromIndex3 = (isset ($params['index3'])) ? $params['index3'] : false;

        #Si estoy dentro del inny, le agrego el filtrado por el index1.
        if (dse_useInny()) {
            $fromIndex1 = Inny :: getLoggedIdSitio();
        }
        $category = & $dao->getLeafCategories($fromView, $fromIndex1);
        #Si no hay categor�as hijas a las que pertenezca el documento, retorno vac�o
        # y salgo.
        if ($category === null || !$category->find()) {
            $repeat = false;
            return '';
        }
        $GLOBALS[$dseGetLeafCategoriesBlockFlag] = $category;
    }
    
    #Comienza la iteraci�n.
    $leafCategories = & $GLOBALS[$dseGetLeafCategoriesBlockFlag];
    if (!$leafCategories->fetch()) {
        #No hay m�s categor�as por las cuales iterar.
        unset ($GLOBALS[$dseGetLeafCategoriesBlockFlag]);
        $repeat = false;
        return $content;
    } else {
        # Si no hay par�metro export, pongo todos los campos.
        if (!isset ($params['export'])) {
            $params['export'] = 'id_category,name,code,id_view,metadata,id_parent,index1,index2,index3,level';
        }
        $array = explode(',', $params['export']);
        foreach ($array as $var) {
            $smarty->assign($var, $leafCategories-> $var);
        }
        $repeat = true;
        return $content;
    }
}