<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */

/**
 * Dokko Search Engine Smarty {dse_search_paginator} block plugin
 *
 * Type: block
 * <br>
 * Name: dse_search_paginator
 * <br>
 * Purpose: Este bloque es utilizado para el paginado de los resultados. Dentro de
 *          �ste se pueden acceder a variables para maquetar el paginador.
 *          Estas variables adquieren su valor de forma autom�tica, es decir,
 *          se las asigna el listado de resultados que lo contiene.
 *          Estas variables son:
 *            - dkp_actual: nro de p�gina actual.
 *            - dkp_last: nro de la �ltima p�gina (cantidad de p�ginas).
 *            - dkp_begin: a partir de cual nro de resultado se est� mostrando.
 *            - dkp_end: hasta cual nro de resultado se est� mostrando.
 *            - dkp_results: resultados por p�gina
 * <br>
 * Examples:
 * <pre>
 * {dse_search_paginator}
 *   {assign var="buttonsTolerance" value=5}
 *   {assign var="ini" value=$dkp_actual-$buttonsTolerance}
 *   {assign var="top" value=$dkp_actual+$buttonsTolerance}
 *   {if $ini < 1}
 *     {assign var="ini" value=1}
 *   {/if}
 *   {if $top > $dkp_last}
 *     {assign var="top" value=$dkp_last}
 *   {/if}
 *   <div class="paginador">
 *     <center>
 *     &nbsp;
 *     {if $dkp_actual > 1}
 *       <a href="{dse_search_hrefpage number=$dkp_actual-1}" title="anterior">&laquo; anterior</a>
 *     {/if}
 *     {section name="pagbutts" start=$ini loop=$top+1}
 *       {if $dkp_actual == $smarty.section.pagbutts.index}
 *         <span class="actual">{$smarty.section.pagbutts.index}</span>
 *       {else}
 *       <a href="{dse_search_hrefpage number=$smarty.section.pagbutts.index}">{$smarty.section.pagbutts.index}</a>
 *       {/if}
 *     {/section}
 *     {if $dkp_actual != $dkp_last}
 *       <a href="{dse_search_hrefpage number=$dkp_actual+1}" title="siguiente">siguiente &raquo;</a>
 *     {/if}
 *     </center>
 *   </div>
 * {/dse_search_paginator}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_block_dse_search_paginator($params, $content, &$smarty, &$repeat){
    
    // Nombre del bloque dse en la clase Denko
    //$dseSearchBlockName = DSE_PLUGIN_BLOCK_NAME;
    $dseSearchBlockName = (!empty($params["name"])) ? $params["name"] : DSE_PLUGIN_BLOCK_NAME;
    
    # Obtengo el DAOLister correspondiente a este paginador
    $daoLister = &$GLOBALS['DK_LISTER'][$dseSearchBlockName];
    
    if($repeat == true){
        $daoLister->setPageVars($smarty);
    }else{
        $daoLister->restorePageVars($smarty);
        return $content;
    }
}
################################################################################