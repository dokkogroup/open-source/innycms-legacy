<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_search_resultslister} block plugin
 *
 * Type: block
 * <br>
 * Name: dse_search_resultslister
 * <br>
 * Purpose: Este bloque de Smarty se utiliza para iterar sobre los resul
 *          tados de una b�squeda.
 * <br>
 * Input:
 * <br>
 * - Opcionales
 *   - export : Una lista de campos sobre la cual est� instalado dse que se desean exportar.
 *              Esta lista ser� anexada a la lista de dse (id_tabla,field_0,field_1,field_2
 *              y score).
 *
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_block_dse_search_resultslister($params, $content, & $smarty, & $repeat) {
    
    global $DSE_DOCUMENT_FIELD_MAPPINGS;
    
    // Nombre del bloque dse en la clase Denko
   // $dseSearchBlockName = DSE_PLUGIN_BLOCK_NAME;
   $dseSearchBlockName = (!empty($params["name"])) ? $params["name"] : DSE_PLUGIN_BLOCK_NAME;
    //Los campos que voy a exportar
    $exportString = DSE_DOCUMENT_TABLEID.','.DSE_DOCUMENT_FIELD_MAPPINGS_STR.',score';
    if (isset($params['export'])) {
        $exportString .= ',' . $params['export'];
    }
    $exportList = explode(',',$exportString);

    $daoLister = & $GLOBALS['DK_LISTER'][$dseSearchBlockName];
    
    if (!$daoLister->fetch()) {
        $repeat = false;
        return $content;
    } else {
        
        $dao = & $daoLister->getDao();
        foreach ($exportList as $fieldName) {
            if (DSE_DOCUMENT_TABLEID == $fieldName) {
                $smarty->assign('id_document', $dao->$fieldName);
            } else {
                if (($searchField = array_search($fieldName,$DSE_DOCUMENT_FIELD_MAPPINGS)) !== false) {
                    $smarty->assign($searchField, $dao->$fieldName);
                    $smarty->assign($fieldName, $dao->$fieldName);
                } else {
                    $smarty->assign($fieldName, $dao->$fieldName);
                }    
            }
        }
        $repeat = true;
        return $content;
    }
}
################################################################################