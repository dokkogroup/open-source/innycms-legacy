<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_search_viewslister} block plugin
 *
 * Type: block
 * <br>
 * Name: dse_search_viewslister
 * <br>
 * Purpose: Este bloque de Smarty se utiliza para iterar sobre las vistas que se
 *          declararon en su bloque padre dse_search.
 * <br>
 * Input:
 * <br>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_block_dse_search_viewslister($params, $content, & $smarty, & $repeat) {

    // Nombre del bloque dse en la clase Denko
    $dseSearchBlockName = DSE_PLUGIN_BLOCK_NAME;
    
    //Los campos que voy a exportar
    $exportList = array('name','metadata');
    
    //Creo el flag para que entre la primera vez.
    $dseViewsBlockFlag = $dseSearchBlockName . '__views__';
    
    if (!isset($GLOBALS[$dseViewsBlockFlag])) {
        $dseVar = & $smarty->_tpl_vars['dse'];
        if (isset($dseVar['views'])) {
            $viewIds = array_keys($dseVar['views']);
        } else {
            Denko::plugin_fatal_error('no existen vistas ','dse_search_viewslister');
        }
                
        $GLOBALS[$dseViewsBlockFlag] = & $viewIds;
    }
    
    $viewIds = & $GLOBALS[$dseViewsBlockFlag];
    
    if (count($viewIds) <= 0) {
        unset($GLOBALS[$dseViewsBlockFlag]);
        $repeat = false;
        return $content;
    } else {
        $id_view = array_shift($viewIds);
        
        $smarty->assign('id_view', $id_view);
        
        if (dse_useInny()) {
        	$index1 = Inny :: getLoggedIdSitio();
        } else {
        	$index1 = isset($params['index1']) ? $params['index1'] : null; 
        }
        
        $index2 = isset($params['index2']) ? $params['index2'] : null;
        
        $index3 = isset($params['index3']) ? $params['index3'] : null;     	
        
        $view = dse_get_view($id_view, $index1, $index2, $index3);
        foreach ($exportList as $fieldName) {
            $smarty->assign('view_' . $fieldName, $view->$fieldName);
        }
        
        $repeat = true;
        return $content;
    }
}
################################################################################