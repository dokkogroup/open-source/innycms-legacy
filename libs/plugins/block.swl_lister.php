<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swl_lister} block plugin
 *
 * Type: block
 * <br>
 * Name: swl_lister
 * <br>
 * Purpose: Este bloque de Smarty se utiliza para mostrar los listados de un sitio
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *   - name: nombre del bloque
 *
 * - Opcionales:
 *   - filter = filtro para restringir los listados que se mostrar�n
 *
 * <br>
 * Examples:
 * <pre>
 *   <ul>
 *   {swl_lister name="veh�culo" filter="veh�culo"}
 *     <li>{$nombre}</li>
 *   {/swl_lister}
 *   </ul>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20bloque%20swl_lister {swl_lister} (Inny wiki)
 * @param array $params par�metros
 * @param string $content En caso de que la etiqueta sea de apertura, este ser� null, si la etiqueta es de cierre el valor ser� del contenido del bloque del template
 * @param Smarty &$smarty instancia de Smarty
 * @param boolean &$repeat es true en la primera llamada de la block-function (etiqueta de apertura del bloque) y false en todas las llamadas subsecuentes
 * @return string
 */
################################################################################
function smarty_block_swl_lister($params, $content, &$smarty, &$repeat){

    # En caso que sea la 1era vez que entra al plugin, verifico que el par�metro "name" exista
    if(!$repeat && empty($params['name'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'el par�metro name es requerido','swl_lister');
        $repeat = false;
        return '';
    }

    # Verifico que el contenido del plugin no est� vac�o
    if(!$repeat && $content == ''){
        return '';
    }

    # Obtengo el DAO Listado
    $global_key = 'INNY_SWL_LISTER_'.strtoupper($params['name']);
    if(isset($GLOBALS[$global_key])){
        $daoListado = &$GLOBALS[$global_key];
    }

    else{

        # Obtengo los listados que pertenezcan al sitio
        $daoSitioLogueado = Inny::getDaoSitio();
        $daoListado = DB_DataObject::factory('listado');

        # Si est� seteados los filtros, se los aplico
        $daoListado->id_sitio = $daoSitioLogueado->id_sitio;
        if(!empty($params['filter'])){
            $daoListado->whereAdd('filtro = \''.$params['filter'].'\'');
        }

        # En caso que existan listados con este criterio
        $GLOBALS[$global_key] = $daoListado->find() ? $daoListado : null;
        if(!isset($GLOBALS[$global_key])){
            return '';
        }
    }

    # En caso que deba obtenerse el contenido del DAO
    if($daoListado && ($repeat == true || isset($content))){

        # Asigno las variables del listado al template
        if($daoListado->fetch()){
            $metadata=json_decode($daoListado->metadata,true);
            # Campos de la tabla listado
            $smarty->assign('id_listado',$daoListado->id_listado);
            $smarty->assign('nombre',$daoListado->nombre);
            $smarty->assign('alias',$metadata['nombre']);
            $smarty->assign('id_sitio',$daoListado->id_sitio);
            $smarty->assign('metadata',$daoListado->metadata);
            $smarty->assign('filtro',$daoListado->filtro);
            $smarty->assign('aud_ins_date',$daoListado->aud_ins_date);
            $smarty->assign('aud_upd_date',$daoListado->aud_upd_date);

            # Datos extra del listado que se setean en el template
            $smarty->assign('cantidad_productos',$daoListado->getCantidadProductos());

            $repeat = true;
        }
        else{
            unset($GLOBALS[$global_key]);
        }

        # Retorno el contenido del bloque
        return $content;
    }
}
################################################################################
?>