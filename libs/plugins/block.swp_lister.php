<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_lister} block plugin
 *
 * Type: block
 * <br>
 * Name: swp_lister
 * <br>
 * Purpose: Este bloque de Smarty se utiliza para mostrar todos los resultados
 *          de un listado.
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *   - name: nombre del listado que se quiere mostrar.
 *     Si el sitio no posee un listado con este nombre, lo crea.
 *
 * - Opcionales:
 *   - resultsPerPage = resultados por p�gina que mostrar� el listado
 *     (default: muestra todos los resultados)
 *   - orderBy = ordena el listado en base a un criterio. Por defecto ordena los
 *     productos de manera ascendente, tomando en cuenta el campo posici�n.
 *   - export = campos del producto que se usar�n para mostrar en el listado.
 *     Si no se setean estos campos, los trae a todos.
 *     Los campos del producto son:
 *       - texto1 = texto primario.
 *       - texto2 = texto secundario.
 *       - texto3 = texto terciario.
 *       - id_archivo = ID del archivo del producto
 *       - ids_archivo = arreglo con los IDs de los archivos del producto.
 * <br>
 * Examples:
 * <pre>
 * <table cellpadding="2" cellspacing="2">
 *    <tr>
 *      <td>nombre</td>
 *      <td>version DOC</td>
 *      <td>version PDF</td>
 *    </tr>
 * {swp_lister name="archivos" resultsPerPage="30"}
 *    <tr>
 *      <td><b>{$texto1|strip_tags|default:"&nbsp;"}</b></td>
 *      <td><a href="{swp_url type="file" id_archivo=$ids_archivo[1]}">descargar</a></td>
 *      <td><a href="{swp_url type="file" id_archivo=$ids_archivo[2]}">descargar</a></td>
 *    </tr>
 * {/swp_lister}
 * </table>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20bloque%20swp_lister {swp_lister} (Inny wiki)
 * @param array $params par�metros
 * @param string $content En caso de que la etiqueta sea de apertura, este ser� null, si la etiqueta es de cierre el valor ser� del contenido del bloque del template
 * @param Smarty &$smarty instancia de Smarty
 * @param boolean &$repeat es true en la primera llamada de la block-function (etiqueta de apertura del bloque) y false en todas las llamadas subsecuentes
 * @return string
 */
include_once '../denko/dk.daolister.php';
################################################################################
function smarty_block_swp_lister($params, $content, &$smarty, &$repeat){

    # Verifico que el contenido del plugin no est� vac�o
    if(!$repeat && $content == ''){
        return '';
    }

    # Verifico que el parametro 'name' est� seteado
    if(empty($params['name'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro name es requerido','swp_lister');
        $repeat = false;
        return '';
    }

    # Convierto el parametro 'name' a min�sculas, que es como se guarda el nombre
    # del listado en la DB. Luego lo convierto a nombre de listado v�lido
    $lowerName = strtolower($params['name']);
    $validName = Denko::toValidTagName($params['name']);

    # En caso que el DAO Listado est� cacheado
    $cachedListadoKey = 'INNY_CACHED_LISTADO_'.strtoupper($validName);
    if(isset($GLOBALS[$cachedListadoKey])){
        $daoListado = &$GLOBALS[$cachedListadoKey];
    }

    # En caso que no est� cacheado, lo busco en la DB
    else{

        # Primero obtengo el listado. Si el listado no existe en el sitio debo crearlo.
        $daoListado = DB_DataObject :: factory('listado');
        $daoListado->nombre = $lowerName;
        $daoListado->id_sitio = Inny::getActualSitio();
        if (!$daoListado->find(true)){

            # Verifico si el Sitio tiene permisos para crear listados
            $id_sitio = Inny::getActualSitio();

            # En caso que no est� seteado con que sitio se est� trabajando
            if($id_sitio === null){
                InnyModule_Reporter::plugin_error(E_USER_WARNING,'No se ha seteado con que Sitio se est� trabajando','swp_lister');
                $repeat = false;
                return '';
            }

            $daoSitio = Inny::getCachedDaoSitio();

            # En caso que el Sitio no tenga permisos para agregar listados, lanzo el error
            if($daoSitio->crea_listados == '0'){
                InnyModule_Reporter::plugin_error(E_USER_WARNING,'No puede crearse el listado '.$params['name'],'swp_lister');
                $repeat = false;
                return '';
            }

            # En caso que el Sitio SI tenga permisos para agregar listados, lo agrego
            $daoListado->nombre = $lowerName;
            $daoListado->id_sitio = $id_sitio;

            # Agrego el Listado a la DB. Si no lo puedo crear lanzo un error.
            if (!$daoListado->insert()){
                InnyModule_Reporter::plugin_error(E_USER_WARNING,'Error al crear el listado '.$params['name'],'swp_lister');
                $repeat = false;
                return '';
            }
        }

        $GLOBALS[$cachedListadoKey] = $daoListado;
    }

    # Flag en variable global. Se utiliza para que esta parte (creacion y
    # configuracion del dao) se ejecute solo la primera vez que se invoca
    # este bloque.
    $swp_listerFlag = 'SWP_LISTER_'.$validName.'_'.$daoListado->id_sitio;

    if (!isset($GLOBALS[$swp_listerFlag])) {
        if($repeat == true){

            # La tabla a listar es 'Producto'
            $params['table'] = 'producto';

            # Filtro con el query del DAO lister en la tabla 'Producto'
            # los que pertenecen al listado que se desea mostrar.
            $params['query'] = 'id_listado = '.$daoListado->id_listado.(!empty($params['query']) ? ' and ('.$params['query'].')' : '');

            if(!empty($params['relatedId'])){
				$ids='-1';
				$pp = Denko::daoFactory('producto_producto');
				$pp->whereAdd('id_producto1='.$params['relatedId']);
				$pp->find();
				while($pp->fetch()) $ids.=','.$pp->id_producto2;
				$pp->free();

				$pp = Denko::daoFactory('producto_producto');
				$pp->whereAdd('id_producto2='.$params['relatedId']);
				$pp->find();
				while($pp->fetch()) $ids.=','.$pp->id_producto1;
				$pp->free();
				
				$related = 'id_producto in ('.$ids.')';
			    $params['query'] .= ' and ( '.$related.' )';
			}

            # En caso de no aclararse el criterio de ordenamiento del listado,
            # asumo que el orden del listado ser� por posicion ascendentemente.
            if(empty($params['orderBy'])){
                $params['orderBy'] = 'posicion ASC';
            }

            new DK_DAOLister($params);
            $GLOBALS[$swp_listerFlag] = true;
        }
    }

    # Comienza la parte de iteracion.
    if (!isset($params['export'])) {
        $params['export'] = 'id_producto,posicion,id_listado';

        # Agrego los campos de texto
        for($i = 1; $i <= InnyCore_Producto::$texto_cantidad; $i++){
            $params['export'].= ',texto'.($i);
        }

        # Agrego los contadores
        for($i = 1; $i <= InnyCore_Producto::$contador_cantidad; $i++){
            $params['export'].= ',contador'.($i);
        }

        # Agrego los �ndices
        for($i = 1; $i <= InnyCore_Producto::$indice_cantidad; $i++){
            $params['export'].= ',indice'.($i);
        }

        # Agrego los campos de auditor�a
        $params['export'].= ',aud_ins_date,aud_upd_date';
    }

    $daoLister = &$GLOBALS['DK_LISTER'][$validName];

    if(!$daoLister->fetch()){
        $repeat = false;
        unset($GLOBALS[$swp_listerFlag]);
        unset($GLOBALS[$cachedListadoKey]);
        return $content.($daoLister->isSetMultiAction()?'
        </form>':'');
    }
    else{
        if(isset($params['export'])){
            $array = explode(',',$params['export']);

            foreach($array as $var){
                $dao = $daoLister->getDao();
                if($var == 'id_producto'){
					$id_archivos = InnyCore_File::getIdsArchivo($dao->$var);
					$smarty->assign('ids_archivo',$id_archivos);
					$smarty->assign('id_archivo',$id_archivos[1]);
	            }

                # En caso que sea un tipo de texto
                if(substr($var,0,5) == 'texto'){
                    $index = substr($var,5);
                    if(!InnyCore_Utils::is_integer($index)){
                        # TODO: Lanzar un mensaje de warning
                        continue;
                    }
                    $innyType = $daoListado->createInnyType('texto',$index);
                    $innyType->setValue($dao->$var);
                    $smarty->assign($var,$innyType->getValue());
                }
                else{
                    $smarty->assign($var,$dao->$var);
                }
            }
        }

        else{
        	$dao = $daoLister->getDao();
        	$id_archivos = InnyCore_File::getIdsArchivo($dao->id_producto);
        	$smarty->assign('ids_archivo',$id_archivos);
        	$smarty->assign('id_archivo',$id_archivos[1]);
        }

        if($daoLister->isSetMultiAction() && $repeat == true){
            require_once $smarty->_get_plugin_filepath('function','dk_include');
            $multiAction = $daoLister->getMultiAction();
            $formName = DAOListerMultiActionPrefix.$daoLister->getName();
            echo smarty_function_dk_include(array('file'=>'js/dk.multiaction.js'),$smarty).'
            <form name="'.$formName.'" method="get" action="'.basename($_SERVER['PHP_SELF']).'">
            '.$multiAction->htmlHiddenInputs();
        }
        $repeat = true;
        return $content;
    }
}
################################################################################
?>
