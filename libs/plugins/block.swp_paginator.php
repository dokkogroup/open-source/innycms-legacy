<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_paginator} block plugin
 *
 * Type: block
 * <br>
 * Name: swp_paginator
 * <br>
 * Purpose: Este bloque es utilizado para el paginado del listado. Dentro de
 *          �ste se pueden acceder a variables para maquetar el paginador.
 *          Estas variables adquieren su valor de forma autom�tica, es decir,
 *          se las asigna el listado que lo contiene. Estas variables son:
 *            - dkp_actual: nro de p�gina actual.
 *            - dkp_last: nro de la �ltima p�gina (cantidad de p�ginas).
 *            - dkp_begin: a partir de cual nro de resultado se est� mostrando.
 *            - dkp_end: hasta cual nro de resultado se est� mostrando.
 *            - dkp_results: resultados por p�gina
 * <br>
 * Examples:
 * <pre>
 * {swp_paginator name="bicicletas"}
 *   {assign var="buttonsTolerance" value=5}
 *   {assign var="ini" value=$dkp_actual-$buttonsTolerance}
 *   {assign var="top" value=$dkp_actual+$buttonsTolerance}
 *   {if $ini < 1}
 *     {assign var="ini" value=1}
 *   {/if}
 *   {if $top > $dkp_last}
 *     {assign var="top" value=$dkp_last}
 *   {/if}
 *   <div class="paginador">
 *     <center>
 *     &nbsp;
 *     {if $dkp_actual > 1}
 *       <a href="{swp_hrefpage number=$dkp_actual-1}" title="anterior">&laquo; anterior</a>
 *     {/if}
 *     {section name="pagbutts" start=$ini loop=$top+1}
 *       {if $dkp_actual == $smarty.section.pagbutts.index}
 *         <span class="actual">{$smarty.section.pagbutts.index}</span>
 *       {else}
 *       <a href="{swp_hrefpage number=$smarty.section.pagbutts.index}">{$smarty.section.pagbutts.index}</a>
 *       {/if}
 *     {/section}
 *     {if $dkp_actual != $dkp_last}
 *       <a href="{swp_hrefpage number=$dkp_actual+1}" title="siguiente">siguiente &raquo;</a>
 *     {/if}
 *     </center>
 *   </div>
 * {/swp_paginator}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20bloque%20swp_paginator {swp_paginator} (Inny wiki)
 * @param array $params par�metros
 * @param string $content En caso de que la etiqueta sea de apertura, este ser� null, si la etiqueta es de cierre el valor ser� del contenido del bloque del template
 * @param Smarty &$smarty instancia de Smarty
 * @param boolean &$repeat es true en la primera llamada de la block-function (etiqueta de apertura del bloque) y false en todas las llamadas subsecuentes
 * @return string
 */
################################################################################
function smarty_block_swp_paginator($params, $content, &$smarty, &$repeat){

    # Verifico que exista el par�metro 'name'
    if(empty($params['name'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'el par�metro <b>name</b> es requerido','swp_paginator');
		return '';
    }

    # Obtengo el DAOLister correspondiente a este paginador
    $validName = Denko::toValidTagName($params['name']);
    $daoLister = &$GLOBALS['DK_LISTER'][$validName];

    # El dao puede no existir, en caso de que se haya intentado acceder a un
    # listado que no le pertence al sitio.
    if (!$daoLister){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'el listado con nombre '.$params['name'].' no ha sido creado','swp_paginator');
    	$repeat = false;
    	return '';
    }

    if($repeat == true){
        $daoLister->setPageVars($smarty);
    }else{
        $daoLister->restorePageVars($smarty);
        return $content;
    }
}
################################################################################
?>