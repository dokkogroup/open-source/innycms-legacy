<?php
/**
 * Denko Smarty plugin
 * @package Denko
 * @subpackage plugins
 */

/**
 * Inny Smarty {dk_redirect} function plugin
 *
 * Type: function
 * <br>
 * Name: dk_redirect
 * <br>
 * Purpose: Hace un redirect hacia una URL determinada
 *
 * @author Dokko Group Developers Team <info at dokkogroup dot com>
 * @link http://wiki.dokkogroup.com.ar/index.php/http://wiki.dojo/index.php/Denko%20Plugin%3A%20funci%F3n%20dk_redirect {dk_redirect} (Denko wiki)
 * @param array $params parámetros
 * @param Smarty &$smarty instancia de Smarty
 * @return string
 */
################################################################################
function smarty_function_dk_redirect($params, &$smarty){
    Denko::redirect($params['url']);
    exit;
}
################################################################################
