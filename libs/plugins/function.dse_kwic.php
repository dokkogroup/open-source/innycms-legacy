<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_kwic} function plugin
 *
 * Type: function
 * <br>
 * Name: dse_kwic
 * <br>
 * Purpose: Sirve para hacer un keyword in context del query buscado en un campo 
 *          de texto.
 * Input:
 * <br>
 *   
 * - Requeridos
 *   - words = El query realizado a DSE (string o fielded, normalmente obtenido como 
 *             $dse.originalQuery).
 *   - text = El texto sobre el cual aplicar el keyword in context.
 * 
 * - Opcionales
 *   - maxlength = La m�xima longitud deseada del keyword in context en caracteres (por
 *                defecto es 300).
 *   - classToApply = Las palabras del query matcheadas en el texto ser�n wrapeadas entre
 *                    dos tags SPAN. Este par�metro, permite asignarle al SPAN una clase
 *                    css.
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_dse_kwic($params,&$smarty){

    //Palabras del query
    $qs = $params['words'];
    //Texto a aplicar KWIC
    $text = $params['text'];
    //Maxima longitud deseada del KWIC
    $maxlength = (isset($params['maxlength'])) ? $params['maxlength'] : 300;
    //Maxima longitud del texto
    $textlength = strlen($text);
    //Clase a aplicar a las palabras encontradas, tiene que ser un SPAN.
    $classToApply = isset($params['classToApply']) ? $params['classToApply'] : ''; 
   
    $matches = array();
    
    if (is_array($qs)) {
        $qs = implode(' ',$qs);
    }
    
    
    if (stripos($qs,"\"") !== FALSE) {
        // Es phresal search, remuevo las comillas y trato de matchear la frase completa.
        $pattern = '/'.str_replace("\"","",preg_quote(trim($qs))).'/i';
        $matches_result = preg_match($pattern, $text, $matches, PREG_OFFSET_CAPTURE, 0);
        $matches[0]['pattern'] = $pattern;
        
    } else {
        // Es un query normal, por cada palabra trato de matchear en el texto.
        foreach (explode(" ",trim($qs)) as $word) {
            // Si la palabra es menor que tres caracteres la ignoro.
            if (strlen($word) > 2) {
                /*str_replace("+","",$word);
                $pattern = str_replace("*","",$pattern);*/
                $pattern = preg_replace('/[^A-Za-z0-9�����]*/', '', $word);
                $pattern = "/".$pattern.'[A-Za-z0-9�����]*(\b|-|,|;)'."/i";
                
                $matches_result = preg_match($pattern, $text, $matching_groups, PREG_OFFSET_CAPTURE, 0);
                
                // Si hubo match, guardo el patr�n y el desplazamiento de la palabra en el texto.
                if ($matches_result) {
                    $matching_groups[0]['pattern'] = $pattern;
                    $matches[] = $matching_groups[0];
                }
            }
        }
    }
    // Si no hay matches, retorno el texto desde el comienzo truncado al m�nimo
    // entre la longitud del texto y la longitud estipulada en el KWIC.
    if (empty($matches)) {
        $length = ($textlength < $maxlength) ? $textlength : $maxlength;  
        return substr($text,0,$length) . ' ...';
    }
    // Si hubo matches los ordeno por orden de aparici�n
    $matches = sortMatches($matches);
    
    //En matches me queda un arreglo en el cual por cada palabra matcheada hay
    //un arreglo en el cual:
    //      En la posicion 0, se encuentra la palabra original matcheada
    //      En la posicion 1, se encuentra la distancia desde el comienzo de ese string
    
    //Si la longitud del texto es mayor que $maxlength, hago un highlight del
    //texto y lo retorno.
    
    if ($textlength <= $maxlength) {
        return highlight($text,$matches,$classToApply);
    } else {
        //Si la longitud del texto es mayor que $maxlength caracteres, armo
        //el texto del KWIC.
        //Por cada match, agrego al texto del kwic 10 palabras antes de la palabra
        //matcheada y 10 palabras despues.
        $matchesCount = 40 - (count($matches)*10);
        foreach ($matches as $match) {
            $kwic .= ' ... ' . getBeforeLeadingWords($text,$match[1] - 1,$matchesCount) .
                     ' ' .$match[0] . ' ' .
                     getAfterLeadingWords($text,$match[1] + strlen($match[0]),$matchesCount);
        }
        
        $kwic.= ' ...';
        
        return highlight($kwic,$matches,$classToApply);
    }
}

function highlight($text,$matches,$classToApply,$highlightdots=false) {
    
    $tagopen = '<span class="'.$classToApply.'">';
    $tagclose = '</span>';
    
    foreach ($matches as $amatch) {
        $text = preg_replace($amatch['pattern'],$tagopen . $amatch[0] . $tagclose,$text);
    }
    
    if ($highlightdots) {
        return str_replace("...",$tagopen . '...' . $tagclose,$text);
    }
    
    return $text;    
}

function getBeforeLeadingWords($text,$start,$amount) {
    
    $finished = false;
    $local_buffer = '';
    $buffer = '';
    $wordcount = 0;
    for ($i = $start; $i >= 0 && !$finished; $i--) {
        if ($text[$i] !== ' ') {
            $local_buffer = $text[$i] . $local_buffer;
        } else {
            if (strlen($local_buffer) > 0) {
                $buffer = $local_buffer . ' ' . $buffer;
                $wordcount++;
            }
            
            if ($wordcount > $amount ) {
                $finished = true;
            }
            
            $local_buffer = '';  
        }    
    }
    
    return  $buffer;
}

function getAfterLeadingWords($text,$start,$amount) {
    
    $finished = false;
    $local_buffer = '';
    $buffer = '';
    $wordcount = 0;
    for ($i = $start; $i < strlen($text) && !$finished; $i++) {
        if ($text[$i] !== ' ') {
            $local_buffer .= $text[$i];
        } else {
            if (strlen($local_buffer) > 0) {
                $buffer = $buffer . ' ' . $local_buffer;
                $wordcount++;
            }
            
            if ($wordcount > $amount ) {
                $finished = true;
            }
            
            $local_buffer = '';  
        }    
    }
    
    return  $buffer;
}

function sortMatches($matches) {
    
    $sortedMatches = array();
    $sortedMatches[] = array_shift($matches);
    
    foreach($matches as $match) {
        
        $i = 0;
        while ($sortedMatches[$i][1] < $match[1] && $i < count($sortedMatches)) {
            $i++;
        }
        
        $start = array_slice($sortedMatches,0, $i);
        $end = array_slice($sortedMatches,$i);
        $start[] = $match;
        $sortedMatches = array_merge($start,$end);
    }
    
    return $sortedMatches;  
}