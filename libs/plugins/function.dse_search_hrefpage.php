<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */

/**
 * Dokko Search Engine Smarty {dse_search_hrefpage} function plugin
 *
 * Type: function
 * <br>
 * Name: dse_search_hrefpage
 * <br>
 * Purpose: Sirve para crear una URL hacia el nro de p�gina que recibe como
 *          par�metro. Esta funci�n solo debe usarse dentro del bloque dse_search.
 * Input:
 * <br>
 * - Requeridos:
 *   - number = n�mero de p�gina.
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_function_dse_search_hrefpage($params,&$smarty){
    
    // Nombre del bloque dse en la clase Denko
    //$dseSearchBlockName = DSE_PLUGIN_BLOCK_NAME;
    $dseSearchBlockName = (!empty($params["name"])) ? $params["name"] : DSE_PLUGIN_BLOCK_NAME;
    
    # Verifico que exista el parametro 'number'
    if(empty($params['number'])){
        Denko::plugin_fatal_error('El par�metro <b>number</b> es requerido','dse_search_hrefpage');
    }

    # Obtengo el par�metro a la p�gina del DAOLister correspondiente
    $daoLister  = &$GLOBALS['DK_LISTER'][$dseSearchBlockName];
    $paramPage  = $daoLister->getParamPage();

    # Armo el query string para el href hacia el nro de p�gina solicitada
    $querystring = '';
    foreach($_GET as $param => $value){
        if($param == $paramPage) continue;
        $querystring.= '&'.$param.'='.$value;
    }

    # Armo y retorno el link
    return basename($_SERVER['PHP_SELF']).'?'.$paramPage.'='.$params['number'].$querystring;
}
################################################################################