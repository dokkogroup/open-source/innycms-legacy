<?php

/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */

/**
 * Dokko Search Engine Smarty {dse_search_hrefsort} function plugin
 *
 * Type: function
 * <br>
 * Name: dse_search_hrefsort
 * <br>
 * Purpose: Sirve para crear una URL hacia el ordenamiento por campos.
 * Input:
 * <br>
 *   
 * - Opcionales
 *   - fields = campos separados por gui�n medio en el orden en que el ordenamiento se apicar�.
 *              Los valores v�lidos para los campos son:
 *                  * 0,1, o 2 se refieren a los campos field_0, field_1 y field_2 respectivamente
 *                  * sof0 o sof1 se refieren a los campos so_field_0 y so_field_1 respectivamente
 *                  * s se refiere a score
 *              Ejemplo:
 *                  "0-s-sof0" ordenar� en primer lugar por field_0, si field_0 no decide luego
 *                  ordenar� por score y si score no decide luego ordenar� por so_field_0
 *              Valor por defecto "s"
 *   - order = tipos de ordenamiento separados por gui�n medio que se aplicar�n al campo correspondiente
 *             en el par�metro fields.
 *             Los valores v�lidos son:
 *                  * "a" ascendente
 *                  * "d" descendente.
 *              Valor por defecto "d" a todos los campos.
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_function_dse_search_hrefsort($params, & $smarty) {

    if (!isset ($params['fields'])) {
        $params['fields'] = 's';
    }
    
    if (empty ($params['order'])) {
        $params['order'] = 'd';
    }

    $sortFieldParamName = DSE_PLUGIN_BLOCK_NAME . 'sf';
    $sortOrderParamName = DSE_PLUGIN_BLOCK_NAME . 'so';
    
    $sortUri = $sortFieldParamName . '=' . $params['fields'] . '&' . $sortOrderParamName . '=' . $params['order'];

    $patterns = array (
        '/&' . $sortFieldParamName . '=[0-2sof-]*&/',
        '/^' . $sortFieldParamName . '=[0-2sof-]*&/',
        '/&' . $sortFieldParamName . '=[0-2sof-]*$/'
    );
    $replacements = array (
        '&',
        '',
        ''
    );
    $querystring = preg_replace($patterns, $replacements, $_SERVER['QUERY_STRING']);

    $patterns = array (
        '/&' . $sortOrderParamName . '=[ad-]*&/',
        '/^' . $sortOrderParamName . '=[ad-]*&/',
        '/&' . $sortOrderParamName . '=[ad-]*$/'
    );
    
    $querystring = preg_replace($patterns, $replacements, $querystring);

    return basename($_SERVER['PHP_SELF']) . '?' . $querystring . '&' . $sortUri;
}
###############################################################################