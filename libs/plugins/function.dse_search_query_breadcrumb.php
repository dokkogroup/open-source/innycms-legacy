<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_query_breadcrumb} function plugin
 *
 * Type: function
 * <br>
 * Name: dse_query_breadcrumb
 * <br>
 * Purpose: Remueve los t�rminos buscados del URL.
 * Input:
 * <br>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_dse_search_query_breadcrumb($params,&$smarty){
    
    $queryString = '';
    
    foreach ($_GET as $pName => $pValue) {
        
        if (!isParamaTerm($pName)) {
            $queryString .= $pName . '=' . $pValue . '&';
        }
        
    }
    $base = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : basename($_SERVER['PHP_SELF']);
    return $base . '?' . $queryString;
}

function isParamaTerm($term) {
    return  $term === DSE_PLUGIN_BLOCK_NAME . 'q' ||
            $term === DSE_PLUGIN_BLOCK_NAME . 'fq' ||
            $term === DSE_PLUGIN_BLOCK_NAME . 'f0' ||
            $term === DSE_PLUGIN_BLOCK_NAME . 'f1' ||
            $term === DSE_PLUGIN_BLOCK_NAME . 'f2';
}