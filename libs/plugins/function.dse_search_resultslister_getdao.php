<?php
/**
 * Dokko Search Engine Smarty plugin
 * @package DSE
 * @subpackage plugins
 */
/**
 * Dokko Search Engine Smarty {dse_search_resultslister_getdao} function plugin
 *
 * Type: function
 * <br>
 * Name: dse_search_resultslister_getdao
 * <br>
 * Purpose: Retorna el DAO de la tabla donde se instal� DSE (dse_document si es standalone).
 * Input:
 *  assign: Nombre de la variable a la cual asignar el DAO.
 * <br>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return DAO
 */
function smarty_function_dse_search_resultslister_getdao($params,&$smarty){
    
    if (!isset($params['assign'])) {
        Denko::plugin_fatal_error('el par�metro <b>assign</b> es requerido','dse_search_resultslister_getdao');
    }
    
    // Nombre del bloque dse en la clase Denko
    $dseSearchBlockName = DSE_PLUGIN_BLOCK_NAME;
    
    $dao = & $GLOBALS['DK_LISTER'][$dseSearchBlockName]->getDao();
    $smarty->assign($params['assign'],$dao);
    
   
}