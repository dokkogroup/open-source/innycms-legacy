<?php
###############################################################################
function smarty_function_dse_sort($params,&$smarty){
    
    if (!isset($params['field']) || ($params['field'] !== '0' && $params['field'] !== '1' && $params['field'] !== '2')) {
        $field_number = 'n';
    } else {
        $field_number = $params['field'];
    }
    
    
    if (isset($params['order']) && $params['order'] == 'd') {
        $field_order = 'd';
    } else {
        $field_order = 'a';
    }
    
    $querystring = preg_replace('/sf=[0-9]&|&sf=[0-9]/','',$_SERVER['QUERY_STRING']);
    $querystring = preg_replace('/so=[ad]&|&so=[ad]/','',$querystring);
    
    return basename($_SERVER['PHP_SELF']). '?' . $querystring . '&sf='.$field_number . '&' . 'so=' . $field_order;
}
###############################################################################