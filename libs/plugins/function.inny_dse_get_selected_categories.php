<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_dse_options_selected_categories} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_dse_options_selected_categories
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_dse_options_selected_categories}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
require_once '../commons/inny.dse.php';
################################################################################
function smarty_function_inny_dse_get_selected_categories($params,&$smarty) {

    # Verifico que el par�metro 'assign' exista
    if(empty($params['assign'])){
        Denko::plugin_fatal_error('el par�metro <b>assign</b> es requerido','inny_dse_get_selected_categories');
    }

    # Verifico que el par�metro 'assign' exista
    if(empty($params['id_producto'])){
        Denko::plugin_fatal_error('el par�metro <b>assign</b> es requerido','inny_dse_get_selected_categories');
    }

    # Obtengo el DAO Producto
    $daoProducto = InnyCore_Dao::getDao('producto',$params['id_producto']);
    if(!empty($daoProducto)){
        $daoDseCategory = $daoProducto->getAddedCategories(); //$daoProducto->getLeafCategories();
        if($daoDseCategory != null && $daoDseCategory->find()){
            $categorias = array();
            while($daoDseCategory->fetch()){
                $path = $daoDseCategory->getPath(true);
                $breadcrums = '';
                foreach($path as $daoCategoryPath){
                    $breadcrums = $breadcrums.'� '.$daoCategoryPath->name.' ';
                }
                $categorias[$daoDseCategory->id_category] = $breadcrums;
            }
            $smarty->assign($params['assign'],$categorias);
        }
    }

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################
?>