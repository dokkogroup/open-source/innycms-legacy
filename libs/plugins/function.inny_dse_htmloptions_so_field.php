<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_dse_htmloptions_so_field} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_dse_htmloptions_so_field
 * <br>
 * Purpose:
 * <br>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_function_inny_dse_htmloptions_so_field($params, &$smarty) {

    # Verifico que est� seteado el par�metro 'nro'
    if(!isset($params['nro'])){
        Denko::plugin_fatal_error('el par�metro <b>nro</b> es requerido','inny_dse_htmloptions_so_field');
    }

    # Genero las claves y valor para los options
    $nro = $params['nro'];
    $daoSitioLogueado = Inny_Clientes::getSitioLogueado();
    $dse_settings = $daoSitioLogueado->getConfig('dse');
    if(!empty($dse_settings['so_field'.$nro]['values'])){
        $params['options'] = array();
        foreach($dse_settings['so_field'.$nro]['values'] as $value => $name){
            $params['options'][$value] = $name;
        }
    }

    # Entrego el HTML correspondiente a los options
    require_once $smarty->_get_plugin_filepath('function','html_options');
    return smarty_function_html_options($params,$smarty);
}
################################################################################
?>