<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_dse_options_category} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_dse_options_category
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_dse_options_category}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_function_inny_dse_options_category($params, &$smarty) {

    # Verifico que el parámetro 'assign' exista
    if(empty($params['assign'])){
        Denko::plugin_fatal_error('el parámetro <b>assign</b> es requerido','inny_dse_options_category');
    }

    # Verifico que el parámetro 'id_sitio' exista
    if(empty($params['id_sitio'])){
        Denko::plugin_fatal_error('el parámetro <b>id_sitio</b> es requerido','inny_dse_options_category');
    }

    # Asigno el arreglo de subcategorías al template
    $smarty->assign($params['assign'],Inny_DSE::getSubcategorias($params['id_sitio'],!empty($params['id_parent_category']) ? $params['id_parent_category'] : null));

    # Este plugin no retorna HTML
    return '';
}
################################################################################
?>