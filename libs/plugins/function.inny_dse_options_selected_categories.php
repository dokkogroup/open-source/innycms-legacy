<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_dse_options_selected_categories} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_dse_options_selected_categories
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_dse_options_selected_categories}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
require_once '../commons/inny.dse.php';
################################################################################
function smarty_function_inny_dse_options_selected_categories($params, &$smarty) {

    # Verifico que el par�metro 'assign' exista
    if(empty($params['assign'])){
        Denko::plugin_fatal_error('el par�metro <b>assign</b> es requerido','inny_dse_options_selected_categories');
    }

    # Preparo el arreglo donde agregar� los options
    $options = array();

    # En caso que haya categor�as en POST
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if(isset($_POST['dse_category_selecteds']) && count($_POST['dse_category_selecteds']) > 0){
            foreach($_POST['dse_category_selecteds'] as $id_category){
                $daoDseCategory = DB_DataObject::factory('dse_category');
                $daoDseCategory->get($id_category);
                $path = $daoDseCategory->getPath(true);
                $breadcrums = '';
                foreach($path as $daoCategoryPath){
                    $breadcrums = $breadcrums.'� '.$daoCategoryPath->name.' ';
                }
                $options[$daoDseCategory->id_category] = $breadcrums;
            }
        }
    }

    # En caso que tenga que obtenerlas desde la DB
    elseif(!empty($params['id_producto'])){
        $daoProducto = DB_DataObject::factory('producto');
        $daoProducto->get($params['id_producto']);
        $daoDseCategory = $daoProducto->getAddedCategories(); //$daoProducto->getLeafCategories();
        if($daoDseCategory != null && $daoDseCategory->find()){
            while($daoDseCategory->fetch()){
                $path = $daoDseCategory->getPath(true);
                $breadcrums = '';
                foreach($path as $daoCategoryPath){
                    $breadcrums = $breadcrums.'� '.$daoCategoryPath->name.' ';
                }
                $options[$daoDseCategory->id_category] = $breadcrums;
            }
        }
    }

    # En caso que haya categor�as seleccionadas, las asigno al template
    $smarty->assign($params['assign'],count($options) > 0 ? $options : null);

    # Este plugin no retorna HTML
    return '';
}
################################################################################
?>