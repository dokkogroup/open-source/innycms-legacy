<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_contenido} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_contenido
 * <br>
 * Purpose: Retorna el Contenido correspondiente a una configuraci�n
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    - id_configuracion = ID de la configuraci�n
 *    - assign = nombre de variable a la que asignar� el DAO.
 * <br>
 * Examples:
 * <pre>
 * {inny_get_contenido id_configuracion=$daoConfiguracion->id_configuracion}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @link http://wiki.dokkogroup.com.ar/index.php/CMS%20ServiciosWeb {inny_get_contenido} (Inny wiki)
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_get_contenido($params, &$smarty) {

    # Verifico que est� el par�metro 'id_configuracion'
    if(empty($params['id_configuracion'])){
        Denko::plugin_fatal_error('el par�metro <b>id_configuracion</b> es requerido','inny_get_contenido');
    }

    # Verifico que est� el par�metro 'assign'
    if(empty($params['assign'])){
        Denko::plugin_fatal_error('el par�metro <b>assign</b> es requerido','inny_get_contenido');
    }

    # Obtengo el contenido y lo asigno al template
    $contenido = new Contenido($params['id_configuracion']);
    $smarty->assign($params['assign'],$contenido);

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################
?>