<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_contenidos} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_contenidos
 * <br>
 * Purpose:
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    -
 * - Opcionales:
 *   -
 * <br>
 * Examples:
 * <pre>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
 
require_once '../commons/dokko_configurator.php';
 
################################################################################
function smarty_function_inny_get_contenidos($params, &$smarty) {

    # Verifico que el parámetro 'idSitio' exista
    if(empty($params['idSitio'])){
        Denko::plugin_fatal_error('el parámetro <b>idSitio</b> es requerido','inny_get_contenidos');
    }

	$daoConfig = DB_DataObject::factory('configuracion');
	$daoConfig->indice1 = $params['idSitio'];

	$config = array();
	if ($daoConfig->find()){
		while ($daoConfig->fetch()){
			$config[] = new Contenido ($daoConfig->id_configuracion);
		}
	}
	if ($params['assign']){
		$smarty->assign($params['assign'],$config);
		return '';
	}

    return $config;
}
################################################################################
?>