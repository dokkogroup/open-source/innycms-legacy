<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_empleados} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_empleados
 * <br>
 * Purpose:
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    -
 * - Opcionales:
 *   -
 * <br>
 * Examples:
 * <pre>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_get_empleados($params, &$smarty) {

    # Verifico que el par�metro 'assign' exista
    if(empty($params['assign'])){
        Denko::plugin_fatal_error('el par�metro <b>assign</b> es requerido','inny_get_empleados');
    }

    #
    $empleados = array();
    if(!empty($params['addNullOption'])){
        $empleados['null'] = $params['addNullOption'];
    }

    # Asigno los estados al template
    $daoEmpleado = DB_DataObject::factory('empleado');
    $daoEmpleado->orderBy('username');
    $daoEmpleado->find();
    while($daoEmpleado->fetch()){
        $empleados[$daoEmpleado->id_empleado] = $daoEmpleado->username;
    }
    $smarty->assign($params['assign'],$empleados);

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################
?>