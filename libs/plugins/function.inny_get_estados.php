<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_estados} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_estados
 * <br>
 * Purpose:
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    -
 * - Opcionales:
 *   -
 * <br>
 * Examples:
 * <pre>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_get_estados($params, &$smarty) {

    # Verifico que el par�metro 'assign' exista
    if(empty($params['assign'])){
        Denko::plugin_fatal_error('el par�metro <b>assign</b> es requerido','inny_get_estados');
    }

    # Asigno los estados al template
    $estados = array();
    $daoEstado = DB_DataObject::factory('estado');
    $daoEstado->orderBy('nombre');
    $daoEstado->find();
    while($daoEstado->fetch()){
        $estados[$daoEstado->id_estado] = $daoEstado->nombre;
    }
    $smarty->assign($params['assign'],$estados);

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################
?>