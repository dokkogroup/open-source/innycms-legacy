<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_gadget} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_gadget
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_get_gadget}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_inny_get_gadget($params,&$smarty){

    # Obtengo el sitio actualmente logeado
    $id_sitio = Inny::getLoggedIdSitio();
    $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);

    # Formateo la URL (le agrego el prefijo "http://" en caso de no tenerlo)
    require_once $smarty->_get_plugin_filepath('modifier','dk_urlformat');
    $url = smarty_modifier_dk_urlformat($daoSitio->url.'/'.Inny::$url_gadget);

    # Obtengo el contenido del gadget
    $gadget_content = @file_get_contents($url);

    # En caso que haya contenido en el gadget
    if($gadget_content !== false){
        if(!empty($params['assign'])){
            $smarty->assign($params['assign'],$gadget_content);
            return '';
        }
        return $gadget_content;
    }

    # En caso de estar vac�o
    return '';
}
################################################################################
?>