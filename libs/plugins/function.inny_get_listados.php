<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_listados} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_listados
 * <br>
 * Purpose:
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    -
 * - Opcionales:
 *   -
 * <br>
 * Examples:
 * <pre>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_get_listados($params, &$smarty) {

    # Verifico que el parámetro 'idSitio' exista
    if(empty($params['idSitio'])){
        Denko::plugin_fatal_error('el parámetro <b>idSitio</b> es requerido','inny_get_listados');
    }

	$daoListado = DB_DataObject::factory('listado');
	$daoListado->id_sitio = $params['idSitio'];
	
	$listados = array();
	if ($daoListado->find()){
		while ($daoListado->fetch()){
			$listados[] = clone $daoListado;
		}
	}

	if ($params['assign']){
		$smarty->assign($params['assign'],$listados);
		return '';
	}

    return $listados;
}
################################################################################
?>