<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_nombre_tipo} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_nombre_tipo
 * <br>
 * Purpose:
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    -
 * - Opcionales:
 *   -
 * <br>
 * Examples:
 * <pre>
 * </pre>
 *
 * @author Denko Developers Team <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_function_inny_get_nombre_tipo($params, &$smarty) {

    # Verifico que exista el parámetro 'type'
    if(empty($params['type'])){
        Denko::plugin_fatal_error('el parámetro <b>type</b> es requerido','inny_get_nombre_tipo');
    }

    # Cargo el speech y retorno el nombre del tipo de dato
    InnyCMS_Speech::config_load($smarty,'tipo');

    # Obtengo el nombre del tipo
    $tipo_nombre = InnyCMS_Speech::getSpeech('tipo','tipo_'.strtolower($params['type']));

    # Asigno el valor
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$tipo_nombre);
        return '';
    }

    return $tipo_nombre;
}
################################################################################
?>