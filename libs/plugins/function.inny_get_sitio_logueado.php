<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_sitio_logueado} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_sitio_logueado
 * <br>
 * Purpose: obtiene el sitio actualmente logueado
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    - assign: nombre de la variable donde se asignará el DAO del sitio logueado
 * <br>
 * Examples:
 * <pre>
 *     {inny_get_sitio_logueado assign="daoSitio"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_get_sitio_logueado($params, &$smarty) {

    # Verifico que el parámetro 'assign' exista
    if(empty($params['assign'])){
        Denko::plugin_fatal_error('el parámetro <b>assign</b> es requerido','inny_get_sitio_logueado');
    }

    # Asigno el DAO del sitio actualmente
    $smarty->assign($params['assign'],Inny_Clientes::getSitioLogueado());

    # Este plugin no retorna HTML
    return '';
}
################################################################################
?>