<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_get_speech} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_get_speech
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_get_speech}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_inny_get_speech($params,&$smarty){

    # Verifico que el par�metro 'seccion' no est� vac�o
    if(empty($params['seccion'])){
        $cadena_error = 'el par�metro "seccion" es requerido.';
        InnyModule_Reporter::plugin_error(E_USER_ERROR,$cadena_error,'Smarty::inny_get_speech');
    }

    # Verifico que el p�rametro 'clave' no est� vac�o
    if(empty($params['clave'])){
        $cadena_error = 'el par�metro "clave" es requerido.';
        InnyModule_Reporter::plugin_error(E_USER_ERROR,$cadena_error,'Smarty::inny_get_speech');
    }
    
    # Arreglo de reemplazos
    $daoSitio = Inny_Clientes::getSitioLogueado();
    $reemplazos = array('%b%' => '</b>',
                        '%b' => '<b>',
                        '%i%' => '</i>',
                        '%i' => '<i>',
                        '\n' => '<br />',
                        '\t' => '&nbsp;&nbsp;&nbsp;&nbsp;',
                        '%sitio' => $daoSitio->url  
                        );
    
    # Obtengo el speech
    InnyCMS_Speech::config_load($smarty,$seccion);
    $speech = InnyCMS_Speech::getSpeech($params['seccion'],$params['clave'],$reemplazos);
    if (!empty($params['assign'])){
        $smarty->assign($params['assign'],$speech);
        return '';
    }
    
    return $speech;
}
################################################################################
?>