<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_htmloptions_sitio} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_htmloptions_empleado
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_htmloptions_empleado}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_htmloptions_empleado($params, &$smarty) {

    #
    $htmloptions_empleado = array();
    $daoEmpleado = DB_DataObject::factory('empleado');
    $daoEmpleado->selectAdd();
    $daoEmpleado->selectAdd('id_empleado,username');
    $daoEmpleado->orderBy('username');
    $daoEmpleado->find();
    while($daoEmpleado->fetch()){
        $htmloptions_empleado[$daoEmpleado->id_empleado] = $daoEmpleado->username;
    }

    #
    $params['options'] = $htmloptions_empleado;
    require_once $smarty->_get_plugin_filepath('function','html_options');
    return smarty_function_html_options($params,$smarty);
}
################################################################################
?>