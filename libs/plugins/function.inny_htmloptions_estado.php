<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {tracker_get_htmloptions_estado} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_htmloptions_estado
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_htmloptions_estado}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_htmloptions_estado($params, &$smarty) {

    #
    $htmloptions_estado = array();
    $daoEstado = DB_DataObject::factory('estado');
    $daoEstado->selectAdd();
    $daoEstado->selectAdd('id_estado,nombre');
    $daoEstado->orderBy('nombre');
    $daoEstado->find();
    while($daoEstado->fetch()){
        $htmloptions_estado[$daoEstado->id_estado] = $daoEstado->nombre;
    }

    #
    $params['options'] = $htmloptions_estado;
    require_once $smarty->_get_plugin_filepath('function','html_options');
    return smarty_function_html_options($params,$smarty);
}
################################################################################
?>