<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_htmloptions_sitio} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_htmloptions_sitio
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_htmloptions_sitio}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_htmloptions_sitio($params, &$smarty) {

    #
    $htmloptions_sitio = array();
    $daoSitio = DB_DataObject::factory('sitio');
    $daoSitio->selectAdd();
    $daoSitio->selectAdd('id_sitio,nombre_sitio');
    $daoSitio->orderBy('nombre_sitio');
    $daoSitio->find();
    while($daoSitio->fetch()){
        $htmloptions_sitio[$daoSitio->id_sitio] = $daoSitio->nombre_sitio;
    }

    #
    $params['options'] = $htmloptions_sitio;
    require_once $smarty->_get_plugin_filepath('function','html_options');
    return smarty_function_html_options($params,$smarty);
}
################################################################################
?>