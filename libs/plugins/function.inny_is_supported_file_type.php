<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_is_supported_file_type} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_is_supported_file_type
 * <br>
 * Purpose: Retorna si un tipo de archivo es soportado
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    - type = tipo de archivo.
 * - Opcionales:
 *   - assign = nombre de variable a la que asignar� el valor retornado.
 * <br>
 * Examples:
 * <pre>
 * {inny_is_supported_file_type type="sound" assign="is_supported"}
 * {capture assign="is_supported"}{inny_is_supported_file_type type="image"}{/capture}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_is_supported_file_type($params, &$smarty) {

    # Verifico que est� el par�metro 'type'
    if(empty($params['type'])){
        $cadena_error = 'el par�metro "type" es requerido';
        InnyModule_Reporter::plugin_error(E_USER_ERROR,$cadena_error,'Smarty::inny_is_supported_file_type');
    }

    # Obtengo si el archivo es soportado
    $is_supported = Inny::isSupportedFileType($params['type']);

    # Si est� seteado el par�metro 'assign', lo asigno al smarty en esa variable
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$is_supported);
        return '';
    }

    # Retorno si es soportado
    return $is_supported;
}
################################################################################
?>