<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_speech_load} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_speech_load
 * <br>
 * Purpose:
 * <br>
 * Input:
 * <br>
 * Examples:
 * <pre>
 *     {inny_speech_load section="header"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_function_inny_speech_load($params, &$smarty) {

    # Verifico que el parámetro 'section' exista
    if(empty($params['section'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'el parámetro <b>assign</b> es requerido','inny_speech_load');
    }

    # Cargo la seccion de speechs
    else{
        InnyCMS_Speech::config_load($smarty,$params['section']);
    }

    # Este plugin no retorna HTML
    return '';
}
################################################################################
?>