<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_type_get_abm_speech} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_type_get_abm_speech
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_type_get_abm_speech}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_inny_type_get_abm_speech($params,&$smarty){

    # Verifico que el par�metro 'assign' no est� vac�o
    if(empty($params['assign'])){
        $cadena_error = 'el par�metro "assign" es requerido.';
        InnyModule_Reporter::plugin_error(E_USER_ERROR,$cadena_error,'Smarty::inny_type_get_abm_speech');
    }

    # Verifico que el tipo de dato est� seteado
    if(empty($params['innyType'])){
        $cadena_error = 'el par�metro "innyType" es requerido.';
        InnyModule_Reporter::plugin_error(E_USER_ERROR,$cadena_error,'Smarty::inny_type_get_abm_speech');
    }

    # Obtengo el innyType
    $innyType = $params['innyType'];
    $innyType->setSmarty($smarty);

    # Obtengo los speechs del ABM, dependiendo del tipo de dato
    InnyCMS_Speech::config_load($smarty,'archivo');
    $smarty->assign(
        $params['assign'],
        array(
            'deleteLinkText'  => InnyCMS_Speech::getSpeech('archivo','archivo_'.$innyType->type.'EliminarLinkText'),
            'deleteLinkTitle' => InnyCMS_Speech::getSpeech('archivo','archivo_'.$innyType->type.'EliminarLinkTitle')
        )
    );

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################
?>