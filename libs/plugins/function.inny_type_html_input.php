<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_type_html_input} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_type_html_input
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_type_html_input}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_inny_type_html_input($params,&$smarty){
    $innyType = $params['innyType'];
    $innyType->setSmarty($smarty);
    $value = isset($_POST[$params['name']]) ? $_POST[$params['name']] : (isset($params['value']) ? $params['value'] : null);
    $innyType->setValue($value);

    $html_params = array();
    foreach($params as $param => $value){
        if($param == 'innyType') continue;
        $html_params[$param] = $value;
    }
    return $innyType->htmlInput($html_params);
}
################################################################################
?>