<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_type_view} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_type_view
 * <br>
 * Purpose:
 * <br>
 * Examples:
 * <pre>
 * {inny_type_view}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return string
 */

/**
 * @param innyType
 * @param size = tiny | medium | normal
 * @param value
 */
function smarty_function_inny_type_view($params,&$smarty){

    # Verifico que el par�metro 'assign' no est� vac�o
    if(empty($params['assign'])){
        $cadena_error = 'el par�metro "assign" es requerido.';
        InnyModule_Reporter::plugin_error(E_USER_ERROR,$cadena_error,'Smarty::inny_type_view');
    }

    # Verifico que el par�metro 'assign' no est� vac�o
    if(empty($params['innyType'])){
        $cadena_error = 'el par�metro "innyType" es requerido.';
        InnyModule_Reporter::plugin_error(E_USER_ERROR,$cadena_error,'Smarty::inny_type_view');
    }

    # Obtengo el innyType
    $innyType = $params['innyType'];
    $innyType->setSmarty($smarty);

    # Obtengo el tipo de dato
    $tipo = $innyType->getMetadataValue('tipo');

    # Obtengo el tama�o del preview
    $size = !empty($params['size']) ? $params['size'] : 'normal';

    #
    $value = isset($params['value']) && $params['value'] != '' ? $params['value'] : null;
    $view_data = null;
    $innyType->setValue($value);



    if($innyType->getValue() != null){

        # ============================= ARCHIVOS ===================================
        if(Inny::isSupportedFileType($tipo)){
            $id_temporal = $value;
            $daoTemporalInfo = InnyCore_File::getTemporalInfo($id_temporal);
            if($daoTemporalInfo){
                switch($size){
                    case 'tiny':
                        $thumb_settings = array('width'=>80,'height'=>80,'quality'=>75);
                        break;

                    case 'medium':
                        $thumb_settings = array('width'=>200,'height'=>200,'quality'=>80);
                        break;

                    default:
                        $thumb_settings = array('width'=>300,'height'=>300,'quality'=>90);
                        break;
                }
                $view_data = $innyType->getViewLinks($id_temporal,$daoTemporalInfo['name'],$thumb_settings);
            }
        }

        # ============================== TEXTO =====================================
        else{
            switch($size){
                case 'tiny':
                    $view_data = $innyType->preview_summary(100);
                    break;
                case 'medium':
                    $view_data = $innyType->preview_summary(100);
                    break;
                default:
                    $view_data = $innyType->preview_normal();
                    break;
            }
        }
    }

    $smarty->assign($params['assign'],$view_data);
    return '';
}
################################################################################
?>