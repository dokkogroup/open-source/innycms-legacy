<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {inny_wysiwyg} function plugin
 *
 * Type: function
 * <br>
 * Name: inny_wysiwyg
 * <br>
 * Purpose: Genera el HTML correspondiente a un WYSIWYG
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    - name: nombre del editor
 * - Opcionales:
 *   - value: contenido por defecto:
 *   - width: ancho del editor
 *   - height: alto del editor
 *   - onLoadEvent: evento en el onload (no me acuerdo para que estaba... igual es raro que se use)
 * <br>
 * Examples:
 * <pre>
 *   {inny_wysiwyg}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_inny_wysiwyg($params,&$smarty) {

    # Verifico que exista el par�metro 'name'
    if(empty($params['name'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'el par�metro name es requerido','inny_wysiwyg');
        return '';
    }

    # Incluyo el JS del WYSIWYG
    smarty_function_dk_include(
        array(
            'file'          => 'ckeditor/ckeditor.js',
            'ignoreVersion' => true,
            'inline'        => false,
            'compress'      => false
        ),
        $smarty
    );

    # Toolbar
    $toolbar = !empty($params['toolbar']) ? json_encode($params['toolbar']) : false;
    
    $plaintext=!empty($params['plaintext'])?$params['plaintext']:false;

    # Genero el HTML para el WYSIWYG
    $html = '
<textarea id="'.$params['name'].'" name="'.$params['name'].'" style="width:550px;height:300px;">'.(isset($params['value']) ? $params['value'] : '').'</textarea>
<script type="text/javascript">
//<![CDATA[
	var plain="'.$plaintext.'";
	if (plain=="true"){
    	CKEDITOR.config.forcePasteAsPlainText = true;
        CKEDITOR.config.keystrokes =[[ CKEDITOR.CTRL + 86 /*V*/,"pastefromword"]];
    }
CKEDITOR.replace("'.$params['name'].'",{
    customConfig : "ckeditor_inny_config.js",
    '.(!empty($toolbar) ? 'toolbar : ['.$toolbar.'],' : '').'
    on : {
        instanceReady : function( ev ){
            this.dataProcessor.writer.setRules("p",{
                indent : false,
                breakBeforeOpen : true,
                breakAfterOpen : false,
                breakBeforeClose : false,
                breakAfterClose : true
            });
        }
    }
});
//]]>
</script>';

    # En caso que tenga que el c�digo generado tenga que asignarlo al template
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$html);
        return '';
    }

    # En caso que deba retornar el c�digo generado
    return $html;
}
################################################################################