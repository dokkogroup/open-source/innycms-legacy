<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_get_ga_code} function plugin
 *
 * Type: function
 * <br>
 * Name: sw_get_ga_code
 * <br>
 * Purpose: Retorna el c�digo correspondiente al google analytics
 * <br>
 * Input:
 * <br>
 * - Opcionales:
 *   - assign = nombre de variable a la que asignar� el valor retornado.
 * <br>
 * Examples:
 * <pre>
 * {sw_get_ga_code}
 * {sw_get_ga_code assign="ga_code"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20sw_get_ga_code {sw_get_ga_code} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_sw_get_ga_code($params, &$smarty) {

    # Obtengo el DAO del Sitio actual
    $daoSitio = Inny::getDaoSitio();

    # Obtengo el ga code del DAO del Sitio actual. Si existe, genero el js
    $ga_code = '';
    if($daoSitio->ga_codigo){
        $ga_code = '
<!-- GOOGLE ANALYTICS -->
<script type="text/javascript">
//<![CDATA[
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
	var pageTracker = _gat._getTracker("'.$daoSitio->ga_codigo.'");
	pageTracker._trackPageview();
//]]>
</script>';
    }

    # En caso de existir el par�metro assign, asigno el c�digo al template
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$ga_code);
        return '';
    }

    # Retorno el c�digo
    return $ga_code;
}
################################################################################
?>