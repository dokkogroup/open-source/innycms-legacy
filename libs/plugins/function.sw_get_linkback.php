<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_get_linkback} function plugin
 *
 * Type: function
 * <br>
 * Name: sw_get_linkback
 * <br>
 * Purpose: Obtiene el link a la p�gina anterior
 * <br>
 * Input:
 * <br>
 *
 * - Opcionales:
 *   - default = URL por default (default: index.php)
 * <br>
 * Examples:
 * <pre>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20sw_get_linkback {sw_get_linkback} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_sw_get_linkback($params, &$smarty) {
    require_once $smarty->_get_plugin_filepath('function','dk_basehref');

    # Obtengo la URL por default
    $default = !empty($params['default']) ? $params['default'] : smarty_function_dk_basehref(array(),$smarty);

    # Obtengo el referer
    $referer = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;

    # En caso que no haya referer, retorno la URL por default
    if($referer == null){
    	return $default;
    }

    # Obtengo el host del referer
    preg_match('@^(?:http://)?([^/]+)@i',$referer,$coincidencias);

    # En caso que el referer provenga del mismo host
    return ($_SERVER['HTTP_HOST'] == $coincidencias[1]) ? $referer : $default;
}
################################################################################
?>
