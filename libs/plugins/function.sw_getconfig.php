<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_getconfig} function plugin
 *
 * Type: function
 * <br>
 * Name: sw_getconfig
 * <br>
 * Purpose: Retorna contenido (texto, imagen) correspondiente al sitio actual.
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    - name = nombre del contenido. Si no existe lo crea.
 *
 * - Opcionales:
 *   - type = tipo de contenido.
 *   - default = valor por defecto que tomar� el contenido cuando es creado.
 *     Solo es necesario en los contenidos del tipo string.
 *   - width = ancho de la imagen. El contenido tomar� este valor cuando es creado.
 *     Solo es necesario en los contenidos del tipo image.
 *   - height = alto de la imagen. El contenido tomar� este valor cuando es creado.
 *     Solo es necesario en los contenidos del tipo image.
 *   - assign = nombre de variable a la que asignar� el valor retornado.
 *     Digamos, la funci�n sw_getconfig siempre retorna el valor del contenido,
 *     excepto que se pase par�metro assign. De esta manera, no lo retorna,
 *     mas bien lo asigna a la variable definida en el par�metro assign.
 *   - mode = Modo en que retorna los datos. En caso que mode sea url, retorna el
 *     valor de la configuraci�n en forma de url. Es muy �til cuando el
 *     contenido es de tipo image, ya que retorna la url a la imagen que guarda
 *     la configuraci�n.
 * <br>
 * Examples:
 * <pre>
 * {sw_getconfig name="texto_principal" type="string"}
 * {sw_getconfig name="titulo_principal" type="string" default="Pernicious Rising"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20sw_getconfig {sw_getconfig} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_sw_getconfig($params, &$smarty) {

    # Verifico que est� el par�metro 'name'
    if(empty($params['name'])){
        $cadena_error = 'el par�metro name es requerido';
        InnyModule_Reporter::plugin_error(E_USER_WARNING,$cadena_error,'sw_getconfig');
        return '';
    }

    # Verifico si est� seteado el par�metro correspondiente al sitio
    # (default: el ID del Sitio logueado)
    $id_sitio = !empty($params['id_sitio']) ? $params['id_sitio'] : Inny::getActualSitio();

    # Obtengo el nombre de la configuraci�n
    $nombre = $params['name'];

    # Verifico que la configuraci�n exista
    $daoConfiguracion = DB_DataObject::factory('configuracion');
    $daoConfiguracion->nombre = $nombre;
    $daoConfiguracion->indice1 = $id_sitio;

    # ================ EN CASO QUE DEBA CREAR LA CONFIGURACION =============== #
    if(!$daoConfiguracion->find()){

        # Verifico si el sitio puede crear configuraciones
        $daoSitio = Inny::getDaoSitioActual();
        if($daoSitio->crea_listados == '0'){
            $cadena_error = 'El sitio no tiene permisos para crear el contenido "'.$nombre.'". Debe definirse en la metadata.';
            InnyModule_Reporter::plugin_error(E_USER_WARNING,$cadena_error,'sw_getconfig');
            return null;
        }

        # En caso que la configuraci�n no exista en la DB, el m�todo getConfig la crea
        $config = getConfig($nombre,$id_sitio);

        # Obtengo la configuraci�n de la DB
        $contenido = new Contenido($nombre,$id_sitio);

        # Obtengo el tipo de dato del Contenido
        $type = isset($params['type']) ? strtolower($params['type']) : InnyCore_Metadata::$defaultMetadata_Contenido['tipo'];

        # Seteo el tipo de contenido
        $contenido->setConfig('tipo',$type);

        switch($type){

            # ===================== TIPOS DE ARCHIVO ========================= #

            # Tipo IMAGE
            case 'image':
                $contenido->tipo = configuracion_image;

                # Si el ancho est� en los par�metros, se lo seteo
                if(!empty($params['width'])){
                    $contenido->setConfig('parametros','width',$params['width']);
                }

                # Si el alto est� en los par�metros, se lo seteo
                if(!empty($params['height'])){
                    $contenido->setConfig('parametros','height',$params['height']);
                }

                # Si la calidad est� en los par�metros, se la seteo
                if(!empty($params['quality'])){
                    $contenido->setConfig('parametros','quality',$params['quality']);
                }
                break;

            # Tipo FLASH
            case 'flash': $contenido->tipo = configuracion_image; break;

            # Tipo SOUND
            case 'sound': $contenido->tipo = configuracion_sound; break;

            # Tipo FILE
            case 'file': $contenido->tipo = configuracion_image; break;

            # ====================== TIPOS DE TEXTO ========================== #

            # Tipo TEXT
            case 'text': $contenido->tipo = configuracion_string; break;

            # Tipo TEXTAREA
            case 'textarea': $contenido->tipo = configuracion_string; break;

            # Tipo RICHTEXT (alias: string)
            case 'string': $contenido->setConfig('tipo','richtext');
            case 'richtext': $contenido->tipo = configuracion_string; break;

            # Tipo INTEGER
            case 'integer': $contenido->tipo = configuracion_integer; break;

            # Tipo FLOAT
            case 'float': $contenido->tipo = configuracion_string; break;

            # Tipo SELECT (alias: multiselect)
            case 'multiselect': $contenido->setConfig('tipo','select');
            case 'select': $contenido->tipo = configuracion_multiselect; break;

            # Tipo DATE
            case 'date': $contenido->tipo = configuracion_date; break;

            # Tipo TIME
            case 'time': $contenido->tipo = configuracion_time; break;

            # Tipo DATETIME
            case 'datetime': $contenido->tipo = configuracion_string; break;

            # Tipo CHECKBOX
            case 'checkbox': $contenido->tipo = configuracion_string; break;

            # Tipo RADIO
            case 'radio': $contenido->tipo = configuracion_string; break;
        }

        # Le seteo el estado en 1
        $contenido->estado = 1;

        # En caso que est� seteado un par�metro por default
        if(isset($params['default'])){
            $contenido->valor = $params['default'];
            $config = $params['default'];
        }

        # Actualizo los datos
        $contenido->update();
    }

    # Si ya est� creada la configuraci�n previamente, obtengo el contenido normalmente
    else{

        $contenido = new Contenido($nombre,$id_sitio);

    }

    # En caso que el valor deba retornarlo como url
    if(!empty($params['urltype']) && ($urltype = strtolower(trim($params['urltype']))) != '' && Inny::isSupportedFileType($urltype)){
        $tipo = $urltype;
    }
    else{
        $tipo = $contenido->getMetadataValue('tipo');
    }

    # En caso que el tipo de contenido sea archivo
    if(Inny::isSupportedFileType($tipo)){

        # Obtengo el ID y datos del archivo nombre del archivo
        $id_temporal = !empty($contenido->valor) ? trim($contenido->valor) : null;

        # Si el contenido no tiene una imagen asignada, seteo NULL
        if(empty($id_temporal)){
        	$config = null;
        }

        else{

            # Obtengo la informaci�n del archivo
            $file_info = InnyCore_File::getTemporalInfo($id_temporal);

            # En caso que no exista el archivo temporal, retorno NULL
            if($file_info != null){
                $innyType = InnyType::factory($tipo);
                $config = $innyType->getContentUrl($id_temporal,$file_info['name'],$params);
            }

            # Sin�, retorno NULL
            else{
                $config = null;
            }
        }
    }

    # En caso que el tipo de dato sea de Texto
    else{
        $innyType = $contenido->createInnyType();
        $innyType->setValue($contenido->valor);
        $config = $innyType->getValue();
    }

    # En caso que deba asignar el valor en el template
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$config);
        return '';
    }

    # Retorno el valor de la configuraci�n
    return $config;
}
################################################################################
?>