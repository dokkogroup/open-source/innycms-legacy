<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_is_allowed_img_mimetype} function plugin
 *
 * Type: function
 * <br>
 * Name: sw_is_allowed_img_mimetype
 * <br>
 * Purpose: Retorna si el mime de una imagen es soportado por el sistema
 * Input:
 * <br>
 * - Requeridos
 *   - mime: mime de la imagen
 * - Opcionales:
 *   - assign: nombre de la variable a la que se asignará el resultado
 * <br>
 * Example:
 * <pre>
 * {sw_is_allowed_img_mimetype mime=$daoProductoTemporal->mime assign="is_allowed"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20sw_is_allowed_img_mimetype {sw_is_allowed_img_mimetype} (Inny wiki)
 * @param array $params parámetros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
require_once '../commons/inny/core.image.php';
################################################################################
function smarty_function_sw_is_allowed_img_mimetype($params, &$smarty) {

    # Verifico que el parámetro 'mime' exista
    if(empty($params['mime'])){
        $cadena_error = 'El parámetro mime es requerido.';
        InnyModule_Reporter::plugin_error(E_USER_WARNING,$cadena_error,'sw_is_allowed_img_mimetype');
        return '';
    }

    #
    $result = in_array($params['mime'],InnyCore_Image::getAllowedImageMime());
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$result);
        return '';
    }

    # Retorno el resultado
    return $result;
}
################################################################################
?>