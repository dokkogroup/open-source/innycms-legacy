<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_is_content} function plugin
 *
 * Type: function
 * <br>
 * Name: sw_is_content
 * <br>
 * Purpose: Retorna si un dato tiene contenido, o simplemente est� vac�o
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *    - content = contenido que evaluar.
 * - Opcionales:
 *   - assign = nombre de variable a la que asignar� el valor retornado.
 * <br>
 * Examples:
 * <pre>
 *     {sw_is_content content=$texto1 assign="is_content"}
 *     {if $is_content}
 *         {$texto1}
 *     {else}
 *         El contenido a�n est� vac�o...
 *     {/if}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20sw_is_content {sw_is_content} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_sw_is_content($params, &$smarty) {

    # Verifico que est� el valor en el par�metro 'content' sea v�lido
    $is_content = isset($params['content']) ? !InnyCore_Dao::isEmpty($params['content']) : false;

    # Si est� seteado el par�metro 'assign', lo asigno al smarty en esa variable
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$is_content);
        return '';
    }

    # Retorno si el contenido no es vacio
    return $is_content;
}
################################################################################
?>