<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_linesplit} function plugin
 *
 * Type: function
 * <br>
 * Name: sw_linesplit
 * <br>
 * Purpose: Al igual que el plugin swp_split, convierte en arreglo los datos
 *          contenidos en un texto. La diferencia es que el delimitador ser� el br,
 *          limpia todo el contenido de tags HTML, no retorna �ndices vac�os,
 *          elimina todos los saltos de l�nea y elimina todos los espacios al
 *          comienzo y final (o sea, aplica trim).
 *          Es ideal para contenidos que est�n separados por lineas y deben
 *          manipularse por separado (por ejemplo listados de mails, items de
 *          botoneras, etc).
 *
 * Input:
 * <br>
 * - Requeridos
 *   - value: contenido HTML que ser� trozado
 *   - assign: nombre de la variable a la que se asignar� el arreglo de valores
 * <br>
 * Example:
 * <pre>
 * {sw_linesplit value=$texto2 assign="texto2"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20sw_linesplit {sw_linesplit} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_sw_linesplit($params, &$smarty){

    # Verifico que exista el par�metro 'value'
	if(empty($params['value'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro value es requerido','sw_linesplit');
        return '';
	}

	# Verifico que exista el par�metro 'assign'
	if(empty($params['assign'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro assign es requerido','sw_linesplit');
        return '';
	}

	# Elimino toda nueva l�nea y todo retorno de carro
    $params['value'] = str_replace("\n",'',str_replace("\r",'',$params['value']));

    # Reemplazo todo br por "\n". Luego lo separo en un arreglo
    $result = explode("\n",preg_replace('/<br\s*\/?>/i',"\n",$params['value']));

    # Elimino todo tag y entidad HTML del contenido
    for($i = 0; $i < count($result); $i++){
        $result[$i] = trim(html_entity_decode(strip_tags($result[$i])));
    }

    # Elimino todo item vac�o en el arreglo
    $result = array_values(array_diff($result,array('')));

    # Por �ltimo, asigno el arreglo a la variable, y no retorno contenido HTML
    $smarty->assign($params['assign'],$result);
	return '';
}
################################################################################
?>