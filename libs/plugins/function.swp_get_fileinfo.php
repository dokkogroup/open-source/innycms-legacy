<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_get_fileinfo} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_get_fileinfo
 * <br>
 * Purpose: Obtiene el nombre y mime de un archivo. Las variables son $filename
 *          para el nombre y $mime para el mime del archivo.
 * Input:
 * <br>
 * - Opcionales:
 *  - id = id del archivo (default: lo obtiene de la variable $id_archivo que
 *    est� seteada en ese momento en el template). Si el archivo no existe,
 *    no asigna valor alguno.
 *  - assign = nombre de la variable donde se asignar� el resultado
 *    (default: lo setea en variables de template).
 * <br>
 * Example:
 * <pre>
 * <table cellpadding="2" cellspacing="2">
 *   {swp_lister name="archivos" resultsPerPage="30" orderBy="posicion DESC" atributos="maxAttach:6"}
 *     {swp_get_fileinfo}
 *     {if !empty($mime)} {* Primero me fijo si hay archivo que descargar *}
 *       <tr>
 *         <td>
 *           {if $mime == "application/msword"}
 *             <img src="images/icon_word.gif" />
 *           {elseif $mime == "application/pdf"}
 *             <img src="images/icon_pdf.gif" />
 *           {/if}
 *         </td>
 *         <td><a href="{swp_url type="file"}" title="descargar el archivo {$filename}">{$filename}</a></td>
 *       </tr>
 *     {/if}
 *   {/swp_lister}
 * </table>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_get_fileinfo {swp_get_fileinfo} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_swp_get_fileinfo($params, &$smarty){

    # Asigno la variable 'id_archivo', dependiendo si se pas� por par�metro o si
    # la obtengo desde el template
    $id_archivo = !empty($params['id']) ? $params['id'] : $smarty->get_template_vars('id_archivo');
    if(empty($id_archivo)){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'Son requeridos el par�metro id o la variable de template id_archivo','swp_get_fileinfo');
        return '';
    }

    # Obtengo la informaci�n del archivo
    $temporalInfo = InnyCore_File::getTemporalInfo($id_archivo);
    if(!empty($temporalInfo)){

        # Seteo la informaci�n del archivo
        $info = array(
            'filename' => $temporalInfo['name'],
            'mime' => $temporalInfo['metadata']['mime'],
            'size' => $temporalInfo['size']
        );

        # En caso que exista descripci�n, la seteo
        if(isset($temporalInfo['metadata']['description'])){
            $info['description'] = $temporalInfo['metadata']['description'];
        }

        # Si est� seteado el par�metro 'assign', lo asigno a esa variable
        if(!empty($params['assign'])){
            $smarty->assign($params['assign'],$info);
        }

        # Sin�, lo asigno directamente en el template
        else{
            foreach($info as $key => $value){
                $smarty->assign($key,$value);
            }
        }
    }

    # Este plugin no retorna contenido HTML
    return '';
}
################################################################################
?>