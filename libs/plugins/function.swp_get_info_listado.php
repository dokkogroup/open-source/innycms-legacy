<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_get_info_listado} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_get_info_listado
 * <br>
 * Purpose: Retorna los datos principales del Listado.
 *
 * Input:
 * <br>
 * - Requeridos:
 *  - name = nombre del listado
 *  - assign = variable del template donde se setear� la informaci�n del listado
 * <br>
 * Example:
 * <pre>
 *     {swp_get_info_listado name="bicicletas" assign="data_listado" }
 *     <head>
 *         <title>{data_listado.nombre} :: Inicio</title>
 *         ...
 *     </head>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_get_info_sitio {swp_get_info_sitio} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_swp_get_info_listado($params, &$smarty){

    # Verifico que el par�metro 'assign' exista
    if(empty($params['assign'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'el par�metro assign es requerido','swp_get_info_listado');
        return '';
    }

    # Verifico que el par�metro 'nombre' exista
    if(empty($params['name'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'el par�metro name es requerido','swp_get_info_listado');
        return '';
    }

    $data = null;

    # Obtengo el DAO correspondiente al sitio actual
    $daoSitio = InnyCore_Dao::getDao('sitio',Inny::getActualSitio());

    $daoListado = DB_DataObject::factory('listado');
    $daoListado->whereAdd('nombre = \''.$params['name'].'\' and id_sitio = \''.$daoSitio->id_sitio.'\'');
    if($daoListado->find(true)){
		$data = array();
    	$data['id_listado'] = $daoListado->id_listado;
    	$data['nombre'] = $daoListado->nombre;
    	$daoProducto = DB_DataObject::factory('producto');
    	$daoProducto->id_listado = $daoListado->id_listado;
    	$data['cantidad_elementos'] = $daoProducto->count();
    }

    # Seteo el arreglo al template
    if(isset($data)){
    	$smarty->assign($params['assign'],$data);
    }

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################
?>
