<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_get_info_sitio} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_get_info_sitio
 * <br>
 * Purpose: Retorna los datos principales del Sitio.
 *
 * Input:
 * <br>
 * - Requeridos:
 *  - assign = variable del template donde se setear� la informaci�n del sitio
 * <br>
 * Example:
 * <pre>
 *     {swp_get_info_sitio assign="data_sitio"}
 *     <head>
 *         <title>{data_sitio.nombre_sitio} :: Inicio</title>
 *         ...
 *     </head>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_get_info_sitio {swp_get_info_sitio} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_swp_get_info_sitio($params, &$smarty){

    # Verifico que el par�metro 'assign' exista
    if(empty($params['assign'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'el par�metro assign es requerido','swp_get_info_sitio');
        return '';
    }

    # Obtengo el DAO correspondiente al sitio actual
    $daoSitio = InnyCore_Dao::getDao('sitio',Inny::getActualSitio());

    # Seteo sus datos en el arreglo de datos
    $array = array(
        'nombre_sitio' => $daoSitio->nombre_sitio,
        'url'          => $daoSitio->url,
        'smtp' => array(
            'fromname' => $daoSitio->getSmtpValue('fromname'),
            'contacto' => $daoSitio->getSmtpValue('contacto')
        )
	);

    # Seteo el arreglo al template
    $smarty->assign($params['assign'],$array);

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################
?>