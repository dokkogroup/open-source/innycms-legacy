<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_get_listado} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_get_listado
 * <br>
 * Purpose: Obtiene el DAO de un listado correspondiente al sitio actual.
 * Input:
 * <br>
 * - Requeridos
 *   - name: nombre del listado
 *   - assign: nombre de la variable donde se asignar� el resultado
 * <br>
 * Example:
 * <pre>
 * {swp_get_listado name="bicicletas" assign="listadoBicicletas"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_get_listado {swp_get_listado} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_swp_get_listado($params, &$smarty){

    # Verifico que exista el par�metro 'assign'
	if(empty($params['assign'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro assign es requerido','swp_get_listado');
        return '';
	}

	# Verifico que exista el par�metro 'name'
	if(empty($params['name'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro name es requerido','swp_get_listado');
        return '';
	}

    # Busco el listado en la DB
    $daoListado = DB_DataObject::factory('listado');
    $daoListado->whereAdd('nombre = \''.strtolower($params['name']).'\' and id_sitio = '.Inny::getActualSitio());

    # En caso que el listado no exista, lanzo el error
    if(!$daoListado->find(true)){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El listado de nombre '.$params['name'].' no existe','swp_get_listado');
        return '';
    }

    # Asigno el listado al template
    $smarty->assign($params['assign'],$daoListado);

    # Este plugin no retorna c�digo HTML
	return '';
}
################################################################################
?>