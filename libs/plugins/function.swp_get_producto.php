<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_get_producto} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_get_producto
 * <br>
 * Purpose: Obtiene todos los datos de un producto, incluyendo los ids de sus
 *          archivos asociados. Puede asignarlos directamente en el template o
 *          asignarlo a una variable.
 * Input:
 * <br>
 * - Requeridos
 *   - id: id del producto
 *   - listado: nombre del listado que contiene al producto
 * - Opcionales:
 *   - assign: nombre de la variable donde se asignar� el resultado
 *     (default: lo setea en variables de template)
 * <br>
 * Example:
 * <pre>
 * {swp_get_producto id="41" listado="bicicletas"}
 * <ul>
 *   <li>Nombre: {$texto1}</li>
 *   <li>Descripcion: {$texto2}</li>
 *   <li>Observaci�n: {$texto3}</li>
 *   <li>Im�genes:
 *     <ul>
 *     {foreach from=$ids_archivo item="id_imagen"}
 *       <li><img src="{swp_url id_archivo=$id_imagen}" alt="{$texto1}"/></li>
 *     {/foreach}
 *     </ul>
 *   </li>
 * </ul>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_get_producto {swp_get_producto} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_swp_get_producto($params, &$smarty){

    # Verifico que exista el par�metro 'id'
	if(empty($params['id'])){
	    InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro id es requerido','swp_get_producto');
        return '';
	}

	if(empty($params['listado'])){
	    InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro listado es requerido','swp_get_producto');
        return '';
	}

	$daoProducto = Inny::getProducto($params['id'],$params['listado']);
    if(!$daoProducto){
        InnyModule_Reporter::plugin_error(E_ERROR,'No existe un producto con ID '.$params['id'].' en el listado '.$params['listado'],'swp_get_producto');
		return '';
	}

    # Obtengo los valores del Producto
    $valores = $daoProducto->getValores();

    # Seteo los valores en el template
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$valores);
    }
    else{
        foreach($valores as $key => $value){
            $smarty->assign($key,$value);
        }
    }

	# Este plugin no retorna c�digo HTML
	return '';
}
################################################################################
