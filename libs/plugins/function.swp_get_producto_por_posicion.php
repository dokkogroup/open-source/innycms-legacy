<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_get_producto_por_posicion} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_get_producto_por_posicion
 * <br>
 * Purpose: Obtiene todos los datos de un producto, incluyendo los ids de sus
 *          archivos asociados. Pueden asignarse directamente en el template o a una variable.
 *
 * Input:
 * <br>
 * - Requeridos
 *   - nombre_listado: nombre del listado al que pertenece el producto
 *   - posicion: nro de posici�n en el listado
 *
 * - Opcionales:
 *   - assign: nombre de la variable donde se asignar� el resultado
 *     (default: lo setea en variables de template)
 * <br>
 * Example:
 * <pre>
 * {swp_get_producto_por_posicion id="41"}
 * <ul>
 *   <li>Nombre: {$texto1}</li>
 *   <li>Descripcion: {$texto2}</li>
 *   <li>Observaci�n: {$texto3}</li>
 *   <li>Im�genes:
 *     <ul>
 *     {foreach from=$ids_archivo item="id_imagen"}
 *       <li><img src="{swp_url id_archivo=$id_imagen}" alt="{$texto1}"/></li>
 *     {/foreach}
 *     </ul>
 *   </li>
 * </ul>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_get_producto_por_posicion {swp_get_producto_por_posicion} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_swp_get_producto_por_posicion($params, &$smarty){

    # Verifico que exista el par�metro 'nombre_listado'
	if(empty($params['nombre_listado'])){
	    InnyModule_Reporter::plugin_error(E_USER_WARNING,'Error: El par�metro "nombre_listado" es requerido','swp_get_producto_por_posicion');
        return '';
	}

	# Verifico que exista el par�metro 'posicion'
	if(empty($params['posicion'])){
	    InnyModule_Reporter::plugin_error(E_USER_WARNING,'Error: El par�metro "posicion" es requerido','swp_get_producto_por_posicion');
        return '';
	}

    # Obtengo el Producto
    $daoProducto = Inny::getProductoPorPosicion($params['nombre_listado'],$params['posicion']);
    if(!empty($daoProducto)){

        # Obtengo los valores del Producto
        $valores = $daoProducto->getValores();

        # Seteo los valores en el template
        if(!empty($params['assign'])){
            $smarty->assign($params['assign'],$valores);
        }
        else{
            foreach($valores as $key => $value){
                $smarty->assign($key,$value);
            }
        }
    }

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################
?>