<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_hrefpage} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_hrefpage
 * <br>
 * Purpose: Sirve para crear una URL hacia el nro de p�gina que recibe como
 *          par�metro. Esta funci�n solo debe usarse dentro del bloque SW Lister
 *          Paginator.
 * Input:
 * <br>
 * - Requeridos:
 *   - number = n�mero de p�gina.
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_hrefpage {swp_hrefpage} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_swp_hrefpage($params,&$smarty){

    # Verifico que exista el parametro 'number'
    if(empty($params['number'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro number es requerido','swp_hrefpage');
        return '';
    }

    # Obtengo el nombre del paginador al cual hace referencia el lister
    $parent_name = Denko::getSmartyParentTag($smarty,'swp_paginator');

    # Obtengo el par�metro a la p�gina del DAOLister correspondiente
    $daoLister  = &$GLOBALS['DK_LISTER'][$parent_name];
    $paramPage  = $daoLister->getParamPage();

    # Armo el query string para el href hacia el nro de p�gina solicitada
    $querystring = '';
    foreach($_GET as $param => $value){
        if($param == $paramPage) continue;
        $querystring.= '&'.$param.'='.$value;
    }

    # Armo y retorno el link
    return basename($_SERVER['PHP_SELF']).'?'.$paramPage.'='.$params['number'].$querystring;
}
################################################################################
?>