<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_split} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_split
 * <br>
 * Purpose: Convierte en arreglo los datos contenidos en un texto.
 * Input:
 * <br>
 * - Requeridos
 *   - value: valor que ser� trozado
 *   - assign: nombre de la variable a la que se asignar� el arreglo de valores
 *   - delimiter: delimitador de las subcadenas
 * - Opcionales:
 *   - regex: indica si el delimitador es una expresi�n regular (default: false)
 *   - allowEmpty: indica si el arreglo contendr� items vac�os (default: false)
 * <br>
 * Example:
 * <pre>
 * {swp_split value=$texto2 delimiter="<br>" assign="texto2"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_split {swp_split} (Inny wiki)
 * @param array $params par�metros
 * @param Smarty &$smarty instancia de smarty
 * @return string
 */
################################################################################
function smarty_function_swp_split($params, &$smarty){

    # Verifico que exista el par�metro 'value'
	if(empty($params['value'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro value es requerido','swp_split');
        return '';
	}

	# Verifico que exista el par�metro 'assign'
	if(empty($params['assign'])){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro assign es requerido','swp_split');
        return '';
	}

	# Verifico que exista el par�metro 'delimiter'
	if(empty($params['delimiter'])){
	    InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro delimiter es requerido','swp_split');
        return '';
	}

    # En caso que hayan pasado una expresi�n regular, ejecuto el split con tal
    # expresi�n. Sin�, uso el explode
	$result = array();
	if(!empty($params['regex'])){
		$result = split($params['delimiter'], $params['value']);
	}
    else{
		$result = explode($params['delimiter'], $params['value']);
	}

	# En caso que deba eliminar los items en el arreglo que est�n vac�os,
    # los elimino. Esta opci�n por default es true
	if(empty($params['allowEmpty']) || $params['allowEmpty'] == "false" || $params['allowEmpty'] === false){
		$result = array_values(array_diff($result, array('')));
	}

	# Por �ltimo, asigno el arreglo a la variable, y no retorno contenido HTML
	$smarty->assign($params['assign'], $result);
	return '';
}
################################################################################
?>