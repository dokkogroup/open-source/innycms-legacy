<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {swp_url} function plugin
 *
 * Type: function
 * <br>
 * Name: swp_url
 * <br>
 * Purpose: Esta funci�n sirve para crear la url correspondiente al archivo del
 *          producto.
 * <br>
 * Input:
 * <br>
 * - Opcionales
 *   - id_archivo = Id del producto al que har� referencia la URL.
 *     Por defecto, asume el que tendr� seteado el template en ese momento
 *     (en caso de no estar seteado, lanzar� un error).
 *   - type: tipo de URL que generar�.
 *     - image = genera la url para mostrar la imagen (este valor es por defecto).
 *     - thumb = link a una versi�n redimensionada de la imagen
 *       - width = ancho de la imagen
 *       - height = alto de la imagen
 *       - quality = calidad de la imagen [0-100]
 *     - crop = link a una versi�n redimensionada de la imagenn que respeta el ancho y alto solicitado, recortando la imagen original
 *       - width = ancho de la imagen
 *       - height = alto de la imagen
 *       - quality = calidad de la imagen [0-100]
 *     - file = genera la url para descarga del archivo.
 *     - sound = link al archivo de audio
 *     - flash = link para archivo flash
 *
 * <br>
 * Examples:
 * <pre>
 * <table cellpadding="0" cellspacing="0">
 * {swp_lister name="bicicletas"}
 *   <tr>
 *     <td>{$texto1}</td>
 *     <!-- NO se pasan por par�metro la variables:
 *          - type: por defecto el type es "image"
 *          - id_archivo, porque asume que el valor seteado es $id_archivo
 *     -->
 *     <td><img src="{swp_url type="image"}" /></td>
 *   </tr>
 * {/swp_lister}
 * </table>
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @link http://wiki.dojo/index.php/InnyCMS%3A%20funci%F3n%20swp_url {swp_url} (Inny wiki)
 * @param array
 * @param Smarty
 * @return string
 */
require_once '../denko/dk.denko.php';
################################################################################
function smarty_function_swp_url($params, &$smarty){

	# Verifico si puedo obtener el id_del archivo
	$id_archivo = !empty($params['id_archivo'])? $params['id_archivo'] : $smarty->get_template_vars('id_archivo');
    if(empty($id_archivo)){
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El par�metro id_archivo es requerido','swp_url');
        return '';
    }

	# Busco la relaci�n Producto_Temporal en la DB
	$daoProductoTemporal = DB_DataObject::factory('producto_temporal');
	$daoProductoTemporal->selectAdd();
	$daoProductoTemporal->selectAdd('id_producto,id_temporal,posicion');
	$daoProductoTemporal->id_temporal = $id_archivo;

	# Verifico que el archivo sea de un Producto
	# Si es as�, debe tener un producto asociado
	if($daoProductoTemporal->find(true)){
		$id_producto = $daoProductoTemporal->id_producto;
		$posicion = $daoProductoTemporal->posicion;
		$id_temporal = $daoProductoTemporal->id_temporal;
	}

	# En caso que el archivo no tenga un producto asociado, lanzo un error
	else{
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'El archivo con id '.$id_archivo.' no existe.','swp_url');
        return '';
	}

	$daoProducto = InnyCore_Dao::getDao('producto',$id_producto);
	$daoListado = InnyCore_Dao::getDao('listado',$daoProducto->id_listado);
	$innyFileType = $daoListado->createInnyType('archivo',$posicion,$params);
	$file_info = InnyCore_File::getTemporalInfo($id_temporal);
	$url = $innyFileType->getContentUrl($id_temporal,$file_info['name'],$params);

    # En caso que est� seteado el par�metro assign
    if(!empty($params['assign'])){
        $smarty->assign($params['assign'],$url);
        return '';
    }

    # Sin�, retorno la URL resultante
    return $url;
}
################################################################################
