<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {tracker_get_dao_empleado_logueado} function plugin
 *
 * Type: function
 * <br>
 * Name: tracker_get_dao_empleado_logueado
 * <br>
 * Purpose: asigna al template el empleado actualmente logueado
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *   - assign = nombre de variable a la que asignar� el DAO del empleado.
 * <br>
 * Examples:
 * <pre>
 * {tracker_get_dao_empleado_logueado assign="tipo_usuario_logueado"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @link http://wiki.dokkogroup.com.ar/index.php/CMS%20ServiciosWeb {tracker_get_dao_empleado_logueado} (Inny wiki)
 * @param array
 * @param Smarty
 * @return boolean
 */
require_once '../commons/inny.tracker.php';
################################################################################
function smarty_function_tracker_get_dao_empleado_logueado($params, &$smarty) {

    # Verifico que el par�metro 'assign' exista
    if(empty($params['assign'])){
        Denko::plugin_fatal_error('el par�metro <b>assign</b> es requerido','tracker_get_dao_empleado_logueado');
    }

    # Asigno el tipo de usuario logueado al template
    $smarty->assign($params['assign'],Inny_Tracker::getDaoEmpleadoLogueado());

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################