<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {tracker_get_dao_sitio} function plugin
 *
 * Type: function
 * <br>
 * Name: tracker_get_dao_sitio
 * <br>
 * Purpose: obtiene un DAO Sitio de la DB
 * <br>
 * Input:
 * <br>
 * - Requeridos
 *   - id_sitio = ID del sitio
 *   - assign   = nombre de variable a la que asignar� el DAO del sitio.
 * <br>
 * Examples:
 * <pre>
 * {tracker_get_dao_sitio assign="daoSitio"}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return boolean
 */
################################################################################
function smarty_function_tracker_get_dao_sitio($params, &$smarty) {

    # Verifico que el par�metro 'assign' exista
    if(empty($params['assign'])){
        InnyModule_Reporter::plugin_error(E_USER_ERROR,'el par�metro <b>assign</b> es requerido','tracker_get_dao_sitio');
    }

    # Verifico que el par�metro 'id_sitio' exista
    if(empty($params['id_sitio'])){
        InnyModule_Reporter::plugin_error(E_USER_ERROR,'el par�metro <b>id_sitio</b> es requerido','tracker_get_dao_sitio');
    }

    # Verifico que el DAO Sitio exista en la DB
    $id_sitio = $params['id_sitio'];
    $daoSitio = InnyCore_Dao::getDao('sitio',$id_sitio);
    if(empty($daoSitio)){
        InnyModule_Reporter::plugin_error(E_USER_ERROR,'El DAO Sitio con ID "'.$id_sitio.'" no existe en la DB','tracker_get_dao_sitio');
    }

    # Asigno el tipo de usuario logueado al template
    $smarty->assign($params['assign'],$daoSitio);

    # Este plugin no retorna c�digo HTML
    return '';
}
################################################################################