<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {tracker_htmloptions_idiomas_cms} function plugin
 *
 * Type: function
 * <br>
 * Name: tracker_htmloptions_idiomas_cms
 * <br>
 * Purpose:
 * <br>
 * Input:
 * <br>
 * Examples:
 * <pre>
 * {tracker_htmloptions_idiomas_cms}
 * </pre>
 *
 * @author Denko Developers Group <info at dokkogroup dot com dot ar>
 * @version 1.0
 * @param array
 * @param Smarty
 * @return string
 */
################################################################################
function smarty_function_tracker_htmloptions_idiomas_cms($params, &$smarty) {
    $params['options'] = Inny_Tracker::getIdiomasCMS();
    require_once $smarty->_get_plugin_filepath('function','html_options');
    return smarty_function_html_options($params,$smarty);
}
################################################################################