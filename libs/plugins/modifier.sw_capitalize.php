<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_capitalize} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_capitalize
 * <br>
 * Purpose: Capitaliza una cadena de texto
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_capitalize {sw_capitalize} (Inny wiki)
 * @param string $string cadena de texto
 * @param string $charset charset
 * @return string
 */
################################################################################
function smarty_modifier_sw_capitalize($string,$charset = 'ISO-8859-1'){
    return Denko::capitalize($string,$charset);
}
################################################################################
?>