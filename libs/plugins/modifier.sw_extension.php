<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_extension} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_extension
 * <br>
 * Purpose: Obtiene la extensi�n del nombre de un archivo
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_extension {sw_extension} (Inny wiki)
 * @param string $string cadena de texto
 * @return string
 */
################################################################################
function smarty_modifier_sw_extension($string){

    # Busco la �ltima aparici�n del '.', para obtener la extensi�n
    $ext = strrchr($string,'.');

    # Si no tiene extensi�n, retorno vac�o. sin�, la extensi�n
    return ($ext === false) ? '' : substr($ext,1);
}
################################################################################
?>