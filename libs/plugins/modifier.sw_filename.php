<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_filename} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_filename
 * <br>
 * Purpose: Convierte una cadena a nombre v�lido de archivo
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_filename {sw_filename} (Inny wiki)
 * @param string $string cadena de texto
 * @param string $replaceSpaces caracter por el que se reemplazar�n los espacios
 * @return string
 */
################################################################################
function smarty_modifier_sw_filename($string, $replaceSpaces = '_'){

    # Primero elimino todos los caracteres que no sean v�lidos
    $string = preg_replace('/[^\w\s]|\_/','',$string);

    # Luego elimino el exceso de espacios
    $string = preg_replace('/\s+/',$replaceSpaces,$string);

    # Por �ltimo convierto los caracteres con acento y/o di�resis y la e�es,
    # adem�s de pasar todo a min�scula
    return strtr(strtolower($string),'����������������','aaaeeeiiiooouuun');
}
################################################################################
?>