<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_key2value} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_key2value
 * <br>
 * Purpose: Elimina todos los tags y entidades HTML de un texto
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_key2value {sw_key2value} (Inny wiki)
 * @param string $texto_clave clave
 * @param integer $texto_numero nro del campo de texto
 * @return string
 */

################################################################################
function smarty_modifier_sw_key2value($texto_clave,$texto_numero=1,$producto_data=null){

    # Verifico si est� dentro de un listado
    $listado_nombre = Denko::getSmartyParentTag($GLOBALS['smarty'],'swp_lister');
    if(!empty($listado_nombre)){
        $validName = Denko::toValidTagName($listado_nombre);
        $cachedListadoKey = 'INNY_CACHED_LISTADO_'.Denko::upper($validName);
        $daoListado = &$GLOBALS[$cachedListadoKey];
    }

    # Verifico si el producto no est� contenido en un listado de productos
    # Ej: en caso que se haya obtenido mediante el plugin swp_get_producto
    elseif(!empty($producto_data)){

        # En caso que sea el ID del producto
        if(InnyCore_Utils::is_integer($producto_data)){
            $daoSitio = Inny::getDaoSitioActual();
            $daoProducto = $daoSitio->getProducto($producto_data);
            if(empty($daoProducto)){
                InnyModule_Reporter::plugin_error(E_USER_WARNING,'El ID de producto "'.$producto_data.'" no pertenece al sitio "'.$daoSitio->nombre.'"','sw_key2value');
                return '';
            }
            $daoListado = $daoProducto->getListado();
        }

        # En caso que sea el nombre de su listado
        else{
            $daoListado = Inny::getListadoPorNombre($producto_data);
            if(empty($daoListado)){
                InnyModule_Reporter::plugin_error(E_USER_WARNING,'El listado con nombre "'.$producto_data.'" no pertenece al sitio "'.$daoSitio->nombre.'"','sw_key2value');
                return '';
            }
        }
    }

    # En caso que haya un error de par�metros
    else{
        InnyModule_Reporter::plugin_error(E_USER_WARNING,'Los par�metros son incorrectos. Ver documentaci�n en wiki','sw_key2value');
        return '';
    }

    # Verifico que el nro de campo de texto sea v�lido
    if(!InnyCore_Utils::is_integer($texto_numero) || $texto_numero < 1 || $texto_numero > $daoListado->getMetadataValue('texto','cantidad')){
        return $texto_clave;
    }

    # Retorno el valor de la clave
    return $daoListado->getMetadataValue('texto','texto'.$texto_numero,'parametros','options',$texto_clave);
}
################################################################################
?>