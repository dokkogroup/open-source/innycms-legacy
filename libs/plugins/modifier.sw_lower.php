<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_lower} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_lower
 * <br>
 * Purpose: Convierte a may�sculas todos los caracteres de una cadena de texto
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_lower {sw_lower} (Inny wiki)
 * @param string $string cadena de texto
 * @param string $charset charset
 * @return string
 */
################################################################################
function smarty_modifier_sw_lower($string,$charset = 'ISO-8859-1'){
    return Denko::lower($string,$charset);
}
################################################################################
?>