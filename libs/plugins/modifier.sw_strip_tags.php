<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_strip_tags} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_strip_tags
 * <br>
 * Purpose: Elimina todos los tags y entidades HTML de un texto
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_strip_tags {sw_strip_tags} (Inny wiki)
 * @param string $string cadena de texto
 * @param integer $length largo del texto
 * @return string
 */
################################################################################
function smarty_modifier_sw_strip_tags($string, $length = null){

    # Stripeo el texto y elimino las entidades HTML (&nbsp, &amp, etc)
    $string = html_entity_decode(strip_tags($string));

    # En caso que haya que resumir el texto stripeado
    return ($length && Denko::isInt($length)) ? Denko::summaryText($string,$length) : $string;
}
################################################################################
?>