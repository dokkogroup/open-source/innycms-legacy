<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_text_to_html} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_text_to_html
 * <br>
 * Purpose: Convierte los saltos de l�nea de un texto plano en entidades BR
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_text_to_html {sw_text_to_html} (Inny wiki)
 * @param string $string cadena de texto
 * @return string
 */
################################################################################
function smarty_modifier_sw_text_to_html($string){
    return str_replace("\n",'<br />',str_replace("\r",'',$string));
}
################################################################################
?>