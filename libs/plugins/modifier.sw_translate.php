<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_translate} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_translate
 * <br>
 * Purpose: Retorna la traducci�n de una cadena, dependiendo del idioma actual del sitio
 * Si recibe language_code, utiliza ese idioma, sino utiliza el de la variable global $speech_lang.
 * Si no hay un idioma seteado, utiliza es_AR
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_translate {sw_translate} (Inny wiki)
 * @param string $string cadena de texto
 * @param string $language_code c�digo de idioma
 * @return string
 */
################################################################################
function smarty_modifier_sw_translate($string,$language_code = null){
    global $speech_lang;
    $lang=empty($language_code)?(empty($speech_lang)?'es_AR':$speech_lang):$language_code;
    return InnyModule_Multilang::getTranslation($string,$lang);
}
################################################################################