<?php
/**
 * Inny Smarty plugin
 * @package Inny
 * @subpackage plugins
 */

/**
 * Inny Smarty {sw_transurl} modifier plugin
 *
 * Type: modifier
 * <br>
 * Name: sw_transurl
 * <br>
 * Purpose: Retorna la URL correspondiente a un archivo, dependiendo del idioma actual del sitio
 *
 * @author Dokko Group <info at dokkogroup dot com>
 * @link http://wiki.dojo/index.php/Inny%20CMS%3A%20modificador%20sw_transurl {sw_transurl} (Inny wiki)
 * @param string $string cadena de texto
 * @param string $language_code c�digo de idioma
 * @return string
 */
################################################################################
function smarty_modifier_sw_transurl($string,$language_code = 'es_AR'){
    return InnyModule_Multilang::getContentUrlTranslation($string,$language_code);
}
################################################################################
?>