<?php
/**
 * Instala DSE en la base de datos.
 * @param string $addToTable nombre de la tabla en la base de datos a la cual se desea
 *                           instalar sooprte DSE. Si es null, se creara la tabla
 *                           dse_document.
 */
function installDSE($addToTable = null) {
    if ($addToTable === null) {
        report('Instalando DSE',R_ACTION);
        
        $query = "CREATE TABLE  `dse_document` (
                            `id_document` int(10) unsigned NOT NULL auto_increment,
                            `field_0` tinytext,
                            `field_1` tinytext,
                            `field_2` text,
                            `d_field_0` tinytext,
                            `d_field_1` tinytext,
                            `d_field_2` text,
                            `category_ids` text,
                            `category_names` text,
                            `so_field_0` int(10) unsigned NOT NULL,
                            `so_field_1` int(10) unsigned NOT NULL,
                            PRIMARY KEY  (`id_document`),
                            FULLTEXT KEY `FIELD_1_TEXT_INDEX` (`field_0`),
                            FULLTEXT KEY `FIELD_2_TEXT_INDEX` (`field_1`),
                            FULLTEXT KEY `FIELD_3_TEXT_INDEX` (`field_2`),
                            FULLTEXT KEY `FULL_TEXT_INDEX` (`field_0`,`field_1`,`field_2`,`category_names`),
                            FULLTEXT KEY `CATEGORY_IDS_INDEX` (`category_ids`),
                            FULLTEXT KEY `CATEGORY_NAMES_INDEX` (`category_names`)
                        ) ENGINE=MyISAM;";
        if (!createTable($query,"dse_document")) return false;
        
    } else {
        report('Instalando DSE a la tabla "'.$addToTable.'"',R_ACTION);
        
        global $mysqlErrno;
        
        $query = "ALTER TABLE  `$addToTable`
                        ADD COLUMN `field_0` tinytext,
                        ADD COLUMN `field_1` tinytext,
                        ADD COLUMN `field_2` text,
                        ADD COLUMN `category_ids` text,
                        ADD COLUMN `category_names` text,
                        ADD COLUMN `so_field_0` int(10) unsigned NOT NULL,
                        ADD COLUMN `so_field_1` int(10) unsigned NOT NULL,
                        ADD FULLTEXT KEY `FIELD_1_TEXT_INDEX` (`field_0`),
                        ADD FULLTEXT KEY `FIELD_2_TEXT_INDEX` (`field_1`),
                        ADD FULLTEXT KEY `FIELD_3_TEXT_INDEX` (`field_2`),
                        ADD FULLTEXT KEY `FULL_TEXT_INDEX` (`field_0`,`field_1`,`field_2`,`category_names`),
                        ADD FULLTEXT KEY `CATEGORY_IDS_INDEX` (`category_ids`),
                        ADD FULLTEXT KEY `CATEGORY_NAMES_INDEX` (`category_names`)
                    ;";
        
        if(!execQuery($query,true,array(1050,1060))){
            reportMysqlErrors();
            report('No se pudo agregar el soporte, verifique que el usuario en DB.ini tiene permisos, que la tabla "'.$addToTable.'" existe y es MyIsam.',R_ERROR);
            return false;
        }
        
        if ($mysqlErrno == 1050) {
            report('DSE ya estaba instalado en "'.$addToTable.'".',R_INFO);
        }
        
    }
    
    $query = "CREATE TABLE  `dse_category` (
                        `id_category` int(10) unsigned NOT NULL auto_increment,
                        `name` varchar(50) NOT NULL,
                        `code` varchar(50) NOT NULL,
                        `id_view` int(10) unsigned default NULL,
                        `metadata` varchar(128) NOT NULL,
                        `id_parent` int(10) unsigned default NULL,
                        PRIMARY KEY  (`id_category`),
                        KEY `INDEX_ID_VISTA` USING BTREE (`id_view`),
                        KEY `INDEX_CODE` (`code`)
                    ) ENGINE=MyISAM;";
    if (!createTable($query,"dse_category")) return false;
    
    $query = "CREATE TABLE  `dse_document_category` (
                        `id_document_category` int(10) unsigned NOT NULL auto_increment,
                        `id_category` int(10) unsigned NOT NULL,
                        `id_document` int(10) unsigned NOT NULL,
                        `id_parent` int(10) unsigned default NULL,
                        PRIMARY KEY  (`id_document_category`),
                        KEY `ID_DOCUMENT_INDEX` (`id_document`),
                        KEY `ID_CATEGORY_INDEX` (`id_category`),
                        KEY `ID_PARENT_INDEX` (`id_parent`)
                    ) ENGINE=MyISAM;";
    if (!createTable($query,"dse_document_category")) return false;
    
    $query = "CREATE TABLE `dse_synonim` (
                      `id_synonim` int(10) unsigned NOT NULL auto_increment,
                      `word` varchar(20) NOT NULL,
                      `synonims` varchar(100) default NULL,
                      `is_synonim_of` int(10) unsigned default NULL,
                      PRIMARY KEY  (`id_synonim`),
                      UNIQUE KEY `WORD_INDEX` (`word`),
                      KEY `WORD_TEXT_INDEX` (`word`)
                    ) ENGINE=MyISAM;";
    if (!createTable($query,"dse_synonim")) return false;
    
    updateDSEV1();
    updateDSEV2();
    updateDSEV3();
    updateDSEV4();
    
    report('DSE instalado correctamente',R_OK);
    
    return true;
}
/**
 * Desinstala DSE en la base de datos.
 * @param string $removeFromTable nombre de la tabla en la base de datos de la cual 
 *                                se desea desinstalar sooprte DSE. Si es null, 
 *                                se intentará remover la tabla dse_document.
 */
function uninstallDSE($removeFromTable = null) {
    if ($removeFromTable === null) {
        report('Desinstalando DSE',R_ACTION);
        return dropTable("dse_document") && dropTable("dse_category") && dropTable("dse_document_category");
    } else {
        global $mysqlErrno;
        
        $query = "ALTER TABLE  `$removeFromTable`
                        DROP COLUMN `field_0`,
                        DROP COLUMN `field_1`,
                        DROP COLUMN `field_2`,
                        DROP COLUMN `category_ids`,
                        DROP COLUMN `category_names`,
                        DROP COLUMN `so_field_0`,
                        DROP COLUMN `so_field_1`,
                        DROP KEY `FIELD_1_TEXT_INDEX`,
                        DROP KEY `FIELD_2_TEXT_INDEX`,
                        DROP KEY `FIELD_3_TEXT_INDEX`,
                        DROP KEY `FULL_TEXT_INDEX`,
                        DROP KEY `CATEGORY_IDS_INDEX`,
                        DROP KEY `CATEGORY_NAMES_INDEX`
                    ;";
        report('Desinstalando DSE en "'.$removeFromTable.'".',R_ACTION);
        $result = execQuery($query,true,array(1054,1091));
        if($mysqlErrno == 1054 || $mysqlErrno == 1091){
            report('DSE no estaba instalado en "'.$removeFromTable.'".',R_INFO);
        }
        
        return  $result && dropTable("dse_category") && dropTable("dse_document_category") && dropTable("dse_synonim");
    
    }
}

/**
 * Agrega el campo refs a la tabla dse_document_category.
 */
function updateDSEV1() {
    
    global $mysqlErrno;
    
    $query = "ALTER TABLE `dse_document_category`
                        ADD COLUMN `refs` INTEGER UNSIGNED NOT NULL DEFAULT 1";
    return execQuery($query,false,array(1060,1050));
}

/**
 * Agrega el campo indice a la tabla dse_category.
 */
function updateDSEV2() {
    
    global $mysqlErrno;
    
    $query = "ALTER TABLE `dse_category`
                        ADD COLUMN `indice` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                        ADD KEY `INDEX_CATEGORY_INDICE` USING BTREE (`indice`)
                        ;";
   $query2 = "ALTER TABLE `dse_document_category`
                        ADD COLUMN `indice` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                        ADD KEY `INDEX_DOCUMENT_CATEGORY_INDICE` USING BTREE (`indice`)
                        ;";
                        
    return execQuery($query,false,array(1060,1050)) && execQuery($query2,false,array(1060,1050));
}

function updateDSEV3() {
    $query = "ALTER TABLE `dse_category` 
                   MODIFY COLUMN `name` VARCHAR(200) NOT NULL,
                   MODIFY COLUMN `code` VARCHAR(200) NOT NULL
                   ;";
    return execQuery($query,false);
}

function updateDSEV4() {
    $query0 = "ALTER TABLE `dse_category` 
                DROP INDEX `INDEX_CATEGORY_INDICE`,
                CHANGE COLUMN `indice` `index1` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                ADD COLUMN `index2` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                ADD COLUMN `index3` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                ADD INDEX `INDEX_CATEGORY_INDEX1` USING BTREE(`index1`),
                ADD INDEX `INDEX_CATEGORY_INDEX2`(`index1`),
                ADD INDEX `INDEX_CATEGORY_INDEX3`(`index2`),
                ADD COLUMN `level` INTEGER UNSIGNED NOT NULL DEFAULT 0
            ;";
            
    $query1 = "ALTER TABLE `dse_document_category`
                DROP INDEX `INDEX_DOCUMENT_CATEGORY_INDICE`,
                CHANGE COLUMN `indice` `index1` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                ADD COLUMN `index2` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                ADD COLUMN `index3` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                ADD INDEX `INDEX_DOCUMENT_CATEGORY_INDEX0` USING BTREE(`index1`),
                ADD INDEX `INDEX_DOCUMENT_CATEGORY_INDEX2`(`index2`),
                ADD INDEX `INDEX_DOCUMENT_CATEGORY_INDEX3`(`index3`)
             ;";
    $queryUndo0='ALTER TABLE `dse_category` DROP COLUMN `indice`;';
    $queryUndo1='ALTER TABLE `dse_document_category` DROP COLUMN `indice`;';

    $res1=execQuery($query0,false);
    $res2=execQuery($query1,false);
    if($res1 && $res2){
        return true;
    }
    $res1=execQuery($queryUndo0,false);
    $res2=execQuery($queryUndo1,false);
    return ($res1 && $res2);
}

function updateDSEV5() {
  $query = "ALTER TABLE `dse_category` 
                ADD COLUMN `docscount` INTEGER UNSIGNED NOT NULL DEFAULT 0
            ;";
            
    return execQuery($query,false,array(1060));  
}

function updateDSEV6() {
  $query = "ALTER TABLE `dse_document_category` 
                ADD COLUMN `added` INTEGER UNSIGNED NOT NULL DEFAULT 0
            ;";
            
    return execQuery($query,false,array(1060));  
}

function updateDSEV7() {
  	
  	require_once 'update_added_flag.php';
            
    return true;  
}