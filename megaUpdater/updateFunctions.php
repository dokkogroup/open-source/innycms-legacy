<?php
/* Mega Updater - UpdateFunctions
 * Versi�n 0.1
 * Propiedad de DokkoGrpup
 *
 * En este archivo se encuentran las funciones
 * de actualizaci�n espec�ficas de cada proyecto.
 *
 * By. FBricker
 */

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Funciones de actualizacion

function updateV1(){
    report('Instalando megaUpdater',R_ACTION);
    return true;
}

function updateV2(){
    // Agrego m�dulo de configuraci�n:
    return installConfigurationModule();

}

function updateV3(){
    // Agrego la tabla temporal
    return installTemporalModule();
}

function updateV4(){

    // instalo el AudDataObject en TODAS las tablas:
    if(!installAudDataObject('comentario')) return false;
    if(!installAudDataObject('ctacorriente')) return false;
    if(!installAudDataObject('empleado')) return false;
    if(!installAudDataObject('renglon')) return false;
    if(!installAudDataObject('sitio')) return false;
    if(!installAudDataObject('tarea')) return false;
    return true;
}

function updateV5(){
    $query = "
        CREATE TABLE  `tarea_temporal` (
            `id_tarea` int(10) unsigned NOT NULL,
            `id_temporal` int(10) unsigned NOT NULL,
            `filename` varchar(100) NOT NULL,
            `mime` varchar(100) NOT NULL,
            PRIMARY KEY  (`id_tarea`,`id_temporal`),
            KEY `Index_2` (`id_tarea`),
            KEY `Index_3` (`id_temporal`),
            CONSTRAINT `FK_tarea_temporal_1` FOREIGN KEY (`id_tarea`) REFERENCES `tarea` (`id_tarea`),
            CONSTRAINT `FK_tarea_temporal_2` FOREIGN KEY (`id_temporal`) REFERENCES `temporal` (`id_temporal`)
        ) ENGINE=InnoDB;";
    return execQuery($query,false,array(1050));
}

function updateV6(){
    return installAudDataObject('tarea_temporal');
}

function updateV7(){
    $query = '
        ALTER TABLE `empleado`
        ADD COLUMN `username` VARCHAR(100) NOT NULL AFTER `id_ctacorriente`,
        ADD COLUMN `password` VARCHAR(100) NOT NULL AFTER `username`;';
    return execQuery($query,false,array(1060));
}

function updateV8(){
    return execQuery('ALTER TABLE `comentario` MODIFY COLUMN `texto` BLOB NOT NULL;');
}

function updateV9(){
    $query = '
        ALTER TABLE `empleado`
            MODIFY COLUMN `nombre` VARCHAR(100) NOT NULL,
            MODIFY COLUMN `apellido` VARCHAR(100) NOT NULL,
            MODIFY COLUMN `email` VARCHAR(100) NOT NULL;';
    return execQuery($query);
}

function updateV10(){
    $query = '
        ALTER TABLE `sitio`
            ADD COLUMN `ftp` VARCHAR(100) AFTER `password`,
            ADD COLUMN `ftp_username` VARCHAR(45) AFTER `ftp`,
            ADD COLUMN `ftp_password` VARCHAR(45) AFTER `ftp_username`,
            ADD COLUMN `cvs` VARCHAR(45) NOT NULL AFTER `ftp_password`;';
    return execQuery($query,false,array(1060));
}

function updateV11(){
    $query = '
        CREATE TABLE `estado` (
            `id_estado` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
            `nombre` VARCHAR(100) NOT NULL,
            PRIMARY KEY (`id_estado`)
        )
        ENGINE = InnoDB;';
    return execQuery($query,false,array(1050));
}

function updateV12(){
    if (!execQuery('ALTER TABLE `tarea` ADD COLUMN `id_estado` INTEGER UNSIGNED DEFAULT NULL AFTER `id_sitio`;',true,array(1060))) return false;
    if (!execQuery('ALTER TABLE `tarea` DROP COLUMN `estado`;',true,array(1091))) return false;
    if (!execQuery('ALTER TABLE `tarea` ADD INDEX `id_estado_4` (`id_estado`);',true,array(1061))) return false;
    if (!execQuery('ALTER TABLE `tarea` ADD CONSTRAINT `FK_tarea_3` FOREIGN KEY `FK_tarea_3` (`id_estado`) REFERENCES `estado` (`id_estado`);',true,array(1005))) return false;
    return execQuery('ALTER TABLE `tarea` MODIFY COLUMN `id_estado` INTEGER UNSIGNED NOT NULL;');
}

function updateV13(){
    return (
           execQuery('INSERT INTO `estado` VALUES (1,\'no comenzado\');',false,array(1136))
        && execQuery('INSERT INTO `estado` VALUES (2,\'en proceso\');',false,array(1136))
        && execQuery('INSERT INTO `estado` VALUES (3,\'finalizado\');',false,array(1136))
        && execQuery('INSERT INTO `estado` VALUES (4,\'en pausa\');',false,array(1136))
        && execQuery('INSERT INTO `estado` VALUES (5,\'anulado\');',false,array(1136))
    );
}

function updateV14(){
    return execQuery('
    ALTER TABLE `sitio`
        MODIFY COLUMN `nombre_encargado` VARCHAR(100) NOT NULL,
        MODIFY COLUMN `apellido` VARCHAR(50) NOT NULL;');
}

function updateV15(){
	return execQuery('
		ALTER TABLE `sitio`
            MODIFY COLUMN `id_ctacorriente` INTEGER UNSIGNED NOT NULL,
            MODIFY COLUMN `telefono` VARCHAR(50) NOT NULL,
            MODIFY COLUMN `direccion` VARCHAR(100) NOT NULL;');
}

function updateV16(){
    return execQuery ('
        ALTER TABLE `empleado`
            MODIFY COLUMN `id_ctacorriente` INTEGER UNSIGNED NOT NULL,
            MODIFY COLUMN `telefono` VARCHAR(100) NOT NULL;');
}

function updateV17(){
	return execQuery('
        ALTER TABLE `sitio`
            ADD COLUMN `habilitado` TINYINT(1) NOT NULL AFTER `aud_upd_date`;',false,array(1060));
}

function updateV18(){
	return execQuery('
        ALTER TABLE `empleado` ADD COLUMN `habilitado` TINYINT(1) NOT NULL AFTER `aud_upd_date`;',false,array(1060));
}

function updateV19(){
	$restrictions='';
    $description='version cache de js y css';
	return createConfiguration('cacheversions',0,'1.0',$restrictions,$description);
}

function updateV20(){
    $restrictions='';
    $description='password del admin';
    return createConfiguration('dokko_admin_pass',0,'4dm1n',$restrictions,$description);
}

function updateV21(){
    $restrictions='';
    $description='username del admin';
    return createConfiguration('dokko_admin_user',0,'admin',$restrictions,$description);
}

function updateV22(){
    return execQuery('
        ALTER TABLE `sitio`
            MODIFY COLUMN `username` VARCHAR(100) NOT NULL;');
}

function updateV23(){
    report('Agregando tabla <b>listado</b>',R_ACTION);
    $query = "
        CREATE TABLE `listado` (
            `id_listado` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
            `nombre` VARCHAR(100) NOT NULL,
            `id_sitio` INTEGER UNSIGNED NOT NULL,
            `atributos` BLOB,
            PRIMARY KEY (`id_listado`),
            KEY `FK_listado_1` (`id_sitio`),
            CONSTRAINT `FK_listado_1` FOREIGN KEY `FK_listado_1` (`id_sitio`) REFERENCES `sitio` (`id_sitio`)
        ) ENGINE = InnoDB;";
    return execQuery($query,false,array(1050));
}

function updateV24(){
    report('Agregando tabla <b>producto</b>',R_ACTION);
    $query = "
        CREATE TABLE `producto` (
            `id_producto` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
            `texto1` BLOB,
            `texto2` BLOB,
            `texto3` BLOB,
            `id_archivo` INTEGER UNSIGNED DEFAULT NULL,
            `filename` varchar(250) NOT NULL,
            `mime` VARCHAR(100) DEFAULT NULL,
            `posicion` INTEGER UNSIGNED NOT NULL DEFAULT '1',
            `id_listado` INTEGER UNSIGNED NOT NULL,
            PRIMARY KEY  (`id_producto`),
            KEY `FK_producto_1` (`id_listado`),
            KEY `FK_producto_2` (`id_archivo`),
            CONSTRAINT `FK_producto_1` FOREIGN KEY `FK_producto_1` (`id_listado`) REFERENCES `listado` (`id_listado`),
            CONSTRAINT `FK_producto_2` FOREIGN KEY `FK_producto_2` (`id_archivo`) REFERENCES `temporal` (`id_temporal`)
        ) ENGINE = InnoDB";
    return execQuery($query,false,array(1050));
}

function updateV25(){
    return (installAudDataObject('listado') && installAudDataObject('producto'));
}

/**
 * Agrega el campo 'crea_listados' a la tabla 'Sitio'
 */
function updateV26(){
    report('Agregando el campo <b>crea_listados</b> a la tabla <b>Sitio</b>',R_ACTION);
    $query = "ALTER TABLE `sitio` ADD COLUMN `crea_listados` CHAR(1) NOT NULL DEFAULT 1 AFTER `habilitado`;";

    /**
     * Me aseguro que el default sea '1' (puede crear listados)
     */
    if(execQuery($query,true,array(1060))){
        return execQuery("UPDATE sitio SET crea_listados = '1'");
    }
    return false;
}

function updateV27() {
	$restrictions='';
    $description='Usuario para el mail smtp';
    return createConfiguration('SMTPMAIL_USERNAME',0,'sitiosweb@dokkogroup.com.ar',$restrictions,$description);
}

function updateV28() {
	$restrictions='';
    $description='Password para el mail smtp';
    return createConfiguration('SMTPMAIL_PASSWORD',0,'sitioswebpass',$restrictions,$description);
}

function updateV29() {
	$restrictions='';
    $description='Host del mail smtp';
    return createConfiguration('SMTPMAIL_HOST',0,'smtp.gmail.com',$restrictions,$description);
}

function updateV30(){
    $query = "
        CREATE TABLE  `producto_temporal` (
            `id_producto` int(10) unsigned NOT NULL,
            `id_temporal` int(10) unsigned NOT NULL,
            `filename` varchar(250) NOT NULL,
            `mime` varchar(100),
            PRIMARY KEY  (`id_producto`,`id_temporal`),
            KEY `Index_2` (`id_producto`),
            KEY `Index_3` (`id_temporal`),
            CONSTRAINT `FK_producto_temporal_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
            CONSTRAINT `FK_producto_temporal_2` FOREIGN KEY (`id_temporal`) REFERENCES `temporal` (`id_temporal`)
        ) ENGINE=InnoDB;";

    return (execQuery($query,false,array(1050)) && installAudDataObject('producto_temporal'));
}

function updateV31(){
	$query = "
        INSERT INTO `producto_temporal` (id_producto,id_temporal,filename,mime,aud_ins_date,aud_upd_date)
            SELECT id_producto,id_archivo,filename,mime,aud_ins_date,aud_upd_date
            FROM `producto`
            WHERE
                id_archivo IS NOT NULL
                AND filename IS NOT NULL
                AND mime IS NOT NULL
                AND mime != '';";
	return execQuery($query,false,array(1062,1054));
}

/**
 * Upgradea a la configuraci�n V3
 * Retorna siempre true por un posible problema de incompatibilidad con la
 * funci�n updateV2, que invoca a installConfigurationModule() y �ste invoca a
 * upgradeConfigurationModuleV2 y upgradeConfigurationModuleV3.
 *
 * Corregir el valor de retorno cuando se decida que hacer cuando ya se han
 * instalado upgrades previos
 */
function updateV32(){
    upgradeConfigurationModuleV3();
    return true;
}


function updateV33(){

	report('Eliminando el campo <b>FK</b> de la tabla <b>producto</b>',R_ACTION);
	if (!execQuery('ALTER TABLE `producto` DROP FOREIGN KEY `FK_producto_2`;',false,array(1091))) return false;

	report('Eliminando el campo <b>INDEX</b> de la tabla <b>producto</b>',R_ACTION);
	if (!execQuery('ALTER TABLE `producto` DROP INDEX `FK_producto_2`;',false,array(1091))) return false;

	report('Eliminando el campo <b>id_archivo</b> de la tabla <b>producto</b>',R_ACTION);
	if (!execQuery('ALTER TABLE `producto` DROP COLUMN `id_archivo`;',false,array(1091))) return false;

	report('Eliminando el campo <b>filename</b> de la tabla <b>producto</b>',R_ACTION);
	if (!execQuery('ALTER TABLE `producto` DROP COLUMN `filename`;',false,array(1091))) return false;

	report('Eliminando el campo <b>mime</b> de la tabla <b>producto</b>',R_ACTION);
	if (!execQuery('ALTER TABLE `producto` DROP COLUMN `mime`;',false,array(1091))) return false;

	return true;
}

function updateV34() {
    return createConfiguration('url_server_clientes',0,'localhost/dokkogroup.com/','','URL Server Clientes');
}

/**
 * Agrega la configuraci�n del el puerto para env�o de mails por SMTP
 * Por default agrego el puerto que usa gmail
 */
function updateV35() {
    return createConfiguration('SMTPMAIL_PORT',1,465,'','Puerto para envio de mails SMTP');
}

/**
 * Agrego la columna 'smtp' a la tabla Sitio
 */
function updateV36(){
    report('Agregando la columna <b>smtp</b> a la tabla <b>sitio</b>',R_ACTION);
    return execQuery('ALTER TABLE `sitio` ADD COLUMN `smtp` VARCHAR(250) AFTER `cvs`;',false,array(1060));
}

/**
 * Agregar la tabla Usuario
 */
function updateV37(){
    report('Agregando la tabla <b>usuario</b>',R_ACTION);
    return execQuery('
        CREATE TABLE `usuario` (
            `id_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `id_sitio` int(10) unsigned NOT NULL,
            `username` varchar(20) NOT NULL,
            `password` varchar(100) NOT NULL,
            `nombre` varchar(100) NOT NULL,
            `apellido` varchar(100) NOT NULL,
            `direccion` varchar(100),
            `telefono` varchar(100),
            `email` varchar(100),
            `habilitado` CHAR(1) NOT NULL DEFAULT 1,
            PRIMARY KEY (`id_usuario`),
            KEY `Index_2` (`id_sitio`),
            CONSTRAINT `FK_usuario_1` FOREIGN KEY (`id_sitio`) REFERENCES `sitio` (`id_sitio`)
        ) ENGINE = InnoDB;
    ',false,array(1050));
}

/**
 * Agregar el m�dulo AudDataObject a la tabla Usuario
 */
function updateV38(){
    return installAudDataObject('usuario');
}

/**
 * Agrega la configuraci�n del protocolo para el envio de mails por SMPT
 * Por defaulta agrega el valor 'ssl'
 */
function updateV39(){
    return createConfiguration('SMTPMAIL_PROTOCOL',0,'ssl','','Protocolo para envio de mails SMTP');
}

/**
 *
 */
function updateV40(){
    if(!createTipoconfiguracion(1,"Sistema","Configuraciones internas del sistema.")) return false;
    if(!createTipoconfiguracion(2,"E-mail","Configuraciones para el env�o de E-mail.")) return false;
    return true;
}

/**
 * Seteo los tipo de configuraciones
 */
function updateV41(){

    /**
     * Seteo configuraciones al tipo Sistema
     */
    report('Seteando configuraciones al tipo <b>Sistema</b>',R_ACTION);
    $querySetTipoConf_1 = '
        update `configuracion`
        set id_tipoconfiguracion = 1
        where (nombre not like \'SMTP_%\' and indice1 is null);
    ';
    if(!execQuery($querySetTipoConf_1)){
        return false;
    }

    /**
     * Seteo configuraciones al tipo E-mail
     */
    report('Seteando configuraciones al tipo <b>E-mail</b>',R_ACTION);
    $querySetTipoConf_2 = '
        update `configuracion`
        set id_tipoconfiguracion = 2
        where (nombre like \'SMTP_%\' and indice1 is null);
    ';
    return execQuery($querySetTipoConf_2);
}

/**
 * Agrega el m�dulo AudDataObject en la tabla Estado
 */
function updateV42(){
	return installAudDataObject('estado');
}

/**
 * Agrego el campo metadata a la tabla Listado
 */
function updateV43(){
    $query = '
        ALTER TABLE `listado`
            ADD COLUMN `metadata`
            TEXT DEFAULT NULL
            AFTER `id_sitio`;';

    return execQuery($query,false,array(1060));
}

/**
 * Agrega la columna 'url' en la tabla Sitio
 */
function updateV44(){
    $query = '
        ALTER TABLE `sitio`
            ADD COLUMN `url`
            VARCHAR(200) NOT NULL
            AFTER `nombre_sitio`;';

    return execQuery($query,false,array(1060));
}

/**
 * Agrega la columna 'mantenimiento' en la tabla Sitio
 */
function updateV45(){
    $query = '
        ALTER TABLE `sitio`
            ADD COLUMN `mantenimiento`
            TINYINT(1) DEFAULT 0
            AFTER `habilitado`;';

    return execQuery($query,false,array(1060));
}

/**
 * Agrega la columna 'version' en la tabla Sitio
 */
function updateV46(){
    $query = '
        ALTER TABLE `sitio`
            ADD COLUMN `version`
            VARCHAR(45) NOT NULL
            AFTER `mantenimiento`;';
    return execQuery($query,false,array(1060));
}

/**
 * Agrega la columna 'posicion' en la tabla Producto_Temporal.
 */
function updateV47(){
    $query = '
        ALTER TABLE `producto_temporal`
            ADD COLUMN `posicion` INTEGER
            AFTER `mime`;';

    return execQuery($query,false,array(1060));
}

/**
 * Agrego 7 campos de texto m�s, completando a 10 campos de texto en total
 */
function updateV48(){

    # hago el foreach
    for($i = 3; $i < 10; $i++){
        $query = '
            ALTER TABLE `producto`
            ADD COLUMN `texto'.($i+1).'` TEXT
            DEFAULT NULL
            AFTER `texto'.($i).'`;';

        if(!execQuery($query,false,array(1060))){
            return false;
        }
    }

    return true;
}

/**
 * Actualiza la columna 'posicion' en la tabla Producto_Temporal de todas las tuplas.
 */
function updateV49(){

	report('Seteando atributo posicion en la tabla producto_temporal',R_ACTION);

	$query = "SELECT id_producto,id_temporal,posicion FROM producto_temporal ORDER BY id_producto,aud_ins_date";
    $result = execQuery($query);

    $id_producto = '';
	$position = 1;
	if($result){
		while($row = mysql_fetch_row($result)) {
			if($id_producto != $row[0]){
				$id_producto = $row[0];
				$position = 1;
			}

			$res = execQuery('UPDATE producto_temporal SET posicion = '.$position.' WHERE id_producto = '.$id_producto.' AND id_temporal = '.$row[1].'');
			$position++;
		}
	}
	return true;
}

/**
 * Agrego el campo 'ga_codigo' a la tabla 'sitio'.
 * Este campo servir� para guardar el c�digo del google analytics
 */
function updateV50(){
    return execQuery('
        ALTER TABLE `sitio`
            ADD COLUMN `ga_codigo`
            VARCHAR(200) DEFAULT NULL
            AFTER `smtp`;
    ',false,array(1060));
}

/**
 * Agrego el campo metadata al Sitio
 */
function updateV51(){

    return execQuery('
        ALTER TABLE `sitio`
            ADD COLUMN `metadata`
            TEXT DEFAULT NULL
            AFTER `ga_codigo`;
    ',false,array(1060));
}

/**
 * Elimino la de la tabla 'temporal' la restricci�n de integridad a la tabla 'listado'
 */
function updateV52(){
    return execQuery('ALTER TABLE `producto` DROP FOREIGN KEY `FK_producto_1`;',false,array(1090));
}

/**
 * Elimino la de la tabla 'producto_temporal' la restricci�n de integridad a la tabla 'producto'
 */
function updateV53(){
    return execQuery('ALTER TABLE `producto_temporal` DROP FOREIGN KEY `FK_producto_temporal_1`;',false,array(1025));
}

/**
 * Seteo la tabla 'producto' en MYISAM
 */
function updateV54(){
    return execQuery('ALTER TABLE `producto` ENGINE = MyISAM;');
}

/**
 * Agrego el DSE
 */
function updateV55(){
    require_once '../megaUpdater/dse_update_functions.php';
    return installDSE('producto');
}

/**
 * Actualizo el DSE a la versi�n 1
 */
function updateV56(){
    require_once '../megaUpdater/dse_update_functions.php';
    return updateDSEV1();
}

/**
 * Actualizo el DSE a la versi�n 2
 */
function updateV57(){
    require_once '../megaUpdater/dse_update_functions.php';
    return updateDSEV2();
}

/**
 * Actualizo el DSE a la versi�n 3
 */
function updateV58(){
    require_once '../megaUpdater/dse_update_functions.php';
    return updateDSEV3();
}

/**
 * Actualizo el DSE a la versi�n 4
 */
function updateV59(){
    require_once '../megaUpdater/dse_update_functions.php';
    return updateDSEV4();
}

/**
 * Agrego la columna 'log_errors' a la tabla 'sitio'
 */
function updateV60(){
    return execQuery('
        ALTER TABLE `sitio`
        ADD COLUMN `log_errors` CHAR(1) NOT NULL DEFAULT 1 AFTER `version`;',false,array(1060));
}

/**
 * Instala el DokkoLogger
 */
function updateV61(){
    return installLogModule();
}

/**
 * Agrego los tipos de logs
 */
function updateV62(){
    $tipos = array('256'=>'ERROR','512'=>'WARNING','1024'=>'NOTICE');
    foreach($tipos as $id_tipolog => $nombre){
        if(!execQuery("INSERT INTO tipolog (id_tipolog,id_padre,nombre,descripcion,aud_ins_date,aud_upd_date) VALUES ($id_tipolog,NULL,'$nombre',NULL,'".date('Y-m-d H:i:s')."','0000-00-00 00:00:00');",false,array(1062))){
            return false;
        }
    }
    return true;
}

/**
 * Agrego el campo filtro en la tabla 'listado'
 */
function updateV63(){
    return execQuery('ALTER TABLE `listado` ADD COLUMN `filtro` TEXT DEFAULT NULL AFTER `metadata`;',false,array(1062));
}

/**
 * Elimino el campo atributos de la tabla 'listado'
 */
function updateV64(){
    return execQuery('ALTER TABLE `listado` DROP COLUMN `atributos`;',false,array(1091));
}

/**
 * Instalo el m�dulo DFM
 */
function updateV65(){
    return installDFMModule();
}

/**
 * Seteo los nombres y mime de los productos en la tabla 'temporal'
 */
function updateV66(){
    return execQuery('
        UPDATE `temporal`,`producto_temporal`
        SET
            `temporal`.name = `producto_temporal`.filename,
            `temporal`.metadata = CONCAT(\'{"mime":"\',`producto_temporal`.mime,\'"}\')
        WHERE
            `temporal`.id_temporal = `producto_temporal`.id_temporal;
    ');
}

/**
 * Elimino las columnas filename y mime de la tabla 'producto_temporal'
 */
function updateV67(){
    return execQuery('
        ALTER TABLE `producto_temporal`
            DROP COLUMN `filename`,
            DROP COLUMN `mime`;
        ',
        false,
        array(1091)
    );
}

/**
 * Migro el nombre y mime de un archivo de la tabla configuracion a la tabla temporal
 */
function updateV68(){
    global $mysqlError,$mysqlErrno;

    $conexion = getDbLink();
    $resultado = mysql_query('
        select * from `configuracion`
        where indice1 is not null
    ');

    if(!$resultado){
        mysql_close($conexion);
        $mysqlError = mysql_error($conexion);
        $mysqlErrno = mysql_errno($conexion);
        reportMysqlErrors();
        return false;
    }

    if(mysql_num_rows($resultado) > 0){
        while($fila = mysql_fetch_assoc($resultado)){
            if(!empty($fila['metadata']) && ($metadata = json_decode($fila['metadata'],true)) && is_array($metadata['parametros']) && !empty($metadata['parametros']['filename'])){

                # Obtengo toda la metadata, a excepci�n del nombre de archivo y MIME
                $new_parametros = array();
                foreach($metadata['parametros'] as $key => $value){
                    if($key == 'filename' || $key == 'mime'){
                        continue;
                    }
                    $new_parametros[$key] = $value;
                }

                # Migro los datos de la metadata de la tabla configuracion a la
                # metadata de la tabla temporal
                $mysql_query = mysql_query('
                    UPDATE `temporal`
                    SET
                        name = \''.urldecode($metadata['parametros']['filename']).'\',
                        metadata = \'{"mime":"'.urldecode($metadata['parametros']['mime']).'"}\'
                    WHERE
                        id_temporal = '.$fila['valor'].';
                ');
                $mysqlError = mysql_error($conexion);
                $mysqlErrno = mysql_errno($conexion);
                if($mysqlErrno){
                    reportMysqlErrors();
                    return false;
                }

                # Acoto los datos en la metadata de la configuraci�n
                $metadata['parametros'] = $new_parametros;
                $mysql_query = mysql_query('
                    UPDATE `configuracion`
                    SET
                        metadata = \''.json_encode($metadata).'\'
                    WHERE
                        id_configuracion = '.$fila['id_configuracion'].';
                ');
                $mysqlError = mysql_error($conexion);
                $mysqlErrno = mysql_errno($conexion);
                if($mysqlErrno){
                    reportMysqlErrors();
                    return false;
                }
            }
        }
    }

    # Cierro la conexi�n
    mysql_close($conexion);
    return true;
}


/**
 * Seteo los nombres y mime de los productos en la tabla 'temporal'
 */
function updateV69(){
    return execQuery('
        UPDATE `temporal`,`tarea_temporal`
        SET
            `temporal`.name = `tarea_temporal`.filename,
            `temporal`.metadata = CONCAT(\'{"mime":"\',`tarea_temporal`.mime,\'"}\')
        WHERE
            `temporal`.id_temporal = `tarea_temporal`.id_temporal;
    ');
}

/**
 * Elimino las columnas filename y mime de la tabla 'producto_temporal'
 */
function updateV70(){
    return execQuery('
        ALTER TABLE `tarea_temporal`
            DROP COLUMN `filename`,
            DROP COLUMN `mime`;
        ',
        false,
        array(1091)
    );
}

/**
 * Agrego la columna 'admin_only' en la tabla Listado
 */
function updateV71(){
    return execQuery('
        ALTER TABLE `listado`
            ADD COLUMN `admin_only` CHAR(1) NOT NULL
            DEFAULT 0
            AFTER `filtro`;
        ',
        false,
        array(1060)
    );
}

/**
 * Agrego la tabla "producto_producto"
 *
 * No pueden agregarse las foreign keys porque la tabla producto es MyISAM
 * @link http://www.verysimple.com/blog/2006/10/22/mysql-error-number-1005-cant-create-table-mydbsql-328_45frm-errno-150/
 */
function updateV72(){
    return execQuery("
        CREATE TABLE  `producto_producto` (
            `id_producto1` int(10) unsigned NOT NULL,
            `id_producto2` int(10) unsigned NOT NULL,
            PRIMARY KEY  (`id_producto1`,`id_producto2`),
            KEY `Index_2` (`id_producto1`),
            KEY `Index_3` (`id_producto2`)
        ) ENGINE=InnoDB;",
        false,
        array(1050)
    );
}

/**
 * Instalo el m�dulo AudDataObject en la tabla "producto_producto"
 */
function updateV73(){
    return installAudDataObject('producto_producto');
}

/**
 * Cambio el engine de la tabla "producto_producto" de InnoDB a MyISAM
 */
function updateV74(){
     return execQuery('ALTER TABLE `producto_producto` ENGINE = MyISAM;');
}

/**
 * Agrego los �ndices "id_listado1" y "id_listado2" en la tabla "producto_producto"
 */
function updateV75(){
     return execQuery('
        ALTER TABLE `producto_producto`
            ADD COLUMN `id_listado1` INT(10) UNSIGNED NOT NULL AFTER `id_producto2`,
            ADD COLUMN `id_listado2` INT(10) UNSIGNED NOT NULL AFTER `id_listado1`,
            ADD INDEX `Index_4`(`id_listado1`),
            ADD INDEX `Index_5`(`id_listado2`);
        ',
        false,
        array(
            1060, # Duplicate column name
            1061  # Duplicate key name
        )
     );
}

/**
 * Ejecuta el update 6 del DSE
 */
function updateV76(){
    require_once '../megaUpdater/dse_update_functions.php';
    return updateDSEV6();
}

/**
 * Ejecuta el update 7 del DSE
 */
function updateV77(){
    require_once '../commons/inny.core.php';
    require_once '../megaUpdater/dse_update_functions.php';
    return updateDSEV7();
}

/**
 * Agrega los contadores a los productos
 */
function updateV78(){

    return execQuery("
        ALTER TABLE `producto`
            ADD COLUMN `contador1` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `texto10`,
            ADD COLUMN `contador2` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `contador1`,
            ADD COLUMN `contador3` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `contador2`,
            ADD COLUMN `contador4` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `contador3`,
            ADD COLUMN `contador5` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `contador4`;
        ",
        false,
        array(
            1060 # Duplicate column name
        )
     );
}

/**
 * Agrega los el campo data a la tabla "producto_producto"
 */
function updateV79(){

    return execQuery("
        ALTER TABLE `producto_producto`
            ADD COLUMN `data` BLOB DEFAULT NULL AFTER `id_listado2`;
        ",
        false,
        array(
            1060 # Duplicate column name
        )
     );
}

/**
 * Agrega �ndices a la tabla "producto"
 */
function updateV80(){

    return execQuery("
        ALTER TABLE `producto`
            ADD COLUMN `indice1` INTEGER UNSIGNED NOT NULL AFTER `contador5`,
            ADD COLUMN `indice2` INTEGER UNSIGNED NOT NULL AFTER `indice1`,
            ADD COLUMN `indice3` INTEGER UNSIGNED NOT NULL AFTER `indice2`,

            ADD INDEX `Index_ind1`(`indice1`),
            ADD INDEX `Index_ind2`(`indice2`),
            ADD INDEX `Index_ind3`(`indice3`);
        ",
        false,
        array(
            1060 # Duplicate column name
        )
     );
}

/**
 * Cambia el tipo de dato de los campos "texto1", "texto2" y "texto3" de BLOB a TEXT
 */
function updateV81(){

    return execQuery("
        ALTER TABLE `producto`
            MODIFY COLUMN `texto1` TEXT DEFAULT NULL,
            MODIFY COLUMN `texto2` TEXT DEFAULT NULL,
            MODIFY COLUMN `texto3` TEXT DEFAULT NULL
        ;"
     );
}

/**
 * Agrego el campo de c�digo de idioma para el CMS de cada sitio del InnyCMS
 */
function updateV82(){

    return execQuery(
        "ALTER TABLE `sitio`ADD COLUMN `idioma_codigo` CHAR(5) NOT NULL DEFAULT 'es_ar' AFTER `smtp`;",
        false,
        array(
            1060 # Duplicate column name
        )
    );
}

/**
 * Agrego diez campos de texto y dos �ndices m�s a la tabla "producto"
 */
function updateV83(){

    return execQuery("
    ALTER TABLE `producto`
        ADD COLUMN `texto11` TEXT DEFAULT NULL AFTER `texto10`,
        ADD COLUMN `texto12` TEXT DEFAULT NULL AFTER `texto11`,
        ADD COLUMN `texto13` TEXT DEFAULT NULL AFTER `texto12`,
        ADD COLUMN `texto14` TEXT DEFAULT NULL AFTER `texto13`,
        ADD COLUMN `texto15` TEXT DEFAULT NULL AFTER `texto14`,
        ADD COLUMN `texto16` TEXT DEFAULT NULL AFTER `texto15`,
        ADD COLUMN `texto17` TEXT DEFAULT NULL AFTER `texto16`,
        ADD COLUMN `texto18` TEXT DEFAULT NULL AFTER `texto17`,
        ADD COLUMN `texto19` TEXT DEFAULT NULL AFTER `texto18`,
        ADD COLUMN `texto20` TEXT DEFAULT NULL AFTER `texto19`,

        ADD COLUMN `indice4` INTEGER UNSIGNED NOT NULL AFTER `indice3`,
        ADD COLUMN `indice5` INTEGER UNSIGNED NOT NULL AFTER `indice4`,

        ADD INDEX `Index_ind4`(`indice4`),
        ADD INDEX `Index_ind5`(`indice5`);"
        ,false,array(
            1060 # Duplicate column name
        )
    );
}

/**
 * Cambio el tipo de dato de la columna 'metadata' de la tabla 'listado'
 */
function updateV84(){
     return execQuery("ALTER TABLE `listado` MODIFY COLUMN `metadata` LONGTEXT DEFAULT NULL;");
}

/**
 * Cambio el tipo de dato de la columna 'metadata' de la tabla 'sitio'
 */
function updateV85(){
     return execQuery("ALTER TABLE `sitio` MODIFY COLUMN `metadata` LONGTEXT DEFAULT NULL;");
}

function updateV86(){
    if (!execQuery('ALTER TABLE `sitio` ADD COLUMN `dynamic_metadata` LONGTEXT DEFAULT NULL'))	return false;
    return true;
}


/**
 * Cambio el tipo de dato de la columna 'valor' de la tabla 'configuracion' a MEDIUMBLOB
 * Cambio el tipo de dato de todos los 'texto' de la tabla 'producto' a MEDIUMTEXT
 */
function updateV87(){
    if (!execQuery('ALTER TABLE `configuracion` CHANGE COLUMN `valor` `valor` MEDIUMBLOB NULL DEFAULT NULL'))	return false;
	if (!execQuery('
		ALTER TABLE `producto`
			CHANGE COLUMN `texto1` `texto1` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto2` `texto2` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto3` `texto3` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto4` `texto4` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto5` `texto5` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto6` `texto6` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto7` `texto7` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto8` `texto8` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto9` `texto9` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto10` `texto10` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto11` `texto11` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto12` `texto12` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto13` `texto13` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto14` `texto14` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto15` `texto15` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto16` `texto16` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto17` `texto17` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto18` `texto18` MEDIUMTEXT NULL DEFAULT NULL,
			CHANGE COLUMN `texto19` `texto19` MEDIUMTEXT NULL DEFAULT NULL, 
			CHANGE COLUMN `texto20` `texto20` MEDIUMTEXT NULL DEFAULT NULL;
	'))	return false;

    return true;
}

function updateV88(){
    return createConfiguration('dokko_login_app',0,'INNY-TRACKER-PRODUCCION','','Nombre de la App en el DokkoLogin',false,1);
}

function updateV89(){

    return execQuery("
        ALTER TABLE `producto`
            MODIFY COLUMN `indice1` INTEGER UNSIGNED NULL,
            MODIFY COLUMN `indice2` INTEGER UNSIGNED NULL,
            MODIFY COLUMN `indice3` INTEGER UNSIGNED NULL,
            MODIFY COLUMN `indice4` INTEGER UNSIGNED NULL,
            MODIFY COLUMN `indice5` INTEGER UNSIGNED NULL,
			MODIFY COLUMN `so_field_0` int(10) unsigned NULL,
			MODIFY COLUMN `so_field_1` int(10) unsigned NULL
		",
        false,
        array(
            1060 # Duplicate column name
        )
     );
}

