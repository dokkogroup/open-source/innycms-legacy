<?php
/**
 * update_added_flag.php
 *
 * Itera por cada documento. De cada documento obtiene todas las relaciones
 * del documento. Luego se queda con las categorias de cada documento que
 * tengan mayor profundidad de nivel en cada vista, y les actualiza el flag.
 *
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 * @copyright Copyright (c) 2009 Dokko Group.
 * @link http://www.dokkogroup.com.ar/
 *
 * @package dse
 * @version $Id: update_added_flag.php,v 1.1 2009-07-27 20:41:05 dnail Exp $
 */

require_once '../commons/dse_common.php';
dse_loadConfigs();

function getDeeperLevel($array,$id_view) {
	
	if (!isset($array[$id_view]) || count($array[$id_view]) === 0) {
		# No hay categorias asignadas
		return -1;
	}
	
	$deeper = 0;
	foreach ($array[$id_view] as $category) {
		if ($deeper < $category->level) {
			$deeper = $category->level;
		}
	}
	
	return $deeper;
}

function updateRefs($flaggedDao) {
	
	# Si hay parent (si no hay parent es vista, por lo que no hay 
	# nada para hacer)
	if ($flaggedDao->id_parent) {
	
		$ref = DB_DataObject :: factory("dse_document_category");
		$ref->id_category = $flaggedDao->id_parent;
		$ref->id_document = $flaggedDao->id_document;
		if ($ref->find(true)) {
			$ref->refs = $ref->refs + 1;
			if (!$ref->update()) {
				 echo "Update Error ... \n";
			}	 
			updateRefs($ref);
		}
		$ref->free();
	
	}
	
	return;
	
}

###############################################################################
##									CUERPO									 ##
###############################################################################

# No quiero limites de tiempo
set_time_limit(0);
error_reporting(E_ALL);

$daoDocument = DB_DataObject :: factory(DSE_DOCUMENT_TABLENAME);
$daoDocument->find();

# Por cada documento
while ($daoDocument->fetch()) {
	# Obtengo las categorias
	$category = $daoDocument->getCategories();
	
	# Si el documento tiene categorias
	if ($category !== null && $category->find()) {
		# Armo el arreglo donde voy a almacenar las categorias de mayor nivel.
		# El arreglo es dela forma id_view => [category1,category2, ... ,categoryN]
		$added = array();
		
		while ($category->fetch()) {
			
			# Si el nivel de la categoria que se esta procesando es mayor
			# que el mas profudo que tengo ahora lo cambio
			$currentDeeper = getDeeperLevel($added,$category->id_view);

			if ($currentDeeper < $category->level) {
				# Remuevo las categorias que tenia almacenadas y coloco la nueva
				unset($added[$category->id_view]);
				$added[$category->id_view] = array(clone($category));
			}
			# Si el nivel es el mas profundo, la agrego a la lista actual
			elseif ($currentDeeper === $category->level) {
				$added[$category->id_view][] = clone($category);
			}
			# Si el nivel actual es mayor, no hago nada
			 else {
				continue;
			}	
			
		}
		
		# Libero la memoria del DAO
		$category->free();
		
		# De todas las categorias agregadas, actualizo las relaciones con el documento
		foreach ($added as $id_view => $cats) {
			
			# Si no hay categorias agregadas para la vista, continuo con las demas
			if (count($cats) === 0) {
				continue;
			}
			
			# Por cada categoria agregada
			foreach ($cats as $aCategory) {
				
				$id_document = DSE_DOCUMENT_TABLEID;
				
				$dc = DB_DataObject :: factory("dse_document_category");
				$dc->id_document = $daoDocument->$id_document;
				$dc->id_category = $aCategory->id_category;
				
				# Si existe la relacion, la actualizo.
				if ($dc->find(true)) {
					$dc->added = 1;
					$dc->update();
					
					# Solo para mostrar el mensaje
					$idDoc = $daoDocument->$id_document;
					echo "Actualizada la relacion ($idDoc, $id_view,$aCategory->id_category) ";
					if ($aCategory->isLeaf()) {echo "HOJA";} else {echo "NO ES HOJA";};
					echo " <br/>\n";
				}
				# Libero la memoria de la relacion
				$dc->free();
				
			}
		}
		echo "-------------------------------------------------- <br/>\n";
		unset($added);
	} else {
		$id_document = DSE_DOCUMENT_TABLEID;
		$idDoc = $daoDocument->$id_document;
		echo "El documento $idDoc no tiene categorias <br/>\n";
	}
}

# Libero la memoria del DAO document 
$daoDocument->free();

# Ahora paso a actualizar las referencias.
# Este paso se hace por si hay casos patologicos.

# En primer lugar pongo todos los contadores de referencias en cero.
$relationships = DB_DataObject :: factory("dse_document_category");
$relationships->refs = 0;
$relationships->whereAdd("refs <> 0");
$relationships->update(true);
$relationships->free();

echo "Puestos todos los contadores de referencias a 0 <br/>\n";

# Una vez puestos los contadores a cero, obtengo las relaciones con flag
# y actualizo los contadores en toda la rama

echo "Actualizando todas las referencias <br/>\n";

$flaggedRelationships = DB_DataObject :: factory("dse_document_category");
$flaggedRelationships->added = 1;

if ($flaggedRelationships->find()) {

	while ($flaggedRelationships->fetch()) {
		# Primero actualizo esta referencia
		$flaggedRelationships->refs = 1;
		if (!$flaggedRelationships->update()) { 
			echo "Update error ... \n";
		}	
		# Luego voy para arriba (o abajo segun Fede)
		updateRefs($flaggedRelationships);
	}
}

$flaggedRelationships->free();

echo "FINALIZADA LA EJECUCION DEL SCRIPT <br/>\n";