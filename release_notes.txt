InnyCMS - Change Log

InnyCMS - v0.1.11 
====================================


------------------------------------
README:
------------------------------------

InnyCMS - v0.1.10 
====================================
- 0007506: [bug] [CLIENTE] No se visualiza el campo de ayuda al editar un producto (gbracalente) - closed.
- 0007008: [new feature] Agregar soporte PNG (gbracalente) - closed.
- 0007380: [bug] [innyCMS] - Cardinalidades de productos (gbracalente) - closed.
- 0004712: [bug] [metadata] Para archivos definido de tipo File en la metadata y al cargar una imagen no hacer el re-dimensionado por defecto (jgutierrez) - closed.
- 0005006: [new feature] [cliente] En el listado de usuarios no se puedem hacer busquedas como el los otros listados comunes. (jgutierrez) - closed.
- 0005086: [new feature] Cambiar imagen de logo en header y footer (jgutierrez) - closed.
- 0005085: [new feature] Armar manual de uso autogenerado para los sitios (jgutierrez) - closed.
- 0005108: [bug] [CORE] El limite de tama�o de los listados no se controla cuando est� chequeado el bot�n de seguir cargando. (jgutierrez) - closed.
- 0005084: [database] Cambiar el tipo de datos de la metadata de los sitios de TEXT a LONGTEXT (jgutierrez) - closed.
- 0005088: [new feature] [CORE] Permitir ocultar/visibilizar los listados relacionados en la carga/edici�n de un producto (jgutierrez) - closed.
[10 issues]

InnyCMS - v0.1.9 
===================================
- 0003878: [new feature] [CORE] El tipo "file" debe tener en cuenta los par�metros en caso que su contenido sea una imagen (user4) - closed.
- 0003889: [new feature] [SMARTY PLUGIN] modificador sw_key2value (user4) - closed.
- 0003860: [new feature] [CORE] Delete en el DAO Sitio (user4) - closed.
- 0003859: [bug] [TRACKER] No se v� el t�tulo en el listado de listados (user4) - closed.
- 0003774: [bug] [CLIENTES] mejorar performance en vaciar un listado (user4) - closed.
- 0003773: [bug] [BASE] Agregar DTD y HTML base al template de email de contacto (user4) - closed.
- 0003745: [bug] [SW PLUGIN] Bug en plugin swp_get_producto (user4) - closed.
- 0004351: [user interface] [CLIENTES] Corregir las flechas de ordenamiento en el listado de productos (user4) - closed.
- 0004344: [user interface] [CLIENTES] Replicar la botonera en el view de un producto (user4) - closed.
- 0004352: [new feature] [CORE] Metadata en listado: par�metro limit (user4) - closed.
- 0004354: [new feature] [CORE] Metadata en listado: par�metro readonly (user4) - closed.
- 0002335: [new feature] [CLIENTES] Eliminar productos de forma masiva en el listado de productos (user4) - closed.
- 0004357: [new feature] [CORE] Metadata en listado: par�metro permission (user4) - closed.
- 0004330: [new feature] [CORE] Nuevo par�metro para el tipo richtext: toolbar (user4) - closed.
- 0004326: [new feature] Cambiar el editor del inny por el CKEditor (user4) - closed.
- 0004325: [new feature] Crear una configuraci�n de sitio para que los listados de productos se ordenen en orden creciente/decreciente (user4) - closed.
- 0004115: [bug] Al realizar una carga masiva desde cvs se consulme toda la memoria y palma el servidor (jgutierrez) - closed.
- 0003910: [bug] [SMARTY PLUGIN] El plugin swp_lister no obtiene los campos de auditor�a (user4) - closed.
- 0003897: [user interface] [CLIENTES] Diferenciar los contenidos privados del resto (user4) - closed.
- 0003896: [user interface] [CLIENTES] Mostrar el listado de contenidos alfab�ticamente (user4) - closed.
- 0003601: [new feature] [API] API para editar Contenidos (user4) - closed.
- 0004358: [bug] [CORE] Controlar la carga masiva en listados con cantidad fija - closed.
[22 issues]

InnyCMS - v0.1.8 
===================================
- 0003691: [user interface] Bot�n "Eliminar todos" del listado, confuso (user4) - closed.
- 0003340: [user interface] [CLIENTES] Diferenciar los listados y contenidos privados (user4) - closed.
- 0003625: [new feature] [CLIENTES] Ampliar los contenidos de los usuarios del listado de usuarios (user4) - closed.
- 0003614: [bug] [CLIENTES] No oculta los listados ocultos en el select de escoger listado (user4) - closed.
- 0003386: [bug] [TRACKER] Mostrar los logs de los sitios (user4) - closed.
- 0003546: [bug] [CLIENTES] Bug en el attach de im�genes del ABM de Productos (user4) - closed.
- 0003545: [new feature] [API] funci�n para obtener un producto dada su posici�n en el listado (user4) - closed.
- 0003494: [bug] [CLIENTES] Problema con el par�metro "checked" de los checkbox (user4) - closed.
- 0003495: [bug] Cuando se cambia un archivo, no se cambia el nombre del archivo original (user4) - closed.
- 0003262: [new feature] Cardinalidades entre productos (user4) - closed.
- 0003626: [bug] [CLIENTES] Cuando se agrega un usuario, lanza un error de par�metro GET (user4) - closed.
- 0002409: [new feature] [CLIENTES] Agregar filtros de b�squeda en el listado de productos (user4) - closed.
- 0002580: [new feature] Multi-Idioma del CMS (user4) - closed.
- 0003675: [user interface] [CLIENTES] Cambiar el wysiwyg (user4) - closed.
- 0003672: [new feature] [CORE] Opci�n para definir si un sitio puede crear contenidos (user4) - closed.
- 0003663: [bug] [BASE] lowercase el protocolo para env�o de emails (user4) - closed.
- 0003637: [bug] [TRACKER] No muestra los links de descarga para los archivos adjuntos a una Tarea (user4) - closed.
- 0003634: [new feature] [CORE] Par�metro "description" para los campos del tipo archivo de los productos (user4) - closed.
- 0003633: [new feature] [CORE] Par�metro "unique" para los campos del tipo texto de los productos (user4) - closed.
- 0003511: [bug] [CLIENTES] Truncar los t�tulos de las columnas del listado de productos (user4) - closed.
- 0003399: [new feature] [SMARTY PLUGIN] Ahora sobra una l�nea en los c�digos del tracker de google (user4) - closed.
- 0003202: [user interface] [CLIENTES] Mostrar las categor�as asociadas a un producto en el view del producto (user4) - closed.
- 0003348: [bug] [ADMIN] Cuando muestra las categor�as de un producto no muestra el nombre de las vistas de cada categor�a (user4) - closed.
- 0003396: [bug] [SMARTY PLUGIN] sw_getconfig: bug cuando se obtiene la URL de una imagen (user4) - closed.
- 0003361: [user interface] [CLIENTES] Mostrar el listado de listado de ordenado por nombre (user4) - closed.
- 0003263: [new feature] Contadores (user4) - closed.
- 0003238: [bug] El tipo checkbox por default siempre asume true (user4) - closed.
- 0003203: [bug] [CLIENTES] No se muestra la vista en los breadcrums de las categor�as en el ABM de Producto (user4) - closed.
- 0003201: [user interface] [CLIENTES] Mejorar la manera como se muestran los archivos en el ABM de un producto (user4) - closed.
- 0003177: [bug] No se ven las im�genes en "Administrar Contenidos" (user4) - closed.
- 0003171: [bug] [ADMIN] Cambiar el texto "Deseleccionar Categoria" por "Quitar Categoria" en los listados con categorias (user4) - closed.
- 0002673: [new feature] Multi-Idioma para los sitios web (user4) - closed.
[32 issues]

InnyCMS - v0.1.7a 
====================================
- 0003420: [new feature] Agregar campos de �ndice al producto, y funcionalidad para su seteo (user4) - closed.
- 0003180: [user interface] [CLIENTES] �conos para acciones sobre archivos en el abm de un producto (mrubio) - closed.
[2 issues]

InnyCMS - v0.1.7 
===================================
- 0002359: [new feature] Implementar el tipo de dato 'select' (user4) - closed.
- 0002713: [new feature] [SMARTY PLUGIN] bloque para iterar listados de un sitio (user4) - closed.
- 0002712: [new feature] Agregar el campo "filtro" en la tabla listado (user4) - closed.
- 0002754: [new feature] Migrar nombre de archivo y mime a la tabla temporal (user4) - closed.
- 0002753: [new feature] Agregar m�dulo DFM (user4) - closed.
- 0002370: [bug] Poder eliminar valores seteados previamente en la metadata del archivo INI (user4) - closed.
- 0002367: [user interface] Que los mensajes de error cuando se convierte la metadata se obtengan del speech (user4) - closed.
- 0002366: [new feature] Agregado de par�metros para el tipo integer (user4) - closed.
- 0002365: [new feature] Agregado de par�metros para el tipo float (user4) - closed.
- 0002360: [user interface] Implementar el tipo de dato 'radio' (user4) - closed.
- 0002946: [new feature] Guardar la metadata en UTF-8 en lugar de codificaci�n url (user4) - closed.
- 0002911: [new feature] [TRACKER] Boton para actualizar toda la metadata de los sitios (user4) - closed.
- 0002909: [bug] Reemplazar la manera de entregar archivos en el download.php (user4) - closed.
- 0002945: [new feature] Cambiar el orden de la clave y valor de la configuracion so_fieldN_values del DSE (user4) - closed.
- 0002711: [user interface] [CLIENTES] Cambios en el look-and-feel del listado de productos (user4) - closed.
- 0002710: [user interface] Setear que campos de texto se mostrar�n en el listado de productos (user4) - closed.
- 0002608: [new feature] Agregar controles de manejo de cache en imagenes y documentos (user4) - closed.
- 0002599: [new feature] Listados y contenidos privados (user4) - closed.
- 0002581: [bug] Separar las reglas htaccess base de las locales de los sitios (user4) - closed.
- 0003098: [bug] Al eliminar una categor�a (en ocasiones) elimina toda una vista de la base de datos. (user4) - closed.
[20 issues]

InnyCMS - v0.1.6 
===================================
- 0001752: [new feature] [ADMIN] Listado de Tareas (user4) - closed.
- 0002271: [bug] Poder definir un numero fijo de archivos para cada producto con posicion fija (hcastillo) - closed.
- 0002287: [new feature] Crear funcionalidad para version Tiny (user4) - closed.
- 0002308: [new feature] Funcionalidad para usuarios registrados (hcastillo) - closed.
- 0002312: [bug] Eliminar los resultados por p�gina del listado de listados (user4) - closed.
- 0001751: [new feature] [ADMIN] ABM de Tareas (user4) - closed.
- 0002344: [new feature] Validaci�n para los archivos del tipo flash (user4) - closed.
- 0002316: [user interface] Valores por default en la metadata (hcastillo) - closed.
- 0002315: [user interface] Que en la metadata se pueda definir el nombre de un archivo (hcastillo) - closed.
- 0002364: [new feature] [TRACKER] Eliminar la funcionalidad de editar contenidos (user4) - closed.
- 0002336: [new feature] Validaci�n para los archivos del tipo sound (user4) - closed.
- 0002088: [bug] Problema cuando se setean par�metros por 1era vez a un Contenido (user4) - closed.
- 0002362: [new feature] Agregar el par�metro 'resize' al tipo 'image' (user4) - closed.
- 0002361: [user interface] Crear un plugin smarty para el wysiwyg (user4) - closed.
- 0002358: [new feature] Valor por defecto de par�metro 'format' en el tipo 'date' (user4) - closed.
- 0002356: [bug] En los contenidos, se resetea el tipo de dato a richtext (user4) - closed.
- 0002355: [bug] Los archivos no se eliminan al actualizar/borrar un Contenido (user4) - closed.
- 0002354: [new feature] [CLIENTES] Agregar bot�n para borrar el valor de los Contenidos (user4) - closed.
- 0002332: [user interface] Plugin Smarty para ver datos de un Producto o un Contenido (user4) - closed.
- 0002331: [user interface] Modificar como se muestran los archivos en el ABM de productos (hcastillo) - closed.
- 0002363: [user interface] Reemplazar el visor de contenidos Thickbox por Smootbox (user4) - closed.
- 0002326: [new feature] Implementar el tipo de dato 'checkbox' (user4) - closed.
- 0002369: [new feature] Migrar funcionalidad para obtener el input basado en el tipo de dato (user4) - closed.
- 0002398: [bug] [CLIENTES] Si un listado solo tiene un producto, no deben aparecer las flechas de ordenamiento (hcastillo) - closed.
- 0002408: [new feature] [CLIENTES] Ampliar el rango de los campos de texto cuando se importa desde CSV (user4) - closed.
- 0002380: [new feature] Agregar valor 'parametros' en la metadata del listado (user4) - closed.
- 0002330: [bug] Posiciones negativas de los archivos relacionados a un Producto (user4) - closed.
- 0002381: [new feature] Funcionalidad para que los productos de un listado sean buscables (user4) - closed.
- 0002470: [new feature] [CLIENTES] ABM y Listado de los Usuarios Registrados al sitio (user4) - closed.
- 0002569: [new feature] Reporte de errores de los plugins (user4) - closed.
- 0002512: [bug] [PROYECTO BASE] Agregar el archivo con nombre "google9fa8845c30dc6688.html" al directorio web (user4) - closed.
- 0002552: [bug] Se pueden ver los productos de un sitio desde otro (user4) - closed.
- 0002546: [bug] Se maman los botones de "volver" luego de editar un producto cuando hay m�s de un tab abierto editando productos de distintos (user4) - closed.
- 0002532: [bug] aparentemente sw_get_listado esta recuperando el listado con nombre X sin importar el sitio (user4) - closed.
- 0002482: [bug] baja performance al usar el plugin swp_lister abriendolo y cerrandolo sin escreibir nada en medio (user4) - closed.
- 0002553: [new feature] Agregar al inny core funciones para obtener productos e ids de listados (user4) - closed.
- 0002625: [new feature] [CLIENTES] Incluir path de PEAR en caso que el sitio est� en TINY MODE (user4) - closed.
- 0002626: [new feature] Los campos de b�squeda del DSE deben poder setearse en la metadata (user4) - closed.
[38 issues]

InnyCMS - v0.1.5 
===================================
- 0002184: [bug] [TRACKER] Se deslogea al intentar ver la informacion de una tarea en el listado (hcastillo) - closed.
- 0002110: [new feature] agregar m�s campos de texto a cada producto (user4) - closed.
- 0002082: [bug] Plugin swp_url: error en los thums (user4) - closed.
- 0002148: [bug] [CLIENTES] cuando se elimina un producto, quiere mostrarlo y lanza error (hcastillo) - closed.
- 0002162: [user interface] [CLIENTES] El listado de listados no debe paginarse (hcastillo) - closed.
- 0002053: [new feature] [CLIENTE] agregar ordenamiento de archivos por posicion dentro de un producto (hcastillo) - closed.
- 0002087: [bug] Problema con la metadata de un contenido del tipo imagen GIF (user4) - closed.
- 0002185: [bug] [CLIENTES] En la seccion estado de cambios al ver un cambio se deforman los botones de acciones (hcastillo) - closed.
- 0002161: [user interface] [CLIENTES] Agregar filtro de b�squeda por nombre en el listados de listados (hcastillo) - closed.
- 0002160: [user interface] [CLIENTES] Agregar switch para cambiar de listado (hcastillo) - closed.
- 0002159: [user interface] [CLIENTES] Que los listados sean link HTML (hcastillo) - closed.
- 0002158: [user interface] [CLIENTES] Que los botones de la botonera principal sean links (hcastillo) - closed.
- 0002057: [bug] [CLIENTES] Corregir los redirects (hcastillo) - closed.
- 0002058: [user interface] [CLIENTES] Orden de los botones (hcastillo) - closed.
- 0002157: [user interface] [CLIENTES] Resaltar cantidad de productos en los listados (hcastillo) - closed.
- 0002177: [bug] no se pueden eliminar archivos en campos de contenidos (hcastillo) - closed.
- 0002113: [user interface] [CLIENTES] No se muestra correctamente la pantalla al editar un elemento de un listado que contenga imagenes. (hcastillo) - closed.
- 0002163: [new feature] Agregados en el tipo "date" (hcastillo) - closed.
- 0002217: [new feature] Agregar p�ginas para errores los errores 403, 404 y 500 (user4) - closed.
- 0002220: [new feature] Agregar campo en la tabla Sitio para el google analytics (user4) - closed.
- 0002221: [bug] No funciona correctamente el link para descargar un archivo en la seccion "Estado de cambio." (hcastillo) - closed.
[21 issues]

InnyCMS - v0.1.4 
===================================
- 0001917: [bug] [CLIENTES] Permitir borrar dentro de un producto el/los archivo/s asociados al mismo. (hcastillo) - closed.
- 0002001: [bug] No se muestra el valor correcto de las cantidades de archivos de un producto (hcastillo) - closed.
- 0001999: [bug] [CLIENTES] Redirect cuando se cambia el orden de un producto (hcastillo) - closed.
- 0001994: [bug] No alojar en sesion el DAO del usuario logueado (hcastillo) - closed.
- 0001988: [user interface] [CLIENTES] Botones al editar un producto (hcastillo) - closed.
- 0002022: [new feature] Agregar el campo version en el DAO Sitio (user4) - closed.
- 0002007: [user interface] Agregar acciones m�ltiples en el listado de sitios (hcastillo) - closed.
- 0001847: [user interface] [CLIENTES] Mejorar los listados de listados (hcastillo) - closed.
- 0001996: [new feature] Agregar el campo url en la tabla Sitio (hcastillo) - closed.
- 0001995: [bug] [CLIENTES] Mostrar el usuario en la vista de datos del Cliente (hcastillo) - closed.
- 0001987: [user interface] [CLIENTES] Agregar un link al sitio web (hcastillo) - closed.
- 0001993: [bug] Agregar funcionalidad para sitios en mantenimiento (hcastillo) - closed.
- 0002080: [bug] [CLIENTES] No se pueden eliminar productos de un listado. (hcastillo) - closed.
- 0002059: [bug] [ADMIN] Se pierden los datos al editar un sitio (user4) - closed.
- 0002055: [new feature] Conversi�n de configuraciones del archivo CONFIG.ini a Metadata (user4) - closed.
- 0002054: [new feature] Las configuraciones para listados y contenidos deben tomarse del archivo CONFIG.ini (user4) - closed.
- 0002051: [bug] [CLIENTE] Ordenar listado de productos por posicion al ingresar a editarlo (hcastillo) - closed.
- 0001840: [new feature] Juntar secci�n Admin con Tracker (user4) - closed.
- 0002081: [bug] [CLIENTES] Cambiar el mensaje que se muestra al hacer el login si en sitio no tiene informacion de metadata. (hcastillo) - closed.
[19 issues]

InnyCMS - v0.1.3 
===================================
- 0001860: [database] Agregar el campo smtp a la tabla sitio (user4) - closed.
- 0001859: [database] Agregar la tabla Usuarios (user4) - closed.
- 0001858: [database] Agregar configuraci�n para el puerto del SMTP (user4) - closed.
- 0001854: [bug] Que se puedan editar los atributos de los listados en el template (user4) - closed.
- 0001853: [bug] [TRACKER] Permitir editar y eliminar listados de productos (user4) - closed.
- 0001870: [deployment] Cambiar de nombre el archivo config.ini a CONFIG.ini (user4) - closed.
- 0001871: [user interface] [ADMIN] Agregar tipo de configuraci�n para sistema y mail (user4) - closed.
- 0001857: [new feature] Agregar par�metro para porcentaje de compresi�n en los atributos de los listados (user4) - closed.
- 0001850: [new feature] agregar al .cvsignore el archivo "config.ini.local" (user4) - closed.
- 0001848: [user interface] [CLIENTES] Que los productos de los listados muestren una imagen, en caso de ser productos con imagen. (hcastillo) - closed.
- 0001888: [bug] Que la tabla Producto_temporal herede de AudDataObject (hcastillo) - closed.
- 0001862: [new feature] Sitios con m�ltiples usuarios (hcastillo) - closed.
- 0001861: [bug] Nueva funcionalidad para el env�o de mails por SMTP (user4) - closed.
- 0001878: [bug] [CLIENTES] no resizear las im�genes en el editar producto (hcastillo) - closed.
- 0001889: [bug] Que la tabla Ctacorriente herede de AudDataObject (hcastillo) - closed.
- 0001887: [user interface] [CLIENTES] incrementar la cantidad de resultados por p�gina de los listados (hcastillo) - closed.
- 0001841: [bug] [CLIENTES] los input para adjuntar archivos se ven horribles (hcastillo) - closed.
- 0001842: [bug] [CLIENTES] Todos los input del tipo file no muestran bien el texto (hcastillo) - closed.
- 0001890: [new feature] Instalar el m�dulo AudDataObject a la tabla Estado (hcastillo) - closed.
- 0001895: [user interface] [TRACKER] Agregar ordenamiento en los id del listado de sitios (hcastillo) - closed.
- 0001899: [new feature] Hacer plugin Smarty swp_linesplit (user4) - closed.
- 0001864: [bug] [CLIENTES] Corregir redirect en listado de productos (hcastillo) - closed.
- 0001863: [user interface] [CLIENTES] Agregar ordenamiento por orden a los productos de un Listado (hcastillo) - closed.
- 0001610: [bug] [CLIENTES] En la edicion de un producto, si ya tenia contenido y lo quiero borrar por completo no cambia el contenido (hcastillo) - closed.
- 0001843: [bug] [TRACKER] Corregir el redirect hacia la secci�n Clientes (hcastillo) - closed.
- 0001650: [bug] [ADMIN] Error en editar los datos de un empleado (hcastillo) - closed.
- 0001845: [deployment] Hacer del sitio Dokkogroup.com un sitio aparte (user4) - closed.
- 0001844: [deployment] Hacer de la demo del InnyCMS un sitio aparte (user4) - closed.
- 0001912: [new feature] Modificar el plugin swp_url. (user4) - closed.
- 0001964: [new feature] Puede mostrarse un thumb en base a solo una dimensi�n (user4) - closed.
[30 issues]

InnyCMS - v0.1 
=================================
- 0000873: [bug] Incrementar la cantidad de datos que se muestra en el "Ver Sitio" (user15) - closed.
- 0000872: [new feature] Poder editar un sitio (user15) - closed.
- 0000870: [new feature] funcionalidades para crear nuevo sitio (user15) - closed.
- 0000890: [new feature] Listado de renglones de cuenta corriente (user15) - closed.
[4 issues]

InnyCMS - v0.1.1 
===================================
- 0000901: [new feature] ver si se puede hacer funcionar el .htaccess en el directorio clientes del host ftp (user4) - closed.
- 0000903: [bug] la longitud del nombre del sitio es demasiado corta. Colocar un m�ximo de 100 caracteres. (user15) - closed.
- 0000907: [user interface] cambiar el color en el listado de issues del tracker cuando la tarea ya se asigno a un empleado (user4) - closed.
- 0000902: [new feature] guardar una cookie con el nombre de usuario para el login de clientes (user4) - closed.
- 0000904: [bug] mensaje de error al intentar editar un pedido de cambio que ya esta comenzado (user4) - closed.
- 0000905: [user interface] que muestre un mensaje indicando que no hay resultados cuando los listados son vacios (user4) - closed.
- 0000900: [bug] Cambiar la palabra "monto" de monto asignado por "costo". (user4) - closed.
- 0000906: [user interface] Definir el contenido de la secci�n "Mi sitio" (user16) - closed.
- 0000932: [bug] admin: separar el footer en un template aparte (user15) - closed.
- 0000898: [bug] cambiar los nombres de imagen que tienen mayusculas por minusculas (user4) - closed.
- 0000920: [bug] admin: no mostrar los passwords en el listado de empleados (user15) - closed.
- 0000921: [bug] admin: cambiar el pass del empleado (user15) - closed.
- 0000924: [bug] admin: listado de sitios ordenados por nombre (user15) - closed.
- 0000935: [bug] admin: poner el link de logout en el header (user15) - closed.
- 0000942: [bug] admin: en listado de empleados, juntar el apellido y nombre (user15) - closed.
- 0000926: [user interface] admin: mostrar mensaje si el listado de cta corriente de un sitio est� vac�o (user15) - closed.
- 0000925: [new feature] admin: link para editar sitio en la p�gina de "Ver Sitio" (user15) - closed.
- 0000927: [bug] admin: en el listado de sitios, indicar si los valores est�n vac�os (user15) - closed.
- 0000922: [bug] admin: en el listado de sitios, poner un link al listado de renglones de su cuenta corriente (user15) - closed.
- 0000929: [bug] admin: link para editar empleado en la p�gina de "Ver Empleado" (user15) - closed.
- 0000930: [bug] admin: listado de empleados ordenados por nombre (user15) - closed.
- 0000936: [bug] admin: problemas con el link de volver en el formulario de agregar/editar usuario (user15) - closed.
- 0000931: [bug] admin: mensaje de error incorrecto al agregar/editar un empleado (user15) - closed.
- 0000928: [user interface] admin: mejorar el paginador (user15) - closed.
- 0000899: [bug] problemas al setear el precio (no lo guarda) (user4) - closed.
- 0000933: [bug] admin: no permitir agregar dos sitios con el mismo nombre (user15) - closed.
- 0000934: [bug] admin: no permitir agregar dos empleados con el mismo username (user15) - closed.
- 0000944: [bug] admin: filtros en el listado de empleados (user15) - closed.
- 0000940: [new feature] admin: filtros en el listado de sitios (user15) - closed.
- 0000941: [bug] admin: agregar ordenamientos en listado de sitios (user15) - closed.
- 0000923: [new feature] admin: js para chequeo de formularios (user15) - closed.
- 0000943: [bug] admin: agregar ordenamientos en listado de empleados (user15) - closed.
- 0000977: [user interface] admin: agregar iconos a los listado (user15) - closed.
- 0000871: [new feature] Poder eliminar un sitio (user15) - closed.
- 0000978: [user interface] Admin: borrado l�gico de sitios y de empleados (user15) - closed.
- 0000999: [bug] admin: aumentar el tama�o del campo "username" en la tabla sitio (user15) - closed.
- 0000982: [bug] Admin: focus en el login (user15) - closed.
- 0000981: [new feature] Admin: Agregar configuraciones (user15) - closed.
- 0001000: [bug] admin: chequear los maxlength de los input de los formularios (user15) - closed.
- 0001216: [user interface] Icono para secci�n de listados de productos (mrubio) - closed.
- 0001233: [user interface] Mejorar la imagen "sinlogo.gif" (mrubio) - closed.
- 0001234: [user interface] Hacer un �cono que indique la descarga de un archivo (mrubio) - closed.
- 0000980: [bug] hacer el metodo para q no quede cachedos los estilos (user4) - closed.
- 0001250: [user interface] Imagenes para los listados de Productos (mrubio) - closed.
[44 issues]

InnyCMS - v0.1.2 
===================================
- 0001724: [new feature] Contemplar la cantidad de archivos m�xima que se podr�n relacionar a un Producto (hcastillo) - closed.
- 0001718: [new feature] Migrar datos de la tabla prducto a una nueva tabla n-n producto_temporal (hcastillo) - closed.
- 0001799: [new feature] [TRACKER] Acceso a la seccion Clientes (hcastillo) - closed.
- 0001800: [new feature] [ADMIN] Agregar la configuraci�n 'url_server_clientes' (hcastillo) - closed.
- 0001760: [user interface] [TRACKER] Que el listado de sitios muestre el ID (hcastillo) - closed.
- 0001808: [user interface] Cambiar el nombre de la variable "id_archivos" a "ids_archivo" (hcastillo) - closed.
- 0001749: [user interface] [TRACKER] Que los listados de tareas se vea ordenado descendente por fecha por defecto (hcastillo) - closed.
- 0001723: [user interface] [CLIENTES] Iconos en listados de productos (hcastillo) - closed.
- 0001720: [new feature] Adaptar el bloque swp_lister para obtener los n archivos relacionados con el producto (hcastillo) - closed.
- 0001719: [new feature] Modificar el ABM del producto de la secci�n Clientes para agregar m�ltiples archivos (hcastillo) - closed.
- 0001810: [bug] Plugin Smarty swp_url: que no requiera estar dentro de un lister (hcastillo) - closed.
- 0001811: [bug] Correcciones en el .htaccess de la carpeta web (hcastillo) - closed.
- 0001812: [new feature] Crear el plugin Smarty swp_get_producto (hcastillo) - closed.
- 0001813: [new feature] Crear el plugin Smarty swp_split (hcastillo) - closed.
- 0001814: [new feature] Plugin Smarty sw_getconfig: agregar par�metro mode (hcastillo) - closed.
- 0001750: [new feature] [TRACKER] Una tarea puede no tener costo (hcastillo) - closed.
- 0001839: [bug] Plugin dk_urlformat: tambi�n evitar mostrar los espacios como   (user4) - closed.
- 0001838: [new feature] Crear la clase Inny (user4) - closed.
- 0001624: [new feature] Agregar en el Tracker secci�n para editar/eliminar contenido de los sitios (user4) - closed.
- 0001622: [user interface] Im�genes de flechas para Listados de Productos (mrubio) - closed.
- 0001616: [new feature] agregarle un campo de prioridad a los productos de los listados (user4) - closed.
- 0001582: [new feature] Datos de usuario editables (user4) - closed.
- 0001594: [bug] Problema en el login del admin (user4) - closed.
- 0001595: [new feature] Flag para bloquear creaci�n de listados en sitios (user4) - closed.
[24 issues]