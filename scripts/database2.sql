-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.32-Debian_7etch5-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema sw_mobile_web_services
--

CREATE DATABASE IF NOT EXISTS sw_mobile_web_services;
USE sw_mobile_web_services;

--
-- Definition of table `comentario`
--

DROP TABLE IF EXISTS `comentario`;
CREATE TABLE `comentario` (
  `id_comentario` int(11) NOT NULL auto_increment,
  `texto` varchar(45) NOT NULL,
  `id_tarea` int(10) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `autor` varchar(45) NOT NULL,
  PRIMARY KEY (`id_comentario`),
  KEY `FK_comentario_1` (`id_tarea`),
  CONSTRAINT `FK_comentario_1` FOREIGN KEY (`id_tarea`) REFERENCES `tarea` (`id_tarea`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comentario`
--

/*!40000 ALTER TABLE `comentario` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentario` ENABLE KEYS */;


--
-- Definition of table `ctacorriente`
--

DROP TABLE IF EXISTS `ctacorriente`;
CREATE TABLE `ctacorriente` (
  `id_ctacorriente` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`id_ctacorriente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ctacorriente`
--

/*!40000 ALTER TABLE `ctacorriente` DISABLE KEYS */;
/*!40000 ALTER TABLE `ctacorriente` ENABLE KEYS */;


--
-- Definition of table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
CREATE TABLE `empleado` (
  `id_empleado` int(10) unsigned NOT NULL auto_increment,
  `id_ctacorriente` int(10) unsigned default NULL,
  `nombre` varchar(100) default NULL,
  `apellido` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `telefono` varchar(100) default NULL,
  PRIMARY KEY (`id_empleado`),
  KEY `FK_empleado_1` (`id_ctacorriente`),
  CONSTRAINT `FK_empleado_1` FOREIGN KEY (`id_ctacorriente`) REFERENCES `ctacorriente` (`id_ctacorriente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empleado`
--

/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;


--
-- Definition of table `renglon`
--

DROP TABLE IF EXISTS `renglon`;
CREATE TABLE `renglon` (
  `id_renglon` int(10) unsigned NOT NULL auto_increment,
  `id_ctacorriente` int(10) unsigned NOT NULL,
  `id_tarea` int(10) unsigned default NULL,
  `monto` int(10) unsigned NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  PRIMARY KEY  (`id_renglon`,`id_ctacorriente`),
  KEY `FK_renglon_1` (`id_ctacorriente`),
  KEY `FK_renglon_2` (`id_tarea`),
  CONSTRAINT `FK_renglon_1` FOREIGN KEY (`id_ctacorriente`) REFERENCES `ctacorriente` (`id_ctacorriente`),
  CONSTRAINT `FK_renglon_2` FOREIGN KEY (`id_tarea`) REFERENCES `tarea` (`id_tarea`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `renglon`
--

/*!40000 ALTER TABLE `renglon` DISABLE KEYS */;
/*!40000 ALTER TABLE `renglon` ENABLE KEYS */;


--
-- Definition of table `sitio`
--

DROP TABLE IF EXISTS `sitio`;
CREATE TABLE `sitio` (
  `id_sitio` int(10) unsigned NOT NULL auto_increment,
  `nombre_sitio` varchar(100) NOT NULL,
  `id_ctacorriente` int(10) unsigned default NULL,
  `nombre_encargado` varchar(100) default NULL,
  `apellido` varchar(50) default NULL,
  `telefono` varchar(50) default NULL,
  `direccion` varchar(100) default NULL,
  `email` varchar(100) default NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY  (`id_sitio`),
  UNIQUE KEY `Index_3` (`username`),
  KEY `FK_sitio_1` (`id_ctacorriente`),
  CONSTRAINT `FK_sitio_1` FOREIGN KEY (`id_ctacorriente`) REFERENCES `ctacorriente` (`id_ctacorriente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sitio`
--

/*!40000 ALTER TABLE `sitio` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitio` ENABLE KEYS */;


--
-- Definition of table `tarea`
--

DROP TABLE IF EXISTS `tarea`;
CREATE TABLE `tarea` (
  `id_tarea` int(10) unsigned NOT NULL auto_increment,
  `estado` varchar(45) NOT NULL,
  `id_sitio` int(10) unsigned NOT NULL,
  `id_empleado` int(10) unsigned default NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `descripcion` blob NOT NULL,
  PRIMARY KEY  (`id_tarea`),
  KEY `FK_tarea_1` (`id_sitio`),
  KEY `FK_tarea_2` (`id_empleado`),
  CONSTRAINT `FK_tarea_1` FOREIGN KEY (`id_sitio`) REFERENCES `sitio` (`id_sitio`),
  CONSTRAINT `FK_tarea_2` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id_empleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tarea`
--

/*!40000 ALTER TABLE `tarea` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarea` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
