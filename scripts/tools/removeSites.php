<?php

define("R_INFO", 1);
define("R_ERROR", 2);
define("R_OK", 3);
define("R_ACTION", 4);
define("R_ECHO", 5);
define("R_IMPORTANT", 6);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once '../../denko/dk.denko.php';

$config_file = 'DB.ini.backup';
function formatOutput($text, $html, $color) {
    if (!$html) {
        return $text;
    }
    return '<font color="' . $color . '"><b>' . $text . '</b></font>';
}

function reportSplit() {
    if (isset($_SERVER['HTTP_HOST'])) {
        report("<hr>");
    } else {
        report("------------------------------------------------------------");
    }
}

function report($text, $tipo = R_ECHO) {
    if (isset($_SERVER['HTTP_HOST'])) {
        $eol = " <br>\n";
        $html = true;
    } else {
        $eol = "\n";
        $html = false;
    }
    switch ($tipo) {
        case R_INFO:
            $sol = formatOutput('Info: ', $html, 'black');
            break;
        case R_ERROR:
            $sol = formatOutput('Error: ', $html, 'red');
            break;
        case R_OK:
            $sol = formatOutput('Ok: ', $html, 'green');
            break;
        case R_ACTION:
            $sol = formatOutput(' -> ', $html, 'black');
            break;
        case R_IMPORTANT:
            $sol = '';
            $text = formatOutput($text, $html, 'black') . ' *';
            break;
        default:
            $sol = '';
            break;
    }
    echo $sol . $text . $eol;
}


function getConnectionInfo() {
    global $config_file;
    if (is_file($config_file)) {
    } else {
        report('No se encuentra el archivo DB.ini.backup correspondiente al BACKUP', R_ERROR);
        exit(6);
    }
    $arr = parse_ini_file($config_file);
    if (!isset($arr['database'])) {
        report('Error en el archivo DB.ini.backup', R_ERROR);
        exit(7);
    }
    $path = str_replace("mysql://", "", $arr['database']);
    list($usuario, $db) = explode("@", $path);
    $arrayTemp = explode(":", $usuario);
    $res['user'] = $arrayTemp[0];
    $res['pass'] = isset($arrayTemp[1]) ? $arrayTemp[1] : '';
    list($res['host'], $res['db']) = explode("/", $db);
    return $res;
}

function getDbLink() {
    global $config_file;
    $cInfo = getConnectionInfo($config_file);
    $link = mysql_connect($cInfo['host'], $cInfo['user'], $cInfo['pass']);
    if (!$link) {
        report('No me pude conectar a la base de datos', R_ERROR);
        exit(2);
    }
    mysql_select_db($cInfo['db'], $link);
    return $link;
}

function reportMysqlErrors() {
    global $mysqlError, $mysqlErrno;
    report("MysqlError #" . $mysqlErrno . " - " . $mysqlError, R_ERROR);
}

function execQuery($query, $quiet = false, $ignoreErrors = array()) {
    global $mysqlError, $mysqlErrno, $db_info;
    $link = getDbLink();
    $result = mysql_query($query, $link);
    $mysqlError = mysql_error($link);
    $mysqlErrno = mysql_errno($link);
    mysql_close($link);
    if (!$result) {
        if (!in_array($mysqlErrno, $ignoreErrors)) {
            if (!$quiet) {
                reportMysqlErrors();
            }
            return false;
        } else {
            return true;
        }
    }
    return $result;
}


function checkSitio($id_sitio) {
    $result = execQuery('SELECT * FROM sitio WHERE id_sitio=' . $id_sitio);
    if (mysql_num_rows($result) == 0) {
        return false;
    }
    return true;
}

function msg($str) {
    echo ($str) . "\n";
}

function confirm($msg) {
    echo $msg;
    $handle = fopen("php://stdin", "r");
    $line = fgets($handle);
    if (trim($line) != 's') {
        return false;
    }
    return true;
}

//-----------------------------------------------------------------------------------------
function deleteTemporal($id_temporal) {
    $result = execQuery('SELECT id_temporal,id_temporal_parent FROM temporal WHERE id_temporal=' . $id_temporal);
    $data = mysql_fetch_assoc($result);
    if (empty($data['id_temporal_parent'])) {
        $id_part = $data['id_temporal'];
        execQuery('DELETE FROM temporal WHERE id_temporal=' . $id_part);
        while ($id_part != null) {
            $result = execQuery('SELECT id_temporal,id_temporal_parent FROM temporal WHERE id_temporal_parent=' . $id_part);
            $data = mysql_fetch_assoc($result);
            $id_part = null;
            if (!empty($data['id_temporal'])) {
                execQuery('DELETE FROM temporal WHERE id_temporal=' . $data['id_temporal']);
                $id_part = $data['id_temporal'];
            }
        }
    }
}

function deleteTemporalHead($id_temporal) {
    if (!empty($id_temporal)){
        execQuery('DELETE FROM temporal WHERE id_temporal=' . $id_temporal);
    }
}


function eliminarTemporales($ids_temporal) {

    foreach ($ids_temporal as $id) {
        if (!empty($id) && $id != 'NULL') {
            deleteTemporalHead($id);
        }
    }
}

function getIdsSimple($id_sitio, $table, $id_table) {
    $ids = array();
    $result = execQuery('SELECT ' . $id_table . ' FROM ' . $table . ' WHERE id_sitio=' . $id_sitio);
    while ($fila = mysql_fetch_assoc($result)) {
        $ids[] = $fila[$id_table];
    }
    if (count($ids) > 0) {
        return implode(',', $ids);
    }
    return 'NULL';

}


function getIdsTareaSitio($id_sitio) {
    $ids = array();
    $result = execQuery('SELECT id_tarea FROM tarea WHERE id_sitio=' . $id_sitio);
    while ($fila = mysql_fetch_assoc($result)) {
        $ids[] = $fila['id_tarea'];
    }
    if (count($ids) > 0) {
        return implode(',', $ids);
    }
    return 'NULL';

}

function getIdsListadoSitio($id_sitio) {
    $ids = array();
    $result = execQuery('SELECT id_listado FROM listado WHERE id_sitio=' . $id_sitio);
    while ($fila = mysql_fetch_assoc($result)) {
        $ids[] = $fila['id_listado'];
    }
    if (count($ids) > 0) {
        return implode(',', $ids);
    }
    return 'NULL';

}


function getIdsProductoSitio($id_sitio) {
    $ids_listado = getIdsListadoSitio($id_sitio);
    $ids_producto = array();
    $result = execQuery('SELECT id_producto FROM producto WHERE id_listado IN (' . $ids_listado . ')');
    while ($fila = mysql_fetch_assoc($result)) {
        $ids_producto[] = $fila['id_producto'];
    }
    if (count($ids_producto) > 0) {
        return implode(',', $ids_producto);
    }
    return 'NULL';

}

function getIdsProductoTemporalSitio($id_sitio) {
    $ids_temporal = array();
    $ids_producto = getIdsProductoSitio($id_sitio);
    $result = execQuery('SELECT id_temporal FROM producto_temporal WHERE id_producto IN (' . $ids_producto . ')');
    while ($fila = mysql_fetch_assoc($result)) {
        $ids_temporal[] = $fila['id_temporal'];
    }
    if (count($ids_temporal) > 0) {
        $ids = implode(',', $ids_temporal);
        return $ids;
    }
    return 'NULL';
}

function getIdsTemporalTareaSitio($id_sitio) {
    $ids_temporal = array();
    $ids_tarea = getIdsTareaSitio($id_sitio);
    $result = execQuery('SELECT id_temporal FROM tarea_temporal WHERE id_tarea IN (' . $ids_tarea . ')');
    while ($fila = mysql_fetch_assoc($result)) {
        $ids_temporal[] = $fila['id_temporal'];
    }
    if (count($ids_temporal) > 0) {
        return implode(',', $ids_temporal);
    }
    return 'NULL';
}


function getTemporalesConf($id_sitio) {
    $ids_temporal = array();
    $result = execQuery('SELECT valor FROM configuracion WHERE indice1=' . $id_sitio . ' AND (metadata like \'%:\"image%\' or metadata like \'%:\"file%\' or metadata like \'%:\"sound%\' or metadata like \'%:\"flash%\')');
    while ($fila = mysql_fetch_assoc($result)) {
        if (!empty($fila['valor'])) {
            $ids_temporal[] = $fila['valor'];
        }
    }
    return $ids_temporal;
}

function purgarTemporalesHuerfanos(){
    $result=execQuery('SELECT id_temporal FROM temporal WHERE id_temporal in (select id_temporal from temporal TT where id_temporal_parent is not null and  not exists (select id_temporal from temporal where id_temporal = TT.id_temporal_parent))');
    $cant=mysql_num_rows($result);
    while ($cant>0){
        $result=execQuery('SELECT id_temporal FROM temporal WHERE id_temporal in (select id_temporal from temporal TT where id_temporal_parent is not null and  not exists (select id_temporal from temporal where id_temporal = TT.id_temporal_parent))');
        $cant=mysql_num_rows($result);
        $ids_delete=array();
        while ($fila = mysql_fetch_assoc($result)) {
            if (!empty($fila['id_temporal'])) {
                $ids_delete[]=$fila['id_temporal'];
            }
        }
        if (count($ids_delete) > 0){
            $query_delete=implode(',',$ids_delete);
            execQuery('DELETE FROM temporal where id_temporal IN ('.$query_delete.')');

        }
    }
}


/** Funcionalidad de purgar los temporales que puedan quedar colgados
 *
 */
function purgarBase() {
    msg('Ejecutando purgado de base');
    execQuery('ALTER TABLE `tarea_temporal` DROP FOREIGN KEY `FK_tarea_temporal_2`');
    execQuery('ALTER TABLE `tarea_temporal` ADD CONSTRAINT `FK_tarea_temporal_2` FOREIGN KEY (`id_temporal`) REFERENCES `temporal` (`id_temporal`) ON DELETE CASCADE');
    execQuery('ALTER TABLE `producto_temporal` DROP FOREIGN KEY `FK_producto_temporal_2`');
    execQuery('ALTER TABLE `producto_temporal` ADD CONSTRAINT `FK_producto_temporal_2`   FOREIGN KEY (`id_temporal`) REFERENCES `temporal` (`id_temporal`)  ON DELETE CASCADE');

    execQuery('DELETE FROM configuracion WHERE indice1=0');
    execQuery('DELETE FROM log WHERE indice1=0');

    $ids_temporal = array();
    $query = execQuery('SELECT valor FROM configuracion WHERE metadata like \'%:\"image%\' or metadata like \'%:\"file%\' or metadata like \'%:\"sound%\' or metadata like \'%:\"flash%\' ');
    while ($fila = mysql_fetch_assoc($query)) {
        if (!empty($fila['valor'])) {
            $ids_temporal[] = $fila['valor'];
        }
    }
    $query_conf = '';
    if (count($ids_temporal) > 0) {
        $ids = implode(',', $ids_temporal);
        $query_conf = 'id_temporal NOT IN (' . $ids . ')';
    }
    $query_temp = execQuery('SELECT id_temporal FROM producto_temporal');
    $temporales = array();
    while ($fila = mysql_fetch_assoc($query_temp)) {
        if (!empty($fila['id_temporal'])) {
            $temporales[] = $fila['id_temporal'];

        }
    }
    $query_temporales = '';
    if (count($temporales) > 0) {
        $ids_temporales = implode(',', $temporales);
        $query_temporales = 'id_temporal NOT IN (' . $ids_temporales . ')';
    }
    $query_temporales_tarea = execQuery('SELECT id_temporal FROM tarea_temporal');
    $temporales_tarea = array();
    while ($fila = mysql_fetch_assoc($query_temporales_tarea)) {
        if (!empty($fila['id_temporal'])) {
            $temporales_tarea[] = $fila['id_temporal'];
        }
    }
    $query_tarea = '';
    if (count($temporales_tarea) > 0) {
        $ids = implode(',', $temporales_tarea);
        $query_tarea = 'id_temporal NOT IN (' . $ids . ')';
    }
    $query_final = '';
    if (!empty($query_temporales)) {
        $query_final = $query_temporales;
    }
    if (!empty($query_conf)) {
        $query_final.= (!empty($query_final)) ? " AND $query_conf" : $query_conf;
    }
    if (!empty($query_tarea)) {
        $query_final .= (!empty($query_final)) ? " AND $query_tarea" : $query_tarea;
    }

    if (!empty($query_final)) {
        $result = execQuery('SELECT id_temporal FROM temporal WHERE ' . $query_final. 'AND id_temporal_parent IS NULL');
        while ($fila = mysql_fetch_assoc($result)) {
            if (!empty($fila['id_temporal'])) {
                deleteTemporalHead($fila['id_temporal']);
            }
        }
    }
    purgarTemporalesHuerfanos();

    execQuery('ALTER TABLE `tarea_temporal` DROP FOREIGN KEY `FK_tarea_temporal_2`');
    execQuery('ALTER TABLE `tarea_temporal` ADD CONSTRAINT `FK_tarea_temporal_2` FOREIGN KEY (`id_temporal`)  REFERENCES `temporal` (`id_temporal`) ON DELETE RESTRICT');
    execQuery('ALTER TABLE `producto_temporal` DROP FOREIGN KEY `FK_producto_temporal_2`');
    execQuery('ALTER TABLE `producto_temporal` ADD CONSTRAINT `FK_producto_temporal_2`   FOREIGN KEY (`id_temporal`) REFERENCES `temporal` (`id_temporal`)  ON DELETE RESTRICT');

}

function execDeleteQueries($id_sitio) {
    msg('######################### Comenzando borrado sitio ' . $id_sitio . ' ###############################');
    $ids_listado = getIdsListadoSitio($id_sitio);
    $ids_producto = getIdsProductoSitio($id_sitio);
    $ids_temporal = getIdsProductoTemporalSitio($id_sitio);
    $ids_temporal_tarea = getIdsTemporalTareaSitio($id_sitio);
    $ids_tarea = getIdsTareaSitio($id_sitio);
    $ids_temporal_conf = getTemporalesConf($id_sitio);

    execQuery('ALTER TABLE `tarea_temporal` DROP FOREIGN KEY `FK_tarea_temporal_2`');
    execQuery('ALTER TABLE `tarea_temporal` ADD CONSTRAINT `FK_tarea_temporal_2` FOREIGN KEY (`id_temporal`) REFERENCES `temporal` (`id_temporal`) ON DELETE CASCADE');
    execQuery('ALTER TABLE `producto_temporal` DROP FOREIGN KEY `FK_producto_temporal_2`');
    execQuery('ALTER TABLE `producto_temporal` ADD CONSTRAINT `FK_producto_temporal_2`   FOREIGN KEY (`id_temporal`) REFERENCES `temporal` (`id_temporal`)  ON DELETE CASCADE');
    execQuery('DELETE FROM usuario WHERE id_sitio=' . $id_sitio);

    msg('Eliminando temporales de configuracion...');
    msg('Temporales: ' . implode(',', $ids_temporal_conf));

    eliminarTemporales($ids_temporal_conf);

    execQuery('DELETE FROM configuracion WHERE indice1=' . $id_sitio);

    execQuery('DELETE FROM log WHERE indice1=' . $id_sitio);

    execQuery('DELETE FROM renglon WHERE id_tarea IN (' . $ids_tarea . ')');

    execQuery('DELETE FROM comentario WHERE id_tarea IN (' . $ids_tarea . ')');

    msg('Eliminando temporales de tarea...');
    msg('Temporales: ' . $ids_temporal_tarea);
    eliminarTemporales(explode(',', $ids_temporal_tarea));
    execQuery('DELETE FROM tarea WHERE id_sitio=' . $id_sitio);
    execQuery('DELETE FROM dse_document_category WHERE index1=' . $id_sitio);
    execQuery('DELETE FROM dse_category WHERE index1=' . $id_sitio);

    execQuery('DELETE FROM producto_producto WHERE id_producto1 IN (' . $ids_producto . ')');

    msg('Eliminando temporales de productos...');
    msg('Temporales: ' . $ids_temporal);

    eliminarTemporales(explode(',', $ids_temporal));
    execQuery('DELETE FROM producto WHERE id_listado IN (' . $ids_listado . ')');

    execQuery('DELETE FROM listado WHERE id_sitio=' . $id_sitio);
    execQuery('DELETE FROM sitio WHERE id_sitio=' . $id_sitio);
    execQuery('ALTER TABLE `tarea_temporal` DROP FOREIGN KEY `FK_tarea_temporal_2`');
    execQuery('ALTER TABLE `tarea_temporal` ADD CONSTRAINT `FK_tarea_temporal_2` FOREIGN KEY (`id_temporal`)  REFERENCES `temporal` (`id_temporal`) ON DELETE RESTRICT');
    execQuery('ALTER TABLE `producto_temporal` DROP FOREIGN KEY `FK_producto_temporal_2`');
    execQuery('ALTER TABLE `producto_temporal` ADD CONSTRAINT `FK_producto_temporal_2`   FOREIGN KEY (`id_temporal`) REFERENCES `temporal` (`id_temporal`)  ON DELETE RESTRICT');
    msg('####################### Borrado finalizado  ##############################');
}

function exportSitio($id_sitio, $confirm = true) {
    $ids_sitio_delete = array();
    if (checkSitio($id_sitio)) {
        $result = execQuery('SELECT id_sitio FROM sitio WHERE id_sitio <> ' . $id_sitio);
        while ($fila = mysql_fetch_assoc($result)) {
            $ids_sitio_delete[] = $fila['id_sitio'];
        }
    }
    if (!$confirm || confirm('Se eliminaran ' . count($ids_sitio_delete) . ' sitios. Desea continuar? (s/n)')) {
        foreach ($ids_sitio_delete as $id_sitio) {
            borrarSitio($id_sitio, $confirm);
        }
        purgarBase();
        msg('---------- Exportacion finalizada -------------------');
    }

}

function exportSitios($ids_sitio, $confirm = true) {
    $sitios_export=str_replace('-',',',$ids_sitio);
    $ids_sitio_delete = array();

        $result = execQuery('SELECT id_sitio FROM sitio WHERE id_sitio NOT IN ( ' . $sitios_export).')';
        while ($fila = mysql_fetch_assoc($result)) {
            $ids_sitio_delete[] = $fila['id_sitio'];
        }

    if (confirm('Se eliminaran ' . count($ids_sitio_delete) . ' sitios. Desea continuar? (s/n)')) {
        foreach ($ids_sitio_delete as $id_sitio) {
            borrarSitio($id_sitio, $confirm);
        }
        purgarBase();
        msg('---------- Exportacion finalizada -------------------');
    }

}

function borrarSitio($id_sitio, $confirm = true) {
    global $config_file;
    $cInfo = getConnectionInfo($config_file);
    msg('Chequeando sitio ... ');
    if (!checkSitio($id_sitio)) {
        msg('No existe el sitio con id :' . $id_sitio . ' en la base: ' . $cInfo['db'] . ' de ' . $cInfo['host']);
    } else {
        msg('Ok');
        $data_sitio = execQuery('SELECT id_sitio,nombre_sitio,url,apellido FROM sitio WHERE id_sitio=' . $id_sitio);
        $data = mysql_fetch_assoc($data_sitio);
        if (!$confirm || confirm('Seguro que desea borrar el sitio >>> ' . $data['nombre_sitio'] . ' | ID: ' . $id_sitio . ' <<<  ' . "\n" . 'de la base: ' . $cInfo['db'] . ' en ' . $cInfo['host'] . ' ? (s/n)')) {
            execDeleteQueries($id_sitio);
        }
    }
}

function showHelp() {
    msg('------------------------------------------------------------------------------');
    msg('--------------------------- Ayuda - REMOVE SITES -----------------------------');
    msg('');
    msg('Uso: [accion] ([id_sitio]) ([opciones])');
    msg('');
    msg('Accion: ');
    msg('           -  list: Muestra los sitios de la base indicada en el DB.ini.backup');
    msg('           -  show: Muestra datos del sitio (id_sitio)');
    msg('           -  delete: Borra el sitio correspondiente a id_sitio');
    msg('           -  export: Borra todos los sitios menos el indicado en id_sitio');
    msg('           -  multiexport: Borra todos los sitios menos los indicados mediante id-id ej: (multiexport 23-45-78)');
    msg('           -  clean: purga la base de datos (logs, configuraciones y temporales huerfanos.');
    msg('');
    msg('Opciones: ');
    msg('           -  -f: Quita la confirmacion para el delete o export');
    msg('');
    msg('------------------------------------------------------------------------------');

}

function getInfoSitio($id_sitio) {
    if (!checkSitio($id_sitio)) {
        msg('No se encontro un sitio con id: ' . $id_sitio);
        exit;
    }
    $result = execQuery('SELECT id_sitio,nombre_sitio,url,apellido FROM sitio WHERE id_sitio=' . $id_sitio);
    $data = mysql_fetch_assoc($result);
    msg('Sitio: ' . $data['nombre_sitio'] . ' | Apellido: ' . $data['apellido'] . ' | Url: ' . $data['url']);
}

function listSitios() {
    global $config_file;
    $cInfo = getConnectionInfo($config_file);
    msg('----------------------------------------------------------------------------------------');
    msg('BASE DE DATOS: ' . $cInfo['db'] . ' - HOST: ' . $cInfo['host']);
    msg('----------------------------------------------------------------------------------------');
    msg('Listado de Sitios: ');
    $result = execQuery('SELECT id_sitio,nombre_sitio,url,apellido FROM sitio');
    while ($data = mysql_fetch_assoc($result)) {
        msg('ID: ' . $data['id_sitio'] . '  | Sitio: ' . $data['nombre_sitio'] . '     | Apellido: ' . $data['apellido']);
    }
    msg('----------------------------------------------------------------------------------------');

}


//--------------------------------------------------------------------------------------------

if (empty($argv[1])) {
    showHelp();
} else {
    if (!empty($argv[2])) {
        switch ($argv[1]) {
            case 'delete':
                $confirm = (!empty($argv[3]) && $argv[3] == '-f') ? false : true;
                borrarSitio($argv[2], $confirm);

                break;
            case 'show':
                getInfoSitio($argv[2]);
                break;
            case 'export':
                $confirm = (!empty($argv[3]) && $argv[3] == '-f') ? false : true;
                exportSitio($argv[2], $confirm);
                break;
            case 'multiexport':
                $confirm = (!empty($argv[3]) && $argv[3] == '-f') ? false : true;
                exportSitios($argv[2], $confirm);
                break;

            default:
                showHelp();
                break;
        }
    } else {
        switch ($argv[1]) {
            case 'list':
                listSitios();
                break;
            case 'clean':
                purgarBase();
                break;
            default:
                showHelp();
                break;
        }
    }
}
//----------------------------------------------------------------------------------------------
