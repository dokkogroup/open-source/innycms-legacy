#!/bin/bash

# This script is used to set the database name DB.ini file

dir=$(dirname $0)
cd "$dir"

cat DB.ini | sed "s/servicios_web/$1/" > DB.ini.local
cd DAOs
ln -s servicios_web.ini "$1.ini"
ln -s servicios_web.links.ini "$1.links.ini"