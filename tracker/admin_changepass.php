<?php
/**
 * Project: Inny Tracker
 * File: admin_changepass.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
################################################################################
function validarAdminPassword(&$smarty){

    $dokko_admin_pass = getConfig('dokko_admin_pass');
    Inny_Tracker::verificarCambiarPassword($smarty,$dokko_admin_pass);

    # En caso que est� todo OK
    if(!Denko::hasErrorMessages()){
        $daoConfiguracion = DB_DataObject::factory('configuracion');
        $daoConfiguracion->nombre = 'dokko_admin_pass';
        $daoConfiguracion->find(true);
        $daoConfiguracion->valor = $_POST['newpassword'];
        $daoConfiguracion->update();
        Denko::redirect('admin_changepass.php?change=ok');
    }

    return false;
}
################################################################################
# Verifico que el usuario logueado sea administrador
Inny_Tracker::verificarAdminLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# En caso que haya variables en POST
if(count($_POST) > 0){
    validarAdminPassword($smarty);
}

# En caso que haya que mostrar el mensaje de confirmaci�n del cambio de contrase�a
if(!empty($_GET['change']) && $_GET['change'] == 'ok'){
    $smarty->config_load('speech.conf','password');
    Denko::addOkMessage('password_okChange');
}

# Muestro el template
$smarty->display('admin_changepass.tpl');