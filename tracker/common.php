<?php
/**
 * Project: Inny Tracker
 * File: common.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once '../commons/inny.common.php';
require_once '../commons/inny.tracker.php';
require_once '../DAOs/Empleado.php';
require_once '../DAOs/Estado.php';
require_once '../DAOs/Configuracion.php';
###############################################################################
/**
 * Verifica que haya un usuario logueado
 */
function checkLoggedUser(){
    if(!isLoggedUser()){
        Denko::redirect('index.php?logout');
    }
}
################################################################################
/**
 * Muestra la pantalla de error ante un error cr�tico
 */
function displayCriticalError(&$smarty){
    $smarty->display('error.tpl');
    exit;
}
################################################################################
/**
 * Verifica que una acci�n sea v�lida
 */
function checkActionValida(&$smarty,$action,$actions){

    /**
     * Cargo los mensajes de error
     */
    $smarty->config_load('speech.conf','error');

    /**
     * En caso que la acci�n no est� seteada
     */
    if($action === null){
        Denko::addErrorMessage('error_missingParam',array('%param' => 'action'));
        displayCriticalError($smarty);
    }

    /**
     * En caso que la acci�n no est� contemplada
     */
    if(!in_array($action,$actions)){
        Denko::addErrorMessage('error_invalidAction',array('%action' => $action));
        displayCriticalError($smarty);
    }

    /**
     * En caso que est� todo OK
     */
    return true;
}
################################################################################
/**
 * Verifica que un DAO sea v�lido. Adem�s, verifica que el par�metro de ID del
 * DAO sea v�lido
 */
function checkDaoValido(&$smarty,$id_dao,$table){

    $smarty->config_load('speech.conf','error');

    /**
     * Verifico que el ID est� seteado
     */
    if($id_dao === null){
        Denko::addErrorMessage('error_missingParam',array('%param' => 'id_'.$table));
        displayCriticalError($smarty);
    }

    /**
     * Verifico que el ID sea un n�mero entero:
     */
    elseif(!Denko::isInt($id_dao)){
        Denko::addErrorMessage('error_paramIsNAN',array('%param' => 'id_'.$table));
        displayCriticalError($smarty);
	}

    /**
     * Verifico que la fila exista en la DB
     */
    $dao = DB_DataObject::factory($table);
    $id = 'id_'.$table;
    $dao->$id = $id_dao;
    if(!$dao->find()){
        Denko::addErrorMessage('error_dbNoExists',array('%param' => 'id_'.$table));
        displayCriticalError($smarty);
    }

    return $dao;
}
################################################################################
/**
 * Setea en sesion la URL de un listado. Sirve para volver al listado con las
 * variables en GET tal cual estaban
 */
function setSessionListadoURL($listado){
    $key = 'LISTADOURL_'.$listado;
    $_SESSION[$key] = $listado.'.php'.(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '');
}
###############################################################################
/**
 * Retorna la URL en sesion de un listado. Sirve para volver al listado con las
 * variables en GET tal cual estaban
 */
function getSessionListadoURL($listado){
    $key = 'LISTADOURL_'.$listado;
    return !empty($_SESSION[$key]) ? $_SESSION[$key] : $listado.'.php';
}
###############################################################################
/**
 * Verifica que la tarea sea v�lida
 */
function checkTareaValida(&$smarty,$id_tarea){
    return checkDaoValido($smarty,$id_tarea,'tarea');
}
################################################################################
/**
 * Verifica que el temporal sea v�lido
 */
function checkTemporalValido(&$smarty,$id_temporal){
    return checkDaoValido($smarty,$id_temporal,'temporal');
}
################################################################################
function checkConfiguracionValida(&$smarty,$id_configuracion){
    $smarty->config_load('speech.conf','error');
    $smarty->config_load('speech.conf','config');

    /**
     * Verifico que el id de la configuraci�n est�
     */
    if($id_configuracion === null){
        displayCriticalError($smarty,$smarty->get_config_vars('configuracion_errorMissingParam'));
    }

    /**
     * Verifico que el ID sea un n�mero entero:
     */
    elseif(!Denko::isInt($id_configuracion)){
        displayCriticalError($smarty,$smarty->get_config_vars('configuracion_errorIsNAN'));
	}

    /**
     * Verifico que la configuraci�n exista
     */
    $daoConfig = DB_DataObject::factory('configuracion');
    $daoConfig->id_configuracion = $id_configuracion;
    if(!$daoConfig->find()){
        displayCriticalError($smarty,$smarty->get_config_vars('configuracion_errorNoUserConfig'),array('%id'=>$id_configuracion));
    }

    /**
     * Verifico que la configuraci�n pertenezca al sitio
     */
    $daoConfig->fetch();
    $daoSitioActual = getLoggedUser();
    if($daoConfig->indice1 != $daoSitioActual->id_sitio){
        displayCriticalError($smarty,$smarty->get_config_vars('configuracion_errorNoUserConfig'),array('%id'=>$id_configuracion));
    }
    return true;
}
################################################################################
################################################################################
################################################################################
