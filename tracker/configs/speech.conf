[application]
application_name = "Inny Tracker"
application_version = "0.1.2"

[common]
common_add = "agregar"
common_edit = "editar"
common_ok = "aceptar"
common_cancel = "cancelar"
common_view = "ver"
common_delete = "eliminar"
common_back = "volver"
common_orderAsc = "orden ascendente"
common_orderDesc = "orden descendente"
common_download = "descargar"
common_empty = "-- sin valor --"
common_contenidos = "contenidos"
common_listado = "listados"
common_no = "No "
common_yes = "Si"
common_enable = "habilitar"
common_disable = "deshabilitar"
common_emptyList = "La consulta no produjo resultados"
common_search = "buscar"

[error]
error_requiredField = "Error: el campo '%field' es requerido."
error_invalidField = "Error: el valor en el campo '%field' no es v�lido."
error_equalsFields = "Error: los campos '%field1' y '%field2' deben ser iguales."
error_missingParam = "Error: el par�metro '%param' es requerido."
error_paramIsNAN = "Error: el par�metro '%param' debe ser un valor num�rico entero."
error_dbNoExists = "Error: el valor en el par�metro '%param' no es v�lido o no existe."
error_invalidAction = "Error: la acci�n '%action' no es v�lida."

error_noFile = "Error: el archivo no existe o no pudo enviarse."
error_mimeCSV = "Error: el archivo debe estar delimitado por coma (',') o punto y coma (';') (extensi�n '.csv')"
error_uploadExceedsSize = "Error: El archivo excede el tama�o m�ximo permitido."
error_uploadPartial = "Error: S�lo parte del archivo ha sido enviado."

[csv]
csv_label = "Archivo CSV"
csv_addFromFile = "agregar desde archivo CSV"
csv_replaceValues = "Reemplazar los valores actuales por los del archivo"
csv_fileHasHeader = "El archivo tiene fila de encabezamiento"
csv_loadFile = "cargar archivo"

[login]
login_pageTitle = "Ingresar"
login_username = "Usuario"
login_password = "Contrase�a"
login_submit = "ingresar"
login_errorUserOrPass = "Error: Usuario y/o Contrase�a no v�lidos"

[renglon]
renglon_sinMonto = "(sin costo)"
renglon_errorIsNAN = "Error: el costo debe ser un numero entero."
renglon_errorEstadoTarea = "Error: Para cambiar a estos estados debe asignarse un costo a la tarea."

[temporal]
temporal_errorMissingParam = "Debe especificarse el ID del temporal."
temporal_errorNoUserConfig = "El temporal con ID %id no existe."
temporal_errorIsNAN = "El ID del temporal debe ser un valor num�rico entero."

# Archivos
temporal_errorUploadIniSize = "Error: el archivo excede el m�ximo tama�o permitido."
temporal_errorUploadPartial = "Error: s�lo parte del archivo fu� subido"
temporal_errorUploadNoFile = "Error: no se pudo subir el archivo"

[userdata_changepass]
changepass_pageTitle = "Cambiar Contrase�a"
changepass_password = "Contrase�a"
changepass_newPass = "Nueva Contrase�a"
changepass_confirmPass = "Confirmar Contrase�a"
changepass_submit = "cambiar contrase�a"
changepass_errorPass = "Error: La contrase�a es incorrecta"

[userdata_edit]
userdataEdit_pageTitle = "Editar datos del usuario"

[paginator]
paginator_pages = "P�ginas"
paginator_prev = "Anterior"
paginator_next = "Siguiente"
paginator_results = "Resultados"

[configuracion]
configuracion_nombre = "Nombre"
configuracion_valor = "Valor"
configuracion_tipo = "Tipo"
configuracion_metadata = "Metadata"
configuracion_descripcion = "Descripci�n"
configuracion_mime = "MIME"
configuracion_ancho = "Ancho"
configuracion_alto = "Alto"
configuracion_deleteConfirm = "�Desea eliminar el contenido %nombre?"
configuracion_defaultValor = "Esta contenido a�n no tiene valor seteado."

# Generar metadata
configuracion_metadataTitle = "Actualizar metadata de todos los sitios"
configuracion_metadataButtonValue = "actualizar metadata"
configuracion_metadataIncluirDeshabilitados = "incluir sitios deshabilitados"

[contenido_listado]
contenidoListado_pageTitle = "Listado de Contenidos de %nombre_sitio"
contenidoListado_listTitleConfiguracion = "Listado de Contenidos"
contenidoListado_listTitleListados = "Listados de Productos"

[contenido_abm]
contenidoAbm_pageTitle = "Editar Contenido"

[contenido_view]
contenidoView_pageTitle = "Detalles del Contenido"

[producto]
producto_posicion = "Pos"
producto_texto1 = "Texto 1"
producto_texto2 = "Texto 2"
producto_texto3 = "Texto 3"
producto_textoDefault = "sin texto
producto_archivo = "Archivo"
producto_archivoDefault = "sin archivo"
producto_deleteConfirmMessage = "�Desea eliminar este producto?"
producto_deleteAllConfirmMessage = "�Desea eliminar todos los productos del listado %nombre?"

[producto_listado]
productoListado_pageTitle = "Productos del Listado %nombre"

[producto_view]
productoView_pageTitle = "Detalles del Producto"

[producto_abm]
productoAbm_pageTitleAdd = "Agregar Producto"
productoAbm_pageTitleEdit = "Editar Producto"

[listado]
listado_label = "Listado"
listado_nombre = "Nombre"
listado_atributos = "Atributos"
listado_atributosDefault = "sin atributos"
listado_deleteConfirmMessage = "�Desea eliminar el listado %nombre?"
listado_atributosNoEditables = "ATENCION: Los atributos deben editarse en el mismo template"

[listado_listado]
listadoListado_pageTitle = "Listados de productos de %nombre_sitio"
listadoListado_viewLinkTitle = "ver detalles del listado %nombre"
listadoListado_productoLinkTitle = "ver productos del listado %nombre"

[listado_view]
listadoView_pageTitle = "Detalles del Listado"

[listado_abm]
listadoAbm_pageTitle = "Editar datos del Listado"

################################################################################
[header]
# Common
header_sitioListado = "sitios"
header_tareaListado = "listado de tareas"
header_logout = "salir"
header_usuarioListado = "usuarios"
header_empleadoListado = "empleados"
header_logListado = "logs"

# Administrador
header_configuracionListado = "configuraciones"

# Empleado
header_miTareaListado = "mis tareas"
header_infoPersonal = "info personal"
header_ctacteListado = "cuenta corriente"

[filter]
filter_submit = "filtrar"
filter_select = "seleccionar"
filter_all = "todos"
filter_none = "ninguno"
filter_invert = "invertir"

[sitio]
# Datos del Sitio
sitio_idSitio = "ID"
sitio_nombreSitio = "Nombre del Sitio"
sitio_url = "URL"
sitio_version = "INNY Versi�n"
sitio_habilitado = "Habilitado"
sitio_enMantenimiento = "En mantenimiento"
sitio_creaContenidoYlistados = "Crea Contenidos y Listados"
sitio_gaCodigo = "C�digo Google Analytics"
sitio_idiomaCMS = "Idioma del CMS"
sitio_logErrors = "Mostrar Errores [DESARROLLO]"

# Datos del Propietario del Sitio
sitio_encargadoNombre = "Nombre del Propietario"
sitio_encargadoApellido = "Apellido del Propietario"
sitio_encargadoNyA = "Propietario"
sitio_telefono = "Tel�fono"
sitio_direccion = "Direcci�n"
sitio_email = "E-mail"
sitio_username = "Usuario"
sitio_password = "Contrase�a"

# Datos del FTP del sitio
sitio_ftp = "URL FTP"
sitio_ftpUsername = "Usuario FTP"
sitio_ftpPassword = "Contrase�a FTP"
sitio_pathCVS = "Path CVS"

# Dynamic metadata del  sitio
sitio_dynamicMetadataLabel="Metadata Din�mica"
sitio_dynamicMetadataLinkTitle="Ver/Editar metadata din�mica"
sitio_dynamicMetadataEditOk="Se actualiz� correctamente la metadata"

# Datos de la configuraci�n de env�o de email por SMTP
sitio_smtpLabel = "Datos del SMTP"
sitio_smtpHost = "SMTP Host (para usar sendmail local: local://sendmail)"
sitio_emailContacto = "Email de Contacto"
sitio_smtpUsername = "SMTP Username"
sitio_smtpPassword = "SMTP Password"
sitio_smtpProtocol = "SMTP Protocol"
sitio_smtpFromName = "SMTP FromName"
sitio_smtpPort = "SMTP Port"

# Secciones
sitio_viewPageTitle = "Detalles del Sitio"
sitio_insertPageTitle = "Agregar un Sitio"
sitio_updatePageTitle = "Editar datos del Sitio"
sitio_listadoPageTitle = "Listado de Sitios"

# Listado
sitio_insert = "agregar sitio"
sitio_contenidoLinkTitle = "contenidos"
sitio_listadoLinkTitle = "listados"
sitio_seccionClientesLinkTitle = "Ir a la secci�n Clientes"
sitio_crearListadosLinkTitle = "haga click aqu� para habilitar la creaci�n de listados para este sitio"
sitio_noCrearListadosLinkTitle = "haga click aqu� para deshabilitar la creaci�n de listados para este sitio"
sitio_crearListadosConfirm = "�Desea que el sitio %nombre_sitio cree listados de elementos?"
sitio_noCrearListadosConfirm = "�Desea que el sitio %nombre_sitio no cree listados de elementos?"
sitio_habilitadoLinkTitle = "Sitio Habilitado. Haga click aqu� para deshabilitarlo"
sitio_deshabilitadoLinkTitle = "Sitio Deshabilitado. Haga click aqu� para habilitarlo"
sitio_habilitarConfirm = "�Desea habilitar al sitio %nombre_sitio?"
sitio_deshabilitarConfirm = "�Desea deshabilitar al sitio %nombre_sitio?"
sitio_mantenimientoLinkTitle = "Sitio en Mantenimiento. Haga click aqu� para ponerlo en Producci�n."
sitio_produccionLinkTitle = "Sitio en Producci�n. Haga click aqu� para ponerlo en Mantenimiento."
sitio_mantenimientoConfirm = "�Desea poner al sitio %nombre_sitio? en mantenimiento"
sitio_produccionConfirm = "�Desea poner al sitio %nombre_sitio? en producci�n"
sitio_ctacteLinkTitle = "haga click aqu� para ver la cuenta corriente de este sitio"
sitio_contenidos = "contenidos"
sitio_contenidosLinkTitle = "haga click aqu� para ver los contenidos de este sitio"
sitio_listados = "listados"
sitio_listadosLinkTitle = "haga click aqu� para ver los listados de este sitio"

#
sitio_logErrorsEnabledConfirm = "�Desea que el sitio %nombre_sitio loguee los errores?"
sitio_logErrorsEnabledLinkTitle = "haga click aqu� para que el sitio loguee los errores"
sitio_logErrorsDisabledConfirm = "�Desea que el sitio %nombre_sitio muestre los errores?"
sitio_logErrorsDisabledLinkTitle = "haga click aqu� para que el sitio muestre los errores"

# Common
sitio_label = "Sitio"
sitio_optionEscoger = "-- escoja un sitio --"
sitio_optionAll = "-- todos los sitios --"
sitio_mantenimiento = "en mantenimiento"
sitio_produccion = "en producci�n"
sitio_metadata = "metadata"
sitio_metadataUpdateLinkTitle = "haga click aqu� para actualizar la metadata"
sitio_updateMetadataConfirm = "�Desea actualizar la metadata de este Sitio?"

# Deprecated ?
sitio_noValue = "(aun no tiene)"
sitio_host = "Host"
sitio_nya = "Apellido y nombre"
sitio_cvs = "CVS"

[usuario]
# Datos del Usuario
usuario_id = "ID"
usuario_username = "Usuario"
usuario_password = "Contrase�a"
usuario_nombre = "Nombre"
usuario_apellido = "Apellido"
usuario_nya = "Apellido y Nombres"
usuario_email = "E-Mail"
usuario_telefono = "Tel�fono"

# Listado
usuario_listadoPageTitle = "Listado de Usuarios"
usuario_habilitado = "Habilitado"
usuario_deshabilitado = "Deshabilitado"
usuario_habilitarLinkTitle = "haga click aqu� para habilitar a este Usuario"
usuario_habilitarConfirm = "�Desea habilitar al usuario %username?"
usuario_deshabilitarLinkTitle = "haga click aqu� para deshabilitar a este Usuario"
usuario_deshabilitarConfirm = "�Desea deshabilitar al usuario %username?"

# Secciones
usuario_viewPageTitle = "Detalles del Usuario"
usuario_insertPageTitle = "Agregar un Usuario"
usuario_updatePageTitle = "Editar datos del Usuario"

# Errores
usuario_errorDuplicateUsername = "Error: ya existe un Usuario con el Nombre de Usuario '%username'."

# Common
usuario_label = "Usuario"
usuario_insert = "agregar usuario"

[ctacorriente]
ctacorriente_label = "Cuenta Corriente"

[password]
password_label = "Contrase�a"
password_change = "Cambiar Contrase�a"
password_newPassword = "Nueva Contrase�a"
password_confirmNewPass = "Confirmar Nueva Contrase�a"
password_error = "Error: la contrase�a es incorrecta."
password_okChange = "La contrase�a ha sido cambiada correctamente."

[empleado]
empleado_id = "ID"
empleado_username = "Usuario"
empleado_password = "Contrase�a"
empleado_nombre = "Nombre"
empleado_apellido = "Apellido"
empleado_nya = "Apellido y Nombres"
empleado_email = "E-Mail"
empleado_telefono = "Tel�fono"
empleado_habilitado = "Habilitado"

# Listado
empleado_listadoPageTitle = "Listados de Empleados"
empleado_habilitadoLinkTitle = "Empleado Habilitado. Haga click aqu� para deshabilitarlo"
empleado_deshabilitadoLinkTitle = "Empleado Deshabilitado. Haga click aqu� para habilitarlo"
empleado_habilitarConfirm = "�Desea habilitar al empleado %username?"
empleado_deshabilitarConfirm = "�Desea deshabilitar al empleado %username?"
empleado_insert = "agregar empleado"
empleado_multiEnable = "�Desea habilitar todos los Empleados seleccionados?"
empleado_multiDisable = "�Desea desabilitar todos los Empleados seleccionados?"

# Secciones
empleado_viewPageTitle = "Detalles del Empleado"
empleado_insertPageTitle = "Agregar un Empleado"
empleado_updatePageTitle = "Editar datos del Empleado"

[configuracion]
configuracion_pageTitle = "Configuraciones"
configuracion_tipoLabel = "Tipo de Configuraci�n"

[contenido]
contenido_etiqueta = "Etiqueta"
contenido_nombre = "Nombre"
contenido_descripcion = "Descripci�n"
contenido_tipo = "Tipo"
contenido_valor = "Valor"

# Mensajes
contenido_zoomLinkText = "haga click aqu� para ampliar este contenido"
contenido_downloadLinkText = "haga click aqu� para descargar este archivo"
contenido_viewLinkText = "haga click aqu� ver este contenido"
contenido_deleteLinkText = "haga click aqu� para eliminar este contenido"
contenido_editLinkText = "haga click aqu� para editar este contenido"
contenido_confirmDelete = "�Desea eliminar este contenido?"
contenido_confirmMultiDelete = "�Desea eliminar estos contenidos?"

# Tipos de contenido
contenido_image = "imagen"
contenido_file = "archivo"
contenido_flash = "flash"
contenido_sound = "audio"
contenido_defaultImage = "sin imagen"
contenido_defaultFile = "sin archivo"
contenido_defaultFlash = "sin archivo flash"
contenido_defaultSound = "sin audio"
contenido_default = "sin contenido"

# Common
contenido_zoom = "ampliar"
contenido_download = "descargar"
contenido_listadoPageTitle = "Listado de contenidos del sitio %nombre_sitio"
contenido_deleteConfirm = "�Desea eliminar todos estos Contenidos?"

#
contenido_viewPageTitle = "Detalles del Contenido"

[listado]
listado_nombre = "Nombre"

# Listado:
listado_pageTitle = "Listados del sitio %nombre_sitio"
listado_multidelete = "eliminar listados"
listado_confirmMultidelete = "�Desea eliminar todos estos Listados?"

# Mensajes
listado_viewLinkTitle = "haga click aqu� para ver los datos de este listado"
listado_editLinkTitle = "haga click aqu� para editar los datos de este listado"
listado_deleteLinkTitle = "haga click aqu� para eliminar este listado"
listado_confirmDelete = "�Desea eliminar el listado '%nombre_listado'?"

[tarea]
tarea_estado = "Estado"
tarea_fechaCreacion = "Fecha de Creaci�n"
tarea_fechaInicio = "Fecha de Inicio"
tarea_fechaFin = "Fecha de Finalizaci�n"
tarea_descripcion = "Descripci�n"
tarea_creacion = "Creaci�n"
tarea_monto = "Costo"
tarea_archivo = "%nro� Archivo Adjunto"
tarea_asignadaA = "Asignada a"
tarea_archivosAdjuntos = "Archivos adjuntos"
tarea_downloadLinkTitle = "haga click aqu� para descargar el archivo '%filename'"
tarea_fechaCreacionDefault = "A�n no comenzada"
tarea_fechaInicioDefault = "A�n no iniciada"
tarea_fechaFinDefault = "A�n no finalizada"

# Listado
tarea_listadoPageTitle = "Listado de Tareas"
tarea_viewLinkTitle = "haga click aqu� para ver esta tarea"
tarea_editLinkTitle = "haga click aqu� para editar esta tarea"
tarea_deleteLinkTitle = "haga click aqu� para eliminar esta tarea"
tarea_confirmDelete = "�Desea eliminar esta tarea?"
tarea_confirmMultiDelete = "�Desea eliminar todas estas tareas?"
tarea_noAsignado = "(no asignada)"

# Secciones
tarea_viewPageTitle = "Detalles de la Tarea"

# Mensajes de error
tarea_errorMissingParam = "Debe especificarse el ID de la tarea."
tarea_errorNoUserConfig = "La tarea con ID %id no existe o no le pertenece."
tarea_errorIsNAN = "El ID de la tarea debe ser un valor num�rico entero."
tarea_errorUnassigned = "La tarea con ID %id no le pertenece."

# ABM
tarea_addPageTitle = "Agregar Tarea"
tarea_editPageTitle = "Editar Tarea"

[estado]
estado_label = "Estado"
estado_optionAll = "-- todos los estados --"

[comentario]
comentario_label = "Comentario"
comentario_listadoPageTitle = "Comentarios"
comentario_sinComentarios = "No hay comentarios para esta tarea"
comentario_wysiwygTitle = "Agregar Comentario"
comentario_submitValue = "agregar comentario"

################################################################################
[date]
date_change = "cambiar fecha..."

[tipo]
tipo_text = "texto"
tipo_textarea = "texto"
tipo_richtext = "texto"
tipo_string = "texto"
tipo_integer = "n�mero"
tipo_float = "n�mero"
tipo_date = "fecha"
tipo_time = "hora"
tipo_datetime = "fecha y hora"
tipo_select = "opci�n"
tipo_radio = "opci�n"
tipo_checkbox = "valor"
tipo_file = "archivo"
tipo_image = "imagen"
tipo_sound = "audio"
tipo_flash = "flash"

# Checkbox
tipo_checkboxChecked = "seleccionado"
tipo_checkboxUnchecked = "no seleccionado"

# Default
tipo_defaultImage = "sin imagen"
tipo_defaultFlash = "sin archivo flash"
tipo_defaultSound = "sin audio"
tipo_defaultFile = "sin archivo"
tipo_default = "sin contenido"

[log]
log_descripcion = "descripci�n"
log_fecha = "fecha"
log_listadoPageTitle = "Listado de Logs"
