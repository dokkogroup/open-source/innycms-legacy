<?php
/**
 * Project: Inny Tracker
 * File: configuracion.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
################################################################################
function updateConfiguraciones(&$smarty){

    # Updateo las configuraciones
    foreach($_POST as $nombre => $valor){
        $daoConfiguracion = DB_DataObject::factory('configuracion');
        $daoConfiguracion->nombre = $nombre;
        $daoConfiguracion->find(true);
        $daoConfiguracion->valor = $valor;
        $daoConfiguracion->update();
    }

    # Me redirijo a la secci�n de Configuraciones
    Denko::redirect(isset($_SERVER['HTTP_REFERER']) ? basename($_SERVER['HTTP_REFERER']) : 'configuracion.php');
}
################################################################################
# Verifico que el usuario logueado sea administrador
Inny_Tracker::verificarAdminLogueado();

# ================================= AJAX ===================================== #

# Actualiza la metadata de todos los sitios
if(!empty($_GET['ajax']) && $_GET['ajax'] == 'actualizarMetadataDeTodosLosSitios'){
    $messages = InnyCore_Metadata::actualizarTodosLosSitios(isset($_GET['incluir_deshabilitados']) && $_GET['incluir_deshabilitados'] == 'true' ? null : 1);
    echo json_encode($messages);
    exit(0);
}

# ================================ NORMAL ==================================== #

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Si hay variables en POST, las valido y asigno a las configuraciones
if(count($_POST) > 0){
    updateConfiguraciones($smarty);
}

# En caso que no est� seteado el par�metro de filtro de tipo de configuraciones,
# seteo por default que muestre las configuraciones de Sistema
$_GET['dkf_lc_btc'] = !empty($_GET['dkf_lc_btc']) ? $_GET['dkf_lc_btc'] : 1;

# Muestro el template
$smarty->display('configuracion.tpl');
################################################################################