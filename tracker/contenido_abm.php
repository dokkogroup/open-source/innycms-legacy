<?php
/**
 * Project: Inny Tracker
 * File: contenido_abm.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
include_once 'common.php';
################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('view','delete','multidelete'));

# Obtengo los backlinks al listado de contenidos
$linkBackContenidos = new Inny_LinkBackContenidos();
$linkBack = $linkBackContenidos->getLinkBack();

# En caso de eliminar m�ltiples contenidos
$action = InnyCore_Utils::getParamValue('action');
if($action == 'multidelete'){
    foreach($_GET['id'] as $id){
        $contenido = new Contenido($id);
        $contenido->delete();
    }
    Denko::redirect($linkBack);
}

# Verifico que el contenido exista en la DB
else{
    $id_configuracion = InnyCore_Utils::getParamValue('id_configuracion');
    $daoConfiguracion = Inny_Common::verificarDao($smarty,'configuracion',$id_configuracion);
    $contenido = new Contenido($id_configuracion);

    # Si hay que eliminar el contenido
    if($action == 'delete'){
        $contenido->delete();
        Denko::redirect($linkBack);
    }
}

# Asigno las variables al template
$smarty->assign('contenido',$contenido);
$smarty->assign('linkBack',$linkBack);

# Muestro el template
$smarty->display('contenido_view.tpl');

################################################################################