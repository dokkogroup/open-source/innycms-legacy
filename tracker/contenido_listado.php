<?php
/**
 * Project: Inny Tracker
 * File: contenido_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
include_once 'common.php';
################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Verifico si me tengo que redirigir a la url cacheada del listado
$linkBackContenidos = new Inny_LinkBackContenidos();
$linkBackContenidos->verifyRedirect();

# Verifico que el sitio exista
$id_sitio = !empty($_GET['id_sitio']) ? $_GET['id_sitio'] : null;
$daoSitio = Inny_Common::verificarDao($smarty,'sitio',$id_sitio);
$daoSitio->fetch();

# Guardo en session la URL con el querystring
$linkBackContenidos->setLinkBack();

# Seteo por default que los contenidos est�n ordenados alfabeticamente
$_GET['dko_lcfg_o_bn'] = !empty($_GET['dko_lcfg_o_bn']) ? $_GET['dko_lcfg_o_bn'] : 'a';

# Asigno el DAO del sitio y el link de volver al listado de sitios
$linkBackSitios = new Inny_LinkBackSitios();
$smarty->assign('linkBack',$linkBackSitios->getLinkBack());
$smarty->assign('daoSitio',$daoSitio);

# Muestro el template
$smarty->display('contenido_listado.tpl');