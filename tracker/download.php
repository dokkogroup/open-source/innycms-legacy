<?php
/**
 * Inny Base Project
 *
 * File: download.php
 * Purpose: script PHP para descarga de archivos
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
$INNY_START_SESSION = true;
require_once 'common.php';
################################################################################
if(!empty($_GET['id_temporal'])){

    $id_temporal = $_GET['id_temporal'];
    InnyCore_File::chequearTemporal($id_temporal);
    $stream = DFM::get($id_temporal);

    # Obtengo el mime y nombre del archivo
    $daoTemporal = DB_DataObject::factory('temporal');
    $daoTemporal->selectAdd();
    $daoTemporal->selectAdd('id_temporal,name,metadata,aud_ins_date');
    $daoTemporal->id_temporal = $id_temporal;
    $daoTemporal->find(true);
    $filename = $daoTemporal->name;
    $metadata = json_decode($daoTemporal->metadata,true);
    $mime = $metadata['mime'];

    # Entrego el contenido
    require_once 'HTTP/Download.php';
    $dl = &new HTTP_Download();
    $dl->setData($stream);
    $dl->setContentDisposition(HTTP_DOWNLOAD_ATTACHMENT,$filename);
    $dl->setBufferSize(1024*80); // 100 K
    $dl->setThrottleDelay(1);    // 1 sec
    $dl->setContentType($mime);

    # En caso que haya que enviar el contenido todo de una (por ejemplo, para flash)
    if(!empty($_GET['mode']) && $_GET['mode'] == 'inline'){
        $dl->setContentDisposition('inline');
    }

    # Control de cache
    $dl->setCache('true');
    $dl->headers['Cache-Control'] = 'public';

    # Entrego el contenido y termino y finaliza el script
    $dl->send();
    exit(0);
}
################################################################################