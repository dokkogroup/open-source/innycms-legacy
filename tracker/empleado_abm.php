<?php
/**
 * Project: Inny Tracker
 * File: empleado_abm.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
###############################################################################
function validarEmpleadoPassword(&$smarty,&$daoEmpleado){

    #
    Inny_Tracker::verificarCambiarPassword($smarty,(Inny_Tracker::getTipoUsuario() == USUARIO_EMPLEADO ? $daoEmpleado->password : null));

    # En caso que est� todo OK
    if(!Denko::hasErrorMessages()){
        $daoEmpleado->password = $_POST['newpassword'];
        $daoEmpleado->update();
        Denko::redirect('empleado_abm.php?action=view'.(Inny_Tracker::getTipoUsuario() == USUARIO_EMPLEADO ? '' : '&id_empleado='.$_POST['id_empleado']).'&change=ok');
    }

    return false;
}
################################################################################
function validateEmpleadoData(&$smarty,&$daoEmpleado){

    # Cargo los mensajes de error
    $smarty->config_load('speech.conf','error');
	$smarty->config_load('speech.conf','empleado');

    # Verifico que el campo 'Usuario' tenga valor seteado
    $username = !empty($_POST['username']) ? trim($_POST['username']) : '';
    if($username == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'empleado_username'));
    }

    # Verifico que el campo 'Contrase�a' tenga valor seteado
    $password = !empty($_POST['password']) ? $_POST['password'] : '';
    if($password == '' && $_POST['action'] != 'edit'){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'empleado_password'));
    }

    # Verifico que el campo 'Apellido' tenga valor seteado
    $apellido = !empty($_POST['apellido']) ? trim($_POST['apellido']) : '';
	if($apellido == ''){
		Denko::addErrorMessage('error_requiredField',array('%field' => 'empleado_apellido'));
	}

	# Verifico que el campo 'Nombre' tenga valor seteado
    $nombre = !empty($_POST['nombre']) ? trim($_POST['nombre']) : '';
	if($nombre == ''){
		Denko::addErrorMessage('error_requiredField',array('%field' => 'empleado_nombre'));
	}

    # Verifico que el campo 'E-mail' tenga valor seteado
    $email = !empty($_POST['email']) ? trim($_POST['email']) : '';
    if($email == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'empleado_email'));
    }

    # Verifico que el valor en el campo 'E-mail' sea v�lido
    elseif(!Denko::hasEmailFormat($email)){
        Denko::addErrorMessage('error_invalidField',array('%field' => 'empleado_email'));
    }

    # Verifico que el campo 'Tel�fono' tenga valor seteado
    $telefono = !empty($_POST['telefono']) ? trim($_POST['telefono']) : '';
    if($telefono == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'empleado_telefono'));
    }

    # En caso que no haya errores
    if(!Denko::hasErrorMessages()){
        $action   = $_POST['action'];
        $id_empleado = $_POST['id_empleado'];
        $username = strtolower($username);

        # Verifico que si cambi� el username, no exista en la DB
        if($action == 'edit' && $username != $daoEmpleado->username){
            $daoOtroEmpleado = DB_DataObject::factory('empleado');
            $daoOtroEmpleado->whereAdd('username = \''.strtolower($username).'\' and id_empleado != '.$daoEmpleado->id_empleado);
            if($daoOtroEmpleado->find()){
                $smarty->config_load('speech.conf','empleado_abm');
                Denko::addErrorMessage('empleadoAbm_usernameError',array('%username' => $username));
                return false;
            }
        }

        #
        $daoEmpleado->username = $username;
        if($_POST['action'] != 'edit'){
        	$daoEmpleado->password = $password;
        }
        $daoEmpleado->nombre = $nombre;
        $daoEmpleado->apellido = $apellido;
        $daoEmpleado->email = $email;
        $daoEmpleado->telefono = $telefono;
        $daoEmpleado->habilitado = !empty($_POST['habilitado']) ? '1' : '0';

        #
        if($action == 'edit'){
            $daoEmpleado->update();
        }
        else{
            $daoEmpleado->insert();
        }

        #
        Denko::redirect('empleado_abm.php?action=view&id_empleado='.$daoEmpleado->id_empleado);
    }
}

################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

$linkBackEmpleados = new Inny_LinkBackEmpleados();
$linkBack = $linkBackEmpleados->getLinkBack();

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('view','insert','edit','changepass','enable','disable','multi_enable','multi_disable'));

# En caso que no se deseen var los datos del empleado, verifico que el usuario
# logueado sea el administrador
$action = InnyCore_Utils::getParamValue('action');
if($action != 'view' && $action != 'changepass'){
    Inny_Tracker::verificarAdminLogueado();
}

# DAO del Empleado
$daoEmpleado = DB_DataObject::factory('empleado');

# En caso que la acci�n sea distinta a 'insert', verifico que el DAO sea v�lido
if($action != 'insert'){

    # En caso que haya que habilitar/deshabilitar el Empleado
    if(!empty($_GET['id']) && is_array($_GET['id']) && count($_GET['id']) > 0 && ($action == 'enable' || $action == 'disable')){
        foreach($_GET['id'] as $id_empleado){
            $daoEmpleado = DB_DataObject::factory('empleado');
            if(!empty($id_empleado) && $daoEmpleado->get($id_empleado)){
                $daoEmpleado->habilitado = ($action == 'enable') ? '1' : '0';
                $daoEmpleado->update();
            }
        }
        Denko::redirect($linkBack);
    }

    # Obtengo el ID del empleado
    $id_empleado = InnyCore_Utils::getParamValue('id_empleado');
    if(Inny_Tracker::getTipoUsuario() == USUARIO_EMPLEADO){
        # Verifico que el empleado no quiera cambiar su password (solo el admin puede)
        if($action == 'changepass') Inny_Tracker::logout();

        # En caso que no se exista el par�metro 'id_empleado', asumo el que est�
        # logueado
        $id_empleadoLogueado = Inny_Tracker::getIdEmpleadoLogueado();
        if($id_empleado == null){
            $id_empleado = $id_empleadoLogueado;
        }
    }

    # Verifico que el ID del empleado sea v�lido y exista en la DB
    $daoEmpleado = Inny_Common::verificarDao($smarty,'empleado',$id_empleado);
    $daoEmpleado->fetch();

    # En caso que haya que habilitar/deshabilitar el Empleado
    if($action == 'enable' || $action == 'disable'){
        $daoEmpleado->habilitado = ($action == 'enable') ? '1' : '0';
        $daoEmpleado->update();
        Denko::redirect($linkBack);
    }
}

# Verifico que haya variables en POST
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    # En caso de editar el pass de un Empleado
    if($action == 'changepass'){
        validarEmpleadoPassword($smarty,$daoEmpleado);
    }

    # En caso de agregar/editar un Empleado,
    else{
        validateEmpleadoData($smarty,$daoEmpleado);
    }
}

# En caso
if(!empty($_GET['change']) && $_GET['change'] == 'ok'){
    $smarty->config_load('speech.conf','password');
    Denko::addOkMessage('password_okChange');
}

# Asigno el DAO del empleado y el link de volver al listado de empleados
$smarty->assign('linkBack',$linkBack);
$smarty->assign('daoEmpleado',$daoEmpleado);

# Muestro el template
$template = 'view';
switch($action){

    # En caso de mostrar el template de ver datos del empleado
    case 'view': $template = 'empleado_view.tpl'; break;

    # En caso de mostrar el template de cambio de contrase�a
    case 'changepass': $template = 'empleado_changepass.tpl'; break;

    # En caso de mostrar el template de edici�n
    default: $template = 'empleado_abm.tpl'; break;
}
$smarty->display($template);

################################################################################
