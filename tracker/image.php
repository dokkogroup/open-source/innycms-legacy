<?php
/**
 * Inny Base Project
 *
 * File: image.php
 * Purpose: script PHP para mostrar im�genes alojadas en la DB
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
$INNY_START_SESSION = true;
require_once 'common.php';
################################################################################
if(!empty($_GET['id_temporal'])){

    $id_temporal = $_GET['id_temporal'];
    InnyCore_File::chequearTemporal($id_temporal);
    $stream = DFM::get($id_temporal);

    # Obtengo el mime y nombre del archivo
    $daoTemporal = DB_DataObject::factory('temporal');
    $daoTemporal->selectAdd();
    $daoTemporal->selectAdd('id_temporal,name,metadata,aud_ins_date');
    $daoTemporal->id_temporal = $id_temporal;
    $daoTemporal->find(true);
    $filename = $daoTemporal->name;
    $metadata = json_decode($daoTemporal->metadata,true);
    $mime = $metadata['mime'];

    # En caso que haya que redimensionar la imagen:
    if(!empty($_GET['type']) && $_GET['type'] == 'thumb'){
        $stream = Denko::createImage($stream,($_GET['width']!=='null'?$_GET['width']:null),($_GET['height']!=='null'?$_GET['height']:null),$_GET['quality'],$mime);
    }

    # Preparo para entregar la imagen
    require_once 'HTTP/Download.php';
    $dl = &new HTTP_Download();
    $dl->setData($stream);
    $dl->setContentDisposition(HTTP_DOWNLOAD_INLINE,$filename);
    $dl->setBufferSize(1024*80); // 100 K
    $dl->setThrottleDelay(1);    // 1 sec
    $dl->setContentType($mime);

    # Control de cache
    $dl->setCache('true');
    $dl->headers['Cache-Control'] = 'public';

    # Entrego el contenido y termino y finaliza el script
    $dl->send();
    exit(0);
}
################################################################################
