<?php
require_once 'common.php';
require_once '../commons/dklogin.php';

$APPNAME=Denko::getConfig('dokko_login_app');
if($APPNAME==''){
	echo "Error: no se encuentra configurada el DokkoLogin APP NAME";
	exit;
}

if(!empty($_GET['k'])){
	$login=dokkoLogin($APPNAME);
	$tipo=$login['access_level'];
	if($tipo=='empleado') {
		$e = DB_DataObject::factory('empleado');
        $e->username=strtolower($login['username']);
		if(!$e->find(true)){
			$e = DB_DataObject::factory('empleado');
			$e->username=strtolower($login['username']);
			$e->insert();
		}
		$e->password = md5(rand(0,321031293012))."\1\0\1\0";
		$e->nombre = $login['name'];
		$e->apellido = $login['lastname'];
		$e->email = $login['email'];
		$e->habilitado = '1';
		$e->update();
		Inny_Tracker::setTipoUsuario(USUARIO_EMPLEADO);
		Inny_Tracker::setIdEmpleadoLogueado($e->id_empleado);
	} else if($tipo=='admin') {
        Inny_Tracker::setTipoUsuario(USUARIO_ADMIN);
	}
	Denko::redirect(Inny_Tracker::$scriptDefault);
	exit;
}

$url=explode('?',"http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
$url=urlencode($url[0]);

if(isset($_GET['logout'])){
	Denko::sessionDestroy();
	dklLoginRedirect('ask',$APPNAME.'&url='.$url);
	exit;
}

dklLoginRedirect('c',$APPNAME.'&url='.$url);
