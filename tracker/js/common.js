/**
 * Retorna si es v�lido el contenido del wysiwyg
 */
function validateWysiwygContent(WysiwygName){
    var contenido      = document.getElementById('wysiwyg'+WysiwygName).contentWindow.document.body.innerHTML;
    var cleanContenido = contenido.replace(/<([^ ]+)>/g,'').replace(/\s<([^ ]+)>/g,'').replace(/\s+/g,'').replace(/&nbsp;/g,'');
    return (cleanContenido.length > 0);
}

function showSelect(div_link,id_div,id_select){
    document.getElementById(div_link).style.display = 'none';
    document.getElementById(id_select).disabled = false;
    document.getElementById(id_div).style.display = 'block';
}