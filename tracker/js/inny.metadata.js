/**
 *
 *
 */
var Inny_Metadata = {

    /**
     * Vac�a un elemento HTML
     */
    _vaciarElemento : function(e){
        while(e.hasChildNodes()){
            e.removeChild(e.firstChild);
        }
    },

    /**
     * Actualiza la metadata de todos los sitios
     */
    actualizarTodosLosSitios : function(){
        var div = document.getElementById('updateMetadataLogs');
        div.style.display = 'none';
        this._vaciarElemento(div);

        var url = "configuracion.php?ajax=actualizarMetadataDeTodosLosSitios&incluir_deshabilitados=" + (document.getElementById('incluir_deshabilitados').checked ? 'true' : 'false');
        var xmlhttp = initAjax();
        xmlhttp.open("GET",url,true);
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){

                    // Obtengo el JSON
                    eval('var xmlhttp_response = '+xmlhttp.responseText+';');

                    // Muestro el DIV
                    div.style.display = 'block';

                    for(var id in xmlhttp_response){

                        // Creo el listado
                        var list = document.createElement('UL');

                        // Attacheo el nombre del sitio
                        var list_li_nombre = document.createElement('LI');
                        list_li_nombre.appendChild(document.createTextNode('Nombre: '));
                        var bolded = document.createElement('B');
                        bolded.appendChild(document.createTextNode(xmlhttp_response[id].nombre));
                        list_li_nombre.appendChild(bolded);
                        list.appendChild(list_li_nombre);

                        // Attacheo la URL del sitio
                        var list_li_url = document.createElement('LI');
                        list_li_url.appendChild(document.createTextNode('URL: '+xmlhttp_response[id].url));
                        list.appendChild(list_li_url);

                        // Attacheo los errores y las alertas
                        if(xmlhttp_response[id].messages == 'none'){
                            list_li_ew = document.createElement('LI');
                            list_li_ew.className = 'ok';
                            list_li_ew.appendChild(document.createTextNode('Errores y alertas: ninguno.'));
                            list.appendChild(list_li_ew);
                        }
                        else{

                            // Agrego los mensajes de alerta
                            if(xmlhttp_response[id].messages['warnings'] != null){
                                var list_li_w = document.createElement('LI');
                                list.appendChild(list_li_w);
                                list_li_w.appendChild(document.createTextNode('Alertas:'));
                                var sublist_warnings = document.createElement('UL');
                                list_li_w.appendChild(sublist_warnings);
                                for(var i = 0; i < xmlhttp_response[id].messages.warnings.length; i++){
                                    var sublist_li = document.createElement('LI');
                                    sublist_li.className = 'warning';
                                    sublist_li.appendChild(document.createTextNode(xmlhttp_response[id].messages.warnings[i]));
                                    sublist_warnings.appendChild(sublist_li);
                                }
                            }

                            // Agrego los mensajes de error
                            if(xmlhttp_response[id].messages['errors'] != null){
                                var list_li_e = document.createElement('LI');
                                list.appendChild(list_li_e);
                                list_li_e.appendChild(document.createTextNode('Errores:'));
                                var sublist_errors = document.createElement('UL');
                                list_li_e.appendChild(sublist_errors);
                                for(var i = 0; i < xmlhttp_response[id].messages.errors.length; i++){
                                    var sublist_li = document.createElement('LI');
                                    sublist_li.className = 'error';
                                    sublist_li.appendChild(document.createTextNode(xmlhttp_response[id].messages.errors[i]));
                                    sublist_errors.appendChild(sublist_li);
                                }
                            }
                        }

                        // Adjunto el listado al DIV que muestra los mensajes
                        div.appendChild(list);
                    }
                }

                // En caso de error de conexi�n
                else{

                }
            }
        }
        xmlhttp.send(null);
    }
};