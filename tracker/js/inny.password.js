var Inny_Password = {

    init : function(){
        this.password = document.getElementById('password');
		this.newpassword = document.getElementById('newpassword');
		this.confirmnewpass = document.getElementById('confirmnewpass');

        // Focus
		if(this.password){
            this.password.focus();
        }
        else{
            this.newpassword.focus();
        }

    },

    validate : function(){

        // Verifico que el campo 'Contrase�a' sea distinto de vac�o
        if(this.password){
            var password = this.password.value;
            if(password.length == 0){
                alert("Error: el campo 'Contrase�a' es requerido.");
                return false;
            }
        }

        // Verifico que el campo 'Nueva Contrase�a' sea distinto de vac�o
        var newpassword = this.newpassword.value;
        if(newpassword.length == 0){
            alert("Error: el campo 'Nueva Contrase�a' es requerido.");
            return false;
        }

        // Verifico que el campo 'Confirmar Nueva Contrase�a' sea distinto de vac�o
        var confirmnewpass = this.confirmnewpass.value;
        if(confirmnewpass.length == 0){
            alert("Error: el campo 'Confirmar Nueva Contrase�a' es requerido.");
            return false;
        }

        // Verifico que los campos 'Nueva Contrase�a' y 'Confirmar Nueva Contrase�a' sean iguales
        if(newpassword != confirmnewpass){
            alert("Error: los campos 'Nueva Contrase�a' y 'Confirmar Nueva Contrase�a' deben ser iguales.");
            return false;
        }

        // En caso que est� todo OK
        return true;
    }
};