var Inny_Sitio = {

    /**
     * Inicializa
     */
    init : function(){
        this.nombre_sitio = document.getElementById('nombre_sitio');
        this.nombre_encargado = document.getElementById('nombre_encargado');
        this.apellido = document.getElementById('apellido');
        this.username = document.getElementById('username');
        this.password = document.getElementById('password');
        this.telefono = document.getElementById('telefono');
        this.direccion = document.getElementById('direccion');
        this.email = document.getElementById('email');
        this.cvs = document.getElementById('cvs');
        this.email_contacto = document.getElementById('email_contacto');

        this.nombre_sitio.focus();
        this.nombre_sitio.select();
    },

    /**
     * Verifica que los datos del formulario sean v�lidos
     *
     */
    validate : function(){

        // Verifico que el campo 'Nombre del Sitio' tenga valor seteado
        var nombre_sitio = this.nombre_sitio.value.trim();
        if(nombre_sitio.length == 0){
            alert("Error: el campo 'Nombre del Sitio' es requerido.");
            return false;
        }

        // Verifico que el campo 'Nombre del Propietario' tenga valor seteado
        var nombre_encargado = this.nombre_encargado.value.trim();
        if(nombre_encargado.length == 0){
            alert("Error: el campo 'Nombre del Propietario' es requerido.");
            return false;
        }

        // Verifico que el campo 'Apellido del Propietario' tenga valor seteado
        var apellido = this.apellido.value.trim();
        if(apellido.length == 0){
            alert("Error: el campo 'Apellido del Propietario' es requerido.");
            return false;
        }

        // Verifico que el campo 'Nombre de Usuario' tenga valor seteado
        var username = this.username.value.trim();
        if(username.length == 0){
            alert("Error: el campo 'Nombre de Usuario' es requerido.");
            return false;
        }

        // Verifico que el campo 'Contrase�a' tenga valor seteado
        var password = this.password.value.trim();
        if(password.length == 0){
            alert("Error: el campo 'Contrase�a' es requerido.");
            return false;
        }

        // Verifico que el campo 'Tel�fono' tenga valor seteado
        var telefono = this.telefono.value.trim();
        if(telefono.length == 0){
            alert("Error: el campo 'Tel�fono' es requerido.");
            return false;
        }

        // Verifico que el campo 'Direcci�n' tenga valor seteado
        var direccion = this.direccion.value.trim();
        if(direccion.length == 0){
            alert("Error: el campo 'Direcci�n' es requerido.");
            return false;
        }

        // Verifico que el campo 'E-mail', en caso de tener valor seteado, sea v�lido
        var email = this.email.value.trim();
        if(email.length > 0 && !hasEmailFormat(email)){
            alert("Error: el valor en el campo 'E-mail' no es v�lido.");
            return false;
        }

        // Verifico que el campo 'Path CVS' tenga valor seteado
        var cvs = this.cvs.value.trim();
        if(cvs.length == 0){
            alert("Error: el campo 'Path CVS' es requerido.");
            return false;
        }

        // Verifico que el campo 'Email de Contacto', en caso de tener valor seteado, sea v�lido
        var email_contacto = this.email_contacto.value.trim().split(',');
        for(i = 0; i < email_contacto.length; i++){
            var contacto = email_contacto[i].trim();
            if(contacto.length > 0 && !hasEmailFormat(contacto)){
                alert("Error: el valor en el campo 'Email de Contacto' no es v�lido.");
                return false;
            }
        }

        // En caso que est� todo OK
        return true;
    }

};
