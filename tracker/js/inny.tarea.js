var Inny_Tarea = {


    /**
     *
     */
    init : function(){
        this.form = document.getElementById('tarea_abm');
        this.id_sitio = document.getElementById('id_sitio');
        this.id_estado = document.getElementById('id_estado');
        this.id_empleado = document.getElementById('id_empleado');
        this.monto = document.getElementById('monto');
    },

    /**
     *
     */
    validate : function(){

        // Verifico que el valor del campo "Sitio" sea v�lido
        var id_sitio = this.id_sitio.options[this.id_sitio.selectedIndex].value;
        if(id_sitio == 'null'){
            alert("Error: el campo 'Sitio' es requerido.");
            return false;
        }

        // Verifico que el campo 'Monto' sea v�lido
        var monto = this.monto.value.trim();
        if(isNaN(monto)){
            alert("Error: el valor en el campo 'Monto' no es v�lido.");
            return false;
        }

        return true;
    }

};