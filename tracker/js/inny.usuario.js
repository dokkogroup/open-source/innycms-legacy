/**
 *
 */
var Inny_Usuario = {

    /**
     * Inicializa
     */
    init : function(){

        // Obtengo los elementos que luego validar�
        this.username = document.getElementById('username');
        this.password = document.getElementById('password');
        this.id_sitio = document.getElementById('id_sitio');
        this.apellido = document.getElementById('apellido');
        this.nombre = document.getElementById('nombre');
        this.email = document.getElementById('email');
        this.telefono = document.getElementById('telefono');

        // Focus
        this.username.focus();
        this.username.select();


    },

    /**
     * Verifica que los datos del formulario sean v�lidos
     */
    validate : function(){

        // Verifico que el campo 'Nombre de Usuario' tenga valor seteado
        var username = this.username.value.trim();
        if(username.length == 0){
            alert("Error: el campo 'Usuario' es requerido.");
            return false;
        }

        // Verifico que el campo 'Contrase�a' tenga valor seteado
        if(this.password){
            var password = this.password.value.trim();
            if(password.length == 0){
                alert("Error: el campo 'Contrase�a' es requerido.");
                return false;
            }
        }


        // Verifico que el campo 'Sitio' tenga valor seteado
        var id_sitio = this.id_sitio.options[this.id_sitio.selectedIndex].value;
        if(id_sitio == 'null'){
            alert("Error: el campo 'Sitio' es requerido.");
            return false;
        }

        // Verifico que el campo 'Apellido del Propietario' tenga valor seteado
        var apellido = this.apellido.value.trim();
        if(apellido.length == 0){
            alert("Error: el campo 'Apellido del Propietario' es requerido.");
            return false;
        }

        // Verifico que el campo 'Nombre' tenga valor seteado
        var nombre = this.nombre.value.trim();
        if(nombre.length == 0){
            alert("Error: el campo 'Nombre' es requerido.");
            return false;
        }

        // Verifico que el campo 'E-mail' tenga valor seteado
        var email = this.email.value.trim();
        if(email.length == 0){
            alert("Error: el campo 'E-mail' es requerido.");
            return false;
        }

        // Verifico que el campo 'E-mail' tenga formato v�lido
        else if(!hasEmailFormat(email)){
            alert("Error: el valor en el campo 'E-mail' no es v�lido.");
            return false;
        }

        // Verifico que el campo 'Tel�fono' tenga valor seteado
        var telefono = this.telefono.value.trim();
        if(telefono.length == 0){
            alert("Error: el campo 'Tel�fono' es requerido.");
            return false;
        }

        // En caso que est� todo OK
        return true;
    }

};
