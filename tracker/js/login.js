function validateLogin(){
    var username = document.login.username.value.trim();
    var password = document.login.password.value;

    if(username.length == 0){
        alert("Error: el campo 'Usuario' es requerido.");
        return false;
    }
    if(password.length == 0){
        alert("Error: el campo 'Contraseņa' es requerido.");
        return false;
    }
    return true;
}