<?php
/**
 * Project: Inny Tracker
 * File: listado_abm.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
include_once 'common.php';
################################################################################
function amListado(&$smarty,&$daoListado){

    # Cargo los speechs para los mensajes de error
    $smarty->config_load('speech.conf','error');
    $smarty->config_load('speech.conf','listado');

    # Obtengo las variables en POST
    $action = $_POST['action'];
    $nombre = isset($_POST['nombre']) ? trim($_POST['nombre']) : '';

    # El campo nombre es requerido, por lo tanto verifico que no sea vac�o
    if($nombre == ''){
        Denko::addErrorMessage('error_requiredField',array('%field'=>'listado_nombre'));
    }

    # En caso que todo est� OK
    else{

        # Asigno el nombre al DAO, luego lo actualizo
        $daoListado->nombre = $nombre;
        $daoListado->update();
        Denko::redirect('listado_listado.php?id_sitio='.$daoListado->id_sitio);
    }
}

################################################################################

# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

$linkBackListados = new Inny_LinkBackListados();
$linkBack = $linkBackListados->getLinkBack();

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('view','edit','delete','multidelete'));

# En caso de eliminar m�ltiples listados
$action = InnyCore_Utils::getParamValue('action');
if($action == 'multidelete'){
    foreach($_GET['id'] as $id){
        $daoListado = DB_DataObject::factory('listado');
        $daoListado->get($id);
        $daoListado->delete();
    }
    Denko::redirect($linkBack);
}

# Verifico que el listado exista en la DB
else{
    $id_listado = InnyCore_Utils::getParamValue('id_listado');
    $daoListado = Inny_Common::verificarDao($smarty,'listado',$id_listado);
    $daoListado->fetch();

    # Si hay que eliminar el listado
    if($action == 'delete'){
        $daoListado->delete();
        Denko::redirect($linkBack);
    }
}

exit(0);

# Asigno las variables al template
$smarty->assign('daoListado',$daoListado);

# Muestro el template
$smarty->display($action == 'view' ? 'listado_view.tpl' : 'listado_edit.tpl');

################################################################################