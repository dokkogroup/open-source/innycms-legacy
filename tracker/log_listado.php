<?php
/**
 * Project: Inny Tracker
 * File: log_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
include_once 'common.php';
################################################################################
if(!empty($_GET['action'])){
    switch($_GET['action']){

        # En caso que haya que eliminar logs
        case 'delete';

            # En caso que sea eliminar solo un log
            if(!empty($_GET['id_log'])){
                $daoLog = InnyCore_Dao::getDao('log',$_GET['id_log']);
                if(!empty($daoLog)){
                    $daoLog->delete();
                }
            }

            # En caso que haya que eliminar de forma masiva
            elseif(!empty($_GET['id'])){
                $daoLog = DB_DataObject::factory('log');
                $daoLog->whereAdd('id_log in ('.implode(',',$_GET['id']).')');
                if($daoLog->find()){
                    while($daoLog->fetch()){
                        $daoLog->delete();
                    }
                }
            }

            # Me redirijo al listado
            Denko::redirect(!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'log_listado.php');

        # En cualquier otro caso
        default: break;
    }
}
################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Obtengo los sitios para el select
$daoSitiosFiltro = DB_DataObject::factory('sitio');
$daoSitiosFiltro->orderBy('nombre_sitio');
if($daoSitiosFiltro->find()){
    $filtro_sitios = array();
    while($daoSitiosFiltro->fetch()){
        $filtro_sitios[] = $daoSitiosFiltro->id_sitio.':'.$daoSitiosFiltro->nombre_sitio;
    }
    $smarty->assign('filtro_sitios',implode('|',$filtro_sitios));
}
unset($daoSitiosFiltro);

# Muestro el template
$smarty->display('log_listado.tpl');