<?php
/**
 * Project: Inny Tracker
 * File: index.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
################################################################################
function validateLogin(&$smarty){

    # Levanto los mensajes de error
    $smarty->config_load('speech.conf','error');
    $smarty->config_load('speech.conf','login');

    # Verifico que el nombre de usuario est� seteado
    $username = strtolower(trim($_POST['username']));
    if($username == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'login_username'));
    }

    # Verifico que la contrase�a est� seteada
    $password = $_POST['password'];
    if($password == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'login_password'));
    }

    # En caso que no haya errores, verifico que los datos ingresados sean v�lidos
    if(!Denko::hasErrorMessages()){

        # En caso que el usuario sea el administrador
        $dokko_admin_user = strtolower(getConfig('dokko_admin_user',null,null,false));
        if($username == $dokko_admin_user){

            # Verifico que la contrase�a sea v�lida
            $dokko_admin_pass = getConfig('dokko_admin_pass',null,null,false);
            if($password != $dokko_admin_pass){
                Denko::addErrorMessage('login_errorUserOrPass');
            }
        }

        # En caso que el usuario sea un desarrollador
        else{

            # Verifico que el usuario est� en la DB
            $daoEmpleado = DB_DataObject::factory('empleado');
            $daoEmpleado->whereAdd('username = \''.$username.'\' and habilitado = \'1\'');

            # En caso que el usuario NO exista o no est� habilitado, lanzo un error
            if(!$daoEmpleado->find(true)){
                Denko::addErrorMessage('login_errorUserOrPass');
            }

            # En caso que el password sea incorrecto
            elseif($daoEmpleado->password != $password){
                Denko::addErrorMessage('login_errorUserOrPass');
            }
        }
    }

    # En caso que no haya errores
    if(!Denko::hasErrorMessages()){
        Inny_Tracker::setTipoUsuario(($username == $dokko_admin_user) ? USUARIO_ADMIN : USUARIO_EMPLEADO);
        if(Inny_Tracker::getTipoUsuario() == USUARIO_EMPLEADO){
            Inny_Tracker::setIdEmpleadoLogueado($daoEmpleado->id_empleado);
        }
        Denko::redirect(Inny_Tracker::$scriptDefault);
    }

    #
    return false;
}
################################################################################
# Logout
if(isset($_GET['logout'])){
    Denko::sessionDestroy();
    Denko::sessionStart();
}

# En caso que ya est� logueado, me redirijo al script por default
if(Inny_Tracker::hayUsuarioLogueado()){
    Denko::redirect(Inny_Tracker::$scriptDefault);
}

# Si hay variables en POST, verifico que sean validas para el login
$smarty = new Smarty();
if(isset($_POST['username']) && isset($_POST['password'])){
    validateLogin($smarty);
}

# Muestro el template
$smarty->display('login.tpl');
