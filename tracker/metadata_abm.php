<?php
/**
 * Project: Inny Tracker
 * File: listado_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
include_once 'common.php';
################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

/*
# Verifico si me tengo que redirigir a la url cacheada del listado
$linkBackListados = new Inny_LinkBackListados();
$linkBackListados->verifyRedirect();*/

# Verifico que el sitio exista
$id_sitio = InnyCore_Utils::getParamValue('id_sitio');;
$daoSitio = Inny_Common::verificarDao($smarty,'sitio',$id_sitio);
$daoSitio->fetch();

if (!empty($_POST)){
    $daoSitio->dynamic_metadata=$_POST['dynamic_metadata'];
    $daoSitio->update();
    InnyCMS_Speech::config_load($smarty,'sitio');
    Denko::addOkMessage('sitio_dynamicMetadataEditOk');
}
    
/*
# Guardo en session la URL con el querystring
$linkBackListados->setLinkBack();
*/
# Asigno el DAO del sitio y el link de volver al listado de sitios
$linkBackSitios = new Inny_LinkBackSitios();
$smarty->assign('linkBack',$linkBackSitios->getLinkBack());
$smarty->assign('daoSitio',$daoSitio);

# Muestro el template
$smarty->display('metadata_abm.tpl');