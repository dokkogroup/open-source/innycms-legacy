<?php
/**
 * Project: Inny Tracker
 * File: sitio_abm.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
###############################################################################
function validateSitioData(&$smarty,&$daoSitio){

    # Cargo los mensajes de error
    $smarty->config_load('speech.conf','error');
	$smarty->config_load('speech.conf','sitio');

    # Verifico que el valor en el campo 'nombre_sitio' sea v�lido
    $nombre_sitio = !empty($_POST['nombre_sitio']) ? trim($_POST['nombre_sitio']) : '';
    if($nombre_sitio == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_nombreSitio'));
    }

    # Verifico que el valor en el campo 'nombre_encargado' sea v�lido
    $nombre_encargado = !empty($_POST['nombre_encargado']) ? trim($_POST['nombre_encargado']) : '';
	if($nombre_encargado == ''){
		Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_encargadoNombre'));
	}

    # Verifico que el valor en el campo 'apellido' sea v�lido
    $apellido = !empty($_POST['apellido']) ? trim($_POST['apellido']) : '';
	if($apellido == ''){
		Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_encargadoApellido'));
	}

    # Verifico que el valor en el campo 'username' sea v�lido
    $username = !empty($_POST['username']) ? trim($_POST['username']) : '';
    if($username == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_username'));
    }

    # Verifico que el valor en el campo 'password' sea v�lido
    $password = !empty($_POST['password']) ? $_POST['password'] : '';
    if($password == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_password'));
    }

    # Verifico que el valor en el campo 'telefono' sea v�lido
    $telefono = !empty($_POST['telefono']) ? trim($_POST['telefono']) : '';
    if($telefono == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_telefono'));
    }

    # Verifico que el valor en el campo 'direccion' sea v�lido
    $direccion = !empty($_POST['direccion']) ? trim($_POST['direccion']) : '';
    if($direccion == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_direccion'));
    }

    # Verifico que el valor en el campo 'email' sea v�lido, en caso de estar seteado
    $email = !empty($_POST['email']) ? trim($_POST['email']) : '';
    if($email != '' && !Denko::hasEmailFormat($email)){
        Denko::addErrorMessage('error_invalidField',array('%field' => 'sitio_email'));
    }

    # Verifico que el valor en el campo 'cvs' sea v�lido
    $cvs = !empty($_POST['cvs']) ? trim($_POST['cvs']) : '';
    if($cvs == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_pathCVS'));
    }

    # Verifico que el valor en el campo 'Email de Contacto' sea v�lido
    $email_contacto = explode(',',!empty($_POST['email_contacto']) ? trim($_POST['email_contacto']) : '');
    foreach($email_contacto as $contacto){
        $contacto=trim($contacto);
        if(!empty($contacto) && !Denko::hasEmailFormat($contacto)){
            Denko::addErrorMessage('error_invalidField',array('%field' => 'sitio_emailContacto'));
        }
    }

    # En caso que los campos tengan formato v�lido
    if(!Denko::hasErrorMessages()){
        $action   = $_POST['action'];
        $username = strtolower($username);

        # Verifico que si cambi� el username, no exista en la DB
        if($action == 'edit' && $username != $daoSitio->username){
            $id_sitio = $_POST['id_sitio'];
            $daoOtroSitio = DB_DataObject::factory('sitio');
            $daoOtroSitio->whereAdd('username = \''.strtolower($username).'\' and id_sitio != '.$daoSitio->id_sitio);
            if($daoOtroSitio->find()){
                $smarty->config_load('speech.conf','sitio_abm');
                Denko::addErrorMessage('sitioAbm_usernameError',array('%username' => $username));
                return false;
            }
        }

        # Datos del sitio
        $daoSitio->nombre_sitio = $nombre_sitio;
        $daoSitio->url = !empty($_POST['url_sitio']) ? trim($_POST['url_sitio']) : '';;
        $daoSitio->habilitado = !empty($_POST['habilitado']) ? '1' : '0';
        $daoSitio->crea_listados = !empty($_POST['crea_listados']) ? '1' : '0';
        $daoSitio->mantenimiento = !empty($_POST['mantenimiento']) ? '1' : '0';
        $daoSitio->log_errors = !empty($_POST['log_errors']) ? '1' : '0';

        $daoSitio->ga_codigo = !empty($_POST['ga_codigo']) ? $_POST['ga_codigo'] : 'null';
        $daoSitio->idioma_codigo = $_POST['idioma_codigo'];

        # Datos del propietario
        $daoSitio->nombre_encargado = $nombre_encargado;
        $daoSitio->apellido = $apellido;
        $daoSitio->username = $username;
        $daoSitio->password = $password;
        $daoSitio->telefono = $telefono;
        $daoSitio->direccion = $direccion;
        $daoSitio->email = $email;

        # FTP
        $daoSitio->ftp = !empty($_POST['ftp']) ? trim($_POST['ftp']) : '';
        $daoSitio->ftp_username = !empty($_POST['ftp_username']) ? trim($_POST['ftp_username']) : '';
        $daoSitio->ftp_password = !empty($_POST['ftp_password']) ? trim($_POST['ftp_password']) : '';
        $daoSitio->cvs = $cvs;

        # Agrego los datos del SMTP
        $daoSitio->setSmtpValue('contacto',trim($_POST['email_contacto'],' ,'));
        $daoSitio->setSmtpValue('host',trim($_POST['smtp_host']));
        $daoSitio->setSmtpValue('username',trim($_POST['smtp_username']));
        $daoSitio->setSmtpValue('password',trim($_POST['smtp_password']));
        $daoSitio->setSmtpValue('fromname',trim($_POST['smtp_fromname']));
        $daoSitio->setSmtpValue('protocol',trim($_POST['smtp_protocol']));
        $daoSitio->setSmtpValue('port',trim($_POST['smtp_port']));

        # En caso de agregar el Sitio
        if($action == 'insert'){
            $daoSitio->insert();
        }

        # En caso de editar el Sitio
        else{
            $daoSitio->update();
        }

        # Me redirijo al listado de Sitios
        Denko::redirect('sitio_abm.php?action=view&id_sitio='.$daoSitio->id_sitio);
    }
}

###############################################################################
function multiAction($action,&$daoSitio){
	switch($action){
       case 'enable':
    		$daoSitio->habilitado = '1';
        	$daoSitio->update();
        	break;

       case 'disable':
    		$daoSitio->habilitado = '0';
        	$daoSitio->update();
        	break;

       case 'crealistados_enable':
       		$daoSitio->crea_listados = '1';
        	$daoSitio->update();
        	break;

       case 'crealistados_disable':
       		$daoSitio->crea_listados = '0';
        	$daoSitio->update();
        	break;

       case 'maintenance':
            $daoSitio->mantenimiento = '1';
        	$daoSitio->update();
        	break;

       case 'no_maintenance':
            $daoSitio->mantenimiento = '0';
        	$daoSitio->update();
        	break;

        case 'logerrors_enable':
       		$daoSitio->log_errors = '1';
        	$daoSitio->update();
        	break;

       case 'logerrors_disable':
       		$daoSitio->log_errors = '0';
        	$daoSitio->update();
        	break;
	}
}
################################################################################

# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('view','insert','edit','crealistados_enable','crealistados_disable','enable','disable','maintenance','no_maintenance','logerrors_enable','logerrors_disable'));

# Obtengo el ID del sitio
$id_sitio = InnyCore_Utils::getParamValue('id_sitio');
$daoSitio = DB_DataObject::factory('sitio');
$linkBackSitios = new Inny_LinkBackSitios();
$linkBack = $linkBackSitios->getLinkBack();

$action = InnyCore_Utils::getParamValue('action');
switch($action){

    # Si la accion es 'insert' (o sea, agregar un Sitio), verifico que el usuario logueado sea Administrador
    case 'insert':
        Inny_Tracker::verificarAdminLogueado();
        break;

    case 'enable':
    case 'disable':
    case 'crealistados_enable':
    case 'crealistados_disable':
    case 'maintenance':
    case 'no_maintenance':
    case 'logerrors_enable':
    case 'logerrors_disable':
        if(!empty($_GET['id']) && count($_GET['id']) > 0){
            foreach($_GET['id'] as $id_sitio){
                $daoSitio = DB_DataObject::factory('sitio');
                $daoSitio = Inny_Common::verificarDao($smarty,'sitio',$id_sitio);
                $daoSitio->fetch();
                multiAction($action,$daoSitio);
            }
        }
        else{
            $daoSitio = Inny_Common::verificarDao($smarty,'sitio',$id_sitio);
            $daoSitio->fetch();
            multiAction($action,$daoSitio);
        }
        Denko::redirect($linkBack);
        break;

    case 'view':
    case 'edit':
        $daoSitio = Inny_Common::verificarDao($smarty,'sitio',$id_sitio);
        $daoSitio->fetch();
        break;
}

# En caso de agregar un nuevo sitio, verifico las variables en POST
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    validateSitioData($smarty,$daoSitio);
}

# Asigno el DAO del sitio y el link de volver al listado de sitios
$smarty->assign('linkBack',$linkBack);
$smarty->assign('daoSitio',$daoSitio);

# Muestro el template
$smarty->display($action == 'view' ? 'sitio_view.tpl' : 'sitio_abm.tpl');

################################################################################