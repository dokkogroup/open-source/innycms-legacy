<?php
/**
 * Project: Inny Tracker
 * File: sitio_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
include_once 'common.php';
################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Verifico si me tengo que redirigir a la url cacheada del listado
$linkBackSitios = new Inny_LinkBackSitios();
$linkBackSitios->verifyRedirect();

# Guardo en session la URL con el querystring
$linkBackSitios->setLinkBack();

# Seteo por default que los sitios est�n ordenados alfabeticamente
$_GET['dko_ls_o_bn'] = !empty($_GET['dko_ls_o_bn']) ? $_GET['dko_ls_o_bn'] : 'a';

# Muestro el template
$smarty->display('sitio_listado.tpl');