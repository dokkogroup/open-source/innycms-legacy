<?php
/**
 * Project: Inny Tracker
 * File: sw_sitio.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
require_once '../commons/inny.clientes.php';
require_once '../commons/dokko_configurator.php';
################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

$config = getConfig('url_server_clientes');
if(substr($config, 0, 4) != 'http'){
	$config = 'http://'.$config;
}
if(!empty($_GET['id_sitio'])){
	$daoSitio = DB_DataObject::factory('sitio');
	$daoSitio->get($_GET['id_sitio']);
	if($daoSitio){
	   $_SESSION[Inny_Clientes::$session_idSitio] = $daoSitio->id_sitio;
	   Inny_Clientes::setMostrarContenidosPrivados(true);
	   Inny::setPermisosTracker(true);
	   Denko::redirect($config);
	}
}
################################################################################
