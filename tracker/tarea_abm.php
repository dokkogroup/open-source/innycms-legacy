<?php
/**
 * Project: Inny Tracker
 * File: tarea_abm.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
################################################################################
function tareaAbmEmpleado(&$smarty,&$daoTarea){

    # Verifico que el valor del campo "Estado" sea v�lido
    if(isset($_POST['id_estado'])){
        $id_estado = !empty($_POST['id_estado']) ? $_POST['id_estado'] : null;
        if(empty($id_estado)){
            Denko::addErrorMessage('error_requiredField',array('%field' => 'estado_label'));
        }
        else{
            $daoEstado = DB_DataObject::factory('estado');
            if(!$daoEstado->get($id_estado)){
                Denko::addErrorMessage('error_invalidField',array('%field' => 'estado_label'));
            }
            else{
                $daoTarea->id_estado = $id_estado;
            }
        }
    }

    # Verifico que el valor del campo "Asignada a" sea v�lido
    if(isset($_POST['id_empleado'])){
        $id_empleado = !empty($_POST['id_empleado']) ? trim($_POST['id_empleado']) : null;
        if(empty($id_empleado)){
            Denko::addErrorMessage('error_requiredField',array('%field' => 'tarea_asignadaA'));
        }
        else{
            if($id_empleado != 'null'){
                $daoEmpleado = DB_DataObject::factory('empleado');
                if(!$daoEmpleado->get($id_empleado)){
                    Denko::addErrorMessage('error_invalidField',array('%field' => 'tarea_asignadaA'));
                }
                else{
                    $daoTarea->id_empleado = $id_empleado;
                }
            }
            else{
                $daoTarea->id_empleado = $id_empleado;
            }
        }
    }

    # Verifico que el valor del campo "Monto" sea v�lido
    if(isset($_POST['monto'])){
        if(!Denko::isInt($monto = trim($_POST['monto']))){
            Denko::addErrorMessage('error_invalidField',array('%field' => 'tarea_monto'));
        }
    }

    # En caso que no haya errores, actualizo los campos de la Tarea
    if(!Denko::hasErrorMessages()){

        # Actualizo los datos del DAO
        $daoTarea->update();

        # Actualizo el monto de la Tarea
        $daoTarea->updateMonto($monto);

        # Me redirijo al listado de tareas
        Denko::redirect('tarea_abm.php?action=view&id_tarea='.$daoTarea->id_tarea);
    }

    return false;
}
################################################################################
function tareaAbmAdmin($action,&$smarty,&$daoTarea){

    # Cargo el speech
    $smarty->config_load('speech.conf','error');
    $smarty->config_load('speech.conf','tarea');
    $smarty->config_load('speech.conf','sitio');
    $smarty->config_load('speech.conf','estado');

    # Verifico que el valor del campo "Sitio" sea v�lido
    $id_sitio = !empty($_POST['id_sitio']) ? $_POST['id_sitio'] : null;
    if(empty($id_sitio)){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_label'));
    }
    else{
        $daoSitio = DB_DataObject::factory('sitio');
        if(!$daoSitio->get($id_sitio)){
            Denko::addErrorMessage('error_invalidField',array('%field' => 'sitio_label'));
        }
    }

    # Verifico que el valor del campo "Estado" sea v�lido
    $id_estado = !empty($_POST['id_estado']) ? $_POST['id_estado'] : null;
    if(empty($id_estado)){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'estado_label'));
    }
    else{
        $daoEstado = DB_DataObject::factory('estado');
        if(!$daoEstado->get($id_estado)){
            Denko::addErrorMessage('error_invalidField',array('%field' => 'estado_label'));
        }
    }

    # Verifico que el valor del campo "Asignada a" sea v�lido
    $id_empleado = !empty($_POST['id_empleado']) ? trim($_POST['id_empleado']) : null;
    if(empty($id_empleado)){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'tarea_asignadaA'));
    }
    elseif($id_empleado != 'null'){
        $daoEmpleado = DB_DataObject::factory('empleado');
        if(!$daoEmpleado->get($id_empleado)){
            Denko::addErrorMessage('error_invalidField',array('%field' => 'tarea_asignadaA'));
        }
    }

    # Verifico que el valor del campo "Monto" sea v�lido
    $monto = isset($_POST['monto']) ? trim($_POST['monto']) : 0;
    if(!Denko::isInt($monto)){
        Denko::addErrorMessage('error_invalidField',array('%field' => 'tarea_monto'));
    }

    # Verifico que el valor del campo "Descripci�n" sea v�lido
    $descripcion = InnyCore_Utils::cleanWysiwygContent($_POST['descripcion']);
    if($descripcion == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'tarea_descripcion'));
    }

    # Verifico los archivos adjuntos
    for($i = 1; $i <= 6; $i++){
        if(isset($_FILES['temporal_'.$i])){

            $upload_error = InnyCore_File::getErrorArchivoAdjunto($_FILES['temporal_'.$i]);
            switch($upload_error){

                # En caso que el archivo exceda el tama�o m�ximo permitido
                case UPLOAD_ERR_INI_SIZE: Denko::addErrorMessage('temporal_errorUploadIniSize'); break;

                # En caso que solo se haya adjuntado parcialmente
                case UPLOAD_ERR_PARTIAL: Denko::addErrorMessage('temporal_errorUploadPartial'); break;

                # En cualquier otro caso
                default: break;
            }
        }
    }


    # En caso que no haya errores, inserto/actualizo el DAO
    if(!Denko::hasErrorMessages()){

        $daoTarea->id_sitio = $id_sitio;
        $daoTarea->id_estado = $id_estado;
        $daoTarea->id_empleado = $id_empleado;
        $daoTarea->descripcion = $_POST['descripcion'];

        # TODO: adjuntar archivos
        if($action == 'add'){
            $daoTarea->insert();
        }
        else{
            $daoTarea->update();
        }

        # Actualizo el monto de la Tarea
        $daoTarea->updateMonto($monto);

        # Adjunto los archivos a la Tarea
        for($i = 1; $i <= 6; $i++){
            if(!empty($_FILES['temporal_'.$i]['tmp_name'])){
                $id_archivo = InnyCore_File::almacenarArchivo($_FILES['temporal_'.$i]);
                $daoTareaTemporal = DB_DataObject::factory('tarea_temporal');
                $daoTareaTemporal->id_tarea = $daoTarea->id_tarea;
                $daoTareaTemporal->id_temporal = $id_archivo;
                $daoTareaTemporal->insert();
                $daoTareaTemporal->free();
                unset($daoTareaTemporal);
            }
        }

        # Me redirijo al listado de tareas
        Denko::redirect('tarea_abm.php?action=view&id_tarea='.$daoTarea->id_tarea);
    }

    # En caso que haya errores, retorno FALSE
    return false;
}
################################################################################
# Verifico que el usuario est� logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia de Smarty y cargo el filtro de los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

$linkBackTareas = new Inny_LinkBackTareas();
$linkBack = $linkBackTareas->getLinkBack();

# Obtengo el tipo de usuario logueado
$tipoUsuarioLogueado = Inny_Tracker::getTipoUsuario();

# Verifico que la acci�n sea v�lida
$acciones = array('view','addcomentario');
if($tipoUsuarioLogueado == USUARIO_ADMIN){
    $acciones = array_merge($acciones,array('add','edit','delete'));
}
Inny_Common::verificarAccion($acciones);

# Verifico que el ID de la tarea sea v�lido
$action = InnyCore_Utils::getParamValue('action');
if($action == 'add'){
    # Creo la instancia del DAO de la Tarea
    $daoTarea = DB_DataObject::factory('tarea');
}
else{
    $id_tarea = InnyCore_Utils::getParamValue('id_tarea');
    $daoTarea = Inny_Common::verificarDao($smarty,'tarea',$id_tarea);
    $daoTarea->fetch();
}

# En caso que haya alguna accion especial que ejecutar
if($_SERVER['REQUEST_METHOD'] == 'GET'){

    switch($action){

        # Elimino la tarea
        case 'delete':
            $daoTarea->delete();
            Denko::redirect($linkBack);
    }
}

# En caso que haya variables en POST
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    switch($action){

        # En caso de agregar/editar una tarea
        case 'add':
        case 'edit':
            tareaAbmAdmin($action,$smarty,$daoTarea);
            break;

        case 'view':
            tareaAbmEmpleado($smarty,$daoTarea);
            break;

        # En caso de agregar un comentario
        case 'addcomentario':
            # Cargo los mensajes de error
            $smarty->config_load('speech.conf','comentario');

            # Verifico qeu el comentario no sea vac�o
            if(empty($_POST['comentario']) || ($comentario = InnyCore_Utils::cleanWysiwygContent($_POST['comentario'])) == ''){
                Denko::addErrorMessage('error_requiredField',array('%field'=>$smarty->get_config_vars('comentario_comment')));
            }

            # Agrego el comentario en la DB
            else{
                $daoComentario = DB_DataObject::factory('comentario');
                $daoComentario->texto = $_POST['comentario'];
                $daoComentario->id_tarea = $id_tarea;
                $daoComentario->fecha = date('Y-m-d H:i:s');

                # Seteo el autor del comentario
                if($tipoUsuarioLogueado == USUARIO_ADMIN){
                    $daoComentario->autor = 'admin';
                }
                else{
                    $daoEmpleadoLogueado = Inny_Tracker::getDaoEmpleadoLogueado();
                    $daoComentario->autor = $daoEmpleadoLogueado->username;
                }

                # Agrego el comentario a la DB y me redirijo al view de la Tarea
                $daoComentario->insert();
                Denko::redirect(basename($_SERVER['PHP_SELF']).'?action=view&id_tarea='.$id_tarea);
            }
            break;
    }
}

# Asigno las variables al template
$smarty->assign('daoTarea',$daoTarea);
$smarty->assign('linkBack',$linkBack);

# Muestro el template
$smarty->display($action == 'view' ? 'tarea_view.tpl' : 'tarea_abm.tpl');

################################################################################