<?php
/**
 * Project: Inny Tracker
 * File: tarea_listado.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Verifico si me tengo que redirigir a la url cacheada del listado
$linkBackTareas = new Inny_LinkBackTareas();
$linkBackTareas->verifyRedirect();

# Guardo en session la URL con el querystring
$linkBackTareas->setLinkBack();

# Muestro el template
$smarty->display('tarea_listado.tpl');