<?php
/**
 * Project: Inny Tracker
 * File: tarea_mistareas.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarEmpleadoLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# En caso que el usuario logueado sea un empleado, Obtengo el DAO y lo asigno+
# al template
$tipoUsuarioLogueado = Inny_Tracker::getTipoUsuario();
if($tipoUsuarioLogueado == USUARIO_EMPLEADO){
    $daoEmpleadoLogueado = Inny_Tracker::getDaoEmpleadoLogueado();
    $smarty->assign('daoEmpleadoLogueado',$daoEmpleadoLogueado);
}

# Muestro el template
$smarty->display('tarea_listado.tpl');