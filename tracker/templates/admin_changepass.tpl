{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="password"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#password_change# linkBack="configuracion.php"}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/inny.password.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{if $smarty.get.change == "ok"}
    {include file="messagebox_okmessages.tpl"}
{/if}
{* ========================================================================== *}
<div class="form">
	<form name="changepass" action="admin_changepass.php" method="post" onsubmit="javascript: return Inny_Password.validate();">
		{#password_label#}:
		<br />
		<input type="password" id="password" name="password" maxlength="40" />
		<br />
		<br />
		{#password_newPassword#}:
		<br />
		<input type="password" id="newpassword" name="newpassword" maxlength="40" />
		<br />
		<br />
		{#password_confirmNewPass#}:
		<br />
		<input type="password" id="confirmnewpass" name="confirmnewpass" maxlength="40" />
		<br />
		<br />
		<input type="submit" value="{#password_change#}" />
	</form>
	<script type="text/javascript">
    //<![CDATA[
        Inny_Password.init();
    //]]>
    </script>
</div>
{* ========================================================================== *}
{include file="footer.tpl"}

core: 681