{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="password"}
{config_load file="speech.conf" section="configuracion"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#configuracion_pageTitle#}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/dk.common.js" inline="false"}
{dk_include file="js/inny.metadata.js" inline="false"}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{* ========================================================================== *}
<div class="form">

    <!-- ============================= METADATA ============================ -->
    <hr />
    <br />
    <b>{#configuracion_metadataTitle#}</b>: <input type="button" value="{#configuracion_metadataButtonValue#}" onclick="javascript:Inny_Metadata.actualizarTodosLosSitios();" />
    &nbsp;
    <input type="checkbox" id="incluir_deshabilitados" /> {#configuracion_metadataIncluirDeshabilitados#}
    <br />
    <div id="updateMetadataLogs" style="display:none;"></div>
    <br />
    <br />

    <!-- =================================================================== -->
    <hr />
    {dk_daolister name="lc" table="configuracion" query="indice1 is null"}
        {dkf_declare
            name = "btc"
            type = "select"
            table = "tipoconfiguracion"
            display = "nombre"
        }
        {dk_filters name="filters"}
            <b>{#configuracion_tipoLabel#}:</b> {dkf_input name="btc" autoSubmit="true" disableShowAllOption="true"}
        {/dk_filters}

        <br />
        <form action="configuracion.php" method="post">
            {dk_lister export="nombre,valor,descripcion"}
            {$descripcion}
            <br />
            {if $nombre == "dokko_admin_pass"}
                <a href="admin_changepass.php" title="{#password_change#}">{#password_change#}</a>
            {else}
                <input type="text" name="{$nombre}" value="{$valor}" />
            {/if}
            <br />
            <br />
            {/dk_lister}
            <input type="submit" value="{#common_edit#}" />
        </form>
    {/dk_daolister}
</div>
{include file="footer.tpl"}