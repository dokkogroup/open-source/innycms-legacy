{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="filter"}
{config_load file="speech.conf" section="contenido"}
{config_load file="speech.conf" section="paginator"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#contenido_listadoPageTitle#|replace:"%nombre_sitio":$daoSitio->nombre_sitio}
{dk_include file="styles/lister.css" inline=false}
{dk_include file="styles/contenido_preview.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{* ========================================================================== *}
{dk_daolister name="lcfg" table="configuracion" query="indice1 is not null and indice1 = "|cat:$daoSitio->id_sitio}
    {dkf_declare
        name = "bn"
        type = "text"
        queryToApply = "nombre like '%@VALUE@%'"
    }
    {dko_declare
        name = "o_bn"
        column = "nombre"
    }
    {dkma_declare
        url = "contenido_abm.php"
        multidelete = #common_delete#|cat:"|"|cat:#contenido_deleteConfirm#
    }

    <!-- ============================= FILTROS ============================= -->
    {dk_filters name="filtros"}
    <div class="filters">
        <table cellpadding="2" cellspacing="2">
            <tbody>
                <tr>
                    <td style="text-align:left;">{#contenido_nombre#}:&nbsp;{dkf_input name="bn"}</td>
                    <td style="text-align:right;"><input class="submit" type="submit" value="{#filter_submit#}" /></td>
                </tr>
                <tr>
                	<td colspan="2" style="text-align: left;">
                        {#filter_select#}:
                        &nbsp;
                    	<a href="{dkma_url type="all"}" title="{#filter_all#}">{#filter_all#}</a>
                    	&nbsp;
                        <a href="{dkma_url type="none"}" title="{#filter_none#}">{#filter_none#}</a>
                        &nbsp;
                        <a href="{dkma_url type="invert"}" title="{#filter_invert#}">{#filter_invert#}</a>
                        &nbsp;
                        {dkma_actions}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    {/dk_filters}

    <!-- ========================== LISTADO ================================ -->
    <br />
    {dkp_ini}
    <div class="paginatorHeader">{#paginator_results#}: <b>{$dkp_begin}</b> - <b>{$dkp_end}</b> de <b>{$dkp_results}</b></div>
    <table class="lister" cellpadding="2" cellspacing="1" style="text-align:left;">
        <thead>
            <tr class="header">
                <td>&nbsp;</td>
                <td class="order">
                    <span class="title">{#contenido_nombre#}</span>
                    <span class="arrows">
                        <a href="{dko_url name="o_bn" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" alt="{#common_orderDesc#}" /></a>
                        <a href="{dko_url name="o_bn" order="asc"}" title="{#common_orderAsc#}"><img src="images/arrow_up.gif" alt="{#common_orderAsc#}" /></a>
                    </span>
                </td>
                <td>{#contenido_valor#}</td>
                <td colspan="3">&nbsp;</td>
            </tr>
        </thead>

        <tbody>
        {dk_lister export="id_configuracion,nombre,tipo,valor,descripcion"}
            {inny_get_contenido assign="contenido" id_configuracion=$id_configuracion}
            {assign var="innyType" value=$contenido->createInnyType()}
            <tr class="normal">
                <td class="icon" style="padding-top:10px;">{dkma_checkbox}</td>
                <td style="vertical-align:top; padding:10px;">{$contenido->getMetadataValue("nombre")}</td>
                <td style="padding:10px;">
                    {if $daoSitio->getMetadataValue("multilang","enabled") == "1" && $innyType->getMetadataValue("parametros","multilang") == "true"}
                        {foreach from=$daoSitio->getMetadataValue("multilang","languages") key="language_code" item="language_label"}
                            <b>{$language_label}:</b>
                            <div style="border: 1px solid #cccccc; padding:5px;">
                                {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor|sw_translate:$language_code size="tiny"}
                            </div>
                            <br />
                        {/foreach}
                    {else}
                        {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor size="tiny"}
                    {/if}
                </td>
                <td class="icon"><a href="{dkc_url url="contenido_abm.php" action="view"}" title="{#contenido_viewLinkText#}"><img src="images/view.gif" alt="{#common_view#}" /></td>
                <td class="icon"><a href="{dkc_url url="contenido_abm.php" action="delete" confirmMessage=#contenido_confirmDelete#}" title="{#contenido_deleteLinkText#}"><img src="images/delete.gif" alt="{#common_delete#}" /></td>
            </tr>
        {/dk_lister}
        </tbody>
    </table>
    {include file="paginador.tpl"}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl"}