{config_load file="speech.conf" section="sitio"}
{config_load file="speech.conf" section="contenido"}
{* ========================================================================== *}
{assign var="daoSitio" value=$contenido->getSitio()}
{assign var="innyType" value=$contenido->createInnyType()}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#contenido_viewPageTitle# linkBack=$linkBack}
{dk_include file="styles/form.css"}
{dk_include file="styles/contenido_preview.css" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="error_messages.tpl"}
{/if}
{* ========================================================================== *}
<table class="formTable" cellpadding="2" cellspacing="1">
    <thead>
        <tr class="header">
            <td>{#contenido_nombre#}</td>
            <td>{#contenido_etiqueta#}</td>
            <td>{#contenido_tipo#}</td>
        </tr>
    </thead>
    <tbody>
        <tr class="data">
            <td style="text-align:center;">{$contenido->getMetadataValue("nombre")}</td>
            <td style="text-align:center;">{$contenido->nombre}</td>
            <td style="text-align:center;">{$contenido->getMetadataValue("tipo")}</td>
        </tr>
        <tr class="header">
            <td colspan="3">{#contenido_valor#}</td>
        </tr>
        <tr class="data">
            <td colspan="3" style="padding:15px; font-family:arial,helvetica,sans-serif; font-size:12px; text-align:left;">
                {if $daoSitio->getMetadataValue("multilang","enabled") == "1" && $innyType->getMetadataValue("parametros","multilang") == "true"}
                    {foreach from=$daoSitio->getMetadataValue("multilang","languages") key="language_code" item="language_label"}
                        <b>{$language_label}</b>
                        <div style="border: 1px solid #cccccc; padding:5px;">
                            {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor|sw_translate:$language_code size="normal"}
                        </div>
                        <br />
                    {/foreach}
                {else}
                    {include file="common_type_view.tpl" innyType=$innyType value=$contenido->valor size="normal"}
                {/if}
        	</td>
        </tr>
    </tbody>
    <tfoot>
        <tr class="submit">
            <td colspan="3">
                <input type="button" value="{#common_back#}" onclick="javascript: window.location.href = '{$linkBack}';" />
            </td>
        </tr>
    </tfoot>
</table>
{* ========================================================================== *}
{include file="footer.tpl"}