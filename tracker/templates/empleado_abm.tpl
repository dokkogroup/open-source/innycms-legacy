{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="password"}
{config_load file="speech.conf" section="empleado"}
{config_load file="speech.conf" section="empleado_abm"}
{* ========================================================================== *}
{assign var="id_empleado" value=$smarty.post.id_empleado|default:$daoEmpleado->id_empleado}
{assign var="action" value=$smarty.get.action|default:$smarty.post.action}
{if $action == "insert"}
    {assign var="pageTitle" value=#empleado_insertPageTitle#}
    {assign var="submitValue" value=#common_add#}
{else}
    {assign var="pageTitle" value=#empleado_updatePageTitle#}
    {assign var="submitValue" value=#common_edit#}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="js/inny.empleado.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{if $smarty.get.change == "ok"}
    {include file="messagebox_okmessages.tpl"}
{/if}
{* ========================================================================== *}
<div class="form">
    <form method="post" action="empleado_abm.php" onsubmit="javascript:return Inny_Empleado.validate();">
        <input type="hidden" id="action" name="action" value="{$action}" />
        {if $action == "edit"}
            <input type="hidden" id="id_empleado" name="id_empleado" value="{$daoEmpleado->id_empleado}" />
        {/if}
        {#empleado_username#}:
        <br />
        <input type="text" class="text" id="username" name="username" value="{$smarty.post.username|default:$daoEmpleado->username}" />
        <br />
        <br />
        {#empleado_password#}:
        <br />
        {if $action == "edit"}
            <a href="empleado_abm.php?action=changepass&id_empleado={$daoEmpleado->id_empleado}" title="{#password_change#}">{#password_change#}</a>
        {else}
            <input type="text" class="text" id="password" name="password" value="{$smarty.post.password|default:$daoEmpleado->password}" />
        {/if}
        <br />
        <br />
        {#empleado_apellido#}:
        <br />
        <input type="text" class="text" id="apellido" name="apellido" value="{$smarty.post.apellido|default:$daoEmpleado->apellido}" />
        <br />
        <br />
        {#empleado_nombre#}:
        <br />
        <input type="text" class="text" id="nombre" name="nombre" value="{$smarty.post.nombre|default:$daoEmpleado->nombre}" />
        <br />
        <br />
        {#empleado_email#}:
        <br />
        <input type="text" class="text" id="email" name="email" value="{$smarty.post.email|default:$daoEmpleado->email}" />
        <br />
        <br />
        {#empleado_telefono#}:
        <br />
        <input type="text" class="text" id="telefono" name="telefono" value="{$smarty.post.telefono|default:$daoEmpleado->telefono}" />
        <br />
        <br />
        {#empleado_habilitado#}: <input type="checkbox" id="habilitado" name="habilitado" {if ($smarty.post.action && $smarty.post.habilitado) || $smarty.get.action == "insert" || ($smarty.get.action == "edit" && $daoEmpleado->habilitado == "1")} checked="checked"{/if} />
        <br />
        <br />
        <input type="submit" value="{$submitValue}" />
    </form>
    <script type="text/javascript">
    //<![CDATA[
        Inny_Empleado.init();
    //]]>
    </script>
</div>
{* ========================================================================== *}
{include file="footer.tpl"}