{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="empleado"}
{config_load file="speech.conf" section="password"}
{* ========================================================================== *}
{tracker_get_tipo_usuario_logueado assign="tipo_usuario_logueado"}
{if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
    {assign var="id_empleado" value=$smarty.get.id_empleado|default:$smarty.post.id_empleado}
    {assign var="linkBack" value="empleado_abm.php?action=view&id_empleado="|cat:$daoEmpleado->id_empleado}
{else}
    {tracker_get_dao_empleado_logueado assign="daoEmpleadoLogueado"}
    {assign var="id_empleado" value=$daoEmpleadoLogueado->id_empleado}
    {assign var="linkBack" value="empleado_abm.php?action=view"}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#password_change# linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/inny.password.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{if $smarty.get.change == "ok"}
    {include file="messagebox_okmessages.tpl"}
{/if}
{* ========================================================================== *}
<div class="form">
    <form action="empleado_abm.php" method="post" onsubmit="javascript: return Inny_Password.validate();">
        <input type="hidden" name="id_empleado" value="{$id_empleado}" />
        <input type="hidden" name="action" value="changepass" />
        {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
            {#empleado_nya#}: <b>{$daoEmpleado->apellido}, {$daoEmpleado->nombre}</b>
            <br />
            <br />
        {else}
            {#password_label#}:
        	<br />
        	<input type="password" id="password" name="password" maxlength="40" />
        	<br />
        	<br />
        {/if}
        {#password_newPassword#}:
        <br />
        <input type="password" id="newpassword" name="newpassword" maxlength="40" />
        <br />
        <br />
        {#password_confirmNewPass#}:
        <br />
        <input type="password" id="confirmnewpass" name="confirmnewpass" maxlength="40" />
        <br />
        <br />
        <input type="submit" value="{#password_change#}" />
    </form>
    <script type="text/javascript">
    //<![CDATA[
        Inny_Password.init();
    //]]>
    </script>
</div>
{* ========================================================================== *}
{include file="footer.tpl"}