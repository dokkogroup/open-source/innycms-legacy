{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="filter"}
{config_load file="speech.conf" section="paginator"}
{config_load file="speech.conf" section="empleado"}
{* ========================================================================== *}
{tracker_get_tipo_usuario_logueado assign="tipo_usuario_logueado"}
{if $tipo_usuario_logueado == $smarty.const.USUARIO_EMPLEADO}
    {assign var="daoListerQuery" value="habilitado = '1'"}
{else}
    {assign var="daoListerQuery" value=""}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#empleado_listadoPageTitle#}
{dk_include file="styles/lister.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{* ========================================================================== *}
{dk_daolister name="empleado" table="empleado" resultsPerPage="25" query=$daoListerQuery}
    {dkf_declare
        name = "bus"
        type = "text"
        queryToApply = "username like '%@VALUE@%'"
    }
    {dkf_declare
        name = "bna"
        type = "text"
        queryToApply = "nombre like '%@VALUE@%' or apellido like '%@VALUE@%'"
    }
    {dko_declare
        name = "order_by_us"
        column = "username"
    }
    {dko_declare
        name = "order_by_apynom"
        onAsc = "apellido asc, nombre asc"
        onDesc = "apellido desc, nombre desc"
    }

    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
        {dkma_declare
            url = "empleado_abm.php"
            enable = #common_enable#|cat:"|"|cat:#empleado_multiEnable#
            disable = #common_disable#|cat:"|"|cat:#empleado_multiDisable#
        }
    {/if}

    <!-- =========================== FILTROS =============================== -->
    {dk_filters name="filtros"}
    <div class="filters">
        <table cellpadding="2" cellspacing="2">
            <tbody>
                <tr>
                    <td>{#empleado_username#}: {dkf_input name="bus"}</td>
                    <td>{#empleado_nya#}: {dkf_input name="bna"}</td>
                    <td style="text-align:right;"><input class="submit" type="submit" value="{#filter_submit#}" /></td>
                </tr>
                {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                    <tr>
                    	<td colspan="2" style="text-align: left;">
                        	<a href="{dkma_url type="all"}" title="{#filter_all#}">{#filter_all#}</a>
                        	&nbsp;
                            <a href="{dkma_url type="none"}" title="{#filter_none#}">{#filter_none#}</a>
                            &nbsp;
                            <a href="{dkma_url type="invert"}" title="{#filter_invert#}">{#filter_invert#}</a>
                            &nbsp;
                            {dkma_actions}
                        </td>
                        <td colspan="3" style="text-align:right;"><a href="empleado_abm.php?action=insert" title="{#empleado_insert#}">{#empleado_insert#}</a></td>
                    </tr>
                {/if}
            </tbody>
        </table>
    </div>
    {/dk_filters}
    <br />

    <!-- ========================== LISTADO ================================ -->
    {dkp_ini}
    {if $dkp_results > 0}
        <div class="paginatorHeader">{#paginator_results#}: <b>{$dkp_begin} - {$dkp_end}</b> de <b>{$dkp_results}</b></div>
        <table class="lister" cellpadding="2" cellspacing="1" style="text-align:left;">
            <thead>
                <tr class="header">
                    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                        <td>&nbsp;</td>
                    {/if}
                    <td>
                        <span class="title">{#empleado_id#}</span>
                    </td>
                    <td class="order">
                        <span class="title">{#empleado_username#}</span>
                        <span class="arrows">
                            <a href="{dko_url name="order_by_us" order="asc"}" title="{#common_orderAsc#}"><img src="images/arrow_up.gif" alt="{#common_orderAsc#}" /></a>
                            <a href="{dko_url name="order_by_us" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" alt="{#common_orderDesc#}" /></a>
                        </span>
                    </td>
                    <td class="order">
                        <span class="title">{#empleado_nya#}</span>
                        <span class="arrows">
                            <a href="{dko_url name="order_by_apynom" order="asc"}" title="{#common_orderAsc#}"><img src="images/arrow_up.gif" alt="{#common_orderAsc#}" /></a>
                            <a href="{dko_url name="order_by_apynom" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" alt="{#common_orderDesc#}" /></a>
                        </span>
                    </td>
                    <td>{#empleado_email#}</td>
                    <td colspan="4">&nbsp;</td>
                </tr>
            </thead>
            <tbody>
            {dk_lister export="id_empleado,username,nombre,apellido,email,id_ctacorriente,habilitado"}
                <tr class="normal">
                    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                        <td>{dkma_checkbox}</td>
                    {/if}
                    <td>{$id_empleado}</td>
                    <td>{$username}</td>
                    <td>{$apellido}, {$nombre}</td>
                    <td>{$email}</td>
                    <td class="icon"><a href="{dkc_url url="empleado_abm.php" action="view"}" title="{#common_view#}"><img src="images/view.gif" alt="{#common_view#}" /></a></td>
                    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                        <td class="icon"><a href="{dkc_url url="empleado_abm.php" action="edit"}" title="{#common_edit#}"><img src="images/edit.gif" alt="{#common_edit#}" /></a></td>
                        <td class="icon">
                            {if $habilitado == $smarty.const.EMPLEADO_HABILITADO}
                                <a href="{dkc_url url="empleado_abm.php" action="disable" confirmMessage=#empleado_deshabilitarConfirm#|replace:"%username":$username}" title="{#empleado_habilitadoLinkTitle#}"><img src="images/enabled.gif" alt="{#common_enable#}" /></a>
                            {else}
                                <a href="{dkc_url url="empleado_abm.php" action="enable"  confirmMessage=#empleado_habilitarConfirm#|replace:"%username":$username}" title="{#empleado_deshabilitadoLinkTitle#}"><img src="images/disabled.gif" alt="{#common_enable#}" /></a>
                            {/if}
                        </td>
                    {/if}
                </tr>
            {/dk_lister}
            </tbody>
        </table>
        <br />
        {include file="paginador.tpl"}
    {else}
        <span class="listadoSinResultados">{#common_emptyList#}</span>
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl"}