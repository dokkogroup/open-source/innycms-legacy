{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="password"}
{config_load file="speech.conf" section="empleado"}
{* ========================================================================== *}
{tracker_get_tipo_usuario_logueado assign="tipo_usuario_logueado"}
{if $tipo_usuario_logueado == $smarty.const.USUARIO_EMPLEADO}
    {tracker_get_dao_empleado_logueado assign="daoEmpleadoLogueado"}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#empleado_viewPageTitle# linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{* ========================================================================== *}
<table class="formTable" cellpadding="2" cellspacing="1">
    <tbody>

    <!-- =================================================================== -->
    <tr class="header">
        {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN || $daoEmpleado->id_empleado == $daoEmpleadoLogueado->id_empleado}
            <td style="width:25%; text-align: center;">{#empleado_id#}</td>
            <td style="width:25%; text-align: center;">{#empleado_username#}</td>
            <td style="width:25%; text-align: center;">{#password_label#}</td>
            <td style="width:25%; text-align: center;">{#empleado_habilitado#}</td>
        {else}
            <td style="width:50%; text-align: center;" colspan="2">{#empleado_id#}</td>
            <td style="width:50%; text-align: center;" colspan="2">{#empleado_username#}</td>
        {/if}
    </tr>
    <tr class="data">
        {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN || (!empty($daoEmpleadoLogueado) && $daoEmpleadoLogueado->id_empleado == $daoEmpleado->id_empleado)}
            <td style="width:25%; text-align: center;">{$daoEmpleado->id_empleado}</td>
            <td style="width:25%; text-align: center;">{$daoEmpleado->username}</td>
            {if $daoEmpleado->id_empleado == $daoEmpleadoLogueado->id_empleado}
                {assign var="urlChangePass" value="empleado_abm.php?action=changepass"}
            {else}
                {assign var="urlChangePass" value="empleado_abm.php?action=changepass&id_empleado="|cat:$daoEmpleado->id_empleado}
            {/if}
            <td style="width:25%; text-align: center;">
                <a href="{$urlChangePass}" title="{#password_change#}">{#password_change#}</a>
            </td>
            <td style="width:25%; text-align: center;">{if $daoEmpleado->habilitado == "1"}{#common_yes#}{elseif $daoEmpleado->habilitado == "0"}{#common_no#}{/if}</td>
        {else}
            <td style="width:50%; text-align: center;" colspan="2">{$daoEmpleado->id_empleado}</td>
            <td style="width:50%; text-align: center;" colspan="2">{$daoEmpleado->username}</td>
        {/if}
    </tr>

    <!-- =================================================================== -->
    <tr class="header">
        <td style="width:25%; text-align:center;">{#empleado_apellido#}</td>
        <td style="width:25%; text-align:center;">{#empleado_nombre#}</td>
        <td style="width:25%; text-align:center;">{#empleado_email#}</td>
        <td style="width:25%; text-align:center;">{#empleado_telefono#}</td>
    </tr>
    <tr class="data">
        <td style="width:25%; text-align:center;">{$daoEmpleado->apellido}</td>
        <td style="width:25%; text-align:center;">{$daoEmpleado->nombre}</td>
        <td style="width:25%; text-align:center;">{$daoEmpleado->email}</td>
        <td style="width:25%; text-align:center;">{$daoEmpleado->telefono}</td>
    </tr>

    <!-- =================================================================== -->
    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
        <tr class="link">
            <td colspan="4">
                <a href="empleado_abm.php?action=edit&id_empleado={$daoEmpleado->id_empleado}" title="{#common_edit#}">{#common_edit#}</a>
            </td>
        </tr>
    {/if}

    </tbody>
</table>
{* ========================================================================== *}
{include file="footer.tpl"}