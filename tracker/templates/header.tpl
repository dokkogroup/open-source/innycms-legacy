{config_load file="speech.conf" section="application"}
{config_load file="speech.conf" section="header"}
{config_load file="speech.conf" section="common"}
{* ========================================================================== *}
{tracker_get_tipo_usuario_logueado assign="tipo_usuario_logueado"}
{* ========================================================================== *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-AR" lang="es-AR">
    <head>
        <title>{#application_name#} :: {$pageTitle}</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="content-language" content="es-ar" />
        <meta name="copyright" content="Copyright 2007-{$smarty.now|date_format:"%Y"}. Dokko Group All Rights Reserved." />
        {dk_include file="styles/main.css"}
        {dk_include_target}
    </head>
    <body>
        <div class="main">
            <center>
            <div class="container">
                <div class="header">
                    <div class="links">
                        <a href="sitio_listado.php" title="{#header_sitioListado#}">{#header_sitioListado#}</a>
                        &nbsp;|&nbsp;
                        <a href="log_listado.php" title="{#header_logListado#}">{#header_logListado#}</a>
                        &nbsp;|&nbsp;
                        <a href="usuario_listado.php" title="{#header_usuarioListado#}">{#header_usuarioListado#}</a>
                        &nbsp;|&nbsp;
                        <a href="tarea_listado.php" title="{#header_tareaListado#}">{#header_tareaListado#}</a>
                        &nbsp;|&nbsp;
                        {if $tipo_usuario_logueado == $smarty.const.USUARIO_EMPLEADO}
                            <a href="tarea_mistareas.php" title="{#header_miTareaListado#}">{#header_miTareaListado#}</a>
                            &nbsp;|&nbsp;
                            <a href="empleado_abm.php?action=view" title="{#header_infoPersonal#}">{#header_infoPersonal#}</a>
                            &nbsp;|&nbsp;
                            {*
                            <a href="ctacorriente.php" title="{#header_ctacteListado#}">{#header_ctacteListado#}</a>
                            &nbsp;|&nbsp;
                            *}
                        {/if}
                        <a href="empleado_listado.php?dko_empleado_order_by_us=a" title="{#header_empleadoListado#}">{#header_empleadoListado#}</a>
    					&nbsp;|&nbsp;
    					{if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                            <a href="configuracion.php" title="{#header_configuracionListado#}">{#header_configuracionListado#}</a>
                            &nbsp;|&nbsp;
                        {/if}
                        <a href="index.php?logout" title="{#header_logout#}">{#header_logout#}</a>
                    </div>
                </div>
                <div class="content">
                    <div class="title">
                        <h1>{$pageTitle}</h1>
                    </div>
                    {if $linkBack}
                        <div class="linkBack"><a title="{#common_back#}" href="{$linkBack}">� {#common_back#}</a></div>
                    {/if}
