<div class="divL2S_link" id="{$id_link}">
    <a href="javascript:showSelect('{$id_link}','{$id_select}','{$selectName}');">{$link_text}</a>
</div>
<div class="divL2S_select" id="{$id_select}">
    <select id="{$selectName}" name="{$selectName}" disabled="disabled">
        {html_options options=$options selected=$selected}
    </select>
</div>