{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="listado"}
{config_load file="speech.conf" section="filter"}
{config_load file="speech.conf" section="paginator"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#listado_pageTitle#|replace:"%nombre_sitio":$daoSitio->nombre_sitio}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="styles/lister.css" inline=false}
{* ========================================================================== *}
{dk_daolister name="ll" table="listado" query="id_sitio = "|cat:$daoSitio->id_sitio}
    {dkf_declare
        name = "bn"
        type = "text"
        queryToApply = "nombre like '%@VALUE@%'"
    }
    {dko_declare
        name = "o_bn"
        column = "nombre"
    }
    {dkma_declare
        url = "listado_abm.php"
        multidelete = #listado_multidelete#|cat:"|"|cat:#listado_confirmMultidelete#
    }

    <!-- ============================= FILTROS ============================= -->
    {dk_filters name="filtros"}
    <div class="filters">
        <table cellpadding="2" cellspacing="2" style="text-align:left;">
            <tbody>
                <tr>
                    <td>{#listado_nombre#}:&nbsp;{dkf_input name="bn"}</td>
                    <td align="right"><input class="submit" type="submit" value="{#filter_submit#}" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        {#filter_select#}:
                        &nbsp;
                        <a href="{dkma_url type="all"}" title="{#filter_all#}">{#filter_all#}</a>
                        &nbsp;
                        <a href="{dkma_url type="none"}" title="{#filter_none#}">{#filter_none#}</a>
                        &nbsp;
                        <a href="{dkma_url type="invert"}" title="{#filter_invert#}">{#filter_invert#}</a>
                        &nbsp;&nbsp;
                        {dkma_actions}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    {/dk_filters}

    <!-- ========================== LISTADO ================================ -->
    <br />
    {dkp_ini}
    <div class="paginatorHeader">{#paginator_results#}: <b>{$dkp_begin}</b> - <b>{$dkp_end}</b> de <b>{$dkp_results}</b></div>
    <table class="lister" cellpadding="2" cellspacing="1" style="text-align:left;">
        <thead>
            <tr class="header">
                <td>&nbsp;</td>
                <td class="order">
                    <span class="title">{#listado_nombre#}</span>
                    <span class="arrows">
                        <a href="{dko_url name="o_bn" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" alt="{#common_orderDesc#}" /></a>
                        <a href="{dko_url name="o_bn" order="asc"}" title="{#common_orderAsc#}"><img src="images/arrow_up.gif" alt="{#common_orderAsc#}" /></a>
                    </span>
                </td>
                <td colspan="2">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
        {dk_lister export="id_listado,nombre"}
            <tr class="normal">
                <td class="icon">{dkma_checkbox}</td>
                <td>{$nombre}</td>
                {*
                <td class="icon"><a href="{dkc_url url="listado_abm.php" action="view"}" title="{#listado_viewLinkTitle#}"><img src="images/view.gif" alt="{#common_view#}" /></td>
                <td class="icon"><a href="{dkc_url url="listado_abm.php" action="edit"}" title="{#listado_editLinkTitle#}"><img src="images/edit.gif" alt="{#common_edit#}" /></td>
                *}
                <td class="icon"><a href="{dkc_url url="listado_abm.php" action="delete" confirmMessage=#listado_confirmDelete#|replace:"%nombre_listado":$nombre}" title="{#listado_deleteLinkTitle#}"><img src="images/delete.gif" alt="{#common_delete#}" border="0" /></td>
            </tr>
        {/dk_lister}
        </tbody>
    </table>
    {include file="paginador.tpl"}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl"}