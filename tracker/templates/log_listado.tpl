{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="sitio"}
{config_load file="speech.conf" section="log"}
{config_load file="speech.conf" section="filter"}
{config_load file="speech.conf" section="paginator"}
{* ========================================================================== *}
{tracker_get_tipo_usuario_logueado assign="tipo_usuario_logueado"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#log_listadoPageTitle#}
{dk_include file="styles/lister.css" inline="false"}
{dk_include file="js/dk.common.js" inline="false"}
{dk_include file="js/dk.multiaction.js" inline="false"}
{* ========================================================================== *}
{dk_daolister name="ll" table="log" query="indice1 is not null and indice1 != 0" resultsPerPage="25"}
    {dkf_declare
        type = "select"
        name = "bps"
    	values = $filtro_sitios
    	queryToApply = "indice1 = @VALUE@"
    }
    {dko_declare
    	name = "os"
    	column = "id_sitio"
    }
    {dko_declare
    	name = "of"
    	column = "aud_ins_date"
    	default = "desc"
    }
    {dkma_declare
        url    = "log_listado.php"
        delete = "eliminar|�Desea eliminar todos los logs seleccionados?."
    }

    <!-- ============================= FILTROS ============================= -->
    {dk_filters name="filtros"}
    <div class="filters">
    	<table cellpadding="2" cellspacing="2">
            <tbody>
                <tr>
                	<td style="text-align: left;">
                    	<a href="{dkma_url type="all"}" title="{#filter_all#}">{#filter_all#}</a>
                    	&nbsp;
                        <a href="{dkma_url type="none"}" title="{#filter_none#}">{#filter_none#}</a>
                        &nbsp;
                        <a href="{dkma_url type="invert"}" title="{#filter_invert#}">{#filter_invert#}</a>
                        &nbsp;
                        {dkma_actions}
                    </td>
                    <td style="text-align: right;">
                        {#sitio_nombreSitio#}: {dkf_input name="bps" showAllText=#sitio_optionAll# autoSubmit="true"}
                    </td>
                </tr>
            </tbody>
	    </table>
    </div>
    {/dk_filters}

    <!-- ========================== LISTADO ================================ -->
    <br />
    {dkp_ini}
    {if $dkp_results > 0}
        <div class="paginatorHeader">{#paginator_results#}: <b>{$dkp_begin}</b> - <b>{$dkp_end}</b> de <b>{$dkp_results}</b></div>
        <table class="lister" cellpadding="2" cellspacing="1" style="text-align:left;">
            <thead>
                <tr class="header">

                    <!-- MULTIACTION -->
                    <td>&nbsp;</td>

                    <!-- NOMBRE DEL SITIO -->
                	<td style="width:180px;">
                	    <span class="title">{#sitio_nombreSitio#}</span>
                        <span class="arrows">
                        	&nbsp;&nbsp;
                            <a href="{dko_url name="os" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" title="{#common_orderDesc#}" alt="{#common_orderDesc#}" /></a>
                            <a href="{dko_url name="os" order="asc"}"  title="{#common_orderAsc#}"><img src="images/arrow_up.gif" title="{#common_orderAsc#}" alt="{#common_orderAsc#}" /></a>
                        </span>
                    </td>

                	<!-- DESCRIPCION -->
                	<td>{#log_descripcion#}</td>

                	<!-- FECHA -->
                	<td style="width:130px;">
        				<span class="title">{#log_fecha#}</span>
                        <span class="arrows">
                        	&nbsp;&nbsp;
                            <a href="{dko_url name="of" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" title="{#common_orderDesc#}" alt="{#common_orderDesc#}" /></a>
                            <a href="{dko_url name="of" order="asc"}"  title="{#common_orderAsc#}"><img src="images/arrow_up.gif" title="{#common_orderAsc#}" alt="{#common_orderAsc#}" /></a>
                        </span>
                    </td>

                    <!-- ELIMINAR -->
                    <td>&nbsp;</td>
                </tr>
            </thead>
            <tbody>
            {dk_lister export="id_log,descripcion,indice1,aud_ins_date"}
                <tr class="normal">
                    {tracker_get_dao_sitio assign="daoSitioLog" id_sitio=$indice1}
                    <td>{dkma_checkbox}</td>
    	            <td style="padding:5px">{$daoSitioLog->nombre_sitio}</td>
                    <td style="padding:5px">{$descripcion}</td>
                    <td style="text-align:center;">{$aud_ins_date}</td>
                    <td class="icon"><a href="{dkc_url url="log_listado.php" action="delete"}" title="{#common_delete#}"><img src="images/delete.gif" alt="{#common_delete#}" /></a></td>
                </tr>
            {/dk_lister}
            </tbody>
        </table>
        {include file="paginador.tpl"}
    {else}
        <span class="listadoSinResultados">{#common_emptyList#}</span>
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl"}