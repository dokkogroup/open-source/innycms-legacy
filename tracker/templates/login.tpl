<html>
    {config_load file="speech.conf" section="application"}
    {config_load file="speech.conf" section="login"}
    <head>
        <title>{#application_name#} :: {#login_pageTitle#}</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
        {dk_include file="js/dk.common.js"}
        {dk_include file="js/login.js"}
    </head>
    <body>
        <table border="1">
            <form name="login" action="{$smarty.server.PHP_SELF}" method="post" onsubmit="javascrit: return validateLogin();">
            <tr>
                <td>{#login_username#}</td>
                <td><input type="text" name="username" maxlength="100" value="{$smarty.cookies.TRACKER_USERNAME}"></td>
            </tr>
            <tr>
                <td>{#login_password#}</td>
                <td><input type="password" name="password" maxlength="100"></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="{#login_submit#}"></td>
            </tr>
            </form>
            <script type="text/javascript" language="javascript">
            <!--
                document.login.username.focus();
            //-->
            </script>
        </table>
		<br/>
        <a href="index.php">Entrar con DOKKO LOGIN</a>
		{dk_showmessages}
            <br />
            {$dkm_message}
        {/dk_showmessages}
    </body>
</html>
