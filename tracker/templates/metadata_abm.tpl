{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="sitio"}
{* ========================================================================== *}

{assign var="id_sitio" value=$smarty.post.id_sitio|default:$daoSitio->id_sitio}
{assign var="submitValue" value=#common_edit#}

{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="js/inny.sitio.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{dk_hasmessages type="OK" assign="hasOKMessages"}
{if $hasOKMessages}
    {include file="messagebox_okmessages.tpl"}
{/if}

{* ========================================================================== *}
<div class="form">
    <form method="post" action="metadata_abm.php" >
        <input type="hidden" id="id_sitio" name="id_sitio" value="{$daoSitio->id_sitio}" />
        <!-- ========================== DYNAMIC METADATA =============================== -->
        <div class="title"><h1>{#sitio_dynamicMetadataLabel#}</h1></div>
        <textarea class="dynamicMetadata" name="dynamic_metadata">{$daoSitio->dynamic_metadata}</textarea>
        <br/>
        <br/>
        <!-- ============================ SUBMIT =========================== -->
        <input type="submit" value="{$submitValue}" />
    </form>
</div>
{* ========================================================================== *}
{include file="footer.tpl"}