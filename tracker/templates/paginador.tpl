<div class="paginatorFooter">
    <b>{#paginator_pages#}:</b>
    {if $dkp_actual > 1}
    &nbsp;&nbsp;&nbsp;&nbsp;
    <a href="{dkp_hrefpage number=$dkp_actual-1}">�&nbsp;{#paginator_prev#}</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="{dkp_hrefpage number=1}">1</a>
    {/if}
    {if $dkp_actual > 3}
    &nbsp;...&nbsp;
    {/if}
    {if $dkp_actual > 2}
    &nbsp;<a href="{dkp_hrefpage number=$dkp_actual-1}">{$dkp_actual-1}</a>&nbsp;
    {/if}
    &nbsp;<b>{$dkp_actual}</b>&nbsp;
    {if $dkp_actual < $dkp_last-0} &nbsp;<a href="{dkp_hrefpage number=$dkp_actual+1}">{$dkp_actual+1}</a>&nbsp; {/if}
    {if $dkp_actual < $dkp_last-3}
    ... &nbsp;<a href="{dkp_hrefpage number=$dkp_last}">{$dkp_last}</a>&nbsp;
    {else}
    {if $dkp_actual < $dkp_last-1} &nbsp;<a href="{dkp_hrefpage number=$dkp_actual+2}">{$dkp_actual+2}</a>&nbsp; {/if}
    {if $dkp_actual < $dkp_last-2} &nbsp;<a href="{dkp_hrefpage number=$dkp_actual+3}">{$dkp_actual+3}</a>&nbsp; {/if}
    {/if}
    {if $dkp_actual < $dkp_last}
    &nbsp;&nbsp;&nbsp;&nbsp;<a href="{dkp_hrefpage number=$dkp_actual+1}">{#paginator_next#}&nbsp;�</a>
    {/if}
</div>