{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="sitio"}
{* ========================================================================== *}
{assign var="id_sitio" value=$smarty.post.id_sitio|default:$daoSitio->id_sitio}
{assign var="action" value=$smarty.get.action|default:$smarty.post.action}
{if $action == "insert"}
    {assign var="pageTitle" value=#sitio_insertPageTitle#}
    {assign var="submitValue" value=#common_add#}
{else}
    {assign var="pageTitle" value=#sitio_updatePageTitle#}
    {assign var="submitValue" value=#common_edit#}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="js/inny.sitio.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{* ========================================================================== *}
<div class="form">
    <form method="post" action="sitio_abm.php" onsubmit="javascript:return Inny_Sitio.validate();">
        <input type="hidden" name="action" value="{$action}" />
        {if $action == "edit"}
            <input type="hidden" id="id_sitio" name="id_sitio" value="{$daoSitio->id_sitio}" />
        {/if}
        (*) {#sitio_nombreSitio#}:
        <br />
        <input type="text" class="text" id="nombre_sitio" name="nombre_sitio" value="{$smarty.post.nombre_sitio|default:$daoSitio->nombre_sitio}" maxlength="100" />
        <br />
        <br />
        (*) {#sitio_encargadoNombre#}:
        <br />
        <input type="text" class="text" id="nombre_encargado" name="nombre_encargado" value="{$smarty.post.nombre_encargado|default:$daoSitio->nombre_encargado}" maxlength="100" />
        <br />
        <br />
        (*) {#sitio_encargadoApellido#}:
        <br />
        <input type="text" class="text" id="apellido" name="apellido" value="{$smarty.post.apellido|default:$daoSitio->apellido}" maxlength="50" />
        <br />
        <br />

        <!-- ============================ SITIO ============================ -->
        {#sitio_url#}:
        <br />
        <input type="text" class="text" id="url_sitio" name="url_sitio" value="{$smarty.post.url_sitio|default:$daoSitio->url}" maxlength="100" />
        <br />
        <br />
        (*) {#sitio_username#}:
        <br />
        <input type="text" class="text" id="username" name="username" value="{$smarty.post.username|default:$daoSitio->username}" maxlength="100" />
        <br />
        <br />
        (*) {#sitio_password#}:
        <br />
        <input type="text" class="text" id="password" name="password" value="{$smarty.post.password|default:$daoSitio->password}" maxlength="45" />
        <br />
        <br />
        (*) {#sitio_telefono#}:
        <br />
        <input type="text" class="text" id="telefono" name="telefono" value="{$smarty.post.telefono|default:$daoSitio->telefono}" maxlength="50" />
        <br />
        <br />
        (*) {#sitio_direccion#}:
        <br />
        <input type="text" class="text" id="direccion" name="direccion" value="{$smarty.post.direccion|default:$daoSitio->direccion}" maxlength="100" />
        <br />
        <br />
        {#sitio_email#}:
        <br />
        <input type="text" class="text" id="email" name="email" value="{$smarty.post.email|default:$daoSitio->email}" maxlength="100" />
        <br />
        <br />
        {#sitio_idiomaCMS#}:
        <br />
        <select id="idioma_codigo" name="idioma_codigo">
            {tracker_htmloptions_idiomas_cms selected=$smarty.post.idioma_codigo|default:$daoSitio->idioma_codigo}
        </select>
        <br />
        <br />

        <!-- ============================ FTP ============================== -->
        {#sitio_ftp#}
        <br />
        <input type="text" class="text" id="ftp" name="ftp" value="{$smarty.post.ftp|default:$daoSitio->ftp}" maxlength="100" />
        <br />
        <br />
        {#sitio_ftpUsername#}:
        <br />
        <input type="text" class="text" id="ftp_username" name="ftp_username" value="{$smarty.post.ftp_username|default:$daoSitio->ftp_username}" maxlength="45" />
        <br />
        <br />
        {#sitio_ftpPassword#}:
        <br />
        <input type="text" class="text" id="ftp_password" name="ftp_password" value="{$smarty.post.ftp_password|default:$daoSitio->ftp_password}" maxlength="45" />
        <br />
        <br />
        (*) {#sitio_pathCVS#}:
        <br />
        <input type="text" class="text" id="cvs" name="cvs" value="{$smarty.post.cvs|default:$daoSitio->cvs}" maxlength="45" />
        <br />
        <br />
        {#sitio_gaCodigo#}:
        <br />
        <input type="text" class="text" id="ga_codigo" name="ga_codigo" value="{$smarty.post.ga_codigo|default:$daoSitio->ga_codigo}" maxlength="200" />
        <br />
        <br />
        - {#sitio_logErrors#}: <input type="checkbox" name="log_errors" {if ($smarty.post.action && $smarty.post.log_errors) || ($smarty.get.action == "edit" && $daoSitio->log_errors == "1")} checked="checked"{/if} />
        <br />
        - {#sitio_habilitado#}: <input type="checkbox" name="habilitado" {if ($smarty.post.action && $smarty.post.habilitado) || $smarty.get.action == "insert" || ($smarty.get.action == "edit" && $daoSitio->habilitado == "1")} checked="checked"{/if} />
        <br />
        - {#sitio_enMantenimiento#}: <input type="checkbox" name="mantenimiento" {if ($smarty.post.action && $smarty.post.mantenimiento) || $smarty.get.action == "insert" || ($smarty.get.action == "edit" && $daoSitio->mantenimiento == "1")} checked="checked"{/if} />
        <br />
        - {#sitio_creaContenidoYlistados#}: <input type="checkbox" name="crea_listados" {if ($smarty.post.action && $smarty.post.crea_listados) || $smarty.get.action == "insert" || ($smarty.get.action == "edit" && $daoSitio->crea_listados == "1")} checked="checked"{/if} />
        <br />
        <br />
        <br />

        <!-- ========================== SMTP =============================== -->
        <div class="title"><h1>{#sitio_smtpLabel#}</h1></div>
        {#sitio_smtpFromName#}:
        <br />
        <input type="text" class="text" id="smtp_fromname" name="smtp_fromname" value="{$smarty.post.smtp_fromname|default:$daoSitio->smtp_data.fromname}" />
        <br />
        <br />
        {#sitio_emailContacto#}:
        <br />
        <input type="text" class="text" id="email_contacto" name="email_contacto" value="{$smarty.post.email_contacto|default:$daoSitio->smtp_data.contacto}" />
        <br /> <span class="remind">(Recuerde que puede agregrar emails de contacto separándolos por coma)</span>
        <br /><br />
        {#sitio_smtpHost#}:
        <br />
        <input type="text" class="text" id="smtp_host" name="smtp_host" value="{$smarty.post.smtp_host|default:$daoSitio->smtp_data.host}" />
        <br />
        <br />
        {#sitio_smtpUsername#}:
        <br />
        <input type="text" class="text" id="smtp_username" name="smtp_username" value="{$smarty.post.smtp_username|default:$daoSitio->smtp_data.username}" />
        <br />
        <br />
        {#sitio_smtpPassword#}:
        <br />
        <input type="text" class="text" id="smtp_password" name="smtp_password" value="{$smarty.post.smtp_password|default:$daoSitio->smtp_data.password}" />
        <br />
        <br />
        {#sitio_smtpProtocol#}:
        <br />
        <input type="text" class="text" id="smtp_protocol" name="smtp_protocol" value="{$smarty.post.smtp_protocol|default:$daoSitio->smtp_data.protocol}" />
        <br />
        <br />
        {#sitio_smtpPort#}:
        <br />
        <input type="text" class="text" id="smtp_port" name="smtp_port" value="{$smarty.post.smtp_port|default:$daoSitio->smtp_data.port}" />
        <br />
        <br />
        <!-- ============================ SUBMIT =========================== -->
        <input type="submit" value="{$submitValue}" />
    </form>
    <script type="text/javascript">
    //<![CDATA[
        Inny_Sitio.init();
    //]]>
    </script>
</div>
{* ========================================================================== *}
{include file="footer.tpl"}