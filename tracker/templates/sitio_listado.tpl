{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="sitio"}
{config_load file="speech.conf" section="ctacorriente"}
{config_load file="speech.conf" section="filter"}
{config_load file="speech.conf" section="paginator"}
{* ========================================================================== *}
{tracker_get_tipo_usuario_logueado assign="tipo_usuario_logueado"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#sitio_listadoPageTitle#}
{dk_include file="styles/lister.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="js/dk.multiaction.js" inline=false}
{dk_include file="js/overlib.js" inline="false"}
{* ========================================================================== *}
{dk_daolister name="ls" table="sitio"}
    {dkf_declare
        name = "bns"
        type = "text"
        queryToApply = "nombre_sitio like '%@VALUE@%'"
    }
    {dkf_declare
        name = "bnu"
    	type = "text"
    	queryToApply = "nombre_encargado like '%@VALUE@%' or apellido like '%@VALUE@%'"
    }
    {dko_declare
    	name = "o_bi"
    	column = "id_sitio"
    }
    {dko_declare
    	name = "o_bn"
    	column = "nombre_sitio"
    }
    {dko_declare
    	name = "o_be"
    	onAsc = "apellido asc, nombre_encargado asc"
    	onDesc = "apellido desc, nombre_encargado desc"
    }

    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
        {dkma_declare
            url                  = "sitio_abm.php"
            enable 		         = "habilitar|�Desea habilitar todos los Sitios seleccionados?."
            disable              = "deshabilitar|�Desea deshabilitar todos los Sitios seleccionados?."
            maintenance    		 = "en mantenimiento|�Desea poner en mantenimiento todos los Sitios seleccionados?."
            no_maintenance 		 = "en produccion|�Desea poner en producci�n todos los Sitios seleccionados?."
            crealistados_enable  = "habilitar crear listados|�Desea permitir que todos los Sitios seleccionados puedan crear listados?."
            crealistados_disable = "deshabilitar crear listados|�Desea permitir que todos los Sitios NO puedan crear listados?."
            logerrors_enable     = "loguear errores de plugins|�Desea loguear los errores de los plugins de estos sitios?\n\n(recomendado para producci�n)"
            logerrors_disable    = "mostrar errores de plugins en pantalla|�Desea mostrar los errores de plugins en pantalla de estos sitios?\n\n(recomendado para desarrollo)"
        }
    {/if}

    <!-- ============================= FILTROS ============================= -->
    {dk_filters name="filtros"}
    <div class="filters">
    	<table cellpadding="2" cellspacing="2">
            <tbody>
                <tr>
                    <td>{#sitio_nombreSitio#}: {dkf_input name="bns"}</td>
                    <td>{#sitio_encargadoNyA#}: {dkf_input name="bnu"}</td>
                    <td style="text-align: right;"><input type="submit" class="submit" value="filtrar" /></td>
                </tr>
                {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                    <tr>
                    	<td colspan="2" style="text-align: left;">
                        	<a href="{dkma_url type="all"}" title="{#filter_all#}">{#filter_all#}</a>
                        	&nbsp;
                            <a href="{dkma_url type="none"}" title="{#filter_none#}">{#filter_none#}</a>
                            &nbsp;
                            <a href="{dkma_url type="invert"}" title="{#filter_invert#}">{#filter_invert#}</a>
                            &nbsp;
                            {dkma_actions}
                        </td>
                        <td style="text-align:right;"><a href="sitio_abm.php?action=insert" title="{#sitio_insert#}">{#sitio_insert#}</a></td>
                    </tr>
                {/if}
            </tbody>
	    </table>
    </div>
    {/dk_filters}

    <!-- ========================== LISTADO ================================ -->
    <br />
    {dkp_ini}
    {if $dkp_results > 0}
        <div class="paginatorHeader">{#paginator_results#}: <b>{$dkp_begin}</b> - <b>{$dkp_end}</b> de <b>{$dkp_results}</b></div>
        <table class="lister" cellpadding="2" cellspacing="1" style="text-align:left;">
            <thead>
                <tr class="header">

                    <!-- MULTIACTION -->
                    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                        <td>&nbsp;</td>
                    {/if}

                    <!-- ID DEL SITIO -->
                	<td class="id" style="width:55px;">
        				<span class="title">{#sitio_idSitio#}</span>
                        <span class="arrows">
                        	&nbsp;&nbsp;
                            <a href="{dko_url name="o_bi" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" title="{#common_orderDesc#}" alt="{#common_orderDesc#}" /></a>
                            <a href="{dko_url name="o_bi" order="asc"}" title="{#common_orderAsc#}"><img src="images/arrow_up.gif" title="{#common_orderAsc#}" alt="{#common_orderAsc#}" /></a>
                        </span>
                    </td>

                    <!-- NOMBRE DEL SITIO -->
                    <td class="order">
                        <span class="title">{#sitio_nombreSitio#}</span>
                        <span class="arrows">
                            <a href="{dko_url name="o_bn" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" title="{#common_orderDesc#}" alt="{#common_orderDesc#}" /></a>
                            <a href="{dko_url name="o_bn" order="asc"}" title="{#common_orderAsc#}"><img src="images/arrow_up.gif" title="{#common_orderAsc#}" alt="{#common_orderAsc#}" /></a>
                        </span>
                    </td>

                    <!-- PROPIETARIO DEL SITIO -->
                    <td class="order">
                        <span class="title">{#sitio_encargadoNyA#}</span>
                        <span class="arrows">
                            <a href="{dko_url name="o_be" method="asc" order="asc"}"><img src="images/arrow_up.gif" /></a>
                            <a href="{dko_url name="o_be" method="desc" order="desc"}"><img src="images/arrow_down.gif" /></a>
                        </span>
                    </td>

{*
                    <!-- TELEFONO DEL PROPIETARIO -->
                    <td><span class="title">{#sitio_telefono#}</span></td>

                    <!-- EMAIL DEL PROPIETARIO -->
                    <td><span class="title">{#sitio_email#}</span></td>
*}
                    <!-- ACCIONES -->
                    <td colspan="10">&nbsp;</td>

                </tr>
            </thead>
            <tbody>
            {dk_lister export="id_sitio,nombre_sitio,nombre_encargado,apellido,telefono,email,id_ctacorriente,habilitado,mantenimiento,crea_listados,log_errors"}
                {dkl_getdao assign="sitio"}
                {assign var="alertValue" value=$sitio->checkAlertValues()}
                <tr class="normal">
                    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                        <td>{dkma_checkbox}</td>
                    {/if}
    	            <td style="text-align:center;">{$id_sitio}</td>
                    <td>{$nombre_sitio}</td>
                    <td>{$apellido}, {$nombre_encargado}</td>
{*
                    <td>{$telefono}</td>
                    <td>{$email|default:#sitio_defaultEmail#}</td>
*}
                    <td class="icon">{if !empty($alertValue)}<img src="images/warning_inny.gif" onmouseover="return overlib('{$alertValue}');" onMouseOut="return nd();"/>{/if}</td>
                    <td class="icon"><a href="{dkc_url url="sitio_abm.php" action="view"}" title="{#common_view#}"><img src="images/view.gif" alt="{#common_view#}" /></a></td>
                    <td class="icon"><a href="{dkc_url url="sitio_abm.php" action="edit"}" title="{#common_edit#}"><img src="images/edit.gif" alt="{#common_edit#}" /></a></td>
                    <td class="icon"><a href="{dkc_url url="contenido_listado.php"}" title="{#sitio_contenidosLinkTitle#}"><img src="images/contenido.gif" alt="{#sitio_contenidos#}" /></a></td>
                    <td class="icon"><a href="{dkc_url url="listado_listado.php"}" title="{#sitio_listadosLinkTitle#}"><img src="images/producto.gif" alt="{#sitio_listados#}" /></a></td>
                    <td class="icon"><a href="{dkc_url url="metadata_abm.php"}" title="{#sitio_dynamicMetadataLinkTitle#}"><img src="images/metadata_edit.gif" alt="" /></a></td>

                    {* ESTO EST� COMENTADO HASTA QUE SE UNIFIQUE LA FUNCIONALIDAD CON LA SECCI�N CLIENTES *}
                    {* NO ELIMINAR !!! *}
                    {*
                    <td class="icon"><a href="{dkc_url url="contenido_listado.php"}" title="{#sitio_contenidoLinkTitle#}"><img src="images/contenido.gif" alt="{#sitio_contenidoLinkTitle#}" /></td>
                    <td class="icon"><a href="{dkc_url url="listado_listado.php"}" title="{#sitio_listadoLinkTitle#}"><img src="images/producto.gif" alt="{#sitio_listadoLinkTitle#}" /></td>
                    *}

                    <td class="icon"><a href="{dkc_url url="sw_sitio.php"}" title="{#sitio_seccionClientesLinkTitle#}"><img src="images/go_sw_web.png" alt="{#sitio_seccionClientesLinkTitle#}" /></td>
                    <td class="icon">
                        {if $crea_listados == "0"}
                            <a href="{dkc_url action="crealistados_enable" url="sitio_abm.php" confirmMessage=#sitio_crearListadosConfirm#|replace:"%nombre_sitio":$nombre_sitio}" title="{#sitio_crearListadosLinkTitle#}"><img src="images/lister_off.gif" alt="{#common_enable#}" /></a>
                        {else}
                            <a href="{dkc_url action="crealistados_disable" url="sitio_abm.php" confirmMessage=#sitio_noCrearListadosConfirm#|replace:"%nombre_sitio":$nombre_sitio}" title="{#sitio_noCrearListadosLinkTitle#}"><img src="images/lister_on.gif" alt="{#common_disable#}" /></a>
                        {/if}
                    </td>
                    <td class="icon">
                        {if $log_errors == "0"}
                            <a href="{dkc_url action="logerrors_enable" url="sitio_abm.php" confirmMessage=#sitio_logErrorsEnabledConfirm#|replace:"%nombre_sitio":$nombre_sitio}" title="{#sitio_logErrorsEnabledLinkTitle#}"><img src="images/logerror_disable.png" alt="{#common_disable#}" /></a>
                        {else}
                            <a href="{dkc_url action="logerrors_disable" url="sitio_abm.php" confirmMessage=#sitio_logErrorsDisabledConfirm#|replace:"%nombre_sitio":$nombre_sitio}" title="{#sitio_logErrorsDisabledLinkTitle#}"><img src="images/logerror_enable.png" alt="{#common_enable#}" /></a>
                        {/if}
                    </td>

                    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                        <td class="icon">
                            {if $habilitado == $smarty.const.SITIO_HABILITADO}
                                <a href="{dkc_url action="disable" url="sitio_abm.php" confirmMessage=#sitio_deshabilitarConfirm#|replace:"%nombre_sitio":$nombre_sitio}" title="{#sitio_habilitadoLinkTitle#}"><img src="images/enabled.gif" alt="{#common_enabled#}" /></a>
                            {else}
                                <a href="{dkc_url action="enable" url="sitio_abm.php" confirmMessage=#sitio_habilitarConfirm#|replace:"%nombre_sitio":$nombre_sitio}" title="{#sitio_deshabilitadoLinkTitle#}"><img src="images/disabled.gif" alt="{#common_disabled#}" /></a>
                            {/if}
                        </td>
                        <td class="icon">
                            {if $mantenimiento == $smarty.const.SITIO_EN_PRODUCCION}
                                <a href="{dkc_url action="maintenance" url="sitio_abm.php" confirmMessage=#sitio_mantenimientoConfirm#|replace:"%nombre_sitio":$nombre_sitio}" title="{#sitio_produccionLinkTitle#}"><img src="images/sin_mantenimiento.gif" alt="{#sitio_produccion#}" /></a>
                            {else}
                                <a href="{dkc_url action="no_maintenance" url="sitio_abm.php" confirmMessage=#sitio_mantenimientoConfirm#|replace:"%nombre_sitio":$nombre_sitio}" title="{#sitio_mantenimientoLinkTitle#}"><img src="images/mantenimiento.gif" alt="{#sitio_mantenimiento#}" /></a>
                            {/if}
                        </td>
                        <td class="icon"><a href="sitio_ctacorriente_view.php?id_ctacorriente={$id_ctacorriente}" title="{#sitio_ctacteLinkTitle#}"><img src="images/money.gif" alt="{#ctacorriente_label#}" /></a></td>
                    {/if}

                    {*
                    <td class="icon"><a href="{dkc_url url="sitio_abm.php" action="updatemetadata" confirmMessage=#sitio_updateMetadataConfirm#}" title="{#sitio_metadataUpdateLinkTitle#}"><img src="images/metadata.gif" alt="{#sitio_metadata#}" /></a></td>
                    *}
                </tr>
            {/dk_lister}
            </tbody>
        </table>
        {include file="paginador.tpl"}
    {else}
        <span class="listadoSinResultados">{#common_emptyList#}</span>
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl"}