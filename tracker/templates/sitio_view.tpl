{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="sitio"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#sitio_viewPageTitle# linkBack=$linkBack}
{dk_include file="styles/form.css" inline="false"}
{* ========================================================================== *}

<!-- ======================= DETALLES DEL SITIO =========================== =-->
<table class="formTable" cellpadding="2" cellspacing="1">
    </tbody>

    <!-- =================================================================== -->
    <tr class="header">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" >
                <tbody>
                    <tr>
                        <td width="33%" style="text-align:center;">{#sitio_idSitio#}</td>
                        <td width="34%" style="text-align:center;">{#sitio_nombreSitio#}</td>
                        <td width="33%" style="text-align:center;">{#sitio_encargadoNyA#}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr class="data">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td width="33%" style="text-align:center;">{$daoSitio->id_sitio}</td>
                        <td width="34%" style="text-align:center;">{$daoSitio->nombre_sitio}</td>
                        <td width="33%" style="text-align:center;">{$daoSitio->apellido}, {$daoSitio->nombre_encargado}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>

    <!-- =================================================================== -->
    <tr class="header">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" >
                <tbody>
                    <tr>
                        <td width="33%" style="text-align:center;">{#sitio_url#}</td>
                        <td width="34%" style="text-align:center;">{#sitio_username#}</td>
                        <td width="33%" style="text-align:center;">{#sitio_password#}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr class="data">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" >
                <tbody>
                    <tr>
                        <td width="33%" style="text-align:center;">{$daoSitio->url|default:#common_empty#}</td>
                        <td width="34%" style="text-align:center;">{$daoSitio->username}</td>
                        <td width="33%" style="text-align:center;">{$daoSitio->password}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>

    <!-- =================================================================== -->
    <tr class="header">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" >
                <tr>
                    <td width="33%" style="text-align:center;">{#sitio_telefono#}</td>
                    <td width="34%" style="text-align:center;">{#sitio_direccion#}</td>
                    <td width="33%" style="text-align:center;">{#sitio_email#}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="data">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="33%" style="text-align:center;">{$daoSitio->telefono}</td>
                    <td width="34%" style="text-align:center;">{$daoSitio->direccion}</td>
                    <td width="33%" style="text-align:center;">{$daoSitio->email|default:#common_empty#}</td>
                </tr>
            </table>
        </td>
    </tr>



    <!-- =================================================================== -->
    <tr class="header">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="33%" style="text-align:center;">{#sitio_ftp#}</td>
                    <td width="33%" style="text-align:center;">{#sitio_ftpUsername#}</td>
                    <td width="34%" style="text-align:center;">{#sitio_ftpPassword#}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="data">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="33%" style="text-align:center;">{$daoSitio->ftp|default:#common_empty#} </td>
                    <td width="33%" style="text-align:center;">{$daoSitio->ftp_username|default:#common_empty#}</td>
                    <td width="34%" style="text-align:center;">{$daoSitio->ftp_password|default:#common_empty#}</td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- =================================================================== -->
    <tr class="header">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" >
                <tbody>
                    <tr>
                        <td width="25%" style="text-align:center;">{#sitio_idiomaCMS#}</td>
                        <td width="25%" style="text-align:center;">{#sitio_pathCVS#}</td>
                        <td width="25%" style="text-align:center;">{#sitio_version#}</td>
                        <td width="25%" style="text-align:center;">{#sitio_gaCodigo#}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr class="data">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0" >
                <tbody>
                    <tr>
                        <td width="25%" style="text-align:center;">{$daoSitio->idioma_codigo}</td>
                        <td width="25%" style="text-align:center;">{$daoSitio->cvs}</td>
                        <td width="25%" style="text-align:center;">{$daoSitio->version}</td>
                        <td width="25%" style="text-align:center;">{$daoSitio->ga_codigo|default:#common_empty#}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>

    <!-- =================================================================== -->
    <tr class="header">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td width="25%" style="text-align:center;">{#sitio_logErrors#}</td>
                        <td width="25%" style="text-align:center;">{#sitio_habilitado#}</td>
                        <td width="25%" style="text-align:center;">{#sitio_enMantenimiento#}</td>
                        <td width="25%" style="text-align:center;">{#sitio_creaContenidoYlistados#}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr class="data">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td width="25%" style="text-align:center;">{if $daoSitio->log_errors == "1"}{#common_yes#}{else}No{/if}</td>
                        <td width="25%" style="text-align:center;">{if $daoSitio->habilitado == "1"}{#common_yes#}{else}No{/if}</td>
                        <td width="25%" style="text-align:center;">{if $daoSitio->mantenimiento == "1"}{#common_yes#}{else}No{/if}</td>
                        <td width="25%" style="text-align:center;">{if $daoSitio->crea_listados == "1"}{#common_yes#}{else}No{/if}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<br />
<br />

<!-- ================================ SMTP ============================= -->
<div class="title"><h1>{#sitio_smtpLabel#}</h1></div>
<table class="formTable" cellpadding="2" cellspacing="1">
    <tbody>
    <tr class="header">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td width="33%" style="text-align:center;">{#sitio_smtpFromName#}</td>
                        <td width="34%" style="text-align:center;">{#sitio_emailContacto#}</td>
                        <td width="33%" style="text-align:center;">{#sitio_smtpHost#}</td>

                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr class="data">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td width="33%" style="text-align:center;">{$daoSitio->smtp_data.fromname|default:#common_empty#}</td>
                        <td width="34%" style="text-align:center;">{$daoSitio->smtp_data.contacto|default:#common_empty#}</td>
                        <td width="33%" style="text-align:center;">{$daoSitio->smtp_data.host|default:#common_empty#}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>

    <tr class="header">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td width="33%" style="text-align:center;">{#sitio_smtpUsername#}</td>
                        <td width="34%" style="text-align:center;">{#sitio_smtpProtocol#}</td>
                        <td width="33%" style="text-align:center;">{#sitio_smtpPort#}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr class="data">
        <td colspan="3">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td width="33%" style="text-align:center;">{$daoSitio->smtp_data.username|default:#common_empty#}</td>
                        <td width="34%" style="text-align:center;">{$daoSitio->smtp_data.protocol|default:#common_empty#}</td>
                        <td width="33%" style="text-align:center;">{$daoSitio->smtp_data.port|default:#common_empty#}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<br />
<table class="formTable" cellpadding="2" cellspacing="1">
    <tbody>
        <tr class="link">
            <td>
                <a href="sitio_abm.php?action=edit&id_sitio={$daoSitio->id_sitio}" title="{#common_edit#}">{#common_edit#}</a>
            </td>
        </tr>
    </tbody>
</table>
{* ========================================================================== *}
{include file="footer.tpl"}