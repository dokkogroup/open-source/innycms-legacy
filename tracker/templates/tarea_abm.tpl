{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="tarea"}
{config_load file="speech.conf" section="estado"}
{config_load file="speech.conf" section="sitio"}
{* ========================================================================== *}
{assign var="id_tarea" value=$smarty.get.id_tarea|default:$smarty.post.id_tarea}
{assign var="action" value=$smarty.post.action|default:$smarty.get.action}
{assign var="daoSitio" value=$daoTarea->getSitio()}
{assign var="daoEmpleado" value=$daoTarea->getEmpleado()}
{assign var="daoEstado" value=$daoTarea->getEstado()}
{assign var="daoRenglon" value=$daoTarea->getRenglon()}
{if !empty($daoRenglon)}
    {assign var="monto" value=$daoRenglon->monto}
{else}
    {assign var="monto" value="0"}
{/if}
{assign var="daoTareaTemporal" value=$daoTarea->getTareaTemporal()}
{if $action == "add"}
    {assign var="pageTitle" value=#tarea_addPageTitle#}
    {assign var="submitValue" value=#common_add#}
{else}
    {assign var="pageTitle" value=#tarea_editPageTitle#}
    {assign var="submitValue" value=#common_edit#}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="js/common.js" inline=false}
{dk_include file="js/inny.comentario.js" inline=false}
{dk_include file="js/inny.tarea.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="error_messages.tpl"}
{/if}
{* ========================================================================== *}
<!-- DETALLES DE LA TAREA -->
<form id="tarea_abm" name="tarea_abm" method="post" action="tarea_abm.php" enctype="multipart/form-data" onsubmit="javascript:return Inny_Tarea.validate();">
    <input type="hidden" id="action" name="action" value="{$action}" />
    {if $action == "edit"}
        <input type="hidden" id="id_tarea" name="id_tarea" value="{$id_tarea}" />
    {/if}
    <table class="formTable" cellpadding="2" cellspacing="1">

        <!-- =============================================================== -->
        <tr class="header">
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                        <td width="25%">{#sitio_label#}</td>
                        <td width="25%">{#estado_label#}</td>
                        <td width="25%">{#tarea_asignadaA#}</td>
                        <td width="25%">{#tarea_monto#}</td>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr class="data">
                            <td style="width:25%; text-align:center;">
                                <select id="id_sitio" name="id_sitio">
                                    <option value="null" label="{#sitio_optionEscoger#}">{#sitio_optionEscoger#}</option>
                                    {inny_htmloptions_sitio selected=$smarty.post.id_sitio|default:$daoTarea->id_sitio}
                                </select>
                            </td>
                            <td style="width:25%; text-align:center;">
                                <select id="id_estado" name="id_estado">
                                    {inny_htmloptions_estado selected=$smarty.post.id_estado|default:$daoTarea->id_estado|default:$smarty.const.ESTADO_NOCOMENZADO}
                                </select>
                            </td>
                            <td style="width:25%; text-align:center;">
                                <select id="id_empleado" name="id_empleado">
                                    <option value="null" label="{#tarea_noAsignado#}">{#tarea_noAsignado#}</option>
                                    {inny_htmloptions_empleado selected=$smarty.post.id_empleado|default:$daoTarea->id_empleado}
                                </select>
                            </td>
                            <td style="width:25%; text-align:center;">
                                <input type="text" id="monto" name="monto" value="{$smarty.post.monto|default:$monto}" style="width:60px; font: 12px verdana,arial,tahoma; color:#000000;" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <!-- =============================================================== -->
        {if $action == "edit"}
            <tr class="header">
                <td>{#tarea_creacion#}</td>
                <td>{#tarea_fechaInicio#}</td>
                <td>{#tarea_fechaFin#}</td>
            </tr>
            <tr class="data">
                <td style="text-align: center;">
                    {if $daoTarea->fecha_creacion == "0000-00-00 00:00:00"}
                        {#tarea_fechaCreacionDefault#}
                    {else}
                        {$daoTarea->fecha_creacion}
                    {/if}
                </td>
                <td style="text-align: center;">
                    {if $daoTarea->fecha_inicio == "0000-00-00 00:00:00"}
                        {#tarea_fechaInicioDefault#}
                    {else}
                        {$daoTarea->fecha_inicio}
                    {/if}
                </td>
                <td style="text-align: center;">
                    {if $daoTarea->fecha_fin == "0000-00-00 00:00:00"}
                        {#tarea_fechaFinDefault#}
                    {else}
                        {$daoTarea->fecha_fin}
                    {/if}
                </td>
            </tr>
        {/if}

        <!-- =============================================================== -->
        <tr class="header">
            <td colspan="3">{#tarea_descripcion#}</td>
        </tr>
        <tr>
            <td colspan="3" class="wysiwyg" style="padding:10px; text-align:center;">
                <center>
                    {include file="wysiwyg.tpl" name="descripcion" value=$smarty.post.descripcion|default:$daoTarea->descripcion}
                </center>
            </td>
        </tr>

        <!-- =============================================================== -->
        <tr class="header">
            <td colspan="3">{#tarea_archivosAdjuntos#}</td>
        </tr>
        <tr class="data">
            <td colspan="3" style="padding:10px; text-align:left;">

                {* LINKS DE DESCARGA DE ARCHIVOS ADJUNTOS A LA TAREA *}
                {assign var="infoArchivosAdjuntos" value=$daoTarea->getInfoArchivos()}
                {if !empty($infoArchivosAdjuntos)}
                    {assign var="cantTemporal_actual" value=$infoArchivosAdjuntos|@count}
                    <ul>
                    {foreach from=$infoArchivosAdjuntos item="infoArchivoAdjunto"}
                        <li>
                            [ID {$infoArchivoAdjunto.id_temporal}]
                            <a href="download/{$infoArchivoAdjunto.id_temporal}/{$infoArchivoAdjunto.filename}" title="{#tarea_downloadLinkTitle#|replace:"%filename":$infoArchivoAdjunto.filename}">{$infoArchivoAdjunto.filename}</a>
                        </li>
                    {/foreach}
                    </ul>
                {else}
                    {assign var="cantTemporal_actual" value=0}
                {/if}

                {* UPLOAD DE ARCHIVOS RESTANTES *}
                {assign var="cantTemporal_max" value="6"}
                {assign var="cantTemporal_restante" value=$cantTemporal_max-$cantTemporal_actual}
                {section name="inputsFile" loop=$cantTemporal_restante}
                    {assign var="index" value=$smarty.section.inputsFile.index+$cantTemporal_actual+1}
                    {#tarea_archivo#|replace:"%nro":$index}: <input type="file" name="temporal_{$index}" />
                    <br />
                {/section}
            </td>
        </tr>

        <!-- =============================================================== -->

        <tr class="submit">
            <td colspan="3">
                <input type="submit" value="{$submitValue}" />
                {if $action == "edit"}
                    <input type="button" value="{#common_delete#}" onclick="javascript:confirmAction('{#tarea_confirmDelete#}','tarea_abm.php?action=delete&id_tarea={$daoTarea->id_tarea}');" />
                {/if}
                <input type="submit" value="{#common_cancel#}" onclick="javascript:window.location.href = '{$linkBack}';" />
            </td>
        </tr>

        <script type="text/javascript">
        //<[CDATA[
            Inny_Tarea.init();
        //]]>
        </script>
    </table>
</form>
{include file="footer.tpl"}