{config_load file="speech.conf" section="tarea"}
{config_load file="speech.conf" section="sitio"}
{config_load file="speech.conf" section="estado"}
{config_load file="speech.conf" section="empleado"}
{config_load file="speech.conf" section="paginator"}
{config_load file="speech.conf" section="filter"}
{* ========================================================================== *}
{if isset($daoEmpleadoLogueado)}
    {assign var="daoListerQuery" value="id_empleado = "|cat:$daoEmpleadoLogueado->id_empleado}
{else}
    {assign var="daoListerQuery" value=""}
{/if}
{tracker_get_tipo_usuario_logueado assign="tipo_usuario_logueado"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#tarea_listadoPageTitle#}
{dk_include file="styles/lister.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="jscalendar/skins/aqua/theme.css" ignoreVersion=true inline=false}
{dk_include file="jscalendar/calendar.js" ignoreVersion=true inline=false}
{dk_include file="jscalendar/lang/calendar-es.js" ignoreVersion=true inline=false}
{dk_include file="jscalendar/calendar-setup.js" ignoreVersion=true inline=false}
{* ========================================================================== *}
{dk_daolister name="tr" table="tarea" resultsPerPage="25" orderBy="fecha_creacion DESC" query=$daoListerQuery}
    {dkf_declare
        name = "bs"
        type = "select"
        table = "estado"
        display = "nombre"
    }
    {if !$id_logged}
        {dkf_declare
            name = "bn"
            type = "text"
            queryToApply = "id_empleado in (select id_empleado from empleado where username like '%@VALUE@%')"
        }
    {/if}
    {dkf_declare
        name = "bdf"
        type = "text"
        queryToApply = "fecha_creacion >= '@VALUE@ 00:00:00'"
    }
    {dkf_declare
        name = "bdt"
        type = "text"
        queryToApply = "fecha_creacion <= '@VALUE@ 23:59:59'"
    }
    {dkf_declare
        name = "bp"
        type = "select"
        table = "sitio"
        display = "nombre_sitio"
        orderBy = "nombre_sitio"
    }
    {dko_declare
        name = "odc"
        column = "fecha_creacion"
    }
    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
        {dkma_declare
            url = "tarea_abm.php"
            multidelete = #common_delete#|cat:"|"|cat:#tarea_confirmMultiDelete#
        }
    {/if}
    {dk_filters name="filters"}
    <div class="projectSelector">
        {#sitio_label#}&nbsp;&nbsp;{dkf_input name="bp" autoSubmit="true" showAllText=#sitio_optionAll#}
    </div>
    <div class="filters">
        <table cellpadding="2" cellspacing="2">
            <tbody>
                <tr>
                    <td>{#estado_label#}:&nbsp;{dkf_input name="bs" showAllText=#estado_optionAll#}</td>
                    {if !$id_logged}
                        <td>{#empleado_username#}:&nbsp;{dkf_input name="bn" class="text"}</td>
                    {/if}
                    <td>
                        {#tarea_creacion#}:
                        &nbsp;
                        {dkf_input name="bdf" id="dkf_tr_bdf" class="text"}&nbsp;<input type="button" id="bdf_trigger" value="..." class="button" />
                        &nbsp;
                        {dkf_input name="bdt" id="dkf_tr_bdt" class="text"}&nbsp;<input type="button" id="bdt_trigger" value="..." class="button" />
                    </td>
                    {dhtml_calendar ifFormat="%Y-%m-%d" inputField="dkf_tr_bdf" button="bdf_trigger" singleClick=true}
                    {dhtml_calendar ifFormat="%Y-%m-%d" inputField="dkf_tr_bdt" button="bdt_trigger" singleClick=true}
                    <td align="right"><input type="submit" class="submit" value="{#filter_submit#}"  /></td>
                </tr>
                {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                <tr>
                	<td colspan="2" style="text-align: left;">
                        {#filter_select#}:
                        &nbsp;
                    	<a href="{dkma_url type="all"}" title="{#filter_all#}">{#filter_all#}</a>
                    	&nbsp;
                        <a href="{dkma_url type="none"}" title="{#filter_none#}">{#filter_none#}</a>
                        &nbsp;
                        <a href="{dkma_url type="invert"}" title="{#filter_invert#}">{#filter_invert#}</a>
                        &nbsp;
                        {dkma_actions}
                    </td>
                </tr>
                {/if}
            </tbody>
        </table>
    </div>
    {/dk_filters}
    <br />
    {dkp_ini}
    {if $dkp_results > 0}
        <div class="paginatorHeader">{#paginator_results#}: <b>{$dkp_begin}</b> - <b>{$dkp_end}</b> de <b>{$dkp_results}</b></div>
        <table class="lister" cellpadding="2" cellspacing="1">
            <thead>
                <tr class="header">
                    {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                        <td>&nbsp;</td>
                    {/if}
                    <td>{#estado_label#}</td>
                    <td>{#empleado_username#}</td>
                    <td>{#tarea_descripcion#}</td>
                    <td class="order">
                        <span class="title">{#tarea_creacion#}</span>
                        <span class="arrows">
                            <a href="{dko_url name="odc" order="desc"}" title="{#common_orderDesc#}"><img src="images/arrow_down.gif" alt="{#common_orderDesc#}" /></a>
                            <a href="{dko_url name="odc" order="asc"}" title="{#common_orderAsc#}"><img src="images/arrow_up.gif" alt="{#common_orderAsc#}" /></a>
                        </span>
                    </td>
                    <td {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}colspan="3"{/if}>&nbsp;</td>
                </tr>
            </thead>
            <tbody>
                {dk_lister export="id_tarea,id_sitio,id_estado,id_empleado,fecha_creacion,descripcion"}
                    {dkl_getdao assign="daoTarea"}
                    {assign var="daoEmpleado" value=$daoTarea->getEmpleado()}
                    {assign var="daoEstado" value=$daoTarea->getEstado()}
                    {if $daoTarea->id_estado == $smarty.const.ESTADO_NOCOMENZADO}
                        {assign var="rowClass" value="new"}
                    {elseif $daoTarea->id_estado == $smarty.const.ESTADO_ENPROCESO}
                        {assign var="rowClass" value="inProcess"}
                    {elseif $daoTarea->id_estado == $smarty.const.ESTADO_FINALIZADO}
                        {assign var="rowClass" value="finalized"}
                    {elseif $daoTarea->id_estado == $smarty.const.ESTADO_ENPAUSA}
                        {assign var="rowClass" value="standby"}
                    {elseif $daoTarea->id_estado == $smarty.const.ESTADO_ANULADO}
                        {assign var="rowClass" value="cancelled"}
                    {/if}
                    <tr class="{$rowClass}">
                        {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                            <td class="icon" style="padding-top:10px;">{dkma_checkbox}</td>
                        {/if}
                        <td>{$daoEstado->nombre}</td>
                        <td>
                            {if $daoEmpleado->username}
                                {$daoEmpleado->username}
                            {else}
                                <b>{#tarea_noAsignado#}</b>
                            {/if}
                        </td>
                        <td style="text-align:left; padding: 0px 5px 0px 5px;">{$descripcion|strip_tags|truncate:60}</td>
                        <td style="text-align:center;">{$fecha_creacion|date_format:"%Y-%m-%d"}</td>
                        <td class="icon"><a href="{dkc_url url="tarea_abm.php" action="view" }" title="{#tarea_viewLinkTitle#}"><img src="images/view.gif" alt="{#common_view#}" /></td>
                        {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                            <td class="icon"><a href="{dkc_url url="tarea_abm.php" action="edit" }" title="{#tarea_editLinkTitle#}"><img src="images/edit.gif" alt="{#common_edit#}" /></td>
                            <td class="icon"><a href="{dkc_url url="tarea_abm.php" action="delete" confirmMessage=#tarea_confirmDelete#}" title="{#tarea_deleteLinkTitle#}"><img src="images/delete.gif" alt="{#common_delete#}" /></td>
                        {/if}
                    </tr>
                {/dk_lister}
            </tbody>
        </table>
        {include file="paginador.tpl"}
    {else}
        <span class="listadoSinResultados">{#common_emptyList#}</span>
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl"}