{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="tarea"}
{config_load file="speech.conf" section="estado"}
{config_load file="speech.conf" section="sitio"}
{config_load file="speech.conf" section="renglon"}
{config_load file="speech.conf" section="comentario"}
{* ========================================================================== *}
{tracker_get_dao_empleado_logueado assign="daoLoggedUser"}
{tracker_get_tipo_usuario_logueado assign="tipo_usuario_logueado"}
{inny_get_estados assign="estados"}
{inny_get_empleados assign="empleados" addNullOption=#tarea_noAsignado#}
{* ========================================================================== *}
{assign var="id_tarea" value=$smarty.get.id_tarea|default:$smarty.post.id_tarea}
{assign var="daoSitio" value=$daoTarea->getSitio()}
{assign var="daoEmpleado" value=$daoTarea->getEmpleado()}
{assign var="daoEstado" value=$daoTarea->getEstado()}
{assign var="daoTareaTemporal" value=$daoTarea->getTareaTemporal()}
{assign var="daoRenglon" value=$daoTarea->getRenglon()}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#tarea_viewPageTitle# linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="js/common.js" inline=false}
{dk_include file="js/inny.comentario.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="error_messages.tpl"}
{/if}
{* ========================================================================== *}
<!-- DETALLES DE LA TAREA -->
<form name="tarea" method="post" action="tarea_abm.php">
    <input type="hidden" name="id_tarea" value="{$id_tarea}" />
    <input type="hidden" name="action" value="view" />
    <table class="formTable" cellpadding="2" cellspacing="1">

        <!-- =============================================================== -->
        <tr class="header">
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                        <td width="25%">{#sitio_label#}</td>
                        <td width="25%">{#estado_label#}</td>
                        <td width="25%">{#tarea_asignadaA#}</td>
                        <td width="25%">{#tarea_monto#}</td>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr class="data">
                            <td style="width:25%; text-align:center;">{$daoSitio->nombre_sitio}</td>
                            <td style="width:25%; text-align:center;">
                                {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN || empty($daoTarea->id_empleado) || $daoTarea->id_empleado != $daoLoggedUser->id_empleado}
                                    {$daoEstado->nombre}
                                {else}
                                    {include file="link_to_select.tpl" id_link="div_linkestado" id_select="div_estado" link_text=$daoEstado->nombre selectName="id_estado" options=$estados selected=$daoTarea->id_estado}
                                {/if}
                            </td>
                            <td style="width:25%; text-align:center;">
                                {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                                    {$daoEmpleado->username|default:#tarea_noAsignado#}
                                {else}
                                    {if !empty($daoTarea->id_empleado) && $daoTarea->id_empleado != "null"}
                                        {assign var="link_text" value=$daoEmpleado->username}
                                    {else}
                                        {assign var="link_text" value=#tarea_noAsignado#}
                                    {/if}
                                    {include file="link_to_select.tpl" id_link="div_linkasignar" id_select="div_asignar" link_text=$link_text selectName="id_empleado" options=$empleados selected=$daoTarea->id_empleado}
                                {/if}
                            </td>
                            <td style="width:25%; text-align:center;">
                                {if $tipo_usuario_logueado == $smarty.const.USUARIO_EMPLEADO && $daoTarea->id_empleado == $daoLoggedUser->id_empleado && $daoEstado->id_estado == $smarty.const.ESTADO_NOCOMENZADO}
                                    <input type="text" name="monto" value="{$smarty.post.monto|default:$daoRenglon->monto}" style="width:60px; font: 12px verdana,arial,tahoma; color:#000000;" />
                                {else}
                                    {if !empty($daoRenglon)}{$daoRenglon->monto}{else}{#renglon_sinMonto#}{/if}
                                {/if}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>

        <!-- =============================================================== -->
        <tr class="header">
            <td>{#tarea_creacion#}</td>
            <td>{#tarea_fechaInicio#}</td>
            <td>{#tarea_fechaFin#}</td>
        </tr>
        <tr class="data">
            <td style="text-align: center;">{$daoTarea->fecha_creacion}</td>
            <td style="text-align: center;">
                {if $daoTarea->fecha_inicio == "0000-00-00 00:00:00"}
                    {#tarea_fechaInicioDefault#}
                {else}
                    {$daoTarea->fecha_inicio}
                {/if}
            </td>
            <td style="text-align: center;">
                {if $daoTarea->fecha_fin == "0000-00-00 00:00:00"}
                    {#tarea_fechaFinDefault#}
                {else}
                    {$daoTarea->fecha_fin}
                {/if}
            </td>
        </tr>

        <!-- =============================================================== -->
        <tr class="header">
            <td colspan="3">{#tarea_descripcion#}</td>
        </tr>
        <tr>
            <td colspan="3" class="wysiwyg" style="padding:10px; text-align:left;">{$daoTarea->descripcion}</td>
        </tr>

        <!-- =============================================================== -->
        {if $daoTareaTemporal}
            <tr class="header">
                <td colspan="3">{#tarea_archivosAdjuntos#}</td>
            </tr>
            <tr class="data">
                <td colspan="3" style="padding:10px; text-align:left;">
                    {assign var="infoArchivosAdjuntos" value=$daoTarea->getInfoArchivos()}
                    {if !empty($infoArchivosAdjuntos)}
                        <ul>
                        {foreach from=$infoArchivosAdjuntos item="infoArchivoAdjunto"}
                            <li>
                                [ID {$infoArchivoAdjunto.id_temporal}]
                                <a href="download/{$infoArchivoAdjunto.id_temporal}/{$infoArchivoAdjunto.filename}" title="{#tarea_downloadLinkTitle#|replace:"%filename":$infoArchivoAdjunto.filename}">{$infoArchivoAdjunto.filename}</a>
                            </li>
                        {/foreach}
                        </ul>
                    {/if}
                </td>
            </tr>
        {/if}

        <!-- =============================================================== -->
        <tr class="submit">
            <td colspan="3">
                {if $tipo_usuario_logueado == $smarty.const.USUARIO_ADMIN}
                    <input type="button" value="{#common_edit#}" onclick="javascript:window.location.href='tarea_abm.php?action=edit&id_tarea={$id_tarea}';" />
                    <input type="button" value="{#common_delete#}" onclick="javascript:confirmAction('{#tarea_confirmDelete#}','tarea_abm.php?action=delete&id_tarea={$daoTarea->id_tarea}');" />
                {else}
                    <input type="submit" value="{#common_ok#}" />
                {/if}
                <input type="button" value="{#common_back#}" onclick="javascript:window.location.href='{$linkBack}';" />
            </td>
        </tr>
    </table>
</form>

<!-- ========================= DETALLES DEL SITIO ========================== -->
<br />
<br />
<div class="title"><h1>{#sitio_viewPageTitle#}</h1></div>
<table class="formTable" cellpadding="2" cellspacing="1">
    <tr class="header">
        <td>{#sitio_host#}</td>
        <td>{#sitio_nya#}</td>
        <td>{#sitio_telefono#}</td>
        <td>{#sitio_email#}</td>
    </tr>
    <tr class="data">
        <td style="text-align:center;">{$daoSitio->nombre_sitio}</td>
        <td style="text-align:center;">{$daoSitio->apellido}, {$daoSitio->nombre_encargado}</td>
        <td style="text-align:center;">{$daoSitio->telefono}</td>
        <td style="text-align:center;">{$daoSitio->email}</td>
    </tr>
    <tr class="header">
        <td>{#sitio_ftp#}</td>
        <td>{#sitio_ftpUsername#}</td>
        <td>{#sitio_ftpPassword#}</td>
        <td>{#sitio_cvs#}</td>
    </tr>
    <tr class="data">
        <td style="text-align:center;">{$daoSitio->ftp|default:#common_empty#}</td>
        <td style="text-align:center;">{$daoSitio->ftp_username|default:#common_empty#}</td>
        <td style="text-align:center;">{$daoSitio->ftp_password|default:#common_empty#}</td>
        <td style="text-align:center;">{$daoSitio->cvs}</td>
    </tr>
</table>

<!-- ============================ COMENTARIOS ============================== -->
<br />
<br />
<div class="title"><h1>{#comentario_listadoPageTitle#}</h1></div>
<form action="tarea_abm.php" method="post" onsubmit="javascript:return Inny_Comentario.validate();">
    <input type="hidden" name="id_tarea" value="{$id_tarea}" />
    <input type="hidden" name="action" value="addcomentario" />
    <table class="formTable" cellpadding="2" cellspacing="1">
        <tbody>
            {capture assign="comentarios"}
                {dk_daolister name="comm" table="comentario" query="id_tarea = "|cat:$id_tarea orderBy="fecha"}
                    {dkp_ini}
                        {assign var="comentarios_cantidad" value=$dkp_results}
                    {/dkp_ini}
                    {dk_lister export="autor,fecha,texto"}
                    <tr class="header">
                        <td class="author">{$autor}</td>
                        <td class="date">{$fecha}</td>
                    </tr>
                    <tr>
                        <td class="wysiwyg" colspan="2" style="text-align: left;">{$texto}</td>
                    </tr>
                    {/dk_lister}
                {/dk_daolister}
            {/capture}
            {if $comentarios|count_characters > 0}
                {$comentarios}
            {else}
                <tr class="data">
                    <td colspan="2" style="text-align: left;">{#comentario_sinComentarios#}</td>
                </tr>
            {/if}
            <tr class="header">
                <td colspan="2">{#comentario_wysiwygTitle#}</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; padding:20px;">
                    {include file="wysiwyg.tpl" name="comentario" value=$smarty.post.comentario}
                </td>
            </tr>
            <tr class="submit">
                <td {if !empty($comentarios_cantidad) && $comentarios_cantidad > 0}colspan="2"{/if}>
                    <input type="submit" value="{#comentario_submitValue#}" />
                    <input type="button" value="{#common_back#}" onclick="javascript:window.location.href='{$linkBack}';" />
                </td>
            </tr>
        </tbody>
    </table>
</form>
{include file="footer.tpl"}