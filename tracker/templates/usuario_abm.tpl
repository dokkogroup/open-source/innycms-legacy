{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="password"}
{config_load file="speech.conf" section="usuario"}
{config_load file="speech.conf" section="sitio"}
{* ========================================================================== *}
{assign var="id_empleado" value=$smarty.post.id_usuario|default:$daoUsuario->id_usuario}
{assign var="action" value=$smarty.get.action|default:$smarty.post.action}
{if $action == "insert"}
    {assign var="pageTitle" value=#usuario_insertPageTitle#}
    {assign var="submitValue" value=#common_add#}
{else}
    {assign var="pageTitle" value=#usuario_updatePageTitle#}
    {assign var="submitValue" value=#common_edit#}
{/if}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=$pageTitle linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{dk_include file="js/inny.usuario.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{* ========================================================================== *}
<div class="form">
    <form method="post" action="usuario_abm.php" onsubmit="javascript:return Inny_Usuario.validate();">
        <input type="hidden" id="action" name="action" value="{$action}" />
        {if $action == "edit"}
            <input type="hidden" id="id_usuario" name="id_usuario" value="{$daoUsuario->id_usuario}" />
        {/if}
        {#usuario_username#}:
        <br />
        <input type="text" class="text" id="username" name="username" value="{$smarty.post.username|default:$daoUsuario->username}" />
        <br />
        <br />
        {#usuario_password#}:
        <br />
        {if $action == "edit"}
            <a href="usuario_abm.php?action=changepass&id_usuario={$daoUsuario->id_usuario}" title="{#password_change#}">{#password_change#}</a>
        {else}
            <input type="text" class="text" id="password" name="password" value="{$smarty.post.password|default:$daoUsuario->password}" />
        {/if}
        <br />
        <br />
        {#sitio_label#}:
        <br />
        <select id="id_sitio" name="id_sitio">
	        {if $action == "add"}
        		<option value="null" label="{#sitio_optionEscoger#}">{#sitio_optionEscoger#}</option>
        	{/if}
	        {dk_daolister name="sitio" table="sitio" orderBy="nombre_sitio"}
	        	{dk_lister export="id_sitio,nombre_sitio"}
	        		{if $action == "edit"}
				    	<option value="{$id_sitio}" label="{$nombre_sitio}"{if $daoUsuario->id_sitio == $id_sitio} selected="selected"{/if}>{$nombre_sitio}</option>
				    {else}
				    	<option value="{$id_sitio}" label="{$nombre_sitio}">{$nombre_sitio}</option>
				    {/if}
			    {/dk_lister}
	        {/dk_daolister}
	    </select>
        <br />
        <br />
        {#usuario_apellido#}:
        <br />
        <input type="text" class="text" id="apellido" name="apellido" value="{$smarty.post.apellido|default:$daoUsuario->apellido}" />
        <br />
        <br />
        {#usuario_nombre#}:
        <br />
        <input type="text" class="text" id="nombre" name="nombre" value="{$smarty.post.nombre|default:$daoUsuario->nombre}" />
        <br />
        <br />
        {#usuario_email#}:
        <br />
        <input type="text" class="text" id="email" name="email" value="{$smarty.post.email|default:$daoUsuario->email}" />
        <br />
        <br />
        {#usuario_telefono#}:
        <br />
        <input type="text" class="text" id="telefono" name="telefono" value="{$smarty.post.telefono|default:$daoUsuario->telefono}" />
        <br />
        <br />
        {#usuario_habilitado#}: <input type="checkbox" name="habilitado" {if ($smarty.post.action && $smarty.post.habilitado) || $smarty.get.action == "insert" || ($smarty.get.action == "edit" && $daoUsuario->habilitado == "1")} checked="checked"{/if} />
        <br />
        <br />
        <input type="submit" value="{$submitValue}" />
    </form>
    <script type="text/javascript">
    //<![CDATA[
        Inny_Usuario.init();
    //]]>
    </script>
</div>
{* ========================================================================== *}
{include file="footer.tpl"}