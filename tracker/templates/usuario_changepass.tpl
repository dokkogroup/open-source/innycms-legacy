{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="usuario"}
{config_load file="speech.conf" section="password"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#password_change# linkBack="usuario_abm.php?action=view&id_usuario="|cat:$daoUsuario->id_usuario}
{dk_include file="styles/form.css" inline=false}
{dk_include file="js/inny.password.js" inline=false}
{* ========================================================================== *}
{dk_hasmessages type="ERROR" assign="hasErrorMessages"}
{if $hasErrorMessages}
    {include file="messagebox_errormessages.tpl"}
{/if}
{if $smarty.get.change == "ok"}
    {include file="messagebox_okmessages.tpl"}
{/if}
{* ========================================================================== *}
<div class="form">
    <form action="usuario_abm.php" method="post" onsubmit="javascript: return Inny_Password.validate();">
        <input type="hidden" name="id_usuario" value="{$smarty.get.id_usuario|default:$smarty.post.id_usuario}" />
        <input type="hidden" name="action" value="changepass" />
        <br />
        {#usuario_nya#}: <b>{$daoUsuario->apellido}, {$daoUsuario->nombre}</b>
        <br />
        <br />
        {#password_newPassword#}:
        <br />
        <input type="password" id="newpassword" name="newpassword" maxlength="40" />
        <br />
        <br />
        {#password_confirmNewPass#}:
        <br />
        <input type="password" id="confirmnewpass" name="confirmnewpass" maxlength="40" />
        <br />
        <br />
        <input type="submit" value="{#password_change#}" />
    </form>
    <script type="text/javascript">
    //<![CDATA[
        Inny_Password.init();
    //]]>
    </script>
</div>
{* ========================================================================== *}
{include file="footer.tpl"}