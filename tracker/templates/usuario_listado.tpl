{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="usuario"}
{config_load file="speech.conf" section="sitio"}

{config_load file="speech.conf" section="filter"}
{config_load file="speech.conf" section="paginator"}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#usuario_listadoPageTitle#}
{dk_include file="styles/lister.css" inline=false}
{dk_include file="js/dk.common.js" inline=false}
{* ========================================================================== *}
{dk_daolister name="usuario" table="usuario" resultsPerPage="25"}
    {dkf_declare
        name = "bus"
        type = "text"
        queryToApply = "username like '%@VALUE@%'"
    }
    {dkf_declare
        name = "bna"
        type = "text"
        queryToApply = "nombre like '%@VALUE@%' or apellido like '%@VALUE@%'"
    }
    {dkf_declare
        name = "bsit"
        type = "select"
        table= "sitio"
        display= "nombre_sitio"
    }

    {dko_declare
        name = "order_by_us"
        column = "username"
    }
    {dko_declare
        name = "order_by_apynom"
        onAsc = "apellido asc, nombre asc"
        onDesc = "apellido desc, nombre desc"
    }

    <!-- =========================== FILTROS =============================== -->
    {dk_filters name="filtros"}
        <div class="filters">
            <table cellpadding="2" cellspacing="2">
                <tbody>
                    <tr>
                        <td>{#usuario_username#}: {dkf_input name="bus"}</td>
                        <td>{#usuario_nya#}: {dkf_input name="bna"}</td>
                        <td>{#sitio_label#}: {dkf_input name="bsit" showAllText=#sitio_optionAll#}</td>
                        <td style="text-align:right;"><input type="submit" class="submit" value="{#filter_submit#}" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align:right;"><a href="usuario_abm.php?action=insert" title="{#usuario_insert#}">{#usuario_insert#}</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    {/dk_filters}
    <br />
    {dkp_ini}
    {if $dkp_results > 0}
        <div class="paginatorHeader">{#paginator_results#}: <b>{$dkp_begin} - {$dkp_end}</b> de <b>{$dkp_results}</b></div>
        <table class="lister" cellpadding="2" cellspacing="1" style="text-align:left;">
            <tr class="header">
                <td>
                    <span class="title">{#usuario_id#}</span>
                </td>
                <td class="order">
                    <span class="title">{#usuario_username#}</span>
                    <span class="arrows">
                        <a href="{dko_url name="order_by_us" order="asc"}"><img src="./images/arrow_up.gif" border="0" /></a>
                        <a href="{dko_url name="order_by_us" order="desc"}"><img src="./images/arrow_down.gif" border="0" /></a>
                    </span>
                </td>
                <td class="order">
                    <span class="title">{#usuario_nya#}</span>
                    <span class="arrows">
                        <a href="{dko_url name="order_by_apynom" order="asc"}"><img src="./images/arrow_up.gif" border="0" /></a>
                        <a href="{dko_url name="order_by_apynom" order="desc"}"><img src="./images/arrow_down.gif" border="0" /></a>
                    </span>
                </td>
                <td>{#usuario_email#}</td>
                <td colspan="4">&nbsp;</td>
            </tr>
            {dk_lister export="id_usuario,username,nombre,apellido,email,id_sitio,habilitado"}
                {if $habilitado == $smarty.const.USUARIO_HABILITADO}
                    {assign var="rowClass" value="enabled"}
                {else}
                    {assign var="rowClass" value="disabled"}
                {/if}
                <tr class="normal">
                    <td>{$id_usuario}</td>
                    <td>{$username}</td>
                    <td>{$apellido}, {$nombre}</td>
                    <td>{$email}</td>
                    <td class="icon"><a href="{dkc_url url="usuario_abm.php" action="view"}" title="{#common_view#}"><img src="images/view.gif" alt="{#common_view#}" /></a></td>
                    <td class="icon"><a href="{dkc_url url="usuario_abm.php" action="edit"}" title="{#common_edit#}"><img src="images/edit.gif" alt="{#common_edit#}" /></a></td>
                    <td class="icon">
                        {if $habilitado == $smarty.const.USUARIO_HABILITADO}
                            <a href="{dkc_url url="usuario_abm.php" action="disable" confirmMessage=#usuario_deshabilitarConfirm#|replace:"%username":$username}" title="{#usuario_deshabilitarLinkTitle#}"><img src="images/enabled.gif" alt="{#common_disable#}" /></a>
                        {else}
                            <a href="{dkc_url url="usuario_abm.php" action="enable" confirmMessage=#usuario_habilitarConfirm#|replace:"%username":$username}" title="{#usuario_deshabilitarLinkTitle#}"><img src="images/disabled.gif" alt="{#common_enable#}" /></a>
                        {/if}
                    </td>
                </tr>
            {/dk_lister}
        </table>
        <br />
        {include file="paginador.tpl"}
    {else}
        <span class="listadoSinResultados">{#common_emptyList#}</span>
    {/if}
    {/dkp_ini}
{/dk_daolister}
{* ========================================================================== *}
{include file="footer.tpl"}