{config_load file="speech.conf" section="common"}
{config_load file="speech.conf" section="password"}
{config_load file="speech.conf" section="usuario"}
{config_load file="speech.conf" section="sitio"}
{* ========================================================================== *}
{assign var="daoSitio" value=$daoUsuario->getDaoSitio()}
{* ========================================================================== *}
{include file="header.tpl" pageTitle=#usuario_viewPageTitle# linkBack=$linkBack}
{dk_include file="styles/form.css" inline=false}
{* ========================================================================== *}
<table class="formTable" cellpadding="2" cellspacing="1">
    <tbody>

        <!-- =============================================================== -->
        <tr class="header">
            <td style="width:25%; text-align:center;">{#usuario_id#}</td>
            <td style="width:25%; text-align:center;">{#usuario_username#}</td>
            <td style="width:25%; text-align:center;">{#password_label#}</td>
            <td style="width:25%; text-align:center;">{#sitio_label#}</td>
        </tr>
        <tr class="data">
            <td style="width:25%; text-align:center;">{$daoUsuario->id_usuario}</td>
            <td style="width:25%; text-align:center;">{$daoUsuario->username}</td>
            <td style="width:25%; text-align:center;"><a href="usuario_abm.php?action=changepass&id_usuario={$daoUsuario->id_usuario}" title="{#password_change#}">{#password_change#}</a></td>
            <td style="width:25%; text-align:center;">{$daoSitio->nombre_sitio}</td>
        </tr>

        <!-- =============================================================== -->
        <tr class="header">
            <td style="width:25%; text-align:center;">{#usuario_apellido#}</td>
            <td style="width:25%; text-align:center;">{#usuario_nombre#}</td>
            <td style="width:25%; text-align:center;">{#usuario_email#}</td>
            <td style="width:25%; text-align:center;">{#usuario_telefono#}</td>

        </tr>
        <tr class="data">
            <td style="width:25%; text-align:center;">{$daoUsuario->apellido}</td>
            <td style="width:25%; text-align:center;">{$daoUsuario->nombre}</td>
            <td style="width:25%; text-align:center;">{$daoUsuario->email}</td>
            <td style="width:25%; text-align:center;">{$daoUsuario->telefono}</td>
        </tr>
    </tbody>

    <!-- =================================================================== -->
    <tfoot>
        <tr class="link">
            <td colspan="4"><a href="usuario_abm.php?action=edit&id_usuario={$daoUsuario->id_usuario}" title="{#common_edit#}">{#common_edit#}</a></td>
        </tr>
    </tfoot>
</table>

{* ========================================================================== *}
{include file="footer.tpl"}