{*
Obligatorios:
    - name: nombre del editor

Opcionales:
    - value: contenido por defecto:
    - width: ancho del editor
    - height: alto del editor
    - onLoadEvent: evento en el onload (no me acuerdo para que estaba... igual es raro que se use)
*}
{dk_include file="wysiwyg/scripts/wysiwyg.js" ignoreVersion=true inline=false}
{dk_include file="wysiwyg/scripts/wysiwyg.css" media="screen" ignoreVersion=true inline=false}
<script type="text/javascript">
//<![CDATA[
    {if $width || $height || $onLoadEvent}
        var settings = new WYSIWYG.Settings();
        {if $width}
            settings.Width = "{$width}px";
        {/if}
        {if $height}
            settings.Height = "{$height}px";
        {/if}
        {if $onLoadEvent}
            settings.onLoadEvent = "{$onLoadEvent}";
        {/if}
        WYSIWYG.attach('{$name}',settings);
    {else}
        WYSIWYG.attach('{$name}');
    {/if}
//]]>
</script>
<textarea id="{$name}" name="{$name}">{$value}</textarea>