<?php
/**
 * Project: Inny Tracker
 * File: perfil.php
 *
 * @copyright 2007-2009 Dokko Group
 * @author Dokko Group <info at dokkogroup dot com dot ar>
 */
require_once 'common.php';
################################################################################
function validarUsuarioPassword(&$smarty,&$daoUsuario){

    #
    Inny_Tracker::verificarCambiarPassword($smarty);

    # En caso que est� todo OK
    if(!Denko::hasErrorMessages()){
        $daoUsuario->password = $_POST['confirmnewpass'];
        $daoUsuario->update();
        Denko::redirect('usuario_abm.php?action=view&id_usuario='.$_POST['id_usuario'].'&change=ok');
    }

    return false;
}
################################################################################
function validateUsuarioData(&$smarty,&$daoUsuario){

    # Obtengo la acci�n
    $action = $_POST['action'];

    # Cargo los mensajes de error
    $smarty->config_load('speech.conf','error');
	$smarty->config_load('speech.conf','usuario');
	$smarty->config_load('speech.conf','sitio');

    # Verifico que el valor en el campo 'username' no sea nulo
    $username = !empty($_POST['username']) ? trim($_POST['username']) : '';
    if($username == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'usuario_username'));
    }

    # Verifico que el valor en el campo 'password' no sea nulo
    if($action == 'insert'){
        $password = !empty($_POST['password']) ? $_POST['password'] : '';
        if($password == ''){
            Denko::addErrorMessage('error_requiredField',array('%field' => 'usuario_password'));
        }
    }

    # Verifico que el Sitio sea v�lido
    $id_sitio = $_POST['id_sitio'];
    if($id_sitio == 'null'){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'sitio_label'));
    }

    # Verifico que el valor en el campo 'apellido' no sea nulo
    $apellido = !empty($_POST['apellido']) ? trim($_POST['apellido']) : '';
	if($apellido == ''){
		Denko::addErrorMessage('error_requiredField',array('%field' => 'usuario_apellido'));
	}

    # Verifico que el valor en el campo 'nombre' no sea nulo
    $nombre = !empty($_POST['nombre']) ? trim($_POST['nombre']) : '';
	if($nombre == ''){
		Denko::addErrorMessage('error_requiredField',array('%field' => 'usuario_nombre'));
	}

    # Verifico que el valor en el campo 'email' sea v�lido
    $email = !empty($_POST['email']) ? trim($_POST['email']) : '';
    if($email == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'usuario_email'));
    }
    elseif(!Denko::hasEmailFormat($email)){
        Denko::addErrorMessage('error_invalidField',array('%field' => 'usuario_email'));
    }

    # Verifico que el valor en el campo 'telefono' no sea nulo
    $telefono = !empty($_POST['telefono']) ? trim($_POST['telefono']) : '';
    if($telefono == ''){
        Denko::addErrorMessage('error_requiredField',array('%field' => 'usuario_telefono'));
    }

    # En caso que no haya errores
    if(!Denko::hasErrorMessages()){

        # Verifico que si cambi� el username, no exista en la DB
        $id_usuario = $_POST['id_usuario'];
        $username = strtolower($username);
        $whereAdd = 'username = \''.strtolower($username).'\' and id_sitio = '.$id_sitio;
        $daoOtroUsuario = DB_DataObject::factory('usuario');
        $daoOtroUsuario->whereAdd($whereAdd);
        if($daoOtroUsuario->find(true) && ($action == 'insert' || ($action == 'edit' && $daoOtroUsuario->id_usuario != $daoUsuario->id_usuario))){
            $smarty->config_load('speech.conf','usuario_abm');
            Denko::addErrorMessage('usuario_errorDuplicateUsername',array('%username' => $username));
            return false;
        }

        # Seteo los datos del Usuario
        $daoUsuario->username = $username;
        if($action== 'insert'){
            $daoUsuario->password = $password;
        }
        $daoUsuario->id_sitio = $_POST['id_sitio'];
        $daoUsuario->apellido = $apellido;
        $daoUsuario->nombre = $nombre;
        $daoUsuario->email = $email;
        $daoUsuario->telefono = $telefono;
        $daoUsuario->habilitado = !empty($_POST['habilitado']) ? '1' : '0';

        # En caso que haya que agregar al Usuario
        if($action == 'insert'){
            $daoUsuario->insert();
        }

        # En caso que haya que editar los datos del Usuario
        else{
            $daoUsuario->update();
        }

        # Me redirijo al view del Usuario
        Denko::redirect('usuario_abm.php?action=view&id_usuario='.$daoUsuario->id_usuario);
    }
}

################################################################################
# Verifico que haya usuario logueado
Inny_Tracker::verificarUsuarioLogueado();

# Creo la instancia Smarty y cargo el filtro para los includes
$smarty = new Smarty();
$smarty->load_filter('output','dk_include');

# Inicializo
Inny_Common::initialize($smarty);

$linkBackUsuarios = new Inny_LinkBackUsuarios();
$linkBack = $linkBackUsuarios->getLinkBack();

# Verifico que la acci�n sea v�lida
Inny_Common::verificarAccion(array('view','insert','edit','changepass','enable','disable'));

# DAO Usuario
$daoUsuario = DB_DataObject::factory('usuario');

# En caso que la acci�n sea distinta a 'insert', verifico que el DAO sea v�lido
$action = InnyCore_Utils::getParamValue('action');
if($action != 'insert'){

    # Verifico que el ID del usuario exista y sea v�lido
    $id_usuario = InnyCore_Utils::getParamValue('id_usuario');
    $daoUsuario = Inny_Common::verificarDao($smarty,'usuario',$id_usuario);
    $daoUsuario->fetch();

    # En caso de habilitar o deshabilitar al Usuario
    if($action == 'enable' || $action == 'disable'){
        $daoUsuario->habilitado = ($action == 'enable') ? '1' : '0';
        $daoUsuario->update();
        Denko::redirect($linkBack);
    }
}

# En caso de agregar/editar un Usuario, verifico las variables en POST
if($_SERVER['REQUEST_METHOD'] == 'POST' && ($action == 'insert' || $action == 'edit' || $action == 'changepass')){
    if($action == 'changepass'){
        validarUsuarioPassword($smarty,$daoUsuario);
    }
    else{
        validateUsuarioData($smarty,$daoUsuario);
    }
}

# Asigno el DAO del usuario y el link de volver al template
$smarty->assign('daoUsuario',$daoUsuario);
$smarty->assign('linkBack',$linkBack);

# Muestro el template
$template = 'view';
switch($action){

    # En caso de mostrar el template de ver datos del usuario
    case 'view': $template = 'usuario_view.tpl'; break;

    # En caso de mostrar el template de cambio de contrase�a
    case 'changepass': $template = 'usuario_changepass.tpl'; break;

    # En caso de mostrar el template de edici�n
    default: $template = 'usuario_abm.tpl'; break;
}
$smarty->display($template);

################################################################################